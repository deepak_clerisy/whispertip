<?php

class Facebook extends CI_Controller {
	
	//public $client_id = '766856950020170';
	//public $client_id = '158968874481744';
	 public $client_id = '911669065540991';

	//public $app_secret = '555e71d278060d47a4843c86d993e5bf';
	//~ public $app_secret = '514bd42387b6e43f410636b9ca04f37c';
	 public $app_secret = '6e980ecf95d10d215a249d17a8526274';
	 public $red_url = '';
	 public $site_url;
	public function __construct()
	{
		parent::__construct();		
		$this->load->library('session');
		$this->load->helper('cookie');		
		$this->load->library('authorize');		
		$this->load->model('register_user');
		$this->load->model('betfair_model');	
		$this->site_url = base_url();
			
	}
	function index()
	{	
		//header('location: https://www.facebook.com/dialog/oauth?client_id='.$this->client_id.'&redirect_uri=http%3A%2F%2Fwww.whispertip.com%2F%3Fc%3Dfacebook%26m%3Dregister&scope=email,user_birthday&response_type=code');	
		header('location: https://www.facebook.com/dialog/oauth?client_id='.$this->client_id.'&redirect_uri='.$this->site_url.'facebook/register&scope=email,user_birthday&response_type=code');	
	 } 
	 function register()
	 {

		 ////////////////////GET ACCESS TOKEN////////////////////
		 
			$get = explode("?code=",$_SERVER['REQUEST_URI']);
			$ch = curl_init();
			$geturl = "https://graph.facebook.com/oauth/access_token?client_id=".$this->client_id.
			"&redirect_uri=".$this->site_url."facebook/register&client_secret=".$this->app_secret."&code=$get[1]";			
			curl_setopt($ch, CURLOPT_URL, $geturl);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		    $userdata = curl_exec($ch);
			curl_close($ch);
			$this->betfair_model->trackErrors($userdata);
			
		//////////////////GET USER DATA////////////////////////	
		
			$accesstoken = explode('&',$userdata);
			$Input_token = explode('=',$accesstoken[0]);
			
			$ch = curl_init();
			$geturl = "https://graph.facebook.com/me?access_token=".$Input_token[1];			
			curl_setopt($ch, CURLOPT_URL, $geturl);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			$userdata1 = curl_exec($ch);			
			curl_close($ch);
			$json = json_decode($userdata1, true);
			//~ echo "<pre>";
			//~ print_r($json);
			//~ die();
			if(isset($json['hometown']))
			{
					$add = $json['hometown']['name'];
					$pos = strpos($add, ',');
					if($pos == true)		
					{
						$con = explode(',',$add);
						$country = trim($con[1],' ');
						$city = trim($con[0],' '); 
						$address = $city."+".$country;
					}
					else
					{
						$city= '';
					$address = $country = trim($add,' ');
						 
						
					}
			}		
			
			
/*------------------------------------------------------------------------------------------*/			
//////////////////////////////////////curl begins///////////////////////////////////////////
/*------------------------------------------------------------------------------------------*/			
				
			if(isset($json['birthday']))
			{
				$birthday = $json['birthday'];
			}	
			else
			{
				$birthday = '';
			}	
			if(isset($json['gender']))
			{
				$gender = $json['gender'];
			}	
			else
			{
				$gender = '';
			}	
			if(isset($json['name']))
			{
				$screen_name = $json['name'];
			}	
			else
			{
				$screen_name = '';
			}	
			if(isset($json['id']))
			{
				$facebook_id = $json['id'];
			}	
			else
			{
				$facebook_id = '';
			}	
			if(isset($json['email']))
			{
				$email = $json['email'];
			}	
			else
			{
				$email = '';
			}	
			if(isset($json['first_name']))
			{
				$first_name = $json['first_name'];
			}	
			else
			{
				$first_name = '';
			}	

			if(isset($json['last_name']))
			{
				$last_name = $json['last_name'];
			}	
			else
			{
				$last_name = '';
			}
			
			if(isset($json['link']))
			{
				$image = $json['link'];
			}	
			else
			{
				$image = '';
			}	
			
			
			$data['check'] = $this->register_user->check_user($email);
			if ($data['check'] == true) 
			{  			
				if( $this->authorize->validate_user_facebook($facebook_id) == true )
					{
					echo '<script type="text/javascript">
						  function hasStorage() {
							try {
								window.localStorage.setItem("mod", "");
								window.localStorage.removeItem("mod");
								return true;
							} catch (exception) {
								return false;
							}
						}
						if(hasStorage()) {
							var arrVal = window.localStorage.getItem("redirect_url");
							if(arrVal=="undefined"){
								location.href="'.$this->site_url.'home/user_profile";
							} else {
								location.href = arrVal;
							}	
						} else {
							location.href="'.$this->site_url.'home/user_profile";
						}
					</script>';
				 }
			}		
			else 
			{ 
				$data['facebook'] = $this->register_user->facebook($email,  $facebook_id, $first_name, $last_name, $screen_name, $gender, $birthday,$image);
				if($data['facebook'] == true)
				{
					if( $this->authorize->validate_user_credentials_social($email) == true )
					{					 
						
					echo '<script type="text/javascript">
					location.href="'.$this->site_url.'home/user_profile";
					</script>';
					 }
				}
			}    
			 
	 }
	 
}
?>

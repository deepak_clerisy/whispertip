<?php
class Google extends CI_Controller
 {
	 public $site_url;
	 function __construct()
	{
		parent::__construct();
		$this->config->load('config');
 		$this->load->helper('cookie');	
		$this->load->library('authorize');	
		$this->load->helper('url');		
		$this->load->model('register_user');
		$this->site_url = base_url();
	}
	
	function index()
	{ 
		$type = $this->uri->segment(3,'');
		$client_id = $this->config->item('googleplus_client_id');
		$client_secret = $this->config->item('googleplus_client_secret');
		$redirect_url = $this->config->item('googleplus_redirect_url');
		
	
		if($type == 'popup')
	    { 
			echo 'https://accounts.google.com/o/oauth2/auth?response_type=code&client_id='.$client_id.'&redirect_uri='.$redirect_url.'&scope=email' ; 
			exit();
	    }
	    else
	    {
			header('location: https://accounts.google.com/o/oauth2/auth?response_type=code&client_id='.$client_id.'&redirect_uri='.$redirect_url.'&scope=email');
	    }
		
	}
	
	function register()
	{ 
		$domain = $this->session->userdata('accDomain');
		$token = $this->session->userdata('accToken');
		$cf = $this->session->userdata('accCf');
		$lang = $this->session->userdata('accLang');
		
		$getcode = explode("?code=",$_SERVER['REQUEST_URI']);
		$code = $getcode[1];
		//echo $code; die;
		try
		{
			if(!$code)
			{
				 throw new Exception("Code Not Found"); 
			}
			else
			{
				$accesstoken = $this->getAcessToken($code);	
				if(!$accesstoken)
				{
					throw new Exception("Invalid token"); 
				}
				else
				{
					$json = $this->getUserInfo($accesstoken) ;
					//~ echo "<pre>" ;
					//~ print_r($json) ;
					//~ die();
					if(isset($json['id']))
					{
						$google_id = $json['id'];
					}
					else
					{
					   $google_id = '' ;
					}
		           	if(isset($json['email']))
					{
					    $email = $json['email'];
					}
					else
					{
					   $email = $google_id.'@Googleplus' ;
					}
		           	if(isset($json['name']['givenName']))
					{
					    $first_name = $json['name']['givenName'];
					}
					else
					{
					   $first_name = '' ;
					}
		            if(isset($json['name']['familyName']))
					{
					    $last_name = $json['name']['familyName'];
					}
					else
					{
					   $last_name = '' ;
					}
		           		if(isset($json['displayName']))
					{
					    $screen_name = $json['displayName'];
					}
					else
					{
					   $screen_name = '' ;
					}
		
					$google_email = $google_id.'@Googleplus' ;
		
					$country = '' ;
					
					if(isset($json['gender']))
					{
					$gender = $json['gender'];
					}
					else
					{
						$gender = '' ;
					}
					
					$introMsg = '' ;
				
					$birthday = '' ;
		             
		           	if(isset($json['image']['url']))
					{
					$avatar = $json['image']['url'];
					}
					else
					{
						$avatar = '' ;
					}
					
					$vendor = 'Googleplus' ;
					$last_login_date = strtotime(date('d-m-Y H:i:s'));
					$registration_date = strtotime(date('d-m-Y H:i:s'));
					//$registration_hash = $this->passwordhash->generateToken();
					$registration_hash = '';
					$sysUpdated = '1';
					$sys_open = '1';
					$getResDomain = explode('@',$email);
					$resDomain = $getResDomain[1];
					//~ if($this->authentication->checkForRestrictedDomain($resDomain) == true)
					//~ {
						 //~ throw new Exception('Error in processing. Please try again later.') ;
					//~ }
				   //~ else 
				    //~ {
						
					$data['check'] = $this->register_user->check_user($email);
					if ($data['check'] == true) 
					{
						if( $this->authorize->validate_user_credentials_social($email) == true )
							{
							echo '<script type="text/javascript">
								  function hasStorage() {
									try {
										window.localStorage.setItem("mod", "");
										window.localStorage.removeItem("mod");
										return true;
									} catch (exception) {
										return false;
									}
								}
								if(hasStorage()) {
									var arrVal = window.localStorage.getItem("redirect_url");
									if(arrVal=="undefined"){
										location.href="'.$this->site_url.'home/user_profile";
									} else {
										location.href = arrVal;
									}	
								} else {
									location.href="'.$this->site_url.'home/user_profile";
								}
							</script>';
						 }
					}
					else 
					{ 
						$data['google'] = $this->register_user->google_plus($email,  $google_id, $first_name, $last_name, $screen_name, $gender, $birthday,$avatar);
						
						if( $data['google'] == true) {
							if( $this->authorize->validate_user_credentials_social($email) == true )
							{					 
								
							echo '<script type="text/javascript">
							location.href="'.$this->site_url.'home/user_profile";
							</script>';
							 }
						}
						
					}
				}
			}	
		}
		catch (Exception $e)
		 {
			//echo $e->getMessage();
			//die;
			$data['ulang']  = $lang ;
			$data['domain'] = $domain ;
			$data['token']  = $token ;
			$data['cf']     = $cf ;
			$data['error']  = $e->getMessage();
			$this->load->view('thirdparty_error_view',$data);
		 }
	}
	
	function getAcessToken($code)
	{
		$client_id = $this->config->item('googleplus_client_id');
		$client_secret = $this->config->item('googleplus_client_secret');
		$redirect_url = $this->config->item('googleplus_redirect_url');
		$url = 'https://accounts.google.com/o/oauth2/token';
		$postfields='code='.$code.'&client_id='.$client_id.'&client_secret='.$client_secret.'&redirect_uri='.$redirect_url.'&grant_type=authorization_code';
		
		$ch = curl_init();	
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 'Content-type: application/x-www-form-urlencoded');
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

		
		$userdata = curl_exec($ch);
		$getInfo = curl_getinfo($ch);
		curl_close($ch);

		if($getInfo['http_code'] == 200)
		 {
			$data = json_decode($userdata);
			if(isset($data->error))
			{
				$accessToken = '';
			}
			else
			{
				$accessToken = $data->access_token ;
			}
		} 
		else
		{
			throw new Exception("Error in access token!"); 
		}
		return $accessToken;
	}
	
	function getUserInfo($accessToken)
	{
		$information = array();
		$new_url = 'https://www.googleapis.com/plus/v1/people/me?access_token='.$accessToken ;
		$new_url1 = 'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token='.$accessToken;
		//echo $new_url;die;
		$ch = curl_init();	
		curl_setopt($ch, CURLOPT_URL, $new_url);
		curl_setopt($ch, CURLOPT_HEADER, 'Content-type: application/x-www-form-urlencoded');
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		$new_userdata = curl_exec($ch);
		$getinformation = curl_getinfo($ch);
		curl_close($ch);
		if($getinformation['http_code'] == 200)
		{
			$ch1 = curl_init();	
			curl_setopt($ch1, CURLOPT_URL, $new_url1);
			curl_setopt($ch1, CURLOPT_HEADER, 'Content-type: application/x-www-form-urlencoded');
			curl_setopt($ch1, CURLOPT_CONNECTTIMEOUT, 30);
			curl_setopt($ch1, CURLOPT_TIMEOUT, 30);
			curl_setopt($ch1, CURLOPT_RETURNTRANSFER, TRUE);
			$new_userdata1 = curl_exec($ch1);
			$getEmail = curl_getinfo($ch1);
			curl_close($ch1);
			if($getEmail['http_code'] == 200)
			 {
				$basic_info = json_decode($new_userdata, true);
				$email_info = json_decode($new_userdata1, true);
				$final = array_merge($basic_info,$email_info); 
				return $final ;
			}
		}
		else
		{
			throw new Exception("This email is not valid !"); 
		}
	}
  }
?>

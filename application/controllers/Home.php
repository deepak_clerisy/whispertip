<?php
class Home extends CI_Controller
{
	var $key = 'Ahk8IHLMfNtjpTNwtPrTH1gGD';
	var $secret = 'xeD3NTsksNOzSUfKjlnQ621m5G9974VL2uOuHLIz1OOEcv0Iti';
	var $request_token = "https://twitter.com/oauth/request_token";
	var $access_token = "https://api.twitter.com/oauth/access_token";
	var $get_user_details = "https://api.twitter.com/1.1/account/verify_credentials.json";
	
	function __construct()
	{
		//session_start();session_destroy();die;
		parent::__construct();
		$this->config->set_item('enable_query_strings', TRUE);
		
		$this->load->model('Register_user');
		$this->load->model('category_model');
		$this->load->model('member_model');
		//~ $this->load->model('wallet_model');
		$this->load->model('betfair_model');
		$this->load->model('venue_model');
		$this->load->library('authorize');
		
		//$this->load->library('paypal');
		$remoteAddr = $_SERVER['REMOTE_ADDR'];
		error_reporting(-1);
		if($_SERVER['REMOTE_ADDR']=='127.0.0.1' || $_SERVER['REMOTE_ADDR']=='::1' || strstr($_SERVER['REMOTE_ADDR'],'192.168.')==$_SERVER['REMOTE_ADDR'])
		{
			$remoteAddr = '122.173.135.80';
		}
		//$timeZone = $this->authorize->getUserTimeZone($remoteAddr);
		$timeZone = $this->session->userdata('timezone');
		if($timeZone=='')
		{
			$timeZone = $this->authorize->getUserTimeZone($remoteAddr);
			$this->session->set_userdata('timezone',$timeZone);
		}
		$this->session->set_userdata('timezone',$timeZone);
		date_default_timezone_set($timeZone);
	}
	
	function index()
	{
		/*$data = array();
		$newdata = array(
					'twitter_id'  => '121212',
					'first_name' => 'satish',				
					'via' => 'twitter',					
					'signup_date' => date('Y/m/d H:m:s')                 
               );
			$this->session->set_userdata('twittersess',$newdata);
		$resp = $this->authorize->user_loggedIn();
		if($resp===true)
		{
			//session_start();session_destroy();die;
			redirect(base_url().'select');
		}
		$this->load->view("template_landing",$data);	
		*/
		$data = array();
		if($this->session->userdata('twitter_auth'))
		{
			$data['auth'] = $this->session->userdata('twitter_auth');
		}
		else
		{
			$data['auth'] = '';
		}
		$this->load->view("new_design",$data);	
	}
	
	function activate()
	{
		$decode = $this->uri->segment(3);
		$code = base64_decode($decode);
		$emailcode = explode('&',$code);
		$email = $emailcode[0];
		$code = $emailcode[1];
		$get = $this->Register_user->verifyCode($email,$code);
		
		if($get == true)
		{				
			if( $this->authorize->validate_user_credentials_social($email) == true )
				{
					
					redirect(base_url().'home');
				}
		}
		else
		{
			
				redirect(base_url().'home');
		}
	}
	
	function verify_venue()
	{
		$decode = $this->uri->segment(2);
		$code = base64_decode($decode);
		$emailcode = explode('&',$code);
		$email = $emailcode[0];
		$code = $emailcode[1];
		$get = $this->Register_user->verify_venue($email,$code);
		
		if($get == true)
		{				
			$this->session->set_flashdata('success','Your account verified successfully.');
			redirect(base_url().'login/venue');
		}
		else
		{
			$this->session->set_flashdata('fail','Your account not verified.Please try again.');
			redirect(base_url().'login/venue');
		}
		
	}
	
	function verify_patron()
	{
		$decode = $this->uri->segment(2);
		$code = base64_decode($decode);
		$emailcode = explode('&',$code);
		$email = $emailcode[0];
		$code = $emailcode[1];
		$get = $this->Register_user->verify_patron($email,$code);
		
		if($get == true)
		{				
			$this->session->set_flashdata('success','Your account verified successfully.');
			redirect(base_url().'login/patron');
		}
		else
		{
			 $this->session->set_flashdata('fail','Your account not verified.Please try again.');
			redirect(base_url().'login/patron');
		}
		
	}
	
	function login()
	{
	 	$userName = $this->input->post('lguserName');
	 	$userPassword = $this->input->post('lguserPassword');
		$resp = $this->Register_user->login($userName,$userPassword);
		if ($resp == false) 
		{ 
			$response = 'error';
			echo json_encode($response);
		}
		else
		{
			$response = 'done';
			echo json_encode($response);
		}			
	}
	
	function forgot_password()
	{
		$data['title']='Forgot Password';
		 $user_type=$this->uri->segment(2); 
		 if(empty($user_type))
		 {
			 $user_type='patron';
		 }


		if($user_type=='venue')
		{
			$this->form_validation->set_rules('email','Email','trim|required');
			if($this->form_validation->run() == true)
			{
					$email = $this->input->post('email');
					$resp = $this->venue_model->check_venue_email($email);
					if ($resp == false) 
					{ 
						$this->session->set_flashdata('fail','Email does not exist');
						redirect(base_url().'forgot/venue');
						exit;
					}
					else
					{
						$to = $email;
						
						$email_template_id = 4;
						$first_name = $resp['venue_name'];
						$last_name = '';
						$name = $resp['email'];
						$user_id=$resp['venue_id'];
						
						$password = $this->venue_model->random_code();
						
						if($this->Register_user->reset_venue_password($user_id,$password)==true)
						{
							$email_content = $this->getEmailTemplate($email_template_id);
							$message = $email_content[0]['body'];							
							$search = array('{FIRSTNAME}','{LASTNAME}','{SITENAME}','{USEREMAIL}','{USERNAME}','{PASSWORD}', '{SITELINK}');
							$replace= array($first_name,$last_name,'Whispertip',$to,$name,$password,'www.whispertip.com/');
							$body=str_replace($search,$replace,$message);	
							$data['template_body'] = $body;
							$page = $this->load->view('email_template',$data,true);	
							$config['charset'] = 'iso-8859-1';
							$config['mailtype'] = 'html';
							$subject = $email_content[0]['subject'];
							$from = $email_content[0]['from'];
							$this->load->library('email');
							$this->email->initialize($config);
							$this->email->from($from);
							$this->email->to($to);
							$this->email->subject($subject);
							$this->email->message($page);
							$email_status = $this->email->send();
							$this->session->set_flashdata('success','Mail Sent Successfully');
							redirect(base_url().'forgot/venue');
						
						}
						
					}	
					
			}
			$data['login_status'] = '';
			$data['page_body'] = $this->load->view("forgot_password",$data,TRUE);	
			$this->load->view("templates_main",$data);
		}
		else if($user_type=='patron')
		{
			$this->form_validation->set_rules('email','Email','trim|required');
			if($this->form_validation->run() == true)
			{
					$email = $this->input->post('email');
					$resp = $this->Register_user->check_email($email);
					if ($resp == false) 
					{ 
						$this->session->set_flashdata('fail','Email does not exist');
						redirect(base_url().'forgot/patron');
						exit;
					}
					else
					{
						$to = $email;
						$email_template_id = 4;
						$first_name = $resp['first_name'];
						$last_name = $resp['last_name'];
						$name = $resp['username'];
						$user_id=$resp['id'];
						
						$password = $this->venue_model->random_code();
						
						if($this->Register_user->reset_patron_password($user_id,$password)==true)
						{
							$email_content = $this->getEmailTemplate($email_template_id);
							$message = $email_content[0]['body'];							
							$search = array('{FIRSTNAME}','{LASTNAME}','{SITENAME}','{USEREMAIL}','{USERNAME}','{PASSWORD}', '{SITELINK}');
							$replace= array($first_name,$last_name,'Whispertip',$to,$name,$password,'www.whispertip.com/');
							$body=str_replace($search,$replace,$message);	
							$data['template_body'] = $body;
							$page = $this->load->view('email_template',$data,true);	
							$config['charset'] = 'iso-8859-1';
							$config['mailtype'] = 'html';
							$subject = $email_content[0]['subject'];
							$from = $email_content[0]['from'];
							$this->load->library('email');
							$this->email->initialize($config);
							$this->email->from($from);
							$this->email->to($to);
							$this->email->subject($subject);
							$this->email->message($page);
							$email_status = $this->email->send();
							$this->session->set_flashdata('success','Mail Sent Successfully');
							redirect(base_url().'forgot/patron');
						
						}
						
					}	
					
			}
			$data['login_status'] = '';
			$data['page_body'] = $this->load->view("forgot_password",$data,TRUE);	
			$this->load->view("templates_main",$data);
				//$this->load->view('forgot_password');
		}
		else
		{	 
			redirect(base_url());
		}
					
	}
	function getEmailTemplate($id)
	{
		$this->db->select('*');
		$this->db->where('email_template_id', $id);
		$query = $this->db->get('email_templates');
		return $query->result_array();
	}
	
		
	function logout()
	{
	 	$this->session->sess_destroy();
	 	header('Location:'.base_url());
		//header('Location:'.base_url().'home');
	}
		
	public function change_password() {
		$data = array();
		$data['title']='Change Password';
		$resp = $this->authorize->user_loggedIn();
		if($resp==false)
		{
			redirect(base_url().'home');
		}
		else{
			$data['login_status'] = 1;
		}
		
		$this->form_validation->set_rules('current_password', 'Current password', 'trim|required');
		$this->form_validation->set_rules('new_password', 'New Password', 'trim|required');
		//$this->form_validation->set_rules('dob', 'Date of birth', 'trim|required');
		
		if ($this->form_validation->run() == TRUE) // form submitted
		{ 
		
			if($this->member_model->updatePassword() == true )
			{
				$this->session->set_flashdata('success','Password has been updated successfully.');
				redirect( base_url(). 'home/change_password');
			}
			else
			{
				$this->session->set_flashdata('error', 'Current password is not correct. Please try again.');
				redirect( base_url(). 'home/change_password');
			}
		}
		
	
		$data['type_tip'] = '';
		$data['place_val'] = '';
		$userId = $this->session->userdata('userid');
		$data['userId'] = $userId;
		$userData = $this->member_model->getMemberDetailsById($userId);
		$category = $this->category_model->getAllCategories();
		$data['userData'] = $userData;
		$data['categories'] = $category;
		$data['sidebar'] = $this->load->view("front_sidebar",$data,true);	
		$data['page_body'] = $this->load->view("change_password",$data,true);	
		$this->load->view("templates_main",$data);	
	}
	
	
	function profile(){ 
		
		$data = array();
		$data['title']='Edit Profile';
		$resp = $this->authorize->user_loggedIn();
		if($resp==false)
		{
			redirect(base_url().'home');
		}
		else
		{
			$data['login_status'] = 1;
		}
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('first_name', 'First name', 'trim|required');
		$this->form_validation->set_rules('last_name', 'Last name', 'trim|required');
		//$this->form_validation->set_rules('dob', 'Date of birth', 'trim|required');
		
		if ($this->form_validation->run() == TRUE) // form submitted
		{ 
		
			if($this->member_model->updateProfile() == true )
			{
				$this->session->set_flashdata('success','User profile has been updated successfully.');
				redirect( base_url(). 'home/profile');
			}
			else
			{
				$this->session->set_flashdata('error', 'user profile could not be uploaded. Please try again.');
				redirect( base_url(). 'home/profile');
			}
		}
		
		$data['type_tip'] = '';
		$isToken = $this->Register_user->getTwitterToken();
		if($isToken)
		{
			$res1 = $isToken;
			$sp1 = explode('&',$res1);
			$final = explode('=',$sp1[0]);
			$final1 = explode('=',$sp1[1]);
			$oauth = array( 'oauth_consumer_key' => $this->key,
                'oauth_nonce' => time(),
                'oauth_signature_method' => 'HMAC-SHA1',
                'oauth_token' => $final[1],
                'oauth_timestamp' => time(),
                'oauth_version' => '1.0');
			$url = "https://api.twitter.com/1.1/account/verify_credentials.json";
			$base_info = $this->buildBaseString($url, 'GET', $oauth);
			$composite_key = rawurlencode($this->secret) . '&' . rawurlencode($final1[1]);
			$oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
			$oauth['oauth_signature'] = $oauth_signature;
			$header = array($this->buildAuthorizationHeader($oauth), 'Expect:');
			$options = array( CURLOPT_HTTPHEADER => $header,
							  CURLOPT_HEADER => false,
							  CURLOPT_URL => $url,
							  CURLOPT_RETURNTRANSFER => true,                 
							  CURLOPT_SSL_VERIFYPEER => false);

			$feed = curl_init();
			curl_setopt_array($feed, $options);
			$json = curl_exec($feed);
			curl_close($feed);
			//var_dump($json);
			$twitter_data = json_decode($json,true);
			//~ $newdata['followers_count'] = $twitter_data['followers_count'];
		    //~ $newdata['friends_count']   = $twitter_data['friends_count'];
		    //~ $newdata['statuses_count']  = $twitter_data['statuses_count'];
		    //~ $this->session->set_userdata('twitterconnectsess',$newdata);
		}
		
		$data['place_val'] = '';
		$data['colortheme'] = "#2888C9";
		$userId = $this->session->userdata('userid');
		$userData = $this->member_model->getMemberDetailsById($userId);
		$category = $this->category_model->getAllCategories();
		$data['userData'] = $userData;
		$data['categories'] = $category;
		$data['sidebar'] = $this->load->view("front_sidebar",$data,true);	
		$data['page_body'] = $this->load->view("profile",$data,true);	
		$this->load->view("templates_main",$data);	
	}
	
	public function chk_email_exist() {
		$this->load->model('user');
		$u_email = $this->input->post('email');
		$user_id = $this->input->post('id');
		$result  = $this->user->chkemail_front($u_email,$user_id);
			
		if($result > 0){
			echo json_encode(FALSE);
		} else{
			echo json_encode(TRUE);
		}
	}
	
	function user_data()
	{
		$resp = $this->authorize->user_loggedIn();
		if($resp===true)
		{
			//session_start();session_destroy();die;
			redirect(base_url().'select');
		}
		$this->load->config();
		$userId = $this->uri->segment(2,0);
		$eventTypeId = $this->uri->segment(3,0);
		$events = $res = $this->betfair_model->getUserAllPlacedTipsEventType($userId,$eventTypeId);
		/*$won=0;
		$loose=0;
		$total = count($events);*/
		$status='';
		foreach($events as $event)
		{
			if($event['placed']==$event['result'])
			{
				$status .='W';
				//$won++;
			}
			else
			{
				$status .='L';
				//$loose++;
			}
		}
		/*if($total)
		{
			$strikeRate = round(($won/$total)*100,2);
		}
		else
		{
			$strikeRate=0;
		}*/
		$userStats = $this->betfair_model->getUserStatisticsForProfile($userId,$eventTypeId);
		//echo '<pre>';print_r($userStats);die;
		if(count($userStats))
		{
			$stRate = $userStats['strike_rate'];
			$ave_odds = $userStats['ave_odds'];
			$rank = $userStats['user_rank'];
		}
		else
		{
			$stRate=0;
			$ave_odds=0;
			$rank = 0;
		}
		$data['total_odds'] = $this->betfair_model->getUserOddsBets($userId,$eventTypeId);
		$data['status'] = substr($status,0,5);
		$data['strikeRate'] = $stRate;
		$data['ave_odds'] = round($ave_odds,2);
		$data['rank'] = $rank;
		$data['memberDetails'] = $this->member_model->getMemberDetailsById($userId);
		$page = $this->load->view('sections/user_data',$data,true);
		echo $page;
	}
	
	
	
	
	
	function view_page()
	{
		//for// acknowledgement faq for-artists privacy terms
	}
	function buildBaseString($baseURI, $method, $params)
	{
		$r = array(); 
		ksort($params); 
		foreach($params as $key=>$value){
			$r[] = "$key=" . rawurlencode($value); 
		}            

		return $method."&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r)); //return complete base string
	}
	function buildAuthorizationHeader($params)
	{
		$r = 'Authorization: OAuth ';
		$values = array();
		foreach($params as $key=>$value)
			$values[] = "$key=\"" . rawurlencode($value) . "\""; 

		$r .= implode(', ', $values); 
		return $r; 
	}
	
	
	
	function user_profile()
	{
	
		$data = array();
		$data['title']='My Profile';
		$resp = $this->authorize->user_loggedIn();
		$usid = $this->session->userdata('userid');
		if($resp==false)
		{
			redirect(base_url().'home');
		}
		else{
			$data['login_status'] = 1;
			}
		
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		$data['type_tip'] ='';
		$data['colortheme'] = "#2888C9";
		$isToken = $this->Register_user->getTwitterToken();
		if($isToken)
		{
			$res1 = $isToken;
			$sp1 = explode('&',$res1);
			$final = explode('=',$sp1[0]);
			$final1 = explode('=',$sp1[1]);
			$oauth = array( 'oauth_consumer_key' => $this->key,
                'oauth_nonce' => time(),
                'oauth_signature_method' => 'HMAC-SHA1',
                'oauth_token' => $final[1],
                'oauth_timestamp' => time(),
                'oauth_version' => '1.0');
			$url = "https://api.twitter.com/1.1/account/verify_credentials.json";
			$base_info = $this->buildBaseString($url, 'GET', $oauth);
			$composite_key = rawurlencode($this->secret) . '&' . rawurlencode($final1[1]);
			$oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
			$oauth['oauth_signature'] = $oauth_signature;
			$header = array($this->buildAuthorizationHeader($oauth), 'Expect:');
			$options = array( CURLOPT_HTTPHEADER => $header,
							  CURLOPT_HEADER => false,
							  CURLOPT_URL => $url,
							  CURLOPT_RETURNTRANSFER => true,                 
							  CURLOPT_SSL_VERIFYPEER => false);

			$feed = curl_init();
			curl_setopt_array($feed, $options);
			$json = curl_exec($feed);
			curl_close($feed);
			//var_dump($json);
			$twitter_data = json_decode($json,true);
			//~ $newdata['followers_count'] = $twitter_data['followers_count'];
		    //~ $newdata['friends_count']   = $twitter_data['friends_count'];
		    //~ $newdata['statuses_count']  = $twitter_data['statuses_count'];
		   // $this->session->set_userdata('twitterconnectsess',$newdata);
		}	
		if($usid)
		{
			$userId = $usid;
			$data['diff_user'] =1; 
			$userData = $this->member_model->getMemberDetailsById($usid);
		}
		else
		{
			$userId = $this->session->userdata('userid');
			$data['diff_user'] =0; 
			$userData = $this->member_model->getMemberDetailsById($userId);
		}
		
		$data['place_val'] = '';
		$category = $this->category_model->getAllCategories();
		$userData = $this->member_model->getMemberDetailsById($userId);
		$data['userId'] = $userId;
		$data['userData'] = $userData;
		$data['categories'] = $category;
		$data['sidebar'] = $this->load->view("front_sidebar",$data,true);	
		$data['page_body'] = $this->load->view("user_profile",$data,true);	
		$this->load->view("templates_main",$data);	
	}
	
	
	function how_it_work()
	{
		$data = array();
		$data['title']='How It Works';
		$uriVal = $this->uri->segment(3);
		
		if($uriVal=='place')
		{
			$statusvalue = 'loginPage';
		}
		else
		{
			$statusvalue = '';
		}
		$data['colortheme'] = "#2888C9";
		$data['type_tip'] ='';
		$data['place_val'] =$statusvalue;
		$userId = $this->session->userdata('userid');
		$data['userData'] = $this->member_model->getMemberDetailsById($userId);
		$resp = $this->authorize->user_loggedIn();
		if($resp==false)
		{
			$data['login_status'] = 0;
		}else{
			$data['login_status'] = 1;
		}
		$data['page_content'] = $this->betfair_model->getPlacePageContent();
		$data['page_content_buy'] = $this->betfair_model->getBuyPageContent();
		$data['page_body'] = $this->load->view("how_it_work_place",$data,true);	
		$this->load->view("templates_main",$data);	
	}
	function send_emails()
	{
		$data = array();
		$mail_result = $this->betfair_model->getAllPlacedUsers();
		foreach($mail_result as $results)
		{
			$user_id = $results['userid'];
			$userData = $this->betfair_model->getUserInfo($user_id);
			$total_val = (count($results)-1)/8 ;
			$txt_val = array();
			for($i = 1;$i<=$total_val;$i++)
			{
				$open_date = $results['open_date'.$i];
				$timezone = $results['timezone'.$i];
				if($results["placedtip".$i]==$results["resulttip".$i])
				{
						$status = "Won";
						$color_val = "color:#008000";
				}
				else
				{
						$status = "Loss";
						$color_val = "color:#FF0000";
					
				}
				$event_markets = $this->betfair_model->getEventsMarketsByMarketId($results['market_id'.$i]);
				$eventDate1 = $this->betfair_model->convert_time_zone($open_date,$timezone,$this->session->userdata('timezone'));
				$allLinks1 = date("l",strtotime($eventDate1))." ".date("d/m/Y",strtotime($eventDate1));
				$date =  $allLinks1 .' ('.date("h:i a",strtotime($eventDate1)).')'; 
				
				$txt_val[]= "".$date." $$$$$ ".$results["event_name".$i]." $$$$$ ".$results["team_name".$i]." $$$$$ ".$results['placedtip'.$i]." $$$$$ ".$status." $$$$$ ".$event_markets['market_name']." $$$$$ ".$color_val."";
			}
				$email_content = $txt_val;
				$email_temp ='';
				$spitVal = '';
				$email_temp .= '<table style="display: block;font-size: 16px;padding: 10px 0;" width="100%"><tbody style="width:100%;display:block;">';
				foreach($email_content as $content) {
					$spitVal = explode("$$$$$",$content); 
					$email_temp .= '  <tr class="result_date">
								<td colspan="3" style="font-size:15px; verticle-align:top">'.$spitVal[1].' - '.$spitVal[0].'</td>
								</tr>
							<tr style="border-bottom: 1px solid black;">
							<td style="font-size:14px;margin-right:10px;vertical-align: top; width:90%;">
								<span style="display:block;"> '.$spitVal[5].' - '.$spitVal[2].'</span>
							 </td>';
						$email_temp .= '<td style="font-size:14px; text-align:right;vertical-align: top;width:8%;'.$spitVal[6].'">'.'('.$spitVal[4].')'.'</td>
						</tr>
						<tr style="border-bottom: 1px solid rgb(0, 0, 0);" border="1"><td height="15" colspan="3"></td></tr>
						';
						}
			$email_temp .= '</tbody></table>';
			$email = $userData['email'];
			$to = $email;
			$email_template_id = 5;
			$first_name = $userData['first_name'];
			$last_name = $userData['last_name'];
			$name = $userData['username'];
			$password = $userData['password'];
			
			$email_content = $this->getEmailTemplate($email_template_id);
			$message = $email_content[0]['body'];
			
			$search = array('{FIRSTNAME}','{LASTNAME}','{EMAIL_RESULT}','{SITELINK}');
			$replace= array($first_name,$last_name,$email_temp,'www.whispertip.com');
			$body=str_replace($search,$replace,$message);	
			$data['template_body'] = $body;
			$page = $this->load->view('email_template',$data,true);	
			//echo $page;die;
			$config['charset'] = 'iso-8859-1';
			$config['mailtype'] = 'html';
			$subject = $email_content[0]['subject'];
			$from = $email_content[0]['from'];
			$this->load->library('email');
			$this->email->initialize($config);
			$this->email->from($from);
			$this->email->to($to);
			$this->email->subject($subject);
			$this->email->message($page);
			$email_status = $this->email->send();
			
		}
		$this->betfair_model->getTopWeekTipster();
	}
	function terms_conditions()
	{
		$this->load->model('page');
		$page_id = 5;
		$data = array();
		$data['title']='Terms Conditions';
		$content =  $this->page->get_pagecontent_by_id( $page_id );
		if( $content ) {
			$data['page_data'] = $content;
		} else {
			$data['page_data'] = array();
		}
		
		$data['place_val'] = '';
		$data['login_status'] = '';
		$data['colortheme'] = "#2888C9";
		$data['type_tip'] ='';
		$data['page_body'] = $this->load->view("terms_condition",$data,true);	
		$this->load->view("templates_main",$data);	
	}
	
	function privacy_policy()
	{
		$this->load->model('page');
		$page_id = 4;
		$data = array();
		$data['title']='Privacy Policy';
		$content =  $this->page->get_pagecontent_by_id( $page_id );
		if( $content ) {
			$data['page_data'] = $content;
		} else {
			$data['page_data'] = array();
		}
		$data['place_val'] = '';
		$data['login_status'] = '';
		$data['colortheme'] = "#2888C9";
		$data['type_tip'] ='';
		$data['page_body'] = $this->load->view("privacy_policy",$data,true);	
		$this->load->view("templates_main",$data);	
	}
	
	function login_popup()
	{
		$data = array();
		$mode = $this->uri->segment(3,0);
		if($mode)
		{
			$data['mode'] = $mode;
		}
		$this->load->view("login_form",$data);	
	}
	function delete_account()
	{
		$userId = $this->session->userdata('userid');
		$status = $this->betfair_model->deleteusers($userId);
		if($status=='true'){
			echo json_encode('sucess');
		}
		else
		{
			echo json_encode('ERROR');
		}
	}
	
	function get_pagination(){
				$event_type = $this->uri->segment(2,0);
				$data['event_type'] = $event_type;
				$userId = $this->session->userdata('userid');
				$data['userId'] = $userId;
				$data['hostPath'] =$this->config->item('path');
				if($event_type)
				{
					$data['category'] = $this->category_model->getAllCategories();
					$eventType = $event_type;
				}
				else
				{
					$data['category'] = $this->category_model->getAllCategories();
					$eventType = $category[0]['slugUrl'];
				}
				$data['eventType'] = $eventType;
				$event_type_id = $this->betfair_model->getCompetitionId($eventType);
				$data['event_type_id'] = $event_type_id ;
				$data['comp'] = $this->betfair_model->getAllCompById($event_type_id);
				$data['league'] = $this->betfair_model->getLeagueData($event_type_id);
				//echo '<pre>';print_r($data['league']);die;
				$this->load->library('pagination');
				$config['base_url'] = base_url().'home/get_pagination';
				$config['total_rows'] = count($data['league']);
				$config['per_page'] = 10;//$this->config->item('pagesize');
				$config['uri_segment'] = 3;
				$config['ajax'] = true;
				$this->pagination->initialize($config); 
				$data['total_rows'] = count($data['league']);
				$data['page_number'] = $this->uri->segment(3,0);
				$data['page_size'] = 10;
				
				$data['league'] = $this->betfair_model->getLeagueData($event_type_id,$data['page_number'], $data['page_size']);
			//	echo '<pre>';print_r($data['league']);die;
			
				echo $this->load->view("paging_leagues",$data);	
				
		}
	
		function user_public_profile(){
			$data = array();
			if($this->input->get('q')){
					$id_val = $this->input->get('q');
					$tempArr = explode('?',$id_val);
					$user_id = $tempArr[0];
				}
				if($this->uri->segment(2,0)){
					$user_id = $this->uri->segment(2,0);
				}
			$data['user_id'] = $user_id;
			$login_userId = $this->session->userdata('userid');
			if(!$user_id){
				redirect(base_url().'home');
				}
			$resp = $this->authorize->user_loggedIn();
			if($resp==false)
			{
				$data['login_status'] = 0;
			}else{
				$data['login_status'] = 1;
				$data['userData'] = $this->member_model->getMemberDetailsById($login_userId);
				}	
			$data['profiler_userData'] = $this->member_model->getMemberDetailsById($user_id);
			if(!$data['profiler_userData']){
				redirect(base_url().'home');
				}
			if($_SERVER['REMOTE_ADDR']=='127.0.0.1' || $_SERVER['REMOTE_ADDR']=='::1' || strstr($_SERVER['REMOTE_ADDR'],'192.168.')==$_SERVER['REMOTE_ADDR'])
			{
				$remoteAddr = '122.173.135.80';
			}
			//$timeZone = $this->authorize->getUserTimeZone($remoteAddr);
			$data['timezone'] = $this->session->userdata('timezone');
			
			if($data['timezone']=='')
			{
				$timeZone = $this->authorize->getUserTimeZone($remoteAddr);
				$this->session->set_userdata('timezone',$timeZone);
				$data['timezone'] = $timeZone;
			}
			
			date_default_timezone_set($timeZone);
			$data['type_tip'] ='';
			$data['colortheme'] = "#2888C9";
			$isToken = $this->register_user->getTwitterPublicToken($user_id);
			$data['twitter_user_id'] = '';
			if($isToken)
			{
				$res1 = $isToken;
				$sp1 = explode('&',$res1);
				$final = explode('=',$sp1[0]);
				$final1 = explode('=',$sp1[1]);
				$user_id_twitter = explode('-',$final[1]);
				$data['twitter_user_id'] = $user_id_twitter[0];
				/*$oauth = array( 'oauth_consumer_key' => $this->key,
					'oauth_nonce' => time(),
					'oauth_signature_method' => 'HMAC-SHA1',
					'oauth_token' => $final[1],
					'oauth_timestamp' => time(),
					'oauth_version' => '1.0');
				$url = "https://api.twitter.com/1.1/account/verify_credentials.json";
				$base_info = $this->buildBaseString($url, 'GET', $oauth);
				$composite_key = rawurlencode($this->secret) . '&' . rawurlencode($final1[1]);
				$oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
				$oauth['oauth_signature'] = $oauth_signature;
				$header = array($this->buildAuthorizationHeader($oauth), 'Expect:');
				$options = array( CURLOPT_HTTPHEADER => $header,
								  CURLOPT_HEADER => false,
								  CURLOPT_URL => $url,
								  CURLOPT_RETURNTRANSFER => true,                 
								  CURLOPT_SSL_VERIFYPEER => false);

				$feed = curl_init();
				curl_setopt_array($feed, $options);
				$json = curl_exec($feed);
				curl_close($feed);
				//var_dump($json);
				$twitter_data = json_decode($json,true);
				$newdata['followers_count'] = $twitter_data['followers_count'];
				$newdata['friends_count']   = $twitter_data['friends_count'];
				$newdata['statuses_count']  = $twitter_data['statuses_count'];
				$this->session->set_userdata('twitterconnectsess',$newdata);*/
			}	
			$userDataInfo = $this->betfair_model->getTipsterDataById($user_id);
			$data['userDataInfo'] = $userDataInfo;
			$data['place_tips'] = $this->betfair_model->getAllUserPlacedTips($user_id);
			
			$data['page_body'] = $this->load->view("user_public_profile",$data,TRUE);	
			$this->load->view("templates_main",$data);	
		}
		
	function registration($user_type)
	{
		$data['title']='Registration';
		$log_as= $this->session->userdata('user_logged_as'); 
		if($log_as)
		{
			if($log_as=='patron')
			{
				$url=base_url().'home/user_profile';
			}
			else
			{
				$url=base_url().'venues/profile';
			}
			redirect($url);
			exit;
		}
		
		
		 //$user_type=$this->uri->segment(2); 
		 $user_type=$this->uri->segment(2); 
		 if(empty($user_type))
		 {
			 $user_type = 'patron';
		 }
		 
		 if($user_type == 'venue')
		 {
				$this->form_validation->set_rules('email','Email', 'trim|required');
				$this->form_validation->set_rules('password','Password', 'trim|required');
		
				if ($this->form_validation->run() == true)
				{ 
					if($this->venue_model->register_venue() == true )
					{
						$this->session->set_flashdata('success','Venue added successfully.');
						redirect(base_url().'registration/venue');
						exit;
					}
					else
					{
						 $this->session->set_flashdata('fail','Venue not added.');
						 redirect(base_url().'registration/venue');
						 exit;
					 }
					
				}
			$data['login_status'] = '';
			$data['page_body'] = $this->load->view("signup_venue",$data,TRUE);	
			$this->load->view("templates_main",$data);
		 }
		 else if($user_type == 'patron')
		 {
			 $this->form_validation->set_rules('username','Username', 'trim|required');
			 $this->form_validation->set_rules('email','Email', 'trim|required');
			 $this->form_validation->set_rules('password','Password', 'trim|required');
		
				if ($this->form_validation->run() == true)
				{ 
					if($this->venue_model->register_patron() == true )
					{
						$this->session->set_flashdata('success','User added successfully.');
						redirect(base_url().'registration/patron');
						exit;
					}
					else
					{
						 $this->session->set_flashdata('fail','User not added.');
						 redirect(base_url().'registration/patron');
						 exit;
					 }
					
				}
			
			$data['login_status'] = '';
			$data['page_body'] = $this->load->view("signup_patron",$data,TRUE);	
			$this->load->view("templates_main",$data);
		 }
		 else
		 {
			redirect(base_url());
		 }
	}
	
	
	function login_new($user_type)
	{
		
		$data['title']='Login';
		
		$log_as= $this->session->userdata('user_logged_as'); 
		if($log_as)
		{
			if($log_as=='patron')
			{
				$url=base_url().'home/user_profile';
			}
			else
			{
				$url=base_url().'venues/profile';
			}
			redirect($url);
			exit;
		}
		
				

		 $user_type=$this->uri->segment(2); 
		 if(empty($user_type))
		 {
			 $user_type='patron';
		 }
		 if($user_type=='venue')
			{
				$this->form_validation->set_rules('email','Email', 'trim|required');
				$this->form_validation->set_rules('password','Password', 'trim|required');
		
				if ($this->form_validation->run() == true)
				{ 
					
					$result	= $this->venue_model->login_venue(); 
				 	 
				 	if($result > 0 )
					{	
						$this->session->set_flashdata('success','Venue logged successfully.');
						redirect(base_url().'venues/profile');
						exit;
					}
					else if($result == 'not_verified')
					{ 
						 $this->session->set_flashdata('fail','Your account not verified.');
						 redirect(base_url().'login/venue');
						 exit;
					}
					else if($result == 'not_active')
					{
						 $this->session->set_flashdata('fail','Your account not active.');
						 redirect(base_url().'login/venue');
						 exit;
					}
					else
					{
						 $this->session->set_flashdata('fail','Invalid login details.');
						 redirect(base_url().'login/venue');
						 exit;
					 }
					
				}
				
			$data['login_status'] = '';
			$data['page_body'] = $this->load->view("login_venue",$data,TRUE);	
			$this->load->view("templates_main",$data);
		//	 $this->load->view("login_venue");
		 }
		 else if($user_type=='patron')
		 {
				$this->form_validation->set_rules('email','Email', 'trim|required');
				$this->form_validation->set_rules('password','Password', 'trim|required');
		
				if ($this->form_validation->run() == true)
				{ 
					 $result = $this->venue_model->login_patron(); 
					
					 if($result > 0 )
					{
						$this->session->set_flashdata('success','Patron logged successfully.');
						redirect(base_url().'home/user_profile');
						exit;
					}
					
					else if($result == 'not_verified')
					{ 
						
						 $this->session->set_flashdata('fail','Your account not verified.');
						 redirect(base_url().'login/patron');
						 exit;
					}
					else if($result == 'not_active')
					{
						 $this->session->set_flashdata('fail','Your account not active.');
						 redirect(base_url().'login/patron');
						 exit;

					}
					
					else
					{
						 $this->session->set_flashdata('fail','Invalid login details.');
						 redirect(base_url().'login/patron');
						 exit;
					 }
					
				}
			$data['login_status'] = '';
			$data['page_body'] = $this->load->view("login_patron",$data,TRUE);	
			$this->load->view("templates_main",$data);
		 }
		 else
		 {
			 	 redirect(base_url());
		 }
		
	}
	
	public function check_patron_username()
	{
		
		$email=$this->input->post('username');
		
		$IsEmail=$this->venue_model->check_patron_username_exist($email);
		
		if($IsEmail)
		{
			echo json_encode(false);
		}else
		{
			echo json_encode(true);
		}
	}
	
	public function check_patron_email()
	{
		
		$email=$this->input->post('email');
		
		$IsEmail=$this->venue_model->check_patron_email_exist($email);
		
		if($IsEmail)
		{
			echo json_encode(false);
		}else
		{
			echo json_encode(true);
		}
	}
	
	public function check_venue_email()
	{
		
		$email=$this->input->post('email');
		
		$IsEmail=$this->venue_model->check_email_exist($email);
		
		if($IsEmail)
		{
			echo json_encode(false);
		}else
		{
			echo json_encode(true);
		}
	}
	
	public function view_leaderboard_index()
	{
		$data['title']='Leaderboard';
		
		$resp = $this->authorize->user_loggedIn();
		$usid = $this->session->userdata('userid');
		if($resp==false)
		{
			redirect(base_url().'home');
		}
		else
		{
			$data['login_status'] = 1;
			$data['place_val'] = '';
		
		}
			
		$userId = $this->session->userdata('userid');
		$userData = $this->member_model->getMemberDetailsById($userId);
		
		if( $userData['main_venue'] != '' && $userData['main_venue'] > 0) {
			redirect( base_url().'home/view_leaderboard');
		}
		
		$this->form_validation->set_rules('main_venue', 'Venue', 'trim|required');
		if ($this->form_validation->run() == TRUE) // form submitted
		{ 
			if($this->member_model->update_main_venue() == true )
			{
				$this->session->set_flashdata('success','Venue has been selected successfully.');
				redirect( base_url(). 'home/view_leaderboard');
			}
			else
			{
				$this->session->set_flashdata('error', 'Venue could not be selected. Please try again.');
				redirect( base_url(). 'home/view_leaderboard_index');
			}
		}
		$data['place_val'] = '';
		
		$venuelist = $this->venue_model->get_venue_list();
		if( $venuelist ) {
			$data['venue_list'] = $venuelist;
		} else {
			$data['venue_list'] = array();
		}
		$data['userData'] = $userData;
		$data['sidebar'] = $this->load->view("front_sidebar",$data,true);	
		$data['page_body'] = $this->load->view("patron_leaderboard_index",$data,true);	
		$this->load->view("templates_main",$data);
		
	}
	
	
	public function view_leaderboard()
	{
		$data['title']='Leaderboard';
		$resp = $this->authorize->user_loggedIn();
		$usid = $this->session->userdata('userid');
		if($resp==false)
		{
			redirect(base_url().'home');
		}
		else
		{
			$data['login_status'] = 1;
			$data['place_val'] = '';
		
		}
			
		$userId = $this->session->userdata('userid');
		$userData = $this->member_model->getMemberDetailsById($userId);
		$venue = $userData['main_venue'];
		
		if( $this->uri->segment(3) != '' ) {
			$type = $this->uri->segment(3);
			$data['total'] = $this->Register_user->get_total_match_by_venue($venue,$type); 
			$result = $this->Register_user->get_patron_leaderboard($venue,$type);
			
			$users=array();
			if(count($result) && is_array($result))
			{
				$all_users=array_count_values(array_column($result, 'id'));			
				arsort($all_users);
				
				foreach($all_users as $user => $count)
				{
					$userDetails=$this->Register_user->get_user_by_id($user);
					$userDetails['result']=$count;
					$users[$user]=$userDetails;
					
				} 
			}
			
			$data['userData'] = $userData;
			$data['leaderboard_data'] = $users;
			$data['sidebar'] = $this->load->view("front_sidebar",$data,true);	
			$data['page_body'] = $this->load->view("view_patron_leaderboard",$data,true);	
			$this->load->view("templates_main",$data);	
			
		}
		else
		{
			$data['userData'] = $userData;
			$data['sidebar'] = $this->load->view("front_sidebar",$data,true);	
			$data['page_body'] = $this->load->view("view_patron_leaderboard",$data,true);	
			$this->load->view("templates_main",$data);
			
		}
			
	}
	

}

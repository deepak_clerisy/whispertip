<?php
class Tips extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->config->set_item('enable_query_strings', TRUE);
		
		$this->load->model('venue_model');
		$this->load->model('member_model');
		$this->load->model('competition');
		$this->load->library('authorize');
		
		$remoteAddr = $_SERVER['REMOTE_ADDR'];
		error_reporting(-1);
		if($_SERVER['REMOTE_ADDR']=='127.0.0.1' || $_SERVER['REMOTE_ADDR']=='::1' || strstr($_SERVER['REMOTE_ADDR'],'192.168.')==$_SERVER['REMOTE_ADDR'])
		{
			$remoteAddr = '122.173.135.80';
		}
		//$timeZone = $this->authorize->getUserTimeZone($remoteAddr);
		$timeZone = $this->session->userdata('timezone');
		if($timeZone=='')
		{
			$timeZone = $this->authorize->getUserTimeZone($remoteAddr);
			$this->session->set_userdata('timezone',$timeZone);
		}
		$this->session->set_userdata('timezone',$timeZone);
		date_default_timezone_set($timeZone);
	}
	
	public function index() {
		$resp = $this->authorize->user_loggedIn();
		if($resp==false)
		{
			redirect(base_url().'home');
		}
		else
		{
			$data['login_status'] = 1;
		}
		
		$userId = $this->session->userdata('userid');
		$userData = $this->member_model->getMemberDetailsById($userId);
		
		if( $userData['main_venue'] != '' && $userData['main_venue'] > 0) {
			redirect( base_url().'tips/competition');
		}
		
			$this->form_validation->set_rules('main_venue', 'Venue', 'trim|required');
			if ($this->form_validation->run() == TRUE) // form submitted
			{ 
				if($this->member_model->update_main_venue() == true )
				{
					$this->session->set_flashdata('success','Venue has been selected successfully.');
					redirect( base_url(). 'tips/competition');
				}
				else
				{
					$this->session->set_flashdata('error', 'Venue could not be selected. Please try again.');
					redirect( base_url(). 'tips/');
				}
			}
		
		$data['place_val'] = '';
		
		$venuelist = $this->venue_model->get_venue_list();
		if( $venuelist ) {
			$data['venue_list'] = $venuelist;
		} else {
			$data['venue_list'] = array();
		}
		$data['userData'] = $userData;
		$data['sidebar'] = $this->load->view("front_sidebar",$data,true);	
		$data['page_body'] = $this->load->view("tips_index",$data,true);	
		$this->load->view("templates_main",$data);	
	}
	
	public function competition() {
		$resp = $this->authorize->user_loggedIn();
		if($resp==false)
		{
			redirect(base_url().'home');
		}
		else
		{
			$data['login_status'] = 1;
		}
		
		$data['place_val'] = '';
		$userId = $this->session->userdata('userid');
		$userData = $this->member_model->getMemberDetailsById($userId);
		$venue = $userData['main_venue'];
		$data['userData'] = $userData;
		
		if( $this->uri->segment(3) != '' ) {
			$comp_type = $this->uri->segment(3);
			$round = ( $this->uri->segment(4) != '' ) ? $this->uri->segment(4) : 1 ;
			
			$compList = $this->competition->fetch_competitions_list( $comp_type, $venue, $round );
			if( $compList ) {
				$data['comp_list'] = $compList;
			} else {
				$data['comp_list'] = array();
			}
			
			$selectedComp = $this->competition->fetch_selected_tips( $comp_type, $venue, $round, $userId );
			if( $selectedComp ) {
				$data['selected_comp'] =  $selectedComp;
			} else {
				$data['selected_comp'] = array();
			}
		}	
		
		
		$save_tips = $this->input->post('save_tips');
		if( isset($save_tips) ) {
			$inert_arr=array();
			$sn = 0;
			foreach( $this->input->post() as $key => $value ) {
				if( strstr($key,"match_") ){
					$sn++;
					$key_array = explode('match_',$key);
					$match_id = $key_array[1];
					
					$data = array(
						'user_id' => $userId,
						'venue_id' => $venue,
						'competition' => $comp_type,
						'round' => $round,
						'match_id' => $match_id,
						'selected_team' => $value,
						'status' => '1',
						'create_date' => date('Y-m-d H:i:s')
					);
					$inert_arr[]=$data;
					
				}
				
			}
			if( $sn > 0 )
			{
				$result = $this->competition->insert_new_tips( $inert_arr );
				if ( $result == true ) {
					$this->session->set_flashdata('success','Tip has been saved successfully.');
					redirect( base_url(). 'tips/competition/'.$comp_type.'/'.$round);
				}
				else
				{
					$this->session->set_flashdata('error', 'Tip could not be saved. Please try again.');
					redirect( base_url(). 'tips/competition/'.$comp_type.'/'.$round);
				}
			}
			else {
				$this->session->set_flashdata('error','Please tip on atleast one team.');
				redirect( base_url().'tips/competition/'.$comp_type.'/'.$round);
			}
		} 
		
		
		$data['sidebar'] = $this->load->view("front_sidebar",$data,true);	
		$data['page_body'] = $this->load->view("tips_competition",$data,true);	
		$this->load->view("templates_main",$data);	
	}
	
	public function change_venue() {
		$resp = $this->authorize->user_loggedIn();
		if($resp==false)
		{
			redirect(base_url().'home');
		}
		else
		{
			$data['login_status'] = 1;
		}
		
		$this->form_validation->set_rules('main_venue', 'Venue' , 'trim|required');
		if( $this->form_validation->run() == TRUE )
		{
			if( $this->member_model->update_main_venue() == true ) {
				$this->session->set_flashdata('success' ,'Venue has been updated successfully.');
				redirect( base_url().'tips/change_venue');
			} else {
				$this->session->set_flashdata('error','Venue could not be updated. Please try again.');
				redirect( base_url().'tips/change_venue');
			}
		}
		$userId = $this->session->userdata('userid');
		$userData = $this->member_model->getMemberDetailsById($userId);
		$venue = $userData['main_venue'];
		
		$venuelist = $this->venue_model->get_venue_list();
		if( $venuelist ) {
			$data['venue_list'] = $venuelist;
		} else {
			$data['venue_list'] = array();
		}
		$data['userData'] = $userData;
		$data['sidebar'] = $this->load->view("front_sidebar",$data,true);	
		$data['page_body'] = $this->load->view("tips_change_venue",$data,true);	
		$this->load->view("templates_main",$data);	
	}
}

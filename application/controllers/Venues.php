<?php
class Venues extends CI_Controller
{
	var $key = 'Ahk8IHLMfNtjpTNwtPrTH1gGD';
	var $secret = 'xeD3NTsksNOzSUfKjlnQ621m5G9974VL2uOuHLIz1OOEcv0Iti';
	//~ var $request_token = "https://twitter.com/oauth/request_token";
	//~ var $access_token = "https://api.twitter.com/oauth/access_token";
	//~ var $get_user_details = "https://api.twitter.com/1.1/account/verify_credentials.json";
	
	function __construct()
	{
		//session_start();session_destroy();die;
		parent::__construct();
		$this->config->set_item('enable_query_strings', TRUE);
		
		$this->load->model('venue_model');
		$this->load->model('Register_user');
		$this->load->library('authorize');
		
		$remoteAddr = $_SERVER['REMOTE_ADDR'];
		error_reporting(-1);
		if($_SERVER['REMOTE_ADDR']=='127.0.0.1' || $_SERVER['REMOTE_ADDR']=='::1' || strstr($_SERVER['REMOTE_ADDR'],'192.168.')==$_SERVER['REMOTE_ADDR'])
		{
			$remoteAddr = '122.173.135.80';
		}
		//$timeZone = $this->authorize->getUserTimeZone($remoteAddr);
		$timeZone = $this->session->userdata('timezone');
		if($timeZone=='')
		{
			$timeZone = $this->authorize->getUserTimeZone($remoteAddr);
			$this->session->set_userdata('timezone',$timeZone);
		}
		$this->session->set_userdata('timezone',$timeZone);
		date_default_timezone_set($timeZone);
	}
	
	public function change_password() {
		$data = array();
		$resp = $this->authorize->venue_loggedIn();
		if($resp==false)
		{
			redirect(base_url().'home');
		}
		else{
			$data['login_status'] = 1;
		}
		
		$this->form_validation->set_rules('current_password', 'Current password', 'trim|required');
		$this->form_validation->set_rules('new_password', 'New Password', 'trim|required');
		
		if ($this->form_validation->run() == TRUE) // form submitted
		{ 
		
			if($this->venue_model->update_venue_Password() == true )
			{
				$this->session->set_flashdata('success','Password has been updated successfully.');
				redirect( base_url(). 'venues/change_password');
			}
			else
			{
				$this->session->set_flashdata('error', 'Current password is not correct. Please try again.');
				redirect( base_url(). 'venues/change_password');
			}
		}
		
	
		$data['type_tip'] = '';
		$data['place_val'] = '';
		$venue_id = $this->session->userdata('venueId');
		$venuedata = $this->venue_model->get_logged_venue_details($venue_id);
		$data['venue_id'] = $venue_id;
		$data['venuedata'] = $venuedata;
		$data['sidebar'] = $this->load->view("front_sidebar",$data,true);	
		$data['page_body'] = $this->load->view("venue/change_password",$data,true);	
		$this->load->view("templates_main",$data);	
	}
	
	
	public function edit_profile(){ 
		$data = array();
		$resp = $this->authorize->venue_loggedIn();
		if($resp==false)
		{
			redirect(base_url().'home');
		}
		else
		{
			$data['login_status'] = 1;
		}
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('venue_name', 'Venue name', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		
		if ($this->form_validation->run() == TRUE) // form submitted
		{ 
		
			if($this->venue_model->update_venue_data() == true )
			{
				$this->session->set_flashdata('success','Venue details has been updated successfully.');
				redirect( base_url(). 'venues/edit_profile');
			}
			else
			{
				$this->session->set_flashdata('error', 'Venue details could not be updated. Please try again.');
				redirect( base_url(). 'venues/edit_profile');
			}
		}
		
		$data['type_tip'] = '';
		$data['place_val'] = '';
		$data['colortheme'] = "#2888C9";
		$venue_id = $this->session->userdata('venueId');
		$venuedata = $this->venue_model->get_logged_venue_details($venue_id);
		$data['venue_id'] = $venue_id;
		$data['venuedata'] = $venuedata;
		$data['sidebar'] = $this->load->view("front_sidebar",$data,true);	
		$data['page_body'] = $this->load->view("venue/edit_profile",$data,true);	
		$this->load->view("templates_main",$data);	
	}
	
	public function chk_email_exist() {
		$this->load->model('user');
		$u_email = $this->input->post('email');
		$user_id = $this->input->post('id');
		$result  = $this->user->chkemail_front($u_email,$user_id);
			
		if($result > 0){
			echo json_encode(FALSE);
		} else{
			echo json_encode(TRUE);
		}
	}
	
	function profile()
	{
		$data = array();
		$resp = $this->authorize->venue_loggedIn();
		$venue_id = $this->session->userdata('venueId');
		if($resp==false)
		{
			redirect(base_url().'home');
		}
		else
		{
			$data['login_status'] = 1;
		}
		
		$data['type_tip'] ='';
		$data['colortheme'] = "#2888C9";
		$data['place_val'] = '';
		$venuedata = $this->venue_model->get_logged_venue_details($venue_id);
		$data['venue_id'] = $venue_id;
		$data['venuedata'] = $venuedata;
		$data['sidebar'] = $this->load->view("front_sidebar",$data,true);	
		$data['page_body'] = $this->load->view("venue/profile",$data,true);	
		$this->load->view("templates_main",$data);	
	}
	
	
	public function leaderboard()
	{
		$data['title']='Leaderboard';
		$resp = $this->authorize->venue_loggedIn();
		$venue_id = $this->session->userdata('venueId');
		if($resp==false)
		{
			redirect(base_url().'home');
		}
		else
		{
			$data['login_status'] = 1;
			$data['place_val'] = '';
		
		}
		$data['type_tip'] ='';
		$data['colortheme'] = "#2888C9";
		
		
		$venuedata = $this->venue_model->get_logged_venue_details($venue_id);
		$data['venue_id'] = $venue_id;
		$data['venuedata'] = $venuedata;
		
		if( $this->uri->segment(3) != '' ) {
			$type = $this->uri->segment(3);
			$data['total'] = $this->Register_user->get_total_match_by_venue($venue_id,$type); 
			$result = $this->Register_user->get_patron_leaderboard($venue_id,$type);
			
			$users=array();
			if(count($result) && is_array($result))
			{
				$all_users=array_count_values(array_column($result, 'id'));			
				arsort($all_users);
				
				foreach($all_users as $user => $count)
				{
					$userDetails=$this->Register_user->get_user_by_id($user);
					$userDetails['result']=$count;
					$users[$user]=$userDetails;
					
				} 
			}
			
			
			$data['leaderboard_data'] = $users;
			
			
		}
		else
		{
			
		}
		
		$data['sidebar'] = $this->load->view("front_sidebar",$data,true);	
		$data['page_body'] = $this->load->view("view_venue_leaderboard",$data,true);	
		$this->load->view("templates_main",$data);	
		
	}
	
	
	public function crm()
	{
		$data['title']='Crm';
		$resp = $this->authorize->venue_loggedIn();
		$venue_id = $this->session->userdata('venueId');
		if($resp==false)
		{
			redirect(base_url().'home');
		}
		else
		{
			$data['login_status'] = 1;
			$data['place_val'] = '';
		
		}
		$data['type_tip'] ='';
		$data['colortheme'] = "#2888C9";
		
		
		$venuedata = $this->venue_model->get_logged_venue_details($venue_id);
		$data['venue_id'] = $venue_id;
		$data['venuedata'] = $venuedata;
		
		if( $this->uri->segment(3) != '' ) {
			$type = $this->uri->segment(3);
			$data['total'] = $this->Register_user->get_total_match_by_venue($venue_id,$type); 
			$result = $this->Register_user->get_patron_leaderboard($venue_id,$type);
			
			$users=array();
			if(count($result) && is_array($result))
			{
				$all_users=array_count_values(array_column($result, 'id'));			
				arsort($all_users);
				
				foreach($all_users as $user => $count)
				{
					$userDetails=$this->Register_user->get_user_by_id($user);
					$userDetails['result']=$count;
					$users[$user]=$userDetails;
					
				} 
			}
			
			
			$data['leaderboard_data'] = $users;
			
			
		}
		else
		{
			
		}
		
		$data['sidebar'] = $this->load->view("front_sidebar",$data,true);	
		$data['page_body'] = $this->load->view("view_venue_crm",$data,true);	
		$this->load->view("templates_main",$data);	
		
	}
	
	
	
	function view_user()
	{
	
		$data['title']='User Profile';
		$resp = $this->authorize->venue_loggedIn();
		$venue_id = $this->session->userdata('venueId');
		if($resp==false)
		{
			redirect(base_url().'home');
		}
		else
		{
			$data['login_status'] = 1;
			$data['place_val'] = '';
		
		}
		$data['type_tip'] ='';
		$data['colortheme'] = "#2888C9";
		
		
		$venuedata = $this->venue_model->get_logged_venue_details($venue_id);
		$data['venue_id'] = $venue_id;
		$data['venuedata'] = $venuedata;
	
		if( $this->uri->segment(3) != '' ) {
			
			$user_id = $this->uri->segment(3);
			$u_id=(int) base64_decode($user_id);
			
			if($u_id > 0)
			{
				$data['user_data'] = $this->Register_user->get_user_by_id($u_id);
				
			}
			else
			{
				redirect(base_url().'venues/profile');
			}
			
	
		}
		else
		{
			redirect(base_url().'venues/profile');
		}
		
		$data['sidebar'] = $this->load->view("front_sidebar",$data,true);	
		$data['page_body'] = $this->load->view("view_profile",$data,true);	
		$this->load->view("templates_main",$data);	
		
	
	}

}

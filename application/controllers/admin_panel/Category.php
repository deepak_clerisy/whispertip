<?php

class Category extends CI_Controller{

function __construct()
{
   parent::__construct();
   $this->lang->load('admin',$this->config->item('language'));
   $this->load->library('authorize');
   if( $this->authorize->is_admin_logged_in() == false )
			redirect(base_url().'admin_panel/login');
			
   $this->load->model('category_model');
   $this->load->helper('form');
}

function index()
{
	
	    $data['categories'] = $this->category_model->getAllCategories();
		$data['page_title'] = $this->lang->line('admin_listview_categories');
		$data['section_heading'] = $this->lang->line('admin_listview_categories');
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/manage_categories',$data, true);
		$this->load->view('admin_panel/template',$data);
}

function add_category()
	{
		$data=array();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('Categoryname', 'Category Name', 'required');
		
		if ($this->form_validation->run() == TRUE)
		{
			if($this->category_model->add_category() == true)
			{
				$this->session->set_flashdata('success', $this->lang->line('admin_operation_success'));
				redirect(base_url().'admin_panel/category/index');
			}
		}
			
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		
		$data['page_title'] = $this->lang->line('admin_Add_shop');
		$data['section_heading'] = $this->lang->line('admin_Add_shop');
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/add_category',$data, true);
		$this->load->view('admin_panel/template',$data);
	}
	
function edit_category()
	{
		$data=array();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('Categoryname', 'Category Name', 'required');
		
		$catId = $this->uri->segment(4,0);
		
		$catData = $this->category_model->getCategoryById($catId);
		if(!count($catData))
		{
			redirect(base_url.'admin_panel/category');
		}
		if ($this->form_validation->run() == TRUE)
		{
			if($this->category_model->edit_category() == true)
			{
				$this->session->set_flashdata('success', $this->lang->line('admin_operation_success'));
				redirect(base_url().'admin_panel/category/index');
			}
		}
			
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		
		$data['catid'] = $catId;
		$data['catData'] = $catData;
		$data['page_title'] = $this->lang->line('admin_edit_shop');
		$data['section_heading'] = $this->lang->line('admin_edit_shop');
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/edit_category',$data, true);
		$this->load->view('admin_panel/template',$data);
	}
	
function active_inactive()
{
	echo $nav_parent_id = $this->uri->segment(5,0);die;
	$EventtypeId = $this->uri->segment(4,0);
	echo $nav_parent_id.'###'.$EventtypeId;die;
	if($this->category_model->activeInactiveCategory($nav_parent_id,$EventtypeId)==true)
		{
			$this->session->set_flashdata('success', 'Requested operation finished successfully.');
			redirect(base_url().'admin_panel/navigation/manage_submenu/'.$EventtypeId.'/'.$nav_parent_id);
		}
		else
		{
			$this->session->set_flashdata('error','Operation unsuccessful. Please try again!');
			redirect( base_url(). 'admin_panel/navigation/manage_submenu/'.$EventtypeId.'/'.$nav_parent_id);
		}	
}

}
?>

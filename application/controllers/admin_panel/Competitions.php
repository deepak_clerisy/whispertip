<?php
class Competitions extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->lang->load('admin', $this->config->item('language'));
		$this->load->library(array('form_validation','authorize'));
		$this->load->Model('Competition');
		if( $this->authorize->is_admin_logged_in() == false )
			redirect(base_url().'admin_panel/login');
			
		$this->load->library('Xajax');

		$this->xajax->processRequests();
	}
	
	function index($sortfield='competition_id',$order='asc',$search = null, $page_num=1)
	{ 
		$data = array();
		$total_row = $this->Competition->count_all_records();
		$config['base_url'] = $this->config->item('admin_url').'/competitions/index/'.$sortfield.'/'.$order.'/'.'='.'/';
		$config['per_page'] = 10;
		$data['per_page'] = $config['per_page'];
		$page_number = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$data['pag'] = $this->uri->segment(4);
		if(empty($page_number)) $page_number = 1;        
		$offset = ($page_number-1) * $config['per_page']; 
		
		$keyword = $this->input->post('keyword');
		$sh = $this->uri->segment(6);
		if( isset( $keyword) ) {

			$data['search'] = $keyword;
			$search = $keyword;
			$total_row = $this->Competition->count_all_records( $keyword );
			$config['base_url'] = $this->config->item('admin_url').'/competitions/index/'.$sortfield.'/'.$order.'/'.$search.'/';
			$page_number = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0;
			if(empty($page_number)) $page_number = 1;        
			$offset = ($page_number-1) * $config['per_page']; 
			$data['pag'] = $this->uri->segment(7);
		}
		else if( $sh == 1  ) {
						
			$data['search'] = '';
			$search = '';
			$total_row = $this->Competition->count_all_records( $search );
			$config['base_url'] = $this->config->item('admin_url').'/competitions/index/'.$sortfield.'/'.$order.'/'.'='.'/';
			$page_number = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0; 
			if(empty($page_number)) $page_number = 1;        
			$offset = ($page_number-1) * $config['per_page']; 
			$data['pag'] = $this->uri->segment(7);
		}
		else if( !empty($sh) ) {
				if( $sh == '='){
					$data['search'] = '';
					$search = '';
					$config['base_url'] = $this->config->item('admin_url').'/competitions/index/'.$sortfield.'/'.$order.'/'.'='.'/';
				}
				else {
					$data['search'] = $sh;
					$search = $sh;
					$config['base_url'] = $this->config->item('admin_url').'/competitions/index/'.$sortfield.'/'.$order.'/'.$search.'/';
				}
			$total_row = $this->Competition->count_all_records( $search );
			
			$page_number = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0;
			if(empty($page_number)) $page_number = 1;        
			$offset = ($page_number-1) * $config['per_page']; 
			$data['pag'] = $this->uri->segment(7);
		}
		
		//$config['uri_segment'] = 4;
		$config['use_page_numbers'] = TRUE; 
		   
		$config["total_rows"] = $total_row;
		$this->pagination->initialize($config);
		//$data["links"] = $this->pagination->create_links();       
		$data['user_listing'] = $this->Competition->get_user_listing($config["per_page"], $offset, $sortfield, $order, $search );     
		
		$data['page_title'] = 'Competitions listing';
		$data['section_heading'] = $this->lang->line('admin_dashboard');
		$data['page_body'] = $this->load->view('admin_panel/competitions_listing',$data,true);
		
	
		
		$this->load->view('admin_panel/template',$data);
	}
	
	public function edit() {
		$user_id = $this->uri->segment(4);

		$this->form_validation->set_rules('competition', 'Competition', 'required');
		$this->form_validation->set_rules('rounds', 'Round', 'required');
        $this->form_validation->set_rules('mname', 'Match Name', 'required');
       
		if ($this->form_validation->run() == TRUE)
		{
			$result = $this->Competition->update_competition($user_id);
			if( $result == true ) { 
				$this->session->set_flashdata('success','Competition has been updated successfully.');
				redirect( $this->config->item('admin_url'). '/competitions/index');
			} else {
				$this->session->set_flashdata('error','Competition could not be updated.');
				redirect( $this->config->item('admin_url'). '/competitions/index');
			}
		}

		$data['user'] = $this->Competition->get_competition($user_id);
		$data['page_title'] = 'Edit Competition';
		$data['page_body'] = $this->load->view('admin_panel/add_competitions',$data,true);
		
		$this->load->view('admin_panel/template',$data);
	}
	
	public function add() {
		
		$venue_id = (int)$this->uri->segment(4);
		
		$data = array();
		
		$this->form_validation->set_rules('competition', 'Competition', 'required');
		$this->form_validation->set_rules('rounds', 'Round', 'required');
        $this->form_validation->set_rules('mname', 'Match Name', 'required');
        //~ $this->form_validation->set_rules('team1', 'Team 1', 'required');
        //~ $this->form_validation->set_rules('team2', 'Team 2', 'required');
       
		if ($this->form_validation->run() == TRUE) {
			$result = $this->Competition->save_competition();
			if( $result == true ) {
				$this->session->set_flashdata('success','The Competition has been saved successfully.');
				redirect( $this->config->item('admin_url'). '/competitions/index');
			} else {
				$this->session->set_flashdata('error','The Competition could not be saved. Please try again.');
				redirect( $this->config->item('admin_url'). '/competitions/index');
			}
		}

		$data['venue'] = $this->Competition->get_venue();	
		$data['venue_part'] = $this->Competition->get_venue_part($venue_id);	
		$data['page_title'] =  'Add Competition';
		$data['user'] = array();
		$data['page_body'] = $this->load->view('admin_panel/add_competitions',$data,true);
		
		$this->load->view('admin_panel/template',$data);
	
	}
	
	public function delete() {
		
		$data = array();
		$user_id = $this->input->post('competition_id');
		$result = $this->Competition->delete( $user_id );
			if( $result == true ) {
				echo 'deleted';
			} else {
				echo 'notdeleted';
			}
			
		//~ $data['user_listing'] = $this->User->get_user_listing();
		//~ 
		//~ $data['page_title'] = 'Users listing';
		//~ $data['section_heading'] = $this->lang->line('admin_dashboard');
		//~ $data['page_body'] = $this->load->view('admin_panel/users_listing',$data,true);
		//~ 
		//~ $this->load->view('admin_panel/template',$data);
	}
	
	public function search() {
		$keyword    =   $this->input->post('keyword');
		$data['search'] = $keyword;
        $data['user_listing']  = $this->Competition->search($keyword);

       if( count($data['user_listing']) > 0 ) {
		  
		   $data['page_title'] = 'Users listing';
			$data['section_heading'] = $this->lang->line('admin_dashboard');
			$data['page_body'] = $this->load->view('admin_panel/competitions_listing',$data,true);
		}
		$this->load->view('admin_panel/template',$data);
	}
	
	public function change_status() {
		$result = $this->Competition->change_status();
		if( $result == 'active' ) {
			echo 'active';
		} else if( $result == 'inactive' ) {
			echo 'inactive';
		} else if( $result == false ) {
			echo 'no';
		}
	}

	public function change_rounds() {
		$competition = $this->input->post('competition');
		if($competition == 'AFL'){
			$roundnum =  $this->config->item('AFL');
		}else if($competition == 'NRL'){
			$roundnum =  $this->config->item('NRL');
		}
		echo '<option value="">Select Round</option>';
		for($i=1;$i<=$roundnum; $i++){
			echo '<option value="'.$i.'">Round '.$i.'</option>';
		}
	}
	public function chk_username(){
		$uName = $this->input->post('username');
		$uid = $this->input->post('competition_id');
		if($uid > 0){
			$result  = $this->Competition->chkuname($uName,$uid);
		}else{ 
			$result  = $this->Competition->chkuname($uName);
		}	
		if($result > 0){
			echo json_encode(FALSE);
		} else{
			echo json_encode(TRUE);
		}
	}
	
	public function chk_email(){
		$email = $this->input->post('email');
		$uid = $this->input->post('competition_id');

		if($uid > 0){
			$result  = $this->Competition->chkemail($email,$uid);
		}else{ 
			$result  = $this->Competition->chkemail($email);
		}	
		if($result > 0){
			echo json_encode(FALSE);
		} else{
			echo json_encode(TRUE);
		}
	}
	
	
}
?>

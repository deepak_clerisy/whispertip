<?php
class Email extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->lang->load('admin', $this->config->item('language'));
		$this->load->library('authorize');
		
		if( $this->authorize->is_admin_logged_in() == false )
			redirect(base_url().'admin_panel/login');
			
		$this->load->library('Xajax');
		$this->load->model('emailtemplates_model');

		//$this->xajax->registerFunction(array('change_image',&$this,'change_image'));
		$this->xajax->processRequests();
	}
	

	function index()
	{
		$this->load->model('emailtemplates_model');
		$data = array();
		$txtSearch = $this->uri->segment(5, '');
		
		$fields = $this->emailtemplates_model->getemailtemplates($txtSearch);
		
		$data['txtSearch'] = $txtSearch;

		$this->load->library('pagination');
		
		$config['base_url'] = base_url().'admin_panel/email/index';
		$config['total_rows'] = count($fields);
		$config['per_page'] = $this->config->item('pagesize');
		$config['uri_segment'] = 4;
		$config['num_links'] = 2;
		if($txtSearch)
		$config['postfix_string'] = '/'.$txtSearch;
		
		$this->pagination->initialize($config); 
		
		$data['page_number'] = $this->uri->segment(4,0);
		
		$data['page_size'] = $this->config->item('pagesize');
	$data['emailtemplate'] = $this->emailtemplates_model->getemailtemplates($txtSearch, $data['page_number'], $data['page_size']);
		
		
		
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}

		$data['page_title'] = $this->lang->line('admin_emailtemplate');
		$data['section_heading'] = $this->lang->line('admin_emailtemplate');
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/home_emailtemplates',$data, true);
		$this->load->view('admin_panel/template',$data);
		
	}
	
	function edit_emailtemplate()
	{
		$this->load->model('emailtemplates_model');	
		$data = array();
		$email_template_id = $this->uri->segment(4, 0);
		if($this->emailtemplates_model->checkIdIsAvaliable('email_templates','email_template_id',$email_template_id) == false)
		{
			redirect( base_url(). 'admin_panel/email');
		}		
		$mode = $this->input->post("editemailtemptale");
		$this->load->library('form_validation');
		$this->form_validation->set_rules('subject', 'subject', 'required');
		$this->form_validation->set_rules('from', 'from', 'required|valid_email');
		
		if($email_template_id)
		{
			$data['emailtemplate'] = $this->emailtemplates_model->getemailtemplateDetailsById($email_template_id);
			$data['mode'] = $mode;
			$data['email_template_id'] = $email_template_id;
			$data['template_variables'] = $this->emailtemplates_model->getEmailTemplateVariables($email_template_id);
		}

		if ($this->form_validation->run() == TRUE)
		{
			if($mode=="editemailtemptale")
			{
			
				if( $this->emailtemplates_model->editEmailTemplate($email_template_id) == true )
				{
				
					$this->session->set_flashdata('success', $this->lang->line('admin_operation_success'));
					redirect( base_url(). 'admin_panel/email/index');
				}
				else
				{
					$this->session->set_flashdata('error', $this->lang->line('admin_operation_unsuccess'));
					redirect( base_url(). 'admin_panel/email/index');
				}		
			}
		}

		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		
		$data['page_title'] = $this->lang->line('admin_edit_emailtemplate');
		$data['section_heading'] = $this->lang->line('admin_edit_emailtemplate');
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/edit_emailtemplate',$data, true);
		$this->load->view('admin_panel/template',$data);
	
	}
	
	function manage_newsletter()
	{
		$data = array();
		$fields = $this->emailtemplates_model->getAllNewsletters();
		$data['newsletter_fields'] = $fields;
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}

		$data['page_title'] = $this->lang->line('admin_emailtemplate');
		$data['section_heading'] = $this->lang->line('admin_emailtemplate');
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/manage_newsletter',$data, true);
		$this->load->view('admin_panel/template',$data);
		
	}
	function add_newsletter()
	{
		$data=array();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('subject', 'subject', 'required');
		$this->form_validation->set_rules('from', 'from', 'required|valid_email');
		if($this->input->post('type')){
			if($this->emailtemplates_model->insertData() == true)
			{
				
				$this->session->set_flashdata('success', $this->lang->line('admin_operation_success'));
				redirect(base_url().'admin_panel/email/manage_newsletter');
				
			}
		}
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		$data['page_title'] = "Add Newsletter";
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/add_newsletter',$data, true);
		$this->load->view('admin_panel/template',$data);
	
	}
	function edit_newsletter()
	{
		$data = array();
		$newletter_id = $this->uri->segment(4, 0);
		if($this->emailtemplates_model->checkIdIsAvaliable('newsletters','newsletter_id',$newletter_id) == false)
		{
			redirect( base_url(). 'admin_panel/email/manage_newsletter');
		}			
		$mode = $this->input->post("editnewsletter");
		$this->load->library('form_validation');
		$this->form_validation->set_rules('subject', 'subject', 'required');
		$this->form_validation->set_rules('from', 'from', 'required|valid_email');
		
		if($newletter_id)
		{
			$data['newsletter'] = $this->emailtemplates_model->getNewsletterDetailsById($newletter_id);
			$data['mode'] = $mode;
			$data['newletter_id'] = $newletter_id;
		}

		if ($this->form_validation->run() == TRUE)
		{
			if($mode=="editnewsletter")
			{
				if( $this->emailtemplates_model->editnewsletter($newletter_id) == true )
				{
				
					$this->session->set_flashdata('success', $this->lang->line('admin_operation_success'));
					redirect( base_url(). 'admin_panel/email/manage_newsletter');
				}
				else
				{
					$this->session->set_flashdata('error', $this->lang->line('admin_operation_unsuccess'));
					redirect( base_url(). 'admin_panel/email/manage_newsletter');
				}		
			}
		}

		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		$data['page_title'] = "Edit Newsletter";
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/edit_newsletter',$data, true);
		$this->load->view('admin_panel/template',$data);
	
	}
	function delete_newsletter()
	{
		$newletter_id = $this->uri->segment(4, 0);
		
		if($newletter_id)
		{
			if($this->emailtemplates_model->deleteNewsletters($newletter_id)==true)
			{
				$this->session->set_flashdata('success', $this->lang->line('admin_operation_success'));
				redirect(base_url().'admin_panel/email/manage_newsletter');
			}
			else
			{
				$this->session->set_flashdata('error', $this->lang->line('admin_operation_unsuccess'));
					redirect(base_url().'admin_panel/email/manage_newsletter');
			}
		}
		else
		{
			redirect(base_url().'admin_panel/email/manage_newsletter');
		}

	}
}
?>

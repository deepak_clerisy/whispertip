<?php
class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->lang->load('admin', $this->config->item('language'));
		$this->load->library('authorize');
		
		if( $this->authorize->is_admin_logged_in() == false )
			redirect(base_url().'admin_panel/login');
			
		$this->load->library('Xajax');

		//$this->xajax->registerFunction(array('change_image',&$this,'change_image'));
		$this->xajax->processRequests();
	}
	
	function index()
	{
		
		$data = array();
		
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		
		$data['page_title'] = $this->lang->line('admin_home');
		$data['section_heading'] = $this->lang->line('admin_dashboard');
		
		//$this->load->view('admin_panel/header',$data);
		$this->load->view('admin_panel/dashboard_template',$data);
		//$this->load->view('admin_panel/footer',$data);

	}


	function change_password()
	{
		$this->load->model('user_authorization_model');
		$data = array();
		$data['mode'] = $this->uri->segment(3);		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('old_pwd', $this->lang->line('admin_old_password'), 'trim|required');
		$this->form_validation->set_rules('new_pwd', $this->lang->line('admin_new_password'), 'trim|required');
		$this->form_validation->set_rules('con_pwd', $this->lang->line('admin_confirm_password'), 'trim|required|matches[new_pwd]');
		
		if ($this->form_validation->run() == TRUE) // form submitted
		{
			if($data['mode'] = 'change_password')
			{

				if( $this->user_authorization_model->changeAdminPassword() == true )
				{
					$this->session->set_flashdata('success', $this->lang->line('admin_password_success'));
					redirect($this->config->item('admin_url'). '/home/change_password');
				}
				else
				{
					$this->session->set_flashdata('error', $this->lang->line('admin_password_unsuccess'));
					redirect($this->config->item('admin_url'). '/home/change_password');
				}
			}
		}
		

		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		
		$data['page_title'] = $this->lang->line('admin_change_password');
		$data['section_heading'] = $this->lang->line('admin_change_password');
		$data['page_body'] = $this->load->view('admin_panel/change_password',$data, true);
		$this->load->view('admin_panel/template',$data);
	}
}
?>

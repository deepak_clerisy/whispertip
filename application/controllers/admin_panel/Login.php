<?php

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();	 
		$this->lang->load('admin', $this->config->item('language'));
		$this->load->library('authorize');
	}
	
	function index()
	{
		$data = array();
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', $this->lang->line('admin_label_username'), 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', $this->lang->line('admin_label_password'), 'trim|required');				
		
		//if ($this->form_validation->run() == TRUE)
		if ( $this->input->post('username') != '' && $this->input->post('password') != '')
		{ 
			$user = $this->input->post('username');
			$pass = $this->input->post('password');
			
			if( $this->authorize->validate_admin_credentials($user, $pass) == true )
			{
				$this->session->set_flashdata('success', $this->lang->line('admin_login_success'));
				redirect($this->config->item('admin_url').'/home');
			}
			else
			{ 
				$this->session->set_flashdata('error', $this->lang->line('admin_message_unable_login'));
				redirect($this->config->item('admin_url').'/login');
			}
		}
	
		//~ if ($this->session->flashdata('success'))
		//~ {
			//~ $data['success'] = $this->session->flashdata('success');
		//~ }		
		//~ if ($this->session->flashdata('message'))
		//~ {
			//~ $data['message'] = $this->session->flashdata('message');
		//~ }
		//~ if ($this->session->flashdata('error'))
		//~ {
			//~ $data['error'] = $this->session->flashdata('error');
		//~ }
		
		$data['page_title'] = $this->lang->line('admin_login_page');
		$this->load->view('admin_panel/login_form',$data);
	}
	
	function logout()
	{
		if( $this->authorize->do_admin_logout() == true )
		{
			$this->session->set_flashdata('success', $this->lang->line('admin_message_logout'));
			redirect($this->config->item('admin_url').'/login');
		}
		else
		{
			redirect($this->config->item('admin_url').'/home');
		}
	}
	
}

?>

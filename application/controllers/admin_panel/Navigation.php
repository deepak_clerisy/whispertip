<?php

class Navigation extends CI_Controller{

function __construct()
{
   parent::__construct();
   $this->lang->load('admin',$this->config->item('language'));
   $this->load->library('authorize');
   if( $this->authorize->is_admin_logged_in() == false )
			redirect(base_url().'admin_panel/login');
			
   $this->load->model('navigation_model');
   $this->load->library('form_validation');
   $this->load->helper('form');
}

function index()
{
	    $data['eventType'] = $this->navigation_model->getAllEventType();
		$data['page_title'] = $this->lang->line('admin_listview_categories');
		$data['section_heading'] = $this->lang->line('admin_listview_categories');
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/manage_navigation',$data, true);
		$this->load->view('admin_panel/template',$data);
}

function manage_navigation()
{
		$uri_seg = $this->uri->segment(4,0);
		$nav_id = $this->uri->segment(5,0);
		$data['eventTypeId'] = $uri_seg;
		//echo '<pre>';print_r($_POST);die;
		
		if($nav_id==0)
		{
			$data['title']  = "Add";
			$data['type'] = '';	
			$nav = $this->input->post('radio_nav');
			$this->form_validation->set_rules('cat_name', 'cat_name', 'trim|required');
			$this->form_validation->set_rules('radio_nav', 'select Atleast one', 'required');
			$this->form_validation->set_rules('radio_nav', 'select Atleast one', 'required');
			if($nav=='compitition'){
				$this->form_validation->set_rules('competitions', 'select Atleast one competetion', 'required');
			}

			
			if ($this->form_validation->run() == TRUE) // form submitted
			{
				if($uri_seg)
				{
					if($this->navigation_model->navigationcomp($uri_seg) == true)
					{
						$this->session->set_flashdata('success', 'Requested operation finished successfully.');
						redirect( base_url(). 'admin_panel/navigation/manage_navigation/'.$uri_seg);
					}
					else
					{
						$this->session->set_flashdata('error', 'Operation unsuccessful. Please try again!');
						redirect( base_url(). 'admin_panel/navigation/manage_navigation/'.$uri_seg);
					}
				}
				
			}
		}
		if($nav_id!=0)
		{
		$data['title']  = "Edit";
		$nav = $this->input->post('radio_nav');
		$navigationByid = $this->navigation_model->getAllNavigationBYID($nav_id);
		if($navigationByid['type']=='')
			{
				$data['type'] = 'competition';
			}else{
			$data['type'] = 'normal';
		}
		$data['navigationByid'] = $navigationByid;
		$this->form_validation->set_rules('cat_name', 'cat_name', 'trim|required');
		$this->form_validation->set_rules('radio_nav', 'select Atleast one', 'required');
		$this->form_validation->set_rules('radio_nav', 'select Atleast one', 'required');
		if($nav=='compitition'){
			$this->form_validation->set_rules('competitions', 'select Atleast one competetion', 'required');
		}
		if ($this->form_validation->run() == TRUE) // form submitted
		{
			
			if($nav_id)
			{
				if($this->navigation_model->EditNavComp($nav_id,$uri_seg) == true)
				{
					$this->session->set_flashdata('success', 'Requested operation finished successfully.');
					redirect( base_url(). 'admin_panel/navigation/manage_navigation/'.$uri_seg);
				}
				else
				{
					$this->session->set_flashdata('error', 'Operation unsuccessful. Please try again!');
					redirect( base_url(). 'admin_panel/navigation/manage_navigation/'.$uri_seg);
				}
			}
			
		}
			
		} 
		$data['EventName'] = $this->navigation_model->getAllEventTypeById($uri_seg);
		$data['navigation'] = $this->navigation_model->getAllNavigation($uri_seg);
		$data['Competition'] = $this->navigation_model->getAllCompetitionbyEventType($uri_seg);
	 	$data['page_title'] = $this->lang->line('admin_listview_categories');
		$data['section_heading'] = $this->lang->line('admin_listview_categories');
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/add_navigation',$data, true);
		$this->load->view('admin_panel/template',$data);
}

function edit_parent_navigation()
{
		$uri_seg = $this->uri->segment(4,0);
		$this->form_validation->set_rules('cat_name', 'cat_name', 'trim|required');
		$this->form_validation->set_rules('competitions', 'competitions', 'required');
		if ($this->form_validation->run() == TRUE) // form submitted
		{
			if($uri_seg)
			{
				if($this->navigation_model->Editnavigationcomp($uri_seg) == true)
				{
					$this->session->set_flashdata('success', 'Requested operation finished successfully.');
					redirect( base_url(). 'admin_panel/navigation/manage_navigation/'.$uri_seg);
				}
				else
				{
					$this->session->set_flashdata('error', 'Operation unsuccessful. Please try again!');
					redirect( base_url(). 'admin_panel/navigation/manage_navigation/'.$uri_seg);
				}
			}
			
		} 
		$data['eventid'] = $uri_seg;
		$data['navigationByid'] = $this->navigation_model->getAllNavigationBYID($uri_seg);
		$data['Competition'] = $this->navigation_model->getAllCompetitionbyEventType($uri_seg);
	 	$data['page_title'] = $this->lang->line('admin_listview_categories');
		$data['section_heading'] = $this->lang->line('admin_listview_categories');
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/edit_parent_navigation',$data, true);
		$this->load->view('admin_panel/template',$data);
}
function manage_submenu()
{
		$uri_seg = $this->uri->segment(4,0);
		$data['Eventtype'] = $uri_seg;
		$nav_id = $this->uri->segment(5,0);
		//echo $nav_id;die;
		$nav_id_sub = $this->uri->segment(6,0);
		$data['nav_id'] = $nav_id ;
		//echo $uri_seg;die;
		if($nav_id_sub==0)
		{
		$data['title']  = "Add";
		$this->form_validation->set_rules('cat_name', 'cat_name', 'trim|required');
		$this->form_validation->set_rules('competitions', 'select Atleast one competetion', 'required');
		if ($this->form_validation->run() == TRUE) // form submitted
		{
			if($uri_seg)
			{
				if($this->navigation_model->Subnavigationcomp($uri_seg,$nav_id) == true)
				{
					$this->session->set_flashdata('success', 'Requested operation finished successfully.');
					redirect( base_url(). 'admin_panel/navigation/manage_submenu/'.$uri_seg.'/'.$nav_id);
				}
				else
				{
					$this->session->set_flashdata('error', 'Operation unsuccessful. Please try again!');
					redirect( base_url(). 'admin_panel/navigation/manage_navigation/'.$uri_seg);
				}
			}
			
		} 
	}
		if($nav_id_sub!=0)
		{
			$data['title']  = "Edit";
		$data['SubnavigationByid'] = $this->navigation_model->getAllSubNavigationBYID($nav_id_sub);
		$this->form_validation->set_rules('cat_name', 'cat_name', 'trim|required');
		$this->form_validation->set_rules('competitions', 'select Atleast one competetion', 'required');
		if ($this->form_validation->run() == TRUE) // form submitted
		{
			if($uri_seg)
			{
				if($this->navigation_model->EditSubnavigationcomp($uri_seg,$nav_id,$nav_id_sub) == true)
				{
					$this->session->set_flashdata('success', 'Requested operation finished successfully.');
					redirect( base_url(). 'admin_panel/navigation/manage_submenu/'.$uri_seg.'/'.$nav_id);
				}
				else
				{
					$this->session->set_flashdata('error', 'Operation unsuccessful. Please try again!');
					redirect( base_url(). 'admin_panel/navigation/manage_submenu/'.$uri_seg.'/'.$nav_id);
				}
			}
			
		} 
			
		} 
		$data['submenu'] = $this->navigation_model->getAllSubmenuById($nav_id,$uri_seg);
		$data['EventName'] = $this->navigation_model->getNavTypeById($nav_id);
		$data['navigation'] = $this->navigation_model->getAllNavigation($uri_seg);
		$data['Competition'] = $this->navigation_model->getAllCompetitionbyEventType($uri_seg);
	 	$data['page_title'] = $this->lang->line('admin_listview_categories');
		$data['section_heading'] = $this->lang->line('admin_listview_categories');
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/add_submenus',$data, true);
		$this->load->view('admin_panel/template',$data);
}

function active_inactive()
{
	$status = $this->uri->segment(7,0);
	$nav_parent_id = $this->uri->segment(5,0);
	$nav_id = $this->uri->segment(6,0);
	$EventtypeId = $this->uri->segment(4,0);
	//echo $status.'##'.$nav_parent_id.'##'.$nav_id.'###'.$EventtypeId;die;
	if($this->navigation_model->activeInactiveCategory($nav_id,$status,$EventtypeId)==true)
		{
			$this->session->set_flashdata('success', 'Requested operation finished successfully.');
			redirect(base_url().'admin_panel/navigation/manage_submenu/'.$EventtypeId.'/'.$nav_parent_id);
		}
		else
		{
			$this->session->set_flashdata('error','Operation unsuccessful. Please try again!');
			redirect( base_url(). 'admin_panel/navigation/manage_submenu/'.$EventtypeId.'/'.$nav_parent_id);
		}	
}
function active_inactive_navigation()
{
	$eventTypeId = $this->uri->segment(4,0);
	$nav_id = $this->uri->segment(5,0);
	$status = $this->uri->segment(6,0);
	if($this->navigation_model->activeInactiveNavigation($nav_id,$status)==true)
		{
			$this->session->set_flashdata('success', 'Requested operation finished successfully.');
			redirect(base_url().'admin_panel/navigation/manage_navigation/'.$eventTypeId);
		}
		else
		{
			$this->session->set_flashdata('error','Operation unsuccessful. Please try again!');
			redirect( base_url(). 'admin_panel/navigation/manage_navigation/'.$eventTypeId);
		}	
}
function active_multi()
{
	//echo '<pre>';print_r($_POST);die;
	$eventTypeId = $this->input->post('eventTypeId');
	if($this->navigation_model->activeInactiveCompMulti()==true)
		{
			$this->session->set_flashdata('success', 'Requested operation finished successfully.');
			redirect(base_url().'admin_panel/navigation/manage_navigation/'.$eventTypeId);
		}
		else
		{
			$this->session->set_flashdata('error','Operation unsuccessful. Please try again!');
			redirect( base_url(). 'admin_panel/navigation/manage_navigation/'.$eventTypeId);
		}	
	
}
}
?>

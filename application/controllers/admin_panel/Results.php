<?php
class Results extends CI_Controller {

	function __construct()
	{
		  parent::__construct();
		   $this->lang->load('admin',$this->config->item('language'));
		   $this->load->library('authorize');
		   if( $this->authorize->is_admin_logged_in() == false )
					redirect(base_url().'admin_panel/login');
					
		   $this->load->model('category_model');
		   $this->load->model('result_model');
		   $this->load->helper('form');
	}
	
	function index()
	{
		$data['category_sports'] = $this->result_model->getAllSport();
		$data['page_title'] = 'Manage Results';
		$data['section_heading'] = 'Manage Results';
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/manage_result',$data, true);
		$this->load->view('admin_panel/template',$data);
	}
	function get_competetions()
	{
		$event_type_id = $this->uri->segment('4','0');
		if(!$event_type_id){
			redirect(base_url().'admin_panel/results');
			}
		$data['category_comp'] = $this->result_model->getAllCompetetions($event_type_id);
		//echo '<pre>';print_r($data['category_comp']);die;
		$data['page_title'] = 'Manage Results';
		$data['section_heading'] = 'Manage Results';
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/manage_competetions',$data, true);
		$this->load->view('admin_panel/template',$data);
	}
	function get_events()
	{
		$comp_id = $this->uri->segment('4','0');
		if(!$comp_id){
			redirect(base_url().'admin_panel/results');
			}
		$data['category_events'] = $this->result_model->getAllEvents($comp_id);
		//echo '<pre>';print_r($data['category_comp']);die;
		$data['page_title'] = 'Manage Results';
		$data['section_heading'] = 'Manage Results';
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/manage_events',$data, true);
		$this->load->view('admin_panel/template',$data);
	}
	function get_markets()
	{
		$event_id = $this->uri->segment('4','0');
		if(!$event_id){
			redirect(base_url().'admin_panel/results');
			}
		$data['category_markets'] = $this->result_model->getAllMarkets($event_id);
		//echo '<pre>';print_r($data['category_markets']);die;
		$data['page_title'] = 'Manage Results';
		$data['section_heading'] = 'Manage Results';
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/manage_markets',$data, true);
		$this->load->view('admin_panel/template',$data);
	}
	function compare_results()
	{
		$data = array();
		$this->load->library('form_validation');
		$market_id = $this->uri->segment('4','0');
		$data['market_id'] = $market_id;
		if(!$market_id){
			redirect(base_url().'admin_panel/results');
			}
		$data['category_results'] = $this->result_model->getRunner($market_id);
		$market_result = $this->result_model->getMarketResults($market_id);
		if($market_result['status']=='closed'){
			$sport_result = $this->result_model->getRunnerById($market_result['market_id'],$market_result['selection_id']);
			if($sport_result){
					$data['sport_result'] = $sport_result;	
				}else{
					$data['sport_result']='no_record';	
				}
			
			}else{
				$data['sport_result']='open';
				}
			$this->form_validation->set_rules('radio_runner', 'Runner', 'required');	
			if ($this->form_validation->run() == TRUE)
			{
				if($this->result_model->UpdateResult($market_id)==true){
					$market_result = $this->result_model->getMarketResults($market_id);
			if($market_result['status']=='closed'){
				$sport_result = $this->result_model->getRunnerById($market_result['market_id'],$market_result['selection_id']);
				if($sport_result){
						$data['sport_result'] = $sport_result;	
					}else{
						$data['sport_result']='no_record';	
					}
				
			}else{
				$data['sport_result']='open';
				}
					}
			}		
		//	echo '<pre>';print_r($data['sport_result']);die;
		$data['page_title'] = 'Manage Results';
		$data['section_heading'] = 'Manage Results';
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/compare_result',$data, true);
		$this->load->view('admin_panel/template',$data);
	}
}
?>

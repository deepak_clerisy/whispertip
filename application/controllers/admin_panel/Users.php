<?php
class Users extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->lang->load('admin', $this->config->item('language'));
		$this->load->library(array('form_validation','authorize'));
		$this->load->Model('User');
		if( $this->authorize->is_admin_logged_in() == false )
			redirect(base_url().'admin_panel/login');
			
		$this->load->library('Xajax');

		$this->xajax->processRequests();
	}
	
	function index($sortfield='id',$order='asc',$search = null, $page_num=1)
	{ 
		$data = array();
		$total_row = $this->User->count_all_records();
		$config['base_url'] = $this->config->item('admin_url').'/users/index/'.$sortfield.'/'.$order.'/'.'='.'/';
		$config['per_page'] = 10;
		$data['per_page'] = $config['per_page'];
		$page_number = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$data['pag'] = $this->uri->segment(4);
		if(empty($page_number)) $page_number = 1;        
		$offset = ($page_number-1) * $config['per_page']; 
		
		$keyword = $this->input->post('keyword');
		$sh = $this->uri->segment(6);
		if( isset( $keyword) ) {
		
			$data['search'] = $keyword;
			$search = $keyword;
			$total_row = $this->User->count_all_records( $keyword );
			$config['base_url'] = $this->config->item('admin_url').'/users/index/'.$sortfield.'/'.$order.'/'.$search.'/';
			$page_number = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0;
			if(empty($page_number)) $page_number = 1;        
			$offset = ($page_number-1) * $config['per_page']; 
			$data['pag'] = $this->uri->segment(7);
		}
		else if( $sh == 1  ) {
						
			$data['search'] = '';
			$search = '';
			$total_row = $this->User->count_all_records( $search );
			$config['base_url'] = $this->config->item('admin_url').'/users/index/'.$sortfield.'/'.$order.'/'.'='.'/';
			$page_number = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0; 
			if(empty($page_number)) $page_number = 1;        
			$offset = ($page_number-1) * $config['per_page']; 
			$data['pag'] = $this->uri->segment(7);
		}
		else if( !empty($sh) ) {
				if( $sh == '='){
					$data['search'] = '';
					$search = '';
					$config['base_url'] = $this->config->item('admin_url').'/users/index/'.$sortfield.'/'.$order.'/'.'='.'/';
				}
				else {
					$data['search'] = $sh;
					$search = $sh;
					$config['base_url'] = $this->config->item('admin_url').'/users/index/'.$sortfield.'/'.$order.'/'.$search.'/';
				}
			$total_row = $this->User->count_all_records( $search );
			
			$page_number = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0;
			if(empty($page_number)) $page_number = 1;        
			$offset = ($page_number-1) * $config['per_page']; 
			$data['pag'] = $this->uri->segment(7);
		}
		
		 
		
        /*$config['full_tag_open'] = '<div class="paging">
            <ul class="tsc_pagination tsc_paginationA tsc_paginationA01 ajax">';
		$config['full_tag_close'] = '</ul></div>';*/
       
		
		//$config['uri_segment'] = 4;
		$config['use_page_numbers'] = TRUE; 
		   
		$config["total_rows"] = $total_row;
		$this->pagination->initialize($config);
		//$data["links"] = $this->pagination->create_links();       
		$data['user_listing'] = $this->User->get_user_listing($config["per_page"], $offset, $sortfield, $order, $search );     
		
		$data['page_title'] = 'View Patrons';
		$data['section_heading'] = $this->lang->line('admin_dashboard');
		$data['page_body'] = $this->load->view('admin_panel/users_listing',$data,true);
		
	
		
		$this->load->view('admin_panel/template',$data);
	}
	
	public function edit() {
		$user_id = $this->uri->segment(4);
		
		$this->form_validation->set_rules('first_name', 'First name', 'required');
		$this->form_validation->set_rules('last_name', 'Last name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('username', 'Username', 'required|max_length[10]|alpha');
       
		if ($this->form_validation->run() == TRUE)
		{
			
			$result = $this->User->update_user( $user_id );
			if( $result == true ) { 
				$this->session->set_flashdata('success','User has been updated successfully.');
				redirect( $this->config->item('admin_url'). '/users/index');
			} else {
				$this->session->set_flashdata('error','User could not be updated.');
				redirect( $this->config->item('admin_url'). '/users/index');
			}
		}
		
		$data['user'] = $this->User->get_user( $user_id );
		$data['page_title'] = 'Edit Patron';
		$data['page_body'] = $this->load->view('admin_panel/add_user',$data,true);
		
		$this->load->view('admin_panel/template',$data);
	}
	
	public function add() {
		$data = array();
		
		$this->form_validation->set_rules('first_name', 'First name', 'required');
		$this->form_validation->set_rules('last_name', 'Last name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[15]');
        $this->form_validation->set_rules('username', 'Username', 'required');
       
		if ($this->form_validation->run() == TRUE)
		{
			
			$result = $this->User->save_user();
			if( $result == true ) {
				$this->session->set_flashdata('success','The user has been saved successfully.');
				redirect( $this->config->item('admin_url'). '/users/index');
			} else {
				$this->session->set_flashdata('error','The user could not be saved. Please try again.');
				redirect( $this->config->item('admin_url'). '/users/index');
			}
		}

		$data['page_title'] =  'Add Patron';
		$data['user'] = array();
		$data['page_body'] = $this->load->view('admin_panel/add_user',$data,true);
		
		$this->load->view('admin_panel/template',$data);
	
	}
	
	public function delete() {
		
		$data = array();
		$user_id = $this->input->post('id');
	
		$result = $this->User->delete( $user_id );
			if( $result == true ) {
				echo 'deleted';
			} else {
				echo 'notdeleted';
			}
			
		//~ $data['user_listing'] = $this->User->get_user_listing();
		//~ 
		//~ $data['page_title'] = 'Users listing';
		//~ $data['section_heading'] = $this->lang->line('admin_dashboard');
		//~ $data['page_body'] = $this->load->view('admin_panel/users_listing',$data,true);
		//~ 
		//~ $this->load->view('admin_panel/template',$data);
	}
	
	public function search() {
		$keyword    =   $this->input->post('keyword');
		$data['search'] = $keyword;
        $data['user_listing']  = $this->User->search($keyword);

       if( count($data['user_listing']) > 0 ) {
		  
		   $data['page_title'] = 'Users listing';
			$data['section_heading'] = $this->lang->line('admin_dashboard');
			$data['page_body'] = $this->load->view('admin_panel/users_listing',$data,true);
		}
		$this->load->view('admin_panel/template',$data);
	}
	
	public function change_status() {
		$result = $this->User->change_status();
		if( $result == 'active' ) {
			echo 'active';
		} else if( $result == 'inactive' ) {
			echo 'inactive';
		} else if( $result == false ) {
			echo 'no';
		}
	}
	public function chk_username(){
		$uName = $this->input->post('username');
		$uid = $this->input->post('id');
		if($uid > 0){
			$result  = $this->User->chkuname($uName,$uid);
		}else{ 
			$result  = $this->User->chkuname($uName);
		}	
		if($result > 0){
			echo json_encode(FALSE);
		} else{
			echo json_encode(TRUE);
		}
	}
	
	public function chk_email(){
		$email = $this->input->post('email');
		$uid = $this->input->post('id');
		if($uid > 0){
			$result  = $this->User->chkemail($email,$uid);
		}else{ 
			$result  = $this->User->chkemail($email);
		}	
		if($result > 0){
			echo json_encode(FALSE);
		} else{
			echo json_encode(TRUE);
		}
	}
	
	
}
?>

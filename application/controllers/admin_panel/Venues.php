<?php
class Venues extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->lang->load('admin', $this->config->item('language'));
		$this->load->library(array('form_validation','authorize'));
		$this->load->Model('Venue');
		if( $this->authorize->is_admin_logged_in() == false )
			redirect(base_url().'admin_panel/login');
			
		$this->load->library('Xajax');

		$this->xajax->processRequests();
	}
	
	function index($sortfield='venue_id',$order='asc',$search = null, $page_num=1)
	{ 
		$data = array();
		$total_row = $this->Venue->count_all_records();
		$config['base_url'] = $this->config->item('admin_url').'/venues/index/'.$sortfield.'/'.$order.'/'.'='.'/';
		$config['per_page'] = 10;
		$data['per_page'] = $config['per_page'];
		$page_number = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$data['pag'] = $this->uri->segment(4);
		if(empty($page_number)) $page_number = 1;        
		$offset = ($page_number-1) * $config['per_page']; 
		
		$keyword = $this->input->post('keyword');
		$sh = $this->uri->segment(6);
		if( isset( $keyword) ) {
		
			$data['search'] = $keyword;
			$search = $keyword;
			$total_row = $this->Venue->count_all_records( $keyword );
			$config['base_url'] = $this->config->item('admin_url').'/venues/index/'.$sortfield.'/'.$order.'/'.$search.'/';
			$page_number = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0;
			if(empty($page_number)) $page_number = 1;        
			$offset = ($page_number-1) * $config['per_page']; 
			$data['pag'] = $this->uri->segment(7);
		}
		else if( $sh == 1  ) {
						
			$data['search'] = '';
			$search = '';
			$total_row = $this->Venue->count_all_records( $search );
			$config['base_url'] = $this->config->item('admin_url').'/venues/index/'.$sortfield.'/'.$order.'/'.'='.'/';
			$page_number = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0; 
			if(empty($page_number)) $page_number = 1;        
			$offset = ($page_number-1) * $config['per_page']; 
			$data['pag'] = $this->uri->segment(7);
		}
		else if( !empty($sh) ) {
				if( $sh == '='){
					$data['search'] = '';
					$search = '';
					$config['base_url'] = $this->config->item('admin_url').'/venues/index/'.$sortfield.'/'.$order.'/'.'='.'/';
				}
				else {
					$data['search'] = $sh;
					$search = $sh;
					$config['base_url'] = $this->config->item('admin_url').'/venues/index/'.$sortfield.'/'.$order.'/'.$search.'/';
				}
			$total_row = $this->Venue->count_all_records( $search );
			
			$page_number = ($this->uri->segment(7)) ? $this->uri->segment(7) : 0;
			if(empty($page_number)) $page_number = 1;        
			$offset = ($page_number-1) * $config['per_page']; 
			$data['pag'] = $this->uri->segment(7);
		}
		
		$config['use_page_numbers'] = TRUE; 
		   
		$config["total_rows"] = $total_row;
		$this->pagination->initialize($config);
 
		$data['user_listing'] = $this->Venue->get_venue_listing($config["per_page"], $offset, $sortfield, $order, $search );     
		
		$data['page_title'] = 'View Venues';
		$data['section_heading'] = $this->lang->line('admin_dashboard');
		$data['page_body'] = $this->load->view('admin_panel/venues_listing',$data,true);
		
	
		
		$this->load->view('admin_panel/template',$data);
	}
	
	public function edit() {
		$user_id = $this->uri->segment(4);

		// $this->form_validation->set_rules('competition', 'Competition', 'required');
		$this->form_validation->set_rules('vname', 'Venue name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('address', 'Address', 'required');
       
		if ($this->form_validation->run() == TRUE)
		{
			$result = $this->Venue->update_venue($user_id);
			if( $result == true ) { 
				$this->session->set_flashdata('success','Venue has been updated successfully.');
				redirect( $this->config->item('admin_url'). '/venues/index');
			} else {
				$this->session->set_flashdata('error','Competition could not be updated.');
				redirect( $this->config->item('admin_url'). '/venues/index');
			}
		}

		$data['user'] = $this->Venue->get_venue($user_id);
		$data['page_title'] = 'Edit Venue';
		$data['page_body'] = $this->load->view('admin_panel/add_venues',$data,true);
		
		$this->load->view('admin_panel/template',$data);
	}
	
	public function add() {
	
		$data = array();
		// $this->form_validation->set_rules('competition', 'Competition', 'required');
		$this->form_validation->set_rules('vname', 'Venue name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[15]');
        $this->form_validation->set_rules('address', 'Address', 'required');
        
		if ($this->form_validation->run() == TRUE) {
			//	echo "ff";
			$result = $this->Venue->save_venue();
			if( $result == true ) {
				$this->session->set_flashdata('success','Venue has been added successfully.');
				redirect( $this->config->item('admin_url'). '/venues/index');
			} else {
				$this->session->set_flashdata('error','venue could not be saved. Please try again.');
				redirect( $this->config->item('admin_url'). '/venues/index');
			}
		}

		$data['page_title'] =  'Add Venue';
		$data['user'] = array();
		$data['page_body'] = $this->load->view('admin_panel/add_venues',$data,true);
		
		$this->load->view('admin_panel/template',$data);
	
	}
	
	public function delete() {
		
		$data = array();
		$user_id = $this->input->post('venue_id');
	
		$result = $this->Venue->delete( $user_id );
			if( $result == true ) {
				echo 'deleted';
			} else {
				echo 'notdeleted';
			}
			
		//~ $data['user_listing'] = $this->User->get_user_listing();
		//~ 
		//~ $data['page_title'] = 'Users listing';
		//~ $data['section_heading'] = $this->lang->line('admin_dashboard');
		//~ $data['page_body'] = $this->load->view('admin_panel/users_listing',$data,true);
		//~ 
		//~ $this->load->view('admin_panel/template',$data);
	}
	
	public function search() {
		$keyword    =   $this->input->post('keyword');
		$data['search'] = $keyword;
        $data['user_listing']  = $this->Venue->search($keyword);

       if( count($data['user_listing']) > 0 ) {
		  
		   $data['page_title'] = 'Users listing';
			$data['section_heading'] = $this->lang->line('admin_dashboard');
			$data['page_body'] = $this->load->view('admin_panel/competitions_listing',$data,true);
		}
		$this->load->view('admin_panel/template',$data);
	}
	
	public function change_status() {
		$result = $this->Venue->change_status();
		if( $result == 'active' ) {
			echo 'active';
		} else if( $result == 'inactive' ) {
			echo 'inactive';
		} else if( $result == false ) {
			echo 'no';
		}
	}
	public function chk_vname(){
		$vName = $this->input->post('vname');
		$uid = $this->input->post('venue_id');

		if($uid > 0){
			$result  = $this->Venue->chkvname($vName,$uid);
		}else{ 
			$result  = $this->Venue->chkvname($vName);
		}	
		
		if($result > 0){
			echo json_encode(FALSE);
		} else{
			echo json_encode(TRUE);
		}
	}
	
	public function chk_email(){
		$email = $this->input->post('email');
		$uid = $this->input->post('venue_id');

		if($uid > 0){
			$result  = $this->Venue->chkemail($email,$uid);
		}else{ 
			$result  = $this->Venue->chkemail($email);
		}	
		if($result > 0){
			echo json_encode(FALSE);
		} else{
			echo json_encode(TRUE);
		}
	}
	
	
}
?>

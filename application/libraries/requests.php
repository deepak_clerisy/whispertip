<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Requests {

	function Requests()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('user_authorization_model');		
		$this->CI->load->model('betfair_model');		
	}
	
function certlogin($appKey,$params)
{
	global $ssl_crt;
	global $ssl_key;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://identitysso-api.betfair.com/api/certlogin");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSLCERT, $ssl_crt);
    curl_setopt($ch, CURLOPT_SSLKEY, $ssl_key);

	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey
    ));

    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $response = json_decode(curl_exec($ch));
   
    curl_close($ch);

    if ($response->loginStatus == "SUCCESS") {
        return $response;
    } else {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    }
}
function debug($debugString)
{
    global $DEBUG;
    if ($DEBUG)
        echo $debugString . "\n\n";
}

function sportsApingRequest($appKey, $sessionToken, $operation, $params,$market_country)
{
    $ch = curl_init();
    if($market_country =='aus'){
    curl_setopt($ch, CURLOPT_URL, "https://api-au.betfair.com/exchange/betting/json-rpc/v1");
}
else{
	curl_setopt($ch, CURLOPT_URL, "https://api.betfair.com/exchange/betting/json-rpc/v1");
	}
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey,
        'X-Authentication: ' . $sessionToken,
        'Accept: application/json',
        'Content-Type: application/json'
    ));

    $postData =
        '[{ "jsonrpc": "2.0", "method": "SportsAPING/v1.0/' . $operation . '", "params" :' . $params . ', "id": 1}]';

    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

    $this->debug('Post Data: ' . $postData);
    $response = json_decode(curl_exec($ch));
    $this->debug('Response: ' . json_encode($response));

    curl_close($ch);

    if (isset($response[0]->error)) {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    } else {
        return $response;
    }

}
function getNextRacingMarket($appKey, $sessionToken, $horseRacingEventTypeId,$markType,$market_country)
{

    $params = '{"filter":{"eventIds":["' . $horseRacingEventTypeId . '"],
              "marketTypeCodes":["'.$markType.'"]},
              "sort":"FIRST_TO_START",
              "maxResults":"1",
              "marketProjection":["RUNNER_DESCRIPTION"]}';

    $jsonResponse = $this->sportsApingRequest($appKey, $sessionToken, 'listMarketCatalogue', $params,$market_country);

    return $jsonResponse[0]->result[0];
}
function getMarketBook($appKey, $sessionToken, $marketId)
{
    $params = '{"marketIds":["' . $marketId . '"], "priceProjection":{"priceData":["EX_BEST_OFFERS"]}}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketBook', $params);

    return $jsonResponse[0]->result[0];
}
	
	function get_team_name($sel_id,$event_id,$type,$market_id)
	{
	$parts  = explode('.', (string)$market_id);	
	if($parts[0]==2)
	{
		$market_country = 'aus';
	}
	else
	{
		$market_country = 'inter';
	}
	echo $market_id.'##'.$market_country.'###'.$event_id;
	echo "$$$$";
	$appKey = '4ae3bb5f6f042268359bba5d24dd3c2ecb1b2f37';
	$params = 'username=nlarkins&password=budda2468';
	$resp = $this->certlogin($appKey,$params);
	$sessionToken = $resp->sessionToken;
	$appKey1 = '97Wtq7sjReOr0uWk';
    $marketType1 =  $this->getNextRacingMarket($appKey1, $sessionToken, $event_id,$type,$market_country);
   if($marketType1) {
			foreach($marketType1->runners as $resp)
			{
				$team_id = $resp->selectionId;
				$team_name_api = $resp->runnerName; 
				if($sel_id==$team_id){
					$team_name = $team_name_api;
					}
			}
			return $team_name;
		}
		
	}
	
}
?>

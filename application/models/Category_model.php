<?php

class Category_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

function getAllCategories()
{

   $this->db->select('*');
	$this->db->where('active',1);
   $query = $this->db->get('categories');
   if($query->num_rows() > 0)
   {
	  $userdata = $query->result_array() ;   
   }
   else
   {
	  $userdata = array() ;   
   }
   return $userdata ;
}

function getAllCategoriesBYId($event_type_id)
{
	$userdata = array();
   $this->db->select('*');
   $this->db->where('event_type_id',$event_type_id);
    $query = $this->db->get('categories');
   if($query->num_rows() > 0)
   {
	  $userdata = $query->row_array() ;   
   }
   return $userdata ;
}

function getCategoryById($catId)
{

   $this->db->select('*');
   $this->db->where('category_id',$catId);
   $query = $this->db->get('categories');
   if($query->num_rows() > 0)
   {
	  $userdata = $query->row_array() ;   
   }
   else
   {
	  $userdata = array() ;   
   }
   return $userdata ;
}


function checkIdIsAvaliable($table,$match_type,$match_id)
	{
		$sql = 'select * from '.$table.' where '.$match_type.' = '.$match_id;
		$query = $this->db->query($sql);
 		if($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
function add_category()
	{
		
		$Categoryname=$this->input->post('Categoryname');
		$Catdesc=$this->input->post('catdesc2');
		$catSlug = $this->authorize->getSlug($Categoryname,'-');
		
		$data=array(
		'name' => $Categoryname,
		'description' => $Catdesc,
		'slugUrl'=>$catSlug
		);
		$this->db->insert('categories', $data);
		$id = $this->db->insert_id();
		
		$this->load->library('upload');
		$this->load->library('image_lib');
		
		$upload_path = './uploads/category_images/';
		
		
		if ($upload_path) 
		{
			$upload_path = $upload_path;
			if(isset($_FILES['Categoryimage']['name']) && $_FILES['Categoryimage']['error']==0)
			{
				$image143 = $_FILES['Categoryimage']['name'];
				$config['upload_path'] = $upload_path;//'./uploads/';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|swf|flv';
				$config['encrypt_name'] = TRUE;
				$config['max_size']	= 5000;
				$config['max_width']  = 0;
				$config['max_height']  = 0;
				$this->upload->initialize($config);
				if( $this->upload->do_upload('Categoryimage'))
				{
					$image_data = $this->upload->data();
					$image_name = $image_data['file_name'];
					
					$new_image_name=$upload_path.'thumb_'.$image_name;
					$this->image_lib->create_thumbnail($upload_path,$image_name,$new_image_name,280,192,"1d1d1d");
					
					$data= array('image'=>$image_name);
					$this->db->where('category_id',$id);
					$this->db->update('categories',$data);
				}
			}
		}

		if($id)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
function edit_category()
	{
		
		$Categoryname=$this->input->post('Categoryname');
		$Catdesc=$this->input->post('catdesc2');
		$Catid=$this->input->post('catid');
		$catSlug = $this->authorize->getSlug($Categoryname,'-');
		
		$data=array(
		'name' => $Categoryname,
		'description' => $Catdesc,
		'slugUrl'=>$catSlug
		);
		$this->db->where('category_id',$Catid);
		$this->db->update('categories', $data);

		
		$this->load->library('upload');
		$this->load->library('image_lib');
		
		$upload_path = './uploads/category_images/';
		
		
		if ($upload_path) 
		{
			$upload_path = $upload_path;
			if(isset($_FILES['Categoryimage']['name']) && $_FILES['Categoryimage']['error']==0)
			{
				$image143 = $_FILES['Categoryimage']['name'];
				$config['upload_path'] = $upload_path;//'./uploads/';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|swf|flv';
				$config['encrypt_name'] = TRUE;
				$config['max_size']	= 5000;
				$config['max_width']  = 0;
				$config['max_height']  = 0;
				$this->upload->initialize($config);
				if( $this->upload->do_upload('Categoryimage'))
				{
					$image_data = $this->upload->data();
					$image_name = $image_data['file_name'];
					
					$new_image_name=$upload_path.'thumb_'.$image_name;
					$this->image_lib->create_thumbnail($upload_path,$image_name,$new_image_name,280,192,"1d1d1d");
					
					$data= array('image'=>$image_name);
					$this->db->where('category_id',$Catid);
					$this->db->update('categories',$data);
				}
				else
				{
					echo $this->upload->display_errors('<p>', '</p>');
					die();
				}
			}
		}

		if($Catid)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	


}

?>

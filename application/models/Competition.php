<?php

class Competition extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		//$this->CI = & get_instance();
	}
	
	public function count_all_records( $keyword = null ) {
		if( $keyword ) {
			$this->db->like('round',$keyword);
			$this->db->or_like('team1',$keyword);
			$this->db->or_like('team2',$keyword);
			$this->db->or_like('winner',$keyword);
			// $this->db->or_like('email',$keyword);
			//$query = $this->db->get('competitions');
			$num_rows = $this->db->count_all_results('competitions');
		} else {
			$num_rows = $this->db->count_all_results('competitions');
		}
		
		return $num_rows;
	}
	
	function get_user_listing( $limit=null,$offset=null, $sortfield, $order , $search = null)
	{
		$this->db->select('competitions.*,venues.venue_name');
		$this->db->from('competitions');
		if( $search != '' ) {
			$this->db->or_like('team1',$search);
			$this->db->or_like('team2',$search);
			$this->db->or_like('winner',$search);
		}
		if($limit)
        {
            $this->db->limit( $limit,$offset );
        }
        $this->db->join('venues', 'venues.venue_id = competitions.venue_id');
        $this->db->order_by("$sortfield", "$order");
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		if ($query->num_rows() > 0)
        {
			return $query->result_array();
		}
	}
	
	function get_competition( $user_id )
	{
		$this->db->select('*');
		$this->db->where('competition_id', $user_id );
		$query = $this->db->get('competitions');
		if( $query->num_rows() > 0 ) {
			return $query->row_array();
		} else {
			return false;
		}
	}
	
	function save_competition() {
		$date = date('Y:m:d h:i:s');
		$team1 = ucfirst(strtolower($this->input->post('team1')));
		$team2 = ucfirst(strtolower($this->input->post('team2')));
		$winner = ucfirst(strtolower($this->input->post('winner')));
		$user_data = array(
			'venue_id' => $this->input->post('venue_id'),
			'competition_type' => $this->input->post('competition'),
			'round' => $this->input->post('rounds'),
			'match_name' => $this->input->post('mname'),
			'winner' => $winner,
			'team1' => $team1,
			'team2' => $team2,
			'match_date' => $this->input->post('date'),
			'time' => $this->input->post('time'),
			'date_created' => $date,
			'status' => $this->input->post('status')
		);

		$query = $this->db->insert('competitions',$user_data);
		if( $query > 0 ) {
			return true;
		} else {
			return false;
		}
			
	}
	
	function update_competition($id) {
		
		$team1 = ucfirst(strtolower($this->input->post('team1')));
		$team2 = ucfirst(strtolower($this->input->post('team2')));
		$winner = ucfirst(strtolower($this->input->post('winner')));
	
		$user_data = array(
			'competition_type' => $this->input->post('competition'),
			'round' => $this->input->post('rounds'),
			'match_name' => $this->input->post('mname'),
			'winner' => $winner,
			'team1' => $team1,
			'team2' => $team2,
			'match_date' => $this->input->post('date'),
			'time' => $this->input->post('time'),
			'status' => $this->input->post('status')
		);
		
		$this->db->where('competition_id', $id);
		$query = $this->db->update('competitions',$user_data);
		if( $query > 0 ) {
			return true;
		} else {
			return false;
		}
			
	}
	
	function delete( $id ) {
		$this->db->where('competition_id', $id);
		$query = $this->db->delete('competitions'); 
		if( $query > 0 ) {
			return true;
		} else {
			return false;
		}
	}
	
	function search( $keyword )
    {
        $this->db->like('first_name',$keyword);
        $this->db->or_like('email',$keyword);
        $query = $this->db->get('competitions');
        //echo $this->db->last_query(); die;
        if( $query->num_rows() > 0 ) {
			return $query->result_array();
		} else {
			return false;
		}
        
    }
    	
	function chkuname($uName,$uid=NULL)
    {
		$this->db->select('username');
		$this->db->from('competitions');
		$this->db->where('username',$uName);
		if($uid){
			$this->db->where_not_in('competition_id',$uid);
		}	
		$query=$this->db->get();
        return $query->num_rows();
	}
	
	function chkemail($email,$uid=NULL) {
        $this->db->select('email');
		$this->db->from('competitions');
		$this->db->where('email',$email);
		if($uid){
			$this->db->where_not_in('competition_id',$uid);
		}
		$query=$this->db->get();
        return $query->num_rows();
	}
    
    function change_status (){
		$user_id  = $this->input->post('competition_id');
		
		$this->db->select('status');
		$this->db->where('competition_id', $user_id);
        $query = $this->db->get('competitions');
        if( $query->num_rows() > 0 ) {
			$result =  $query->row_array();
			
			if( $result['status'] == 1 ) {
				$status = '0';
			} else {
				$status = '1';
			}
		
			$data = array(
				'status' => $status,
			);
			
			$this->db->where( 'competition_id', $user_id);
			$row = $this->db->update('competitions',$data);
			if( $row > 0 ) {
				if( $status == 1 ) {
					$ret_var = 'active';
				} else {
					$ret_var =  'inactive';
				}
				return $ret_var;
			} else {
				return false;
			}
				
		} else {
			return false;
		}
        
    }
    
    function get_venue(){
		$this->db->select('venue_id,venue_name');
		$this->db->order_by('venue_name','asc');
		$query = $this->db->get('venues');
		
		return $query->result_array();
	}
	
	function get_venue_part($venue_id){
		$this->db->select('venue_name');
		$this->db->where('venue_id',$venue_id);
		$query = $this->db->get('venues');
		// echo $this->db->last_query();
		return $query->result_array();
	}
	
	public function fetch_competitions_list( $comp_type, $venue, $round ) {
		$array = array('competition_type' => $comp_type, 'competitions.venue_id' => $venue, 'round' => $round, 'competitions.status' => '1');
		$this->db->select('competitions.*,venues.venue_name');
		$this->db->where( $array );
		$this->db->join('venues', 'venues.venue_id = competitions.venue_id');
		$query = $this->db->get('competitions');
	
		if( $query->num_rows() > 0 ) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	
	public function insert_new_tips( $data ) {
		$res = $this->db->insert_batch('tiping',$data);
		if( $res > 0 ) {
			return true;
		} else {
			return false;
		}
	}
	
	public function fetch_selected_tips( $comp_type, $venue, $round, $userId ) {
		$array = array('competition' => $comp_type, 'venue_id' => $venue, 'round' => $round, 'status' => '1','user_id' => $userId);
		$this->db->select('*');
		$this->db->where( $array );	
		$query = $this->db->get('tiping');
		if( $query->num_rows() > 0 ) {
			return $query->result_array();
		} else{
			return false;
		}
	
	}

}

?>

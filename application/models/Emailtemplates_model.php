<?php
class Emailtemplates_model extends CI_Model {


	function __construct()
	{
		parent::__construct();
		//$this->CI =& get_instance();
	}
	
	function getemailtemplateDetailsById($page_name)
	{
		$page = array();
		$this->db->select('*');

		$this->db->where('email_template_id', $page_name);	
		$query = $this->db->get('email_templates');
		if($query->num_rows() > 0)
		{
			$page = $query->row_array();
		}
		return $page;
	}
	
	function getemailtemplates($strSearch='',$start_position=0, $pagesize=0)
	{
		$userdata=array();
		$this->db->select('*');
		if($strSearch)
		{
			$this->db->like('template_name', $strSearch, 'both'); 
			$this->db->order_by("template_name", "asc");
			$query = $this->db->get('email_templates'); 
		}
		else if($pagesize)
		{
			$query = $this->db->get('email_templates', $pagesize, $start_position);
		}
		else
		{
			$query = $this->db->get('email_templates');
		}
		
		if($query->num_rows()>0)
		{
			$userdata=$query->result_array();
		}
		return $userdata;
	}
	
	function editEmailTemplate($email_template_id)
	{
		if($email_template_id)
		{
			$subject=$this->input->post('subject');
			$from=$this->input->post('from');
			$content=$this->input->post('content1');
			
			$active_chk=$this->input->post('active');
			$active = ($active_chk) ? 1 : 0;
							
			$data=array(
			'subject' => $subject,
			'from' => $from,
			'active' => $active,
			'body' => $content
			);
			
			$this->db->where('email_template_id', $email_template_id);
			$this->db->update('email_templates',$data);
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function getEmailTemplateVariables($email_template_id)
	{
		$variables = array();
		$this->db->where('email_template_id', $email_template_id);
		$query = $this->db->get('email_template_variables');
		
		if($query->num_rows()>0)
		{
			$variables=$query->result_array();
			return $variables;
		}
		else
		{
			return false;
		}
		
	}
	
	function checkIdIsAvaliable($table,$match_type,$match_id)
	{
		$sql = 'select * from '.$table.' where '.$match_type.' = '.$match_id;
		$query = $this->db->query($sql);
 		if($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	function getAllNewsletters()
	{
		$userdata=array();
		$this->db->select('*');
		$query = $this->db->get('newsletters');
		if($query->num_rows()>0)
		{
			$variables=$query->result_array();
			return $variables;
		}
		else
		{
			return false;
		}
	}	
	function insertData()
	{
		$subject=$this->input->post('subject');
		$from=$this->input->post('from');
		$content=$this->input->post('content1');
		$monthlyday=$this->input->post('monthly');
		$active_chk=$this->input->post('active');
		$active = ($active_chk) ? 1 : 0;
		$mntopt=$this->input->post('send_monthly_day');
		$dayopt=$this->input->post('senddate');
		$date_time = date('Y-m-d H:i:s');
		
		if($monthlyday=='monthly')
		{
		$opt=$mntopt;
		$mnt=1;
		}
		else
		{
		$opt='';
		$mnt=0;
		}
		
		if($monthlyday=='later')
		{
		$opt=0;
		@$day = date("y-m-d", strtotime($dayopt));
		$mnt=0;
		}
		$data=array(
		'subject' => $subject,
		'from_email' => $from,
		'active' => $active,
		'date_created' => $date_time, 
		'body' => $content,
		'send_monthly' => $mnt,
		'send_monthly_day' => $opt,
		'send_date' => @$day
		);
		$this->db->insert('newsletters', $data);
		$id = $this->db->insert_id();
			if($id)
			{
				return true;
			}
			else
			{
				return false;
			}
	}
	function getNewsletterDetailsById($newsletter_id)
	{
		$data = array();
		$this->db->select('*');

		$this->db->where('newsletter_id ', $newsletter_id);	
		$query = $this->db->get('newsletters');
		if($query->num_rows() > 0)
		{
			$data = $query->row_array();
		}
		return $data;
	}
	function editnewsletter($newsletter_id)
	{
		if($newsletter_id)
		{
		$subject=$this->input->post('subject');
		$from=$this->input->post('from');
		$content=$this->input->post('content1');
		$monthlyday=$this->input->post('monthly');
		$active_chk=$this->input->post('active');
		$active = ($active_chk) ? 1 : 0;
		$mntopt=$this->input->post('send_monthly_day');
		$dayopt=$this->input->post('senddate');
		if($monthlyday=='monthly')
		{
		$opt=$mntopt;
		$mnt=1;
		$date_time=NULL;
		$day=NULL;
		}
		else
		{
		$opt='';
		$mnt=0;
		}
		
		if($monthlyday=='later')
		{
		$opt=0;
		$day = date("y-m-d", strtotime($dayopt));
		$date_time   = date('Y-m-d H:i:s');
		
		$mnt=0;
		}
		
		$data=array(
		'subject' => $subject,
		'from_email' => $from,
		'active' => $active,
		'body' => $content,
		'date_modified' => $date_time,
		'send_monthly' => $mnt,
		'send_monthly_day' => $opt,
		'send_date' => $day
		);
		$this->db->where('newsletter_id', $newsletter_id);
		$this->db->update('newsletters',$data);
		return true;
		}
	}
	function deleteNewsletters($newletter_id)
	{
		if($newletter_id)
		{
		$this->db->where('newsletter_id', $newletter_id);
		$this->db->delete('newsletters');
		return true;
		}
		else
		{
		return false;
		}
	}
}

?>

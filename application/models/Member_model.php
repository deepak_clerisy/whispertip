<?php

class Member_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		//$this->CI = & get_instance();
		$this->load->library('email');
	}
	
	function getAllMembers()
	{
		$data = array();
		$this->db->select('*');
		$query = $this->db->get('users');
		if($query->num_rows())
		{
			$data = $query->result_array();
			return $data;
		}
		else
		{
			return $data;
		}
	}
	
	function getMemberDetailsById($userid)
	{
		$data = array();
		$this->db->select('*');
		$this->db->where('id',$userid);
		$query = $this->db->get('users');
		if($query->num_rows())
		{
			$data = $query->row_array();
			return $data;
		}
		else
		{
			return $data;
		}
	}
	
	function getPaidDetails()
	{
		$data = array();
		$userid = $this->session->userdata('userid');
		$this->db->select('*');
		$this->db->where('userid',$userid);
		$query = $this->db->get('user_bet_rates');
		if($query->num_rows())
		{
			$data = $query->row_array();
			return $data;
		}
		else
		{
			return $data;
		}
	}
	
	function updateGetPaid()
	{
		$userId = $this->session->userdata('userid');
		$this->db->select('*');
		$this->db->where('userid',$userId);
		$sel = $this->db->get('user_bet_rates');
		
		$insArr = array('customer_price'=>$this->input->post('customerprice'),
						'whisper_tip'=>$this->input->post('whisperamount'),
						'purchase_amount'=>$this->input->post('purchaseamount'));
						
		if($sel->num_rows())
		{
			$this->db->where('userid',$userId);
			$fl = $this->db->update('user_bet_rates',$insArr);
		}
		else
		{
			$insArr['userid'] = $userId;
			$fl = $this->db->insert('user_bet_rates',$insArr);
		}
		
		if($fl)
			return true;
		else
			return false;
	}
	
	function updateProfile()
	{
		$userId = $this->session->userdata('userid');
		$profile1 = $this->input->post('profile_image1');
			
		$insArr = array('first_name'=>$this->input->post('first_name'),
						'last_name'=>$this->input->post('last_name'),
						'username'=>$this->input->post('username'),
						'email' => $this->input->post('email'),
						'gender'=>$this->input->post('gender'),
						'country'=>$this->input->post('country'),
						'phone' => $this->input->post('phone'),
						'dob' => $this->input->post('dob'),
						'address' => $this->input->post('address')
					);
						
		$this->db->where('id',$userId);
		$fl = $this->db->update('users',$insArr);
						
		$this->load->library('upload');
		$this->load->library('image_lib');
		
		$upload_path = './uploads/profile_picture/';
		
		
		if ($upload_path) 
		{
			$upload_path = $upload_path;
			if(isset($_FILES['profilePic']['name']) && $_FILES['profilePic']['error']==0)
			{
				$image143 = $_FILES['profilePic']['name'];
				$config['upload_path'] = $upload_path;//'./uploads/';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|swf|flv';
				$config['encrypt_name'] = TRUE;
				$config['max_size']	= 5000;
				$config['max_width']  = 0;
				$config['max_height']  = 0;
				$this->upload->initialize($config);
				if( $this->upload->do_upload('profilePic'))
				{
					$image_data = $this->upload->data();
					$image_name = $image_data['file_name'];
					
					$new_image_name=$upload_path.'thumb_'.$image_name;
					$new_image_name1=$upload_path.'small_'.$image_name;
					//$this->image_lib->create_thumbnail($upload_path,$image_name,$new_image_name,125,118,"1d1d1d");
					//$this->image_lib->create_thumbnail($upload_path,$image_name,$new_image_name1,280,192,"1d1d1d");
					
					$data= array('profile_image'=>$image_name);					
					$this->db->where('id',$userId);
					$fl = $this->db->update('users',$data);
				}
				
				if( $profile1 != '' ) {
					unlink($upload_path.$profile1);
				}
			}
		}
		if($fl)
			return true;
		else
			return false;
	}
	
	public function updatePassword() {
		$user_id = $this->session->userdata('userid');
		
		$current_pass = md5( $this->input->post('current_password'));
		
		$this->db->select('password');
		$this->db->where('id',$user_id);
		$query = $this->db->get('users');
		if($query->num_rows())
		{
			$result = $query->row_array();
			if( $current_pass == $result['password'] ) {
				$data = array(
					'password' => md5($this->input->post('new_password')),
					);
				$this->db->where('id',$user_id);
				$res = $this->db->update('users',$data);
				if($res > 0 ) {
					return true;
				}
			} else {
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	public function update_main_venue() {
		$user_id = $this->session->userdata('userid');
		$data = array(
			'main_venue' => $this->input->post('main_venue'),
		);
		
		$this->db->where('id',$user_id);
		$res = $this->db->update('users',$data);
		if( $res > 0 ) {
			return true;
		} else {
			return false;
		}		
	}
	
}
?>

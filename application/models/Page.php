<?php

class Page extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		//$this->CI = & get_instance();
	}
	
	public function count_all_records( $keyword = null ) {
		if( $keyword ) {
			$this->db->like(' page_name ',$keyword);
			//~ $this->db->or_like('team1',$keyword);
			//~ $this->db->or_like('team2',$keyword);
			//~ $this->db->or_like('winner',$keyword);
			// $this->db->or_like('email',$keyword);
			//$query = $this->db->get('competitions');
			$num_rows = $this->db->count_all_results('static_pages');
		} else {
			$num_rows = $this->db->count_all_results('static_pages');
		}
		
		return $num_rows;
	}
	
	function get_user_listing( $limit=null,$offset=null, $sortfield, $order )
	{
		$this->db->select('*');
		$this->db->from('static_pages');
		if($limit)
        {
            $this->db->limit( $limit,$offset );
        }
        $this->db->order_by("$sortfield", "$order");
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		if ($query->num_rows() > 0)
        {
			return $query->result_array();
		}
	}
	
	function get_page( $user_id )
	{
		$this->db->select('*');
		$this->db->where('static_page_id', $user_id );
		$query = $this->db->get('static_pages');
		if( $query->num_rows() > 0 ) {
			return $query->row_array();
		} else {
			return false;
		}
	}
	
	
	function update_page($id) {

		$date = date('Y:m:d h:i:s');
		$user_data = array(
			'page_name' => $this->input->post('pagetitle'),
			'meta_description' => $this->input->post('metadescription'),
			'meta_keywords' => $this->input->post('metakeyword'),
			'page_title' => $this->input->post('pagetitle'),
			'page_url' => $this->input->post('acknowledgement'),
			'page_body' => $this->input->post('content1'),
			'date_modified' => $date,

		);
		
		$this->db->where('static_page_id', $id);
		$query = $this->db->update('static_pages',$user_data);
		if( $query > 0 ) {
			return true;
		} else {
			return false;
		}
			
	}
	
	public function get_pagecontent_by_id( $page_id ) {
		$this->db->select('*');
		$this->db->where('static_page_id',$page_id);
		$result = $this->db->get('static_pages');
		if( $result->num_rows() > 0 ) {
			return $result->row_array();			
		} else {
			return false;
		}
	}
	

}

?>

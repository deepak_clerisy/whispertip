<?php
class Pages_model extends CI_Model {


	function __construct()
	{
		parent::__construct();
		//$this->CI =& get_instance();
	}
	
	function getAllPages($strSearch='',$start_position=0, $pagesize=0)
	{
		$userdata=array();
		$this->db->select('*');
		$this->db->where("rshow", "1");
		if($strSearch)
		{
		$this->db->like('page_title', $strSearch, 'both'); 
		$this->db->order_by("page_title", "asc");
		$query = $this->db->get('static_pages');
		}
		else if($pagesize)
		{
			//$this->db->like('page_title', $strSearch, 'both');
			$query = $this->db->get('static_pages', $pagesize, $start_position);
		}
		else
		{
			$query = $this->db->get('static_pages');
		}
		
		if($query->num_rows()>0)
		{
		$userdata=$query->result_array();
		return $userdata;
		}
	}
	
	function getAllPagesMeta($strSearch='',$start_position=0, $pagesize=0)
	{
		$userdata=array();
		$this->db->select('*');
		if($strSearch)
		{
		$this->db->like('page_title', $strSearch, 'both'); 
		$this->db->order_by("page_title", "asc");
		$query = $this->db->get('static_pages');
		}
		else if($pagesize)
		{
			//$this->db->like('page_title', $strSearch, 'both');
			$query = $this->db->get('static_pages', $pagesize, $start_position);
		}
		else
		{
			$query = $this->db->get('static_pages');
		}
		
		if($query->num_rows()>0)
		{
		$userdata=$query->result_array();
		return $userdata;
		}
	}
	
	function getPageDetailsById($page_id)
	{
		$page = array();
		$this->db->select('*');
		$this->db->where('static_page_id', $page_id);	
		$query = $this->db->get('static_pages');
		if($query->num_rows() > 0)
		{
			$page = $query->row_array();
		}
		return $page;
	}
	
	function editPage($page_id)
	{
		if($page_id)
		{
		$pagetitle=$this->input->post('pagetitle');
		$metadescription=$this->input->post('metadescription');
		$metakeywords=$this->input->post('metakeywords');
		$page_url=$this->input->post('page_url');
		$page_content=$this->input->post('content1');
		$active_chk=$this->input->post('active');
		$active = ($active_chk) ? 1 : 0;
		$data=array(
		'page_title' => $pagetitle,
		'meta_description' => $metadescription,
		'meta_keywords' => $metakeywords,
		'page_url' => $page_url,
		'date_modified' => date('Y-m-d H:i:s'),
		'active' => $active,
		'page_body' => $page_content
		);
		$this->db->where('static_page_id', $page_id);
		$this->db->update('static_pages',$data);
		return true;
		}
	}
	function editPageplace($page_id)
	{
		if($page_id)
		{
		$pagetitle=$this->input->post('pagetitle');
		$metadescription=$this->input->post('metadescription');
		$metakeywords=$this->input->post('metakeywords');
		$page_url=$this->input->post('page_url');
		$page_content=$this->input->post('content1');
		$active_chk=$this->input->post('active');
		$active = ($active_chk) ? 1 : 0;
		$data=array(
		'page_title' => $pagetitle,
		'meta_description' => $metadescription,
		'meta_keywords' => $metakeywords,
		'page_url' => $page_url,
		'date_modified' => date('Y-m-d H:i:s'),
		'active' => $active,
		'page_body' => $page_content
		);
		$this->db->where('static_page_id', $page_id);
		$this->db->update('static_pages',$data);
		return true;
		}
	}
	
	function editfooterPage($page_id)
	{
		if($page_id)
		{
		$page_content=$this->input->post('page_body');		
		$data=array(
		'page_body' => $page_content
		);
		$this->db->where('static_page_id', $page_id);
		$this->db->update('static_pages',$data);
		return true;
		}
	}
	
	function getPageDetailsByName($page_url)
	{
		$page = array();
		$sql = "select * from static_pages where page_url = '$page_url' and active=1";
         //echo $sql;die;
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
			$page = $query->row_array();
		}
		return $page;
	}
	function publicSearchforPagination($txtsearch)
	{
		$data = array();
		if($txtsearch)
		{
			
			$sql = "select a.*,b.* from users_personal_details a,users b where (a.firstname like '%$txtsearch%' or a.lastname like '%$txtsearch%') and a.userid=b.userid";
			
			$query = $this->db->query($sql);
			if($query->num_rows() > 0)
			{
				$data = $query->result_array();
				return $data;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function public_search($txtsearch,$start_pos,$page_size)
	{
		$data = array();
		if($txtsearch)
		{
					
			$sql="select a.*,b.* from users_personal_details a,users b where (a.firstname like '%$txtsearch%' or a.lastname like '%$txtsearch%') and a.userid=b.userid limit $start_pos,$page_size";
			
			$query = $this->db->query($sql);
			if($query->num_rows() > 0)
			{
				$data = $query->result_array();
				return $data;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function public_search_boothforpagination($txtsearch)
	{
		$data = array();
		if($txtsearch)
		{
		
			$sql ="select * from booths where booth_name like '%$txtsearch%'";
			
			$query = $this->db->query($sql);
			if($query->num_rows() > 0)
			{
				$data = $query->result_array();
				
				return $data;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function public_search_booth($txtsearch,$start_pos,$page_size)
	{
		$data = array();
		if($txtsearch)
		{
		
			$sql ="select * from booths where booth_name like '%$txtsearch%' limit $start_pos,$page_size";
			
			$query = $this->db->query($sql);
			if($query->num_rows() > 0)
			{
				$data = $query->result_array();
				
				return $data;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function subscribe_Newslatter($email)
	{
		$page = array();
		$sql = "select * from newsletters where from_email = '$email' ";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
			$page = $query->row_array();
			return $page;
		}
		else
		{
			return false;	
		}	
		
	}
	function add_Newslatter($email)
	{
		$subject='newslatter';
		$from=$email;
		$content='';
		$monthlyday='';
		$active_chk='yes';
		$active = ($active_chk) ? 1 : 0;
		$mntopt='';
		$dayopt='';
		$date_time = date('Y-m-d H:i:s');
		

		$data=array(
		'subject' => @$subject,
		'from_email' => @$from,
		'active' => @$active,
		'date_created' => @$date_time, 
		'body' => @$content,
		'send_monthly' => 'yes',
		'send_monthly_day' => $date_time,
		'send_date' => @$day
		);
		$this->db->insert('newsletters', $data);
		$id = $this->db->insert_id();
		if($id)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	function checkIdIsAvaliable($table,$match_type,$match_id)
	{
		$sql = 'select * from '.$table.' where '.$match_type.' = '.$match_id;
		$query = $this->db->query($sql);
 		if($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	function getAllPagesName($page_type)
	{
		$page_name= array();
		$sql = "select * from static_pages where page_type='$page_type'";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
			$page_name = $query->result_array();
			
		}
		return $page_name;
		
	}
	function editMetaPage($page_id)
	{
		if($page_id)
		{
			$pagetitle=$this->input->post('pagetitle');
			$metadescription=$this->input->post('metadescription');
			$metakeywords=$this->input->post('metakeywords');
			$data=array(
			'page_title' => $pagetitle,
			'meta_description' => $metadescription,
			'meta_keywords' => $metakeywords,
			);
			$this->db->where('static_page_id', $page_id);
			$this->db->update('static_pages',$data);
			return true;
		}
	}
	
	function getPageDetailsByProductId($pid)
	{
		$sql = "SELECT p.product_desc as meta_description,c.category_id  ,sp.meta_keywords 
				FROM products as p join categories as c 
				on p.category_id = c.category_id
				join static_pages sp 
				on c.page_url = sp.page_url
				where product_id=$pid";
		$query = $this->db->query($sql);
		if($query->num_rows()>0)
		{
			$data = $query->row_array();
		}
		else
		{
			$data = array();
		}
		return $data;
	}
	function getHowItPages()
	{
		$userdata=array();
		$this->db->select('*');
		$query = $this->db->get('tbl_how_it_work');
		if($query->num_rows()>0)
		{
		$userdata=$query->result_array();
		return $userdata;
		}
	}
	function getPlacePageDetailsById($page_id)
	{
		$page = array();
		$this->db->select('*');
		$this->db->where('how_it_id', $page_id);	
		$query = $this->db->get('tbl_how_it_work');
		if($query->num_rows() > 0)
		{
			$page = $query->row_array();
		}
		//echo '<pre>';print_r($page);die;
		return $page;
	}
	function editPlacePage($page_id)
	{
		if($page_id==1)
		{
		$pagetitle=$this->input->post('pagetitle');
		$heading1=$this->input->post('heading1');
		$desc1=$this->input->post('desc1');
		$heading2=$this->input->post('heading2');
		$desc2=$this->input->post('desc2');
		$heading3=$this->input->post('heading3');
		$desc3=$this->input->post('desc3');
		$heading4=$this->input->post('heading4');
		$desc4=$this->input->post('desc4');
		$data=array(
		'page_title' => $pagetitle,
		'heading1' => $heading1,
		'description1' => $desc1,
		'heading2' => $heading2,
		'description2' =>$desc2,
		'heading3' => $heading3,
		'description3' => $desc3,
		'heading4' => $heading4,
		'description4' => $desc4,
		'date_created' =>  date('Y-m-d H:i:s')
		);
		$this->db->where('how_it_id', $page_id);
		$this->db->update('tbl_how_it_work',$data);
		return true;
		}
		else
		{
		$pagetitle=$this->input->post('pagetitle');
		$heading1=$this->input->post('heading1');
		$desc1=$this->input->post('desc1');
		$heading2=$this->input->post('heading2');
		$desc2=$this->input->post('desc2');
		$heading3=$this->input->post('heading3');
		$desc3=$this->input->post('desc3');
		$data=array(
		'page_title' => $pagetitle,
		'heading1' => $heading1,
		'description1' => $desc1,
		'heading2' => $heading2,
		'description2' =>$desc2,
		'heading3' => $heading3,
		'description3' => $desc3,
		'date_created' =>  date('Y-m-d H:i:s')
		);
		$this->db->where('how_it_id', $page_id);
		$this->db->update('tbl_how_it_work',$data);
		return true;	
		}
	}
}

?>

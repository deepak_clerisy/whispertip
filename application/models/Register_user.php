<?php

class Register_user extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		//$this->CI = & get_instance();
		$this->load->library('email');
	}
	function check_user($email)
	{
		$this->db->select('*');
		$this->db->where('email',$email);
		$this->db->where('verified', 1);
		$query = $this->db->get('users');
		if($query->num_rows() >0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function check_username( $username )
	{
		$this->db->select('*');
		$this->db->where('username',$username);
		$query = $this->db->get('users');
		if($query->num_rows() >0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function check_email($email)
	{
		$this->db->select('*');
		$this->db->where('email',$email);
		$query = $this->db->get('users');
		if($query->num_rows() >0)
		{
			$response = $query->row_array();
			return $response;
		}
		else
		{
			return false;
		}
	}
	
	function check_user_twitter($id)
	{
		$this->db->select('*');
		$this->db->where('twitter_id',$id);
		$query = $this->db->get('users');
		if($query->num_rows() >0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}	
		
	
	function facebook($email, $facebook_id, $first_name, $last_name, $screen_name, $gender, $birthday, $image)
	{
		$username = $first_name;
		$birthday = date('Y-m-d', strtotime($birthday));
		$data = array(
			'username' => $username,
			'first_name' => $first_name,
			'last_name' => $last_name,
			'email' => $email,
			'facebook_id' => $facebook_id,
			'dob' => $birthday,
			'via' => 'facebook',
			'status' => '1',
			'login_type' => 'Patron',
			'gender'=> ucfirst($gender),
			'verified' => '1',
			'signup_date' => date('Y/m/d H:m:s'),
			'profile_image'=>'https://graph.facebook.com/'.$facebook_id.'/picture?type=normal'
		);
		$this->db->insert('users',$data);		
		$id = $this->db->insert_id();
		
		if($id)
		{
			return true;
		}
		else
		{
			return false;
		}
			
	}
	
	function google_plus($email, $google_id, $first_name, $last_name, $screen_name, $gender, $birthday,$image)
	{
		$username = $first_name;
		$birthday = date('Y-m-d', strtotime($birthday));
		$data = array(
			'first_name' => $first_name,
			'last_name' => $last_name,
			'email' => $email,
			'google_plus_id' => $google_id,
			'dob' => $birthday,
			'via' => 'google_plus',
			'status' => '1',
			'login_type' => 'Patron',
			'gender'=> ucfirst($gender),
			'verified' => '1',
			'signup_date' => date('Y/m/d H:m:s'),
			//'profile_image'=>'https://lh3.googleusercontent.com/'.$google_id.'/picture?type=normal'
			'profile_image'=>$image
		);
		$this->db->insert('users',$data);		
		$id = $this->db->insert_id();
		
		if($id)
		{
			return true;
		}
		else
		{
			return false;
		}
			
	}
	
	function twitter($email)
	{
		$key =$this->config->item('mailchimp_key');
		$list_id =$this->config->item('mailchimp_list_id');
		$twitter = $this->session->userdata('twittersess');		
		$data = array(
		'first_name' => $twitter['first_name'],		
		'email' => $email,
		'twitter_id' => $twitter['twitter_id'],		
		'profile_image' => $twitter['profile_image'],		
		'via' => 'twitter',	
		'active' => '1',
		'verified' => '1',
		'signup_date' => date('Y/m/d H:m:s'),
		'tw_screen_name' => $twitter['screen_name'],
		);
		$this->db->insert('users',$data);			
		$id = $this->db->insert_id();
		if($id)
		{
			$user_wallet = array(
				'userid' => $id,
				'balance' => 0.00,
				'active'=>1,
				'last_updated_datetime'=>date('Y/m/d H:m:s'),
			);
			
			
			$this->db->insert('user_wallet',$user_wallet);	
			$val = BASEPATH;
			$basepath = str_replace('system', 'assets', BASEPATH);
			$filetoinclude = $basepath.'MCAPI.class.php';
			include_once $filetoinclude;
			$api = new MCAPI($key);//Live
			$merge_vars = array('FNAME'=>$twitter['first_name']); 
			$api->listSubscribe($list_id, $email, $merge_vars);
			return true;	
			
		}
		
	}
	
	
	function twitter_register($twitter_id, $first_name, $screen_name, $image)
	{
		$key =$this->config->item('mailchimp_key');
		$list_id =$this->config->item('mailchimp_list_id');
		$twitter = $this->session->userdata('twittersess');		
		$data = array(
			'username' => $screen_name,		
			'first_name' => $first_name,		
			'email' => $screen_name.'@twitter.com',
			'twitter_id' => $twitter_id,		
			'profile_image' => $image,		
			'via' => 'twitter',	
			'status' => '1',
			'verified' => '1',
			'login_type' => 'Patron',
			'signup_date' => date('Y/m/d H:m:s'),
			'tw_screen_name' => $screen_name,
		);
		$this->db->insert('users',$data);			
		$id = $this->db->insert_id();
		
		$email = $screen_name.'@twitter.com';
		
		if($id)
		{
			return true;	
		} else {
			return false;
		}
	}
	
	function registerUser()
	{
		$key =$this->config->item('mailchimp_key');
		$list_id =$this->config->item('mailchimp_list_id');
		$data = array(
		'email' => $this->input->post('userEmail'),
		'username' => $this->input->post('userName'),
		'password'=> md5($this->input->post('userPassword')),
		'first_name'=>$this->input->post('userFirstName'),
		'last_name'=>$this->input->post('userLastName'),
		'country'=>$this->input->post('userCountry'),
		'login_type'=>$this->input->post('userLogintype'),
		'status' => '1',
		'verified' => '1',
		'signup_date' => date('Y/m/d H:m:s'),
		);
		$this->db->insert('users',$data);			
		$id = $this->db->insert_id();
		
		/************* Register Email *********************************/
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'iso-8859-1';
		$config['mailtype'] = 'html';
		$config['wordwrap'] = TRUE;
		
		$email = $this->input->post('userEmail');
		$first_name = $this->input->post('userFirstName');
		
		$this->email->initialize($config);
		$this->email->from('narinder1407@gmail.com', 'Whispertip');
		$this->email->to($email);
		$this->email->subject('Whispertip || Account created');
		$this->email->message('<p>Hello '.$first_name.',</p>
			<p>Greetings for the day!! </p>
			<p>Thank You for registering with the Whispertip.com. For any kind of support you can contact site admin at admin@whispertip.com. Our executve will be happy to assist you.</p>
			<p></p><br/>
			<p>Regards & Thanks,</p>
			<p>Team Whispertip</p>'
		);
		$this->email->send();
		/**************************************************************/
		
		
		return true;
	}
	
	function getEmailTemplate($id)
	{
		$this->db->select('*');
		$this->db->where('email_template_id', $id);
		$query = $this->db->get('email_templates');
		return $query->result_array();
	}
	
	function loginuserSocial($email)
	{
		$data = array();
		$this->db->select('*');
		$this->db->where('email',$email);
		$this->db->where('verified',1);
		$query = $this->db->get('users');
		if($query->num_rows() > 0)
		{
			$data = $query->row_array();
		}
		return $data;	
	}
	
	function loginuserFacebook($facebook_id)
	{
		$data = array();
		$this->db->select('*');
		$this->db->where('facebook_id',$facebook_id);
		$query = $this->db->get('users');
		if($query->num_rows() > 0)
		{
			$data = $query->row_array();
		}
		return $data;	
	}
	
	function loginuserTwitter($twitter_id)
	{
		$data = array();
		$this->db->select('*');
		$this->db->where('twitter_id',$twitter_id);
		$query = $this->db->get('users');
		if($query->num_rows() > 0)
		{
			$data = $query->row_array();
		}
		return $data;	
	}
	
	function verifyCode($email,$code)
	{
		$this->db->select('*');
		$this->db->where('email',$email);
		$this->db->where('verification_key',$code);
		$query = $this->db->get('users');
		if($query->num_rows() > 0)
		{
			$data = array(
			'verified'=> '1',
			'status'=> '1'			
			);
			$this->db->where('email',$email);
			$this->db->update('users',$data);
			return true;	
		}
		else
		{
				return false;
		}
	}
	
	function verify_venue($email,$code)
	{
		$this->db->select('*');
		$this->db->where('email',$email);
		$this->db->where('verify_code',$code);
		$query = $this->db->get('venues');
		if($query->num_rows() > 0)
		{
			$data = array(
			'verified'=> '1',
			'status'=> '1'			
			);
			$this->db->where('email',$email);
			$this->db->update('venues',$data);
			return true;	
		}
		else
		{
				return false;
		}
	}
	
	function verify_patron($email,$code)
	{
		$this->db->select('*');
		$this->db->where('email',$email);
		$this->db->where('verification_key',$code);
		$query = $this->db->get('users');
		if($query->num_rows() > 0)
		{
			$data = array(
			'verified'=> '1',
			'status'=> '1'			
			);
			$this->db->where('email',$email);
			$this->db->update('users',$data);
			return true;	
		}
		else
		{
				return false;
		}
	}
	
	function login($userName,$userPassword)
	{
		$userPassword = md5($userPassword);
		$sql = "SELECT * FROM (`users`) WHERE (`username` = '".$userName."' OR `email` = '".$userName."') AND `password` = '".$userPassword."' AND `verified` = 1";
		$query = $this->db->query($sql);
		
		if($query->num_rows() > 0)
		{
			$data = $query->row_array();
			$useremail = $data['email'];
			$userid = $data['id'];
			$username = $data['username'];
			$this->session->set_userdata('useremail', $useremail);
			$this->session->set_userdata('userid', $userid);
			$this->session->set_userdata('username', $username);
			$this->session->set_userdata('user_loggedin', true);
			return true;	
		}
		else
		{
			return false;
		}
	}
	
	function insertTwitterToken($token)
	{
		$explodeStr = explode('&',$token);
		//echo '<pre>';print_r($explodeStr);die;
		$screen_name = $explodeStr[3];
		$explodeStr1 = explode('=',$screen_name);
		$userId = $this->session->userdata('userid');
		//$userId = $this->CI->session->userdata('userid');
		$update = array('twitter_token'=>$token,'tw_screen_name'=>$explodeStr1[1]);
		$this->db->where('id',$userId);
		$fl = $this->db->update('users',$update);
		if($fl)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function getTwitterToken()
	{
		$userId = $this->session->userdata('userid');
		$this->db->where('id',$userId);
		$query = $this->db->get('users');
		if($query->num_rows())
		{
			$result = $query->row_array();
			if($result['twitter_token']!='')
			{
				return $result['twitter_token'];
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	function check_user_name($user_name)
	{
		$this->db->select('*');
		$this->db->where('username',$user_name);
		$this->db->where('verified', 1);
		$query = $this->db->get('users');
		if($query->num_rows() >0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	function getTwitterPublicToken($userId)
	{
		$this->db->where('userid',$userId);
		$query = $this->db->get('users');
		if($query->num_rows())
		{
			$result = $query->row_array();
			if($result['twitter_token']!='')
			{
				return $result['twitter_token'];
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function reset_patron_password($user_id,$password)
	{
		$this->db->set('password',md5($password));
		$this->db->where('id',$user_id);
		$this->db->update('users');
		if($this->db->affected_rows() == 1 )
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	function reset_venue_password($user_id,$password)
	{
		$this->db->set('password',md5($password));
		$this->db->where('venue_id',$user_id);
		$this->db->update('venues');
		if($this->db->affected_rows() == 1 )
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	function get_total_match_by_venue($venue_id,$type)
	{
		$this->db->where('venue_id',$venue_id);
		$this->db->where('competition_type',$type);
		$this->db->where('status','1');
		$this->db->from('competitions');
		return $this->db->count_all_results();
		
	}
	
	function get_patron_leaderboard($venue_id,$type)
	{
		$where=array(
		't.venue_id'=>$venue_id,
		't.competition'=>$type
		);
		$this->db->select('u.id, u.first_name, u.last_name, u.profile_image');
		$this->db->from('tiping t');
		$this->db->join('users u','t.user_id = u.id');
		$this->db->join('`competitions` c','c.venue_id = t.venue_id');
		$this->db->where($where);
		$this->db->where('c.winner = t.selected_team and t.round = c.round and t.match_id = c.competition_id ');
		$query=$this->db->get();
		
		if($query->num_rows() >0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
		
	}
	function get_user_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id',$id);
		$query=$this->db->get('users');
		if($query->num_rows() >0)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}
	
}
?>

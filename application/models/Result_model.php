<?php
class Result_model extends CI_Model {


	function __construct()
	{
		parent::__construct();
		//$this->CI =& get_instance();
	}
	
	function getAllSport()
	{
		$userdata = array();
		$this->db->select('*');
		$this->db->where("active", "1");
		$query = $this->db->get('categories');
		if($query->num_rows()>0)
		{
			$userdata = $query->result_array();
			return $userdata;
		}
	}
	function getAllCompetetions($event_type_id)
	{
		$userdata = array();
		$this->db->select('*');
		$this->db->where("active", "1");
		$this->db->where("event_type_id", $event_type_id);
		$query = $this->db->get('betfair_events_competitions');
		if($query->num_rows()>0)
		{
			$userdata = $query->result_array();
			return $userdata;
		}
	}
	function getAllEvents($comp_id)
	{
		$userdata = array();
		$this->db->select('*');
		$this->db->where("competition_id", $comp_id);
		$query = $this->db->get('betfair_events');
		if($query->num_rows()>0)
		{
			$userdata = $query->result_array();
			return $userdata;
		}
	}
	function getAllMarkets($comp_id)
	{
		$userdata = array();
		$this->db->select('*');
		$this->db->where("event_id", $comp_id);
		$query = $this->db->get('betfair_events_markets');
		if($query->num_rows()>0)
		{
			$userdata = $query->result_array();
			return $userdata;
		}
	}
	function getRunner($market_id)
	{
		$userdata = array();
		$this->db->select('*');
		$this->db->where("market_id", $market_id);
		$query = $this->db->get('betfair_event_runners');
		if($query->num_rows()>0)
		{
			$userdata = $query->result_array();
			return $userdata;
		}
	}
	function getMarketResults($market_id)
	{
		$userdata = array();
		$this->db->select('*');
		$this->db->where("market_id", $market_id);
		$query = $this->db->get('betfair_events_results');
		if($query->num_rows()>0)
		{
			$userdata = $query->row_array();
			return $userdata;
		}
	}
	function getResultsAndRunner($market_id)
	{
		$this->db->select('res.*,runn.selection_id as runner_selection_id,runn.runner_name');
		$this->db->from('betfair_events_results res');
		$this->db->join('betfair_event_runners runn', 'runn.market_id = res.market_id'); // this joins the user table to topics
		$this->db->where('res.market_id',$market_id); // this joins the user table to topics
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		 if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}        
		else
			return false;
	}
	function getRunnerById($market_id,$runner_id)
	{
		$userdata = array();
		$this->db->select('*');
		$this->db->where("market_id", $market_id);
		$this->db->where("selection_id", $runner_id);
		$query = $this->db->get('betfair_event_runners');
		if($query->num_rows()>0)
		{
			$userdata = $query->row_array();
			return $userdata;
		}
	}
	function UpdateResult($market_id)
	{
		if($this->input->post('marketId') && $this->input->post('radio_runner')){
			$userdata = array('selection_id'=>$this->input->post('radio_runner'),'status'=>'closed');
			$this->db->where("market_id", $market_id);
			$this->db->update('betfair_events_results',$userdata);
			return true;
		}
	}
}

?>

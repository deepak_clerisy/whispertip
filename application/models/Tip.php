<?php

class Tip extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		//$this->CI = & get_instance();
	}
	
	public function count_all_records($user_id, $keyword = null ) {
		$comp = $this->input->get('competition');
		$round =$this->input->get('rounds');
		
		$this->db->select('*');
		$this->db->where('user_id', $user_id );
		$this->db->join('venues', 'venues.venue_id = tiping.venue_id');
		if( $keyword != '' || $comp != '' || $round != '' ) { 
			$this->db->like('competition',$comp);
			
			if( $round > 0 )
				$this->db->where('round',$round);
			
			if( $keyword > 0 )
			$this->db->where('tiping.venue_id',$keyword);
			$num_rows = $this->db->count_all_results('tiping');
		} else {
			$num_rows = $this->db->count_all_results('tiping');
		}
	
		//echo $this->db->last_query();die;
		return $num_rows;
	}
	
	function get_user_listing($user_id,$limit=null,$offset=null, $search = null)
	{ 
	
		$comp = $this->input->get('competition');
		$round =$this->input->get('rounds');
		
		$this->db->select('tiping.*,venues.venue_name');
		$this->db->where('user_id',$user_id);
		$this->db->join('venues', 'venues.venue_id = tiping.venue_id');
		$this->db->order_by("tip_id", "asc");
		if($limit)
        {
            $this->db->limit( $limit,$offset );
        }
		if( $search != '' || $comp != '' || $round != '') {
			$this->db->like('competition',$comp);
			
			if( $round > 0 )
				$this->db->where('round',$round);
			
			if( $search > 0 )
			$this->db->Where('tiping.venue_id',$search);
		}  
		$query = $this->db->get('tiping');
     
		if ($query->num_rows() > 0)
        {
			return $query->result_array();
		}
	
	}
	/*function get_user_listing($user_id,$limit=null,$offset=null, $sortfield, $order , $search = null) {
		$this->db->select('*');
		$this->db->where('user_id',$user_id );
		$this->db->from('tiping');
		if( $search != '' ) {
			$this->db->like('competition',$search);
			$this->db->or_like('round',$search);
		}
		if($limit)
        {
            $this->db->limit( $limit,$offset );
        }
        $this->db->order_by("$sortfield", "$order");
		$query = $this->db->get();
		// echo $this->db->last_query(); die();
		if ($query->num_rows() > 0)
        {
			return $query->result_array();
		}
	}
	*/
	function get_user($user_id)
	{
		$this->db->select('*');
		$this->db->where('user_id', $user_id );
		$query = $this->db->get('tiping');
		if( $query->num_rows() > 0 ) {
			return $query->row_array();
		} else {
			return false;
		}
	}
	
	function delete($id) {
		$this->db->where('tip_id', $id);
		$query = $this->db->delete('tiping'); 
		if( $query > 0 ) {
			return true;
		} else {
			return false;
		}
	}
	
	function search($keyword)
    {
        $this->db->like('first_name',$keyword);
        $this->db->or_like('email',$keyword);
        $query = $this->db->get('tiping');
        //echo $this->db->last_query(); die;
        if( $query->num_rows() > 0 ) {
			return $query->result_array();
		} else {
			return false;
		}
        
    }
    	
	function chkuname($uName,$uid=NULL)
    {
		$this->db->select('username');
		$this->db->from('tiping');
		$this->db->where('username',$uName);
		if($uid){
			$this->db->where_not_in('tip_id',$uid);
		}	
		$query=$this->db->get();
        return $query->num_rows();
	}
	
	function chkemail($email,$uid) {
        $this->db->select('email');
		$this->db->from('tiping');
		$this->db->where('email',$email);
		if($uid){
			$this->db->where_not_in('tip_id',$uid);
		}
		$query=$this->db->get();
        return $query->num_rows();
	}
    
    function change_status (){
		$user_id  = $this->input->post('tip_id');
		
		$this->db->select('status');
		$this->db->where('tip_id', $user_id);
        $query = $this->db->get('tiping');
        if( $query->num_rows() > 0 ) {
			$result =  $query->row_array();
			
			if( $result['status'] == 1 ) {
				$status = 0;
			} else {
				$status = 1;
			}
			
			$data = array(
				'status' => $status,
			);
			
			$this->db->where( 'tip_id', $user_id);
			$row = $this->db->update('tiping',$data);
			if( $row > 0 ) {
				if( $status == 1 ) {
					$ret_var = 'active';
				} else {
					$ret_var =  'inactive';
				}
				return $ret_var;
			} else {
				return false;
			}
				
		} else {
			return false;
		}
        
    }
    
    public function chkemail_front($email,$uid) {
        $this->db->select('email');
		$this->db->from('tiping');
		$this->db->where('email',$email);
		if($uid){
			$this->db->where_not_in('tip_id',$uid);
		}
		$query=$this->db->get();
        return $query->num_rows();
	}
	
	function get_venue(){
		$this->db->select('venue_id,venue_name');
		$this->db->order_by('venue_name','asc');
		$query = $this->db->get('venues');
		
		return $query->result_array();
	}
}
?>

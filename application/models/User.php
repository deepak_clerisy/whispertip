<?php

class User extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		//$this->CI = & get_instance();
	}
	
	public function count_all_records( $keyword = null ) {
		if( $keyword ) {
			$this->db->like('username',$keyword);
			$this->db->or_like('email',$keyword);
			//$query = $this->db->get('users');
			$num_rows = $this->db->count_all_results('users');
		} else {
			$num_rows = $this->db->count_all_results('users');
		}
		
		return $num_rows;
	}
	
	function get_user_listing( $limit=null,$offset=null, $sortfield, $order , $search = null)
	{
		$this->db->select('*');
		$this->db->from('users');
		if( $search != '' ) {
			$this->db->like('username',$search);
			$this->db->or_like('email',$search);
		}
		if($limit)
        {
            $this->db->limit( $limit,$offset );
        }
        $this->db->order_by("$sortfield", "$order");
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		if ($query->num_rows() > 0)
        {
			return $query->result_array();
		}
	}
	
	function get_user( $user_id )
	{
		$this->db->select('*');
		$this->db->where('id', $user_id );
		$query = $this->db->get('users');
		if( $query->num_rows() > 0 ) {
			return $query->row_array();
		} else {
			return false;
		}
	}
	
	function save_user() {

		$user_data = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'username' => $this->input->post('username'),
			'email' => $this->input->post('email'),
			'password' => md5($this->input->post('password')),
			'gender' => $this->input->post('gender'),
			'country' => $this->input->post('country'),
			'phone' => $this->input->post('phone'),
			'dob' => $this->input->post('dob'),
			'address' => $this->input->post('address'),
			'status' => $this->input->post('status')
		);

		$query = $this->db->insert('users',$user_data);
		if( $query > 0 ) {
			return true;
		} else {
			return false;
		}
			
	}
	
	function update_user( $id ) {
	//print_r($_POST); die();
		$user_data = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'username' => $this->input->post('username'),
			'email' => $this->input->post('email'),
			'gender' => $this->input->post('gender'),
			'country' => $this->input->post('country'),
			'phone' => $this->input->post('phone'),
			'dob' => $this->input->post('dob'),
			'address' => $this->input->post('address'),
			'status' => $this->input->post('status')
		);
		
		$this->db->where( 'id', $id);
		$query = $this->db->update('users',$user_data);
		if( $query > 0 ) {
			return true;
		} else {
			return false;
		}
			
	}
	
	function delete( $id ) {
		$this->db->where('id', $id);
		$query = $this->db->delete('users'); 
		if( $query > 0 ) {
			return true;
		} else {
			return false;
		}
	}
	
	function search( $keyword )
    {
        $this->db->like('first_name',$keyword);
        $this->db->or_like('email',$keyword);
        $query = $this->db->get('users');
        //echo $this->db->last_query(); die;
        if( $query->num_rows() > 0 ) {
			return $query->result_array();
		} else {
			return false;
		}
        
    }
    	
	function chkuname($uName,$uid=NULL)
    {
		$this->db->select('username');
		$this->db->from('users');
		$this->db->where('username',$uName);
		if($uid){
			$this->db->where_not_in('id',$uid);
		}	
		$query=$this->db->get();
        return $query->num_rows();
	}
	
	function chkemail($email,$uid=NULL) {
        $this->db->select('email');
		$this->db->from('users');
		$this->db->where('email',$email);
		if($uid){
			$this->db->where_not_in('id',$uid);
		}
		$query=$this->db->get();
        return $query->num_rows();
	}
    
    function change_status (){
		$user_id  = $this->input->post('id');
		
		$this->db->select('status');
		$this->db->where('id', $user_id);
        $query = $this->db->get('users');
        if( $query->num_rows() > 0 ) {
			$result =  $query->row_array();
			
			if( $result['status'] == 1 ) {
				$status = 0;
			} else {
				$status = 1;
			}
			
			$data = array(
				'status' => $status,
			);
			
			$this->db->where( 'id', $user_id);
			$row = $this->db->update('users',$data);
			if( $row > 0 ) {
				if( $status == 1 ) {
					$ret_var = 'active';
				} else {
					$ret_var =  'inactive';
				}
				return $ret_var;
			} else {
				return false;
			}
				
		} else {
			return false;
		}
        
    }
    
    public function chkemail_front($email,$uid) {
        $this->db->select('email');
		$this->db->from('users');
		$this->db->where('email',$email);
		if($uid){
			$this->db->where_not_in('id',$uid);
		}
		$query=$this->db->get();
        return $query->num_rows();
	}
	
	
}

?>

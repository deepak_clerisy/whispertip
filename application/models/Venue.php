<?php

class Venue extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		//$this->CI = & get_instance();
	}
	
	public function count_all_records( $keyword = null ) {
		if( $keyword ) {
			$this->db->or_like('venue_name',$keyword);
			$this->db->or_like('email',$keyword);
			//$query = $this->db->get('competitions');
			$num_rows = $this->db->count_all_results('venues');
		} else {
			$num_rows = $this->db->count_all_results('venues');
		}
		
		return $num_rows;
	}
	
	function get_venue_listing( $limit=null,$offset=null, $sortfield, $order , $search = null)
	{
		$this->db->select('*');
		$this->db->from('venues');
		if( $search != '' ) {
			$this->db->like('venue_name',$search);
			$this->db->or_like('email',$search);
		}
		if($limit)
        {
            $this->db->limit( $limit,$offset );
        }
        $this->db->order_by("$sortfield", "$order");
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		if ($query->num_rows() > 0)
        {
			return $query->result_array();
		}
	}
	
	function get_venue( $user_id )
	{
		$this->db->select('*');
		$this->db->where('venue_id', $user_id );
		$query = $this->db->get('venues');
		
		if( $query->num_rows() > 0 ) {
			return $query->row_array();
		} else {
			return false;
		}
	}
	
	function save_venue() {
		
		$user_data = array(
			'venue_name' => $this->input->post('vname'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'password' => md5($this->input->post('password')),
			'contact' => $this->input->post('contact'),
			'address' => $this->input->post('address'),
			'status' => $this->input->post('status')
		);

		$query = $this->db->insert('venues',$user_data);
		if( $query > 0 ) {
			return true;
		} else {
			return false;
		}
			
	}
	
	function update_venue($id) {
		
		$user_data = array(
			'venue_name' => $this->input->post('vname'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'contact' => $this->input->post('contact'),
			'address' => $this->input->post('address'),
			'status' => $this->input->post('status')
		);
		
		$this->db->where('venue_id', $id);
		$query = $this->db->update('venues',$user_data);
		if( $query > 0 ) {
			return true;
		} else {
			return false;
		}
			
	}
	
	function delete( $id ) {
		$this->db->where('venue_id', $id);
		$query = $this->db->delete('venues'); 
		
		if( $query > 0 ) {
			return true;
		} else {
			return false;
		}
	}
	
	function search( $keyword )
    {
        $this->db->like('first_name',$keyword);
        $this->db->or_like('email',$keyword);
        $query = $this->db->get('venues');
        //echo $this->db->last_query(); die;
        if( $query->num_rows() > 0 ) {
			return $query->result_array();
		} else {
			return false;
		}
        
    }
    	
	function chkvname($vName,$uid=NULL)
    {
		$this->db->select('venue_name');
		$this->db->from('venues');
		$this->db->where('venue_name',$vName);
		if($uid){
			 $this->db->where_not_in('venue_id',$uid);
		}	
		$query=$this->db->get();
        return $query->num_rows();
	}
	
	function chkemail($email,$uid=NULL) {
        $this->db->select('email');
		$this->db->from('venues');
		$this->db->where('email',$email);
		if($uid){
			$this->db->where_not_in('venue_id',$uid);
		}
		$query=$this->db->get();
        return $query->num_rows();
	}
    
    function change_status (){
		$user_id  = $this->input->post('venue_id');
		
		$this->db->select('status');
		$this->db->where('venue_id', $user_id);
        $query = $this->db->get('venues');
        if( $query->num_rows() > 0 ) {
			$result =  $query->row_array();
			
			if( $result['status'] == 1 ) {
				$status = '0';
			} else {
				$status = '1';
			}
			
			$data = array(
				'status' => $status,
			);
			
			$this->db->where( 'venue_id', $user_id);
			$row = $this->db->update('venues',$data);
			if( $row > 0 ) {
				if( $status == 1 ) {
					$ret_var = 'active';
				} else {
					$ret_var =  'inactive';
				}
				return $ret_var;
			} else {
				return false;
			}
				
		} else {
			return false;
		}
        
    }
	
	
}

?>

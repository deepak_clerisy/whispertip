<?php
class Venue_model extends CI_Model {
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url'); 
		$this->load->library('email');    
		$this->load->library('session');    
	}
	
	
	public function random_code($number=5) {
		$alphabet = 'abcdefghijklmnopqrstuvwx#yzABCDEFGH$IJKLMNOPQRST@UVWXYZ1234567890';
		$pass = array();
		$alphaLength = strlen($alphabet) - 1;
		for ($i = 0; $i < $number; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass);
	}
	
	public function register_venue()
	{
		$code=$this->random_code(10);
		
		$password = $this->input->post('password');
			
		$data = array(
					'venue_name' => $this->input->post('name'),
					'address' => $this->input->post('address'),
					'phone' => $this->input->post('phone'),
					'contact' => $this->input->post('contact'),
					'email' => $this->input->post('email'),
					'email' => $this->input->post('email'),
					'password' => md5($password),
					'status' => '0',
					'verify_code' => $code,
					'verified' => '0',
					'create_date' => date('Y-m-d H:i:s')
					
			);
			
		$result =	$this->db->insert('venues',$data);	
		if($result)
		{
			
			/************* Register Email *********************************/
					$config['protocol'] = 'sendmail';
					$config['mailpath'] = '/usr/sbin/sendmail';
					$config['charset'] = 'iso-8859-1';
					$config['mailtype'] = 'html';
					$config['wordwrap'] = TRUE;
					
					$email = $this->input->post('email');
					$first_name = $this->input->post('name');
					
					$this->email->initialize($config);
					$this->email->from('info@whispertip.com', 'Whispertip');
					$this->email->to($email);
					$this->email->subject('Whispertip || Account created');
					$b_code=base64_encode($email.'&'.$code);  
					$url=base_url().'verify_venue/'.$b_code;  
					$this->email->message('<p>Hello '.$first_name.',</p>
						<p>Greetings for the day!! </p>
						<p>Thank You for registering with the Whispertip.com. For any kind of support you can contact site admin at admin@whispertip.com. Our executve will be happy to assist you.</p>
						<p>Please click here to <a href="'.$url.'">verify email</a>. </p>
						<p></p><br/>
						<p>Regards & Thanks,</p>
						<p>Team Whispertip</p>'
					);
					$this->email->send();
				/**************************************************************/
				
			return true;
		}
		else
		{
			return false;
		}
			
	}
	
	public function register_patron()
	{
		$code=$this->random_code(10);
		
		$password = $this->input->post('password');
			
		$data = array(
					'username' => $this->input->post('username'),
					'first_name' => $this->input->post('first_name'),
					'last_name' => $this->input->post('last_name'),
					'email' => $this->input->post('email'),
					'country' => $this->input->post('country'),
					'password' => md5($password),
					'status' => '0',
					'verification_key' => $code,
					'verified' => '0',
					'signup_date' => date('Y-m-d H:i:s')
					
			);
			
		$result =	$this->db->insert('users',$data);	
		if($result)
		{
				/************* Register Email *********************************/
					$config['protocol'] = 'sendmail';
					$config['mailpath'] = '/usr/sbin/sendmail';
					$config['charset'] = 'iso-8859-1';
					$config['mailtype'] = 'html';
					$config['wordwrap'] = TRUE;
					
					$email = $this->input->post('email');
					$first_name = $this->input->post('first_name');
					
					$this->email->initialize($config);
					$this->email->from('info@whispertip.com', 'Whispertip');
					$this->email->to($email);
					$this->email->subject('Whispertip || Account created');
					$b_code=base64_encode($email.'&'.$code);  
					$url=base_url().'verify_patron/'.$b_code;  
					$this->email->message('<p>Hello '.$first_name.',</p>
						<p>Greetings for the day!! </p>
						<p>Thank You for registering with the Whispertip.com. For any kind of support you can contact site admin at admin@whispertip.com. Our executve will be happy to assist you.</p>
						<p>Please click here to <a href="'.$url.'">verify email</a>. </p>
						<p></p><br/>
						<p>Regards & Thanks,</p>
						<p>Team Whispertip</p>'
					);
					$this->email->send();
				/**************************************************************/
		
			return true;
		}
		else
		{
			return false;
		}
			
	}
	
	public function login_venue()
	{
		$email=$this->input->post('email');
		$password=$this->input->post('password');
		$rem_me = $this->input->post('remember_me');
		
		if($rem_me)
		{
			$cookie_username = "v_username";
			$cookie_password = "v_password";
			setcookie($cookie_username, $email, time() + (86400 * 30), "/"); // 86400 = 1 day
			setcookie($cookie_password, $password, time() + (86400 * 30), "/"); // 86400 = 1 day
		}
		else
		{
			$cookie_username = "v_username";
			$cookie_password = "v_password";
			setcookie($cookie_username, '', time() - (86400 * 30), "/"); // 86400 = 1 day
			setcookie($cookie_password, '', time() - (86400 * 30), "/"); // 86400 = 1 day
		}
		
		$this->db->select('*');
		$this->db->where('email', $email);
		$this->db->where('password', md5($password));
		//$this->db->where('status', '1');
		//$this->db->where('verified','1');
		$this->db->from('venues');
		$query = $this->db->get();
		$result=$query->row_array();
		//echo '<pre>';print_r($result);die;
		
		if(!empty($result))
		{
			if($result['verified'] !='1')
			{
				return 'not_verified';
				exit;
			}
			else if($result['status'] !='1')
			{
				return 'not_active';
				exit;
			}
			else
			{
				$venue_data = array(
							'venueId'   => $result['venue_id'],
							'venue_email'   => $result['email'],
							'venue_logged_in' => TRUE,
							'user_logged_as' => 'venue'
							);
						//print_r($result[0]->language);die;
				$this->session->set_userdata($venue_data);
				return true;	
			
			}
		}
		else
		{
			return false;
		}
		
		
	}
	
	public function login_patron()
	{
		$email=$this->input->post('email');
		$password=$this->input->post('password');
		$rem_me = $this->input->post('remember_me');
		
		if($rem_me)
		{
			$cookie_username = "p_username";
			$cookie_password = "p_password";
			setcookie($cookie_username, $email, time() + (86400 * 30), "/"); // 86400 = 1 day
			setcookie($cookie_password, $password, time() + (86400 * 30), "/"); // 86400 = 1 day
		}
		else
		{
			$cookie_username = "p_username";
			$cookie_password = "p_password";
			setcookie($cookie_username, '', time() - (86400 * 30), "/"); // 86400 = 1 day
			setcookie($cookie_password, '', time() - (86400 * 30), "/"); // 86400 = 1 day
		}
		
		$this->db->select('*');
		$this->db->where('email', $email);
		$this->db->or_where('username', $email);
		$this->db->where('password', md5($password));
		//$this->db->where('status', '1');
		//$this->db->where('verified','1');
		$this->db->from('users');
		$query = $this->db->get();
		$result=$query->row_array();
		
		if(!empty($result))
		{
			if($result['verified'] !='1')
			{
				return 'not_verified';
				exit;
			}
			else if($result['status'] !='1')
			{
				return 'not_active';
				exit;
			}
			else
			{
			
				$user_data = array(
							'useremail'   => $result['email'],
							'userid'   => $result['id'],
							'username'   => $result['username'],
							'user_loggedin' => TRUE,
							'user_logged_as' => 'patron'
							);
				$this->session->set_userdata($user_data);
				return true;	
			
			}
		}
		else
		{
			return false;
		}
		
		
	}
	
	public function check_email_exist($email)
	{	
		$this->db->select('email');
		$this->db->from('venues');
		$this->db->where('email',$email);
		$query = $this->db->get();
		return $query->num_rows();
	}
	
	public function check_patron_email_exist($email)
	{	
		$this->db->select('email');
		$this->db->from('users');
		$this->db->where('email',$email);
		$query = $this->db->get();
		return $query->num_rows();
	}
	
	public function check_patron_username_exist($email)
	{	
		$this->db->select('username');
		$this->db->from('users');
		$this->db->where('username',$email);
		$query = $this->db->get();
		return $query->num_rows();
	}
	
	public function get_logged_venue_details( $venue_id ) {
		$this->db->select('*');
		$this->db->from('venues');
		$this->db->where('venue_id', $venue_id);
		$query = $this->db->get();
		if( $query->num_rows() > 0 ) {
			return $query->row_array();
		} else {
			return false;
		}
	}
	
	public function check_venue_email($email)
	{	
		$this->db->select('*');
		$this->db->from('venues');
		$this->db->where('email',$email);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			$response = $query->row_array();
			return $response;
		}
		else
		{
			return false;
		}
	}
	
	public function update_venue_data() {
		$venue_id = $this->session->userdata('venueId');
		$data = array(
			'venue_name' => $this->input->post('venue_name'),
			'address' => $this->input->post('address'),
			'phone' => $this->input->post('phone'),
			'contact' => $this->input->post('contact'),
			'email' => $this->input->post('email'),
		);
		
		$this->db->where('venue_id',$venue_id);
		$result = $this->db->update('venues',$data);
		if( $result > 0 ) {
			return true;
		} else {
			return false;
		}
	}
	
	public function update_venue_Password() {
		$venue_id = $this->session->userdata('venueId');
		
		$current_pass = md5( $this->input->post('current_password'));
		
		$this->db->select('password');
		$this->db->where('venue_id',$venue_id);
		$query = $this->db->get('venues');
		if($query->num_rows())
		{
			$result = $query->row_array();
			if( $current_pass == $result['password'] ) {
				$data = array(
					'password' => md5($this->input->post('new_password')),
					);
				$this->db->where('venue_id',$venue_id);
				$res = $this->db->update('venues',$data);
				if($res > 0 ) {
					return true;
				}
			} else {
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	public function get_venue_list() {
		$this->db->select('venue_id,venue_name');
		$this->db->where('status', '1' );
		$query = $this->db->get('venues');
		if( $query->num_rows() > 0 ) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	
	
	
}


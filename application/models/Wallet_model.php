<?php

class Wallet_model extends Model
{
	function Wallet_model()
	{
		parent::Model();
		$this->CI = & get_instance();
		$this->load->library('email');
	}
	
	function getUserWallet($userid)
	{
		$this->db->select('*');
		$this->db->where('userid',$userid);
		$query = $this->db->get('user_wallet');
		if($query->num_rows() >0)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}
	function withdrawFromWallet($amount,$email)
	{
		$userid = $this->session->userdata('userid');
		$wallet_data = $this->wallet_model->getUserWallet($userid);
		$wallet_amount = $wallet_data['balance'];
		$unique_val = $this->session->userdata('unique_value');
		$total_amount = $wallet_amount-$amount;
		$upd_array= array('balance'=>$total_amount);
		
		$this->db->where('userid',$userid);
		$query = $this->db->update('user_wallet',$upd_array);
		if($query)
		{
			$arrIns = array('userid'=>$userid,
						'amount'=>$amount,
						'reciever_email'=>$email,
						'transaction_id'=>'',
						'transaction_type'=>'debit',
						'transaction_status'=>1,
						'payment_type'=>'physical',
						'type'=>'MassPayment',
						'mass_payment_unique_id'=>$unique_val,
						'transaction_datetime'=>date('Y-m-d H:i:s'));
			$succQuery = $this->db->insert('user_transactions',$arrIns);
			if($succQuery){
				$this->session->unset_userdata('unique_value');
				return true;
			}
		}
		else
		{
			$this->session->unset_userdata('unique_value');
			return false;
		}
	}
	function addInWallet($amount,$transactionId,$type)
	{
		$userid = $this->session->userdata('userid');
		$wallet_data = $this->wallet_model->getUserWallet($userid);
		$wallet_amount = $wallet_data['balance'];
		
		$total_amount = $wallet_amount+$amount;
		$upd_array= array('balance'=>$total_amount);
		
		$this->db->where('userid',$userid);
		$query = $this->db->update('user_wallet',$upd_array);
		if($query)
		{
			$arrIns = array('userid'=>$userid,
						'amount'=>$amount,
						'transaction_id'=>$transactionId,
						'transaction_type'=>'credit',
						'transaction_status'=>1,
						'payment_type'=>'physical',
						'type'=>$type,
						'transaction_datetime'=>date('Y-m-d H:i:s'));
			$succQuery = $this->db->insert('user_transactions',$arrIns);
			if($succQuery){
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
}
?>

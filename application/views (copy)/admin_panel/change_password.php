<div style="color:#FF0000"><?php echo validation_errors(); ?></div>
<form name="frmChangePass" id="frmChangePass" action="" method="post">
	<div class="account-right-div">
		<div class="dashboard-heading"><h2><?php echo $page_title; ?></h2></div>
		<div class="dashboard-inner change_password">
			<div class="main-dash-summry Edit-profile">
				<div class="input-row">
					<div class="full">
						<div class="input-block">
							<label>Current Password :</label>
							<span class='reg_span'><input type="password" name="old_pwd" value="" class="inputbox-main"></span>
						</div>
					</div>
				</div>
				<div class="input-row">
					<div class="full">
						<div class="input-block">
							<label>New Password :</label>
							<span class='reg_span'><input type="password" id="password" class="inputbox-main" name="new_pwd" value=""></span>
						</div>
					</div>
				</div>
				<div class="input-row">
					<div class="full">
						<div class="input-block">
							<label>Confirm Password :  </label>
							<span class='reg_span'><input type="password" id="con_pwd" class="inputbox-main" name="con_pwd" value=""></span>
						</div>
					</div>
				</div>
				<div class="input-row">
					<div class="full">
						<div class="input-block">
							<span class='reg_span reg_span_btn'><input type="submit" value="Submit" class="btn-submit btn">
							<input type="button" value="Cancel" onclick="cancelButton()" class="btn-submit btn">
							 </span>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
	$(function() {
		$("#frmChangePass").validate({
			rules: {
				old_pwd: "required",
				new_pwd: {
					  required: true,
					  minlength: 5
			    },
				con_pwd: {
						equalTo: "#password"
					},
			},
			messages: {
				old_pwd: "Please enter current password.",
				new_pwd: {
					  required: "Please enter new password.",
					  minlength: "Your password must be at least 5 characters long."
			    },
				con_pwd:"New password and confirm password should be same.",
			},
			errorElement:"div",
			errorClass:"valid-error",
			submitHandler: function(form) {
				form.submit();
			}
		});
	});
	  
	function cancelButton()  {
		location.assign("<?php echo $this->config->item('admin_url'); ?>/home");
	} 
</script>
	
<style>
.radio_btn { width: 26% !important; }
</style>

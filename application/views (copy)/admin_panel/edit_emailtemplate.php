<div id="box">
<h3><?php print 'Edit Email template';//@$section_heading; ?>

</h3>
<br>
<script language="JavaScript" src="<?php print base_url();?>assets/js/gen_validatorv31.js" type="text/javascript"></script>
<style>
.table,td,tr
{
	border:none;
}
.redtext
{
 color:#FF0000;
}
</style>

<div class="yui-skin-sam">
<form name="pageeditform" method="post" action="">
<div class="redtext"><?php echo validation_errors(); ?></div>


<table width="100%" border="0" cellspacing="12">
<tr>
    <td><b>Template Name:</b>&nbsp;<em class="redtext">*</em></td>
    <td>
        <input class="textbox" type="text" name="template_name" disabled id="templatename" value="<?php echo @$emailtemplate['template_name']; ?>">
    </td>
  </tr>
  <tr>
    <td><b>Subject:</b>&nbsp;<em class="redtext">*</em></td>
    <td>
        <input class="textbox" type="text" name="subject" id="pagetitle" value="<?php print @$emailtemplate['subject'];?>">
    </td>
  </tr>
  <tr>
    <td valign="top"><b>From:</b>&nbsp;<em class="redtext">*</em></td>
    <td>
    <input class="textbox" type="text" name="from" id="pagesubtitle" value="<?php print @$emailtemplate['from'];?>"/>
      </td>
  </tr>

  <tr>
    <td valign="top"><b>Insert Variable:</b></td>
    <td>
    	<select name="template_variables" id="template_variables" onchange="InsertVariable()" class="selectbox">
        	<option value="">--Select--</option>
            <?php
				if(count($template_variables))
				{ 
					foreach($template_variables as $vars)
					{
						print '<option value="'.$vars['variable_value'].'">'.$vars['variable_name'].'</option>';
					}
				}
			?>
    	</select>
    </td>
  </tr>
  
  <tr>
  <td valign="top"><b>Email Template Body:</b></td>
  <td>
  
		<div>
		<?php
		if(strstr($_SERVER['HTTP_HOST'], 'whispertip/beta'))
		{
			$server = $_SERVER['DOCUMENT_ROOT'];
		}
		else
		{
			$server = FCPATH;
		}
		$this->load->file($server."/assets/ckeditor/ckeditor.php",true);
		$ckeditor = new CKEditor();
		$ckeditor->basePath = base_url().'assets/ckeditor/';
		$ckeditor->config['width'] = 600;
		$ckeditor->editor('content1',$this->input->get_value('content1',$emailtemplate['body'],''));
		?>
		</div>	
  
  
  </td>
  </tr>
   
 <tr>
  <td><b>Active:</b></td>
  <td><label>
  <?php $chk= @$emailtemplate['active'];?>
    <input class="checkbox" type="checkbox" name="active" id="active"  value="1" <?php if($chk == 1) print 'checked="checked"';?>/>
  </label></td>
  </tr>
  
  <tr>
  <td></td>
  <td  colspan="3">
    <input type="submit"  value="Edit" class="button" style="width:55px;"/>&nbsp;&nbsp;
    <input  type="hidden" name="editemailtemptale" id="editemailtemptale" value="editemailtemptale" />
    &nbsp;&nbsp;
    <input class="button"  type="button" name="btncancel" id="btncancel" value="Cancel" style="width:65px;"
     onClick="javascript:document.location.href='<?php echo base_url();?>admin_panel/email/index';"/>
    
  </td>
  </tr>
 </table>
</form>
</div>

</div>
<script type="text/javascript">
	
var frmvalidator = new Validator("pageeditform");
frmvalidator.addValidation("templatename","req","<?php print $this->lang->line('validation_required', $this->lang->line('admin_templatename'));?>");
frmvalidator.addValidation("pagetitle","req","Field subject is requried.");
frmvalidator.addValidation("pagesubtitle","req","Field from is requried.");

</script>
<script type="text/javascript">

function InsertVariable()
{
	var vardd = document.getElementById("template_variables");
	var variable_code = "<span>"+vardd.options[vardd.selectedIndex].value+"</span>";
	var oEditor = CKEDITOR.instances.content1;
	var newElement = CKEDITOR.dom.element.createFromHtml( variable_code, oEditor.document );
	oEditor.insertElement( newElement );
}
</script>						

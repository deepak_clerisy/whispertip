<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<title><?php print $page_title; ?></title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/admin/style.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/admin/font-awesome.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/admin/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/admin/jquery.datetimepicker.css"/>
<script language="JavaScript" src="<?php print base_url();?>assets/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script language="JavaScript" src="<?php print base_url();?>assets/js/gen_validatorv31.js" type="text/javascript"></script>
<script language="JavaScript" src="<?php print base_url();?>assets/js/jquery.validate.js" type="text/javascript"></script>
<script language="JavaScript" src="<?php print base_url();?>assets/js/jquery.datetimepicker.full.js" type="text/javascript"></script>

<!--
<script type="text/javascript" src="<?php print base_url();?>assets/js/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="<?php print base_url();?>assets/js/yui/build/container/container-min.js"></script>
<script type="text/javascript" src="<?php print base_url();?>assets/js/yui/build/animation/animation-min.js"></script>
<script type="text/javascript" src="<?php print base_url();?>assets/js/yui/build/calendar/calendar-min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php print base_url();?>assets/js/yui/build/calendar/assets/skins/sam/calendar.css" />
-->
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="<?php print base_url();?>assets/css/admin/css/ie-sucks.css" />
<![endif]-->

<?php if(isset($extra_head_content)) print $extra_head_content;?>
<style>

</style>
</head>

<body <?php if(isset($bodyclass)) print $bodyclass;?>>
	<header>
		<div id="wrapper2">
			<div class="header-base">
				<div class="logo-div">
					<a href ="<?php echo base_url();?>admin_panel/home"><img src="<?php echo base_url(); ?>assets/images/logo.png"/></a>
				</div>
				<div class="header-right-main">
					<nav  class="dashboard-nav">
						<div class="welcometxt"> 
							<h2><span>
							<?php
								$url=base_url().'blog/automatic_login';
								if( $this->authorize->is_admin_logged_in() == true)
								print $this->lang->line('admin_welcome')." ". $this->session->userdata('admin_username') . ' &nbsp;|&nbsp;<a style="color:#FFFFFF; text-decoration:none;" href="'.base_url().'admin_panel/login/logout">'.$this->lang->line('admin_logout').'</a><br>';
								print '<a style="color:#FFFFFF; text-decoration:none;" href="'.$url.'"><span>'.$this->lang->line('admin_automatic_blog_login').'</span></a>'; 
							?>
							</span></h2>
						</div>					
					</nav>
					<div class="navi-outer">
						<ul>
							<li><a href="<?php echo $this->config->item('admin_url'); ?>/home"> Dashboard </a> </li>
							<!-- li><a href="<?php echo $this->config->item('admin_url'); ?>/users"> Manage Users </a></li -->
							<li id="users"><a>Manage Users <i class="fa fa-angle-down dp-arrow"></i></a>
								<ul class="users-child nav-child" style="display:none">
									<li><a href="<?php echo $this->config->item('admin_url'); ?>/users">Patrons</a></li>
									<li><a href="<?php echo $this->config->item('admin_url'); ?>/venues">Venues</a></li>
								</ul>
							</li>
							<li> <a href="<?php echo $this->config->item('admin_url'); ?>/competitions">Manage Competitions</a></li>
							<li> <a href="<?php echo $this->config->item('admin_url'); ?>/contents">CMS</a></li>
							<!-- li> <a  href="<?php echo $this->config->item('admin_url'); ?>/category">Category</a> </li>
							<li> <a  href="<?php echo $this->config->item('admin_url'); ?>/results">Results</a> </li>
							<li> <a  href="<?php echo $this->config->item('admin_url'); ?>/email">Email</a> </li>
							<li><a href="#">Email</a> </li -->
							<li id="settings"><a>Settings <i class="fa fa-angle-down dp-arrow"></i></a>
							<ul class="settings-child nav-child" style="display:none">
								<li><a href="<?php echo $this->config->item('admin_url'); ?>/home/change_password"> Change Password  </a></li>
							</ul>
						</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="border-div"></div>	
	<div class="clr"></div>
	<div class="regi-main">
		<div id="wrapper2">
			<div id="container">
				<div id="header">
					<!-- Main Content Starts Here -->
					<div id="content">
						<?php
						if(isset($error))
						{
							print '
							<div id="message-red" style="display:block">
								<table cellspacing="0" cellpadding="0" style="width:600px;border:0px;">
								<tr>
									<td class="red-left" style="border:0px;">'.$error.'</td>
									<td class="red-right" style="padding:0px;border:0px;">
									<a class="close-red" onclick="CloseMessage()"><img alt="" src="'.base_url().'assets/css/admin/img/close_red.png" align="left" style="margin:0px;"></a>
									</td>
								</tr>
								</table>
							</div>';
							
						}
						if(isset($success))
						{
							print '
							<div id="message-green" style="display:block" >
								<table cellspacing="0" cellpadding="0" style="width:600px;border:0px;">
								<tr>
									<td class="green-left" style="border:0px;">'.$success.'</td>
									<td class="green-right" style="padding:0px;border:0px;">
									<a class="close-green" onclick="CloseMessage()"><img alt="" src="'.base_url().'assets/css/admin/img/close_green.png" align="left" style="margin:0px;"></a>
									</td>
								</tr>
								</table>
							</div>';  
							
						}
						if(isset($message))
						{
							print '
							<div id="message-yellow" style="display:block">
								<table cellspacing="0" cellpadding="0" style="width:600px;border:0px;">
								<tr>
									<td class="yellow-left" style="border:0px;">'.$success.'</td>
									<td class="yellow-right" style="padding:0px;border:0px;">
									<a class="close-yellow" onclick="CloseMessage()"><img alt="" src="'.base_url().'assets/css/admin/img/close_yellow.png" align="left" style="margin:0px;"></a>
									</td>
								</tr>
								</table>
							</div>';  
							
						}
						?>
						<?php if(isset($page_body)) print $page_body; ?>
					</div>
					<!-- Main Content Ends Here -->
				</div>	</div>	</div>	</div>

<footer>
			<div class="wrapper footer-base">
				<p>        &copy; whispertip.com. All rights reserved </p>
			</div>
		</footer>
				
 

<script>
	/*$(".nav li").on("click", function() {
		$(".navi-outer ul li a").removeClass("active");
		$(this).addClass("active");
    }); */
    $(function() {
		/*$("#date").datepicker({
		dateFormat: "yy-mm-dd",
		});
		*/
		$.datetimepicker.setLocale('en');
		$('#dob').datetimepicker({
			timepicker:false,
			format:'Y-m-d',
			formatDate:'yy-mm-dd',
			maxDate: new Date(),//'-1970/01/02', // yesterday is minimum date
			//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
		});
		$('#date').datetimepicker({
			timepicker:false,
			format:'Y-m-d',
			formatDate:'yy-mm-dd',
			minDate: new Date(),//'-1970/01/02', // yesterday is minimum date
			//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
		});
		$('#time').datetimepicker({
			datepicker:false,
			format:'H:i:s',      
		});
	});
	
	$(function() {
		var pgurl = window.location.href.substr(window.location.href.lastIndexOf("/")+1);
		if(pgurl == 'change_password'){
			$('.navi-outer ul li a').removeClass('active');
			$('#settings a').addClass('active');
			
		}
		if(pgurl == 'users' || pgurl == 'add'){
			$('#manage_user a').addClass('active');
		}
	});

	$('#settings').click(function(){
		$('.settings-child').toggle();
	});

	$('#users').click(function(){
		$('.users-child').toggle();
	});
function CloseMessage()
{
	var divs = document.getElementsByTagName("div");
	for(var i=0;i<divs.length;i++)
	{
		if( divs[i].id.substr(0,8)=="message-")
		{
			divs[i].style.display="none";
		}
	}
}

<?php if( isset($error) || isset($message) || isset($success)) {
	print 'var t=setTimeout("CloseMessage()",10000);';
}
?>
</script>
</body>
</html>

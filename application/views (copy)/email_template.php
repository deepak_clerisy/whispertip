<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd
html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<!-- Facebook sharing information tags -->
<meta property="og:title" content="*|MC:SUBJECT|*" />

<title>*|MC:SUBJECT|*</title>
<style type="text/css">
body{ }
#templatePreheader{}
#templateContainer{}
a{}
#templateBody{}
span{}

 @media only screen and (max-width: 600px) {
        body,table,td,p,a,li,blockquote {
        -webkit-text-size-adjust:none !important;
        }
        table {width: 100% !important;}
        .responsive-image img {
        height: auto !important;
        max-width: 100% !important;
        width: 100% !important;
        }
    }
</style>
</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="background:#eeeeee; font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:10px;">
<center>
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable">
<tr>
<td align="center" valign="top">
<!-- // Begin Template Preheader \\ -->
<table border="0" cellpadding="10" cellspacing="0" width="100%" id="templatePreheader" style='background:#dcdcdc; border-bottom:2px dotted #fff;border-radius:10px 10px 0px 0px;-moz-border-radius:10px 10px 0px 0px;-webkit-border-radius:10px 10px 0px 0px;'>
<tr>
<td valign="top" class="preheaderContent">
<center><img src="http://whispertip.com/assets/images/sports_logo.png" width="200"/></img></center>
</td>
</tr>
</table>
<!-- // End Template Preheader \\ -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateContainer" style="background:#ffffff;border-radius:0px 0px 10px 10px;-moz-border-radius:0px 0px 10px 10px;-webkit-border-radius:0px 0px 10px 10px;">
<tr>
<td align="center" valign="top">
<!-- // Begin Template Body \\ -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody" style="border-bottom:1px solid #ddd;">
<tr>
<td colspan="3" valign="top" class="bodyContent">

<!-- // Begin Module: Standard Content \\ -->
<table border="0" cellpadding="20" cellspacing="0" width="100%">
<tr>
<td valign="top">
<div mc:edit="std_content00">
<?php echo $template_body;?>

</div>
</td>
</tr>
</table>
<!-- // End Module: Standard Content \\ -->

</td>
</tr>

</table>
<!-- // End Template Body \\ -->
</td>
</tr>
<tr>
<td align="center" valign="top">
<!-- // Begin Template Footer \\ -->
<table border="0" cellpadding="10" cellspacing="0" width="550" id="templateFooter">
<tr>
<td valign="top" class="footerContent">

<!-- // Begin Module: Standard Footer \\ -->
<table border="0" cellpadding="10" cellspacing="0" width="100%">
<tr>
	<td style=" width: 96px; font-size:14px;">Follow Us On: <a href="http://instagram.com/whispertip" target="_blank">
	<img src="http://whispertip.com/assets/images/icon_instagram.png" style="margin-left: 5px; vertical-align: bottom;"></a>
	<a href="http://twitter.com/whispertip" target="_blank"><img src="http://whispertip.com/assets/images/icon_tweet.png" style="margin-left: 5px; vertical-align: bottom;"></a>
	<a href="https://facebook.com/whispertip" target="_blank"><img src="http://whispertip.com/assets/images/icon_facebook.png" style="margin-left: 5px; vertical-align: bottom;"></a></td>
	
</tr>
</table><!-- // End Module: Standard Footer \\ -->

</td>
</tr>
</table>
<!-- // End Template Footer \\ -->
</td>
</tr>
</table>
<br />
</td>
</tr>
</table>
</center>
</body>
</html>

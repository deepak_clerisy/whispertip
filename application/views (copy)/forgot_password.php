<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Forgot Password</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<script src="js/html5.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style_new.css">
	<script language="JavaScript" src="<?php print base_url();?>assets/js/jquery-1.7.1.min.js" type="text/javascript"></script>
	<script language="JavaScript" src="<?php print base_url();?>assets/js/gen_validatorv31.js" type="text/javascript"></script>
	<script language="JavaScript" src="<?php print base_url();?>assets/js/jquery.validate.js" type="text/javascript"></script>        
	<script src="<?php echo base_url() ?>assets/js/new_design/bootstrap.min.js"></script>
	
	<script>
		$(document).ready(function() {
			$("#loginfm").validate({
				rules: {
					email: {
						required: true,
						email:true
					}
					
				},
				messages: {
					email: {
						required: "Please enter email.",
						email: "Please enter valid email.",
						
					}
				},
				errorElement:"div",
				errorClass:"login-error",
				submitHandler: function(form) {
					form.submit();
				}
			});
		});
	</script>
</head>
<body class="bg_image">
<div class="home">
	<div class="container">
		<div class="login_outer">
			<center><img src="<?php echo base_url() ?>assets/images/login_logo.png"/></center>
			<?php if($this->session->flashdata('fail'))
			{?> 			
			<div id="userSignUpError" class="entry_row userSignUpError"><?php echo $this->session->flashdata('fail');?></div>			
			<?php } ?>
			<?php if($this->session->flashdata('success'))
			{?> 
			<div id="userSignUpok" class="entry_row userSignUpok"> <?php echo $this->session->flashdata('success');?> </div>
			<?php } ?>
						 
			<div class="login">
				<div id="userLoginError" class="entry_row" style="margin-bottom:4px;color:red;"></div>
					<form name="loginfm" id="loginfm" method="post">
						<input type="text" name="email" id="email"  placeholder="Email" />
						<!-- input id="lgloginBtn" name="submit" type="submit" class="btn login" value="Login"/ -->
						<input type="submit" class="login_btn" style="float:left;width:76px;" value="Forgot">
						<a href="<?php echo base_url(); ?>" class="login_btn" id="reg_cancel" style="float:left;width:76px;margin-left:60px;" name="cancelButtn">Cancel</a>
						<a href="<?php echo base_url(); ?>login" class="pull-right forgot">Login</a>
					</form>
				<div class="social_support" style="margin-top:200px;">
					Social sports Tipping EXCHANGE
				</div>
				<div class="social_support" style="font-family: 'tradegothicbold_condensed';">
					GET AHEAD OF THE GAME
				</div>

			</div>
		</div>
	</div>
</div>
</body>
</html>

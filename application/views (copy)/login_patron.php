﻿<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Login</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<script src="js/html5.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style_new.css">
	<script language="JavaScript" src="<?php print base_url();?>assets/js/jquery-1.7.1.min.js" type="text/javascript"></script>
	<script language="JavaScript" src="<?php print base_url();?>assets/js/gen_validatorv31.js" type="text/javascript"></script>
	<script language="JavaScript" src="<?php print base_url();?>assets/js/jquery.validate.js" type="text/javascript"></script>        
	<script src="<?php echo base_url() ?>assets/js/new_design/bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/miscellaneous.js"></script>
	<script>
		$(document).ready(function() {
			$("#loginfm").validate({
				rules: {
					email: {
						required: true,
					},
					password: {
						required: true,
						
					},
				},
				messages: {
					email: {
						required: "Please enter username or email.",
						
					},
					password: {
						required: "Please enter password.",
					},
				},
				errorElement:"div",
				errorClass:"login-error",
				submitHandler: function(form) {
					form.submit();
				}
			});
		});
	</script>
</head>
<body class="bg_image">
<div class="home">
	<div class="container">
		<div class="login_outer login_outer-sp">
			<center><img src="<?php echo base_url() ?>assets/images/login_logo.png"/></center>
			
			
			 <?php 
					 if(@$_COOKIE["p_username"] !=''){
						$user_name=$_COOKIE["p_username"];
					 } else{
						 $user_name="";
						 }
						 
					if(@$_COOKIE["p_password"] !=''){
						$pass_word=$_COOKIE["p_password"];
					 } else{
						 $pass_word="";
						 }	 
						 
				?>
						 
			<div class="login">
				<?php if($this->session->flashdata('fail'))
				{ ?> 			
					<div id="userSignUpError" class="entry_row userSignUpError"><?php echo $this->session->flashdata('fail');?></div>			
				<?php } ?>
					<?php if($this->session->flashdata('success'))
				{ ?> 
					<div id="userSignUpok" class="entry_row userSignUpok"> <?php echo $this->session->flashdata('success');?> </div>
				<?php } ?>
				
				<div id="userLoginError" class="entry_row" style="margin-bottom:4px;color:red;"></div>
					<form name="loginfm" id="loginfm" method="post">
						<div class="input-block">
							<a href="<?php echo base_url()?>facebook" class="login_btn f_btn" name="facebook">Log in with facebook</a>
							
						</div>
						<div class="input-block">
							<a href="<?php echo base_url()?>twitter" class="login_btn t_btn" name="twitter">Log in with twitter</a>
						</div>
						<div class="input-block">
							<a href="<?php echo base_url()?>google" class="login_btn g_btn" name="twitter">Log in with Google plus</a>
						</div>
						<div class="or-block"><div class="or-main">OR</div></div>
						<div class="input-block input-height">
							<input type="text" name="email" id="email" value="<?php echo $user_name; ?>" placeholder="User Name or Email" />
						</div>
						<div class="input-block input-height">
							<input type="password" name="password" id="password" value="<?php echo $pass_word; ?>" placeholder="Password" />
						</div>
						
						<!-- input id="lgloginBtn" name="submit" type="submit" class="btn login" value="Login"/ -->
						<input type="submit" class="login_btn" style="float:left;width:76px;" value="login">
						<a href="#" class="login_btn" data-uri="<?php echo base_url(); ?>" id="reg_cancel" style="float:left;width:76px;margin-left:60px;" name="cancelButtn">Cancel</a>
						<input name="remember_me" <?php if($user_name !='') { echo 'checked'; } ?> type="checkbox" style="display:inline-block; float:left;margin-top:6px;"/><span>Remember me</span>
						<a href="<?php echo base_url(); ?>forgot" class="pull-right forgot">Forgot your password?</a>
						<input type="hidden" value="<?php echo base_url();?>" id="url" name="url">
					</form>
				<div class="social_support" style="margin-top:200px;">
					Social sports Tipping EXCHANGE
				</div>
				<div class="social_support" style="font-family: 'tradegothicbold_condensed';">
					GET AHEAD OF THE GAME
				</div>

			</div>
		</div>
	</div>
</div>
</body>
</html>

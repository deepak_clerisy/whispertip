<div class="rugby blue">
	<div class="container">
		<div class="main_outer">
		<div class="content">
			<center><img class='inner-pages-image' src="<?php echo base_url().'assets/images/logo.png'; ?>"/></center>		
			<div class="sort"></div>
		
			<?php echo $sidebar; ?>
			
			<div class="right_content" id="page_content">
				<div class="blog coach_blog">
					<div class="but_tip_coach">
					<form id="select_venue_frm" name="select_venue_frm" method="post" action="">
						<div class="widthdraw">
							<div class="widthdraw_heading">Select Venue :</div>
							
							<input type="hidden" value="<?php echo $userData['id']; ?>" id="user_id" />
							<?php
							$dropdown = array();
								if( !empty($venue_list) ) {
									$dropdown = "<select name='main_venue' id ='main_venue' >";
									$dropdown .= "<option value=''>Select Venue</option>";
									foreach( $venue_list as $venues ) {
										$sel_venue = ($venues['venue_id'] == $userData['main_venue'] ) ? 'selected' : '';
										$dropdown .= "<option value='".$venues['venue_id']."' ".$sel_venue.">".$venues['venue_name']."</option>";
									}
									$dropdown .= "</select>";
								} 
							?>
							<div class="profile_form">
								<label>Select Venue</label>
								<div class="profile_input">
									<?php echo $dropdown; ?>
								</div>
							</div>
							
							<div class="btn_profile">
								<input type="submit" class="buy_tip pull-left submit-class" style="margin-top:5px;margin-top:6px;" value="Save Changes" />
							</div>
						</div>
					</form>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>	
</div>

<script>
$(function() {
	$("#select_venue_frm").validate({
		rules: {
			main_venue: "required",
		},
		messages: {
			main_venue: "Please select venue.",
		},
			errorElement:"div",
			errorClass:"valid-error",
			submitHandler: function(form) {
				form.submit();
			}
		});
	});
</script>

<div class="left_section">
            	<div class="user_detail">
                	<center>
						<?php 
						if((strstr($userData['profile_image'],'http://')==$userData['profile_image'] || strstr($userData['profile_image'],'https://')==$userData['profile_image']) && $userData['profile_image']!='')
							{
								echo "<img src='".str_replace('_normal','',$userData['profile_image'])."' height='125' width='118'/>";
							}
							else
							{
								if($userData['profile_image']!='' && file_exists('uploads/profile_picture/thumb_'.$userData['profile_image']))
								{
									echo "<img src='".base_url().'uploads/profile_picture/thumb_'.$userData['profile_image']."' />";
								}
								else
								{
									echo "<img src='".base_url().'assets/images/pro_img.png'."'>";
								}
							}
						?>
						
					</center>
                    <h1><?php echo $userData['first_name'].' '.$userData['last_name']; ?></h1>
                    <span>@<?php echo $userData['username'];?><?php ?></span>
                    <p>-<?php echo $userData['email']; ?></p>
                </div>
                
                
				<?php
					if($userData['twitter_token'])
					{
						$twitterdata = $this->session->userdata('twitterconnectsess'); 
                ?>
					<div class="left_section_footer">
						<div class="follow">
							<span>
								<?php 
									if(isset($twitterdata['statuses_count']))
									echo $twitterdata['statuses_count'];
								?>
							</span>
							<p>TWEET</p>
						</div>
						<div class="follow">
							<span>
								<?php 
									if(isset($twitterdata['followers_count']))
									echo $twitterdata['followers_count'];
								?>
							</span>
							<p>FOLLOWERS</p>
						</div>
						<div class="follow">
							<span>
								<?php 
									if(isset($twitterdata['friends_count']))
									echo $twitterdata['friends_count'];
								?>
							</span>
							<p>FOLLOWING</p>
						</div>
					</div>
                 <?php 
					}
					else
					{
						echo "<div class='left_section_footer' style='padding-bottom:5px;'>
								<div class='follow' style='width:414px;'>
									<span style='float:left;margin-top:10px;font: normal 17px sans-serif;margin-right:10px;'>
										Not connected with Twitter
									</span>
									<img id='connectTwitter' src='".base_url()."assets/images/connect.png' style='cursor:pointer;'>
								</div>
							</div>";
					}
                 ?>
                </div>
            </div>

﻿<!DOCTYPE html>
<html lang="en">
<head>
	<title>Registration</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<script src="js/html5.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style_new.css">
	<script language="JavaScript" src="<?php print base_url();?>assets/js/jquery-1.7.1.min.js" type="text/javascript"></script>
	<script language="JavaScript" src="<?php print base_url();?>assets/js/gen_validatorv31.js" type="text/javascript"></script>
	<script language="JavaScript" src="<?php print base_url();?>assets/js/jquery.validate.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>assets/js/new_design/bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/miscellaneous.js"></script>
	<script>
		$(document).ready(function() {
			$("#signup-form").validate({
				rules: {
					userName: {
						required: true,
					},
					password: {
						required: true,
						
					},
				},
				messages: {
					userName: {
						required: "Please enter password.",
						
					},
					username: {
						required: "Please enter username.",
					},
				},
				 errorElement:"div",
				errorClass:"login-error",
				submitHandler: function(form) {
					form.submit();
				}
			});
		});
	</script>
</head>
<body class="bg_image">
<div class="home">
	<div class="container">
		<div class="login_outer">
			<center><img src="<?php echo base_url() ?>assets/images/login_logo.png"/></center>
			<div id="userSignUpError" class="entry_row userSignUpError" style="display:none;"></div>
			<div id="userSignUpok" class="entry_row userSignUpok" style="display:none;"></div>
			<div class="login sign-fields">
				<form method="post" action="" class="" id="signup-form">
					<input type="text" placeholder="username" name="userName" id="userName"/>
					<input type="text" placeholder="your email" name="userEmail" id="userEmail"/>
					<input type="text" placeholder="first name" name="userFirstName" id="userFirstName">
					<input type="text" placeholder="last name" name="userLastName" id="userLastName">
					<input type="password" placeholder="password" name="userPassword" id="userPassword">
					<input type="password" placeholder="confirm password" name="usercPass" id="usercPass">
					<input type="text" placeholder="country" name="userCountry" id="userCountry">
					<select name="login_type" id="loginType">
						<!--option value="">Select Login Type</option-->
						<option value="Patron" selected>Patron</option>
						<option value="Venue">Venue</option>
					</select>
					<span class="criteria">By clicking below to sign up, you are agreeing to the Whispertip  TERMS OF SERVICES AND PRIVACY POLICY</span>
					<a href="javascript:void(0)" data-uri="<?php echo base_url() ?>" class="login_btn sign" name="sendbttn" id="signupBtn">signup</a>
					<a href="javascript:void(0)" data-uri="<?php echo base_url() ?>" class="login_btn sign" name="cancel_signup" id="reg_cancel">cancel</a>
				</form>	
			</div>
		</div>
	</div>
</div>
</body>
</html>

﻿<!DOCTYPE html>
<html lang="en">
<head>
	<title>Registration</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<script src="js/html5.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style_new.css">
	<script language="JavaScript" src="<?php print base_url();?>assets/js/jquery-1.7.1.min.js" type="text/javascript"></script>
	<script language="JavaScript" src="<?php print base_url();?>assets/js/gen_validatorv31.js" type="text/javascript"></script>
	<script language="JavaScript" src="<?php print base_url();?>assets/js/jquery.validate.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>assets/js/new_design/bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/miscellaneous.js"></script>
	<script>
		$(document).ready(function() {
			$("#signup-form").validate({
				rules: {
					username: {
						required: true,
						remote: {
								url: "<?php echo base_url();?>home/check_patron_username",
								type: "post",
								data: {
								username: function() {
									return $( "#username" ).val();
								  }
								}
							}
					},
					email: {
						required: true,
						email: true,
						remote: {
									url: "<?php echo base_url();?>home/check_patron_email",
									type: "post",
									data: {
									email: function() {
										return $( "#email" ).val();
									  }
									}
								}
						
					},
					first_name: {
						required: true,
						
						
					},
					last_name: {
						required: true,
						
						
					},
					password: {
						required: true,
						
					},
					cpassword: {
						required: true,
						equalTo:'#password'	
						
					},
					country: {
						required: true,
					},
				},
				messages: {
					username: {
						required: "Please enter username.",
						remote:"Username aleardy exists."
					},
					email: {
						required: "Please enter email.",
						email: "Please enter valid email.",
						remote:"Email aleardy exists."
					},
					first_name: {
						required: "Please enter first name.",
					},
					last_name: {
						required: "Please enter last name.",
					},
					password: {
						required: "Please enter password.",
					},
					cpassword: {
						required: "Please enter confirm password.",
						equalTo: "Password and confirm password should be same.",
					},
					country: {
						required: "Please enter country.",
					}
				},
				 errorElement:"div",
				errorClass:"login-error",
				submitHandler: function(form) {
					form.submit();
				}
			});
		});
	</script>
</head>
<body class="bg_image">
<div class="home">
	<div class="container">
		<div class="login_outer">
			<center><img src="<?php echo base_url() ?>assets/images/login_logo.png"/></center>
			<?php if($this->session->flashdata('fail'))
			{?> 			
			<div id="userSignUpError" class="entry_row userSignUpError"><?php echo $this->session->flashdata('fail');?></div>			
			<?php } ?>
			<?php if($this->session->flashdata('success'))
			{?> 
			<div id="userSignUpok" class="entry_row userSignUpok"> <?php echo $this->session->flashdata('success');?> </div>
			<?php } ?>
			<div id="userSignUpError" class="entry_row userSignUpError" style="display:none;"></div>
			<div id="userSignUpok" class="entry_row userSignUpok" style="display:none;"></div>
			<div class="login sign-fields">
				<form method="post" id="signup-form">
					<input type="text" placeholder="username" name="username" id="username"/>
					<input type="text" placeholder="your email" name="email" id="email"/>
					<input type="text" placeholder="first name" name="first_name" id="first_name">
					<input type="text" placeholder="last name" name="last_name" id="last_name">
					<input type="password" placeholder="password" name="password" id="password">
					<input type="password" placeholder="confirm password" name="cpassword" id="cpassword">
					<input type="text" placeholder="country" name="country" id="country">
					<span class="criteria">By clicking below to sign up, you are agreeing to the Whispertip  TERMS OF SERVICES AND PRIVACY POLICY</span>
					<input type="submit" name="submit" id="submit" value="signup" class="login_btn sign" />
					<a href="javascript:void(0)" data-uri="<?php echo base_url() ?>" class="login_btn sign" name="cancel_signup" id="reg_cancel">cancel</a>
				</form>	
			</div>
		</div>
	</div>
</div>
</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <title>WhisperTip</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <script src="<?php echo base_url(); ?>assets/js/html5.js" type="text/javascript"></script>
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/football_title_new.ico" type="image/x-icon" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/popModal.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/style.css">
        <script src="<?php echo base_url(); ?>assets/js/jquery-1.7.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/miscellaneous.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/popModal.js"></script>
        <script>
$(function(){
	$('#termsServices').click(function(){
		$('.dialog_content').dialogModal({
			onOkBut: function() {},
			onCancelBut: function() {},
			onLoad: function() {},
			onClose: function() {},
		});
	});
	
});
function verify_email()
{
	//alert('here');return false;
	$('.via-email').css('display','none');
	$('.via-social').css('display','none');
	$('.via-forgot').css('display','none');
	$('.via-signup').css('display','none');
	$('#emailGetMask').show();
}
</script>

<style>
	.ulclass {
	 color: #777777;
    line-height: 22px;
    margin-left: 31px;
}
.ulclass li {
font-size: 13px;
}
.tip_list p
{
	line-height: 22px;
}

	#loading_img {
		background: none repeat scroll 0 0 #E5E5E5;
		border: 4px solid #FFFFFF;
		border-radius: 7px 7px 7px 7px;
		box-shadow: 0 0 5px #555555;
		padding: 10px;
		text-align: center;
		width: 200px;
	}
	.spin-loading {
		-webkit-animation: spin 1.5s infinite linear;
		-moz-animation: spin 1.5s infinite linear;
		-o-animation: spin 1.5s infinite linear;
		-ms-animation: spin 1.5s infinite linear;
	}
		@-webkit-keyframes spin {
		0% { -webkit-transform: rotate(0deg);}
		100% { -webkit-transform: rotate(360deg);}
	}
		@-moz-keyframes spin {
		0% { -moz-transform: rotate(0deg);}
		100% { -moz-transform: rotate(360deg);}
	}
		@-o-keyframes spin {
		0% { -o-transform: rotate(0deg);}
		100% { -o-transform: rotate(360deg);}
	}
		@-ms-keyframes spin {
		0% { -ms-transform: rotate(0deg);}
		100% { -ms-transform: rotate(360deg);}
	}

</style>

</head>
<body>
<div class="home">
	<div class="">
		<div class="login_outer">
			<center>
			<img src="<?php echo base_url(); ?>assets/images/login_logo.png"/></center>
			<div class="login via-social">
				<a href="#" class="login_btn f_btn" name="facebook">Log in with facebook</a>
				<a href="#" class="login_btn t_btn" name="twitter">Log in with twitter</a>
				<a href="#" class="login_btn g_btn" name="email">Log in with Email</a>
				<div class="social_support" style="font-size:11px;margin-top:10px;">
				Don't have an account? <a class="sign_up" style="color:#7a7a7a;font-family: 'tradegothicbold_condensed';">Sign up</a>
				<a href="<?php echo base_url()?>home/how_it_work/place" style=" color: #B41414;float: left;font-size: 12px;margin-left: 41px;margin-top: 59px;background: none repeat scroll 0 0 #B41414;    border-radius: 4px 4px 4px 4px;color: #fff;display: inline-block;font: bold 0.875rem;padding: 0.75rem 2.5rem;text-align: center;" target="_blank">HOW IT WORKS</a>
				</div>
				<div class="social_support" style="margin-top:180px;">
					Social sports Tipping EXCHANGE
				</div>
				<div class="social_support" style="font-family: 'tradegothicbold_condensed';margin-bottom:100px;">
					GET AHEAD OF THE GAME
				</div>
			</div>
			<!--------------------------------------------------------------------------------------------->
			<!----------------------------------------- Login --------------------------------------->
			<!--------------------------------------------------------------------------------------------->
				<div class="login via-email" style="display:none;">
			 <div id="userLoginError" class="entry_row" style="margin-top:2px;margin-bottom:0px;color:red;">
           </div>
            <form name="loginfm" id="loginfm" method="post">
			 	<input type="text" name="lguserName" id="lguserName" placeholder="User Name" onkeypress="checkButton(event)"/>
				<input type="password" name="lguserPassword" id="lguserPassword" placeholder="Password" onkeypress="checkButton(event)"/>
				<a href="#" class="login_btn"  id="lgloginBtn" style="float:left;width:76px;">Login</a>
				<a href="#" class="login_btn" id="cancelforgot" style="float:left;width:76px;margin-left:60px;" name="cancelButtn">Cancel</a>
				<input type="checkbox" style="display:inline-block; float:left;margin-top:6px;"/><span>Remember me</span><a href="#" class="pull-right forgot">Forgot your password?</a>
				<input type="hidden" value="<?php echo base_url();?>" id="url" name="url">
				</form>
				<div class="social_support" style="margin-top:200px;">
					Social sports Tipping EXCHANGE
				</div>
				<div class="social_support" style="font-family: 'tradegothicbold_condensed';margin-bottom:100px;">
					GET AHEAD OF THE GAME
				</div>
			</div>
			<!--------------------------------------------------------------------------------------------->
			<!----------------------------------------- Forgot --------------------------------------->
			<!--------------------------------------------------------------------------------------------->
			<div class="login via-forgot" style="display:none;">
			<input type="text" name="lguserName" id="lguserName" placeholder="Email Address" style="visibility:hidden;" />
			<div id="userForgotError" class="entry_row" style="margin-top:2px;margin-bottom:0px;color:red;">
				
            </div>
			<input type="text" name="lguserName" id="forgotEmail" placeholder="Email Address" />
				<a href="#" class="login_btn" name="sendbttn" id="sendforgotbtn" style="float:left;width:76px;">Send</a>
				<a href="#" class="login_btn" id="cancelforgot" name="cancel_forgot" style="float:left;width:76px;margin-left:60px;">Cancel</a>
				<div class="social_support" style="margin-top:200px;">
					Social sports Tipping EXCHANGE
				</div>
				<div class="social_support" style="font-family: 'tradegothicbold_condensed';margin-bottom:100px;">
					GET AHEAD OF THE GAME
				</div>
			</div>
			
			<!--------------------------------------------------------------------------------------------->
			<!----------------------------------------- SIGN UP --------------------------------------->
			<!--------------------------------------------------------------------------------------------->
			
			<div class="login via-signup" style="display:none;">
			<div id="userSignUpError" class="entry_row" style="margin-top:2px;margin-bottom:0px;color:green;display:none;">
            </div>
			<input type="text" name="userName" id="userName" placeholder="Username" />
			<input type="text" name="userEmail" id="userEmail" placeholder="Your Email" />
			<input type="text" name="userFirstName" id="userFirstName" placeholder="First Name" />
			<input type="text" name="userLastName" id="userLastName" placeholder="Last Name" />
			<input type="password" name="userPassword" id="userPassword" placeholder="Password" />
			<input type="password" name="usercPass" id="usercPass" placeholder="Confirm Password" />
			<input type="text" name="userCountry" id="userCountry" placeholder="Country" />
				<span style="width:100%;">By clicking below to sign up, you are agreeing to the Whispertip <a href="#" style="color: #347DB2;font-weight: bold;" id="termsServices">TERMS OF SERVICES AND PRIVACY POLICY</a></span>
				<a href="#" class="login_btn" name="sendbttn" id="signupBtn" style="float:left;width:76px;">Sign up</a>
				<a href="#" class="login_btn" name="cancel_signup" id="cancelforgot" style="float:left;width:76px;margin-left:60px;">Cancel</a>
				<div class="social_support" style="margin-top:90px;">
					Social sports Tipping EXCHANGE
				</div>
				<div class="social_support" style="font-family: 'tradegothicbold_condensed';margin-bottom:100px;">
					GET AHEAD OF THE GAME
				</div>
			</div>
			
			<!--------------------------------------------------------------------------------------------->
			<!----------------------------------------- Twitter Email Verification --------------------------->
			<!--------------------------------------------------------------------------------------------->
		<div class="login" id="emailGetMask" style="display:none;">
		 <div id="" class="popup_row getEmail_message" style="color:#557D00"> </div>
          	 	<input name="email_id" id="getemail" type="text" placeholder="Email id"/>
			 	
				<input name="email_id" id="getemail_verify" type="text" placeholder="Repeat-Email id"/>
				<a href="#" class="login_btn_twitter twitternew" id="getEmailLgnBtn" style="float:left;width:76px;">Submit</a>
				<a href="#" class="login_btn_twitter" id="canceltwitter" style="float:left;width:76px;margin-left:60px;" name="cancelButtn">Cancel</a>
				<input type="hidden" value="<?php echo base_url();?>" id="url" name="url">
				<div class="social_support" style="margin-top:200px;">
					Social sports Tipping EXCHANGE
				</div>
				<div class="social_support" style="font-family: 'tradegothicbold_condensed';margin-bottom:100px;">
					GET AHEAD OF THE GAME
				</div>
			</div>
			
			
		</div>
	</div>
</div>

<div id="dialog_content" class="dialog_content" style="display:none">
	<div class="dialogModal_header" style='color: #347DB2;
    font-size: 18px;'>TERMS AND SERVICES</div>
	<div class="dialogModal_content" style="height:400px;overflow:scroll;">
		<div class="tip_list">
	<p>Access to, and use of this website is subject to these terms and conditions (this “Statement”). 
	By accessing, browsing or otherwise using the site you signify your acceptance of these terms. 
	If at any time you do not agree to these terms you should discontinue your use of this website, 
	including any mobile version of this website, any ‘smart phone application connected to this 
	website and any third party medium (such as Twitter) on which information from this website is 
	authorized to be displayed (“The Website”). </p>

	<h1>1. Privacy</h1>
		<p>
		Your privacy is very important to us. We designed our Privacy Policy to make important 
		disclosures to you about how we collect and use the information you post on The 
		Website. We encourage you to read the Privacy Policy, and to use the information it 
		contains to help make informed decisions. 
		</p>
	</div>	
	<div class="tip_list">
		<h1>2. Site Information and Content </h1>
			<p>
			<ul class="ulclass"><li>a. While The Website takes all care in the preparation of information we accept no 
			responsibility nor warrant in any way to the accuracy of the information displayed. </li>
			<li>b. Information on The Website has been provided for entertainment purposes only 
			and The Website accepts no responsibility for any assumptions based or derived 
			from any information displayed or assumed. </li>
			<li>c. WARNING: GAMBLING INVOKES RISK. For the avoidance of doubt, we are not 
			responsible for your gambling losses. Any monetary investment based on 
			information displayed on the site (or affiliated sites) is at the risk of the investor. </li>
		<li>	d. All information relating to race fields, including horse numbers, scratchings, odds 
			and statistics should be checked with an official source. </li>
		<li>	e. We do not guarantee or in any way assure that the tips, suggestions or other 
			information provided to you through the Website will be correct and we expressly 
			disclaim any liability for any loss you suffer as a result of relying on such 
			information.</li>
			</p>
	</div>	
	<div class="tip_list">
		<h1>3. Sharing Your Content and Information </h1>
			<p>You own all of the content and information you post on The Website. In order for us to 
				use certain types of content and provide you with The Website, you agree to the 
				following:
			<ul class="ulclass"><li>A. For content that is covered by intellectual property rights, like photos ("IP 
				content"), you specifically give us the following permission, subject to 
				your privacy: you grant us a non­exclusive, transferable, sub­licensable, 
				royalty­free, worldwide license to use any IP content that you post on or in 
				connection with The Website ("IP License"). This IP License ends when you 
				delete your IP content or your account (except to the extent your content has been 
				shared with others, and they have not deleted it). </li>
			<li>b .When you delete IP content, it is deleted in a manner similar to emptying the 
				recycle bin on a computer. However, you understand that removed content may 
				persist in backup or cached copies for a reasonable period of time.  </li>
		<li>	c. We always appreciate your feedback or other suggestions about The Website, 
				but you understand that we may use them without any obligation to compensate 
				you for them (just as you have no obligation to offer them). </li>
			</p>
	</div>	
	<div class="tip_list">
		<h1>4. Safety </h1>
			<p>We do our best to keep The Website safe, but we cannot guarantee it. We need your 
		help in order to do that, which includes the following commitments: 
			<ul class="ulclass"><li>a. You will not send or otherwise post unauthorized commercial communications to 
users (such as spam).</li>
			<li>b. You will not collect users' information, or otherwise access The Website, using 
automated means (such as harvesting bots, robots, spiders, or scrapers) without 
our permission.  </li>
		<li>c. You will not upload viruses or other malicious code. </li>
		<li>d. You will not solicit login information or access an account belonging to someone 
else.</li>
		<li>f. You will not post content that is hateful, threatening, pornographic, or that contains 
nudity or graphic or gratuitous violence. </li>
		<li>g. You will not use The Website to do anything unlawful, misleading, malicious, or 
discriminatory.  </li>
		<li>h. You will not facilitate or encourage any violations of this Statement. </li>
		If you do any of the foregoing, we reserve the right to disable your account and ban you from 
using The Website.  
			</p>
	</div>	
	<div class="tip_list">
					<h1>5. Registration and Account Security</h1>
					<p>The Website users provide their real names and information, and we
					need your help to keep it that way. Here are some commitments you
					make to us relating to registering and maintaining the security of your
					account:
					<ul class="ulclass"><li>a. You will not provide any false personal information on The
					Website, or create an account for anyone other than yourself
					without permission.
					<li>b. You will not use The Website if you are under 18.</li>
					<li>c. You will keep your contact information accurate and up-to-date.</li>
				<li>	d. You will not share your password, let anyone else access your
					account, or do anything else that might jeopardize the security of
					your account.</li>
				<li>	e. You will not transfer your account to anyone without first
					getting our written permission.</li>
				<li>	f. You will not have more than one active account without first
					getting our written permission.</li></p>
				</div>	
				<div class="tip_list">
					<h1>6. Protecting Other People's Rights</h1>
					<p>We respect other people's rights, and expect you to do the same.
					<ul class="ulclass"><li>a. You will not post content or take any action on The Website that
					infringes someone else's rights or otherwise violates the law. can
					remove any content you post on The Website if we believe that it
					violates this Statement.</li>
					<li>b. If we removed your content for infringing someone else's
					copyright, and you believe we removed it by mistake please
					contact us.</li>
					<li>c. If you repeatedly infringe other people's intellectual property
					rights, we will disable your account when appropriate.</li>
					<li>d. You will not use our copyrights or trademarks without our
					written permission.</li>
					<li>e. You will not post anyone's identification documents or sensitive
					financial information on The Website.</li></p>
				</div>	
				<div class="tip_list">
					<h1>7. Mobile</h1>
					<p><ul class="ulclass"><li>a. We currently provide our mobile services for an advertised cost
						which is withdrawn from your The Website account, but please
						be aware that your carrier's normal rates and fees, such as text
						messaging fees, will still apply if you call or send us an SMS.</li>
						<li>b. In the event you change or deactivate your mobile telephone
						number, you will update your account information on The
						Website within 48 hours to ensure that your messages are not
						sent to the person who acquires your old number.</li></p>
				</div>	
				<div class="tip_list">
					<h1>8. Tipping</h1>
					<p>
					If you buy, enter your tips or access tipping information you agree to
					our</p>
				</div>	
				<div class="tip_list">
					<h1>9. Payments</h1>
					<p>
					If you post or view Comments you agree to our</p>
				</div>	
				<div class="tip_list">
					<h1>10. Comments</h1>
					<p>
					If you make or receive payments you agree to our</p>
				</div>	
				<div class="tip_list">
					<h1>11. About Advertisements on The Website</h1>
					<p>
					Our goal is to deliver ads that are not only valuable to advertisers, but
					also valuable to you. In order to do that, you agree to the following:
					<ul class="ulclass"><li>a. We do not give your content to advertisers.</li>
					<li>b. You understand that we may not always identify paid services
					and communications as such.</li>
					</p>
				</div>	
				<div class="tip_list">
					<h1>12. Special Provisions Applicable to Advertisers</h1>
					<p>
					You can target your specific audience by buying ads on The Website or
					our publisher network. The following additional terms apply to you if
					advertise with us:
					If you are placing ads on someone else's behalf, we need to make sure
					you have permission to place those ads, including the following:
					<ul class="ulclass"><li>a. When you advertise with us, you will tell us the type of
					advertising you want to buy, the amount you want to spend. If we
					accept your advertisement, we will deliver your ads as inventory
					becomes available.</li>
				<li>	b. You will pay in accordance with our . The amount you owe will be
					calculated based on your advertising requirements.</li>
				<li>	c. We will determine the size, placement, and positioning of your
					ads.</li>
				<li>	d. We do not guarantee the activity that your ads will receive, such
					as the number of clicks you will get.</li>
				<li>	e. We cannot control how people interact with your ads, and are not
					responsible for click fraud or other improper actions that affect
					the cost of running ads.</li>
				<li>	f. You will not offer any contest or sweepstakes ("promotion")
					without our prior written consent. If we consent, you take full
					responsibility for the promotion and all applicable laws.</li>
				<li>	g. You can cancel your advertisement at any time by contacting us
					but it may take us seven days before the ad stops running.</li>
				<li>	h. Our license to run your ad will end when we have completed
					your rder. You understand, however, that if users have interacted
					with your ads, your ads may remain until the users delete it.</li>
				<li>	i. We can use your ads and related information for marketing or
					promotional purposes.</li>
			<li>		j. You will not issue any press release or make public statements
					about your relationship with The Website without written
					permission.</li>
				<li>	k. We may reject or remove any ad for any reason.</li>
			<li>		l. You warrant that you have the legal authority to bind the
					advertiser to this Statement.</li>
			<li>		m. You agree that if the advertiser you represent violates this
					Statement, we may hold you responsible for that violation.</li>

					</p>
				</div>	
				<div class="tip_list">
					<h1>13. Amendments</h1>
					<p>
					From time to time we may amend the terms. By continuing to use the
					site after such amendments have been made you are agreeing to be
					bound by the terms as amended. If at any time you do not agree to these
					terms you should discontinue your use of the website
					</p>
				</div>	
				<div class="tip_list">
					<h1>14. Termination</h1>
					<p>
					If you violate the letter or spirit of this Statement, or otherwise create
					possible legal exposure for us, we can stop providing all or part of The
					Website to you. We will generally try to notify you, but have no
					obligation to do so. You may also delete your account or disable your
					application at any time by . In all such cases, this Statement will still
					apply.
					</p>
				</div>	
				<div class="tip_list">
					<h1>15. Disputes</h1>
					<p>
					<ul class="ulclass"><li>a. You will resolve any claim, cause of action or dispute ("claim")
					you have with us arising out of or relating to this Statement or
					The Website in a state or federal court located in Australia. The
					laws of the Australia will govern this Statement, as well as any
					claim that might arise between you and us, without regard to
					conflict of law provisions. You agree to submit to the personal
					jurisdiction of the courts located in Australia for the purpose of
					litigating all such claims.</li>
					<li>b. If anyone brings a claim against us related to your actions or your
					content on The Website, you will indemnify and hold us harmless
					from and against all damages, losses, and expenses of any kind
					(including reasonable legal fees and costs) related to such claim.</li>
					<li>c. WE TRY TO KEEP UP, BUG-FREE, AND SAFE, BUT YOU USE IT AT
					YOUR OWN RISK. WE ARE PROVIDING "AS IS" WITHOUT ANY
					EXPRESS OR IMPLIED WARRANTIES INCLUDING, BUT NOT
					LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY,
					FITNESS FOR A PARTICULAR PURPOSE, AND
					NON-INFRINGEMENT. WE DO NOT GUARANTEE THAT WILL BE
					SAFE OR SECURE. IS NOT RESPONSIBLE FOR THE ACTIONS OR
					CONTENT OF THIRD PARTIES, AND YOU RELEASE US, OUR
					DIRECTORS, OFFICERS, EMPLOYEES, AND AGENTS FROM ANY
					CLAIMS AND DAMAGES, KNOWN AND UNKNOWN, ARISING OUT
					OF OR IN ANY WAY CONNECTED WITH ANY CLAIM YOU HAVE
					AGAINST ANY SUCH THIRD PARTIES. WE WILL NOT BE LIABLE TO
					YOU FOR ANY LOST PROFITS OR OTHER CONSEQUENTIAL,
					SPECIAL, INDIRECT, OR INCIDENTAL DAMAGES ARISING OUT OF
					OR IN CONNECTION WITH THIS STATEMENT OR , EVEN IF WE
					HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
					OUR AGGREGATE LIABILITY ARISING OUT OF THIS STATEMENT
					OR WILL NOT EXCEED THE GREATER OF ONE HUNDRED
					DOLLARS ($100) OR THE AMOUNT YOU HAVE PAID US IN THE
					PAST TWELVE MONTHS. APPLICABLE LAW MAY NOT ALLOW THE
					LIMITATION OR EXCLUSION OF LIABILITY OR INCIDENTAL OR
					CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR
					EXCLUSION MAY NOT APPLY TO YOU. IN SUCH CASES, 'S
					LIABILITY WILL BE LIMITED TO THE FULLEST EXTENT
					PERMITTED BY APPLICABLE LAW.</li>
					</p>
				</div>	
				<div class="tip_list">
					<h1>16. Definitions</h1>
					<p>
				<ul class="ulclass"><li>a. By "The Website" we mean the features and services we make
				available, including through (a) our website at and any other The
				Website branded or co-branded websites (including
				sub-domains, international versions, and mobile versions); (b)
				our Platform; and (c) other media, devices or networks now
				existing or later developed.</li>
				<li>b. By "us," "we" and "our" we mean Pty Ltd. and/or its affiliates.</li>
				<li>c. By "Platform" we mean a set of APIs and services that enable
				applications, developers, operators or services to retrieve data
				from The Website and provide data to us relating to The Website
				users.</li>
				<li>d. By "content" we mean the content, information and tipping you
				post on The Website, including information about you and the
				actions you take.</li>
				<li>e. By "post" we mean post on The Website or otherwise make
				available to us.</li>
			<li>	f. By "use" we mean use, copy, publicly perform or display,
				distribute, modify, translate, and create derivative works of.</li>
					</p>
				</div>	
				<div class="tip_list">
					<h1>17. Other</h1>
					<p>
					<ul class="ulclass"><li>a. This Statement makes up the entire agreement between the
					parties regarding The Website, and supersedes any prior
					agreements.</li>
					<li>b. If any portion of this Statement is found to be unenforceable, the
					remaining portion will remain in full force and effect.</li>
					<li>c. If we fail to enforce any of this Statement, it will not be
					considered a waiver.</li>
				<li>	d. Any amendment to or waiver of this Statement must be made in
					writing and signed by us.</li>
				<li>	e. You will not transfer any of your rights or obligations under this
					Statement to anyone else without our consent.</li>
				<li>	f. All of our rights and obligations under this Statement are freely
					assignable by us in connection with a merger, acquisition, or sale
					of assets, or by operation of law or otherwise.</li>
				<li>	g. Nothing in this Agreement shall prevent us from complying with
					the law.</li>
				<li>	h. This Statement does not confer any third party beneficiary
					rights.</li>
					</p>
				</div>	
</div>
	
</div>

<!--/////////////////////////popup-start/////////////////////////////-->
	<div class="popup_container_login"  id="popup_container_login" style="display:none">
		<div class="popup_outer_login">
			<a href="javascript:void(0)" class="popup_img_login" style="left:5px;"></a>
			<a href="javascript:void(0)" class="popup_img_login" style="right:15px; top:10px;">
				<img src="<?php echo base_url();?>assets/images_new/cross.png" class="close_popup_login" /></a>
			<h1>Login Form</h1>
			
			<div class="reg_option">
				<?php if($auth == 'authenticated')
					{
						$url =  base_url()."home/login_popup/auth"; 
					} 
					else
					{
						$url = base_url()."home/login_popup";
					}
						 ?>
				<script>
				$(document).ready(function(){
					$('#iframe').load('<?php echo $url;?>');
					})
				</script>
				<div src="<?php echo $url;?>" id="iframe" frameborder="0" ></div>
			</div>
	</div>
	
<!--////////////////////////////////////// popup ////////////////////////////////////-->
<div id="loading_img" style="display:none";>
			<img class="spin-loading" style= "margin-top: 10px;" border="0" src="<?php echo base_url()?>assets/images/football_spin.png" />
			<p style="margin-top: 10px;">LOADING PLEASE WAIT...</p>
		</div>
</body>
</html>

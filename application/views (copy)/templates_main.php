<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <title>WhisperTip</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/football_title_new.ico" type="image/x-icon" />
		<link href="<?php echo base_url();?>assets/css/front/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/style_new.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/front/bootstrap.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/popModal.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/front/login_popup.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style1.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/admin/jquery-ui.css">
        
        <script src="<?php echo base_url() ?>assets/js/html5.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/js/ajax.googleapis.com.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
        
        <!--script src="http://code.jquery.com/jquery-1.11.0.min.js"></script-->
        <script src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.autocomplete.js"></script>
        <script src="<?php echo base_url() ?>assets/js/miscellaneous.js"></script>
       
		<script language="JavaScript" src="<?php print base_url();?>assets/js/jquery.validate.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>assets/js/jquery-ui.js"></script>
		<!-- script type="text/javascript">
		$(function(){
		    var stickyRibbonTop = $('.stickyribbon').offset().top;
		      
		    $(window).scroll(function(){
		            if( $(window).scrollTop() > stickyRibbonTop ) {
		                    $('.stickyribbon').css({position: 'fixed', top: '90px'});
		            } else {
		                    $('.stickyribbon').css({position: 'static', top: '0px'});
		            }
		    });
		});
		</script -->
 <!--script>
	var base_url = '<?php echo  base_url();?>';

       	window.twttr = (function(d, s, id) {
            var t, js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "https://platform.twitter.com/widgets.js";
            fjs.parentNode.insertBefore(js, fjs);
            return window.twttr || (t = {
                _e: [],
                ready: function(f) {
                    t._e.push(f)
                }
            })
        }(document, "script", "twitter-wjs"));
	</script>
	<script>
	  !function(g,s,q,r,d){r=g[r]=g[r]||function(){(r.q=r.q||[]).push(
	  arguments)};d=s.createElement(q);q=s.getElementsByTagName(q)[0];
	  d.src='//d1l6p2sc9645hc.cloudfront.net/tracker.js';q.parentNode.
	  insertBefore(d,q)}(window,document,'script','_gs');

	  _gs('GSN-972951-J');
	</script-->

<!--style>
	#loading_img {
		background: none repeat scroll 0 0 #E5E5E5;
		border: 4px solid #FFFFFF;
		border-radius: 7px 7px 7px 7px;
		box-shadow: 0 0 5px #555555;
		padding: 10px;
		text-align: center;
		width: 200px;
	}
	.spin-loading {
		-webkit-animation: spin 1.5s infinite linear;
		-moz-animation: spin 1.5s infinite linear;
		-o-animation: spin 1.5s infinite linear;
		-ms-animation: spin 1.5s infinite linear;
	}
		@-webkit-keyframes spin {
		0% { -webkit-transform: rotate(0deg);}
		100% { -webkit-transform: rotate(360deg);}
	}
		@-moz-keyframes spin {
		0% { -moz-transform: rotate(0deg);}
		100% { -moz-transform: rotate(360deg);}
	}
		@-o-keyframes spin {
		0% { -o-transform: rotate(0deg);}
		100% { -o-transform: rotate(360deg);}
	}
		@-ms-keyframes spin {
		0% { -ms-transform: rotate(0deg);}
		100% { -ms-transform: rotate(360deg);}
	}

</style-->
</head>
<body class="body_bg_color">
<?php if($login_status==1){
	
	 if( $this->session->userdata('user_logged_as') == 'venue') { ?>
		<header class="edit_profile">
			<span><?php echo $venuedata['venue_name']; ?></span>
			<div class="media_link">
			
				<a href="<?php echo base_url()?>home/logout" class="buy_tip">logout</a>
			</div>
		</header>
	<?php } else {
			if( $userData['profile_image']!='' )
			{
				if($userData['profile_image']!='' && file_exists('uploads/profile_picture/'.$userData['profile_image']))
				{
					$image =   "<img src='".base_url().'uploads/profile_picture/'.$userData['profile_image']."' style='height:30px;width:30px;'/>";
					
				} else {
					
					$image =   "<img src='".base_url().'assets/images/default-no-profile-pics.jpg'."' style='height:30px;width:30px;'>";
				}
			}
			else
			{
				$image =   "<img src='".base_url().'assets/images/default-no-profile-pics.jpg'."' style='height:30px;width:30px;'>";

			}
			
			if( $userData['google_plus_id'] != ''  && $userData['profile_image'] != '' ) {
				$image =   "<img src='".$userData['profile_image']."' style='height:30px;width:30px;'>";
			}
			
			if( $userData['facebook_id'] != ''  && $userData['profile_image'] != '' ) {
				$image =   "<img src='".$userData['profile_image']."' style='height:30px;width:30px;'>";
			}
			
			
			if( $image != '' ) { $image = $image;} else { $image = "<img class='inner-pages-image' src='".base_url().'assets/images/default-no-profile-pics.jpg'."' style='height:30px;width:30px;'>"; }
		?>
			
	<header class="edit_profile">
		
			<?php echo $image; ?> <span><?php echo $userData['first_name'].' '.$userData['last_name']; ?></span>
			<div class="media_link">
				<a href="<?php echo base_url()?>home/logout" class="buy_tip">logout</a>
			</div>
	</header>
	<?php } 
	
 } ?>

	<?php if($place_val!='loginPage') { ?>

<?php }?>

<input type="hidden" value="<?php echo base_url()?>" id="url">

<?php if($this->session->flashdata('success')) { ?>
	<div class="entry_row userSignUpok" id="userSignUpok"><?php echo $this->session->flashdata('success'); ?></div>
<?php } ?>
 
<?php if($this->session->flashdata('error')) { ?>
	<div class="entry_row userSignUpError" id="userSignUpError"><?php echo $this->session->flashdata('error'); ?></div>
<?php } ?>	
  <?php echo $page_body; ?>
  
<div id="loading_img" style="display:none";>
			<img class="spin-loading" style= "margin-top: 10px;" border="0" src="<?php echo base_url()?>assets/images/football_spin.png" />
			<p style="margin-top: 10px;">LOADING PLEASE WAIT...</p>
		</div>

<script src="<?php echo base_url(); ?>assets/js/popModal.js"></script>

<div class="popup_container_login"  id="popup_container_login" style="display:none">
		<div class="popup_outer_login">
			<a href="javascript:void(0)" class="popup_img_login" style="left:5px;"></a>
			<a href="javascript:void(0)" class="popup_img_login" style="right:15px; top:10px;">
				<img src="<?php echo base_url();?>assets/images_new/cross.png" class="close_popup_login" /></a>
			<h1>Login Form</h1>
			
			<div class="reg_option">
				
			</div>
	</div></div>


<div class="footer">
	<div class="container">
		<div class="left_link">
			<ul>
				<li><a href="<?php echo base_url(); ?>terms_conditions/">Terms and conditions</a></li>
				<li><a href="<?php echo base_url(); ?>privacy_policy/">Privacy policy</a></li>
				<li><a href="mailto:support@whispertip.com">Contact Us</a></li>
			</ul>
		</div>
		<div class="social_icon">
			<p>Get Social With Us</p>
			<ul>
			<li><a target="_blank" href="https://facebook.com/whispertip"><img src="http://localhost/whispertip/assets/images/facebook.png"></a></li>
			<li><a target="_blank" href="http://twitter.com/whispertip"><img src="http://localhost/whispertip/assets/images/twitter.png"></a></li>
			<li><a href=""><img src="http://localhost/whispertip/assets/images/incledion.png"></a></li>
			<li><a target="_blank" href="http://instagram.com/whispertip"><img src="http://localhost/whispertip/assets/images/instagram.png"></a></li>
			
			</ul>
		</div>
	</div>
</div>
<!-- footer>

		<div class="container">
			<span>© whispertip.com. All rights reserved</span>
		</div>

	
</footer -->
	
</body>
<script type="text/javascript">
function twitter_share11(user_id) {
	window.open("https://twitter.com/intent/follow?user_id="+user_id,'_blank','width=810,height=570');
}
</script>
</html>


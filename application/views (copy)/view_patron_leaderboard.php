<div class="rugby blue">
	<div class="container">
		<div class="main_outer">
		<div class="content">
			<center><img class='inner-pages-image' src="<?php echo base_url().'assets/images/logo.png'; ?>"/></center>		
			<div class="sort"></div>
		
			<?php echo $sidebar; ?>
			
			<div class="right_content" id="page_content">
				<div class="blog coach_blog">
					<div class="but_tip_coach">
					
						<div class="widthdraw">
							
							<?php if( $this->uri->segment(3) != '') { ?>
							<div class="widthdraw_heading">Select Teams :</div>
							
							<?php if( !empty($leaderboard_data) ) { 
								echo '<table>';
								foreach($leaderboard_data as $user )
								{
									
									echo '<tr>';
									
									
									echo '<td>';
									echo '<img height=50 width=50 src="'.base_url().'uploads/profile_picture/'.$user['profile_image'].'"/>';
									echo '</td>';
									echo '<td>';
									echo $user['first_name'] .' '. $user['last_name'];
									echo '</td>';
									echo '<td>';
									echo 'Correct tips:'. $user['result'].'/'.$total;
									echo '</td>';
									
									echo '</tr>';
								}
								echo '</table>';
							//	echo "<pre>";
							//	print_r($leaderboard_data);
							 } 
							
							else {
								
								 echo "<table><tr><td colspan=3>NO Leaderboard Data</td></tr></table>";
								
								} ?>
							
							<?php } else { ?>
							<div class="widthdraw_heading">Select Competition :</div>
					
				
							<div class="profile_form">
								<label>Select Competition</label>
								<div class="profile_input">
									<a href="<?php echo base_url(); ?>home/view_leaderboard/AFL">AFL</a>
									<a href="<?php echo base_url(); ?>home/view_leaderboard/NRL">NRL</a>
								</div>
							</div>
							
							<?php } ?>
						
						
						
						
						</div>
					
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>	
</div>

<script>
	
$(document).on('change','#change_rounds',function(){
	var round_id = $(this).val();
	window.location.href = '<?php echo base_url(); ?>tips/competition/<?php echo $this->uri->segment(3); ?>/'+ round_id;
});

$(function() {
	$("#select_venue_frm").validate({
		rules: {
			main_venue: "required",
		},
		messages: {
			main_venue: "Please select venue.",
		},
			errorElement:"div",
			errorClass:"valid-error",
			submitHandler: function(form) {
				form.submit();
			}
		});
	});
</script>


<style>
table {
  border: 1px solid black;
  height: auto;
  width: 548px;
}
tr {
  border-bottom: 1px solid;
  float: left;
  height: 70px;
}
td {
  border-right: 1px solid;
  float: left;
  width: 182px;
}
</style>

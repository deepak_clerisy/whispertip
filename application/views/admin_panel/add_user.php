<?php 
	$venud = $this->uri->segment(3);
	if($venud == 'edit'){
		$pagename = 'Edit Patron';
	} else if ($venud == 'add') {
		$pagename = 'Add Patron';
	}
	if( !empty( $user ))
	{
		$id = $user['id'];
		$fname = $user['first_name'];
		$lname = $user['last_name'];
		$email = $user['email'];
		$gender = $user['gender'];
		$status = $user['status'];
		$username = $user['username'];
		$country = $user['country'];
		$phone = $user['phone'];
		$dob = $user['dob'];
		$address = $user['address'];
		$username_type=$email_type="readonly";
	} else {
		$id=$fname=$lname=$email=$gender=$status=$username=$dob=$country=$phone=$dob=$address=$username_type=$email_type='';
	}
?>

<form method="post" id="add_user_form" enctype="multipart/form-data"> 

<div class="account-right-div">
					
		<div class="dashboard-heading">
			<h2>
				<?php echo $page_title; ?>
			</h2>
			<h5>
				<a href="<?php echo base_url(); ?>admin_panel/home">Dashboard</a><i class="fa fa-angle-right"></i>
			</h5>
			<h5>
				<a href="<?php echo base_url(); ?>admin_panel/venues">Patrons</a><i class="fa fa-angle-right"></i>
			</h5>
			<h5>
				<?php echo $pagename; ?>
			</h5>
		</div>
	
		<div class="dashboard-inner">
			<div class="main-dash-summry Edit-profile">
			  <form name='add_hospital_form' id='add_hospital_form' method='post'>
				  <?php if( $id != '' ) { ?>
						<input type="hidden" name="id" id="user_id" value="<?php echo $id; ?>" id="hospitalid">
						<?php }else { ?>
				<input type="hidden" name="userid" id="user_id" value=''>
				<?php  } ?>
				<div class="input-row">
					<div class="full">
						<div class="input-block">
							<label>First Name :</label>
							<span class='reg_span'><input type="text" id='fname' name="first_name" value="<?php echo $fname; ?>" class="inputbox-main"></span>
						</div>
					</div>
				</div>
				
				<div class="input-row pull-right">
					<div class="full">
						<div class="input-block">
							<label>Last Name :</label>
							<span class='reg_span'><input type="text" id='lname' class="inputbox-main" type="text" name="last_name" value="<?php echo $lname; ?>"></span>
						</div>
					</div>
				</div>
				<div class="input-row">
					<div class="full">
						<div class="input-block">
							<label>Username :</label>
							<span class='reg_span'><input <?php echo $username_type; ?> type="text" id='username' class="inputbox-main" type="text" name="username" value="<?php echo $username; ?>"></span>
						</div>
					</div>
				</div>
				<div class="input-row pull-right">
					<div class="full">
						<div class="input-block">
							<label>Email :</label>
							<span class='reg_span'><input <?php echo $email_type; ?> type="text" id='email' class="inputbox-main" type="text" name="email" value="<?php echo $email; ?>"></span>
						</div>
					</div>
				</div>
			
				<?php  if($id == '') { ?>
				<div class="input-row ">
					<div class="full">
						<div class="input-block">
							<label>Password :</label>
							<span class='reg_span'><input type="password" id='pwd' class="inputbox-main" type="text" name="password" value=""></span>
						</div>
					</div>
				</div>
				
				<div class="input-row pull-right">
					<div class="full">
						<div class="input-block">
							<label>Confirm Password :</label>
							<span class='reg_span'>
								<input type="password" id='confirmPwd' class="inputbox-main" type="text" name='confirmPwd' value="">
							</span>
						</div>
					</div>
				</div>
				<?php } ?>
				
				<div class="input-row">
					<div class="full">
						<div class="input-block">
							<label>Country:</label>
							<span class='reg_span'><input type="text" id='country' class="inputbox-main" type="text" name="country" value="<?php echo $country; ?>"></span>
						</div>
					</div>
				</div>
				
				<div class="input-row pull-right">
					<div class="full">
						<div class="input-block">
							<label>Gender :</label>
							<span class='reg_span'>
								<select id='gender' class="inputbox-main input_select"  name="gender">
									<option value="">Select Gender</option>
									<option value="Male" <?php if($gender =='Male') { echo 'selected'; } ?>>Male</option>
									<option value="Female" <?php if($gender== 'Female') { echo 'selected'; } ?>>Female</option>
								</select>
							</span>
						</div>
					</div>
				</div>
				
				<div class="input-row">
					<div class="full">
						<div class="input-block">
							<label>Phone:</label>
							<span class='reg_span'><input type="text" id='phone' class="inputbox-main" type="text" name="phone" value="<?php echo $phone; ?>" maxlength="15"></span>
						</div>
					</div>
				</div>
				
				<div class="input-row pull-right">
					<div class="full">
						<div class="input-block">
							<label>Date of Birth:</label>
							<span class='reg_span'><input type="text" id='dob' class="inputbox-main" type="text" name="dob" value="<?php echo $dob; ?>"></span>
						</div>
					</div>
				</div>
				
				<div class="input-row">
					<div class="full">
						<div class="input-block">
							<label>Address:</label>
							<span class='reg_span'><textarea rows="10" col="20" id="address" class="textarea-main" name="address"><?php echo $address; ?></textarea></span>
						</div>
					</div>
				</div>
						
				<div class="input-row pull-right">
					<div class="full">
						<div class="input-block">
							<label>Status :</label>
							<span class='reg_span'>
								<select id='status' class="inputbox-main input_select"  name="status">
									<option value="">Select Status</option>
									<option value="1" <?php if($status == 1) { echo 'selected'; } ?>>Active</option>
									<option value="0" <?php if($status== 0) { echo 'selected'; } ?>>Inactive</option>
								</select>
							</span>
						</div>
					</div>
				</div>
				
				<div class="footer_button">
					<div class="">
						<div class="full">
							<div class="input-block add-user-btn">
								<span class="">
									<input type="submit" value="Submit" class="btn-submit btn"> 
									<input type="button" value="Cancel" onclick="javascript:history.go(-1);" class="btn-submit btn"> 
								</span>
							</div>
						</div>	
					</div>
				</div>
					
					
				</div>
				
				

				</form>
			</div>
		</div>
	</div>
</form>
	
<script type="text/javascript">
	$(document).ready(function () {
			  var value = $("input[name=change_pwd]:checked", "#add_user_form").val();
			 	if(value=="1"){
					$('.change_pwd_block').show();
					$('#password').attr('name', 'password');
				} else if(value=="0"){
					$('.change_pwd_block').hide();
					$('#password').attr('name', 'changed');
				}
			$("input:radio[name=change_pwd]").click(function() {
				var value = $(this).val();
				if(value=="1"){
					$('.change_pwd_block').show();
					$('#password').attr('name', 'password');
				} else {
					$('.change_pwd_block').hide();
					$('#password').attr('name', 'changed');
				}
			});
					   
		});
	  $(function() {
		  
		  var hospitalid=$("#hospitalid").val();
		$("#add_user_form").validate({
					rules: {
						first_name: "required",
						last_name: "required",
						password: {
							required: true,
							minlength: 5
						},
						confirmPwd: {
							required: true,
							//minlength: 5,
							equalTo: "#pwd"
							
						},
						email: {
							required: true,
							email: true,
							remote: {
								url: "<?php echo base_url();?>admin_panel/Users/chk_email",
								type: "post",
								data: {
									username: function() { return $("#email").val(); },
									id:function(){ return $("#user_id").val(); }  
								}
							},
						},
						status: "required",
						gender: "required",
						phone: {
							number : true
						},
						username:{
								required:true,
								remote: {
									url: "<?php echo base_url();?>admin_panel/Users/chk_username",
									type: "post",
									data: {
										username: function() { return $("#username").val(); },
										id:function(){ return $("#user_id").val(); }
									}
								}
							},
					},
					messages: {
					first_name: "Please enter first name.",
					last_name: "Please enter last name.",
					password: {
						required: "Please enter a password.",
						minlength: "Password should be at least 5 characters long."
					},
					
					confirmPwd:{
						required: "Please enter confirm password.",
						equalTo: "Both passwords should be same.",
						// minlength: "Confirm password should be at least 5 characters long."
					},
					
					email: {
						required: "Please enter an email address.",
						email: "Please enter a valid email address.",
						remote:"Email already exist, try another."
					},
					status: "Please select status.",
					gender: "Please select gender.",
					phone: {
						number: "Please enter numeric value only.",
					},
					username: {
						required:"Please enter username.",
						remote:"Username already exist, try another."
					},
				},
			errorElement:"div",
			errorClass:"valid-error",
			submitHandler: function(form) {
				form.submit();
			}
		});
	});
	  
	 function cancelButton()
	  {
		location.assign("<?php echo base_url(); ?>snadmin/dashboard");
	  } 
	  
	  function acccountType()
	  {
	   var type=$('#role').val();
	   if(type=='therapist')
	   {
	    $('.forTherapist').show();
	    $('.forClient').hide();
	   }
	   else if(type=='client')
	   {
		 $('.forTherapist').hide();
	     $('.forClient').show();
	   }
	   else
	   {
	    $('.forTherapist').hide();
	    $('.forClient').hide();
	   }
	  }
	</script>

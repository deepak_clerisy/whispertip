<?php 
	
	$venud = $this->uri->segment(3);
	if($venud == 'edit'){
		$pagename = 'Edit Venue';
	} else if ($venud == 'add') {
		$pagename = 'Add Venue';
	}
	
	if(!empty($user)) {
		$id = $user['venue_id'];
		$vname = $user['venue_name'];
		$address = $user['address'];
		$phone = $user['phone'];
		$contact = $user['contact'];
		$email = $user['email'];
		$status = $user['status'];
	} else {
		$id=$vname=$address=$phone=$contact=$email=$status='';
	}
?>
	<form method="post" id="add_competition_form" enctype="multipart/form-data"> 
		<div class="account-right-div">
			<div class="dashboard-heading">
				<h2>
					<?php echo $page_title; ?>
				</h2>
				<h5>
					<a href="<?php echo base_url(); ?>admin_panel/home">Dashboard</a><i class="fa fa-angle-right"></i>
				</h5>
				<h5>
					<a href="<?php echo base_url(); ?>admin_panel/venues">Patrons</a><i class="fa fa-angle-right"></i>
				</h5>
				<h5>
					<?php echo $pagename; ?>
				</h5>				
			</div>
			<div class="dashboard-inner">
				<div class="main-dash-summry Edit-profile">
					<form name='add_hospital_form' id='add_hospital_form' method='post'>
						<?php if( $id != '' ) { ?>
							<input type="hidden" name="id" id="user_id" value="<?php echo $id; ?>" id="hospitalid">
						<?php }else { ?>
							<input type="hidden" name="userid" id="user_id" value=''>
						<?php  } ?>
												
						<div class="input-row">
							<div class="full">
								<div class="input-block">
									<label>Venue Name :</label>
									<span class='reg_span'><input type="text" id="vname" name="vname" value="<?php echo $vname; ?>" class="inputbox-main"></span>
								</div>
							</div>
						</div>
						
						<div class="input-row pull-right">
							<div class="full">
								<div class="input-block">
									<label>Email :</label>
									<span class='reg_span'><input type="text" id="email" class="inputbox-main" type="text" name="email" value="<?php echo $email; ?>"></span>
								</div>
							</div>
						</div>
						
						<?php  if($id == '') { ?>
							<div class="input-row">
								<div class="full">
									<div class="input-block">
										<label>Password :</label>
										<span class='reg_span'><input type="password" id="pwd" class="inputbox-main" type="text" name="password" value=""></span>
									</div>
								</div>
							</div>
							<div class="input-row pull-right">
								<div class="full">
									<div class="input-block">
										<label>Confirm Password :</label>
										<span class='reg_span'>
											<input type="password" id="confirmPwd" class="inputbox-main" type="text" name="confirmPwd" value="">
										</span>
									</div>
								</div>
							</div>
						<?php } ?>
						
						<div class="input-row">
							<div class="full">
								<div class="input-block">
									<label>Phone :</label>
									<span class='reg_span'><input type="text" id="phone" class="inputbox-main" type="text" name="phone" value="<?php echo $phone; ?>"></span>
								</div>
							</div>
						</div>

						<div class="input-row pull-right">
							<div class="full">
								<div class="input-block">
									<label>Contact :</label>
									<span class='reg_span'><input type="text" id="contact" class="inputbox-main" type="text" name="contact" value="<?php echo $contact; ?>"></span>
								</div>
							</div>
						</div>
						
						<div class="input-row">
							<div class="full">
								<div class="input-block">
									<label>Address:</label>
									<span class='reg_span'><textarea rows="10" col="20" id="address" class="textarea-main" name="address"><?php echo $address; ?></textarea></span>
								</div>
							</div>
						</div>
					
						<div class="input-row pull-right">
							<div class="full">
								<div class="input-block">
									<label>Status :</label>
									<span class="reg_span reg_span_textarea">
										<select id="status" class="inputbox-main input_select"  name="status">
											<option value="">Select Status</option>
											<option value="1" <?php if($status == 1) { echo 'selected'; } ?>>Active</option>
											<option value="0" <?php if($status== 0) { echo 'selected'; } ?>>Inactive</option>
										</select>
									</span>
								</div>
							</div>
						</div>

					<div class="footer_button">	
						<div class="">
							<div class="full">
								<div class="input-block add-user-btn">
									<span class="">
										<input type="submit" value="Submit" class="btn-submit btn"> 
										<input type="button" value="Cancel" onclick="javascript:history.go(-1);" class="btn-submit btn"> 
									</span>
								</div>
							</div>	
						</div>
						</div>

					</div>
					</form>
				</div>
			</div>
		</div>
	</form>
	
<script type="text/javascript">
	$(document).ready(function () {
			  var value = $("input[name=change_pwd]:checked", "#add_user_form").val();
			 	if(value=="1"){
					$('.change_pwd_block').show();
					$('#password').attr('name', 'password');
				} else if(value=="0"){
					$('.change_pwd_block').hide();
					$('#password').attr('name', 'changed');
				}
			$("input:radio[name=change_pwd]").click(function() {
				var value = $(this).val();
				if(value=="1"){
					$('.change_pwd_block').show();
					$('#password').attr('name', 'password');
				} else {
					$('.change_pwd_block').hide();
					$('#password').attr('name', 'changed');
				}
			});
					   
		});
	 $(function() {
		var hospitalid=$("#hospitalid").val();
		$("#add_competition_form").validate({
			rules: {
				competition: "required",
				vname: {
					required: true,
					remote: {
						url: "<?php echo base_url();?>admin_panel/Venues/chk_vname",
						type: "post",
						data: {
							vname: function() { return $("#vname").val(); },
							venue_id:function(){ return $("#user_id").val(); }  
						}
					},
				},
				email: {
					required: true,
					email: true,
					remote: {
						url: "<?php echo base_url();?>admin_panel/Venues/chk_email",
						type: "post",
						data: {
							email: function() { return $("#email").val(); },
							venue_id:function(){ return $("#user_id").val(); }  
						}
					},
				},
				phone:{
					required: true,
					number: true
				},
				password: {
					required: true,
					minlength: 5
				},
				confirmPwd: {
					required: true,
					//minlength: 5,
					equalTo: "#pwd"
				},
				contact: "required",
				address: "required",
				status: "required",
			},
			messages: {
				competition: "Please select competition.",
				vname: {
					required: "Please enter a venue name.",
					remote:"venue name already exist, try another."
				},
				email: {
					required: "Please enter an email address.",
					email: "Please enter a valid email address.",
					remote:"Email already exist, try another."
				},
				phone: "Please enter phone number.",
				password: {
					required: "Please enter a password.",
					minlength: "Password should be at least 5 characters long."
				},
				confirmPwd:{
					required: "Please enter confirm password.",
					equalTo: "Both passwords should be same.",
				},
				contact: "Please enter contact.",
				address: "Please enter address.",				
				status: "Please select status.",
			},
			errorElement:"div",
			errorClass:"valid-error",
			submitHandler: function(form) {
				form.submit();
			}
		});
	});
	  
	 function cancelButton()
	  {
		location.assign("<?php echo base_url(); ?>snadmin/dashboard");
	  } 
	  
	  function acccountType()
	  {
	   var type=$('#role').val();
	   if(type=='therapist')
	   {
	    $('.forTherapist').show();
	    $('.forClient').hide();
	   }
	   else if(type=='client')
	   {
		 $('.forTherapist').hide();
	     $('.forClient').show();
	   }
	   else
	   {
	    $('.forTherapist').hide();
	    $('.forClient').hide();
	   }
	  }
	</script>

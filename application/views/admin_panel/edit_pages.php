	<div class="account-right-div">
		<div class="dashboard-heading"><h2><?php echo $page_title; ?></h2></div>
		<div class="dashboard-inner">
			<div class="main-dash-summry Edit-profile">
				<form id="pageeditform" name="pageeditform" method="post" action="">
					<div class="input-row">
						<div class="full">
							<div class="input-block">
								<label>Page Title:</label>
								<span class="reg_span"><input type="text" class="inputbox-main" value="<?php echo $user['page_title']; ?>" name="pagetitle" id="pagetitle"></span>
							</div>
						</div>
					</div>
					<div class="input-row">
						<div class="full">
							<div class="input-block">
								<label>Meta Description:</label>
								<span class="reg_span"><input type="text" class="inputbox-main" value="<?php echo $user['meta_description']; ?>" name="metadescription" id="metadescription"></span>
							</div>
						</div>
					</div>	
					<div class="input-row">
						<div class="full">
							<div class="input-block">
								<label>Meta Keywords:</label>
								<span class="reg_span"><input type="text" class="inputbox-main" value="<?php echo $user['meta_keywords']; ?>" name="metakeyword" id="metakeyword"></span>
							</div>
						</div>
					</div>	
					<div class="input-row">
						<div class="full">
							<div class="input-block">
								<label>Page Url:</label>
								<span class="reg_span"><input type="text" class="inputbox-main" value="<?php echo $user['page_url']; ?>" name="pageurl" id="pageurl"></span>
							</div>
						</div>
					</div>	
					<div class="input-row ckeditor-row">
						<div class="full">
							<div class="input-block">
								<label>Page Body:</label>
								<span class="reg_span ckeditor_span">
									<div>
										<?php
											$this->load->file(FCPATH."/assets/ckeditor/ckeditor.php",true);
											$ckeditor = new CKEditor();
											$ckeditor->basePath = base_url().'assets/ckeditor/';
											$ckeditor->config['width'] = 715;
											$ckeditor->editor('content1',$user['page_body']);
										?>
									</div>	
								</span>
							</div>
						</div>
					</div>	
					<!-- div class="input-row">
						<div class="full">
							<div class="input-block">
								<label>Status :</label>
								<span class='reg_span'>
									<select id='status' class="inputbox-main input_select"  name="status">
										<option value="">Select Status</option>
										<option value="1" <?php if($user['active'] == 1) { echo 'selected'; } ?>>Active</option>
										<option value="0" <?php if($user['active'] == 0) { echo 'selected'; } ?>>Inactive</option>
									</select>
								</span>
							</div>
						</div>
					</div -->
					<div class="input-row">
						<div class="full">
							<div class="input-block add-user-btn">
								<span class='reg_span reg_span_btn'>
									<input type="submit" value="Submit" class="btn-submit btn"> 
									<input type="button" value="Cancel" onclick="javascript:history.go(-1);" class="btn-submit btn"> 
								</span>
							</div>
						</div>	
					</div>
				</form>
			</div>
		</div>
	</div>

<script>
	 $(function() {
		// var hospitalid=$("#hospitalid").val();
		$("#pageeditform").validate({
			ignore: [],  
			rules: {
				pagetitle: "required",
				content1:
                {
                    required: function()
                    {
                        CKEDITOR.instances.content1.updateElement();
                    }
                }, 
			},
			messages: {
				pagetitle: "Please enter page title.",			
				content1: "Please enter page content.",
			},
			errorElement:"div",
			errorClass:"valid-error",
			submitHandler: function(form) {
				form.submit();
			}
		});
	});

</script>

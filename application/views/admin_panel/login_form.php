<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
		<title>Login</title>
		<script language="JavaScript" src="<?php print base_url();?>assets/js/jquery-1.7.1.min.js" type="text/javascript"></script>
		<script language="JavaScript" src="<?php print base_url();?>assets/js/gen_validatorv31.js" type="text/javascript"></script>
		<script language="JavaScript" src="<?php print base_url();?>assets/js/jquery.validate.js" type="text/javascript"></script>
		<script>
			$(function() {
				$("#email").focus();
				$("#login-form").validate({
					rules: {
						username: {
							required: true,
						},
						password: {
							required: true,
							
						},
					},
					messages: {
						password: {
							required: "Please enter password.",
							
						},
						username: {
							required: "Please enter username.",
						},
					},
					errorElement:"div",
					errorClass:"login-error",
					submitHandler: function(form) {
						form.submit();
					}
				});
			});
		</script>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/admin/style.css"/>
		<!-- script>
			setTimeout(function(){ $('#succ_flash').fadeOut('slow'); }, 4000);
		</script -->
	</head>
	<body class="admin_body login-body">
		<header>
			<div id="wrapper2">
				<div class="header-base">
					<div class="logo-div">
						<a href ="<?php echo base_url();?>"><img src="<?php echo base_url(); ?>assets/images/logo.png"/></a>
					</div>
				</div>
			</div>
		</header>
		<div class="border-div"></div>	
		<div class="regi-main <?php if($this->session->flashdata('success')!='' || $this->session->flashdata('fail')!='') {
			echo 'lognMsg';
		} ?>">
			<div class="regi-base login_main">
				<div class="messages">
					<?php if($this->session->flashdata('success')) {	?> 
						<div class="alert alert-success" id="succ_flash"><?php echo $this->session->flashdata('success');?></div>
					<?php } ?>	
				</div>
				<div class="message">
					<?php if($this->session->flashdata('error')) {?> 
						<div class="alert alert-error" id="succ_flash"><?php echo $this->session->flashdata('error');?></div>
					<?php } ?>
				</div>	
				<div class="login-box">
					<h2><img src="<?php echo base_url(); ?>assets/images/admin/login_icon.png"/><div class="login_title"><span>WHISPERTIP</span><p>Admin Panel</p></div></h2>
					<!-- form id="form" name="formlogin" method="post" action=""  -->
					<form method="post" action="" class="form-horizontal" id="login-form">
						<fieldset id="personal">
							<legend style="margin-left: 0; padding-left: 8px; text-align: center; width: 100%"></legend>
							<div class="input-prepend">      
								<label for="lastname"><?php print $this->lang->line('admin_label_username');?> :</label>
								<input name="username" id="username" type="text" tabindex="1"  style="width:250px;"/><br />
							</div>
							<div class="clearfix"></div>
							<div class="input-prepend">
								<label for="password"><?php print $this->lang->line('admin_label_password');?> :</label>
								<input name="password" id="password" type="password" tabindex="2" style="width:250px;"/><br />
							</div>
							<div class="button-login" >	
								<input id="button1" name="submit" type="submit" class="btn login" value="<?php  print $this->lang->line('admin_btn_login'); ?>"/>
							</div>
						</fieldset>
					</form>
				</div>			
			</div>	
			
		
		
		</div>
		<footer>
			<div class="wrapper">
				<p>Copyright&copy;2016 whispertip.com. All rights reserved </p>
			</div>
		</footer>
	</body>
</html>

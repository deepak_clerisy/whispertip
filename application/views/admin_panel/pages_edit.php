<div id="box">
	<h3>
		<?php print 'Edit Page';//@$section_heading; ?>
	</h3>
<br>
<script language="JavaScript" src="<?php print base_url();?>assets/js/gen_validatorv31.js" type="text/javascript"></script>
<style>
.table,td,tr
{
	border:none;
}
.redtext
{
 color:#FF0000;
}
</style>

<div class="yui-skin-sam">
<form name="pageeditform" method="post" action="">
<div class="redtext"><?php echo validation_errors(); ?></div>

<table width="100%" border="0" cellspacing="12">
  <tr>
    <td><b>Page Title:</b>&nbsp;<em class="redtext">*</em></td>
    <td>
        <input class="textbox" type="text" name="pagetitle" id="pagetitle" style="width:265px;" value="<?php print @$pages['page_title']; ?>">
    </td>
  </tr>
  <tr>
    <td valign="top"><b>Meta Description:</b>&nbsp;<em class="redtext">*</em></td>
    <td>      
       <textarea class="textbox" style="border:solid 1px #CCCCCC;width:269px;height: 127px;" name="metadescription" id="pagesubtitle" rows="3"><?php print @$pages['meta_description'];?></textarea>
      </td>
  </tr>
  <tr>
    <td valign="top"><b>Meta Keywords:</b></td>
    <td>
        <textarea style="border:solid 1px #CCCCCC;width:269px;" name="metakeywords" id="metakeywords" rows="3"><?php print @$pages['meta_keywords'];?></textarea>
    </td>
  </tr>
    <tr>
    <td><b>Page Url:</b>&nbsp;<em class="redtext">*</em></td>
    <td>
        <input readonly="readonly" class="textbox" type="text" name="page_url" id="page_url" style="width:265px;" value="<?php print @$pages['page_url']; ?>">
    </td>
  </tr>
  
  <tr>
  <td valign="top"><b>Page Body:</b></td>
  <td>
    
      <div>
		<?php
		$this->load->file(FCPATH."/assets/ckeditor/ckeditor.php",true);
		$ckeditor = new CKEditor();
		$ckeditor->basePath = base_url().'assets/ckeditor/';
		$ckeditor->config['width'] = 600;
		$ckeditor->editor('content1',$pages['page_body']);
		?>
		</div>	

	</td>
  </tr>
  
  
  <tr>
  <td><b>Active:</b></td>
  <td><label>
  <?php $chk=@$pages['active'];?>
    <input class="checkbox" type="checkbox" name="active" id="active"  value="1" <?php if($chk == 1) print 'checked="checked"';?>/>
  </label></td>
  </tr>
  <tr>
  <td></td>
  <td >
    <input type="submit" value="Edit" class="button" style="width:55px;"/>&nbsp;&nbsp;
    <input  type="hidden" name="editpages" id="editpages" value="editpages" />
    &nbsp;&nbsp;
    <input class="button"  type="button" name="btncancel" style="width:65px;" id="btncancel" value="Cancel"
     onClick="javascript:document.location.href='<?php echo base_url();?>admin_panel/content/index';"/>
    
  </td>
  </tr>
 </table>
</form>
</div>

</div>
<script type="text/javascript">
	
var frmvalidator = new Validator("pageeditform");
frmvalidator.addValidation("pagetitle","req","Field page title is required.");
frmvalidator.addValidation("pagesubtitle","req","Field meta description is required.");
frmvalidator.addValidation("page_url","req","<?php print 'Field page url is required';?>");
frmvalidator.addValidation("page_url","regexp=^[A-Za-z._-]*$","<?php print 'only alphabets and ._- are allowed';?>");


//document.getElementById("old_password").focus();


</script>

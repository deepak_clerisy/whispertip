<?php
$tab = $this->uri->segment(2);
$current = 'class="current"';

if($tab == 'home')
{
	$selecthome = $current;
}
else if($tab == 'content')
{
	$selectcontent = $current;
}
if($tab == 'email')
{
	$selectemail = $current;
}
if($tab == 'category')
{
	$selectcategory = $current;
}
if($tab == 'navigation')
{
	$selectnavigation = $current;
}
if($tab == 'results')
{
	$selectresult = $current;
}

?>

   


    <!-- Left Menu Starts Here -->
        <div id="wrapper">
			<div id="sidebar">
					<ul>
						   <li><h3><a href="<?php print base_url();?>admin_panel/home" class="house">Dashboard</a></h3>
						   <li><a href="<?php print base_url();?>admin_panel/home/change_password" class="arrow">Change Password</a></li> 
					</ul> 
					<ul>
						   <li><h3><a href="<?php print base_url();?>admin_panel/content" class="content">Content</a></h3>
						   <li><a href="<?php print base_url();?>admin_panel/content" class="arrow">Manage Pages</a></li>
						</li>    
					</ul> 
					<ul>
                	   <li><h3><a href="<?php print base_url();?>admin_panel/email" class="email">Email</a></h3>
                       <li><a href="<?php print base_url();?>admin_panel/email" class="arrow">Manage Email Templates</a></li>
                       <li><a href="<?php print base_url();?>admin_panel/email/manage_newsletter" class="arrow">Manage Newsletter</a></li>
						</li>    
					</ul> 
					<ul>
                	   <li><h3><a href="<?php print base_url();?>admin_panel/category" class="content">Category</a></h3>
                       <li><a href="<?php print base_url();?>admin_panel/category" class="arrow">Manage Category</a></li>
						</li>    
					</ul> 
					<ul>
                	   <li><h3><a href="<?php print base_url();?>admin_panel/category" class="content">Navigation</a></h3>
                       <li><a href="<?php print base_url();?>admin_panel/category" class="arrow">Manage Navigation</a></li>
						</li>    
					</ul> 
					
			</div>
		</div>	
    <!-- Left Menu Ends Here -->

<?php 
	$sort_arrow_uname='fa fa-angle-down menu-down';
	$sort_arrow_name='fa fa-angle-down menu-down';
	$sort_arrow_email='fa fa-angle-down menu-down';
	$sort_arrow_accType='fa fa-angle-down menu-down';
	//~ $page  = isset($page) && $page != 0 ? $page : 0;
	//~ $srno = isset($offset) ? $offset+1 : 1;
	if(isset($search) && $search != '')
	{
		if( $search == '=' ) {
			$search = '';
		} else {
			$search = $search;
		}
	} 
	else
	{
		$search = '';
	}
	
	$page_num = (int)$this->uri->segment(4);
	if($page_num == 0) $page_num=1;
	$order_seg = $this->uri->segment(5,"asc"); // if the 5th segment not present,it will return asc. default value.

	$name_seg = $this->uri->segment(4); // if the 5th segment not present,it will return asc. default value.
	if($order_seg == "asc"){ 
		$order = "desc"; 
	}else {
		$order = "asc";
	}
	//$order = isset($order) && $order == 'asc' ? 'desc' : 'asc';
	$sort_arrow_email = isset($sort) && $sort=='email' ? ($order == 'asc' ? 'fa fa-angle-down menu-down' : 'fa fa-angle-up menu-down'): 'fa fa-angle-down menu-down';
	if($order == 'asc'){
		if( $name_seg== 'username'){
			$sort_arrow_uname = 'fa fa-angle-down menu-down';
		}elseif($name_seg== 'first_name'){
			$sort_arrow_name = 'fa fa-angle-down menu-down';		
		}elseif($name_seg== 'email'){
			$sort_arrow_email = 'fa fa-angle-down menu-down';
		}
	}else{
		if( $name_seg== 'username'){
			$sort_arrow_uname = 'fa fa-angle-up menu-down';
		}elseif($name_seg== 'first_name'){
			$sort_arrow_name = 'fa fa-angle-up menu-down';		
		}elseif($name_seg== 'email'){
			$sort_arrow_email = 'fa fa-angle-up menu-down';
		}
	}
	// $sort_arrow_uname = isset($sort) && $sort=='username' ? ($order == 'asc' ? 'fa fa-angle-down menu-down' : 'fa fa-angle-up menu-down'): 'fa fa-angle-down menu-down';
	?>
<div id="message-green" style="display:none;"><table cellspacing="0" cellpadding="0" style="width:600px;border:0px;"><tbody><tr><td style="border:0px;" class="green-left">User status has been updated succesfully.</td><td style="padding:0px;border:0px;" class="green-right"><a onclick="CloseMessage()" class="close-green"><img align="left" style="margin:0px;" src="<?php echo base_url();?>/assets/css/admin/img/close_green.png" alt=""></a></td></tr></tbody></table></div>
<div id="message-green1" style="display:none;"><table cellspacing="0" cellpadding="0" style="width:600px;border:0px;"><tbody><tr><td style="border:0px;" class="green-left">User has been deleted succesfully.</td><td style="padding:0px;border:0px;" class="green-right"><a onclick="CloseMessage()" class="close-green"><img align="left" style="margin:0px;" src="<?php echo base_url();?>/assets/css/admin/img/close_green.png" alt=""></a></td></tr></tbody></table></div>
<?php if($this->session->flashdata('success')) { ?>
	<div id="message-green">
		<table cellspacing="0" cellpadding="0" style="width:600px;border:0px;">
		<tbody><tr>
			<td style="border:0px;" class="green-left"><?php echo $this->session->flashdata('success'); ?></td>
			<td style="padding:0px;border:0px;" class="green-right">
			<a onclick="CloseMessage()" class="close-green"><img align="left" style="margin:0px;" src="<?php echo base_url();?>/assets/css/admin/img/close_green.png" alt=""></a>
			</td>
		</tr>
		</tbody></table>
	</div>
<?php } ?>
<?php if($this->session->flashdata('error')) { ?>
	<div id="message-red">
		<table cellspacing="0" cellpadding="0" style="width:600px;border:0px;">
		<tbody><tr>
			<td style="border:0px;" class="red-left"><?php echo $this->session->flashdata('error'); ?></td>
			<td style="padding:0px;border:0px;" class="red-right">
			<a onclick="CloseMessage()" class="close-red"><img align="left" style="margin:0px;" src="<?php echo base_url();?>/assets/css/admin/img/close_red.png" alt=""></a>
			</td>
		</tr>
		</tbody></table>
	</div>
<?php } ?>

	<div class="dashboard-heading">
		<h2><?php echo $page_title; ?></h2>
	</div>
	<div class="dashboard-inner">
		<div class="dash-search user_search">
			<form action="<?php echo $this->config->item('admin_url'); ?>/users/index" method = "post">
				<input type='text' name='keyword' id='search' value='<?php echo $search; ?>' class='list-search' placeholder='Username or Email' />
				<input type="submit" value = "Search" />
			</form>
			
			<a href = "<?php echo $this->config->item('admin_url'); ?>/users/add" class="btn-submit btn" >Add Patron</a>
		</div>

		<div class="main-dash-summry Edit-profile nopadding11">
			<div class="my_table_div">
				<table class="fixes_layout">
					<thead>
						<tr>
							<th class="forWidthSno"> <h1 class=""> S. No. </h1> </th>
							<th>
								<a class='underline_classs' href='<?php echo $this->config->item('admin_url'); ?>/users/index/username/<?php echo $order; ?>/<?php echo $search; ?>/<?php echo $page_num; ?>'>
								<h1 class="">
									User Name <i class="<?php echo $sort_arrow_uname; ?>"></i>
								</h1>
								</a>
							</th>
							<th>
								<a class='underline_classs' href='<?php echo $this->config->item('admin_url'); ?>/users/index/first_name/<?php echo $order; ?>/<?php echo $search; ?>/<?php echo $page_num; ?>'>
								<h1 class="">
									Name <i class="<?php echo $sort_arrow_name; ?>"></i>
								</h1>
								</a>
							</th>
							<th class="forWidth">
								<a class='underline_classs' href='<?php echo $this->config->item('admin_url'); ?>/users/index/email/<?php echo $order; ?>/<?php echo $search; ?>/<?php echo $page_num; ?>'>
								<h1 class=""> 
									Email <i class="<?php echo $sort_arrow_email; ?>"></i>
								</h1>
								</a>
							</th>
							<th><h1 class=''>Gender</h1></th>
							
							<th style="width:10%"> <h1 class="" > Status </h1> </th>
							<th> <h1 class=""> Actions </h1> </th>
						</tr>
					</thead>
					<tbody>
						<?php 
							if( !empty($user_listing) ) {
								$page = $pag;
								$srno=1;
								if(isset($page) && !empty($page) && $page !=1 )
								{
								 $srno=($page-1)*$per_page+1;
								}
								
								foreach($user_listing as $user){
									if($user['status']  == 1) {
										$img = base_url().'assets/images/admin/status-active.png';
										$img_title="Active";
									} else {
										$img = base_url().'assets/images/admin/status-inactive.png';
										$img_title="Inactive";
									}
									?>
									<tr>
										<td><?php echo $srno; ?></td>
										<td class="wide">
											<span data-toggle="tooltip" title="<?php echo $user['username']; ?>" >
												<?php echo $user['username']; ?>
											</span>
										</td>
										<td class="wide">
											<span data-toggle="tooltip" title="<?php echo $user['first_name'].' '.$user['last_name']; ?>" >
												<?php echo $user['first_name'].' '.$user['last_name']; ?>
											</span>
										</td>
										<td class="email_word"><?php echo $user['email']; ?></td>
										<td><?php echo $user['gender']; ?></td>
										<td id="user_status<?php echo $user['id']; ?>">
											<a  href="javascript:void(0)"  onclick="change_status(<?php echo $user['id']; ?>)">
												<img src='<?php echo $img;?>' title='<?php echo $img_title; ?>' style="cursor:pointer"/>
											</a>
										</td>
										<td class="action-main-block">
											<a title="Edit User" class="edit" href='<?php echo $this->config->item('admin_url'); ?>/users/edit/<?php echo $user['id']; ?>'>&nbsp;</a>
											<a title="Delete User" onclick="delete_user(<?php echo $user['id']; ?>)" href='javascript:void(0)' class="del" >&nbsp;</a>
											<a href="<?php echo $this->config->item('admin_url'); ?>/tips/view/<?php echo $user['id']; ?>" title="View Tipping" class="zoom" >&nbsp;</a>
										</td>
									</tr>
							<?php $srno++; } } else { ?>
							<tr>
								<td colspan="5" style='text-align:center;'>No result found.</td>
							</tr>
							<?php } ?>
					</tbody>
				</table>
					<!--end-->
			</div><!-- my_table_div -->
			<div class='pagination' style='text-align:center'>
				<?php echo $this->pagination->create_links(); ?>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function change_status(id) {
			$.ajax({
				type:'POST',
				url:'<?php echo $this->config->item('admin_url'); ?>/users/change_status',
				data:{'id':id},
				success : function(data) {
					if(data == 'inactive') {
						$('#user_status'+id+' a').html('<img src="<?php echo base_url();?>assets/images/admin/status-inactive.png" title="Inactive" style="cursor:pointer"/>');
						$('#message-green').css('display','block');
					} else if(data == 'active')	{
						$('#user_status'+id+' a').html('<img src="<?php echo base_url();?>assets/images/admin/status-active.png" title="Active" style="cursor:pointer"/>');
						$('#message-green').css('display','block');				
					}
					setTimeout(function(){ $('#message-green').fadeOut('slow'); }, 4000);
				}
			});
		}


		function delete_user(id) {
			var check = confirm('Are you sure you want to delete user');
			if( check == true ){
				$.ajax({
					type:'POST',
					url:'<?php echo $this->config->item('admin_url'); ?>/users/delete',
					data:{'id':id},
					success : function(data){
						if(data == 'deleted') {
							location.reload(true)
							$('#message-green1').css('display','block');				
						} else if(data == 'notdeleted'){
							location.reload(true)
						}
						//setTimeout(function(){ $('#succ_flash').fadeOut('slow'); }, 4000);
					}
				});
			} else {
				return false;
			}
		}
	</script>

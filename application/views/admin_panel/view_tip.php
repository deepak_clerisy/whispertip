<?php
	$user_id = $this->uri->segment(4);
	$venu_id = $this->input->get('venue_id');
	$comp = $this->input->get('competition');
	$round = $this->input->get('rounds');
?>

	<div class="dashboard-heading">
		<h2><?php echo $page_title; ?></h2>
	</div>
	<div class="dashboard-inner">
		<div class="dash-search user_search">
			<div class="input-view-row">
				<div class="full">
					<div class="input-block">
						<form id="view-tip-form" class="view-tip-form" name="view-tip-form" action="<?php echo $this->config->item('admin_url'); ?>/tips/view/<?php echo $user_id ; ?>/1/search" method = "get">	
							<span class="reg_span tip_option">
								
								<select id="venue_id" class="inputbox-main input_select" name="venue_id">
								<option value="">Select Venue</option>
								<?php  
									foreach($venue as $ven){
										$selected = ( $ven['venue_id'] == $sel_venue ) ? 'selected' : '';
										echo '<option value='.$ven['venue_id'].' '.$selected.'>'.$ven['venue_name'].'</option>';
									}
								?>
								</select>
							</span>
							
							<span class="reg_span tip_option">
								<select id="competition" class="inputbox-main input_select"  name="competition" onChange="getRound(this.value)";>
									<option value="">Select Competition</option>
									<option value="AFL" <?php if($comp =='AFL') { echo 'selected'; } ?> >AFL</option>
									<option value="NRL" <?php if($comp =='NRL') { echo 'selected'; } ?> >NRL</option>
								</select>
							</span>
							
							<span class="reg_span tip_option venue_span">
								<select id="rounds" class="inputbox-main input_select" name="rounds">
									<option value="">Select Round</option>
								</select>
							</span>
							<input type="hidden" value="<?php echo $user_id ?>" >
							<input type="submit" value = "Search" />
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="main-dash-summry Edit-profile nopadding11">
			<div class="my_table_div">
				<table class="fixes_layout">
					<thead>
						<tr>
							<th class="forWidthSno"><h1 class="">S. No.</h1> </th>
							<th><h1 class="">Competition</h1></th>
							<th><h1 class="">Venue</h1> </th>
							<th class="forWidth"><h1 class="">Round</h1></th>
							<th><h1 class=''>Your Choice</h1></th>
							<!-- th style="width:10%"><h1 class="">Status</h1></th -->
							<th><h1 class="">Actions</h1></th>
						</tr>
					</thead>
					<tbody>
						<?php 
							if( !empty($user_listing) ) {
								
								$page = $page_number;
								$srno=1;
								if(isset($page) && !empty($page) && $page !=1 )
								{
									$srno=($page-1)*$per_page+1;
								}
								
								foreach($user_listing as $user){
									if($user['status']  == 1) {
										$img = base_url().'assets/images/admin/status-active.png';
										$img_title="Active";
									} else {
										$img = base_url().'assets/images/admin/status-inactive.png';
										$img_title="Inactive";
									}
									?>
									<tr>
										<td><?php echo $srno; ?></td>
										<td class="wide">
											<span data-toggle="tooltip" title="<?php echo $user['competition']; ?>" >
												<?php echo $user['competition']; ?>
											</span>
										</td>
										<td class="wide">
											<span data-toggle="tooltip" title="<?php echo $user['venue_id'] ; ?>" >
												<?php echo $user['venue_name']; ?>
											</span>
										</td>
										<td class="email_word"><?php echo $user['round']; ?></td>
										<td><?php echo $user['selected_team']; ?></td>
										<!-- td id="user_status<?php echo $user['id']; ?>">
											<a  href="javascript:void(0)"  onclick="change_status(<?php echo $user['id']; ?>)">
												<img src='<?php echo $img;?>' title='<?php echo $img_title; ?>' style="cursor:pointer"/>
											</a>
										</td -->
										<td class="action-main-block">
											<!-- a title="Edit User" class="edit" href='<?php echo $this->config->item('admin_url'); ?>/users/edit/<?php echo $user['id']; ?>'>&nbsp;</a -->
											<a title="Delete Tip" onclick="delete_user(<?php echo $user['tip_id']; ?>)" href='javascript:void(0)' class="del" >&nbsp;</a>
											<!-- a href="<?php echo $this->config->item('admin_url'); ?>/tips/view/<?php echo $user['id']; ?>" title="View Tipping" class="zoom" >&nbsp;</a -->
										</td>
									</tr>
							<?php $srno++; } } else { ?>
							<tr>
								<td colspan="5" style='text-align:center;'>No result found.</td>
							</tr>
							<?php } ?>
					</tbody>
				</table>
					<!--end-->
			</div><!-- my_table_div -->
			<div class='pagination' style='text-align:center'>
				<?php echo $this->pagination->create_links(); ?>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	
		$(document).ready(function () {
		
			var com_val =  $('#competition').val();
			$.ajax({
				type:'POST',
				url:'<?php echo $this->config->item('admin_url'); ?>/tips/change_rounds',
				data:{'competition':com_val},
				success : function(data) {
					$('#rounds').html(data);
					var rid = '<?php echo $round ?>';
					$('#rounds').val(rid);
					
				}
			});
			
			var venue = '<?php echo $venu_id ?>';
			$('#venue_id').val(venue);			
			
		});
		function change_status(id) {
			$.ajax({
				type:'POST',
				url:'<?php echo $this->config->item('admin_url'); ?>/users/change_status',
				data:{'id':id},
				success : function(data) {
					if(data == 'inactive') {
						$('#user_status'+id+' a').html('<img src="<?php echo base_url();?>assets/images/admin/status-inactive.png" title="Inactive" style="cursor:pointer"/>');
						$('#message-green').css('display','block');
					} else if(data == 'active')	{
						$('#user_status'+id+' a').html('<img src="<?php echo base_url();?>assets/images/admin/status-active.png" title="Active" style="cursor:pointer"/>');
						$('#message-green').css('display','block');				
					}
					setTimeout(function(){ $('#message-green').fadeOut('slow'); }, 4000);
				}
			});
		}

		function delete_user(id) {
			var check = confirm('Are you sure you want to delete user');
			if( check == true ){
				$.ajax({
					type:'POST',
					url:'<?php echo $this->config->item('admin_url'); ?>/users/delete',
					data:{'id':id},
					success : function(data){
						if(data == 'deleted') {
							location.reload(true)
							$('#message-green1').css('display','block');				
						} else if(data == 'notdeleted'){
							location.reload(true)
						}
						//setTimeout(function(){ $('#succ_flash').fadeOut('slow'); }, 4000);
					}
				});
			} else {
				return false;
			}
		}
		
		function getRound(comp) {
			$.ajax({
				type:'POST',
				url:'<?php echo $this->config->item('admin_url'); ?>/tips/change_rounds',
				data:{'competition':comp},
				success : function(data) {
					//alert(data);
					$('#rounds').html(data);
				}
			});
		}  	
		
		
	</script>

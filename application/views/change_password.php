<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/basketball.css">
<!-- ***************************TWITTER CONNECT************************* ---------->

<div class="rugby blue">
	<div class="container">
		
		<div class="main_outer">
		<div class="content">
			


				<center class="logo-margin"><img class='inner-pages-image' src="<?php echo base_url().'assets/images/logo.png'; ?>"/></center>		
			
				<div class="sort"><!-- span>sort:</span>    <a href="#">OVERALL RANKING</a>   <a href="#">STRIKE RATE</a>   <a href="#">BEST OVER LAST 5</a --></div>
		<?php echo $sidebar; ?>
			
			<div class="right_content" id="page_content">
			
				<div class="blog coach_blog">
					
					<div class="but_tip_coach">
						<form id="change_front_password" name="editProfilefrm" method="post" action="">
							<div class="widthdraw">
							<div class="widthdraw_heading">Change Password</div>
							<div class="pwd_form">
									<label>Current Password</label>
									<div class="pwd_input">
										<input type="password" name="current_password" class="input_txt" />
									</div>
							</div>
							<div class="pwd_form">
									<label>New Password</label>
									<div class="pwd_input">						
									<input type="password" name="new_password" class="input_txt" id ="new_password" />	</div>
							</div>
							<div class="pwd_form">
									<label>Confirm Password</label>	
									<div class="pwd_input">					
									<input type="password" name="confirm_password"  class="input_txt"  />	</div>
							</div>
							<div class="pwd_form btn_profile-padding">
								<label></label>
								<input type="submit" value="Save Changes" style="margin-top:5px;margin-top:6px;" class="buy_tip pull-left submit-class">
								<a href="<?php echo base_url(); ?>home/user_profile" class="buy_tip pull-left" style="margin-left:5px;margin-top:5px;">Cancel</a></div>
						</form>
						</div>
					</div>
				</div>
						
				</div>
			</div>
		</div>
	</div>
</div>


<script>
 $(function() {
	$("#change_front_password").validate({
					rules: {
						current_password: "required",
						new_password:{
							required: true,
							minlength: 5
						},
						confirm_password: {
							equalTo: "#new_password"
						},
						
					},
					messages: {
						current_password: "Please enter current password.",
						new_password: {
							required: "Please enter new password.",
							minlength: "Password should be at least 5 characters long."
						},
						
						confirm_password:{
							equalTo: "Both passwords should be same.",
						},
					},
			errorElement:"div",
			errorClass:"valid-error",
			submitHandler: function(form) {
				form.submit();
			}
		});
	});
</script>


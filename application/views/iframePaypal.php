<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-1.7.1.min.js"></script>
<script src="https://www.paypalobjects.com/js/external/dg.js" type="text/javascript"></script>
</head>

<body>
<form action="https://www.sandbox.paypal.com/webapps/adaptivepayment/flow/pay" target="PPDGFrame" class="standard">


<input type="image" id="paypalSbBtn" value="Pay with PayPal" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif" style="display:none;">
<input id="type" type="hidden" name="expType" value="mini">
<input id="paykey" type="hidden" name="paykey" value="<?php echo $payKey; ?>">
</form>
<script type="text/javascript" charset="utf-8">
var www_root = '<?php echo base_url(); ?>';
//var dgFlowMini = new PAYPAL.apps.DGFlowMini({trigger: 'paypalSbBtn',callbackFunction: 'returnFromPayPal'});
var embeddedPPFlow = new PAYPAL.apps.DGFlow({trigger: 'paypalSbBtn',callbackFunction: 'returnFromPayPal'});
$('#paypalSbBtn').trigger('click');
</script>

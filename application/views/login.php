﻿<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Login</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<script src="js/html5.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style_new.css">
	<script language="JavaScript" src="<?php print base_url();?>assets/js/jquery-1.7.1.min.js" type="text/javascript"></script>
	<script language="JavaScript" src="<?php print base_url();?>assets/js/gen_validatorv31.js" type="text/javascript"></script>
	<script language="JavaScript" src="<?php print base_url();?>assets/js/jquery.validate.js" type="text/javascript"></script>        
	<script src="<?php echo base_url() ?>assets/js/new_design/bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/miscellaneous.js"></script>
	<script>
		$(document).ready(function() {
			$("#loginfm").validate({
				rules: {
					lguserName: {
						required: true,
					},
					lguserPassword: {
						required: true,
						
					},
				},
				messages: {
					lguserName: {
						required: "Please enter password.",
						
					},
					lguserPassword: {
						required: "Please enter username.",
					},
				},
				errorElement:"div",
				errorClass:"login-error",
				submitHandler: function(form) {
					form.submit();
				}
			});
		});
	</script>
</head>
<body class="bg_image">
<div class="home">
	<div class="container">
		<div class="login_outer">
			<center><img src="<?php echo base_url() ?>assets/images/login_logo.png"/></center>
			<div class="login">
				<div id="userLoginError" class="entry_row" style="margin-bottom:4px;color:red;"></div>
					<form name="loginfm" id="loginfm" method="post">
						<input type="text" name="lguserName" id="lguserName" placeholder="User Name or Email" onkeypress="checkButton(event)"/>
						<input type="password" name="lguserPassword" id="lguserPassword" placeholder="Password" onkeypress="checkButton(event)"/>
						<!-- input id="lgloginBtn" name="submit" type="submit" class="btn login" value="Login"/ -->
						<a href="#" class="login_btn"  id="lgloginBtn" style="float:left;width:76px;">Login</a>
						<a href="#" class="login_btn" data-uri="<?php echo base_url(); ?>" id="reg_cancel" style="float:left;width:76px;margin-left:60px;" name="cancelButtn">Cancel</a>
						<input type="checkbox" style="display:inline-block; float:left;margin-top:6px;"/><span>Remember me</span>
						<a href="#" class="pull-right forgot">Forgot your password?</a>
						<input type="hidden" value="<?php echo base_url();?>" id="url" name="url">
					</form>
				<div class="social_support" style="margin-top:200px;">
					Social sports Tipping EXCHANGE
				</div>
				<div class="social_support" style="font-family: 'tradegothicbold_condensed';">
					GET AHEAD OF THE GAME
				</div>

			</div>
		</div>
	</div>
</div>
</body>
</html>

<div class="rugby blue">
	<div class="container">
		<div class="main_outer">
		<div class="content">
			<center><img class='inner-pages-image' src="<?php echo base_url().'assets/images/logo.png'; ?>"/></center>		
			<div class="sort"></div>
		
			<?php echo $sidebar; ?>
			
			<div class="right_content" id="page_content">
				<div class="blog coach_blog">
					<div class="but_tip_coach">
					<form id="select_venue_frm" name="select_venue_frm" method="post" action="">
						<div class="widthdraw">
							<div class="widthdraw_heading">Select Venue :</div>
							
							<input type="hidden" value="<?php echo $userData['id']; ?>" id="user_id" />
							<?php
							$dropdown = '';
							$dropdown .= "<option value=''>Select Venue</option>";
								if( !empty($venue_list) ) {
									foreach( $venue_list as $venues ) {
									
										$sel_venue = ($venues['venue_id'] == $userData['main_venue'] ) ? 'selected' : '';
									
										$dropdown .= "<option value='".$venues['venue_id']."' ".$sel_venue.">".$venues['venue_name']."</option>";
									}
									
								} 
							?>
							
							<div class="venue-selet">
								<select name='main_venue' id ='main_venue' >
										<?php echo $dropdown; ?>
								</select>
							</div>
							
							<div class="venue-btn">
								<input type="submit" class="buy_tip submit-class" style="margin-top:5px;margin-top:6px;" value="SUBMIT" />
							</div>
							
						</div>
					</form>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>	
</div>

<script>
$(function() {
	$("#select_venue_frm").validate({
		rules: {
			main_venue: "required",
		},
		messages: {
			main_venue: "Please select venue.",
		},
			errorElement:"div",
			errorClass:"valid-error",
			submitHandler: function(form) {
				form.submit();
			}
		});
	});
</script>

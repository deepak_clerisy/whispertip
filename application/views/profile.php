<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/basketball.css">
<!-- ***************************TWITTER CONNECT************************* ---------->
<?php
			if($userData['twitter_token'])
			{
				$twitterdata = $this->session->userdata('twitterconnectsess'); 
				if(isset($twitterdata['statuses_count'])) 
				{
					$tweets =  $twitterdata['statuses_count'];
				}
				if(isset($twitterdata['followers_count']))
				{
					$followers =  $twitterdata['followers_count'];
				}
				if(isset($twitterdata['friends_count']))
				{
					$following =  $twitterdata['friends_count'];
				}
		     }
			else
			{
				$img_connect = 'blog_icon1.png';
			}
?>
<div class="rugby blue">
	<div class="container">
	
		<div class="main_outer">
		<div class="content">
			


				<center><img class='inner-pages-image' src="<?php echo base_url().'assets/images/logo.png'; ?>"/></center>		
			
				<div class="sort"><!-- span>sort:</span>    <a href="#">OVERALL RANKING</a>   <a href="#">STRIKE RATE</a>   <a href="#">BEST OVER LAST 5</a --></div>
		
			<?php echo $sidebar; ?>
			
			<div class="right_content" id="page_content">
			
				<div class="blog coach_blog">
					
					<div class="but_tip_coach">
					<form id="editProfilefrm" name="editProfilefrm" enctype="multipart/form-data" method="post" action="">
					<div class="widthdraw">
						<div class="widthdraw_heading">Edit Profile</div>
						
						<input type="hidden" value="<?php echo $userData['id']; ?>" id="user_id" />
						
						<div class="profile_form">
							<label>First Name</label>
							<div class="profile_input"><input type="text" name="first_name" class="input_txt" value="<?php echo $userData['first_name']; ?>" /></div>
						</div>
						
						<div class="profile_form">
							<label>Last Name</label>						
							<div class="profile_input"><input type="text" name="last_name" class="input_txt"  value="<?php echo $userData['last_name']; ?>" /></div>
						</div>
						
						<div class="profile_form">
							<label>Username</label>						
							<div class="profile_input"><input type="text" name="username"  class="input_txt" readonly= "readonly" value="<?php echo $userData['username']; ?>" /></div>
						</div>
						
						<div class="profile_form">
							<label>Email</label>						
							<div class="profile_input"><input type="text" name="email"  id="email" class="input_txt" value="<?php echo $userData['email']; ?>" /></div>
						</div>
						
						<div class="profile_form">
							<label>profile picture</label>						
							<div class="profile_input"><input type="file" class="input_txt" style="display:inline-block; padding:0px;" name="profilePic" id="profilePic"/></div>
						<?php if( $userData['profile_image']!='' )
							{
								if($userData['profile_image']!='' && file_exists('uploads/profile_picture/'.$userData['profile_image']))
								{
									$image =   "<img src='".base_url().'uploads/profile_picture/'.$userData['profile_image']."' style='height:142px;width:140px;'/>";
								}
								//echo $image;
							} ?>
							
							<input type="hidden" name="profile_image1" value= <?php echo $userData['profile_image']; ?> />
						</div>
						
						<div class="profile_form"><label>Gender</label>						
							<div class="profile_input">
								<input type="radio" name="gender" class="input_txt" value="Male"  <?php if( $userData['gender'] == 'Male') { echo "checked"; } ?> />	<label class="option">&nbsp;Male</label>
							
								<input type="radio" name="gender" class="input_txt" value="Female" <?php if( $userData['gender'] == 'Female') { echo "checked"; } ?> /><label class="option">&nbsp;Female</label>
							</div>
						</div>
						
						<div class="profile_form">	
							<label>Country</label>		
							<div class="profile_input"><input type="text" name="country"  class="input_txt" value="<?php echo $userData['country']; ?>" /></div>
						</div>
						
						<div class="profile_form">
							<label>Phone No.</label>		
							<div class="profile_input">
							<?php $phone = ( $userData['phone'] != '' && $userData['phone'] != 0 ) ? $userData['phone'] : ''; ?> 
							<input type="text" name="phone"  class="input_txt" value="<?php echo $phone; ?>" maxlength='12' />
							</div>
						</div>
				
						<div class="profile_form">
							<label>DOB</label>		
							<div class="profile_input">	<input type="text" id="datepicker"  name="dob"  class="input_txt" value="<?php echo $userData['dob']; ?>"  data-val="true"  /></div>
						</div>
						
						<div class="profile_form">
							<label>Address</label>		
							<div class="profile_input">	<input type="text" name="address"  class="input_txt"  value="<?php echo $userData['address']; ?>" /></div>
						</div>
						
						
						<div class="btn_profile">
						<input type="submit" class="buy_tip pull-left submit-class" style="margin-top:5px;margin-top:6px;" value="Save Changes" />
						<a href="<?php echo base_url()?>home/user_profile" class="buy_tip pull-left" style="margin-left:5px;margin-top:5px;">Cancel</a>
						</div>
						</form>
						</div>
					</div>
				</div>
						
				</div>
			</div>
		</div>
	</div>
</div>
					
<script>
$(function() {
   $( "#datepicker" ).datepicker({
		changeMonth: true,
        changeYear: true,
        dateFormat: 'yy-mm-dd'
        });
	/*$('#datepicker').datepicker(
		{ dateFormat: 'y-m-d' }
	);*/
   });
   
 $(function() {
	$("#editProfilefrm").validate({
					rules: {
						first_name: "required",
						last_name: "required",
						country: "required",
						phone: "number",
						email: {
							required: true,
							email: true,
							remote: {
								url: "<?php echo base_url();?>home/chk_email_exist",
								type: "post",
								data: {
									email: function() { return $("#email").val(); },
									id:function(){ return $("#user_id").val(); }  
								}
							},
						},
					},
					messages: {
						first_name: "Please enter first name.",
						last_name: "Please enter last name.",
						country: "Please enter country.",
						phone: "Please enter numeric value only.",
						email: {
							required: "Please enter an email address.",
							email: "Please enter a valid email address.",
							remote:"Email already exist. Please try another."
						},
						
					},
			errorElement:"div",
			errorClass:"valid-error",
			submitHandler: function(form) {
				form.submit();
			}
		});
	});
</script>

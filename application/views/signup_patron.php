﻿
	<script>
		$(document).ready(function() {
			$("#signup-form").validate({
				rules: {
					username: {
						required: true,
						remote: {
								url: "<?php echo base_url();?>home/check_patron_username",
								type: "post",
								data: {
								username: function() {
									return $( "#username" ).val();
								  }
								}
							}
					},
					email: {
						required: true,
						email: true,
						remote: {
									url: "<?php echo base_url();?>home/check_patron_email",
									type: "post",
									data: {
									email: function() {
										return $( "#email" ).val();
									  }
									}
								}
						
					},
					first_name: {
						required: true,
						
						
					},
					last_name: {
						required: true,
						
						
					},
					password: {
						required: true,
						
					},
					cpassword: {
						required: true,
						equalTo:'#password'	
						
					},
					country: {
						required: true,
					},
				},
				messages: {
					username: {
						required: "Please enter username.",
						remote:"Username aleardy exists."
					},
					email: {
						required: "Please enter email.",
						email: "Please enter valid email.",
						remote:"Email aleardy exists."
					},
					first_name: {
						required: "Please enter first name.",
					},
					last_name: {
						required: "Please enter last name.",
					},
					password: {
						required: "Please enter password.",
					},
					cpassword: {
						required: "Please enter confirm password.",
						equalTo: "Both passwords should be same.",
					},
					country: {
						required: "Please enter country.",
					}
				},
				 errorElement:"div",
				errorClass:"login-error",
				submitHandler: function(form) {
					form.submit();
				}
			});
		});
	</script>

<div class="home">
	<div class="container">
		<div class="login_outer">
			<div class="login sign-fields">				
				<form method="post" id="signup-form">
					<div class="input-block input-height">
						<input type="text" placeholder="username" name="username" id="username"/>
					</div>
					<div class="input-block input-height">
						<input type="text" placeholder="your email" name="email" id="email"/>
					</div>
					<div class="input-block input-height">
						<input type="text" placeholder="first name" name="first_name" id="first_name">
					</div>
					<div class="input-block input-height">
						<input type="text" placeholder="last name" name="last_name" id="last_name">
					</div>
					<div class="input-block input-height">
						<input type="password" placeholder="password" name="password" id="password">
					</div>
					<div class="input-block input-height">
						<input type="password" placeholder="confirm password" name="cpassword" id="cpassword">
					</div>
					<div class="input-block input-height">
						<input type="text" placeholder="country" name="country" id="country">
					</div>
					<span class="criteria">By clicking below to sign up, you are agreeing to the Whispertip  TERMS OF SERVICES AND PRIVACY POLICY</span>
					<input type="submit" name="submit" id="submit" value="signup" class="login_btn sign" />
					<a href="javascript:void(0)" data-uri="<?php echo base_url() ?>" class="login_btn sign" name="cancel_signup" id="reg_cancel">cancel</a>
				</form>	
			</div>
		</div>
	</div>


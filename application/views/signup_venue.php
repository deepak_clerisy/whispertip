	<script>
		$(document).ready(function() {
			$("#signup-venue").validate({
				rules: {
					name: {
						required: true,
					},
					address: {
						required: true,
					},
					phone: {
						required: true,
						number: true
					},
					contact: {
						required: true,
						
					},
					email: {
						required: true,
						email: true,
						remote: {
									url: "<?php echo base_url();?>home/check_venue_email",
									type: "post",
									data: {
									email: function() {
										return $( "#email" ).val();
									  }
									}
								}
						
					},					
					password: {
						required: true,
						
					},
					cpassword: {
						required: true,
						equalTo:'#password'
						
					}
				},
				messages: {
					name: {
						required: "Please enter name.",
						
					},
					address: {
						required: "Please enter address.",
						
					},
					phone: {
						required: "Please enter phone.",
						number: "Please enter valid number.",
						
					},
					contact: {
						required: "Please enter contact.",
						
					},
					email: {
						required: "Please enter email.",
						email: "Please enter valid email.",
						remote:"Email aleardy exists."
						
					},					
					password: {
						required: "Please enter password.",
					},
					cpassword: {
						required: "Please enter confirm password.",
						equalTo: "Both passwords should be same.",
					},
				},
				 errorElement:"div",
				errorClass:"login-error",
				submitHandler: function(form) {
					form.submit();
				}
			});
		});
	</script>
<!--/head>
<body class="bg_image"-->
<div class="home">
	<div class="container">
		<div class="login_outer">
			<div class="login sign-fields">
				<form method="post" name="signup-venue" id="signup-venue">
					<div class="input-block input-height">	
						<input type="text" placeholder="name" name="name" id="name"/>
					</div>
					<div class="input-block input-height">
						<input type="text" placeholder="address" name="address" id="address"/>
					</div>
					<div class="input-block input-height">
						<input type="text" placeholder="phone" name="phone" id="phone"/>
					</div>
					<div class="input-block input-height">
						<input type="text" placeholder="contact" name="contact" id="contact"/>
					</div>
					<div class="input-block input-height">
						<input type="text" placeholder="email" name="email" id="email"/>
					</div>
					<div class="input-block input-height">
						<input type="password" placeholder="password" name="password" id="password"/>
					</div>
					<div class="input-block input-height">
						<input type="password" placeholder="confirm password" name="cpassword" id="cpassword"/>
					</div>
					
					<span class="criteria">By clicking below to sign up, you are agreeing to the Whispertip  TERMS OF SERVICES AND PRIVACY POLICY</span>
					<input type="submit" name="submit" id="submit" value="signup" class="login_btn sign" />
					<a href="javascript:void(0)" data-uri="<?php echo base_url() ?>" class="login_btn sign" name="cancel_signup" id="reg_cancel">cancel</a>
				</form>	
			</div>
		</div>
	</div>
</div>


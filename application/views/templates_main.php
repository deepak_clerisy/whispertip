<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <title><?php echo isset($title) ? $title : 'WhisperTip';  ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/football_title_new.ico" type="image/x-icon" />
		<link href="<?php echo base_url();?>assets/css/front/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/style_new.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/front/bootstrap.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/popModal.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/front/login_popup.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/admin/jquery-ui.css">
        
        <script src="<?php echo base_url() ?>assets/js/html5.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/js/ajax.googleapis.com.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
        
        <!--script src="http://code.jquery.com/jquery-1.11.0.min.js"></script-->
        <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.autocomplete.js"></script>
        <script src="<?php echo base_url() ?>assets/js/miscellaneous.js"></script>
       
		<script language="JavaScript" src="<?php print base_url();?>assets/js/jquery.validate.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>assets/js/jquery-ui.js"></script>

</head>
<script>
	  $(document).ready(function(){
		 $('.close-icon').on('click',function() 
		 {
			 $('.pop-up-outter').hide();
		 });
		 
		 $('#login-btn').on('click',function() 
		 { 
			 $('.login-pop').show();
		 });
		 
		 
		 $('#register-btn').on('click',function() 
		 {
			 $('.register-pop').show();
		 });
		 
		 setTimeout(function(){ $('.msg-top').fadeOut('slow'); }, 5000);
		 
	  });
</script>	


<!---------------pop up start--------------->
	
<div class="pop-up-outter login-pop">

	<div class="popup-main">
		
		<div class="popup-head">
			<p> Login as </p>
			<img class="popup-cross close-icon" src="<?php echo base_url(); ?>assets/images/cross-top.png"/>
		</div>
		
		<div class="popup-base">
			
			<div class="popup-left">
				<a class="pop-img" href="<?php echo base_url(); ?>login/venue">  </a>
				<div class="pop-btn-outer">
				<a class="popup-btn" href="<?php echo base_url(); ?>login/venue">  Venue </a>
				</div>
				<div class="pop-or"> <p> or </p> </div>
			</div>
			
			<div class="popup-right">
				<a class="pop-img pop-img2" href="<?php echo base_url(); ?>login/patron">  </a>
				<div class="pop-btn-outer">
				<a class="popup-btn" href="<?php echo base_url(); ?>login/patron">  Patron </a>
				</div>
				
			</div>
			
		</div>
		
	</div>

</div>



<div class="pop-up-outter register-pop" >

	<div class="popup-main">
		
		<div class="popup-head">
			<p> Sign up as </p>
			<img class="popup-cross close-icon" src="<?php echo base_url(); ?>assets/images/cross-top.png"/>
		</div>
		
		<div class="popup-base">
			
			<div class="popup-left">
				<a class="pop-img" href="<?php echo base_url(); ?>registration/venue">  </a>
				<div class="pop-btn-outer">
				<a class="popup-btn" href="<?php echo base_url(); ?>registration/venue">  Venue </a>
				</div>
				<div class="pop-or"> <p> or </p> </div>
			</div>
			
			<div class="popup-right">
				<a class="pop-img pop-img2" href="<?php echo base_url(); ?>registration/patron">  </a>
				<div class="pop-btn-outer">
				<a class="popup-btn" href="<?php echo base_url(); ?>registration/patron">  Patron </a>
				</div>
				
			</div>
			
		</div>
		
	</div>

</div>

<!---------------pop up end--------------->



<?php if($login_status ==1 ){ ?>
	<body class="body_bg_color">
		
		<div class="page-wrap">
		
		<div class="tems-condition">
	<?php if( $this->session->userdata('user_logged_as') == 'venue') { ?>
	
		<header class="edit_profile">
			<span><?php echo $venuedata['venue_name']; ?></span>
			<div class="media_link">
			
				<a href="<?php echo base_url()?>home/logout" class="buy_tip">logout</a>
			</div>
		</header>
	<?php } else {
			if( $userData['profile_image']!='' )
			{
				if($userData['profile_image']!='' && file_exists('uploads/profile_picture/'.$userData['profile_image']))
				{
					$image =   "<img src='".base_url().'uploads/profile_picture/'.$userData['profile_image']."' style='height:30px;width:30px;'/>";
					
				} else {
					
					$image =   "<img src='".base_url().'assets/images/default-no-profile-pics.jpg'."' style='height:30px;width:30px;'>";
				}
			}
			else
			{
				$image =   "<img src='".base_url().'assets/images/default-no-profile-pics.jpg'."' style='height:30px;width:30px;'>";

			}
			
			if( $userData['google_plus_id'] != '' && $userData['google_plus_id'] > 0 && $userData['profile_image'] != '' ) {
				$image =   "<img src='".$userData['profile_image']."' style='height:30px;width:30px;'>";
			}
			
			if( $userData['facebook_id'] != '' && $userData['facebook_id'] > 0  && $userData['profile_image'] != '' ) {
				$image =   "<img src='".$userData['profile_image']."' style='height:30px;width:30px;'>";
			}
			
			
			if( $image != '' ) { $image = $image;} else { $image = "<img class='inner-pages-image' src='".base_url().'assets/images/default-no-profile-pics.jpg'."' style='height:30px;width:30px;'>"; }
		?>
			
	<header class="edit_profile">
		
			<?php echo $image; ?> <span><?php echo $userData['first_name'].' '.$userData['last_name']; ?></span>
			<div class="media_link">
				<a href="<?php echo base_url()?>home/logout" class="buy_tip">logout</a>
			</div>
	</header>
<?php } 

	if($this->session->flashdata('success')) { ?>
	<div class="entry_row userSignUpok msg-top" id="userSignUpok">
		<img class="msg-image" src="<?php echo base_url(); ?>assets/images/suc-msg.png">
		<span class="suc-bold">Success!</span>
		<?php echo $this->session->flashdata('success'); ?></div>
	<?php } 
	if($this->session->flashdata('error')) { ?>
	<div class="entry_row userSignUpError msg-top" id="userSignUpError">
		<img class="msg-image" src="<?php echo base_url(); ?>assets/images/suc-msg.png">
		<span class="err-bold">Error!</span>
		<?php echo $this->session->flashdata('error'); ?></div>
	<?php } 
	if($this->session->flashdata('fail')) { ?>
	<div class="entry_row userSignUpError msg-top" id="userSignUpError">
		<span class="err-bold">Error!</span>
		<?php echo $this->session->flashdata('fail'); ?></div>
	<?php } 	
	
} else { 
	?>
<body class="bg_image">
 <div class="page-wrap">
	<div class="tems-condition">
<header>
	<div class="container">
		<div class="logo">
			<h1><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url()?>assets/images/logo.png"></a></h1>
		</div>
		<div class="login_registration">
			
			<?php $log_as= $this->session->userdata('user_logged_as'); 
				if($log_as)
				{
					if($log_as=='patron')
					{
						$url=base_url().'home/user_profile';
					}
					else
					{
						$url=base_url().'venues/profile';
					}
					$logout=base_url().'home/logout';
					echo "<a class='header_button' href='$url'>My Profile</a>";
					echo "<a class='header_button' href='$logout'>Logout</a>";
					
					
				}
				else
				{
					echo '<input type="button" id="login-btn" class="header_button" value="Login">
						<input type="button" id="register-btn" class="header_button" value="Registration">';
				} 
			?>
		
		</div>
	</div>
</header>

<?php if($this->session->flashdata('success')) { ?>
	<div class="entry_row userSignUpok msg-top outer-noti" id="userSignUpok">
		<img class="msg-image" src="<?php echo base_url(); ?>assets/images/suc-msg.png">
		<span class="suc-bold">Success!</span>
		<?php echo $this->session->flashdata('success'); ?></div>
<?php } ?>
 
<?php if($this->session->flashdata('error')) { ?>

	<div class="entry_row userSignUpError msg-top outer-noti" id="userSignUpError">
		<img class="msg-image" src="<?php echo base_url(); ?>assets/images/suc-msg.png">
		<span class="err-bold">Error!</span>
		<?php echo $this->session->flashdata('error'); ?></div>
<?php } ?>	
<?php if($this->session->flashdata('fail')) { ?>
	<div class="entry_row userSignUpError msg-top outer-noti" id="userSignUpError">
		<img class="msg-image" src="<?php echo base_url(); ?>assets/images/suc-msg.png">
		<span class="err-bold">Error!</span>
		<?php echo $this->session->flashdata('fail'); ?></div>
<?php } ?>	
	 
<?php	 } ?>


<input type="hidden" value="<?php echo base_url()?>" id="url">


  <?php echo $page_body; ?>
  

<script src="<?php echo base_url(); ?>assets/js/popModal.js"></script>

<div class="popup_container_login"  id="popup_container_login" style="display:none">
		<div class="popup_outer_login">
			<a href="javascript:void(0)" class="popup_img_login" style="left:5px;"></a>
			<a href="javascript:void(0)" class="popup_img_login" style="right:15px; top:10px;">
				<img src="<?php echo base_url();?>assets/images_new/cross.png" class="close_popup_login" /></a>
			<h1>Login Form</h1>
			
			<div class="reg_option">
				
			</div>
	</div></div>
</div>
</div>
<div class="footer">
	<div class="container">
		<div class="left_link">
			<ul>
				<li><a href="<?php echo base_url(); ?>terms_conditions/">Terms and conditions</a></li>
				<li><a href="<?php echo base_url(); ?>privacy_policy/">Privacy policy</a></li>
				<li><a href="mailto:support@whispertip.com">Contact Us</a></li>
			</ul>
		</div>
		<div class="social_icon">
			<p>Get Social With Us</p>
			<ul>
			<li><a target="_blank" href="https://facebook.com/whispertip"><img src="<?php echo base_url(); ?>assets/images/facebook.png"></a></li>
			<li><a target="_blank" href="http://twitter.com/whispertip"><img src="<?php echo base_url(); ?>/assets/images/twitter.png"></a></li>
			<li><a href=""><img src="<?php echo base_url(); ?>assets/images/incledion.png"></a></li>
			<li><a target="_blank" href="http://instagram.com/whispertip"><img src="<?php echo base_url(); ?>assets/images/instagram.png"></a></li>
			
			</ul>
		</div>
	</div>
</div>


	
</body>
<script type="text/javascript">
function twitter_share11(user_id) {
	window.open("https://twitter.com/intent/follow?user_id="+user_id,'_blank','width=810,height=570');
}
</script>
</html>


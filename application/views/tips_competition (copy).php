<div class="rugby blue">
	<div class="container">
		<div class="main_outer">
		<div class="content">
			<center><img class='inner-pages-image' src="<?php echo base_url().'assets/images/logo.png'; ?>"/></center>		
			<div class="sort"></div>
		
			<?php echo $sidebar; ?>
			
			<div class="right_content" id="page_content">
				<div class="blog coach_blog">
					<div class="but_tip_coach">
					
						<div class="widthdraw">
							
							<?php if( $this->uri->segment(3) != '') { ?>
							<div class="widthdraw_heading">Select Teams :</div>
							
							<div class="rounds">
								<?php if( $this->uri->segment(3) == 'AFL' ) {
									$rounds = $this->config->item('AFL');
								} else if( $this->uri->segment(3) == 'NRL') {
									$rounds = $this->config->item('NRL');
								} ?>
								
								<select name="rounds" id="change_rounds">
									<?php for( $i = 1; $i <= $rounds; $i++ ) {
										$sel_round = ( $this->uri->segment(4) != '' ) ? $this->uri->segment(4) : 1;
										$selected = ( $sel_round == $i ) ?  'selected' : '';
										echo "<option value={$i} {$selected}>Round {$i}</option>";
									} ?>
								</select>
								
							</div>
							
							<?php if( !empty($comp_list) ) { 
								
								$match_ids = array();
								if( !empty( $selected_comp )) {
									foreach( $selected_comp as $sel_comp ) {
										$match_ids[] = $sel_comp['match_id'];	
										$sel_team[$sel_comp['match_id']] = $sel_comp['selected_team'];										
									}
								}
								?>	
								<form id="select_venue_frm" name="select_venue_frm" method="post" action="">
								<table>
								
										<?php $sn = 0;
										foreach( $comp_list as $comp ) {
											
											$diff =abs(strtotime($comp['match_date'].' '.$comp['time'])) - strtotime(date('Y-m-d H:i:s'));
											$minutes = round($diff / 60);
											
											if( in_array($comp['competition_id'],$match_ids )) {
												$disabled = 'disabled';
												$sn++;
											} else if( $minutes < $this->config->item('time_limit') ) {
												$disabled = 'disabled';
												$sn++;
											} else {
												$disabled = '';
											}
											
											$checked1 = '';
											$checked2 = '';
											$team_name = @$sel_team[$comp['competition_id']];
											if( $team_name == $comp['team1'] ) {
												$checked1 = 'checked';
											} else if( $team_name == $comp['team2'] ) {
												$checked2 = 'checked';
											}
											
											
									echo "<tr>";
										
											$timestamp = strtotime($comp['match_date']);
											$day = date('l', $timestamp);
											
											$time  = date("g:i a", strtotime($comp['time']));
											echo "<td>".$day.' '.$comp['match_date'].'<br/>'.$comp['team1'].' vs '.$comp['team2']. '<br/>'. $comp['venue_name'].' '.$time."</td>";
											echo "<td><input type='radio' name='match_".$comp['competition_id']."' value='".$comp['team1']."' ".$disabled." ".$checked1." /><span>".$comp['team1']."</span></td>";
											echo "<td><input type='radio' name='match_".$comp['competition_id']."'' value='".$comp['team2']."' ".$disabled." ".$checked2." /><span>".$comp['team2']."</span></td>";
									echo "</tr>";
									
									
										}
										?>
									
							
								</table>
								
								<?php if( $sn == count( $comp_list) ) {
									
								} else { ?>
									<div class="btn_profile">
										<input type="submit" class="buy_tip pull-left submit-class" style="margin-top:5px;margin-top:6px;" name ="save_tips" value="Save Changes" />
									</div>
								<?php } ?>
								</form>
							<?php } else {
								 echo "<table><tr><td colspan=3>NO TEAM FOUND</td></tr></table>";
								
								} ?>
							
							<?php } else { ?>
							<div class="widthdraw_heading">Select Competition :</div>
					
				
							<div class="profile_form">
								<label>Select Competition</label>
								<div class="profile_input">
									<a href="<?php echo base_url(); ?>tips/competition/AFL">AFL</a>
									<a href="<?php echo base_url(); ?>tips/competition/NRL">NRL</a>
								</div>
							</div>
							
							<?php } ?>
						
						
						
						
						</div>
					
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>	
</div>

<script>
	
$(document).on('change','#change_rounds',function(){
	var round_id = $(this).val();
	window.location.href = '<?php echo base_url(); ?>tips/competition/<?php echo $this->uri->segment(3); ?>/'+ round_id;
});

$(function() {
	$("#select_venue_frm").validate({
		rules: {
			main_venue: "required",
		},
		messages: {
			main_venue: "Please select venue.",
		},
			errorElement:"div",
			errorClass:"valid-error",
			submitHandler: function(form) {
				form.submit();
			}
		});
	});
</script>


<style>
table {
  border: 1px solid black;
  height: auto;
  width: 548px;
}
tr {
  border-bottom: 1px solid;
  float: left;
  height: 70px;
}
td {
  border-right: 1px solid;
  float: left;
  width: 182px;
}
</style>

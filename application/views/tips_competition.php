<div class="rugby blue">
	<div class="container">
		<div class="main_outer">
		<div class="content">
			<center><img class='inner-pages-image' src="<?php echo base_url().'assets/images/logo.png'; ?>"/></center>		
			<div class="sort"></div>
		
			<?php echo $sidebar; ?>
			
			<div class="right_content" id="page_content">
				<div class="blog coach_blog">
					<div class="but_tip_coach">
					
						<div class="widthdraw my_pro">
							<?php
								$c_type = ( $this->uri->segment('3') != '' ) ? $this->uri->segment('3') : 'AFL';
								$round_no = ( $this->uri->segment('4') != '' ) ? $this->uri->segment('4') : 1;
							?>
							
							
							<?php if( $this->uri->segment(3) != '') { ?>
							
							<div class="pross-bar">
								<a href="<?php echo base_url(); ?>tips/competition" > 1. Select Competition </a>
								<a class="last-pross active" href="<?php echo base_url(); ?>tips/competition/<?php echo $c_type.'/'.$round_no; ?>" > 2. Select Round </a>
							</div>
							
							<form id="select_venue_frm" name="select_venue_frm" method="post" action="">	
							<div class="round-outter">
								<div class="round-head">  
							
								<?php if( $this->uri->segment(3) == 'AFL' ) {
									$rounds = $this->config->item('AFL');
								} else if( $this->uri->segment(3) == 'NRL') {
									$rounds = $this->config->item('NRL');
								} ?>
								
								<?php $show_round = ( $this->uri->segment(4) != '' ) ? $this->uri->segment(4) : 1; ?>
									<h5> ROUND <?php echo $show_round; ?> </h5>
									<select name="rounds" id="change_rounds">
									<?php for( $i = 1; $i <= $rounds; $i++ ) {
										$sel_round = ( $this->uri->segment(4) != '' ) ? $this->uri->segment(4) : 1;
										$selected = ( $sel_round == $i ) ?  'selected' : '';
										echo "<option value={$i} {$selected}>Round {$i}</option>";
									} ?>
									</select>
								</div>
								<div class="round-table">
									
									<?php if( !empty($comp_list) ) { 
										$match_ids = array();
										if( !empty( $selected_comp )) {
											foreach( $selected_comp as $sel_comp ) {
												$match_ids[] = $sel_comp['match_id'];	
												$sel_team[$sel_comp['match_id']] = $sel_comp['selected_team'];				
											}
										}
									?>	
									
								
									<table>
										
											<?php $sn = 0;
										foreach( $comp_list as $comp ) {
											
											$diff =abs(strtotime($comp['match_date'].' '.$comp['time'])) - strtotime(date('Y-m-d H:i:s'));
											$minutes = round($diff / 60);
											
											if( in_array($comp['competition_id'],$match_ids )) {
												$disabled = 'disabled';
												$sn++;
											} else if( $minutes < $this->config->item('time_limit') ) {
												$disabled = 'disabled';
												$sn++;
											} else {
												$disabled = '';
											}
											
											$checked1 = '';
											$checked2 = '';
											$team_name = @$sel_team[$comp['competition_id']];
											if( $team_name == $comp['team1'] ) {
												$checked1 = 'checked';
											} else if( $team_name == $comp['team2'] ) {
												$checked2 = 'checked';
											}
											
											$timestamp = strtotime($comp['match_date']);
											$day = date('l', $timestamp);
											$time  = date("g:i a", strtotime($comp['time']));
											$comp['match_date'] = date('F d Y',$timestamp);
								 ?>
									
									<tr>
											<td class="round-tabl-bg">
												<p> <?php echo $day.', '.$comp['match_date']; ?> </p>
												<!--h6><?php echo $comp['team1'].' V '.$comp['team2']; ?></h6-->
												<h6><?php echo $comp['match_name']; ?></h6>
												<p> <?php echo $comp['venue_name'].', '.$time; ?> </p>
											</td>
											
											<td class="round-tabl-bg2 tab-check">
												<label>
												<?php echo "<input type='radio' name='match_".$comp['competition_id']."' value='".$comp['team1']."' ".$disabled." ".$checked1." /><span>".$comp['team1']."</span>"; ?> 
													<!--input type="radio"-->
													
												</label>
											</td>
											<td class="round-tabl-bg2 tab-check">
												<label>
													<?php echo "<input type='radio' name='match_".$comp['competition_id']."' value='".$comp['team2']."' ".$disabled." ".$checked2." /><span>".$comp['team2']."</span>"; ?> 
													<!--input type="radio">
													<span> RICHMOND </span-->
												</label>
											</td>
										</tr>
									
								<?php		}
										?>
									</table>
									
								</div>
								
								<?php if( $sn == count( $comp_list) ) {
									
								} else { ?>
									<div class="venue-btn">
										<input type="submit" class="buy_tip submit-class" style="margin-top:5px;margin-top:6px;" name ="save_tips" value="SUBMIT" />
									</div>
								<?php } ?>
						
								
							<?php } else {
								 echo "<table><tr><td class='no-padding' colspan=3><span class='no-match'>No match added yet</span></td></tr></table>";
								
								} ?>
					</form>		
							<?php } else { ?>
							<div class="pross-bar">
								<a href="<?php echo base_url(); ?>tips/competition" class="active"> 1. Select Competition </a>
								<a class="last-pross" href="<?php echo base_url(); ?>tips/competition/<?php echo $c_type.'/'.$round_no; ?>" > 2. Select Round </a>
							</div>
							
							<div class="com-team-outer">
								<ul>
									<li>
										<a href="<?php echo base_url(); ?>tips/competition/AFL">
											<img src="<?php echo base_url(); ?>assets/images/team-img-2.png">
											<h4> AFL </h4>
											<div class="com-team-hover"></div>
										</a> 
									</li>
									<li>
										<a href="<?php echo base_url(); ?>tips/competition/NRL"> 
											<img src="<?php echo base_url(); ?>assets/images/team-img-1.png">
											<h4> NRL </h4>
											<div class="com-team-hover"></div>
										</a> 
									</li>
								</ul>
							</div>
							
							<?php } ?>
						</div>
					
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>	
</div>

<script>
	
$(document).on('change','#change_rounds',function(){
	var round_id = $(this).val();
	window.location.href = '<?php echo base_url(); ?>tips/competition/<?php echo $this->uri->segment(3); ?>/'+ round_id;
});

$(function() {
	$("#select_venue_frm").validate({
		rules: {
			main_venue: "required",
		},
		messages: {
			main_venue: "Please select venue.",
		},
			errorElement:"div",
			errorClass:"valid-error",
			submitHandler: function(form) {
				form.submit();
			}
		});
	});
</script>


<!--style>
table {
  border: 1px solid black;
  height: auto;
  width: 548px;
}
tr {
  border-bottom: 1px solid;
  float: left;
  height: 70px;
}
td {
  border-right: 1px solid;
  float: left;
  width: 182px;
}
</style-->

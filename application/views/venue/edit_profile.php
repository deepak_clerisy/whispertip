<div class="rugby blue">
	<div class="container">
		<div class="main_outer">
		<div class="content">
			<center class="logo-margin"><img class='inner-pages-image' src="<?php echo base_url().'assets/images/logo.png'; ?>"/></center>		
				<div class="sort"></div>
			<?php echo $sidebar; ?>
			
			<div class="right_content" id="page_content">
			
				<div class="blog coach_blog">
					
					<div class="but_tip_coach">
					<form id="editVenuefrm" name="editVenuefrm" enctype="multipart/form-data" method="post" action="">
					<div class="widthdraw">
						<div class="widthdraw_heading">Edit Profile</div>
						
						<input type="hidden" value="<?php echo $venuedata['venue_id']; ?>" id="venue_id" />
						
						<div class="profile_form">
							<label>Venue Name</label>
							<div class="profile_input"><input type="text" name="venue_name" class="input_txt" readonly='readonly' value="<?php echo $venuedata['venue_name']; ?>" /></div>
						</div>
						
						<div class="profile_form">
							<label>Email</label>						
							<div class="profile_input"><input type="text" name="email"  id="email" class="input_txt" value="<?php echo $venuedata['email']; ?>" readonly='readonly' /></div>
						</div>
						<div class="profile_form">
							<label>Contact</label>						
							<div class="profile_input"><input type="text" name="contact" class="input_txt"  value="<?php echo $venuedata['contact']; ?>" /></div>
						</div>
											
						<div class="profile_form">
							<label>Phone No.</label>		
							<div class="profile_input">
							<?php $phone = ( $venuedata['phone'] != '' && $venuedata['phone'] != 0 ) ? $venuedata['phone'] : ''; ?> 
							<input type="text" name="phone"  class="input_txt" value="<?php echo $phone; ?>" maxlength='12' />
							</div>
						</div>
				
						<div class="profile_form">
							<label>Address</label>		
							<div class="profile_input">	<input type="text" name="address"  class="input_txt"  value="<?php echo $venuedata['address']; ?>" /></div>
						</div>
						
						
						<div class="btn_profile">
						<input type="submit" class="buy_tip pull-left submit-class" style="margin-top:5px;margin-top:6px;" value="Save Changes" />
						<a href="<?php echo base_url()?>venues/profile" class="buy_tip pull-left" style="margin-left:5px;margin-top:5px;">Cancel</a>
						</div>
						</form>
						</div>
					</div>
				</div>
						
				</div>
			</div>
		</div>
	</div>
</div>
					
<script>
$(function() {
	$("#editVenuefrm").validate({
					rules: {
						phone: "number",
					},
					messages: {
						phone: "Please enter numeric value only.",
					},
			errorElement:"div",
			errorClass:"valid-error",
			submitHandler: function(form) {
				form.submit();
			}
		});
	});
</script>

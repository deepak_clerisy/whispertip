<div class="rugby blue">
	<div class="container">
		<div class="main_outer">
		<div class="content">
			<center><img class='inner-pages-image' src="<?php echo base_url().'assets/images/logo.png'; ?>"/></center>		
			<div class="sort"></div>
		
			<?php echo $sidebar; ?>
			
			<div class="right_content" id="page_content">
				<div class="blog coach_blog">
					<div class="but_tip_coach">
					
						<div class="widthdraw my_pro">
							<?php
								$c_type = ( $this->uri->segment('3') != '' ) ? $this->uri->segment('3') : 'AFL';
								$round_no = ( $this->uri->segment('4') != '' ) ? $this->uri->segment('4') : 1;
							?>
							
							<?php if( $this->uri->segment(3) != '') { ?>
							<div class="pross-bar">
								<a href="<?php echo base_url(); ?>venues/leaderboard" > 1. Select Competition </a>
								<a class="last-pross active" href="<?php echo base_url(); ?>venues/leaderboard/<?php echo $c_type.'/'.$round_no; ?>" > 2. Leaderboard </a>
							</div>
							
							<div class="leader-head">
								
								<div class="widthdraw_heading">Venue Name <?php echo $c_type; ?> TIPPING</div>
								
							</div>
							<div class="leader-outer">
								
								<div class="leader-inner">
									<?php if( !empty($leaderboard_data) ) { 
										echo '<ul>';
											$sn=1;
											foreach($leaderboard_data as $user )
											{
												echo '<li>';
												if($sn==1)
												{
													echo '<img class="flag-img" src="'.base_url().'assets/images/flag.png"/>';
												}
												echo '<div class="leader-img">';
												if(!empty($user['profile_image']))
												{
													$image=base_url().'uploads/profile_picture/'.$user['profile_image'];
												}
												else
												{
													$image=base_url().'assets/images/default-no-profile-pics.jpg';
												}
												
												echo '<a href="'.base_url().'venues/view_user/'.base64_encode($user['id']).'">';
												echo '<img src="'.$image.'"/>';
												echo '</a>';
												echo '</div><div class="leader-contant">';
												echo '<h6>'.$user['first_name'] .' '. $user['last_name'].'</h6>';
												echo '<h5> <span> Ranking: </span>'.$sn.'st </h5>'; 
												echo '<h5> <span> Correct tips: </span>'.$user['result'].'/'.$total.'</h5></div>';   
												echo '</li>';
												$sn++;
											}
										echo '</ul>';	
										//leader-bg
										}
										else
										{
											echo '<span>No Leaderboard Data</span>';
										}
										?>	
									
								</div>
								
							</div>	<!-------leader-outer------->
							
							<?php } else { ?>
							
							<div class="pross-bar">
								<a href="<?php echo base_url(); ?>venues/leaderboard" class="active"> 1. Select Competition </a>
								<a class="last-pross" href="<?php echo base_url(); ?>venues/leaderboard/<?php echo $c_type.'/'.$round_no; ?>" > 2. Leaderboard </a>
							</div>
							
							<div class="com-team-outer">
								<ul>
									<li>
										<a href="<?php echo base_url(); ?>venues/leaderboard/AFL">
											<img src="<?php echo base_url(); ?>assets/images/team-img-2.png">
											<h4> AFL </h4>
											<div class="com-team-hover"></div>
										</a> 
									</li>
									<li>
										<a href="<?php echo base_url(); ?>venues/leaderboard/NRL"> 
											<img src="<?php echo base_url(); ?>assets/images/team-img-1.png">
											<h4> NRL </h4>
											<div class="com-team-hover"></div>
										</a> 
									</li>
								</ul>
							</div>
					
							<?php } ?>
						
						</div>
					
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>	
</div>

<script>
	
$(document).on('change','#change_rounds',function(){
	var round_id = $(this).val();
	window.location.href = '<?php echo base_url(); ?>tips/competition/<?php echo $this->uri->segment(3); ?>/'+ round_id;
});

$(function() {
	$("#select_venue_frm").validate({
		rules: {
			main_venue: "required",
		},
		messages: {
			main_venue: "Please select venue.",
		},
			errorElement:"div",
			errorClass:"valid-error",
			submitHandler: function(form) {
				form.submit();
			}
		});
	});
</script>


<style>
table {
  border: 1px solid black;
  height: auto;
  width: 548px;
}
tr {
  border-bottom: 1px solid;
  float: left;
  height: 70px;
}
td {
  border-right: 1px solid;
  float: left;
  width: 182px;
}
</style>

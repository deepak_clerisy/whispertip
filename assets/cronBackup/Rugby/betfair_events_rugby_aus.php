<?php

if($_SERVER['HTTP_HOST']=='localhost')
{
	mysql_connect('192.168.1.135','root','root');
	mysql_select_db('whispertip');
	global $ssl_crt ;
	global $ssl_key ;
	$ssl_crt = "/var/www/betfair/ssl/client-2048.crt";
	$ssl_key = "/var/www/betfair/ssl/client-2048.key";
}
else
{
	mysql_connect('localhost','root','Wh#$%45@@*Tip');
	mysql_select_db('whispertip1');
	global $ssl_crt ;
	global $ssl_key ;
	$ssl_crt = "/var/ssl_betfair/client-2048.crt";
	$ssl_key = "/var/ssl_betfair/client-2048.key";
}
function certlogin($appKey,$params)
{
	global $ssl_crt;
	global $ssl_key;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://identitysso-api.betfair.com/api/certlogin");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSLCERT, $ssl_crt);
    curl_setopt($ch, CURLOPT_SSLKEY, $ssl_key);

	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey
    ));

    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $response = json_decode(curl_exec($ch));
   
    curl_close($ch);

    if ($response->loginStatus == "SUCCESS") {
        return $response;
    } else {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    }
}
function sportsApingRequest($appKey, $sessionToken, $operation, $params)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api-au.betfair.com/exchange/betting/json-rpc/v1");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey,
        'X-Authentication: ' . $sessionToken,
        'Accept: application/json',
        'Content-Type: application/json'
    ));

    $postData =
        '[{ "jsonrpc": "2.0", "method": "SportsAPING/v1.0/' . $operation . '", "params" :' . $params . ', "id": 1}]';

    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

    debug('Post Data: ' . $postData);
    $response = json_decode(curl_exec($ch));
    debug('Response: ' . json_encode($response));

    curl_close($ch);

    if (isset($response[0]->error)) {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    } else {
        return $response;
    }

}
function extractEventTypeId($allEventTypes)
{
	$eventTypeArr = array();
    foreach ($allEventTypes as $eventType) {
        if ($eventType->eventType->name != '') {
			$eventTypeArr[] = $eventType->eventType->id;
        }
        
    }
     return $eventTypeArr;

}

function getAllEventTypes($appKey, $sessionToken)
{

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listEventTypes', '{"filter":{}}');

    return $jsonResponse[0]->result;

}
function getEventType($appKey, $sessionToken, $horseRacingEventTypeId)
{
	$params = '{"filter":{"competitionIds":["' . $horseRacingEventTypeId . '"]}}';
    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listEvents', $params);

    return $jsonResponse[0];

}
function getMarketType($appKey, $sessionToken, $horseRacingEventTypeId)
{
	$params = '{"filter":{"eventIds":["' . $horseRacingEventTypeId . '"]}}';
    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketTypes', $params);

    return $jsonResponse[0];

}

function getNextRacingMarket($appKey, $sessionToken, $horseRacingEventTypeId,$markType)
{

    $params = '{"filter":{"eventIds":["' . $horseRacingEventTypeId . '"],
              "marketTypeCodes":["'.$markType.'"]},
              "sort":"FIRST_TO_START",
              "maxResults":"1",
              "marketProjection":["RUNNER_DESCRIPTION"]}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketCatalogue', $params);

    return $jsonResponse[0]->result[0];
}
function getNextMarket($appKey, $sessionToken, $horseRacingEventTypeId)
{

    $params = '{"filter":{"eventTypeIds":["' . $horseRacingEventTypeId . '"],
              "marketCountries":["GB"],
              "marketTypeCodes":["WIN"],
              "marketStartTime":{"from":"' . date('c') . '"}},
              "sort":"FIRST_TO_START",
              "maxResults":"1",
              "marketProjection":["RUNNER_DESCRIPTION"]}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketCatalogue', $params);

    return $jsonResponse[0]->result[0];
}

function getCompetition($appKey, $sessionToken, $horseRacingEventTypeId)
{

    $params = '{"filter":{"eventTypeIds":["' . $horseRacingEventTypeId . '"]}}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listCompetitions', $params);
    

    return $jsonResponse[0]->result;
}
function getVenues($appKey, $sessionToken, $horseRacingEventTypeId)
{

    $params = '{"filter":{"eventTypeIds":["' . $horseRacingEventTypeId . '"]}}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listVenues', $params);
    

    return $jsonResponse[0];
}
function getTimeRanges($appKey, $sessionToken, $horseRacingEventTypeId)
{

    $params = '{"filter":{"eventTypeIds":["' . $horseRacingEventTypeId . '"]}}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listTimeRanges', $params);
    

    return $jsonResponse[0];
}

function getMarketBook($appKey, $sessionToken, $marketId)
{
    $params = '{"marketIds":["' . $marketId . '"], "priceProjection":{"priceData":["EX_BEST_OFFERS"]}}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketBook', $params);

    return $jsonResponse[0]->result[0];
}
function getMarketCatelogue($appKey, $sessionToken, $horseRacingEventTypeId,$val)
{

    $params = '{"filter":{"eventIds":["' . $horseRacingEventTypeId . '"],
              "marketTypeCodes":["'.$val.'"]},
              "sort":"FIRST_TO_START",
              "maxResults":"1",
              "marketProjection":["RUNNER_DESCRIPTION"]}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketCatalogue', $params);

    return $jsonResponse[0];
}
function printMarketIdRunnersAndPrices($nextHorseRacingMarket, $marketBook)
{

    function printAvailablePrices($selectionId, $marketBook)
    {

        // Get selection
        foreach ($marketBook->runners as $runner)
            if ($runner->selectionId == $selectionId) break;

        echo "\nAvailable to Back: \n";
        foreach ($runner->ex->availableToBack as $availableToBack)
            echo $availableToBack->size . "@" . $availableToBack->price . " | ";

        echo "\n\nAvailable to Lay: \n";
        foreach ($runner->ex->availableToLay as $availableToLay)
            echo $availableToLay->size . "@" . $availableToLay->price . " | ";

    }


    echo "MarketId: " . $nextHorseRacingMarket->marketId . "\n";
    echo "MarketName: " . $nextHorseRacingMarket->marketName;

    foreach ($nextHorseRacingMarket->runners as $runner) {
        echo "\n\n\n===============================================================================\n";

        echo "SelectionId: " . $runner->selectionId . " RunnerName: " . $runner->runnerName . "\n";
        printAvailablePrices($runner->selectionId, $marketBook);
    }
}

function debug($debugString)
{
    global $DEBUG;
    if ($DEBUG)
        echo $debugString . "\n\n";
}

$appKey = '4ae3bb5f6f042268359bba5d24dd3c2ecb1b2f37';
$params = 'username=nlarkins&password=budda2468';
$resp = certlogin($appKey,$params);

//$sessionToken = 'Oc7tHJKtYJeViN/Auey+3EPOncSCPhQZvRB7DWuxZMA=';
$sessionToken = $resp->sessionToken;

$appKey1 = '97Wtq7sjReOr0uWk';

$insCron = "insert into cron_betfair_events_log(start_datetime) values(now())";
mysql_query($insCron);
$insId = mysql_insert_id();
$totalIns=0;
$query = "select * from betfair_events_competitions where is_aust=1 and event_type_id=1477 order by event_type_id";
$resQuery = mysql_query($query);
if(mysql_num_rows($resQuery))
{
	while($respQuery = mysql_fetch_array($resQuery))
	{
		$competition_id = $respQuery['competition_id'];
		$nextMarket = getEventType($appKey1, $sessionToken, $competition_id);
		
		foreach($nextMarket->result as $next)
		{
			
			// insert the events of comptition
			
			$event_name = $next->event->name;
			$event_id = $next->event->id;
			$country_code = $next->event->countryCode;
			$timezone = $next->event->timezone;
			$ser = array('T','Z');
			$rep = array(' ',' ');
			$open_date = str_replace($ser,$rep,$next->event->openDate);
			$open_date = date("Y-m-d H:i:s",strtotime($open_date));
			
			$select = "select * from betfair_events where event_id = $event_id";
			$res = mysql_query($select);
			if(mysql_num_rows($res)>0)
			{
			$sql11 = "update betfair_events set event_id=".$event_id."',name='".$event_name."',country_code='".$country_code."',timezone='".$timezone."',open_date='".$open_date."',is_aust=1 where event_id='".$event_id."'";
				mysql_query($sql11);		
			}
			else
			{
				$sql = "insert into betfair_events(event_id,competition_id,name,country_code,timezone,open_date,is_aust) values('".$event_id."','".$competition_id."','".$event_name."','".$country_code."','".$timezone."','".$open_date."',1)";
				mysql_query($sql);
				$totalIns++;
			}
			//echo "<pre>";
			$marketType = getMarketType($appKey1, $sessionToken, $next->event->id);
			foreach($marketType->result as $marksType)
			{
				//echo $marksType->marketType."<br/>";
				$markType = $marksType->marketType;
				$marketType1 = getNextRacingMarket($appKey1, $sessionToken, $next->event->id,$markType);
				//print_r($marketType1);
				$marketType2 = getMarketBook($appKey1, $sessionToken, $marketType1->marketId);
				//echo "<pre>";
				//print_r($marketType1);
				//print_r($marketType2);
				
				$market_id = $marketType1->marketId;
				$team1_id = $marketType1->runners[0]->selectionId;
				$team1_name = $marketType1->runners[0]->runnerName;
				$team2_id = $marketType1->runners[1]->selectionId;
				$team2_name = $marketType1->runners[1]->runnerName;
				$draw_id = $marketType1->runners[2]->selectionId;
				$catlogue = getMarketCatelogue($appKey1, $sessionToken, $event_id,$markType);
				$market_name = $catlogue->result[0]->marketName;
				$select1 = "select * from betfair_events_markets where event_id = $event_id and market_type='".$markType."'";
				//echo "sql1 = ".$select1."<br/>";
				$res = mysql_query($select1);
				if(mysql_num_rows($res)>0)
				{
					$sql3 = "update betfair_events_markets set market_id = $market_id,market_type='".$markType."',competition_id='".$competition_id."',event_id='".$event_id."',market_name='".$market_name."',is_austrialian=1 where event_id = $event_id and market_type='".$markType."'";
				mysql_query($sql3);
				}
				else
				{
					$sql = "insert into betfair_events_markets(market_id,market_type,competition_id,event_id,market_name,is_austrialian) values('".$market_id."','".$markType."','".$competition_id."','".$event_id."','".$market_name."',1)";
				//	echo "sql2 = ".$sql."<br/>";
				//	echo "<br/>";
				//	echo "<br/>";
					$sql2 = "insert into betfair_events_results(market_id,competition_id,date_added,is_austrialian) values('".$market_id."','".$competition_id."','".$open_date."')";
					
					mysql_query($sql);
					mysql_query($sql2);
				}
				foreach($marketType1->runners as $resp)
				{
					$market_id = $marketType1->marketId;
					$event_id = $next->event->id;
					$team_id = $resp->selectionId;
					$team_name = $resp->runnerName;
					$sort_pr = $resp->sortPriority;
					$sqlCh = "select * from betfair_markets_odds where market_id = ".$market_id." and team_id ='".$team_id."'";
						//echo $sql1."<br/>";
					$chRes = mysql_query($sqlCh);
					if(mysql_num_rows($chRes)>0)
					{
						
					}
					else
					{
						$sqlins = "insert into betfair_markets_odds(market_id,event_id,team_id,team_name,sort_priority) values('".$market_id."','".$event_id."','".$team_id."','".$team_name."','".$sort_pr."')";
						mysql_query($sqlins);
					}
				}
			}
			//die("here");
		}
	}
}
$upCron = "update cron_betfair_events_log set records_affected=$totalIns, end_datetime = now() where betfair_events_log_id=".$insId;
mysql_query($upCron);
?>

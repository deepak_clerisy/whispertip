<?php

if($_SERVER['HTTP_HOST']=='localhost')
{
	mysql_connect('192.168.1.135','root','root');
	mysql_select_db('whispertip');
	global $ssl_crt ;
	global $ssl_key ;
	$ssl_crt = "/var/www/betfair/ssl/client-2048.crt";
	$ssl_key = "/var/www/betfair/ssl/client-2048.key";
}
else
{
	mysql_connect('localhost','root','Wh#$%45@@*Tip');
	mysql_select_db('whispertip1');
	global $ssl_crt ;
	global $ssl_key ;
	$ssl_crt = "/var/ssl_betfair/client-2048.crt";
	$ssl_key = "/var/ssl_betfair/client-2048.key";
}
function certlogin($appKey,$params)
{
	global $ssl_crt;
	global $ssl_key;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://identitysso-api.betfair.com/api/certlogin");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSLCERT, $ssl_crt);
    curl_setopt($ch, CURLOPT_SSLKEY, $ssl_key);

	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey
    ));

    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $response = json_decode(curl_exec($ch));
   
    curl_close($ch);

    if ($response->loginStatus == "SUCCESS") {
        return $response;
    } else {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    }
}
function sportsApingRequest($appKey, $sessionToken, $operation, $params)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.betfair.com/exchange/betting/json-rpc/v1");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey,
        'X-Authentication: ' . $sessionToken,
        'Accept: application/json',
        'Content-Type: application/json'
    ));

    $postData =
        '[{ "jsonrpc": "2.0", "method": "SportsAPING/v1.0/' . $operation . '", "params" :' . $params . ', "id": 1}]';

    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

    debug('Post Data: ' . $postData);
    $response = json_decode(curl_exec($ch));
    debug('Response: ' . json_encode($response));

    curl_close($ch);

    if (isset($response[0]->error)) {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    } else {
        return $response;
    }

}
function extractEventTypeId($allEventTypes)
{
	$eventTypeArr = array();
    foreach ($allEventTypes as $eventType) {
        if ($eventType->eventType->name != '') {
			$eventTypeArr[] = $eventType->eventType->id;
        }
        
    }
     return $eventTypeArr;

}

function getAllEventTypes($appKey, $sessionToken)
{

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listEventTypes', '{"filter":{}}');

    return $jsonResponse[0]->result;

}
function getEventType($appKey, $sessionToken, $horseRacingEventTypeId)
{
	$params = '{"filter":{"competitionIds":["' . $horseRacingEventTypeId . '"]}}';
    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listEvents', $params);

    return $jsonResponse[0];

}
function getMarketType($appKey, $sessionToken, $horseRacingEventTypeId)
{
	$params = '{"filter":{"eventIds":["' . $horseRacingEventTypeId . '"]}}';
    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketTypes', $params);

    return $jsonResponse[0];

}

function getNextRacingMarket($appKey, $sessionToken, $horseRacingEventTypeId)
{

    $params = '{"filter":{"eventIds":["' . $horseRacingEventTypeId . '"],
              "marketTypeCodes":["MATCH_ODDS"]},
              "sort":"FIRST_TO_START",
              "maxResults":"1",
              "marketProjection":["RUNNER_DESCRIPTION"]}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketCatalogue', $params);

    return $jsonResponse[0]->result[0];
}
function getNextMarket($appKey, $sessionToken, $horseRacingEventTypeId)
{

    $params = '{"filter":{"eventTypeIds":["' . $horseRacingEventTypeId . '"],
              "marketCountries":["GB"],
              "marketTypeCodes":["WIN"],
              "marketStartTime":{"from":"' . date('c') . '"}},
              "sort":"FIRST_TO_START",
              "maxResults":"1",
              "marketProjection":["RUNNER_DESCRIPTION"]}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketCatalogue', $params);

    return $jsonResponse[0]->result[0];
}
function getCompetition($appKey, $sessionToken, $horseRacingEventTypeId)
{

    $params = '{"filter":{"eventTypeIds":["' . $horseRacingEventTypeId . '"]}}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listCompetitions', $params);
    

    return $jsonResponse[0]->result;
}
function getVenues($appKey, $sessionToken, $horseRacingEventTypeId)
{

    $params = '{"filter":{"eventTypeIds":["' . $horseRacingEventTypeId . '"]}}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listVenues', $params);
    

    return $jsonResponse[0];
}
function getTimeRanges($appKey, $sessionToken, $horseRacingEventTypeId)
{

    $params = '{"filter":{"eventTypeIds":["' . $horseRacingEventTypeId . '"]}}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listTimeRanges', $params);
    

    return $jsonResponse[0];
}

function getMarketBook($appKey, $sessionToken, $marketId)
{
    $params = '{"marketIds":["' . $marketId . '"], "priceProjection":{"priceData":["EX_BEST_OFFERS"]}}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketBook', $params);

    return $jsonResponse[0]->result[0];
}

function printMarketIdRunnersAndPrices($nextHorseRacingMarket, $marketBook)
{

    function printAvailablePrices($selectionId, $marketBook)
    {

        // Get selection
        foreach ($marketBook->runners as $runner)
            if ($runner->selectionId == $selectionId) break;

        echo "\nAvailable to Back: \n";
        foreach ($runner->ex->availableToBack as $availableToBack)
            echo $availableToBack->size . "@" . $availableToBack->price . " | ";

        echo "\n\nAvailable to Lay: \n";
        foreach ($runner->ex->availableToLay as $availableToLay)
            echo $availableToLay->size . "@" . $availableToLay->price . " | ";

    }


    echo "MarketId: " . $nextHorseRacingMarket->marketId . "\n";
    echo "MarketName: " . $nextHorseRacingMarket->marketName;

    foreach ($nextHorseRacingMarket->runners as $runner) {
        echo "\n\n\n===============================================================================\n";

        echo "SelectionId: " . $runner->selectionId . " RunnerName: " . $runner->runnerName . "\n";
        printAvailablePrices($runner->selectionId, $marketBook);
    }
}

function debug($debugString)
{
    global $DEBUG;
    if ($DEBUG)
        echo $debugString . "\n\n";
}

	$appKey = '4ae3bb5f6f042268359bba5d24dd3c2ecb1b2f37';
	$params = 'username=nlarkins&password=budda2468';
	$resp = certlogin($appKey,$params);

	//$sessionToken = 'Oc7tHJKtYJeViN/Auey+3EPOncSCPhQZvRB7DWuxZMA=';
	$sessionToken = $resp->sessionToken;

	$appKey1 = '97Wtq7sjReOr0uWk';
	
	$insCron = "insert into cron_betfair_odds_log(start_datetime) values(now())";
	mysql_query($insCron);
	$insId = mysql_insert_id();
	$totalIns = 0;
	$evntTypeId =1477; 
	//$select = "select * from  betfair_events_markets where is_austrialian=0";
	//$resSelect =mysql_query($select);
	$getCompetitionByEventID = getCompetition($appKey1, $sessionToken, $evntTypeId);
	foreach($getCompetitionByEventID as $comp) {
		$competition_name = $comp->competition->name;
		$competition_id = $comp->competition->id;
	$select = "select * from  betfair_events_markets where competition_id=$competition_id";
	$resSel = mysql_query($select);
	if(mysql_num_rows($resSel)>0)
	{
		echo "<pre>";
		while($resp = mysql_fetch_array($resSel))
		{
			if($resp['market_id']!='' && $resp['market_id']!='0')
			{
				$marketType2 = getMarketBook($appKey1, $sessionToken, $resp['market_id']);
				if(count($marketType2) && isset($marketType2->runners))
				{
					foreach($marketType2->runners as $respm)
					{			
						$team_id = 	$respm->selectionId;
						$team_odds = $respm->ex->availableToBack[0]->price;
						
						$sql1 = "update betfair_markets_odds set team_odds=$team_odds where market_id = ".$resp['market_id']." and team_id ='".$team_id."'";
						//echo $sql1."<br/>";
						mysql_query($sql1);
						$totalIns++;
					}
				}
			}
		}
	}
}
	$upCron = "update cron_betfair_odds_log set records_affected=$totalIns, end_datetime = now() where betfair_odds_log_id=".$insId;
	mysql_query($upCron);
?>

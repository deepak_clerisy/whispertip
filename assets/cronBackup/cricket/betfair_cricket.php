<?php
if($_SERVER['HTTP_HOST']=='localhost')
{
	mysql_connect('localhost','root','root');
	mysql_select_db('whispertip1');
	global $ssl_crt ;
	global $ssl_key ;
	$ssl_crt = "/var/www/betfair/ssl/client-2048.crt";
	$ssl_key = "/var/www/betfair/ssl/client-2048.key";
}
else
{
	mysql_connect('localhost','root','Wh#$%45@@*Tip');
	mysql_select_db('whispertip1');
	global $ssl_crt ;
	global $ssl_key ;
	$ssl_crt = "/var/ssl_betfair/client-2048.crt";
	$ssl_key = "/var/ssl_betfair/client-2048.key";
}
function certlogin($appKey,$params)
{
	global $ssl_crt;
	global $ssl_key;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://identitysso-api.betfair.com/api/certlogin");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSLCERT, $ssl_crt);
    curl_setopt($ch, CURLOPT_SSLKEY, $ssl_key);

	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey
    ));

    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $response = json_decode(curl_exec($ch));
   
    curl_close($ch);

    if ($response->loginStatus == "SUCCESS") {
        return $response;
    } else {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    }
}
function sportsApingRequest($appKey, $sessionToken, $operation, $params)
{
    $ch = curl_init();
    $proxy = "199.195.141.245:80";
    curl_setopt($ch, CURLOPT_URL, "https://api.betfair.com/exchange/betting/json-rpc/v1");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_PROXY, $proxy);
	curl_setopt($ch, CURLOPT_PROXYUSERPWD, "whispertip:yhc5346gt365");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey,
        'X-Authentication: ' . $sessionToken,
        'Accept: application/json',
        'Content-Type: application/json'
    ));

    $postData =
        '[{ "jsonrpc": "2.0", "method": "SportsAPING/v1.0/' . $operation . '", "params" :' . $params . ', "id": 1}]';

    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

    debug('Post Data: ' . $postData);
    $response = json_decode(curl_exec($ch));
    debug('Response: ' . json_encode($response));

    curl_close($ch);

    if (isset($response[0]->error)) {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    } else {
        return $response;
    }

}
function extractEventTypeId($allEventTypes)
{
	$eventTypeArr = array();
    foreach ($allEventTypes as $eventType) {
        if ($eventType->eventType->name != '') {
			$eventTypeArr[] = $eventType->eventType->id;
        }
        
    }
     return $eventTypeArr;

}

function getAllEventTypes($appKey, $sessionToken)
{

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listEventTypes', '{"filter":{}}');

    return $jsonResponse[0]->result;

}
function getEventType($appKey, $sessionToken, $horseRacingEventTypeId)
{
	$params = '{"filter":{"competitionIds":["' . $horseRacingEventTypeId . '"]}}';
    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listEvents', $params);

    return $jsonResponse[0];

}
function getMarketType($appKey, $sessionToken, $horseRacingEventTypeId)
{
	$params = '{"filter":{"eventIds":["' . $horseRacingEventTypeId . '"]}}';
    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketTypes', $params);

    return $jsonResponse[0];

}

function getNextRacingMarket($appKey, $sessionToken, $horseRacingEventTypeId)
{

    $params = '{"filter":{"eventIds":["' . $horseRacingEventTypeId . '"],
              "marketTypeCodes":["MATCH_ODDS"]},
              "sort":"FIRST_TO_START",
              "maxResults":"1",
              "marketProjection":["RUNNER_DESCRIPTION"]}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketCatalogue', $params);

    return $jsonResponse[0]->result[0];
}
function getNextMarket($appKey, $sessionToken, $horseRacingEventTypeId)
{

    $params = '{"filter":{"eventTypeIds":["' . $horseRacingEventTypeId . '"],
              "marketCountries":["GB"],
              "marketTypeCodes":["WIN"],
              "marketStartTime":{"from":"' . date('c') . '"}},
              "sort":"FIRST_TO_START",
              "maxResults":"1",
              "marketProjection":["RUNNER_DESCRIPTION"]}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketCatalogue', $params);

    return $jsonResponse[0]->result[0];
}
function getCompetition($appKey, $sessionToken, $horseRacingEventTypeId)
{

    $params = '{"filter":{"eventTypeIds":["' . $horseRacingEventTypeId . '"]}}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listCompetitions', $params);
    

    return $jsonResponse[0]->result;
}
function getVenues($appKey, $sessionToken, $horseRacingEventTypeId)
{

    $params = '{"filter":{"eventTypeIds":["' . $horseRacingEventTypeId . '"]}}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listVenues', $params);
    

    return $jsonResponse[0];
}
function getTimeRanges($appKey, $sessionToken, $horseRacingEventTypeId)
{

    $params = '{"filter":{"eventTypeIds":["' . $horseRacingEventTypeId . '"]}}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listTimeRanges', $params);
    

    return $jsonResponse[0];
}

function getMarketBook($appKey, $sessionToken, $marketId)
{
    $params = '{"marketIds":["' . $marketId . '"], "priceProjection":{"priceData":["EX_BEST_OFFERS"]}}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketBook', $params);

    return $jsonResponse[0]->result[0];
}

function printMarketIdRunnersAndPrices($nextHorseRacingMarket, $marketBook)
{

    function printAvailablePrices($selectionId, $marketBook)
    {

        // Get selection
        foreach ($marketBook->runners as $runner)
            if ($runner->selectionId == $selectionId) break;

        echo "\nAvailable to Back: \n";
        foreach ($runner->ex->availableToBack as $availableToBack)
            echo $availableToBack->size . "@" . $availableToBack->price . " | ";

        echo "\n\nAvailable to Lay: \n";
        foreach ($runner->ex->availableToLay as $availableToLay)
            echo $availableToLay->size . "@" . $availableToLay->price . " | ";

    }


    echo "MarketId: " . $nextHorseRacingMarket->marketId . "\n";
    echo "MarketName: " . $nextHorseRacingMarket->marketName;

    foreach ($nextHorseRacingMarket->runners as $runner) {
        echo "\n\n\n===============================================================================\n";

        echo "SelectionId: " . $runner->selectionId . " RunnerName: " . $runner->runnerName . "\n";
        printAvailablePrices($runner->selectionId, $marketBook);
    }
}

function debug($debugString)
{
    global $DEBUG;
    if ($DEBUG)
        echo $debugString . "\n\n";
}

$appKey = '4ae3bb5f6f042268359bba5d24dd3c2ecb1b2f37';
$params = 'username=nlarkins&password=budda2468';
$resp = certlogin($appKey,$params);

//$sessionToken = 'Oc7tHJKtYJeViN/Auey+3EPOncSCPhQZvRB7DWuxZMA=';
$sessionToken = $resp->sessionToken;
$appKey1 = '97Wtq7sjReOr0uWk';

$resp1 = getAllEventTypes($appKey1, $sessionToken);


// Insert event types to table//
foreach($resp1 as $resp)
{
	$eventtype_name = $resp->eventType->name;
	$eventtype_name_id = $resp->eventType->id;
	//$sql = "insert into betfair_events_types(event_type_id,name) values('".$eventtype_name_id."','".$eventtype_name."')";
	//mysql_query($sql);
} 

$eventTypeId = extractEventTypeId($resp1);   //extract event id from  response
echo "<pre>";

//print_r($eventTypeId);

foreach($eventTypeId as $eventId)
{
	if($eventId==4)
	{
		$listCompetions = getCompetition($appKey1, $sessionToken, $eventId); // get competition for event type
		
		//print_r($listCompetions);
		$sqlupdate1 = "update navigation set comp_active=0 where event_type_id=4 and type=''";
		$res111 = mysql_query($sqlupdate1);
		$sqlupdate = "update betfair_events_competitions set active=0 where event_type_id=4";
		$res11 = mysql_query($sqlupdate);
		foreach($listCompetions as $resp)
		{
			
			// Insert competitions of event types//
			
			$competition_name = $resp->competition->name;
			$competition_id = $resp->competition->id;
			$QryNav= "select * from navigation where competition_id=$competition_id and type=''";
			$NavRes = mysql_query($QryNav);
			if(mysql_num_rows($NavRes)>0)
			{
				$NavCron = "update navigation set comp_active=1,is_aust=0,active=1 where competition_id=".$competition_id;
				mysql_query($NavCron);
			}
			else{
			$navsql = "insert into navigation(competition_id,event_type_id,name,is_aust,comp_active,active) values('".$competition_id."','".$eventId."','".$competition_name."',0,1,1)";
			mysql_query($navsql);
		}
			$selectQuery= "select * from betfair_events_competitions where competition_id=$competition_id";
			$res1 = mysql_query($selectQuery);
			if(mysql_num_rows($res1)>0)
			{
				$upCron = "update betfair_events_competitions set name='".$competition_name."',is_aust=0,active=1 where competition_id=".$competition_id;
				mysql_query($upCron);
			}
			else
			{
			$sql = "insert into betfair_events_competitions(competition_id,event_type_id,name,is_aust,active) values('".$competition_id."','".$eventId."','".$competition_name."',0,1)";
			mysql_query($sql);
		}
			
		}
	}
	
}



?>

 var myWindow;
 var redirectUrl;
 $( document ).ajaxStart(function() {
  var windowWidth = $(window).width();
		var windowHeight = $(window).height();
		var FinalHeight = windowHeight/2;
		var FinalWidth = (windowWidth/2)-100;
		
		$("#loading_img").css({ 
			'display':'block',
			'top':FinalHeight,
			'left':FinalWidth,
			'position':'fixed'
		
		});
});
$( document ).ajaxComplete(function() {
  $("#loading_img").css('display','none');
});
 $(document).ready(function(){
	$( "#lguserName" ).focus();
	$(document).on('click', '.login_btn',function(){
	var name = $(this).attr('name');
	 var url = $.trim($('#url').val());
	if(name == 'twitter')
	{
		var url =url+"twitter";
		window.top.location.href=url;
	}						
	else if(name == 'facebook')
	{
		var url =url+"facebook";
		window.top.location.href=url;
	}	
	else if(name == 'sendbttn')
	{
	   var email = $.trim($('#forgotEmail').val());	
	    if (email == '') {
		    $('#forgotEmail').focus();
            $('#forgotEmail').addClass('error');
        }
		else {
				var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				var get = regex.test(email);
				if (get == false) {
					$("#forgotEmail").focus();
					$("#forgotEmail").addClass("error");
					return false;
				}
            var url1 = url + 'home/forgot_password';
           	
			$('#forgotEmail').removeClass('error');
			$('#userForgotError').text('');
			jQuery.ajax({
				url: url1,
				type: 'POST',
				data:{forgotEmail:email},
				success: function (data) {
					var dataValue = data.split('"');
					if (dataValue[1] == 'error') {	
					//	$('#userForgotError').css('color','red');
						$('#userForgotError').addClass('userSignUpError');
						$('#userForgotError').text('Email doesn\'t exist.');
						$('#innerFgPDiv').height('110px');
						$('#forgotEmail').val('');
					}else {
						$('#userForgotError').removeClass('userSignUpError');
						$('#userForgotError').css('color','green');
						$('#userForgotError').css('background-color','#edf6e5');
						$('#userForgotError').css('border','1px solid #9fc76f');
						$('#userForgotError').css('padding','5px 10px');
						$('#userForgotError').text('Email has been sent to your provided email-id.');
						$('#forgotEmail').val('');
						}
				},
				error: function (httpRequest, textStatus, errorThrown) {

					alert(url1+"status=" + textStatus + ",error=" + errorThrown);
				}
			});
        }
	}	
	else if(name == 'cancelButtn' || name == 'cancel_signup' || name == 'cancel_forgot')
	{
		$('.via-email').css('display','none');
        $('.via-social').css('display','block');
        $('.via-forgot').css('display','none');
       $('.via-signup').css('display','none');
	}
	
	else
	{
		$('.via-email').css('display','block');
        $('.via-social').css('display','none');
        $('.via-forgot').css('display','none');
       $('.via-signup').css('display','none');
    	$( "#lguserName" ).focus();    
	}					
	});
});

function verify_email()
{
	$('.via-email').css('display','none');
	$('.via-social').css('display','none');
	$('.via-forgot').css('display','none');
	$('.via-signup').css('display','none');
	$('#emailGetMask').show();
}
// User Twitter SignUp with Email //
jQuery(document).on('click', '#getEmailLgnBtn', function () {
		 var url =$('#url').val();
        var email = $.trim($('#getemail').val());
        var email_verify = $.trim($('#getemail_verify').val());
        if (email == '') {
            $('#getemail').focus();
            $('#getemail').addClass('error');
        }else  if(email_verify == '') {
			$('#getemail').removeClass('error');			
			$('#getemail_verify').focus();
            $('#getemail_verify').addClass('error');
		} else {
            var url1 = url + 'twitter/getEmailForTwitter/';
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var get = regex.test(email);
            if (get == false) {

                $("#getemail").focus();
                $("#getemail").addClass("error");
                return false;
            }else if(email != email_verify){
				$("#email_id").removeClass("error");
				 $('.getEmail_message').attr('id', 'fp_error');
                 $('#fp_error').text('Email Do Not Match');
				return false;
            } else {
                $('#getemail').removeClass("error");

                jQuery.ajax({
                    url: url1,
                    type: 'POST',
                    data:{twemail:email},
                    dataType: 'JSON',
                    success: function (data) {
                        if (data == 'done') {							
                            setTimeout(function(){								
								var url123 = url+'select';								
								window.top.location.href = url123;
								$('#emailGetMask').hide();
                        }  ,100);
                        }else {
							$('.getEmail_message').attr('id', 'fp_error');
							$('#fp_error').text('Please use different Email');
							}
                    },
                    error: function (httpRequest, textStatus, errorThrown) {

                        alert(url1+"status=" + textStatus + ",error=" + errorThrown);
                    }
                });
            }
        }
    });		
    
    
    // User Sign Up //
    jQuery(document).on('click','#signupBtn', function () {
        var email = $.trim($('#userEmail').val());
        var url = $.trim($('#url').val());
        var base_url = $(this).attr('data-uri');
        var name = $.trim($('#userName').val());
        var pass = $.trim($('#userPassword').val());
        var cpass = $.trim($('#usercPass').val());
        var firstname = $.trim($('#userFirstName').val());
        var lastname = $.trim($('#userLastName').val());
        var country = $.trim($('#userCountry').val());
        var login_type = $.trim($('#loginType').val());
    if (name == '') {
			$('#userEmail').removeClass('error');		
            $('#userName').focus();
            $('#userName').addClass('error');
        }
        else if (email == '') {
            $('#userEmail').focus();
            $('#userEmail').addClass('error');
        } 
        
        else  if(firstname == '') {
			$('#userEmail').removeClass('error');
			$('#userName').removeClass('error');			
			$('#userPassword').removeClass('error');			
			$('#usercPass').removeClass('error');			
			
			$('#userFirstName').focus();
            $('#userFirstName').addClass('error');
		}
		else  if(lastname == '') {
			$('#userEmail').removeClass('error');
			$('#userName').removeClass('error');			
			$('#userPassword').removeClass('error');			
			$('#usercPass').removeClass('error');			
			$('#userFirstName').removeClass('error');			
			
			$('#userLastName').focus();
            $('#userLastName').addClass('error');
		}
        else  if(pass == '') {
			$('#userEmail').removeClass('error');
			$('#userName').removeClass('error');			
			
			$('#userPassword').focus();
            $('#userPassword').addClass('error');
		}
		else  if(cpass == '') {
			$('#userEmail').removeClass('error');
			$('#userName').removeClass('error');			
			$('#userPassword').removeClass('error');			
			$('#usercPass').focus();
            $('#usercPass').addClass('error');
		}
		else  if(country == '') {
			$('#userEmail').removeClass('error');
			$('#userName').removeClass('error');			
			$('#userPassword').removeClass('error');	
			$('#usercPass').removeClass('error');						
			$('#userFirstName').removeClass('error');	
			$('#userLastName').removeClass('error');	
			
			$('#userCountry').focus();
            $('#userCountry').addClass('error');
		}
		else if( login_type == '' ) {
			$('#userEmail').removeClass('error');
			$('#userName').removeClass('error');			
			$('#userPassword').removeClass('error');	
			$('#usercPass').removeClass('error');						
			$('#userFirstName').removeClass('error');	
			$('#userLastName').removeClass('error');
			$('#userCountry').removeClass('error');	
			
			$('#loginType').focus();
            $('#loginType').addClass('error');
		}
		else {
			$('#userCountry').removeClass('error');
			var url1 = url + 'twitter/checkEmail/';
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var get = regex.test(email);
			if (get == false) { 	
				$('#userEmail').focus();
				$('#userEmail').addClass('error');
				return false; 			
			}
	
            if(pass != cpass){
				$('#userEmail').removeClass('error');
				$('#userName').removeClass('error');			
				$('#userPassword').removeClass('error');	
				$('#usercPass').removeClass('error');	
				$('#userFirstName').removeClass('error');	
				$('#userLastName').removeClass('error');
				$('#userSignUpError').css('display','block');
			    $('#userSignUpError').text('Password Do Not Match');
				return false;
            } else {
                $('#userEmail').removeClass('error');
				$('#userName').removeClass('error');			
				$('#userPassword').removeClass('error');	
				$('#usercPass').removeClass('error');
				$('#userFirstName').removeClass('error');	
				$('#userLastName').removeClass('error');
				$('#userCountry').removeClass('error');
				//$('#userSignUpok').css('display','block');
				//$('#userSignUpok').text('');
                jQuery.ajax({
                    url: url1,
                    type: 'POST',
                    data:{userEmail:email,userName:name,userPassword:pass,userFirstName:firstname,userLastName:lastname,userCountry:country,userLogintype:login_type},
                    dataType: 'JSON',
                    success: function (data) {
					    if (data == 'emailerror') {
							$('#userSignUpError').val('');						
							$('#userSignUpError').css('color','red');	
							$('#userSignUpok').css('display','none');					
							$('#userSignUpError').css('display','block');						
							$('#userSignUpError').text('Email Id already exist.');
                        }else if(data =='usernameerror'){
							$('#userSignUpError').css('color','red');	
							$('#userSignUpok').css('display','none');	
							$('#userSignUpError').css('display','block');						
							$('#userSignUpError').text('User Name already exist.');
							}
                        else{
							$('#userSignUpError').css('display','none');	
							$('#userSignUpok').css('display','block');	
							$('#userSignUpok').text('User has been registered sucessfully.');
							$('.sign-fields').find('input').val('');
							//setTimeout("window.location.href="+base_url,4000);
							/*window.setTimeout(function() {
							window.location.href = base_url;
							}, 3000); */
							//window.location.href = url+'home';
							}
                    },
                    error: function (httpRequest, textStatus, errorThrown) {

                        alert(url1+"status=" + textStatus + ",error=" + errorThrown);
                    }
                });
            }
        }
    });		
    jQuery(document).on('click', '#canceltwitter', function () {
		$('.close_popup_login').trigger('click');
	});
    jQuery(document).on('click', '.forgot', function () {
		$('.via-email').css('display','none');
        $('.via-social').css('display','none');
        $('.via-forgot').css('display','block');
        $('.via-signup').css('display','none');
	});
    jQuery(document).on('click', '#canceltwitter', function () {
		 $('#emailGetMask').css('display','none');
        $('.via-social').css('display','block');
    });
	  jQuery(document).on('click', '.sign_up', function () {
		$('.via-email').css('display','none');
		$('#login_iframe').css('min-height','436px');
		$('.popup_outer_login').css('margin','38px auto auto');
        $('.via-social').css('display','none');
        $('.via-forgot').css('display','none');
        $('.via-signup').css('display','block');
	});
	
    // User Login //
    jQuery(document).on('click', '#lgloginBtn', function () {
	    var name = $.trim($('#lguserName').val());
        var pass = $.trim($('#lguserPassword').val());
        var url = $.trim($('#url').val());
       
        //alert(pass);return false;
        if (name == '') {
            $('#lguserName').focus();
            $('#lguserName').addClass('error');
        }
        else  if(pass == '') {
			$('#lguserPassword').focus();
			$('#lguserName').removeClass('error');			
			
            $('#lguserPassword').addClass('error');
		}
		else {
            var url1 = url + 'home/login/';
			$('#lguserPassword').removeClass('error');	
			$('#userLoginError').text('');
			jQuery.ajax({
				url: url1,
				type: 'POST',
				data:{lguserName:name,lguserPassword:pass},
				success: function (data) {
					
					var dataValue = data.split('"');
					if (dataValue[1] == 'error') {
						$('#userLoginError').css('display','block');
						$('#userLoginError').text('Invalid login details.');
					}else {
						if(redirectUrl){
							window.top.location.href = redirectUrl;
						}else{
							window.top.location.href = url+'home/user_profile';
						}
					}
				},
				error: function (httpRequest, textStatus, errorThrown) {

					alert(url1+"status=" + textStatus + ",error=" + errorThrown);
				}
			});
        }
    });		
    	
    jQuery(document).on('click', '#fgForgotBtn', function () {
		$('#innerFgPDiv').height('95px');
        var email = $.trim($('#forgotEmail').val());	
        if (email == '') {
            $('#forgotEmail').focus();
            $('#forgotEmail').addClass('error');
        }
		else {
				var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				var get = regex.test(email);
				if (get == false) {
					$("#forgotEmail").focus();
					$("#forgotEmail").addClass("error");
					return false;
				}
            var url1 = base_url + 'home/forgot_password';
           	
			$('#forgotEmail').removeClass('error');
			$('#userForgotError').text('');
			jQuery.ajax({
				url: url1,
				type: 'POST',
				data:{forgotEmail:email},
				dataType: 'JSON',
				success: function (data) {
					
					if (data == 'error') {							
						$('#userLoginError').css('color','red');
						$('#userLoginError').css('margin-bottom','10px !important');
						$('#userLoginError').css('display','block');
						$('#userLoginError').text('Email doesn\'t exist.');
						//$('#innerFgPDiv').height('110px');
					}else {
						$('.userSignUpok').text('Email has been sent to your provided email-id.');
						$('.userSignUpok').css('color','green');
						$('#innerFgPDiv').height('125px');
						//$('#fp_error').text('Email Has Been Sent To Your Id');
						}
				},
				error: function (httpRequest, textStatus, errorThrown) {

					alert(url1+"status=" + textStatus + ",error=" + errorThrown);
				}
			});
        }
    });		
 jQuery(document).on('click', '#closeSignup', function () {
	 $('#signupPopUp').fadeOut(500);
	 $('#emailGetMask').fadeOut(500);
 });
 jQuery(document).on('click', '#loginBtn', function () {
	 $('#loginPopUp').fadeIn(500);
	 $('#emailGetMask').fadeIn(500);
	 $('#lguserName').focus();
 });
 jQuery(document).on('click', '#closeLoginup', function () {
	 $('#loginPopUp').fadeOut('slow');
	 $('#emailGetMask').fadeOut('slow');
 });
 jQuery(document).on('click', '#closeTwitter', function () {
	 $('#emailGet').fadeOut('slow');
	 $('#emailGetMask').fadeOut('slow');
 });
 jQuery(document).on('click', '#tipbutton1', function () {
	var dis = $('#purchaseTip').css('display');
	if(dis == 'none')
	{
		$('#purchaseTip').slideDown(500);
	}
	else
	{
		$('#purchaseTip').slideUp(500);
	}
 });
 jQuery(document).on('click', '#settingBtn', function () {
	var dis = $('#settingDiv').css('display');
	if(dis == 'none')
	{
		$('#settingDiv').slideDown(500);
	}
	else
	{
		$('#settingDiv').slideUp(500);
	}
 });
 jQuery(document).on('mouseover', '#tipbutton1', function () {
	 //$('#settingDiv').slideUp(500);
	//$('#purchaseTip').slideDown(500);
 });
 jQuery(document).on('mouseover', '#settingBtn', function () {
	 //$('#purchaseTip').slideUp(500);
	//$('#settingDiv').slideDown(500);
 });
 $(document).mouseup(function (e)
{
    var container = $("#settingDiv");
    var container1 = $("#purchaseTip");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.slideUp(500);
    }
    if (!container1.is(e.target) // if the target of the click isn't the container...
        && container1.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container1.slideUp(500);
    }
});
 
  jQuery(document).on('click', '#saveGetPaid', function () {
		
        var custPrice = $.trim($('#customerprice').val());
        var whisPrice = $.trim($('#whisperamount').val());
		var purcPrice = $.trim($('#purchaseamount').val());
		
        if (custPrice == '') {
            $('#customerprice').focus();
            $('#customerprice').addClass('error');
        }
        else  if(whisPrice == '') {
			$('#customerprice').removeClass('error');			
			$('#whisperamount').focus();
            $('#whisperamount').addClass('error');
		}	
        else  if(purcPrice == '') {
			$('#customerprice').removeClass('error');			
			$('#whisperamount').removeClass('error');			
			$('#purchaseamount').focus();
            $('#purchaseamount').addClass('error');
		}	
		else
		{
			$('#customerprice').removeClass('error');			
			$('#whisperamount').removeClass('error');
			$('#purchaseamount').removeClass('error');
			$("#getpaidFrm").submit();
		}
});

  jQuery(document).on('click', '#saveProfile', function () {
		
        var first_name = $.trim($('#first_name').val());
        var last_name = $.trim($('#last_name').val());
		var dob = $.trim($('#dob').val());
	   if (first_name == '') {
            $('#first_name').focus();
            $('#first_name').addClass('error');
        }
        else  if(last_name == '') {
			$('#first_name').removeClass('error');			
			$('#last_name').focus();
            $('#last_name').addClass('error');
		}	
        else  if(dob == '') {
			$('#first_name').removeClass('error');			
			$('#last_name').removeClass('error');			
			$('#dob').focus();
            $('#dob').addClass('error');
		}	
		else
		{
			$('#first_name').removeClass('error');			
			$('#last_name').removeClass('error');
			$('#dob').removeClass('error');
			$("#editProfilefrm").submit();
		}
});
  jQuery(document).on('click', '#connectTwitter', function () {
	  var url = $('#url').val();
	 var url =url+"twitter/index/connect";
	myWindow=window.open(url,'_blank','width=810,height=570');
	myWindow.focus();
		 if (custPrice == '') {
            $('#customerprice').focus();
            $('#customerprice').addClass('error');
        }
        else  if(whisPrice == '') {
			$('#customerprice').removeClass('error');			
			$('#whisperamount').focus();
            $('#whisperamount').addClass('error');
		}	    
});
  jQuery(document).on('click', '#addMoneyBtn', function () {
	$('#addMoneyPopup').fadeIn('slow');    
	 $('#emailGetMask').fadeIn('slow');
});
  jQuery(document).on('click', '#closeAddMoneyup', function () {
	$('#addMoneyPopup').fadeOut('slow');    
	 $('#emailGetMask').fadeOut('slow');
});
  jQuery(document).on('click', '#withdrawMoneyBtn', function () {
	  $('.err_msg').html('');
	$('#withdrawMoneyPopup').fadeIn('slow');    
	 $('#emailGetMask').fadeIn('slow');
});
  jQuery(document).on('click', '#closeWithdrawMoneyup', function () {
	$('#withdrawMoneyPopup').fadeOut('slow');    
	 $('#emailGetMask').fadeOut('slow');
});
  jQuery(document).on('click', '#closePurchasePop', function () {
	  $('.error_message').html('');
	$('.error_message').css('border','none');
	$('#purchaseTipPopup').fadeOut('slow');    
	 $('#emailGetMask').fadeOut('slow');
});
  jQuery(document).on('click', '#closewalletError', function () {
	$('#walletErrorPopup').css('display','none');    
	 $('#buyTipSports').fadeIn('slow');
});
  jQuery(document).on('click', '#closePurchasePop', function () {
	$('#walletErrorPopup').css('display','none');    
	 $('#buyTipSports').fadeIn('slow');
});
  jQuery(document).on('click', '#forgotUserPass', function () {
	$('#forgotPasswordPopUp').fadeIn('slow');    
	$('#loginPopUp').fadeOut('slow');
});
  jQuery(document).on('click', '#closeForgot', function () {
	$('#forgotPasswordPopUp').fadeOut('slow');    
	$('#emailGetMask').fadeOut('slow');
	$('#userForgotError').html('');
});
function AddWalletMoney(url)
{
	var addamount = $.trim($('#addamount').val());
	if (addamount == '') {
		$('#addamount').focus();
		$('#addamount').addClass('error');
	}
	else
	{
	var urls =  url+"home/addMoney";
	//alert(urls);return false;
	var formdata='addamount='+addamount;
		$.ajax({
		type:"post",
		url: urls,
		data:formdata,
		 success: function (response)
		{
			if(response){
			var myWindow=window.open(response,'_blank','scrollbars=1');
			var timer = setInterval(function() {  
			if(myWindow.closed) {  
				clearInterval(timer);  
				alert('closed');  
			}  
		}, 1000); 
	}
			
		}
	});	
	
	}

}
function close_credit_window(url)
{
	myWindow.close();
	var urls = url+'home/walletAddMoney';
	 location.href=urls;
}
function close_credit_windowCancel(url)
{
	myWindow.close();
	//var urls = url+'home/walletAddMoneyCancel';
	// location.href=urls;
}
function close_email_window(url)
{
	myWindow.close();
	var urls = url+'home/walletAddMoney';
	 location.href=urls;
}
function close_email_windowCancel(url)
{
	myWindow.close();
	//var urls = url+'home/walletAddMoneyCancel';
	// location.href=urls;
}
function close_window(url)
{
	myWindow.close();
	var urls = url+'home/walletAddMoney';
	 location.href=urls;
}
function close_windowCancel(url)
{
	myWindow.close();
	var urls = url+'home/walletAddMoneyCancel';
	 location.href=urls;
}
jQuery(document).on('click', '#withdrawAmountBtn', function () {
	
	var paypalemail = $.trim($('#paypalemail').val());
	var withdrawamount = $.trim($('#withdrawamount').val());
	var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	var get = regex.test(paypalemail);
	
	if (get == false) {
		$('#paypalemail').focus();
		$('#paypalemail').addClass('error');
	}
	else if (withdrawamount == '') {
		$('#paypalemail').removeClass('error');
		$('#withdrawamount').focus();
		$('#withdrawamount').addClass('error');
	}
	else
	{
		$('#withdrawMoneyForm').submit();
	}
});


function checkButton(e)
{
	if(e.which==13)
	{
		jQuery('#lgloginBtn').trigger('click');
	}
}
function checkButton1(e)
{
	if(e.which==13)
	{
		jQuery('#fgForgotBtn').trigger('click');
	}
}

jQuery(document).on('click', '#closePointPopups', function () {	
	var divId = $(this).attr('data-id');			
	$("#"+divId).fadeOut('slow')
});
jQuery(document).on('click', '#puchaseTips', function () {
	var sess_user_id = $('#session_user_id').val();
	var user_id = $('#user_id').val();
	var event_type_id_sel = $(this).attr('data-uri');
	$('#event_type_select').val(event_type_id_sel);
	var url = $('#url').val();
	var whisperTipFee = $('#whisperTipFee').val();
	if(user_id!=sess_user_id)
	{
				var url1 = url + 'home/check_balance/';
					jQuery.ajax({
									url: url1,
									type: 'get',
									datatype:'json',
									success: function (data) {
										var res = jQuery.parseJSON(data);
										var bal = parseFloat(res.balance);
										if(bal>=whisperTipFee)
										{
											$('#buyTipSports').css('display','none');
											$('#purchaseTipPopup').fadeIn('slow');    
											$('#emailGetMask').fadeIn('slow');
										}
										else
										{
											$('#buyTipSports').css('display','none');
											$('#walletErrorPopup').fadeIn('slow');    
											$('#emailGetMask').fadeIn('slow');
											
										}
									},
									error: function (httpRequest, textStatus, errorThrown) {

										alert(url1+"status=" + textStatus + ",error=" + errorThrown);
									}
								});
	}
	});
jQuery(document).on('click', '#puchase_tips_sport', function () {
	var sess_user_id = $('#session_user_id').val();
	var user_id = $('#user_id').val();
	var market_id = $(this).attr('data-one');
	var event_id = $(this).attr('data-two');
	var selected_odd = $(this).attr('data-three');
	var event_type_id_sel = $(this).attr('data-four');
	var open_date = $(this).attr('data-five');
	var place_tip_id = $(this).attr('data-six');
	var tip_type = $(this).attr('data-seven');
	$('#market_id_hiddn').val(market_id);
	$('#event_id_hiddn').val(event_id);
	$('#selected_odd_hiddn').val(selected_odd);
	$('#event_type_select').val(event_type_id_sel);
	$('#open_date').val(open_date);
	$('#placed_tip_id').val(place_tip_id);
	var url = $('#url').val();
	var whisperTipFee = $('#whisperTipFee').val();
	if(user_id!=sess_user_id)
		{
			if(tip_type=='sell'){
				var url1 = url + 'home/check_balance/';
						jQuery.ajax({
										url: url1,
										type: 'POST',
										data:{place_tip_id:place_tip_id},
										datatype:'json',
										success: function (data) {
											var res = jQuery.parseJSON(data);
											var bal = parseFloat(res.balance);
											if(bal>=whisperTipFee)
											{
												$('#buyTipSports').css('display','none');
												$('#purchaseTipPopup').fadeIn('slow');    
												$('#emailGetMask').fadeIn('slow');
											}
											else
											{
											$('#buyTipSports').css('display','none');
												$('#walletErrorPopup').fadeIn('slow');    
												$('#emailGetMask').fadeIn('slow');
												
											}
										},
										error: function (httpRequest, textStatus, errorThrown) {

											alert(url1+"status=" + textStatus + ",error=" + errorThrown);
										}
									});

	}
	else
	{
		url = $('#url').val();
		var place_tip_id = $('#placed_tip_id').val();
		market_id_hiddn = $('#market_id_hiddn').val();
		event_id_hiddn = $('#event_id_hiddn').val();
		selected_odd_hiddn = $('#selected_odd_hiddn').val();
		event_type_id = $('#event_type_select').val();
		open_date = $('#open_date').val();
		sel_user_id = $('#user_id').val();
		var url1 = url + 'home/savePurchaseFree/';
		jQuery.ajax({
				url: url1,
				type: 'POST',
				data:{eventTypeId:event_type_id,tipsterId:sel_user_id,market_id:market_id_hiddn,event_id:event_id_hiddn,selected_odd:selected_odd_hiddn,open_date:open_date,place_tip_id:place_tip_id},
				dataType: 'JSON',
				async:false,
				success: function (data) {
					if (data == 'error') {	
						$('.popup_container').css('display','block');
						$('.twitter_content').css('display','none');
						$('.twitter_content1').css('display','none');
						$('.purchase_tip_notify').css('display','none');
						$('.popup_inner').css('color','#CC0000');
						$('.msg-content').text('You have Already Purchased this tip');
						return false;
					}else {
						var url1 = url + 'home/getpurchasedData/';
						jQuery.ajax({
							url: url1,
							type: 'POST',
							success: function (data) {
								var url1 = url + 'home/getpurchasedData/';
								jQuery.ajax({
									url: url1,
									type: 'POST',
									success: function (data) {
									$('.popup_container').css('display','block');
									$('.twitter_content').css('display','none');
									$('.twitter_content1').css('display','none');
									$('.purchase_tip_notify').css('display','block');
									$('.purchase_tip_notify').html(data);
									$('.popup_inner').css('color','#008000');
									$('.msg-content').text('You have successfully Purchased the tip');
									return false;
									window.location.href =url+'mypurchasedtip'; 
								}	
							});
						}	
					});//window.location.href =url+'mypurchasedtip'; 
				}
				},
				error: function (httpRequest, textStatus, errorThrown) {

					alert(url1+"status=" + textStatus + ",error=" + errorThrown);
				}
			});
		
	}
}
	});
	jQuery(document).on('click', '#addMoneyBtnFinal', function () {
		$('#walletAddForm').submit();
	});
	//////////////////////////////////////////////////////Place Tip ///////////////////////////////////
	
jQuery(document).on('click', '.list_competetion', function () {
		var url =  $('#url').val();
		var competition_id =  $(this).attr('data-one');
		var navigation_id =  $(this).attr('data-two');
		var parent_id =  $(this).attr('data-three');
		var type =  $(this).attr('data-four');
		var url=url+"home/getEventByComp";
		var formdata='id='+competition_id+'&nav_id='+navigation_id+'&parent_id='+parent_id+'&type='+type;
		$.ajax({
		type:"post",
		url: url,
		data:formdata,
		success: function (response)
		{
			//alert(response);
			$('#backbuttn').css('display','block');
			$('.right_content').html(response);
		}
	});		
	});
jQuery(document).on('click', '.list_eventByNav', function () {
		var url =  $('#url').val();
		var competition_id =  $(this).attr('data-one');
		var navigation_id =  $(this).attr('data-two');
		var parent_id =  $(this).attr('data-three');
		var type =  $(this).attr('data-four');
		var url=url+"home/getCompNav";
		var formdata='id='+competition_id+'&nav_id='+navigation_id+'&parent_id='+parent_id+'&type='+type;
		//alert(formdata);
		$.ajax({
		type:"post",
		url: url,
		data:formdata,
		success: function (response)
		{
			$('.right_content').html(response);
		}
	});		
	});
jQuery(document).on('click', '.list_event', function () {
		var url =  $('#url').val();
		var competition_id =  $(this).attr('data-one');
		var navigation_id =  $(this).attr('data-two');
		var parent_id =  $(this).attr('data-three');
		var type =  $(this).attr('data-four');
		var url=url+"home/getEventByCompNav";
		var formdata='id='+competition_id+'&nav_id='+navigation_id+'&parent_id='+parent_id+'&type='+type;
		$.ajax({
		type:"post",
		url: url,
		data:formdata,
		success: function (response)
		{
			$('.right_content').html(response);
		}
	});		
	});
jQuery(document).on('click', '.getmarkets', function () {
		var url =  $('#url').val();
		var competition_id =  $(this).attr('data-one');
		var event_id =  $(this).attr('data-two');
		var date =  $(this).attr('data-three');
		var event_name =  $(this).attr('data-four');
	/*	var windowWidth = $(window).width();
		var windowHeight = $(window).height();
		var FinalHeight = windowHeight/2;
		var FinalWidth = (windowWidth/2)-100;
		
		$("#loading_img").css({ 
			'display':'block',
			'top':FinalHeight,
			'left':FinalWidth,
			'position':'fixed'
		
		});
		*/
		var url=url+"home/getMarketByComp";
	var formdata='comp_id='+competition_id+'&event_id='+event_id+'&date='+date+'&event_name='+event_name
		$.ajax({
		type:"post",
		url: url,
		data:formdata,
		success: function (response)
		{
			$(".list_competetion").css('display','none');
			//$("#loading_img").css('display','none');
			$('.right_content').html(response);
		}
	});		
	});
jQuery(document).on('click', '.marketsodd', function () {
		var url =  $('#url').val();
		var event_name = $('#event_name_hiddn').val();
		event_id = $(this).attr('data-one');
		market_id = $(this).attr('data-two');
		competition_id = $(this).attr('data-three');
		
		/* var windowWidth = $(window).width();
		var windowHeight = $(window).height();
		var FinalHeight = windowHeight/2;
		var FinalWidth = (windowWidth/2)-100;
		
		$("#loading_img").css({ 
			'display':'block',
			'top':FinalHeight,
			'left':FinalWidth,
			'position':'fixed'
		
		});
		*/
		//$("#loading_img").css('display','block');
		var url1 = url+'league_data_match_odds_API/'+event_id+'/'+market_id+'/'+competition_id+'/'+event_name;
		jQuery.ajax({
                    url: url1,
                    type: 'get',
                    success: function (data) {
						//$("#loading_img").css('display','none');
						$('.right_content').html(data);
                    },
                    error: function (httpRequest, textStatus, errorThrown) {

                        alert(url1+"status=" + textStatus + ",error=" + errorThrown);
                    }
                });
	});
jQuery(document).on('click', '.place_bet_notify', function () {
		var login_status = $('#login_status').val();
		if(login_status==0){
			$('.registration').trigger('click');
			return false;
			}
		var uniqId = $(this).attr('id');
		event_id = $(this).attr('data-one');
		market_id = $(this).attr('data-two');
		select_id = $(this).attr('data-three');
		competition_id = $(this).attr('data-four');
		selected_odd = $(this).attr('data-five');
		team_name = $(this).attr('data-six');
		event_name = $(this).attr('data-seven');
		open_date = $(this).attr('data-eight');
		timezone = $(this).attr('data-nine');
		event_type_id = $(this).attr('data-ten');
		if(selected_odd){
		var url11 =  $('#url').val();
		var url1 = url11 + 'home/check_place_tip/';
		jQuery.ajax({
				url: url1,
				type: 'POST',
				data:{eventId:event_id,marketId:market_id,selectId:select_id,competitionId:competition_id,selectedOdd:selected_odd,eventTypeId:event_type_id,teamName:team_name,eventName:event_name,openDate:open_date,Timezone:timezone},
				dataType: 'JSON',
				success: function (data) {
					if (data == 'error') {	
						$('.popup_container').css('display','inline-block');
						$('.twitter_content').css('display','none');
						$('.twitter_content1').css('display','none');
						$('.popup_inner').css('color','#CC0000');
						$('.msg-content').text('You have already placed tip on this game.');
						
						
					}else {
						$('#selectionDiv').val(uniqId);
						$('.popup_container_placed').css('display','block');
						$('.twitter_content_placed').css('display','block');
						$('.twitter_content1_placed').css('display','block');
						$('.popup_inner_placed').css('color','#008000');
						
						}
				},
				error: function (httpRequest, textStatus, errorThrown) {

					alert(url1+"status=" + textStatus + ",error=" + errorThrown);
				}
			});
			$('#messagePlace').show();
		}
	else
	{
		$('.popup_container').css('display','inline-block');
		$('.twitter_content').css('display','none');
		$('.twitter_content1').css('display','none');
		$('.msg-content').css('color','red');
		$('.msg-content').text('You can\'t place a tip on this market as the odds have not been established yet');
	}	
	});
jQuery(document).on('click', '.place_bet', function () {	
	var tip_type = $('#radio_placed_tip').val();
	var url11 =  $('#url').val();
	var selectionDiv =  $('#selectionDiv').val();
		
		var url1 = url11 + 'home/save_place_tip/';
		jQuery.ajax({
				url: url1,
				type: 'POST',
				data:{placed_tip_type:tip_type},
				dataType: 'JSON',
				success: function (data) {
					if (data == 'error') {	
						$('.popup_container').css('display','inline-block');
						$('.twitter_content').css('display','none');
						$('.twitter_content1').css('display','none');
						$('.popup_inner').css('color','#CC0000');
						$('.msg-content').text('You have already placed tip on this game.');
					}else {
						$('#'+selectionDiv).addClass('bet_placed');
						$('#'+selectionDiv+ ' h2').addClass('bet_placed_color');
						$('#'+selectionDiv+ ' span').addClass('bet_placed_color');
						$('.popup_container').css('display','block');
						$('.twitter_content').css('display','block');
						$('.twitter_content1').css('display','block');
						$('.popup_inner').css('color','#008000');
						$('.msg-content').text('You have successfully placed the tip');
						$('.popup_container_placed').css('display','none');
						}
				},
				error: function (httpRequest, textStatus, errorThrown) {

					alert(url1+"status=" + textStatus + ",error=" + errorThrown);
				}
			});
			$('#messagePlace').show();
});
jQuery(document).on('click', '.withdrawmoney', function () {	
	$('.withdraw').fadeIn('slow');    
	$('.mywallet').css('display','none');
	$('.addmoneyfm').css('display','none');
	$('.creditcard').css('display','none');
});
jQuery(document).on('click', '.addmoney', function () {	
	$('.err_msg').html('');
	$('.addmoneyfm').fadeIn('slow');    
	$('.withdraw').css('display','none');
	$('.mywallet').css('display','none');
	$('.creditcard').css('display','none');
});
jQuery(document).on('click', '.addcreditcard', function () {	
	$('.err_msg').html('');
	$('.creditcard').fadeIn('slow');   
	$('.addmoneyfm').css('display','none');
	$('.withdraw').css('display','none');
	$('.mywallet').css('display','none');
	
});
jQuery(document).on('click', '.backwithdraw', function () {	
	$('.err_msg').html('');
	$('.mywallet').fadeIn('slow');    
	$('.withdraw').css('display','none');
	$('.addmoneyfm').css('display','none');
	$('.creditcard').css('display','none');
});
jQuery(document).on('click', '#purchaseTipBtnFinal', function () {
	url = $('#url').val();
	market_id_hiddn = $('#market_id_hiddn').val();
	event_id_hiddn = $('#event_id_hiddn').val();
	selected_odd_hiddn = $('#selected_odd_hiddn').val();
	event_type_id = $('#event_type_select').val();
	open_date = $('#open_date').val();
	sel_user_id = $('#user_id').val();
	var place_tip_id = $('#placed_tip_id').val();
		var url1 = url + 'home/save_purchase_tip/';
		jQuery.ajax({
				url: url1,
				type: 'POST',
				data:{eventTypeId:event_type_id,tipsterId:sel_user_id,market_id:market_id_hiddn,event_id:event_id_hiddn,selected_odd:selected_odd_hiddn,open_date:open_date,place_tip_id:place_tip_id},
				dataType: 'JSON',
				async:false,
				success: function (data) {
					if (data == 'error') {	
						$('.popup_container').css('display','block');
						$('.twitter_content').css('display','none');
						$('.twitter_content1').css('display','none');
						$('.purchase_tip_notify').css('display','none');
						$('.popup_inner').css('color','#CC0000');
						$('.msg-content').text('You have Already Purchased this tip');
						return false;
					}else {
						var url1 = url + 'home/getpurchasedData/';
						jQuery.ajax({
							url: url1,
							type: 'POST',
							success: function (data) {
								var url1 = url + 'home/getpurchasedData/';
								jQuery.ajax({
									url: url1,
									type: 'POST',
									success: function (data) {
									$('.popup_container').css('display','block');
									$('.twitter_content').css('display','none');
									$('.twitter_content1').css('display','none');
									$('.purchase_tip_notify').css('display','block');
									$('.purchase_tip_notify').html(data);
									$('.popup_inner').css('color','#008000');
									$('.msg-content').text('You have successfully Purchased the tip');
									return false;
									window.location.href =url+'mypurchasedtip'; 
								}	
							});
						}	
					});//window.location.href =url+'mypurchasedtip'; 
				}
				},
				error: function (httpRequest, textStatus, errorThrown) {

					alert(url1+"status=" + textStatus + ",error=" + errorThrown);
				}
			});
		
	//	$('#purchaseTipBtn').hide();
	//	$('#emailGetMask').hide();
	//	$('#purchaseTipPopup').hide();
	//	$('#messagePurchase').show();
	});
	jQuery(document).on('click', '#navigate_purchased', function () {
		window.location.href =base_url+'mypurchasedtip'; 
	});
jQuery(document).on('click', '.userresult', function () {
		var event_type_id = $(this).attr('data-one');
		var event_type_user_id = $(this).attr('data-two');
		var url = $('#url').val();
	/*	 var windowWidth = $(window).width();
		var windowHeight = $(window).height();
		var FinalHeight = windowHeight/2;
		var FinalWidth = (windowWidth/2)-100;
		
		$("#loading_img").css({ 
			'display':'block',
			'top':FinalHeight,
			'left':FinalWidth,
			'position':'fixed'
		
		});
		*/
		var url1 = url+'tips_results/'+event_type_id+'/'+event_type_user_id;
		var url2 = url+'user_profile_information/'+event_type_id+'/'+event_type_user_id;
		jQuery.ajax({
                    url: url1,
                    type: 'get',
                    success: function (data) {
						jQuery.ajax({
						url: url2,
						type: 'get',
						success: function (data1) {
						//$("#loading_img").css('display','none');
							$.getScript(url+"assets/js/popModal.js");
						$('#backbuttn_profile').css('display','block');
						$('#buy_sports').html(data);
						$('.coach_blog').html(data1);
						
						},
						error: function (httpRequest, textStatus, errorThrown) {

							alert(url1+"status=" + textStatus + ",error=" + errorThrown);
						}
					});
                    },
                    error: function (httpRequest, textStatus, errorThrown) {

                        alert(url1+"status=" + textStatus + ",error=" + errorThrown);
                    }
                });
       });
	function redirectBilling(url)
	{
		window.location.href=url+"home/billing_shipping' ?>";
	}
	function printresponse(val)
	{
		alert(val);
	}
jQuery(document).on('click', '#confirm_payment', function () {
		var amount = $('#amount').val();
		var ccnumber = $('#ccnumber').val();
		var cvv = $('#cvv').val();
		var month = $('#month').val();
		var year = $('#year').val();
		 if (amount == '') {
            $('#amount').focus();
            $('#amount').addClass('error');
        }
        else  if(ccnumber == '') {
			$('#amount').removeClass('error');			
			$('#ccnumber').focus();
            $('#ccnumber').addClass('error');
		}	
        else  if(ccnumber == '') {
			$('#amount').removeClass('error');			
			$('#ccnumber').focus();
            $('#ccnumber').addClass('error');
		}	
        else  if(cvv == '') {
			$('#ccnumber').removeClass('error');			
			$('#cvv').focus();
            $('#cvv').addClass('error');
		}	
        else {
			$('#cvv').removeClass('error');			
			$('#payment_form').submit();
		}	
       });
jQuery(document).on('click', '.sorting_content', function () {
		$('.sorting_content').removeClass('sort_sel');
		$(this).addClass('sort_sel');
		var status = $(this).attr('data-uri');
		var url = $('#url').val();
		var event_type_id = $(this).attr('data-two');
		if(status!='sport') {
		if(status=='sport_user'){
			$('.sorting_content').removeClass('sort_sel');
		$(this).addClass('sort_sel');
			var user_id = $(this).attr('data-three');
				var url1 = url+'sort_users_sport/'+status+'/'+event_type_id+'/'+user_id;
				jQuery.ajax({
							url: url1,
							type: 'get',
							success: function (data) {
								//$("#loading_img").css('display','none');
								$('.sortingDiv').html(data);
							
							},
							error: function (httpRequest, textStatus, errorThrown) {

								alert(url1+"status=" + textStatus + ",error=" + errorThrown);
							}
						});
			
			}
			else {
				var url1 = url+'sort_users/'+status+'/'+event_type_id;
				jQuery.ajax({
							url: url1,
							type: 'get',
							success: function (data) {
								$.getScript(url+"assets/js/popModal.js");
								$("#loading_img").css('display','none');
								$('.sortingDiv').html(data);
							
							},
							error: function (httpRequest, textStatus, errorThrown) {

								alert(url1+"status=" + textStatus + ",error=" + errorThrown);
							}
						});
			}
		}
			else
			{
				var url1 = url+'sort_users_sport/'+status+'/'+event_type_id;
				jQuery.ajax({
                    url: url1,
                    type: 'get',
                    success: function (data) {
						$("#loading_img").css('display','none');
						$('.sortingDiv').html(data);
					
                    },
                    error: function (httpRequest, textStatus, errorThrown) {
				
                        alert(url1+"status=" + textStatus + ",error=" + errorThrown);
                    }
                });	
			}
		
		
	});	
	jQuery(document).on('click', '.proceed', function () {
		var url = $('#url').val();
		var login_status = $('#login_status').val();
		if(login_status==0){
			$('.registration').trigger('click');
			return false;
			}
		var eventType = $('#eventType').val();
		var user_id = $(this).attr('data-three');
		var url1 = url+'home/purchase_list';
		var status  = 1;
		var flag = 1;
		jQuery.ajax({
				url: url1,
				type: 'POST',
				data:{'user_id':user_id,'eventType':eventType,'status':status,'flag':flag},
				success: function (data) {
					if(data) {
						/* $('.sort').html('');
						$('.sort').css('padding','0');
						$('.sort').css('height','0');
						*/
						$('.right_content').html(data);
					}
				},
			
			});	
	});	
	
	
jQuery(document).on('click', '.sorting_content_sort', function () {
		$('.sorting_content_sort').removeClass('sort_sel');
		$(this).addClass('sort_sel');
		var status = $(this).attr('data-uri');
		var tipester_id = $(this).attr('data-one');
		var event_type_id = $(this).attr('data-two');
		var url = $('#url').val();
		/* var windowWidth = $(window).width();
		var windowHeight = $(window).height();
		var FinalHeight = windowHeight/2;
		var FinalWidth = (windowWidth/2)-100;
		$("#loading_img").css({ 
			'display':'block',
			'top':FinalHeight,
			'left':FinalWidth,
			'position':'fixed'
		
		});
		*/
		var url1 = url+'sort_purchased_tip/'+status+'/'+tipester_id+'/'+event_type_id;
		jQuery.ajax({
                    url: url1,
                    type: 'get',
                    success: function (data) {
					//	alert(data);
						//$("#loading_img").css('display','none');
						$('.curn_tip').html(data);
					
                    },
                    error: function (httpRequest, textStatus, errorThrown) {

                        alert(url1+"status=" + textStatus + ",error=" + errorThrown);
                    }
                });
		
		
	});	
jQuery(document).on('click', '.sorting_tipester', function () {
		$('.sorting_tipester').removeClass('sort_sel');
		$(this).addClass('sort_sel');
		var status = $(this).attr('data-uri');
		var url = $('#url').val();
		var url1 = url+'sort_tipester_list/'+status;
		jQuery.ajax({
                    url: url1,
                    type: 'get',
                    success: function (data) {
						var DataAppend = data.split('#####');
						$('#buy_sports').html(DataAppend[0]);
						$('.teams').html(DataAppend[1]);
					
                    },
                    error: function (httpRequest, textStatus, errorThrown) {

                        alert(url1+"status=" + textStatus + ",error=" + errorThrown);
                    }
                });
		
		
	});	
jQuery(document).on('click', '.collapsed', function () {
	
		$('#left_link_ul').css('display','block');
		$('#left_link_ul').css('height','auto');
	
	});	
jQuery(document).on('click', '.close_popup', function () {
	
		$('.popup_container').css('display','none');
			
	});	
jQuery(document).on('click', '.close_popup_login', function () {
		
		$('.via-email').css('display','none');
        $('.via-social').css('display','block');
        $('.via-forgot').css('display','none');
       $('.via-signup').css('display','none');
		$('.popup_container_login').attr('style','display:none;');
			
	});	
jQuery(document).on('click', '.close_popup_placed', function () {
	
		$('.popup_container_placed').css('display','none');
			
	});	

     
$(document).ready(function(){
	var windowWidth = $(window).width();
		var windowHeight = $(window).height();
		if(windowWidth <= 600) { 
			$('#left_link_ul').css('display','none');
		}
});
jQuery(document).on('click', '.tipester_list', function () {
	var event_id = $(this).attr('data-uri');
	var event_type_id = $(this).attr('data-two');
	var url = $('#url').val();
	var url1 = url+'list_tipester/'+event_id+'/'+event_type_id;
	jQuery.ajax({
                    url: url1,
                    type: 'get',
                    success: function (data) {
						$.getScript(url+"assets/js/popModal.js");
						$("#loading_img").css('display','none');
						$('.sortingDiv').html(data);
					
                    },
                    error: function (httpRequest, textStatus, errorThrown) {

                        alert(url1+"status=" + textStatus + ",error=" + errorThrown);
                    }
                });
});	
jQuery(document).on('click', '.loadtipesterPurchaser', function () {
		var url = $('#url').val();
		var eventType = $('#eventType').val();
		var user_id = $(this).attr('data-uri');
		var event_id = $(this).attr('data-one');
		var url1 = url+'home/purchase_list';
		var status  = 1;
		var flag = 0;
		jQuery.ajax({
				url: url1,
				type: 'POST',
				data:{'user_id':user_id,'eventType':eventType,'status':status,'event_id':event_id,'flag':flag},
				success: function (data) {
					if(data) {
						/* $('.sort').html('');
						$('.sort').css('padding','0');
						$('.sort').css('height','0');
						*/
						$('.right_content').html(data);
					}
				},
			
			});	
});	
jQuery(document).on('click', '.tipesterlistgame', function () {
		var url = $('#url').val();
		var eventType = $('#eventType').val();
		var user_id = $(this).attr('data-uri');
		var event_id = $(this).attr('data-one');
		var url1 = url+'home/purchase_list';
		var status  = 1;
		jQuery.ajax({
				url: url1,
				type: 'POST',
				data:{'user_id':user_id,'eventType':eventType,'status':status,'event_id':event_id},
				success: function (data) {
					if(data) {
						$('.sort').html('');
						$('.sort').css('padding','0');
						$('.sort').css('height','0');
						
						$('.right_content').html(data);
					}
				},
			
			});	
});	
jQuery(document).on('click', '.listplacedtip', function () {
		var url = $('#url').val();
		var eventType = $('#eventType').val();
		var event_id = $(this).attr('data-uri');
		var user_id = $(this).attr('data-one');
		var url1 = url+'home/purchase_list';
		var status  = 1;
		jQuery.ajax({
				url: url1,
				type: 'POST',
				data:{'user_id':user_id,'eventType':eventType,'status':status,'event_id':event_id},
				success: function (data) {
					if(data) {
						$('.right_content').html(data);
					}
				},
			
			});	
});	
jQuery(document).on('click', '.tipesterlistgame', function () {
		var url = $('#url').val();
		var eventType = $('#eventType').val();
		var user_id = $(this).attr('data-uri');
		var event_id = $(this).attr('data-one');
		var url1 = url+'home/purchase_list';
		var status  = 1;
		jQuery.ajax({
				url: url1,
				type: 'POST',
				data:{'user_id':user_id,'eventType':eventType,'status':status,'event_id':event_id},
				success: function (data) {
					if(data) {
						$('.sort').html('');
						$('.sort').css('padding','0');
						$('.sort').css('height','0');
						
						$('.right_content').html(data);
					}
				},
			
			});	
});	

jQuery(document).on('click', '.sport_purchased', function () {
		var url = $('#url').val();
		var event_type_id = $(this).attr('data-uri');
		var typester_id = $(this).attr('data-uri1');
		var user_id = $(this).attr('data-uri2');
		var status = $(this).attr('data-uri3');
	/*	var windowWidth = $(window).width();
		var windowHeight = $(window).height();
		var FinalHeight = windowHeight/2;
		var FinalWidth = (windowWidth/2)-100;
		$("#loading_img").css({ 
			'display':'block',
			'top':FinalHeight,
			'left':FinalWidth,
			'position':'fixed'
		
		});
		*/
		var url1 = url+'home/load_purchased_tip';
		jQuery.ajax({
				url: url1,
				type: 'POST',
				data:{'event_type_id':event_type_id,'typester_id':typester_id,'user_id':user_id,'status':status},
				success: function (data) {
				//alert(data);return false;
					if(data) {
					//	$("#loading_img").css('display','none');
					$.getScript(url+"assets/js/popModal.js");
						$('.sort').html('');
						$('.sort').css('padding','0');
						$('.sort').css('height','0');
						
						$('.content').html(data);
					}
				},
			
			});	
});	
jQuery(document).on('click', '.cancelpaypalpayment', function () {	
	$('.err_msg').html('');
	$('.withdraw').css('display','none');
	$('.addmoneyfm').css('display','none');
	$('.creditcard').css('display','none');
	$('#confirm_credit_payment').css('display','none');
	$('#walletwrapper').css('display','block');
	$('.mywallet').fadeIn('slow');
});
jQuery(document).on('click', '.registration', function () { 
		redirectUrl = $(this).attr('data-uri');
		if(hasStorage()) {
			window.localStorage.setItem("redirect_url", redirectUrl);
		}
		$('#iframe').attr('src', base_url+'home/login_popup');
		$('#popup_container_login').attr('style','display:block;');
	});
jQuery(document).on('click','.registration_page', function () { 
	window.location.href = base_url+'registration';
});
jQuery(document).on('click', '#delete_user', function () {
	if( confirm("Do you really want to Delete your account? This will delete all your data and information from whispertip.com?") == true)
	{
		var user_id = $(this).attr('data-uri');
		var url1 = base_url+'home/delete_account';
			jQuery.ajax({
					url: url1,
					type: 'POST',
					dataType: 'JSON',
					data:{'user_id':user_id},
					success: function (data) {
						if(data=='sucess') {
							
							window.location.href =base_url+'home'; 	
						}
						else
						{
							window.location.href =base_url+'profile'; 	
						}
					},
				
				});	
		}
	});
jQuery(document).on('change', '#time_range', function () {
	var selval = $(this).val();
	var timerange = $('#timerange').val(selval);
	if(selval!='')
	{
		$('#sort_by').css('display','inline-block');
	}	
	else
	{
		$('#sort_by').css('display','none');
	}
	});	
jQuery(document).on('change', '#sort_by', function () {
	var sortVal = $(this).val();
	var event_type_id = $('#league_id').val();
	var timerange = $('#timerange').val();
	//alert(event_type_id+'###'+sortVal+'###'+timerange);
	
	var url1 = base_url+'sort_time/'+sortVal+'/'+event_type_id+'/'+timerange;
				jQuery.ajax({
							url: url1,
							type: 'get',
							success: function (data) {
								$('.sortingDiv').html(data);
							}
		});
	});	
jQuery(document).on('change', '#time_range', function () {
	var sortVal = $('#sort_by').val();
	if($('#sort_by').val()!=''){
	var event_type_id = $('#league_id').val();
	var timerange = $('#timerange').val();
	//alert(event_type_id+'###'+sortVal+'###'+timerange);
	
	var url1 = base_url+'sort_time/'+sortVal+'/'+event_type_id+'/'+timerange;
				jQuery.ajax({
							url: url1,
							type: 'get',
							success: function (data) {
								$('.sortingDiv').html(data);
							}
		});
	}
	});	
	  window.fbAsyncInit = function() {
      FB.init({
          appId: '766856950020170', // Set YOUR APP ID
          status: true, // check login status
          cookie: true, // enable cookies to allow the server to access the session
          xfbml: true // parse XFBML
      });
  };
     // Load the SDK asynchronously
      (function(d) {
          var js, id = 'facebook-jssdk',
              ref = d.getElementsByTagName('script')[0];
          if (d.getElementById(id)) {
              return;
          }
          js = d.createElement('script');
          js.id = id;
          js.async = true;
          js.src = "//connect.facebook.net/en_US/all.js";
          ref.parentNode.insertBefore(js, ref);
      }(document));

	// Share video with facebook 
	function share(userid) {
      var share = {
          method: 'feed',
          name: 'Whispertip',
          link: 'http://www.whispertip.com/?c=home&m=user_public_profile&q='+userid,
          picture: 'http://www.whispertip.com/assets/images/amarican_rule.png',
          description: 'Whispertip',
          source: 'https://www.youtube.com/v/1CE6W5BubQo'
      };

      FB.ui(share, function(response) {
          if (response && response.post_id !== undefined) {
			  //$('.twitter_content').css('display','none');
			//  $('.msg-content').css('margin-left','32px');
			//  $('.msg-content').text('Sucessfully Shared');
          }
      });
  }
 ajax_paging = function(){
    $("p.pagination a").click(function() {     
		alert('asasassas');return false;          
               $.ajax({
                 type: "GET",
                 url: $(this).get(),
                 success: function(html){
					// alert(html);
         $("#display-content").html(html);
                  }
               });               
             });            
       return false;
     };  
jQuery(document).on('click', '.more_markets', function () {
	$("#market").submit();
})
jQuery(document).on('change', '#competition_filter', function () {
		comp_id = $(this).val();
		event_type_id = $('#event_type_id').val();
		var url1 = base_url+'filter_competition/'+comp_id +'/'+event_type_id+'/'+0;
		jQuery.ajax({
					url: url1,
					type: 'get',
					success: function (data) {
						//alert(data);return false;
						$('#table_container').html(data);
					}
		});
	});
jQuery(document).on('click', '.more_markets', function () {
		var dataUri = $(this).attr('data-uri');
		$('#market_'+dataUri).submit();
		});
function twitter_share(user_id) {
	window.open("https://twitter.com/intent/follow?user_id="+user_id,'_blank','width=810,height=570');
}
function hasStorage() {
	try {
		window.localStorage.setItem("mod", '');
		window.localStorage.removeItem("mod");
		return true;
	} catch (exception) {
		return false;
	}
}
jQuery(document).on('click','#reg_cancel', function () {
 	var base_url = $(this).attr('data-uri');
	window.location.href = base_url;
});
jQuery(document).on('click','#login_email', function () {
 	var base_url = $(this).attr('data-uri');
	window.location.href = base_url+'login';
});

<?php
class Content extends Controller {

	function Content()
	{
		parent::Controller();	
		$this->lang->load('admin', $this->config->item('language'));
		$this->load->library('authorize');
		
		if( $this->authorize->is_admin_logged_in() == false )
			redirect(base_url().'admin_panel/login');
			
		$this->load->library('Xajax');

		//$this->xajax->registerFunction(array('change_image',&$this,'change_image'));
		$this->xajax->processRequests();
	}
	
	function index()
	{
		$this->load->model('pages_model');
		$data = array();
		$txtSearch = $this->uri->segment(5, '');
		
		$fields = $this->pages_model->getAllPages($txtSearch);
		
		$data['txtSearch'] = $txtSearch;

		$this->load->library('pagination');
		
		$config['base_url'] = base_url().'admin_panel/content/index';
		$config['total_rows'] = count($fields);
		$config['per_page'] = $this->config->item('pagesize');
		$config['uri_segment'] = 4;
		$config['num_links'] = 2;
		if($txtSearch)
		$config['postfix_string'] = '/'.$txtSearch;
		
		$this->pagination->initialize($config); 
		
		$data['page_number'] = $this->uri->segment(4,0);
		
		$data['page_size'] = $this->config->item('pagesize');
		$data['pages'] = $this->pages_model->getAllPages($txtSearch, $data['page_number'], $data['page_size']);
	
		//$data['pages'] = $this->pages_model->getAllPages();
		
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		
		$data['page_title'] = $this->lang->line('admin_listview_pages');
		$data['section_heading'] = $this->lang->line('admin_listview_pages');
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/manage_pages',$data, true);
		$this->load->view('admin_panel/template',$data);

	}
	function manage_place_tip()
	{
		$this->load->model('pages_model');
		$data = array();
		$txtSearch = $this->uri->segment(5, '');
		
		$fields = $this->pages_model->getHowItPages();
		$data['field'] = $fields;
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		
		$data['page_title'] = $this->lang->line('admin_listview_pages');
		$data['section_heading'] = $this->lang->line('admin_listview_pages');
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/manage_place_tip_page',$data, true);
		$this->load->view('admin_panel/template',$data);

	}

	function edit_page()
	{
		$this->load->model('pages_model');
		$data = array();
		$page_id = $this->uri->segment(4, 0);
		if($this->pages_model->checkIdIsAvaliable('static_pages','static_page_id',$page_id) == false)
		{
			redirect( base_url(). 'admin_panel/content');
		}
		$mode = $this->input->post("editpages");
		$this->load->library('form_validation');
		$this->form_validation->set_rules('pagetitle', 'pagetitle', 'required');
		$this->form_validation->set_rules('metadescription', 'metadescription', 'required');
		
		if($page_id)
		{
			$data['pages'] = $this->pages_model->getPageDetailsById($page_id);
			$data['mode'] = $mode;
			$data['page_name'] = $page_id;
		
		}

		if ($this->form_validation->run() == TRUE)
		{
			if($mode=="editpages")
			{
			
				if( $this->pages_model->editPage($page_id) == true )
				{
				
					$this->session->set_flashdata('success', $this->lang->line('admin_operation_success'));
					redirect( base_url(). 'admin_panel/content/index');
				}
				else
				{
					$this->session->set_flashdata('error', $this->lang->line('admin_operation_unsuccess'));
					redirect( base_url(). 'admin_panel/content/index');
				}		
			}
		}
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		
		$data['page_title'] = $this->lang->line('admin_edit_pages');
		$data['section_heading'] = $this->lang->line('admin_edit_pages');
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/pages_edit',$data, true);
		$this->load->view('admin_panel/template',$data);
	}
	function place_edit_page()
	{
		$this->load->model('pages_model');
		$data = array();
		$page_id = $this->uri->segment(4, 0);
		if($this->pages_model->checkIdIsAvaliable('static_pages','static_page_id',$page_id) == false)
		{
			redirect( base_url(). 'admin_panel/content');
		}
		$mode = $this->input->post("editpages");
		$this->load->library('form_validation');
		$this->form_validation->set_rules('pagetitle', 'pagetitle', 'required');
		$this->form_validation->set_rules('heading1', 'heading1', 'required');
		
		if($page_id)
		{
			$data['pages'] = $this->pages_model->getPlacePageDetailsById($page_id);
			$data['mode'] = $mode;
			$data['page_name'] = $page_id;
		
		}

		if ($this->form_validation->run() == TRUE)
		{
			if($mode=="editpages")
			{
			
				if( $this->pages_model->editPlacePage($page_id) == true )
				{
				
					$this->session->set_flashdata('success', $this->lang->line('admin_operation_success'));
					redirect( base_url(). 'admin_panel/content/manage_place_tip');
				}
				else
				{
					$this->session->set_flashdata('error', $this->lang->line('admin_operation_unsuccess'));
					redirect( base_url(). 'admin_panel/content/manage_place_tip');
				}		
			}
		}
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		
		$data['page_title'] = $this->lang->line('admin_edit_pages');
		$data['section_heading'] = $this->lang->line('admin_edit_pages');
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/place_page_edit',$data, true);
		$this->load->view('admin_panel/template',$data);
	}
	//////////   dinesh code starts here  ///////////////
	function manage_file()
	{
		$this->load->model('emailtemplates_model');
		$data = array();
		$totalsegs = $this->uri->total_segments();
		$filebasepath = './uploads/manage_file/';
		$path ='';
		for($s=4;$s <=$totalsegs;$s++)
		{
			$path .= $this->uri->segment($s)."/";

		}
		$data['path'] = '';
		$data['list'] = '';
		if($path)
		{
			$data['list'] = 'folderdata';
			$data['path'] = $path;
		}

		if($this->input->post('btnsubmit'))
		{
			$fpath = @$_POST['folderpath'];
			$overwrite = @$_POST['chk'];

			foreach($_FILES as $k=>$v)
			{

				$filename = $v['name'];

				if($filename !='')
				{
					$filetempath = $v['tmp_name'];
					$target = $filebasepath.$fpath;	
					$upfilename = $filename;
					$target = $target.$upfilename;
					if($overwrite=='yes')
					{
						$file = directory_map($filebasepath.$fpath);
						foreach($file as $k=>$v)
						{
							if($filename == $v)
							{
								unlink($filebasepath.$fpath.$v);
							}
						}
					}
					if(move_uploaded_file($filetempath,$target))
					{
						chmod($target,0777);
					}
				}
			}
		}



		
		$data['extra_head_content'] = $this->xajax->getjavascript(null, base_url() . 'assets/js/xajax.js');
		$data['page_title'] = $this->lang->line('admin_file_manage');
		$data['section_heading'] = $this->lang->line('admin_file_manage');
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/filemanage_view',$data, true);
		$this->load->view('admin_panel/template',$data);
	}
	
	function addfolder()
	{
		$totalsegs = $this->uri->total_segments();
		$path ='';
		$filebasepath = './uploads/manage_file/';
		for($s=4;$s <=$totalsegs;$s++)
		{
			$path .= $this->uri->segment($s)."/";
		}
		$path = substr($path,0,strlen($path)-1);

		mkdir($filebasepath.$path, 0777);
		chmod($filebasepath.$path, 0777);
		redirect(base_url().'admin_panel/content/manage_file/'.$path);

	}
	function deletefolder()
	{
		$totalsegs = $this->uri->total_segments();
		$path ='';
		$filebasepath = './uploads/manage_file/';
		$basename = '';
		for($s=4;$s <=$totalsegs;$s++)
		{
			$path .= $this->uri->segment($s)."/";
		}
		//print $folderpath."<br>";
		$path = substr($path,0,strlen($path)-1);
		for($s=4;$s <$totalsegs;$s++)
		{
			$basename .= $this->uri->segment($s)."/";
		}
		//die;
		if(is_file($filebasepath.$path))
		{
			//die("/var/www/socialecommerce/development/socialecommerce/uploads/filefolder/".$path);
			unlink($filebasepath.$path);
			redirect(base_url().'admin_panel/content/manage_file/'.$basename);
		}
		else
		{
			//print'f';
			//die("/var/www/socialecommerce/development/socialecommerce/uploads/filefolder/".$path);
			rmdir($filebasepath.$path);
			redirect(base_url().'admin_panel/content/manage_file/'.$basename);

		}
	}
	function show_fileinfo22($path,$fname,$divid)
	{
		$objResponse = new xajaxResponse();
		$sScript = '';
		$filebasepath = './uploads/manage_file/';
		$sScript .= '
		var divs = document.getElementsByTagName("div");
		for(var ii=0; ii<divs.length;ii++)
		{
		var cdiv = divs[ii];
		if(cdiv.id.substring(0,9)=="imageinfo")
		{
		cdiv.style.display="none";
		}
		}
		';

		$filedata = get_file_info($filebasepath.$path.$fname);
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$mimetype = finfo_file($finfo, $filebasepath.$path.$fname);
		$imgdata = getimagesize($filebasepath.$path.$fname);
		if($imgdata)
		{
			$img = array();
			$img['width'] = @$imgdata[0];
			$img['height'] = @$imgdata[1];
			$mimetype = @$imgdata['mime'];
			$widthH = $img['width'].' x '.$img['height'];
		}
		else
		{
			$img = array();
		}

		$bytes = $filedata['size'];
		if ($bytes >= 1073741824)
		{
			$bytes = number_format($bytes / 1073741824, 2) . ' GB';
		}
		elseif ($bytes >= 1048576)
		{
			$bytes = number_format($bytes / 1048576, 2) . ' MB';
		}
		elseif ($bytes >= 1024)
		{
			$bytes = number_format($bytes / 1024, 2) . ' KB';
		}
		elseif ($bytes > 1)
		{
			$bytes = $bytes . ' bytes';
		}
		elseif ($bytes == 1)
		{
			$bytes = $bytes . ' byte';
		}
		else
		{
			$bytes = '0 bytes';
		}

		$date = date("d/m/Y g:i a", $filedata['date']);
		$size = $bytes; 
		if($fname=='')
		{
			$newfile = $basename;
		}
		else
		{
			$newfile = $fname;
		}

		$imgpath = base_url().'uploads/manage_file/'.$path.$newfile;
		ob_start();

		print'<table border="0" width="100%">
		<tr height="20">
		<td valign="top" width="400">
		'.$this->lang->line('admin_file_name').' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$newfile.' <br><br>
		'.$this->lang->line('admin_upload_date').' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$date.'<br><br>
		'.$this->lang->line('admin_file_size').' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$size.'<br><br>
		type &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$mimetype.'
		</td>
		<td valign="top" align="center">
		<img src="'.@$imgpath.'" height="150"><br>
		'.@$widthH.'
		</td>
		</tr>
		</table>';

		$pgcontent = ob_get_contents();
		ob_end_clean();
		$did = 'imageinfo'.$divid;
		$sScript .= 'if(document.getElementById("'.$did.'").style.display=="none")
		{
			document.getElementById("'.$did.'").style.display="block";
		}
		else
		{
			document.getElementById("'.$did.'").style.display="none";
		}
		';

		$objResponse->addAssign($did,"innerHTML",$pgcontent);
		$objResponse->addScript($sScript);
		return $objResponse->getXML();
	}
	
	function downloaduserfile()
	{
		$totalsegs = $this->uri->total_segments();
		$path ='';
		$filebasepath = './uploads/manage_file/';
		$basename = '';
		for($s=4;$s <=$totalsegs;$s++)
		{
			$path .= $this->uri->segment($s)."/";
		}
		$path = substr($path,0,strlen($path)-1);
		for($s=4;$s <=$totalsegs;$s++)
		{
			$basename = $this->uri->segment($s);
		}

		$downloadpath = $filebasepath.$path;
		$data = file_get_contents($downloadpath);
		force_download($basename,$data);

	}
	function edit_footer()
	{
		$this->load->model('pages_model');
		$data['pages'] = $this->pages_model->getPageDetailsById(7);
		$data['extra_head_content'] = $this->xajax->getjavascript(null, base_url() . 'assets/js/xajax.js');
		$data['page_title'] = $this->lang->line('admin_file_manage');
		$data['section_heading'] = $this->lang->line('admin_file_manage');
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/footer_edit',$data, true);
		$this->load->view('admin_panel/template',$data);
		
	}	
	//////////   dinesh code ends here  ///////////////
	function edit_footer_page()
	{
		$this->load->model('pages_model');
		$data = array();
		$page_id = 7;
		if($this->pages_model->checkIdIsAvaliable('static_pages','static_page_id',$page_id) == false)
		{
			redirect( base_url(). 'admin_panel/content');
		}
		$this->load->library('form_validation');
		$this->form_validation->set_rules('page_body', 'page_body', 'required');
		
		if($page_id)
		{
			$data['pages'] = $this->pages_model->getPageDetailsById($page_id);
			$data['page_name'] = $page_id;
		
		}

		if ($this->form_validation->run() == TRUE)
		{
			if( $this->pages_model->editfooterPage($page_id) == true )
			{
				$this->session->set_flashdata('success', $this->lang->line('admin_operation_success'));
				redirect( base_url(). 'admin_panel/content/index');
			}
			else
			{
				$this->session->set_flashdata('error', $this->lang->line('admin_operation_unsuccess'));
				redirect( base_url(). 'admin_panel/content/index');
			}		
		}
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		
		$data['page_title'] = $this->lang->line('admin_edit_pages');
		$data['section_heading'] = $this->lang->line('admin_edit_pages');
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/pages_edit',$data, true);
		$this->load->view('admin_panel/template',$data);
	}
	function manage_metadata()
	{
		$this->load->model('pages_model');
		$data = array();
		$txtSearch = $this->uri->segment(5, '');
		
		$fields = $this->pages_model->getAllPagesMeta($txtSearch);
		
		$data['txtSearch'] = $txtSearch;

		$this->load->library('pagination');
		
		$config['base_url'] = base_url().'admin_panel/content/manage_metadata';
		$config['total_rows'] = count($fields);
		$config['per_page'] = 100;
		$config['uri_segment'] = 4;
		$config['num_links'] = 2;
		if($txtSearch)
		$config['postfix_string'] = '/'.$txtSearch;
		
		$this->pagination->initialize($config); 
		
		$data['page_number'] = $this->uri->segment(4,0);
		
		$data['page_size'] = 100;
		$data['pages'] = $this->pages_model->getAllPagesMeta($txtSearch, $data['page_number'], $data['page_size']);
	
		//$data['pages'] = $this->pages_model->getAllPages();
		
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		
		$data['page_title'] = $this->lang->line('admin_listview_pages');
		$data['section_heading'] = $this->lang->line('admin_listview_pages');
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/manage_pages_meta',$data, true);
		$this->load->view('admin_panel/template',$data);
	}
	
	function edit_meta_page()
	{
		$this->load->model('pages_model');
		$data = array();
		$page_id = $this->uri->segment(4, 0);
		if($this->pages_model->checkIdIsAvaliable('static_pages','static_page_id',$page_id) == false)
		{
			redirect( base_url(). 'admin_panel/content');
		}
		$mode = $this->input->post("editpages");
		$this->load->library('form_validation');
		$this->form_validation->set_rules('pagetitle', 'pagetitle', 'required');
		$this->form_validation->set_rules('metadescription', 'metadescription', 'required');
		
		if($page_id)
		{
			$data['pages'] = $this->pages_model->getPageDetailsById($page_id);
			$data['mode'] = $mode;
			$data['page_name'] = $page_id;
		
		}

		if ($this->form_validation->run() == TRUE)
		{
			if($mode=="editpages")
			{
			
				if( $this->pages_model->editMetaPage($page_id) == true )
				{
				
					$this->session->set_flashdata('success', $this->lang->line('admin_operation_success'));
					redirect( base_url(). 'admin_panel/content/manage_metadata');
				}
				else
				{
					$this->session->set_flashdata('error', $this->lang->line('admin_operation_unsuccess'));
					redirect( base_url(). 'admin_panel/content/manage_metadata');
				}		
			}
		}
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		
		$data['page_title'] = $this->lang->line('admin_edit_pages');
		$data['section_heading'] = $this->lang->line('admin_edit_pages');
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/pages_meta_edit',$data, true);
		$this->load->view('admin_panel/template',$data);
	}
}
?>

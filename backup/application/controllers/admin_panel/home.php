<?php
class Home extends Controller {

	function Home()
	{
		parent::Controller();	
		$this->lang->load('admin', $this->config->item('language'));
		$this->load->library('authorize');
		
		if( $this->authorize->is_admin_logged_in() == false )
			redirect(base_url().'admin_panel/login');
			
		$this->load->library('Xajax');

		//$this->xajax->registerFunction(array('change_image',&$this,'change_image'));
		$this->xajax->processRequests();
	}
	
	function index()
	{
		$data = array();
		
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		
		$data['page_title'] = $this->lang->line('admin_home');
		$data['section_heading'] = $this->lang->line('admin_dashboard');
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/view',$data, true);
		$this->load->view('admin_panel/template',$data);

	}


	function change_password()
	{
		$this->load->model('user_authorization_model');
		$data = array();
		$data['mode'] = $this->uri->segment(3);		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('old_password', $this->lang->line('admin_old_password'), 'trim|required');
		$this->form_validation->set_rules('new_password', $this->lang->line('admin_new_password'), 'trim|required');
		$this->form_validation->set_rules('confirm_password', $this->lang->line('admin_confirm_password'), 'trim|required|matches[new_password]');
		
		if ($this->form_validation->run() == TRUE) // form submitted
		{
			if($data['mode'] = 'change_password')
			{
				if( $this->user_authorization_model->changeAdminPassword() == true )
				{
					$this->session->set_flashdata('success', $this->lang->line('admin_operation_success'));
					redirect( base_url(). 'admin_panel/home');
				}
				else
				{
					$this->session->set_flashdata('error', $this->lang->line('admin_operation_unsuccess'));
					redirect( base_url(). 'admin_panel/home/change_password');
				}
			}
		}

		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		
		$data['page_title'] = $this->lang->line('admin_change_password');
		$data['section_heading'] = $this->lang->line('admin_change_password');
		$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
		$data['page_body'] = $this->load->view('admin_panel/change_password',$data, true);
		$this->load->view('admin_panel/template',$data);
	}
}
?>

<?php

class Login extends Controller {

	function Login()
	{
		parent::Controller();	 
		$this->lang->load('admin', $this->config->item('language'));
		$this->load->library('authorize');
	}
	
	function index()
	{
		$data = array();
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', $this->lang->line('admin_label_username'), 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', $this->lang->line('admin_label_password'), 'trim|required');				
		
		if ($this->form_validation->run() == TRUE)
		{
			$user = $this->input->post('username');
			$pass = $this->input->post('password');
			if( $this->authorize->validate_admin_credentials($user, $pass) == true )
			{
				$this->session->set_flashdata('success', $this->lang->line('admin_login_success'));
				redirect(base_url().'admin_panel/home');
			}
			else
			{
				$this->session->set_flashdata('error', $this->lang->line('admin_message_unable_login'));
				redirect(base_url().'admin_panel/login');
			}
		}
	
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		
		$data['page_title'] = $this->lang->line('admin_login_page');
		$this->load->view('admin_panel/login_form',$data);
	}
	
	function logout()
	{
		if( $this->authorize->do_admin_logout() == true )
		{
			$this->session->set_flashdata('success', $this->lang->line('admin_message_logout'));
			redirect(base_url().'admin_panel/login');
		}
		else
		{
			redirect(base_url().'admin_panel/home');
		}
	}
	
}

?>

<?php
	class Orders extends Controller
	{
			function Orders()
			{
				parent::Controller();
				$this->lang->load('admin', $this->config->item('language'));
				$this->load->library('authorize');
				$this->load->model('users_model');
				if( $this->authorize->is_admin_logged_in() == false )
				{
					redirect(base_url().'admin_panel/login');
				}
				$this->load->model('shopping_model');
				$this->load->model('customize_model');
				$this->load->library('Xajax');
				$this->xajax->registerFunction(array('settracking',&$this,'settracking'));
				$this->xajax->processRequests();
			}
			function index(){
				$data = array();
				$month = $this->uri->segment(7,date('m'));
				$txtSearch = $this->uri->segment(8);
				$order_by = $this->uri->segment(5);
				$order_type = $this->uri->segment(6);
				if ($this->session->flashdata('success'))
				{
					$data['success'] = $this->session->flashdata('success');
				}		
				if ($this->session->flashdata('message'))
				{
					$data['message'] = $this->session->flashdata('message');
				}
				if ($this->session->flashdata('error'))
				{
					$data['error'] = $this->session->flashdata('error');
				}
				$this->load->library('pagination');
				$fields = $this->shopping_model->getOrderDetails_admin($txtSearch,$start_position=0, $pagesize=0 ,$orderBy='order_date' , $orderType='desc',$month);
				$graphQuery = $this->shopping_model->getAllMonthsOrder();
				$graphQuery2 = $this->shopping_model->getAllMonthsDateOrder($month);
				$config['base_url'] = base_url().'admin_panel/orders/index';
				$config['total_rows'] = count($fields);
				$config['per_page'] = $this->config->item('pagesize');
				$config['uri_segment'] = 4;
				$config['num_links'] = 2;
				if($txtSearch)
				$config['postfix_string'] = '/'.$txtSearch;
				if($order_by)
				{
					$config['postfix_string'] = '/'.$order_by;
				}
				else
				{
					$config['postfix_string'] = '/order_date';
					$order_by = 'order_date';
				}
				if($order_type)
				{
					$config['postfix_string'] .= '/'.$order_type;
				}
				else
				{
					$config['postfix_string'] .= '/desc';
					$order_type = 'desc';
				}
				$config['postfix_string'] .= '/'.$month;
				if($txtSearch)
				{
					$config['postfix_string'] .= '/'.$txtSearch;
				}
				
				$this->pagination->initialize($config); 
				$data['order_by'] = $order_by ;
				$data['order_type'] = $order_type ;
				$data['page_number'] = $this->uri->segment(4,0);
				$data['sel_month'] = $month;
				$data['graphQuery'] = $graphQuery;
				$data['graphQuery2'] = $graphQuery2;
				
				$data['page_size'] = $this->config->item('pagesize');
				$data['txtSearch']=$txtSearch;
				$response = $this->shopping_model->getOrderDetails_admin($txtSearch, $data['page_number'], $data['page_size'],$data['order_by'],$data['order_type'],$month);
				$data['orders'] = $response;
				$data['page_title'] ='Manage Orders';
				$data['section_heading'] = $this->lang->line('admin_dashboard');
				$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
				$data['page_body'] = $this->load->view('admin_panel/manage_orders',$data, true);
				$this->load->view('admin_panel/template',$data);
			}
			function order_detail_byid()
			{
				
				$order_id = $this->uri->segment(4);
				$order_info = $this->shopping_model->get_order_details_by_id($order_id);
				if(count($order_info))
				{
					$data['order_details'] = $order_info;
				}
				else
				{
					redirect(base_url().'admin_panel/orders');
				}
				
				if ($this->session->flashdata('success'))
				{
					$data['success'] = $this->session->flashdata('success');
				}		
				if ($this->session->flashdata('message'))
				{
					$data['message'] = $this->session->flashdata('message');
				}
				if ($this->session->flashdata('error'))
				{
					$data['error'] = $this->session->flashdata('error');
				}
				$data['orderid'] = $order_id;
				$data['page_title'] ='Manage Orders';
				$data['section_heading'] = $this->lang->line('admin_dashboard');
				$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
				$data['page_body'] = $this->load->view('admin_panel/view_order_detail',$data, true);
				$this->load->view('admin_panel/template',$data);
			}
			function billing_info()
			{
				$order_id = $this->uri->segment(4);
				$data['orderid'] = $order_id;
				$billing_info = $this->shopping_model->get_order_details_by_id($order_id);
				
				$user_id=$billing_info['userid'];
				$data['user_info']=$this->shopping_model->get_user_info($user_id);
				$data['extra_head_content']= $this->xajax->getjavascript(null, base_url() . 'assets/js/xajax.js');
				$data['billing_info']=$billing_info;
				$data['page_title'] ='Billing Information';
				$data['section_heading'] = $this->lang->line('admin_dashboard');
				$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
				$data['page_body'] = $this->load->view('admin_panel/view_billing_info',$data, true);
				$this->load->view('admin_panel/template',$data);
			}
			function product_detail()
			{
				$order_id = $this->uri->segment(4);
				$order_info = $this->shopping_model->order_info($order_id);
				
				$data['extra_head_content']= $this->xajax->getjavascript(null, base_url() . 'assets/js/xajax.js');
				$data['orderid'] = $order_id;
				$data['order_info'] = $order_info;
				$data['page_title'] ='Product Information';
				$data['section_heading'] = $this->lang->line('admin_dashboard');
				$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
				$data['page_body'] = $this->load->view('admin_panel/view_product_info',$data, true);
				$this->load->view('admin_panel/template',$data);
			}
			function product_images()
			{
				$order_id = $this->uri->segment(4);
				$order_info = $this->shopping_model->order_info($order_id);
				//echo "<pre>";
				//print_r($order_info);
				$data['extra_head_content']= $this->xajax->getjavascript(null, base_url() . 'assets/js/xajax.js');
				$data['orderid'] = $order_id;
				$data['order_info'] = $order_info;
				$data['page_title'] ='Product Images';
				$data['section_heading'] = $this->lang->line('admin_dashboard');
				$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
				$data['page_body'] = $this->load->view('admin_panel/view_product_images',$data, true);
				$this->load->view('admin_panel/template',$data);
			}
			function notes()
			{
				$order_id = $this->uri->segment(4);
				$this->load->library('form_validation');
				$this->form_validation->set_rules('notetext', 'Text', 'required');
				
				if ($this->form_validation->run() == TRUE)
				{
					if($this->shopping_model->edit_note() == true)
					{
						$this->session->set_flashdata('success', $this->lang->line('admin_operation_success'));
						redirect(base_url().'admin_panel/orders/');
					}
				}
				//echo "<pre>";
				//print_r($order_info);
				$note = $this->shopping_model->getNoteById($order_id);
				$data['extra_head_content']= $this->xajax->getjavascript(null, base_url() . 'assets/js/xajax.js');
				$data['orderid'] = $order_id;
				$data['note'] = $note;
				$data['page_title'] ='Order Notes';
				$data['section_heading'] = $this->lang->line('admin_dashboard');
				$data['top_links'] = $this->load->view('admin_panel/sections/top_links', $data, true);
				$data['page_body'] = $this->load->view('admin_panel/view_order_notes',$data, true);
				$this->load->view('admin_panel/template',$data);
			}
			function download_orders()
			{
				function cleanData(&$str) {
					$str = preg_replace("/\t/", "\\t", $str); 
					$str = preg_replace("/\r?\n/", "\\n", $str);
					if(strstr($str, '"'))
					$str = '"' . str_replace('"', '""', $str) . '"'; 
				} 
				// filename for download 
				$order_date1 = $this->input->post('senddate1');
				$order_date2 = $this->input->post('senddate2');
				$data1 = $this->shopping_model->getAllOrdersByDate($order_date1,$order_date2);

				$filename = "Orders-" . date('Ymdhis') . ".xls"; 
				header("Content-Disposition: attachment; filename=\"$filename\""); 
				header("Content-Type: application/vnd.ms-excel"); 
				$flag = false; 
				foreach($data1 as $row)
				{ 
					if(!$flag) {
						 // display field/column names as first row 
						 $keys= array_keys($row);
						 echo implode("\t", array_keys($row)) . "\r\n";
						 $flag = true;
					 } 
					array_walk($row, 'cleanData');
					echo implode("\t", array_values($row)) . "\r\n"; 
				}
			}
			
			function change_orderstatus()
			{
				if(count($_POST)<=1)
				{
					redirect(base_url().'admin_panel/orders');
				}
				$res = $this->shopping_model->change_orderstatus();
				if($res)
				{
					$this->session->set_flashdata('success', $this->lang->line('admin_operation_success'));
					redirect(base_url().'admin_panel/orders');
				}
			}
			
			function importcsv()
			{
				$resp = $this->shopping_model->import_csv();
				if($resp)
				{
					$this->session->set_flashdata('success', $this->lang->line('admin_operation_success'));
					redirect(base_url().'admin_panel/orders');
				}
			}
			
	}

?>

<?php
class Home extends Controller
{
	var $key = 'Ahk8IHLMfNtjpTNwtPrTH1gGD';
	var $secret = 'xeD3NTsksNOzSUfKjlnQ621m5G9974VL2uOuHLIz1OOEcv0Iti';
	var $request_token = "https://twitter.com/oauth/request_token";
	var $access_token = "https://api.twitter.com/oauth/access_token";
	var $get_user_details = "https://api.twitter.com/1.1/account/verify_credentials.json";
	
	function Home()
	{
		//session_start();session_destroy();die;
		parent::Controller();
		$this->config->set_item('enable_query_strings', TRUE);
		$this->load->model('register_user');
		$this->load->model('category_model');
		$this->load->model('member_model');
		$this->load->model('wallet_model');
		$this->load->model('betfair_model');
		$this->load->library('authorize');
		$this->load->library('paypal');
		$remoteAddr = $_SERVER['REMOTE_ADDR'];
		error_reporting(0);
		if($_SERVER['REMOTE_ADDR']=='127.0.0.1' || $_SERVER['REMOTE_ADDR']=='::1' || strstr($_SERVER['REMOTE_ADDR'],'192.168.')==$_SERVER['REMOTE_ADDR'])
		{
			$remoteAddr = '122.173.135.80';
		}
		//$timeZone = $this->authorize->getUserTimeZone($remoteAddr);
		$timeZone = $this->session->userdata('timezone');
		if($timeZone=='')
		{
			$timeZone = $this->authorize->getUserTimeZone($remoteAddr);
			$this->session->set_userdata('timezone',$timeZone);
		}
		$this->session->set_userdata('timezone',$timeZone);
		date_default_timezone_set($timeZone);
	}
	
	function index()
	{
		/*$data = array();
		$newdata = array(
					'twitter_id'  => '121212',
					'first_name' => 'satish',				
					'via' => 'twitter',					
					'signup_date' => date('Y/m/d H:m:s')                 
               );
			$this->session->set_userdata('twittersess',$newdata);
		$resp = $this->authorize->user_loggedIn();
		if($resp===true)
		{
			//session_start();session_destroy();die;
			redirect(base_url().'select');
		}
		$this->load->view("template_landing",$data);	
		*/
		$data = array();
		if($this->session->userdata('twitter_auth'))
		{
			$data['auth'] = $this->session->userdata('twitter_auth');
		}
		else
		{
			$data['auth'] = '';
		}
		$this->load->view("new_design",$data);	
	}
	
	function activate()
	{
		$decode = $this->uri->segment(3);
		$code = base64_decode($decode);
		$emailcode = explode('&',$code);
		$email = $emailcode[0];
		$code = $emailcode[1];
		$get = $this->register_user->verifyCode($email,$code);
		
		if($get == true)
		{				
			if( $this->authorize->validate_user_credentials_social($email) == true )
				{
					
					redirect(base_url().'home');
				}
		}
		else{
			
				redirect(base_url().'home');
			}
	}
	
	function tipster_list(){
		$data = array();
		$resp = $this->authorize->user_loggedIn();
		if($resp==false)
		{
			$data['login_status'] = 0;
		}else{
			$data['login_status'] = 1;
			}
			//$users = $this->member_model->getAllMembers();
		$event_type = $this->uri->segment(2,0);
		$userId = $this->session->userdata('userid');
		if($event_type)
		{
			$category = $this->category_model->getAllCategories();
			$eventType = $event_type;
		}
		else
		{
			$category = $this->category_model->getAllCategories();
			$eventType = $category[0]['slugUrl'];
		}
		//$a->$b;
		$data['eventType'] = $eventType;
		$event_type_id = $this->betfair_model->getCompetitionId($eventType);
		$data['event_type_id'] = $event_type_id ;
		$users = $this->betfair_model->getuserBetPlacedEvents($event_type_id);
		$ii=0;
		foreach($users as $user)
		{
			
				$result[$ii]['rank'] = $ii+1;
				$result[$ii]['user_id'] = $user['userid'];
				$result[$ii]['username'] = $user['username'];
				$result[$ii]['firstname'] = $user['first_name'];
				$result[$ii]['lastname'] = $user['last_name'];
				$result[$ii]['slugurl'] = $user['username'];
				$result[$ii]['profit'] = $user['profit'];
				$result[$ii]['tw_token'] = $user['twitter_token'];
				$result[$ii]['tw_screen_name'] = $user['tw_screen_name'];
				$result[$ii]['total_placed_tips'] = $user['total_placed_tips'];
				if((strstr($user['profile_image'],'http://')==$user['profile_image'] || strstr($user['profile_image'],'https://')==$user['profile_image']) && $user['profile_image']!='')
				{
					if(strstr($user['profile_image'],'https://graph.facebook.com')==$user['profile_image'])
					{
						$result[$ii]['image'] = str_replace('normal','large',$user['profile_image']);
						$result[$ii]['image_margin'] = '42px';
					}
					else
					{
						$result[$ii]['image'] = str_replace('_normal','',$user['profile_image']);
						$result[$ii]['image_margin'] = '42px';
					}
				}
				else
				{
					if($user['profile_image']!='' && file_exists('uploads/profile_picture/thumb_'.$user['profile_image']))
					{
						$result[$ii]['image'] = base_url().'uploads/profile_picture/small_'.$user['profile_image'];
						$result[$ii]['image_margin'] = '0px;';
					}
					else
					{
						$result[$ii]['image'] = base_url().'assets/images/small_no_profile_pic.jpg';
						$result[$ii]['image_margin'] = '0px;';
					}
				}
				
				$ii++;
			
		}
		$data['total'] = count($result);
		$data['result'] = $result;
		$wallet_data = $this->wallet_model->getUserWallet($userId);
		$data['wallet_data'] = $wallet_data;
		$data['categories'] = $category;
		$data['event_type'] = $event_type;
		$data['event_type_id'] = $event_type_id;
		$userId = $this->session->userdata('userid');
		$data['userData'] = $this->member_model->getMemberDetailsById($userId);
		if($event_type =='soccer')
		{
			$data['colortheme'] = "#B41414";
			$data['type_tip'] = "purchase";
			$data['page_body'] = $this->load->view("soccer",$data,true);
		}
		else if($event_type =='cricket')
		{
			$data['colortheme'] = "#2888C9";
			$data['type_tip'] = "purchase";
			$data['page_body'] = $this->load->view("cricket",$data,true);
		}
		
		else if($event_type =='basketball')
		{
			$data['colortheme'] = "#2888C9";
			$data['type_tip'] = "purchase";
			$data['page_body'] = $this->load->view("basketball",$data,true);
		}
		
		else if($event_type =='tennis-cup')
		{
			$data['colortheme'] = "#60D1CF;";
			$data['type_tip'] = "purchase";
			$data['page_body'] = $this->load->view("tennis",$data,true);
		}
		
		else if($event_type =='hockey-league')
		{
			$data['colortheme'] = "#2888C9";
			$data['type_tip'] = "purchase";
			$data['page_body'] = $this->load->view("hockey",$data,true);
		}
		
		else if($event_type =='boxing-league')
		{
			$data['colortheme'] = "#ED281B";
			$data['type_tip'] = "purchase";
			$data['page_body'] = $this->load->view("boxing",$data,true);
		}
		
		else if($event_type =='rugby-league' || $event_type =='rugby-union')
		{
			$data['colortheme'] = "#FFC814";
			$data['type_tip'] = "purchase";
			$data['page_body'] = $this->load->view("rugby",$data,true);
		}
		
		else if($event_type =='australian-rules')
		{
			$data['colortheme'] = "#ED281B";
			$data['type_tip'] = "purchase";
			$data['page_body'] = $this->load->view("austrialian_rules",$data,true);
		}
		
		else if($event_type =='baseball')
		{
			$data['colortheme'] = "#000000";
			$data['type_tip'] = "purchase";
			$data['page_body'] = $this->load->view("baseball",$data,true);
		}
		
		else if($event_type =='martial-arts')
		{
			$data['colortheme'] = "#B41414";
			$data['type_tip'] = "purchase";
			$data['page_body'] = $this->load->view("martial_art",$data,true);
		}
		else if($event_type =='golf')
		{
			$data['colortheme'] = "#000000";
			$data['type_tip'] = "purchase";
			$data['page_body'] = $this->load->view("golf",$data,true);
		}
		else if($event_type =='motor-sport')
		{
			$data['colortheme'] = "#FFC814";
			$data['type_tip'] = "purchase";
			$data['page_body'] = $this->load->view("motor",$data,true);
		}
		else if($event_type =='american-football')
		{
			$data['colortheme'] = "#B41414";
			$data['type_tip'] = "purchase";
			$data['page_body'] = $this->load->view("american_football",$data,true);
		}
		else if($event_type =='surfing')
		{
			$data['colortheme'] = "#0FB3EE";
			$data['type_tip'] = "purchase";
			$data['page_body'] = $this->load->view("surfing",$data,true);
		}
		
		//$data['page_body'] = $this->load->view("categories",$data,true);	
		$this->load->view("templates_main",$data);		
	}
	
	function purchase_list() {
		$data = array();
		$data['sess_user_id'] = $this->session->userdata('userid');
		$data['whisperTipFee'] = $this->config->item('whispertipfee');
		$eventType = $this->input->post('eventType');
		$user_id = $this->input->post('user_id');
		$status = $this->input->post('status');
		$data['user_id'] = $user_id;
		if($eventType)
		{
			$category = $this->category_model->getAllCategories();
			$eventType = $eventType;
		}
		else
		{
			$category = $this->category_model->getAllCategories();
			$eventType = $category[0]['slugUrl'];
		}
		$data['eventType'] = $eventType;
		$event_type_id = $this->betfair_model->getCompetitionId($eventType);
		$data['event_type_id'] = $event_type_id ;
		$users = $this->betfair_model->getuserBetPlacedEvents($event_type_id);
		$data['tipsPlaced'] = $this->betfair_model->getuserBetPlacedEventsByUser($user_id,$event_type_id);
		$data['tipsPlacedEvent'] = $this->betfair_model->getuserBetPlacedEventsByUser2($user_id,$event_type_id);
		$ii=0;
		foreach($users as $user)
		{
			
				$result[$ii]['rank'] = $ii+1;
				$result[$ii]['user_id'] = $user['userid'];
				$result[$ii]['username'] = $user['username'];
				$result[$ii]['firstname'] = $user['first_name'];
				$result[$ii]['lastname'] = $user['last_name'];
				$result[$ii]['slugurl'] = $user['username'];
				$result[$ii]['profit'] = $user['profit'];
				$result[$ii]['tw_screen_name'] = $user['tw_screen_name'];
				$result[$ii]['total_placed_tips'] = $user['total_placed_tips'];
				if((strstr($user['profile_image'],'http://')==$user['profile_image'] || strstr($user['profile_image'],'https://')==$user['profile_image']) && $user['profile_image']!='')
				{
					if(strstr($user['profile_image'],'https://graph.facebook.com')==$user['profile_image'])
					{
						$result[$ii]['image'] = str_replace('normal','large',$user['profile_image']);
						$result[$ii]['image_margin'] = '42px';
					}
					else
					{
						$result[$ii]['image'] = str_replace('_normal','',$user['profile_image']);
						$result[$ii]['image_margin'] = '42px';
					}
				}
				else
				{
					if($user['profile_image']!='' && file_exists('uploads/profile_picture/thumb_'.$user['profile_image']))
					{
						$result[$ii]['image'] = base_url().'uploads/profile_picture/small_'.$user['profile_image'];
						$result[$ii]['image_margin'] = '0px;';
					}
					else
					{
						$result[$ii]['image'] = base_url().'assets/images/small_no_profile_pic.jpg';
						$result[$ii]['image_margin'] = '0px;';
					}
				}
				
				$ii++;
			
		}
		if($eventType =='soccer')
		{
			$data['colortheme'] = "#B41414";
		}
		else if($eventType =='cricket')
		{
			$data['colortheme'] = "#2888C9";
		}
		
		else if($eventType =='basketball')
		{
			$data['colortheme'] = "#2888C9";
		}
		
		else if($eventType =='tennis-cup')
		{
			$data['colortheme'] = "#60D1CF;";
		}
		
		else if($eventType =='hockey-league')
		{
			$data['colortheme'] = "#2888C9";
		}
		
		else if($eventType =='boxing-league')
		{
			$data['colortheme'] = "#ED281B";
		}
		
		else if($eventType =='rugby-league' || $eventType =='rugby-union')
		{
			$data['colortheme'] = "#FFC814";
		}
		
		else if($eventType =='australian-rules')
		{
			$data['colortheme'] = "#ED281B";
		}
		
		else if($eventType =='baseball')
		{
			$data['colortheme'] = "#000000";
		}
		
		else if($eventType =='martial-arts')
		{
			$data['colortheme'] = "#B41414";
		}
		else if($eventType =='golf')
		{
			$data['colortheme'] = "#000000";
		}
		else if($eventType =='motor-sport')
		{
			$data['colortheme'] = "#FFC814";
		}
		else if($eventType =='american-football')
		{
			$data['colortheme'] = "#B41414";
		}
		else if($eventType =='surfing')
		{
			$data['colortheme'] = "#0FB3EE";
		}
		$data['total'] = count($result);
		$data['result'] = $result;
		$userId = $this->session->userdata('userid');
		$wallet_data = $this->wallet_model->getUserWallet($userId);
		$data['wallet_data'] = $wallet_data;
		$data['categories'] = $category;
		$data['event_type_id'] = $event_type_id;
		$data['timeZonedata'] = $this->session->userdata('timezone');
		if($status ==0)
		{
			echo $data['page_body'] = $this->load->view("purchase_list",$data);
		}
		else
		{
			
			$flag = $this->input->post('flag');
			$data['flag'] = $flag;
			if($flag==1)
			{
				$data['tips_placed'] = $this->betfair_model->getAllTipsPlacedByUser($user_id,$event_type_id);
				$data['event_id'] = $event_id;
			}
			else {
				$event_id = $this->input->post('event_id');
				$data['tips_placed'] = $this->betfair_model->getTipsPlacedByEvents($user_id,$event_id);
				$data['event_id'] = $event_id;
			}
			echo $data['page_body'] = $this->load->view("purchaser_tips",$data);
		}
	}
	function purchase_tips(){
		$params = $this->uri->uri_to_assoc(2);
		$data = array();
		$resp = $this->authorize->user_loggedIn();
		if($resp==false)
		{
			$data['login_status'] = 0;
		}else{
			$data['login_status'] = 1;
			}
		$members = $this->member_model->getAllMembers();
		$category = $this->category_model->getAllCategories();
		$data['categories'] = $category;
		$data['page_body'] = $this->load->view("categories",$data,true);	
		$this->load->view("templates_main",$data);	
	}
	/*function place_tips(){
		$params = $this->uri->uri_to_assoc(2);

		$data = array();
		$resp = $this->authorize->user_loggedIn();
		if($resp==false)
		{
			redirect(base_url().'home');
		}
		$members = $this->member_model->getAllMembers();
		$category = $this->category_model->getAllCategories();
		$data['categories'] = $category;
		$data['page_body'] = $this->load->view("place_tips",$data,true);	
		$this->load->view("templates_main",$data);	
	}
	*/
	
	function login()
	{
	 	$userName = $this->input->post('lguserName');
	 	$userPassword = $this->input->post('lguserPassword');
		$resp = $this->register_user->login($userName,$userPassword);
		if ($resp == false) 
		{ 
			$response = 'error';
			echo json_encode($response);
		}
		else
		{
			$response = 'done';
			echo json_encode($response);
		}			
	}
	
	function forgot_password()
	{
		$data = array();
	 	$email = $this->input->post('forgotEmail');
		$resp = $this->register_user->check_email($email);
		if ($resp == false) 
		{ 
			echo json_encode('error');
		}
		else
		{
			$to = $email;
			$email_template_id = 4;
			$first_name = $resp['first_name'];
			$last_name = $resp['last_name'];
			$name = $resp['username'];
			$password = $resp['password'];
			
			$email_content = $this->getEmailTemplate($email_template_id);
			$message = $email_content[0]['body'];
			
			$search = array('{FIRSTNAME}','{LASTNAME}','{SITENAME}','{USEREMAIL}','{USERNAME}','{PASSWORD}', '{SITELINK}');
			$replace= array($first_name,$last_name,'Whispertip',$to,$name,$password,'www.whispertip.com/');
			$body=str_replace($search,$replace,$message);	
			$data['template_body'] = $body;
			$page = $this->load->view('email_template',$data,true);	
			$config['charset'] = 'iso-8859-1';
			$config['mailtype'] = 'html';
			$subject = $email_content[0]['subject'];
			$from = $email_content[0]['from'];
			$this->load->library('email');
			$this->email->initialize($config);
			$this->email->from($from);
			$this->email->to($to);
			$this->email->subject($subject);
			$this->email->message($page);
			$email_status = $this->email->send();
			echo json_encode('done');
		}			
	}
	function getEmailTemplate($id)
	{
		$this->db->select('*');
		$this->db->where('email_template_id', $id);
		$query = $this->db->get('email_templates');
		return $query->result_array();
	}
	
	function seloperation()
	{
	 	$type = $this->uri->segment(2,0);
	 	$this->session->set_userdata('seltype',$type);
	 	if($type=='purchase')
	 	{
			redirect(base_url().'tipsters');
		}
		else if($type=='place')
		{
			redirect(base_url().'place-tips');
		}
	}
	
	function logout()
	{
	 	$this->session->unset_userdata('useremail');
	 	$this->session->unset_userdata('userid');
	 	$this->session->unset_userdata('username');
	 	$this->session->unset_userdata('user_loggedin');
	 	$this->session->unset_userdata('seltype');
	 	$this->session->unset_userdata('twitterconnectsess');
	 	$this->session->unset_userdata('typeTw');
		header('Location:'.base_url().'home');
	}
	
	function getCategories()
	{
		$resp = $this->authorize->user_loggedIn();
		if($resp===true)
		{
			redirect(base_url().'select');
		}
		$category = $this->category_model->getAllCategories();
		$ii=0;
		foreach($category as $cat)
		{
			$result[$ii]['category_id'] = $cat['name'];
			$result[$ii]['name'] = $cat['name'];
			$result[$ii]['description'] = $cat['description'];
			$result[$ii]['slugurl'] = $cat['slugUrl'];
			$result[$ii]['image'] = base_url().'uploads/category_images/thumb_'.$cat['image'];
			$result[$ii]['games_upcoming'] = count($this->betfair_model->CountCompetetionByEventId($cat['event_type_id']));
			$result[$ii]['event_type_id'] = $cat['event_type_id'];
			$ii++;
		}
		
		$data['total'] = count($result);
		$data['result'] = $result;
		//echo "<pre>";
		//print_r($data);
		echo json_encode($data);
		
	}
	function getUserPlacedTip()
	{
		$resp = $this->authorize->user_loggedIn();
		if($resp===true)
		{
			//session_start();session_destroy();die;
			redirect(base_url().'select');
		}
		$category = $this->category_model->getAllCategories();
		$userId = $this->uri->segment(2,0);
		//echo "<pre>";print_r($category);die;
		$ii=0;
		foreach($category as $cat)
		{
			$result[$ii]['event_type_id'] = $cat['event_type_id'];
			$result[$ii]['category_id'] = $cat['name'];
			$result[$ii]['name'] = $cat['name'];
			$result[$ii]['description'] = $cat['description'];
			$result[$ii]['slugurl'] = $cat['slugUrl'];
			$result[$ii]['image'] = base_url().'uploads/category_images/thumb_'.$cat['image'];
			//$result[$ii]['tips_placed'] = count($this->betfair_model->getUserPlacedTips($cat['event_type_id'],$userId));
			$ii++;
		}
		
		$data['total'] = count($result);
		$data['result'] = $result;
		//echo "<pre>";
		//print_r($data);
		echo json_encode($data);
		
	}
	function getTipsters()
	{
		$resp = $this->authorize->user_loggedIn();
		if($resp===true)
		{
			//session_start();session_destroy();die;
			redirect(base_url().'select');
		}
		//$users = $this->member_model->getAllMembers();
		$event_type = $this->uri->segment(2);
		$event_type_id = $this->betfair_model->getCompetitionId($event_type);
		//$event_competitons = $this->betfair_model->getEventsCompetition($event_type_id); // get all competitions
		$users = $this->betfair_model->getuserBetPlacedEvents($event_type_id);
		$ii=0;
		foreach($users as $user)
		{
			
				$result[$ii]['rank'] = $ii+1;
				$result[$ii]['user_id'] = $user['userid'];
				$result[$ii]['username'] = $user['username'];
				$result[$ii]['firstname'] = $user['first_name'];
				$result[$ii]['lastname'] = $user['last_name'];
				$result[$ii]['slugurl'] = $user['username'];
				$result[$ii]['profit'] = $user['profit'];
				$result[$ii]['tw_screen_name'] = $user['tw_screen_name'];
				$result[$ii]['total_placed_tips'] = $user['total_placed_tips'];
				if((strstr($user['profile_image'],'http://')==$user['profile_image'] || strstr($user['profile_image'],'https://')==$user['profile_image']) && $user['profile_image']!='')
				{
					if(strstr($user['profile_image'],'https://graph.facebook.com')==$user['profile_image'])
					{
						$result[$ii]['image'] = str_replace('normal','large',$user['profile_image']);
						$result[$ii]['image_margin'] = '42px';
					}
					else
					{
						$result[$ii]['image'] = str_replace('_normal','',$user['profile_image']);
						$result[$ii]['image_margin'] = '42px';
					}
				}
				else
				{
					if($user['profile_image']!='' && file_exists('uploads/profile_picture/thumb_'.$user['profile_image']))
					{
						$result[$ii]['image'] = base_url().'uploads/profile_picture/small_'.$user['profile_image'];
						$result[$ii]['image_margin'] = '0px;';
					}
					else
					{
						$result[$ii]['image'] = base_url().'assets/images/small_no_profile_pic.jpg';
						$result[$ii]['image_margin'] = '0px;';
					}
				}
				
				$ii++;
			
		}
		$data['total'] = count($result);
		$data['result'] = $result;
		//echo "<pre>";
		//print_r($data);
		echo json_encode($data);
		
	}
	
	function profile(){
		$data = array();
		$resp = $this->authorize->user_loggedIn();
		if($resp==false)
		{
			redirect(base_url().'home');
		}
		else{
			$data['login_status'] = 1;
			}
		$this->load->library('form_validation');
		$this->form_validation->set_rules('first_name', 'First name', 'trim|required');
		$this->form_validation->set_rules('last_name', 'Last name', 'trim|required');
		$this->form_validation->set_rules('dob', 'Date of birth', 'trim|required');
		
		if ($this->form_validation->run() == TRUE) // form submitted
		{
			if($this->member_model->updateProfile() == true )
			{
				$this->session->set_flashdata('success','Changes updated successfully.');
				redirect( base_url(). 'home/profile');
			}
			else
			{
				$this->session->set_flashdata('error', 'Unable to update changes, Please try again later.');
				redirect( base_url(). 'home/profile');
			}
		}
		
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		$data['type_tip'] = '';
		$isToken = $this->register_user->getTwitterToken();
		if($isToken)
		{
			$res1 = $isToken;
			$sp1 = explode('&',$res1);
			$final = explode('=',$sp1[0]);
			$final1 = explode('=',$sp1[1]);
			$oauth = array( 'oauth_consumer_key' => $this->key,
                'oauth_nonce' => time(),
                'oauth_signature_method' => 'HMAC-SHA1',
                'oauth_token' => $final[1],
                'oauth_timestamp' => time(),
                'oauth_version' => '1.0');
			$url = "https://api.twitter.com/1.1/account/verify_credentials.json";
			$base_info = $this->buildBaseString($url, 'GET', $oauth);
			$composite_key = rawurlencode($this->secret) . '&' . rawurlencode($final1[1]);
			$oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
			$oauth['oauth_signature'] = $oauth_signature;
			$header = array($this->buildAuthorizationHeader($oauth), 'Expect:');
			$options = array( CURLOPT_HTTPHEADER => $header,
							  CURLOPT_HEADER => false,
							  CURLOPT_URL => $url,
							  CURLOPT_RETURNTRANSFER => true,                 
							  CURLOPT_SSL_VERIFYPEER => false);

			$feed = curl_init();
			curl_setopt_array($feed, $options);
			$json = curl_exec($feed);
			curl_close($feed);
			//var_dump($json);
			$twitter_data = json_decode($json,true);
			$newdata['followers_count'] = $twitter_data['followers_count'];
		    $newdata['friends_count']   = $twitter_data['friends_count'];
		    $newdata['statuses_count']  = $twitter_data['statuses_count'];
		    $this->session->set_userdata('twitterconnectsess',$newdata);
		}
		$data['colortheme'] = "#2888C9";
		$userId = $this->session->userdata('userid');
		$userData = $this->member_model->getMemberDetailsById($userId);
		$category = $this->category_model->getAllCategories();
		$data['userData'] = $userData;
		$data['categories'] = $category;
		$data['page_body'] = $this->load->view("profile",$data,true);	
		$this->load->view("templates_main",$data);	
	}
	
	function getpaid(){
		$data = array();
		$resp = $this->authorize->user_loggedIn();
		if($resp==false)
		{
			redirect(base_url().'home');
		}
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('customerprice', 'Paid to you', 'trim|required');
		$this->form_validation->set_rules('purchaseamount', 'Charged to purchase', 'trim|required');
		
		if ($this->form_validation->run() == TRUE) // form submitted
		{
			if($this->member_model->updateGetPaid() == true )
			{
				$this->session->set_flashdata('success','Changes updated successfully.');
				redirect( base_url(). 'home/getpaid');
			}
			else
			{
				$this->session->set_flashdata('error', 'Unable to update changes, Please try again later.');
				redirect( base_url(). 'home/getpaid');
			}
		}
		
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		
		$userId = $this->session->userdata('userid');
		$userData = $this->member_model->getMemberDetailsById($userId);
		$category = $this->category_model->getAllCategories();
		$paidDetails = $this->member_model->getPaidDetails();
		$data['userData'] = $userData;
		$data['paidDetails'] = $paidDetails;
		$data['categories'] = $category;
		$data['page_body'] = $this->load->view("getpaid",$data,true);	
		$this->load->view("templates_main",$data);	
	}
	
	function select()
	{
		$data = array(); 
		$resp = $this->authorize->user_loggedIn();
		if($resp==false)
		{
			$data['login_status'] = 0;
		}else{
			$data['weekly_tipster'] = $this->betfair_model->getCurrentWeekTipster();
			if($data['weekly_tipster']){
				$week_start_date = $data['weekly_tipster']['week_start_date'];
				$week_end_date = $data['weekly_tipster']['week_end_date'];
				$data['tipster'] = $this->betfair_model->getTipsterProfile($data['weekly_tipster']['tipster_id'],$week_start_date,$week_end_date);
				
				$total_placed = $data['tipster']['total_tips'];
				$total_won = $data['weekly_tipster']['correct_tips'];
				$strikeRate = round(($total_won/$total_placed)*100,2);
				$data['tipster']['strike_rate'] = $strikeRate;
				$isToken = $this->register_user->getTwitterPublicToken($data['weekly_tipster']['tipster_id']);
				$data['twitter_user_id'] = '';
				if($isToken)
				{
					$res1 = $isToken;
					$sp1 = explode('&',$res1);
					$final = explode('=',$sp1[0]);
					$final1 = explode('=',$sp1[1]);
					$user_id_twitter = explode('-',$final[1]);
					$data['twitter_user_id'] = $user_id_twitter[0];
					$oauth = array( 'oauth_consumer_key' => $this->key,
						'oauth_nonce' => time(),
						'oauth_signature_method' => 'HMAC-SHA1',
						'oauth_token' => $final[1],
						'oauth_timestamp' => time(),
						'oauth_version' => '1.0');
					$url = "https://api.twitter.com/1.1/account/verify_credentials.json";
					$base_info = $this->buildBaseString($url, 'GET', $oauth);
					$composite_key = rawurlencode($this->secret) . '&' . rawurlencode($final1[1]);
					$oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
					$oauth['oauth_signature'] = $oauth_signature;
					$header = array($this->buildAuthorizationHeader($oauth), 'Expect:');
					$options = array( CURLOPT_HTTPHEADER => $header,
									  CURLOPT_HEADER => false,
									  CURLOPT_URL => $url,
									  CURLOPT_RETURNTRANSFER => true,                 
									  CURLOPT_SSL_VERIFYPEER => false);

					$feed = curl_init();
					curl_setopt_array($feed, $options);
					$json = curl_exec($feed);
					curl_close($feed);
					//var_dump($json);
					$twitter_data = json_decode($json,true);
					$data['followers_count'] = $twitter_data['followers_count'];
					$data['friends_count']   = $twitter_data['friends_count'];
					$data['statuses_count']  = $twitter_data['statuses_count'];
				}	
			}
			$data['login_status'] = 1;
			}
		$data['type_tip'] ='';
		$userId = $this->session->userdata('userid');
		$data['userData'] = $this->member_model->getMemberDetailsById($userId);
		$resp = $this->authorize->user_loggedIn();
		$data['status'] = $this->uri->segment(2,0);
		$category = $this->category_model->getAllCategories();
		$data['category'] = $category;
		$data['colortheme'] = "#2888C9";
		$data['page_body'] = $this->load->view("user_profile_latest",$data,true);	
		$this->load->view("templates_main",$data);	
	}
	function mywallet()
	{
		$data = array();
		$resp = $this->authorize->user_loggedIn();
		if($resp==false)
		{
			redirect(base_url().'home');
		}else{
			$data['login_status'] = 1;
			}
		
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		$show_add_money = $this->input->post('show_add_money');
		$eventType = $this->input->post('eventType');
		$isToken = $this->register_user->getTwitterToken();
		if($isToken)
		{
			$res1 = $isToken;
			$sp1 = explode('&',$res1);
			$final = explode('=',$sp1[0]);
			$final1 = explode('=',$sp1[1]);
			$oauth = array( 'oauth_consumer_key' => $this->key,
                'oauth_nonce' => time(),
                'oauth_signature_method' => 'HMAC-SHA1',
                'oauth_token' => $final[1],
                'oauth_timestamp' => time(),
                'oauth_version' => '1.0');
			$url = "https://api.twitter.com/1.1/account/verify_credentials.json";
			$base_info = $this->buildBaseString($url, 'GET', $oauth);
			$composite_key = rawurlencode($this->secret) . '&' . rawurlencode($final1[1]);
			$oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
			$oauth['oauth_signature'] = $oauth_signature;
			$header = array($this->buildAuthorizationHeader($oauth), 'Expect:');
			$options = array( CURLOPT_HTTPHEADER => $header,
							  CURLOPT_HEADER => false,
							  CURLOPT_URL => $url,
							  CURLOPT_RETURNTRANSFER => true,                 
							  CURLOPT_SSL_VERIFYPEER => false);

			$feed = curl_init();
			curl_setopt_array($feed, $options);
			$json = curl_exec($feed);
			curl_close($feed);
			//var_dump($json);
			$twitter_data = json_decode($json,true);
			$newdata['followers_count'] = $twitter_data['followers_count'];
		    $newdata['friends_count']   = $twitter_data['friends_count'];
		    $newdata['statuses_count']  = $twitter_data['statuses_count'];
		    $this->session->set_userdata('twitterconnectsess',$newdata);
		}
		if($show_add_money==1)
		{
			$data['show_add_money']=1;
		}
		else
		{
			$data['show_add_money']=0;
		}
		if($eventType =='soccer')
		{
			
			$data['colortheme'] = "#B41414";
			$data['logo'] = "football_logo.png";
			$data['className'] = "football dark_red";
			$data['style'] = 'soccer.css';
		}
		else if($eventType =='cricket')
		{
			$data['colortheme'] = "#2888C9";
			$data['logo'] = "cricket_logo.png";
			$data['className'] = "cricket blue";
			$data['style'] = 'cricket.css';
		}
		
		else if($eventType =='basketball')
		{
			$data['colortheme'] = "#2888C9";
			$data['logo'] = "coach_logo.png";
			$data['className'] = "sport blue";
			$data['style'] = 'basketball.css';
		}
		
		else if($eventType =='tennis-cup')
		{
			$data['colortheme'] = "#60D1CF;";
			$data['logo'] = "tennis_logo.png";
			$data['className'] = "sport tennis green";
			$data['style'] = 'tennis.css';
		}
		
		else if($eventType =='hockey-league')
		{
			$data['colortheme'] = "#2888C9";
			$data['logo'] = "ice_hockey_logo.png";
			$data['className'] = "ice_hockey blue";
			$data['style'] = 'hockey.css';
		}
		
		else if($eventType =='boxing-league')
		{
			$data['colortheme'] = "#ED281B";
			$data['logo'] = "boxing_logo.png";
			$data['className'] = "boxing red";
			$data['style'] = 'boxing.css';
		}
		
		else if($eventType =='rugby-league' || $eventType =='rugby-union')
		{
			$data['colortheme'] = "#FFC814";
			$data['logo'] = "rugby_logo.png";
			$data['className'] = "rugby orange";
			$data['style'] = 'rugby.css';
		}
		
		else if($eventType =='australian-rules')
		{
			$data['colortheme'] = "#ED281B";
			$data['logo'] = "amarican_rule_logo.png";
			$data['className'] = "amarican_rule red";
			$data['style'] = 'australian_rules.css';
		}
		
		else if($eventType =='baseball')
		{
			$data['colortheme'] = "#000000";
			$data['logo'] = "baseball_logo.png";
			$data['className'] = "baseball black";
			$data['style'] = "baseball.css";
		}
		
		else if($eventType =='martial-arts')
		{
			$data['colortheme'] = "#B41414";
			$data['logo'] = "martial_logo.png";
			$data['className'] = "martial dark_red";
			$data['style'] = 'martial_art.css';
		}
		else if($eventType =='golf')
		{
			$data['colortheme'] = "#000000";
			$data['logo'] = "golf_logo.png";
			$data['className'] = "golf black";
			$data['style'] = 'golf.css';
		}
		else if($eventType =='motor-sport')
		{
			$data['colortheme'] = "#FFC814";
			$data['logo'] = "motor_logo.png";
			$data['className'] = "motor_sport orange";
			$data['style'] = 'motor_sport.css';
		}
		else if($eventType =='american-football')
		{
			$data['colortheme'] = "#B41414";
		$data['logo'] = "amarican_football_logo.png";
		$data['className'] = "amarican_football dark_red";
		$data['style'] = 'american_football.css';
		}
		else if($eventType =='surfing')
		{
			$data['colortheme'] = "#0FB3EE";
			$data['logo'] = "surfing_logo.png";
			$data['className'] = "surfing sky";
			$data['style'] = 'surfing.css';
		}
		else
		{
			$data['colortheme'] = "#2888C9";
			$data['logo'] = "coach_logo.png";
			$data['style'] = 'basketball.css';
			$data['className'] = "sport blue";
		}
		
		$data['type_tip'] ='';
		$userId = $this->session->userdata('userid');
		$wallet_data = $this->wallet_model->getUserWallet($userId);
		$userData = $this->member_model->getMemberDetailsById($userId);
		$category = $this->category_model->getAllCategories();
		$data['wallet_data'] = $wallet_data;
		//$data['payKey'] = $pay_key;
		$data['userData'] = $userData;
		$data['categories'] = $category;
		//echo '<pre> ';print_r($userData);echo "###";
		//echo '<pre> ';print_r($wallet_data);die;
		$data['page_body'] = $this->load->view("mywallet",$data,true);	
		$this->load->view("templates_main",$data);	
	}
	function myplacedtip()
	{
		$event_type_id = $this->uri->segment(2,0);
		$data = array();
		$resp = $this->authorize->user_loggedIn();
		if($resp==false)
		{
			redirect(base_url().'home');
		}
		else{
			$data['login_status'] = 1;
			}
		$userId = $this->session->userdata('userid');
		$data['user_id'] = $userId;
		if($resp==false)
		{
			redirect(base_url().'home');
		}
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		$data['type_tip'] ='';
		$isToken = $this->register_user->getTwitterToken();
		if($isToken)
		{
			$res1 = $isToken;
			$sp1 = explode('&',$res1);
			$final = explode('=',$sp1[0]);
			$final1 = explode('=',$sp1[1]);
			$oauth = array( 'oauth_consumer_key' => $this->key,
                'oauth_nonce' => time(),
                'oauth_signature_method' => 'HMAC-SHA1',
                'oauth_token' => $final[1],
                'oauth_timestamp' => time(),
                'oauth_version' => '1.0');
			$url = "https://api.twitter.com/1.1/account/verify_credentials.json";
			$base_info = $this->buildBaseString($url, 'GET', $oauth);
			$composite_key = rawurlencode($this->secret) . '&' . rawurlencode($final1[1]);
			$oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
			$oauth['oauth_signature'] = $oauth_signature;
			$header = array($this->buildAuthorizationHeader($oauth), 'Expect:');
			$options = array( CURLOPT_HTTPHEADER => $header,
							  CURLOPT_HEADER => false,
							  CURLOPT_URL => $url,
							  CURLOPT_RETURNTRANSFER => true,                 
							  CURLOPT_SSL_VERIFYPEER => false);

			$feed = curl_init();
			curl_setopt_array($feed, $options);
			$json = curl_exec($feed);
			curl_close($feed);
			//var_dump($json);
			$twitter_data = json_decode($json,true);
			$newdata['followers_count'] = $twitter_data['followers_count'];
		    $newdata['friends_count']   = $twitter_data['friends_count'];
		    $newdata['statuses_count']  = $twitter_data['statuses_count'];
		    $this->session->set_userdata('twitterconnectsess',$newdata);
		}
		$data['colortheme'] = "#2888C9";
		$userId = $this->session->userdata('userid');
		$wallet_data = $this->wallet_model->getUserWallet($userId);
		$userData = $this->member_model->getMemberDetailsById($userId);
		$category = $this->category_model->getAllCategories();
		$data['wallet_data'] = $wallet_data;
		$data['event_type_id'] = $event_type_id;
		$data['userData'] = $userData;
		$data['categories'] = $category;
		$data['events'] = $this->betfair_model->getEventsByUserIdAndEventType($userId,$event_type_id);
		$data['eventsData'] = $this->betfair_model->getUserPlacedTipsEventsId($userId);
		if($event_type_id)
		{
			$data['page_body'] = $this->load->view("mytip_new",$data,true);	
		}
		else
		{
			$data['page_body'] = $this->load->view("mytipevents",$data,true);		
		}
//echo "<pre>";print_r($data11);die;
		$this->load->view("templates_main",$data);	
	}
	function mypurchasedtip()
	{
		$tipsterId = $this->uri->segment(2,0);
		$status = $this->uri->segment(3,0);
		$data = array();
		$resp = $this->authorize->user_loggedIn();
		$userId = $this->session->userdata('userid');
		if($resp==false)
		{
			redirect(base_url().'home');
		}
		else{
			$data['login_status'] = 1;
			}
		
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		$data['type_tip'] ='';
		$isToken = $this->register_user->getTwitterToken();
		if($isToken)
		{
			$res1 = $isToken;
			$sp1 = explode('&',$res1);
			$final = explode('=',$sp1[0]);
			$final1 = explode('=',$sp1[1]);
			$oauth = array( 'oauth_consumer_key' => $this->key,
                'oauth_nonce' => time(),
                'oauth_signature_method' => 'HMAC-SHA1',
                'oauth_token' => $final[1],
                'oauth_timestamp' => time(),
                'oauth_version' => '1.0');
			$url = "https://api.twitter.com/1.1/account/verify_credentials.json";
			$base_info = $this->buildBaseString($url, 'GET', $oauth);
			$composite_key = rawurlencode($this->secret) . '&' . rawurlencode($final1[1]);
			$oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
			$oauth['oauth_signature'] = $oauth_signature;
			$header = array($this->buildAuthorizationHeader($oauth), 'Expect:');
			$options = array( CURLOPT_HTTPHEADER => $header,
							  CURLOPT_HEADER => false,
							  CURLOPT_URL => $url,
							  CURLOPT_RETURNTRANSFER => true,                 
							  CURLOPT_SSL_VERIFYPEER => false);

			$feed = curl_init();
			curl_setopt_array($feed, $options);
			$json = curl_exec($feed);
			curl_close($feed);
			//var_dump($json);
			$twitter_data = json_decode($json,true);
			$newdata['followers_count'] = $twitter_data['followers_count'];
		    $newdata['friends_count']   = $twitter_data['friends_count'];
		    $newdata['statuses_count']  = $twitter_data['statuses_count'];
		    $this->session->set_userdata('twitterconnectsess',$newdata);
		}
		$userId = $this->session->userdata('userid');
		$data['userId'] = $this->session->userdata('userid');
		$wallet_data = $this->wallet_model->getUserWallet($userId);
		$userData = $this->member_model->getMemberDetailsById($userId);
		$category = $this->category_model->getAllCategories();
		$data['wallet_data'] = $wallet_data;
		$data['tipsterId'] = $tipsterId;
		$data['userData'] = $userData;
		$data['categories'] = $category;
		$data['colortheme'] = "#2888C9";
		$data['tipsterData'] = $this->betfair_model->getUserPurchasedTispstersId($userId);
		if($tipsterId)
		{
			$userData = $this->member_model->getMemberDetailsById($tipsterId);
			$data['tipesterData'] = $userData;
			if($status=='current') {
				$data['status'] = 'current';
				$data['tip_type'] = 'MY CURRENT PURCHASED TIPS';
				$data['tipsterData'] = $this->betfair_model->listSportByPurchased($userId,$tipsterId,'current');
				}
			else if($status=='past')
			{
				$data['status'] = 'past';
				$data['tip_type'] = 'MY PAST PURCHASED TIPS';
				$data['tipsterData'] = $this->betfair_model->listSportByPurchased($userId,$tipsterId,'past');
				//echo '<pre>';print_r($data['tipsterData']);die;
				//$data['events'] = $this->betfair_model->listPastPurchasedTip($userId,$tipsterId);
			}
			else
			{
				$data['status'] = 'future';
				$data['tip_type'] = 'MY Future TIPS';
				$data['tipsterData'] = $this->betfair_model->listSportByPurchased($userId,$tipsterId,'future');
			}
			$data['page_body'] = $this->load->view("mypurchasedtipsports",$data,true);	
		}
		else
		{
			$data['page_body'] = $this->load->view("mypurchasedtipuser",$data,true);	
		}
		//echo "<pre>";print_r($data11);die;
		$this->load->view("templates_main",$data);	
	}
	
	function user_data()
	{
		$resp = $this->authorize->user_loggedIn();
		if($resp===true)
		{
			//session_start();session_destroy();die;
			redirect(base_url().'select');
		}
		$this->load->config();
		$userId = $this->uri->segment(2,0);
		$eventTypeId = $this->uri->segment(3,0);
		$events = $res = $this->betfair_model->getUserAllPlacedTipsEventType($userId,$eventTypeId);
		/*$won=0;
		$loose=0;
		$total = count($events);*/
		$status='';
		foreach($events as $event)
		{
			if($event['placed']==$event['result'])
			{
				$status .='W';
				//$won++;
			}
			else
			{
				$status .='L';
				//$loose++;
			}
		}
		/*if($total)
		{
			$strikeRate = round(($won/$total)*100,2);
		}
		else
		{
			$strikeRate=0;
		}*/
		$userStats = $this->betfair_model->getUserStatisticsForProfile($userId,$eventTypeId);
		//echo '<pre>';print_r($userStats);die;
		if(count($userStats))
		{
			$stRate = $userStats['strike_rate'];
			$ave_odds = $userStats['ave_odds'];
			$rank = $userStats['user_rank'];
		}
		else
		{
			$stRate=0;
			$ave_odds=0;
			$rank = 0;
		}
		$data['total_odds'] = $this->betfair_model->getUserOddsBets($userId,$eventTypeId);
		$data['status'] = substr($status,0,5);
		$data['strikeRate'] = $stRate;
		$data['ave_odds'] = round($ave_odds,2);
		$data['rank'] = $rank;
		$data['memberDetails'] = $this->member_model->getMemberDetailsById($userId);
		$page = $this->load->view('sections/user_data',$data,true);
		echo $page;
	}
	
	
	function tips_results()
	{
		$data = array();
		$event_type_id = $this->uri->segment(2,0);
		$userId = $this->uri->segment(3,0);
		$data['events'] = $this->betfair_model->getUserPlacedTips($event_type_id,$userId);
		echo $this->load->view('sections/tips_results',$data,true);
		die;
		//echo $page;
	}
	function user_profile_information()
	{
		$data = array();
		$event_type_id = $this->uri->segment(2,0);
		$userId = $this->uri->segment(3,0);
		$userData = $this->member_model->getMemberDetailsById($userId);
		$userStats = $this->betfair_model->getUserStatisticsForProfile($userId,$event_type_id);
		$totalWonloss = $this->betfair_model->getTotalWonloss($userId,$event_type_id);
		$resBetOdds = $this->betfair_model->getUserOddsBets($userId,$event_type_id);
		$data['tipesterData'] = $userData;
		$data['userStats'] = $userStats;
		$data['totalWonloss'] = $totalWonloss;
		$data['resBetOdds'] = $resBetOdds;
		$data['userData'] = $userData;
		echo $this->load->view('profile_info',$data,true);
		die;
		//echo $page;
	}
	function league_data()
	{
		$data = array();
		$event_type_id = $this->uri->segment(2);
		$data['competitions'] = $this->betfair_model->getEventTypesCompetitions($event_type_id);
		$page = $this->load->view('sections/league_data',$data,true);
		echo $page;
	}
	function league_data_matches()
	{
		$data = array();
		$competition_id = $this->uri->segment(2,0);
		$data['competition_data']= $this->betfair_model->getCompetitionById($competition_id);
		$data['events'] = $this->betfair_model->getEvents($competition_id);
		$page = $this->load->view('sections/league_data_matches',$data,true);
		echo $page;
	}
	function purchasetip_data()
	{
		$data = array();
		$event_type_id = $this->uri->segment(2);
		$user_id = $this->uri->segment(3);
		
		$event_competitons = $this->betfair_model->getEventsCompetition($event_type_id);
		$events = $this->betfair_model->listuserBetPlacedEvents(31,$user_id);
		$data['events'] = $events;
		$data['user_id'] = $user_id;
		$page = $this->load->view('sections/purchase_data_matches',$data,true);
		echo $page;
	}
	
	
	function view_page()
	{
		//for// acknowledgement faq for-artists privacy terms
	}
	function buildBaseString($baseURI, $method, $params)
	{
		$r = array(); 
		ksort($params); 
		foreach($params as $key=>$value){
			$r[] = "$key=" . rawurlencode($value); 
		}            

		return $method."&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r)); //return complete base string
	}
	function buildAuthorizationHeader($params)
	{
		$r = 'Authorization: OAuth ';
		$values = array();
		foreach($params as $key=>$value)
			$values[] = "$key=\"" . rawurlencode($value) . "\""; 

		$r .= implode(', ', $values); 
		return $r; 
	}
	function addMoney()
	{
		
		$amount = $this->input->post('addamount');
		$currency = $this->input->post('currency');
		$res = $this->paypal->getPayKey($amount,$currency);
		echo $res;
		
	}
	function withdrawMoney()
	{
		$amount = $this->input->post('withdrawamount');
		$currencywithdraw = $this->input->post('currencywithdraw');
		$email = $this->input->post('paypalemail');
		$userid = $this->session->userdata('userid');
		$wallet_data = $this->wallet_model->getUserWallet($userid);
		if($amount > $wallet_data['balance'])
		{
			$this->session->set_flashdata('error',"Amount to be withdrawn is not valid.");
			redirect(base_url().'mywallet/');
		}
		$res = $this->paypal->getWithdrawviaMassPay($email,$amount);
		
		if($res['ACK']=='Failure')
		{
			$this->session->unset_userdata('unique_value');
			$this->session->set_flashdata('error',urldecode($res["L_LONGMESSAGE0"]));
		}
		if($res['ACK']=='Success')
		{
			$this->wallet_model->withdrawFromWallet($amount,$email);
			$this->session->set_flashdata('success',"Money withdrawl successfully.");
		}
		redirect(base_url().'mywallet/');
	}
	function walletAddMoney()
	{
		session_start();
		if(isset($_SESSION['amount']))
		{
			$amount = $_SESSION['amount'];
		}
		if(isset($_SESSION['paykey']))
		{
			$paykey = $_SESSION['paykey'];
		}
		$emailpayment = $this->session->userdata('creditpayment');
		if(isset($emailpayment))
		{
			redirect(base_url()."mywallet");
		}
		
	}
	function walletAddMoneyCancel()
	{
	$this->session->set_flashdata('error',"Some error occured while processing your account");
		redirect(base_url()."mywallet");
	}
	function getPaypalDgflow()
	{
		$data['payKey'] = $this->uri->segment(2);
		$this->load->view('iframePaypal',$data);
	}
	function save_place_tip()
	{
		$data = array();
		$resp = $this->betfair_model->savePlacedTipFreeSell();
		$userid = $this->session->userdata('userid');
		if($resp === true)
		{
			/* $data = array();
			$this->load->library('email');
			$userid = $this->session->userdata('userid');
			$eventName = $this->input->post('eventName');
			$userInfo = $this->betfair_model->getUserInfo($userid);
			$email = $userInfo['email'];
			$to = $email;
			$email_template_id = 2;
			$first_name = $userInfo['first_name'];
			$last_name = $userInfo['last_name'];
			$email_content = $this->getEmailTemplate($email_template_id);
			$message = $email_content[0]['body'];
			$search = array('{FIRSTNAME}','{LASTNAME}','{EVENTNAME}','{SITELINK}');
			$replace= array($first_name,$last_name,$eventName,'www.whispertip.com');
			$body=str_replace($search,$replace,$message);
			$data['template_body'] = $body;
			$page = $this->load->view('email_template',$data,true);
			$config['charset'] = 'iso-8859-1';
			$config['mailtype'] = 'html';
			$subject = $email_content[0]['subject'];
			$from = $email_content[0]['from'];
			$this->email->initialize($config);
			$this->email->from($from);
			$this->email->to($to);
			$this->email->subject($subject);
			$this->email->message($page);
			$this->email->send();
			*/
			echo json_encode('success');
		}
		else
		{
			echo json_encode('error');
		}
	}
	function check_place_tip()
	{
		$data = array();
		$userid = $this->session->userdata('userid');
		$resp = $this->betfair_model->checkPlacedTips();
		if($resp === true)
		{
			echo json_encode('success');
		}
		else
		{
			echo json_encode('error');
		}
	}
	function save_purchase_tip()
	{
		$data = array();
		$place_id = $this->input->post('place_tip_id');
		$this->session->set_userdata('placed_tip_id',$place_id);
		$resp = $this->betfair_model->savePurchaseTip($place_id);
		$tipsterId = $this->input->post('tipsterId');
		if($resp === true)
		{
			/*$this->load->library('email');
			$userid = $this->session->userdata('userid');
			$tipsterId = $this->input->post('tipsterId');
			$event_id = $this->input->post('event_id');
			$userInfo = $this->betfair_model->getUserInfo($userid);
			$tipester_detail = $this->betfair_model->getUserInfo($tipsterId);
			$Event_detail = $this->betfair_model->getEventById($event_id);
			$tipester_name = $tipester_detail['first_name'].' '.$tipester_detail['last_name'];
			$email = $tipester_detail['email'];
			$to = $email;
			$email_template_id = 3;
			$first_name_tipster = $tipester_detail['first_name'];
			$last_name_tipster = $tipester_detail['last_name'];
			$first_name_user = $userInfo['first_name'];
			$last_name_user = $userInfo['last_name'];
			$userName = $first_name_user.' '.$last_name_user;
			$tipsterName = $first_name_tipster.' '.$last_name_tipster;
			$email_content = $this->getEmailTemplate($email_template_id);
			$message = $email_content[0]['body'];
			$search = array('{tipstername}','{usermame}','{eventname}','{SITELINK}');
			$replace= array($tipsterName,$userName,$Event_detail['name'],'http://www.whispertip/');
			$body=str_replace($search,$replace,$message);	
			$data['template_body'] = $body;
			$page = $this->load->view('email_template',$data,true);
			$config['charset'] = 'iso-8859-1';
			$config['mailtype'] = 'html';
			$subject = $email_content[0]['subject'];
			$from = $email_content[0]['from'];
			$this->email->initialize($config);
			$this->email->from($from);
			$this->email->to($to);
			$this->email->subject($subject);
			$this->email->message($page);
			$this->email->send();
			*/
			echo json_encode('success');
			
		}
		else
		{
			echo json_encode('error');
		}
	}
	function savePurchaseFree()
	{
		$data = array();
		$place_id = $this->input->post('place_tip_id');
		$this->session->set_userdata('placed_tip_id',$place_id);
		$resp = $this->betfair_model->savePurchaseTipFree($place_id);
		$tipsterId = $this->input->post('tipsterId');
		if($resp === true)
		{
			echo json_encode('success');
			
		}
		else
		{
			echo json_encode('error');
		}
	}
	function test()
	{
		//$resp = $this->betfair_model->getEventTypeEvents(1);
		$resp = $this->betfair_model->getUserPlacedTips(1,5);
		echo "<pre>";
		print_r($resp);
	}
	function league_data_byUserID()
	{
		$data = array();
		$data['competitions'] = $this->betfair_model->getEventTypesCompetitions(1);
		$page = $this->load->view('sections/league_data',$data,true);
		echo $page;
	}
	
	function check_balance()
	{
		$userId = $this->session->userdata('userid');
		$wallet_data = $this->wallet_model->getUserWallet($userId);
		echo json_encode($wallet_data);
	}
	
	function getUserTipsters()
	{
		$userId = $this->session->userdata('userid');
		$tipsterData = $this->betfair_model->getUserPurchasedTispstersId($userId);
		
		//echo "<pre>";
		//print_r($tipsterData);
		//die("here");
		echo json_encode($tipsterData);
	}
	
	function getUserEventsTips()
	{
		$userId = $this->session->userdata('userid');
		$eventsData = $this->betfair_model->getUserPlacedTipsEventsId($userId);
		echo json_encode($eventsData);
	}
	
	function league_data_match_selected()
	{
		$data = array();
		$event_id = $this->uri->segment(2,0);
		$market_id = $this->uri->segment(3,0);
		$competition_id = $this->uri->segment(4,0);
		$data['event'] = $this->betfair_model->getEventById($event_id);
		$data['marketOdds'] = $this->betfair_model->getEventsMarkets($event_id);
		$page = $this->load->view('sections/league_data_match_selected',$data,true);
		echo $page;
	}
	function league_data_match_odds()
	{
		$data = array();
		$event_id = $this->uri->segment(2,0);
		$market_id = $this->uri->segment(3,0);
		$competition_id = $this->uri->segment(4,0);
		$data['event'] = $this->betfair_model->getEventById($event_id);
		$data['marketOdds'] = $this->betfair_model->getEventsMarkets($event_id);
		$page = $this->load->view('sections/league_data_match_odds',$data,true);
		echo $page;
	}
	function league_data_match_odds_new()
	{
		$data = array();
		$event_id = $this->uri->segment(2,0);
		$market_id = $this->uri->segment(3,0);
		$competition_id = $this->uri->segment(4,0);
		$data['event'] = $this->betfair_model->getEventById($event_id);
		//$data['marketOdds'] = $this->betfair_model->getEventsMarkets($event_id);
		$data['marketDetail'] = $this->betfair_model->getEventsMarketsByMarketId($market_id);
		$data['marketOdds'] = $this->betfair_model->getMarketOddsByMarketId($market_id);
		$page = $this->load->view('sections/league_data_match_odds_new',$data,true);
		echo $page;
	}
	function league_data_match_odds_API()
	{
		$data = array();
		session_start();
		$event_id = $this->uri->segment(2,0);
		$data['event_id'] = $this->uri->segment(2,0);
		$market_id = $this->uri->segment(3,0);
		$data['market_id'] = $this->uri->segment(3,0);
		$data['competition_id'] = $this->uri->segment(4,0);
		$data['event_name_val'] = $this->uri->segment(5,0);
		$data['event'] = $this->betfair_model->getEventById($event_id);
		$data['Type'] = $this->betfair_model->getMarketType($market_id);
		$data['date'] =  $this->session->userdata('date');
		$data['event_type_id'] = $this->betfair_model->getEventTypeById($data['competition_id']);
		$data['event_name'] = $this->session->userdata('event_name');
		$page = $this->load->view('sections/league_data_match_odds_API',$data,true);
		echo $page;
	}
	function purchase_tip_tipster()
	{
		$data = array();
		$resp = $this->authorize->user_loggedIn();
		
		$userId = $this->input->post('hidUserId');
		$event_type_id = $this->input->post('hidEventTypeId');
		
		if($resp==false)
		{
			redirect(base_url().'home');
		}
		if(!$userId)
		{
			redirect(base_url().'home');
		}
		
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
	
		$userData = $this->member_model->getMemberDetailsById($userId);
		
		$isToken = $this->register_user->getTwitterToken();
		if($isToken)
		{
			$res1 = $isToken;
			$sp1 = explode('&',$res1);
			$final = explode('=',$sp1[0]);
			$final1 = explode('=',$sp1[1]);
			$oauth = array( 'oauth_consumer_key' => $this->key,
                'oauth_nonce' => time(),
                'oauth_signature_method' => 'HMAC-SHA1',
                'oauth_token' => $final[1],
                'oauth_timestamp' => time(),
                'oauth_version' => '1.0');
			$url = "https://api.twitter.com/1.1/account/verify_credentials.json";
			$base_info = $this->buildBaseString($url, 'GET', $oauth);
			$composite_key = rawurlencode($this->secret) . '&' . rawurlencode($final1[1]);
			$oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
			$oauth['oauth_signature'] = $oauth_signature;
			$header = array($this->buildAuthorizationHeader($oauth), 'Expect:');
			$options = array( CURLOPT_HTTPHEADER => $header,
							  CURLOPT_HEADER => false,
							  CURLOPT_URL => $url,
							  CURLOPT_RETURNTRANSFER => true,                 
							  CURLOPT_SSL_VERIFYPEER => false);

			$feed = curl_init();
			curl_setopt_array($feed, $options);
			$json = curl_exec($feed);
			curl_close($feed);
			//var_dump($json);
			$twitter_data = json_decode($json,true);
			$newdata['followers_count'] = $twitter_data['followers_count'];
		    $newdata['friends_count']   = $twitter_data['friends_count'];
		    $newdata['statuses_count']  = $twitter_data['statuses_count'];
		    $this->session->set_userdata('twitterconnectsess',$newdata);
		}	
		$userStats = $this->betfair_model->getUserStatistics($userId);
		if(count($userStats))
		{
			$stRate = $userStats['strike_rate'];
			$ave_odds = $userStats['ave_odds'];
			$rank = $userStats['rank'];
		}
		else
		{
			$stRate=0;
			$ave_odds=0;
			$rank = 0;
		}
		
		$data['strikeRate'] = $stRate;
		$data['rank'] = $rank;
		$data['ave_odds'] = $ave_odds;
		$category = $this->category_model->getAllCategories();
		$data['userId'] = $userId;
		$data['event_type_id'] = $event_type_id;
		$data['userData'] = $userData;
		$data['categories'] = $category;
		$data['page_body'] = $this->load->view("purchase_tip_user",$data,true);	
		$this->load->view("templates_main",$data);	
	}
	
	function user_rankings()
	{
		$userId = $this->uri->segment(3,0);
		$event_type_id = $this->uri->segment(2,0);
		$data['response'] = $this->betfair_model->getUserStatisticsForProfile($userId,$event_type_id);
		//echo "<pre>";
		//print_r($data['response']);
		$result = $this->load->view("sections/user_rankings",$data,true);
		echo $result;
	}
	function get_curl_data()
	{
		$url = $this->input->post('myurl');
		$eventId = $this->input->post('eventId');
		$competitionId = $this->input->post('competitionId');
		$this->load->library('authorize');
		$timeZone = $this->authorize->getMenuLinks($url,$competitionId,$eventId);
		echo $timeZone;
	}

	function show_match_odds()
	{
		$data = array();
		$this->load->library('authorize');
		$resp = $this->authorize->user_loggedIn();
		$userId = $this->session->userdata('userid');
		if($resp==false)
		{
			redirect(base_url().'home');
		}
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		
		$isToken = $this->register_user->getTwitterToken();
		if($isToken)
		{
			$res1 = $isToken;
			$sp1 = explode('&',$res1);
			$final = explode('=',$sp1[0]);
			$final1 = explode('=',$sp1[1]);
			$oauth = array( 'oauth_consumer_key' => $this->key,
                'oauth_nonce' => time(),
                'oauth_signature_method' => 'HMAC-SHA1',
                'oauth_token' => $final[1],
                'oauth_timestamp' => time(),
                'oauth_version' => '1.0');
			$url = "https://api.twitter.com/1.1/account/verify_credentials.json";
			$base_info = $this->buildBaseString($url, 'GET', $oauth);
			$composite_key = rawurlencode($this->secret) . '&' . rawurlencode($final1[1]);
			$oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
			$oauth['oauth_signature'] = $oauth_signature;
			$header = array($this->buildAuthorizationHeader($oauth), 'Expect:');
			$options = array( CURLOPT_HTTPHEADER => $header,
							  CURLOPT_HEADER => false,
							  CURLOPT_URL => $url,
							  CURLOPT_RETURNTRANSFER => true,                 
							  CURLOPT_SSL_VERIFYPEER => false);

			$feed = curl_init();
			curl_setopt_array($feed, $options);
			$json = curl_exec($feed);
			curl_close($feed);
			//var_dump($json);
			$twitter_data = json_decode($json,true);
			$newdata['followers_count'] = $twitter_data['followers_count'];
		    $newdata['friends_count']   = $twitter_data['friends_count'];
		    $newdata['statuses_count']  = $twitter_data['statuses_count'];
		    $this->session->set_userdata('twitterconnectsess',$newdata);
		}	
		$userId = $this->session->userdata('userid');
		$wallet_data = $this->wallet_model->getUserWallet($userId);
		$userData = $this->member_model->getMemberDetailsById($userId);
		$category = $this->category_model->getAllCategories();
		$data['wallet_data'] = $wallet_data;
		$data['userData'] = $userData;
		$data['categories'] = $category;
		$data['userId'] = $userId;
		$market_id = $this->uri->segment(2,0);
		$data['selection_id'] = $this->uri->segment(3,0);
		$data['market_id'] = $market_id;
		$MarktData = $this->betfair_model->getMarketType($market_id);
	//	echo '<pre>';print_r($MarktData);die;
		$data['Type'] = $MarktData['market_type'];
		$data['event_id'] = $MarktData['event_id'];
		$data['Type'] = $this->betfair_model->getMarketType($market_id);
		$data['odd']= $this->betfair_model->getOddsByMarketId($market_id);
		$data['left_section'] = $this->load->view("sections/left_section",$data,true);	
		$data['page_body'] = $this->load->view("match_odd_data",$data,true);		
		$this->load->view("templates_main",$data);
	}
	
	function get_curl_data_db()
	{	
		$dumm = 0;
		$this->load->library('authorize');
		$values = $this->input->post('val');
		/*if($values =='soccer')
		{
			$eventTypeid = 1;
		}
		else
		{
			$eventTypeid = 6;
		}
		*/
		$eventTypeid = $this->input->post('event_type_id');
		$cc = 0;
		$aa = 0;
		$val =2;
		$url = $this->input->post('myurl');
		$eventId = $this->input->post('eventId');
		$competitionId = $this->input->post('competitionId');
		$comp = $this->betfair_model->getAllCompById($eventTypeid);
		//echo "<pre>";print_r($comp);die;
		$timeZone = $this->authorize->getMenuLinksDB($comp,$cc,$dumm,$aa,$val);
		echo $timeZone;
	}
	function getEventByComp()
	{
		$this->load->library('authorize');
		$dumm = 0;
		$cc = 0;
		$comp_id = $this->input->post('id');
		$parent_id = $this->input->post('parent_id');
		$nav_id = $this->input->post('nav_id');
		$url = $this->input->post('myurl');
		$type = $this->input->post('type');
		$SessArr = array(
		'clas_name' =>'list_competetion',
		'comp_id' =>$comp_id,
		'navigation_id'=>$nav_id,
		'parent_id'=>$parent_id,
		'type'=>$type
		);
		$this->session->set_userdata('event_sess', $SessArr);
		$this->session->set_userdata('state', 1);
		if($comp_id!=0 && $type==''){
			$val = 1;
		$event = $this->betfair_model->getEvents($comp_id,$nav_id);
		}
		else if($comp_id!=0 && $type=='comp')
		{ 
			$val = 2;
			$event = $this->betfair_model->getEventsBynavComp($comp_id,$nav_id);
		}
		else 
		{
			$val = 3;
			$event = $this->betfair_model->getEventsBynav($comp_id,$nav_id);
		}
		$userdata = $this->session->userdata('timezone');
		$timeZone = $this->authorize->getMenuLinksDB($cc,$event,$dumm,$userdata,$val);
		echo $timeZone;
	}
	function getEventByCompNav()
	{
		$this->load->library('authorize');
		$dumm = 0;
		$cc = 0;
		$comp_id = $this->input->post('id');
		$parent_id = $this->input->post('parent_id');
		$nav_id = $this->input->post('nav_id');
		$url = $this->input->post('myurl');
		$type = $this->input->post('type');
		$val = 4;
		$event = $this->betfair_model->getEvents($comp_id,$nav_id);
		$userdata = $this->session->userdata('timezone');
		$timeZone = $this->authorize->getMenuLinksDB($cc,$event,$dumm,$userdata,$val);
		echo $timeZone;
	}
	function getCompNav()
	{
		$this->load->library('authorize');
		$dumm = 0;
		$cc = 0;
		$comp_id = $this->input->post('id');
		$parent_id = $this->input->post('parent_id');
		$nav_id = $this->input->post('nav_id');
		$url = $this->input->post('myurl');
		$type = $this->input->post('type');
		$val = 5;
		$SessArr = array(
		'clas_name' =>'list_eventByNav',
		'comp_id' =>$comp_id,
		'navigation_id'=>$nav_id,
		'parent_id'=>$parent_id,
		'type'=>$type
		);
		$this->session->set_userdata('comp_sess_nav', $SessArr);
		$this->session->set_userdata('state', 4);
		$event = $this->betfair_model->getEventsBynavComp($comp_id,$nav_id);
		//echo '<pre>';print_r($event);die;
		$userdata = $this->session->userdata('timezone');
		$timeZone = $this->authorize->getMenuLinksDB($cc,$event,$dumm,$userdata,$val);
		echo $timeZone;
	}
	function getMarketByComp()
	{
		$this->load->library('authorize');
		$dumm = 0;
		$cc = 0;
		$aa = 0;
		$comp_id = $this->input->post('comp_id');
		$event_id = $this->input->post('event_id');
		$date = $this->input->post('date');
		$event_name = $this->input->post('event_name');
		$this->session->set_userdata('date', $date);
		$this->session->set_userdata('event_name', $event_name);
		$SessArr = array(
		'class_name' =>'getmarkets',
		'comp_id' =>$comp_id,
		'date'=>$date,
		'event_id'=>$event_id,
		'event_name'=>$event_name,
		);
		$this->session->set_userdata('market_sess', $SessArr);
		$this->session->set_userdata('state', 2);
		$val=2;
		$market = $this->betfair_model->getEventsMarkets($comp_id,$event_id);
		$timeZone = $this->authorize->getMenuLinksDB($dumm,$cc,$market,$aa,$val);
		echo $timeZone;
	}
	
	
	function paymentsuccess()
	{
		print '<script>
		opener.close_window("'.base_url().'");
		myWindow.close();
		</script>';
	}
	function cancelpayment()
	{
		print '<script>
		opener.close_windowCancel("'.base_url().'");
		myWindow.close();
		</script>';
	}
	function place_tips()
	{ 
		$data = array();
		$userId = $this->session->userdata('userid');
		$data['userId'] = $userId;
		$resp = $this->authorize->user_loggedIn();
		if($resp==false)
		{
			$data['login_status'] = 0;
		}else{
			$data['login_status'] = 1;
			}
		$event_type = $this->uri->segment(2,0);
		$data['event_type'] = $event_type;
		$data['userData'] = $this->member_model->getMemberDetailsById($userId);
		if($event_type)
		{
			$data['category'] = $this->category_model->getAllCategories();
			$eventType = $event_type;
		}
		else
		{
			$data['category'] = $this->category_model->getAllCategories();
			$eventType = $category[0]['slugUrl'];
		}
		$data['eventType'] = $eventType;
		$event_type_id = $this->betfair_model->getCompetitionId($eventType);
		$data['event_type_id'] = $event_type_id ;
		$this->load->library('pagination');
		$data['comp'] = $this->betfair_model->getAllCompById($event_type_id);
		$data['league'] = $this->betfair_model->getLeagueData($event_type_id);
		$this->load->library('pagination');
		$data['hostPath'] =$this->config->item('path');
		$config['base_url'] = base_url().'home/place-tips';
		$config['total_rows'] = count($data['league']);
		$config['per_page'] = 10;
		$config['uri_segment'] = 4;
		$config['ajax'] = true;
		$this->pagination->initialize($config); 
		$data['page_number'] = $this->uri->segment(4,0);		
		$data['page_size'] = 10;
		$data['league'] = $this->betfair_model->getLeagueData($event_type_id,$data['page_number'], $data['page_size']);
		
		
		if($event_type =='soccer')
		{
			$data['colortheme'] = "#B41414";
			$data['page_body'] = $this->load->view("place_tip_soccer_latest",$data,true);
		}
		else if($event_type =='cricket')
		{
			$data['colortheme'] = "#2888C9";
			$data['page_body'] = $this->load->view("place_tip_cricket",$data,true);
		}
		else if($event_type =='boxing-league')
		{
			$data['colortheme'] = "#ED281B";
			$data['page_body'] = $this->load->view("place_tip_boxing",$data,true);
		}
		else if($event_type =='basketball')
		{
			$data['colortheme'] = "#2888C9";
			$data['page_body'] = $this->load->view("place_tip_basketball",$data,true);
		}
		
		else if($event_type =='tennis-cup')
		{
			$data['colortheme'] = "#60D1CF";
			$data['page_body'] = $this->load->view("place_tip_tennis",$data,true);
		}
		
		else if($event_type =='hockey-league')
		{
			$data['colortheme'] = "#2888C9";
			$data['page_body'] = $this->load->view("place_tip_hockey",$data,true);
		}
		
		else if($event_type =='rugby-league')
		{
			$data['colortheme'] = "#FFC814";
			$data['page_body'] = $this->load->view("place_tip_rugby_league",$data,true);
		}
		
		else if($event_type =='rugby-union')
		{   
			$data['colortheme'] = "#FFC814";
			$data['page_body'] = $this->load->view("place_tip_rugby_union",$data,true);
		}
		
		else if($event_type =='australian-rules')
		{
			$data['colortheme'] = "#ED281B";
			$data['page_body'] = $this->load->view("place_tip_austrialian",$data,true);
		}
		
		else if($event_type =='baseball')
		{
			$data['colortheme'] = "#000000";
			$data['page_body'] = $this->load->view("place_tip_baseball",$data,true);
		}
		
		else if($event_type =='martial-arts')
		{
			$data['colortheme'] = "#B41414";
			$data['page_body'] = $this->load->view("place_tip_martial",$data,true);
		}
		else if($event_type =='golf')
		{
			$data['colortheme'] = "#000000";
			$data['page_body'] = $this->load->view("place_tip_golf",$data,true);
		}
		else if($event_type =='motor-sport')
		{
			$data['colortheme'] = "#FFC814";
			$data['page_body'] = $this->load->view("place_tip_motor",$data,true);
		}
		else if($event_type =='american-football')
		{
			$data['colortheme'] = "#B41414";
			$data['page_body'] = $this->load->view("place_tip_american_football",$data,true);
		}
		else if($event_type =='surfing')
		{
			$data['colortheme'] = "#0FB3EE";
			$data['page_body'] = $this->load->view("place_tip_surfing",$data,true);
		}
		$this->load->view("templates_main",$data);	
			/* } */
		
	}
	function user_profile()
	{
		$data = array();
		$resp = $this->authorize->user_loggedIn();
		$usid = $this->session->userdata('userid');
		if($resp==false)
		{
			redirect(base_url().'home');
		}
		else{
			$data['login_status'] = 1;
			}
		
		if ($this->session->flashdata('success'))
		{
			$data['success'] = $this->session->flashdata('success');
		}		
		if ($this->session->flashdata('message'))
		{
			$data['message'] = $this->session->flashdata('message');
		}
		if ($this->session->flashdata('error'))
		{
			$data['error'] = $this->session->flashdata('error');
		}
		$data['type_tip'] ='';
		$data['colortheme'] = "#2888C9";
		$isToken = $this->register_user->getTwitterToken();
		if($isToken)
		{
			$res1 = $isToken;
			$sp1 = explode('&',$res1);
			$final = explode('=',$sp1[0]);
			$final1 = explode('=',$sp1[1]);
			$oauth = array( 'oauth_consumer_key' => $this->key,
                'oauth_nonce' => time(),
                'oauth_signature_method' => 'HMAC-SHA1',
                'oauth_token' => $final[1],
                'oauth_timestamp' => time(),
                'oauth_version' => '1.0');
			$url = "https://api.twitter.com/1.1/account/verify_credentials.json";
			$base_info = $this->buildBaseString($url, 'GET', $oauth);
			$composite_key = rawurlencode($this->secret) . '&' . rawurlencode($final1[1]);
			$oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
			$oauth['oauth_signature'] = $oauth_signature;
			$header = array($this->buildAuthorizationHeader($oauth), 'Expect:');
			$options = array( CURLOPT_HTTPHEADER => $header,
							  CURLOPT_HEADER => false,
							  CURLOPT_URL => $url,
							  CURLOPT_RETURNTRANSFER => true,                 
							  CURLOPT_SSL_VERIFYPEER => false);

			$feed = curl_init();
			curl_setopt_array($feed, $options);
			$json = curl_exec($feed);
			curl_close($feed);
			//var_dump($json);
			$twitter_data = json_decode($json,true);
			$newdata['followers_count'] = $twitter_data['followers_count'];
		    $newdata['friends_count']   = $twitter_data['friends_count'];
		    $newdata['statuses_count']  = $twitter_data['statuses_count'];
		    $this->session->set_userdata('twitterconnectsess',$newdata);
		}	
		if($usid)
		{
			$userId = $usid;
			$data['diff_user'] =1; 
			$userData = $this->member_model->getMemberDetailsById($usid);
		}
		else
		{
			$userId = $this->session->userdata('userid');
			$data['diff_user'] =0; 
			$userData = $this->member_model->getMemberDetailsById($userId);
		}
		$category = $this->category_model->getAllCategories();
		$userData = $this->member_model->getMemberDetailsById($userId);
		$data['userId'] = $userId;
		$data['userData'] = $userData;
		$data['categories'] = $category;
		$data['page_body'] = $this->load->view("user_profile",$data,true);	
		$this->load->view("templates_main",$data);	
	}
	function dopayment(){
		$this->load->library('DoDirectPaymentReceipt');
		$this->load->library('session');
		$userId = $this->session->userdata('userid');
		$userData = $this->member_model->getMemberDetailsById($userId);
		$result= $this->dodirectpaymentreceipt->doPayment($userData);
		//echo '<pre>';print_r($result);die;
		if($result && !isset($result['ACK'])){
			if($result['TRANSACTIONID']) {
				$type = 'credit card';
				$this->wallet_model->addInWallet($result['totalAmount'],$result['TRANSACTIONID'],$type);
				$this->session->set_flashdata('success',"Money add successfully into wallet.");
				return true;
			}
			else
			{
			$this->session->set_flashdata('error',"Some error occured while processing your account");
			return true;
			}
			//redirect(base_url()."mywallet");
		}
		else
		{
			$error_message = $result['L_LONGMESSAGE0'];
			$this->session->set_flashdata('error',$error_message);
			return true;
			//redirect(base_url()."mywallet");
			
		}
	}
	function delete_tip()
	{
		$place_id = $this->uri->segment(2,0);
		 $eventType_id = $this->uri->segment(3,0);
		if ($place_id)
			{
				if($this->betfair_model->delete_place_tip($place_id) == true)
				{
					$this->session->set_flashdata('success', $this->lang->line('admin_operation_success'));
					redirect(base_url().'myplacedtip/'.$eventType_id);
				}
			}
			
	}
	function get_suggestions()
	{
		$resp = array();
		$search = $this->input->get('q');
		if($search != '') {
			$resp = $this->betfair_model->getSuggestionText($search);
		}
		//print json_encode($resp);
	}
	function get_event_market($competition_id,$event_type_id,$event_id,$event_date,$event_name)
	{
		$this->load->library('authorize');
		$dumm = 0;
		$cc = 0;
		$aa = 0;
		$comp_id = $competition_id;
		$event_id = $event_id;
		$date = $event_date;
		$event_name = $event_name;
		$this->session->set_userdata('date', $date);
		$this->session->set_userdata('event_name', $event_name);
		$SessArr = array(
		'class_name' =>'getmarkets',
		'comp_id' =>$comp_id,
		'date'=>$date,
		'event_id'=>$event_id,
		'event_name'=>$event_name,
		);
		$this->session->set_userdata('market_sess', $SessArr);
		$this->session->set_userdata('state', 2);
		$val=2;
		$market = $this->betfair_model->getEventsMarkets($comp_id,$event_id);
		$timeZone = $this->authorize->getMenuLinksDB($dumm,$cc,$market,$aa,$val);
		return $timeZone;
	}
	function sort_users()
	{	
		$status = $this->uri->segment(2,0);
		$event_type_id = $this->uri->segment(3,0);
		if($event_type_id)
		{
				$users = $this->betfair_model->getusersortByEventId($event_type_id,$status);
				$ii=0;
				$responseSort ='';
				foreach($users as $user)
				{
					
						$result[$ii]['rank'] = $ii+1;
						$result[$ii]['user_id'] = $user['userid'];
						$result[$ii]['username'] = $user['username'];
						$result[$ii]['firstname'] = $user['first_name'];
						$result[$ii]['lastname'] = $user['last_name'];
						$result[$ii]['slugurl'] = $user['username'];
						$result[$ii]['profit'] = $user['profit'];
						$result[$ii]['tw_screen_name'] = $user['tw_screen_name'];
						$result[$ii]['tw_token'] = $user['twitter_token'];
						$result[$ii]['total_placed_tips'] = $user['total_placed_tips'];
						if((strstr($user['profile_image'],'http://')==$user['profile_image'] || strstr($user['profile_image'],'https://')==$user['profile_image']) && $user['profile_image']!='')
						{
							if(strstr($user['profile_image'],'https://graph.facebook.com')==$user['profile_image'])
							{
								$result[$ii]['image'] = str_replace('normal','large',$user['profile_image']);
								$result[$ii]['image_margin'] = '42px';
							}
							else
							{
								$result[$ii]['image'] = str_replace('_normal','',$user['profile_image']);
								$result[$ii]['image_margin'] = '42px';
							}
						}
						else
						{
							if($user['profile_image']!='' && file_exists('uploads/profile_picture/thumb_'.$user['profile_image']))
							{
								$result[$ii]['image'] = base_url().'uploads/profile_picture/small_'.$user['profile_image'];
								$result[$ii]['image_margin'] = '0px;';
							}
							else
							{
								$result[$ii]['image'] = base_url().'assets/images/small_no_profile_pic.jpg';
								$result[$ii]['image_margin'] = '0px;';
							}
						}
						
						$ii++;
					
				}
				//$data['total'] = count($result);
				//$data['result'] = $result;
			//	echo '<pre>';print_r($result);
				if($result) {
				 foreach($result as $res) { 
					$tipsPlaced = $this->betfair_model->getuserBetPlacedEventsByUser($res['user_id'],$event_type_id);
					$userStats = $this->betfair_model->getUserStatisticsForProfile($res['user_id'],$event_type_id);
					//$resBetOdds = $this->betfair_model->getUserOddsBets($res['user_id'],$event_type_id);
					$totalWonloss = $this->betfair_model->getTotalWonloss($res['user_id'],$event_type_id);
					if(count($userStats))
					{
						$stRate = $userStats['strike_rate'];
						$ave_odds = round($userStats['ave_odds'],3);
						$rank = $userStats['user_rank'];
					}
					else
					{
						$stRate=0;
						$ave_odds=0;
						$rank = 0;
					}
					$twitter_user_id='';
					if($res['tw_token'])
					{
						$res1 = $res['tw_token'];
						$sp1 = explode('&',$res1);
						$final = explode('=',$sp1[0]);
						$final1 = explode('=',$sp1[1]);
						$user_id_twitter = explode('-',$final[1]);
						$twitter_user_id = $user_id_twitter[0];
					}	
					if($twitter_user_id)
					{
						$redirect = '<a href="javascript:void(0)" onclick="twitter_share('.$twitter_user_id.')" class="twitter_share1" title="Follow">';
						$twitter_style="display:block";	
					}
					else
					{
						$redirect = '<a href="javascript:void(0)">';
						$twitter_style="display:none";
					}
				$total = $this->betfair_model->totalTipPlaced($res['user_id'],$event_type_id);
				if($total){
					$total_tips = $total;
					}
					else
					{
						$total_tips = '0';
					}
				$responseSort .= '<div class="blog">
					<div class="number"><span>NO</span><sub>'.$res['rank'].'</sub></div>
					<div class="blog_img">';
					if(count($tipsPlaced)) {
					$responseSort .= '<a class="loadPurchaser" data-uri="'.$res['user_id'].'">';
					} 
					$responseSort .= '<img src="'.$res['image'].'" style="height:99px;width: 77px;"></a>
					<a class="buy_tip loadPurchaser" data-uri="'.$res['user_id'].'">';
					if(count($tipsPlaced)) { 
					$responseSort .= ' buy tips';
					}
					$responseSort .= '</a></div>
					<div class="blog_dis">
							<h1>';
							if(count($tipsPlaced)) {
							$responseSort .= '<a class="loadPurchaser" data-uri="'. $res['user_id'].'">';
							}
							$responseSort .= ''.$res['firstname'].' '.$res['lastname'].'</a>
								<div class="pull-right" style="'.$twitter_style.'">
								'.$redirect.'<img src="'.base_url().'assets/images/blog_icon1.png"/>
								<img src="'.base_url().'assets/images/blog_icon2.png"/></a>
								</div></h1>';
							$responseSort .='<div class="blog_row"><div class="pull-left">Correct Tip';
							
							$responseSort .='</div><div class="pull-right">'.$totalWonloss['total_win'].'/'.$totalWonloss['total_placed_tip'].'</div></div>
							<div class="blog_row"><div class="pull-left">Average odds';
							$name = ucwords($res['firstname'].' '.$res['lastname']);
							$responseSort .='</div><div class="pull-right">'.$ave_odds.'</div></div>';
							
							if($totalWonloss['total_placed_tip'] < 10) {
								$strike = 0;	
							} 
							else
							{
								$strike = $stRate;	
							}
							$responseSort .='<div class="blog_row"><div class="pull-left">							
							Strike rate</div><div class="pull-right">'. $strike.'</div></div><div class="blog_row">
							<div class="hintModal">
								<div class="pull-left">Profit</div>
							<div class="pull-right">';
							if(isset($res['profit'])){
							$responseSort .='$'.$res['profit'].'';
							} else
								{
									$responseSort .='$0.00';
								}
							$responseSort .='</div>
							<div class="hintModal_container">
								Based off placing $100 on each tips you would have made
							</div>
							</div></div>
							<div class="blog_row"><div class="pull-left">Available Tips';
							$responseSort .='</div><div class="pull-right">'.$total_tips.'</div>
							</div>
					</div>
				</div>';
				} 
			}
			else
			{
					$responseSort .= "<span style='float:left;width:100%;text-align:center;color:white;font-size:17px; margin-top: 20px;'>There are currently no Tipsters</span>";
			}
				
		}
		else
		{
			$responseSort .= "<span style='float:left;width:100%;text-align:center;color:white;font-size:17px; margin-top: 20px;'>There are currently no Tipsters</span>";
		}
		echo $responseSort;
	}
	
	function list_tipester()
	{	
		$event_id = $this->uri->segment(2,0);
		$event_type_id = $this->uri->segment(3,0);
		if($event_type_id)
		{
				$users = $this->betfair_model->getusersortByEventIdBySport($event_id,$event_type_id);
				$ii=0;
				$responseSort ='';
				foreach($users as $user)
				{
					
						$result[$ii]['rank'] = $ii+1;
						$result[$ii]['user_id'] = $user['userid'];
						$result[$ii]['username'] = $user['username'];
						$result[$ii]['firstname'] = $user['first_name'];
						$result[$ii]['lastname'] = $user['last_name'];
						$result[$ii]['slugurl'] = $user['username'];
						$result[$ii]['profit'] = $user['profit'];
						$result[$ii]['tw_screen_name'] = $user['tw_screen_name'];
						if((strstr($user['profile_image'],'http://')==$user['profile_image'] || strstr($user['profile_image'],'https://')==$user['profile_image']) && $user['profile_image']!='')
						{
							if(strstr($user['profile_image'],'https://graph.facebook.com')==$user['profile_image'])
							{
								$result[$ii]['image'] = str_replace('normal','large',$user['profile_image']);
								$result[$ii]['image_margin'] = '42px';
							}
							else
							{
								$result[$ii]['image'] = str_replace('_normal','',$user['profile_image']);
								$result[$ii]['image_margin'] = '42px';
							}
						}
						else
						{
							if($user['profile_image']!='' && file_exists('uploads/profile_picture/thumb_'.$user['profile_image']))
							{
								$result[$ii]['image'] = base_url().'uploads/profile_picture/small_'.$user['profile_image'];
								$result[$ii]['image_margin'] = '0px;';
							}
							else
							{
								$result[$ii]['image'] = base_url().'assets/images/small_no_profile_pic.jpg';
								$result[$ii]['image_margin'] = '0px;';
							}
						}
						
						$ii++;
					
				}
				 foreach($result as $res) { 
					 $tipsPlaced = $this->betfair_model->countTipPlaced($res['user_id']);
					$userStats = $this->betfair_model->getUserStatisticsForProfile($res['user_id'],$event_type_id);
					//$resBetOdds = $this->betfair_model->getUserOddsBets($res['user_id'],$event_type_id);
					$totalWonloss = $this->betfair_model->getTotalWonloss($res['user_id'],$event_type_id);
					if(count($userStats))
					{
						$stRate = $userStats['strike_rate'];
						$ave_odds = round($userStats['ave_odds'],3);
						$rank = $userStats['user_rank'];
					}
					else
					{
						$stRate=0;
						$ave_odds=0;
						$rank = 0;
					}
					
					if($res['tw_screen_name'])
					{
						$redirect = '<a href="https://twitter.com/'.$res['tw_screen_name'].'" target="_blank">';
						$twitter_style="display:block";	
					}
					else
					{
						$redirect = '<a href="javascript:void(0)">';
						$twitter_style="display:none";
					}
				$responseSort .= '<div class="blog">
					<div class="number"><span>NO</span><sub>'.$res['rank'].'</sub></div>
					<div class="blog_img">';
					if(count($tipsPlaced)) {
					$responseSort .= '<a class="loadtipesterPurchaser" data-uri="'.$res['user_id'].'" data-one="'.$event_id.'">';
					} 
					$responseSort .= '<img src="'.$res['image'].'" style="height:99px;width: 77px;"></a>
					<a class="buy_tip loadtipesterPurchaser" data-uri="'.$res['user_id'].'" data-one="'.$event_id.'">';
					if(count($tipsPlaced)) { 
					$responseSort .= ' buy tips';
					}
					$responseSort .= '</a></div>
					<div class="blog_dis">
							<h1>';
							if(count($tipsPlaced)) {
							$responseSort .= '<a class="loadtipesterPurchaser" data-uri="'. $res['user_id'].'" data-one="'.$event_id.'">';
							}
							$responseSort .= ''.$res['firstname'].' '.$res['lastname'].'</a>
								<div class="pull-right" style="'.$twitter_style.'">
								'.$redirect.'<img src="'.base_url().'assets/images/blog_icon1.png"/>
								<img src="'.base_url().'assets/images/blog_icon2.png"/></a>
								</div></h1>';
							$responseSort .='<div class="blog_row"><div class="pull-left">Correct Tip';
							
							$responseSort .='</div><div class="pull-right">'.$totalWonloss['total_win'].'/'.$totalWonloss['total_placed_tip'].'</div></div>
							<div class="blog_row"><div class="pull-left">Average odds';
							$name = ucwords($res['firstname'].' '.$res['lastname']);
							$responseSort .='</div><div class="pull-right">'.$ave_odds.'</div></div>';
							if($totalWonloss['total_placed_tip'] < 10) {
								$strike = 0;	
							} 
							else
							{
								$strike = $stRate;	
							}
							$responseSort .='<div class="blog_row"><div class="pull-left">							
							Strike rate</div><div class="pull-right">'. $strike.'</div></div><div class="blog_row">
							<div class="hintModal">
								<div class="pull-left">Profit</div>
							<div class="pull-right">';
								if(isset($res['profit'])){
								$responseSort .='$'.$res['profit'].'';
								} else
									{
										$responseSort .='$0.00';
									}
								$responseSort .='</div>
							<div class="hintModal_container">
								Based off placing $100 on each tips you would have made
							</div>
							</div></div>
							<div class="blog_row"><div class="pull-left">tip price';
							$responseSort .='</div><div class="pull-right">$6.00	</div>
							</div>
					</div>
				</div>';
				} 
				
		}
		echo $responseSort;
	}
	
	function sort_users_sport()
	{	
		$status = $this->uri->segment(2,0);
		$event_type_id = $this->uri->segment(3,0);
		$user_id = $this->uri->segment(4,0);
		$eventback = array(
		'status' =>$status,
		'event_type_id' =>$event_type_id,
		'user_id'=>$user_id,
		);
		$this->session->set_userdata('eventback', $eventback);
		$data['timeZonedata'] = $this->session->userdata('timezone');
		if(!$user_id) {
			$data['stat'] = 1;
			$data['clasName'] = "tipester_list";
			$data['tipsPlaced'] = $this->betfair_model->getuserBetPlacedEventsByUserBySports($event_type_id);
		}
		else
		{
			$data['user_id'] = $user_id;
			$data['stat'] = 0;
			$data['clasName'] = "listplacedtip";
			$data['tipsPlaced'] = $this->betfair_model->getuserBetPlacedEventsByUserSports($event_type_id,$user_id);
		}
		$eventByPurchase = $this->load->view("purchased_event",$data,true);
		echo $eventByPurchase;
	}
	function sort_purchased_tip()
	{
		$status = $this->uri->segment(2,0);
		$tipester_id = $this->uri->segment(3,0);
		$event_type_id = $this->uri->segment(4,0);
		$cur_status = $this->uri->segment(5,0);
		$userId = $this->session->userdata('userid');
		$data['userId'] = $userId;
		$data['tipsterId'] = $tipester_id;
		if($status=='past') {
			$data['events'] = $this->betfair_model->listPastPurchasedTip($userId,$tipester_id,$event_type_id);
			$data['tip_type'] = 'PAST PURCHASED TIPS';
		}
		else if($status=='current')
		{
			$data['tip_type'] = 'CURRENT PURCHASED TIPS';
			$data['events'] = $this->betfair_model->listPurchasedTipBySport($userId,$tipester_id,$event_type_id);	
		}
		else 
		{
			$data['tip_type'] = 'FUTURE TIPS';
			$data['events'] = $this->betfair_model->listFutureTipBySport($userId,$tipester_id,$event_type_id);	
		}
		
		$past_data = $this->load->view("past_tip",$data);
	//	$userData = $this->member_model->getMemberDetailsById($tipsterId);	
	}
	function postBookToTwitter() 
    { 
		$msg = "testing tweet";
		require APPPATH.'libraries/codebird.php';
	 	 \Codebird\Codebird::setConsumerKey("Rx9y90Ybv5zdyFO5dAibG7Ruh","ttED1QG4aEMuLXyXYfyFoGgl7KfrbV3Q7983YdfaaDNt0f5Vp2"); 
        $cb =  \Codebird\Codebird::getInstance(); 
        $cb->setToken("98817072-PzHfgh6a2VTNEOTmfbimSU0vsV8ZcuoygMdannu0V", "9RM5C0YYm9yIzZtKzvzciVbLZpzyRzJPgAr6iGgZB3iaD"); 
        $params = array( 
          'status' => $msg 
        ); 
		//echo '<pre>';print_r($params);die;
        $reply = $cb->statuses_update($params); 
          echo '<pre>';print_r($reply);die;
      }
    function sort_tipester_list()
	{
		$status = $this->uri->segment(2,0);
		$userId = $this->session->userdata('userid');
		$data['userId'] = $userId;
		if($status=='past') {
			$data['tipsterData'] = $this->betfair_model->getTipersterPurchasedList($userId);
			$data['tip_type'] = 'MY PAST PURCHASED TIPS';
			$data['tip_status'] = 'past';
		}
		else if($status=='current')
		{
			$data['tip_type'] = 'MY CURRENT PURCHASED TIPS';
			$data['tipsterData'] = $this->betfair_model->getUserPurchasedTispstersId($userId);
			$data['tip_status'] = 'current';
		}
		else
		{
			$data['tip_type'] = 'MY Future TIPS';
			$data['tipsterData'] = $this->betfair_model->getFuturePurchasedTisps($userId);
			$data['tip_status'] = 'future';
		}
		
		$past_data = $this->load->view("tipester_purchased_tip",$data);
	}
    function paymentsuccessCredit()
	{
		$userAgent = $_SERVER['HTTP_USER_AGENT'];
		if (preg_match('/Safari/i', $userAgent)) { 
		  header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
		} 
		$payment_type= 'credit';
		//header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
		$this->session->set_userdata('creditpayment',$payment_type);
		$payer_id =  $_GET['PayerID'];
		$token = $_GET["token"];
		$token_arr = array('payer_id'=>$payer_id,'token'=>$token);
		$this->session->set_userdata('token_value',$token_arr);
		redirect(base_url().'walletAddMoney');
	}
	function cancelpaymentCredit()
	{
		print '<script>
		opener.close_credit_windowCancel("'.base_url().'");
		myWindow.close();
		</script>';
	}
    function payment_success_email()
	{
		$payment_type= 'email';
		$userAgent = $_SERVER['HTTP_USER_AGENT'];
		if (preg_match('/Safari/i', $userAgent)) { 
		  header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
		} 
		$this->session->set_userdata('creditpayment',$payment_type);
		$payer_id =  $_GET['PayerID'];
		$token = $_GET["token"];
		$token_arr = array('payer_id'=>$payer_id,'token'=>$token);
		$this->session->set_userdata('token_value',$token_arr);
		redirect(base_url().'walletAddMoney');
	}
	function cancel_payment_email()
	{
		print '<script>
		close_email_windowCancel("'.base_url().'");
		</script>';
	}
	function load_purchased_tip()
	{
		$event_type_id = $this->input->post('event_type_id');
		$tipester_id = $this->input->post('typester_id');
		$user_id = $this->input->post('user_id');
		$status = $this->input->post('status');
		$category = $this->category_model->getAllCategories();
		$data['categories'] = $category;
		$userId = $this->session->userdata('userid');
		$data['userId'] = $userId;
		$data['status'] = $status;
		$data['event_type_id'] = $event_type_id;
		$data['tipsterId'] = $tipester_id;
		$userData = $this->member_model->getMemberDetailsById($tipester_id);
		$userStats = $this->betfair_model->getUserStatisticsForProfile($tipester_id,$event_type_id);
		$totalWonloss = $this->betfair_model->getTotalWonloss($tipester_id,$event_type_id);
		//$resBetOdds = $this->betfair_model->getUserOddsBets($tipester_id,$event_type_id);
		$data['tipesterData'] = $userData;
		$data['userStats'] = $userStats;
		$data['totalWonloss'] = $totalWonloss;
		$data['resBetOdds'] = $resBetOdds;
		if($status=='past') {
			$data['events'] = $this->betfair_model->listPastPurchasedTip($user_id,$tipester_id,$event_type_id);
			$data['tip_type'] = 'PAST PURCHASED TIPS';
		}
		else if($status=='current')
		{
			$data['tip_type'] = 'CURRENT PURCHASED TIPS';
			$data['events'] = $this->betfair_model->listPurchasedTipBySport($user_id,$tipester_id,$event_type_id);
		}
		else
		{
			$data['tip_type'] = 'Future TIPS';
			$data['events'] = $this->betfair_model->listFutureTipBySport($user_id,$tipester_id,$event_type_id);
		}
		$past_data = $this->load->view("mysortpurchased",$data);
	}
	function how_it_work()
	{
		$data = array();
		$uriVal = $this->uri->segment(3);
		if($uriVal=='place')
		{
			$statusvalue = 'loginPage';
		}
		else
		{
			$resp = $this->authorize->user_loggedIn();
			if($resp==false)
			{
				$data['login_status'] = 0;
			}else{
				$data['login_status'] = 1;
			}
			$statusvalue = '';
		}
		$data['colortheme'] = "#2888C9";
		$data['type_tip'] ='';
		$data['place_val'] =$statusvalue;
		$userId = $this->session->userdata('userid');
		$data['userData'] = $this->member_model->getMemberDetailsById($userId);
		$resp = $this->authorize->user_loggedIn();
		$data['page_content'] = $this->betfair_model->getPlacePageContent();
		$data['page_content_buy'] = $this->betfair_model->getBuyPageContent();
		$data['page_body'] = $this->load->view("how_it_work_place",$data,true);	
		$this->load->view("templates_main",$data);	
	}
	function send_emails()
	{
		$data = array();
		$mail_result = $this->betfair_model->getAllPlacedUsers();
		foreach($mail_result as $results)
		{
			$user_id = $results['userid'];
			$userData = $this->betfair_model->getUserInfo($user_id);
			$total_val = (count($results)-1)/8 ;
			$txt_val = array();
			for($i = 1;$i<=$total_val;$i++)
			{
				$open_date = $results['open_date'.$i];
				$timezone = $results['timezone'.$i];
				if($results["placedtip".$i]==$results["resulttip".$i])
				{
						$status = "Won";
						$color_val = "color:#008000";
				}
				else
				{
						$status = "Loss";
						$color_val = "color:#FF0000";
					
				}
				$event_markets = $this->betfair_model->getEventsMarketsByMarketId($results['market_id'.$i]);
				$eventDate1 = $this->betfair_model->convert_time_zone($open_date,$timezone,$this->session->userdata('timezone'));
				$allLinks1 = date("l",strtotime($eventDate1))." ".date("d/m/Y",strtotime($eventDate1));
				$date =  $allLinks1 .' ('.date("h:i a",strtotime($eventDate1)).')'; 
				
				$txt_val[]= "".$date." $$$$$ ".$results["event_name".$i]." $$$$$ ".$results["team_name".$i]." $$$$$ ".$results['placedtip'.$i]." $$$$$ ".$status." $$$$$ ".$event_markets['market_name']." $$$$$ ".$color_val."";
			}
				$email_content = $txt_val;
				$email_temp ='';
				$spitVal = '';
				$email_temp .= '<table style="display: block;font-size: 16px;padding: 10px 0;" width="100%"><tbody style="width:100%;display:block;">';
				foreach($email_content as $content) {
					$spitVal = explode("$$$$$",$content); 
					$email_temp .= '  <tr class="result_date">
								<td colspan="3" style="font-size:15px; verticle-align:top">'.$spitVal[1].' - '.$spitVal[0].'</td>
								</tr>
							<tr style="border-bottom: 1px solid black;">
							<td style="font-size:14px;margin-right:10px;vertical-align: top; width:90%;">
								<span style="display:block;"> '.$spitVal[5].' - '.$spitVal[2].'</span>
							 </td>';
						$email_temp .= '<td style="font-size:14px; text-align:right;vertical-align: top;width:8%;'.$spitVal[6].'">'.'('.$spitVal[4].')'.'</td>
						</tr>
						<tr style="border-bottom: 1px solid rgb(0, 0, 0);" border="1"><td height="15" colspan="3"></td></tr>
						';
						}
			$email_temp .= '</tbody></table>';
			$email = $userData['email'];
			$to = $email;
			$email_template_id = 5;
			$first_name = $userData['first_name'];
			$last_name = $userData['last_name'];
			$name = $userData['username'];
			$password = $userData['password'];
			
			$email_content = $this->getEmailTemplate($email_template_id);
			$message = $email_content[0]['body'];
			
			$search = array('{FIRSTNAME}','{LASTNAME}','{EMAIL_RESULT}','{SITELINK}');
			$replace= array($first_name,$last_name,$email_temp,'www.whispertip.com');
			$body=str_replace($search,$replace,$message);	
			$data['template_body'] = $body;
			$page = $this->load->view('email_template',$data,true);	
			//echo $page;die;
			$config['charset'] = 'iso-8859-1';
			$config['mailtype'] = 'html';
			$subject = $email_content[0]['subject'];
			$from = $email_content[0]['from'];
			$this->load->library('email');
			$this->email->initialize($config);
			$this->email->from($from);
			$this->email->to($to);
			$this->email->subject($subject);
			$this->email->message($page);
			$email_status = $this->email->send();
			
		}
		$this->betfair_model->getTopWeekTipster();
	}
	function terms_conditions()
	{
		$data = array();
		$uriVal = $this->uri->segment(2);
		if($uriVal=='do_login')
		{
			$statusvalue = 'loginPage';
		}
		else
		{
			$resp = $this->authorize->user_loggedIn();
			if($resp==false)
			{
				redirect(base_url().'home');
			}
			$statusvalue = '';
		}
		$data['place_val'] =$statusvalue;
		$userId = $this->session->userdata('userid');
		$data['userData'] = $this->member_model->getMemberDetailsById($userId);
		$data['colortheme'] = "#2888C9";
		$data['type_tip'] ='';
		$data['page_body'] = $this->load->view("terms_condition",$data,true);	
		$this->load->view("templates_main",$data);	
	}
	function privacy_policy()
	{
		$data = array();
		$uriVal = $this->uri->segment(2);
		if($uriVal=='do_login')
		{
			$statusvalue = 'loginPage';
		}
		else
		{
			$resp = $this->authorize->user_loggedIn();
			if($resp==false)
			{
				$data['login_status'] = 0;
			}else{
				$data['login_status'] = 1;
			}
			$statusvalue = '';
		}
		$data['place_val'] =$statusvalue;
		$userId = $this->session->userdata('userid');
		$data['userData'] = $this->member_model->getMemberDetailsById($userId);
		$data['colortheme'] = "#2888C9";
		$data['type_tip'] ='';
		$data['page_body'] = $this->load->view("privacy_policy",$data,true);	
		$this->load->view("templates_main",$data);	
	}
	function getpurchasedData()
	{
		$placed_tip_id = $this->session->userdata('placed_tip_id');
		$time_val = $this->session->userdata('timezone');
		$purchasedData = $this->betfair_model->getMyPurchasedTipById($placed_tip_id);
		$MarketName = $this->betfair_model->getMarketNameById($purchasedData['market_id']);
		$date = $this->betfair_model->convert_time_zone($purchasedData['open_date'],$purchasedData['timezone'],$time_val);
		$date1 = date("l",strtotime($date))." ".date("d/m/Y",strtotime($date));
		$html = '';
		$html .= '<div style="color:#737373;font-size:17px;margin-top:10px">Tip detail:</div>
		<span style="border:none;color: #737373;font-size: 14px;">'.$date1.'</span>
						<div class="team_names no_border" style="border-bottom: 1px solid #ccc">'.$purchasedData['event_name'].'</div>
							<div style="text-align:center; border-bottom: 1px solid #CCCCCC;">
								<div class="team_names" style="display:inline-block;"><p>'.$MarketName['market_name'].'</p></div>
							</div>
							<div style="text-align:center;color: #737373;">
								<div class="blog_dis">
								<div class="pull-left">'.$purchasedData['team_name'].'</div>
								<div class="pull-right">'.$purchasedData['selected_odd'].'</div>
								</div>
							</div>
							<div style="color:#737373;font-size:15px;margin-top:15px">You can see all your purchased tips 
							<span style="color:blue;cursor:pointer" id="navigate_purchased">here</span></div>';
							echo $html;
	}
	function landing()
	{
		$data = array();
		$newdata = array(
					'twitter_id'  => '121212',
					'first_name' => 'satish',				
					'via' => 'twitter',					
					'signup_date' => date('Y/m/d H:m:s')                 
               );
			$this->session->set_userdata('twittersess',$newdata);
		$resp = $this->authorize->user_loggedIn();
		if($resp===true)
		{
			//session_start();session_destroy();die;
			redirect(base_url().'select');
		}
		$this->load->view("template_landing",$data);	
		
	}
	function login_popup()
	{
		$data = array();
		$mode = $this->uri->segment(3,0);
		if($mode)
		{
			$data['mode'] = $mode;
		}
		$this->load->view("login_form",$data);	
	}
	function delete_account()
	{
		$userId = $this->session->userdata('userid');
		$status = $this->betfair_model->deleteusers($userId);
		if($status=='true'){
			echo json_encode('sucess');
		}
		else
		{
			echo json_encode('ERROR');
		}
	}
	function sort_time()
	{
		$sortVal = $this->uri->segment(2,0);
		$event_type_id = $this->uri->segment(3,0);
		$timerange = $this->uri->segment(4,0);
		if($event_type_id)
		{
				$users = $this->betfair_model->sortByTime($sortVal,$event_type_id,$timerange);
				$ii=0;
				$responseSort ='';
				foreach($users as $user)
				{
						$result[$ii]['userid'] = $user['userid'];
						$result[$ii]['firstname'] = $user['first_name'];
						$result[$ii]['lastname'] = $user['last_name'];
						$result[$ii]['profit'] = $user['profit'];
						$result[$ii]['strikeRate'] = $user['strikeRate'];
						$result[$ii]['ave_odds'] = $user['ave_odds'];
						$result[$ii]['total_won'] = $user['total_won'];
						$result[$ii]['total_loss'] = $user['total_loss'];
						$result[$ii]['total_won_loss'] = $user['total_won_loss'];
						$result[$ii]['tw_screen_name'] = $user['tw_screen_name'];
						$result[$ii]['tw_token'] = $user['twitter_token'];
						if((strstr($user['profile_image'],'http://')==$user['profile_image'] || strstr($user['profile_image'],'https://')==$user['profile_image']) && $user['profile_image']!='')
						{
							if(strstr($user['profile_image'],'https://graph.facebook.com')==$user['profile_image'])
							{
								$result[$ii]['image'] = str_replace('normal','large',$user['profile_image']);
								$result[$ii]['image_margin'] = '42px';
							}
							else
							{
								$result[$ii]['image'] = str_replace('_normal','',$user['profile_image']);
								$result[$ii]['image_margin'] = '42px';
							}
						}
						else
						{
							if($user['profile_image']!='' && file_exists('uploads/profile_picture/thumb_'.$user['profile_image']))
							{
								$result[$ii]['image'] = base_url().'uploads/profile_picture/small_'.$user['profile_image'];
								$result[$ii]['image_margin'] = '0px;';
							}
							else
							{
								$result[$ii]['image'] = base_url().'assets/images/small_no_profile_pic.jpg';
								$result[$ii]['image_margin'] = '0px;';
							}
						}
						
						$ii++;
					
				}
				//$data['total'] = count($result);
				//$data['result'] = $result;
			//	echo '<pre>';print_r($result);
				if($result) {
					$i=1;
				 foreach($result as $res) { 
					$tipsPlaced = $this->betfair_model->getuserBetPlacedEventsByUser($res['userid'],$event_type_id); 
					$total = $this->betfair_model->totalTipPlaced($res['userid'],$event_type_id);
					//$total = $this->betfair_model->totalTipPlaced($res['user_id'],$event_type_id);

					$twitter_user_id='';
					if($res['tw_token'])
					{
						$res1 = $res['tw_token'];
						$sp1 = explode('&',$res1);
						$final = explode('=',$sp1[0]);
						$final1 = explode('=',$sp1[1]);
						$user_id_twitter = explode('-',$final[1]);
						$twitter_user_id = $user_id_twitter[0];
					}	
					
					if($twitter_user_id)
					{
						$redirect = '<a href="javascript:void(0)" onclick="twitter_share('.$twitter_user_id.')" class="twitter_share1" title="Follow">';
						$twitter_style="display:block";	
					}
					else
					{
						$redirect = '<a href="javascript:void(0)">';
						$twitter_style="display:none";
					}
				if($total){
					$total_tips = $total;
					}
					else
					{
						$total_tips = '0';
					}
				$responseSort .= '<div class="blog">
					<div class="number"><span>NO</span><sub>'.$i.'</sub></div>
					<div class="blog_img">';
					if(count($tipsPlaced)) {
					$responseSort .= '<a class="loadPurchaser" data-uri="'.$res['userid'].'">';
				}
					$responseSort .= '<img src="'.$res['image'].'" style="height:99px;width: 77px;"></a>
					<a class="buy_tip loadPurchaser" data-uri="'.$res['userid'].'">';
					if(count($tipsPlaced)) { 
					$responseSort .= ' buy tips';
				}
					$responseSort .= '</a></div>
					<div class="blog_dis">
							<h1>';
							if(count($tipsPlaced)) {
							$responseSort .= '<a class="loadPurchaser" data-uri="'. $res['user_id'].'">';
							}
							$responseSort .= ''.$res['firstname'].' '.$res['lastname'].'</a>
								<div class="pull-right" style="'.$twitter_style.'">
								'.$redirect.'<img src="'.base_url().'assets/images/blog_icon1.png"/>
								<img src="'.base_url().'assets/images/blog_icon2.png"/></a>
								</div></h1>';
							$responseSort .='<div class="blog_row"><div class="pull-left">Correct Tip';
							
							$responseSort .='</div><div class="pull-right">'.$res['total_won'].'/'.$res['total_won_loss'].'</div></div>
							<div class="blog_row"><div class="pull-left">Average odds';
							$name = ucwords($res['firstname'].' '.$res['lastname']);
							$responseSort .='</div><div class="pull-right">'.round($res['ave_odds'],3).'</div></div>';
							$responseSort .='<div class="blog_row"><div class="pull-left">							
							Strike rate</div><div class="pull-right">'. $res['strikeRate'].'</div></div><div class="blog_row">
							<div class="hintModal">
								<div class="pull-left">Profit</div>
							<div class="pull-right">';
							if(isset($res['profit'])){
							$responseSort .='$'.$res['profit'].'';
							} else
								{
									$responseSort .='$0.00';
								}
							$responseSort .='</div>
							<div class="hintModal_container">
								Based off placing $100 on each tips you would have made
							</div>
							</div></div>
							<div class="blog_row"><div class="pull-left">Available Tips';
							$responseSort .='</div><div class="pull-right">'.$total_tips.'</div>
							</div>
					</div>
				</div>';
				$i++;
				} 
			}
			else
			{
					$responseSort .= "<span style='float:left;width:100%;text-align:center;color:white;font-size:17px; margin-top: 20px;'>There are currently no Tipsters</span>";
			}
				
		}
		else
		{
			$responseSort .= "<span style='float:left;width:100%;text-align:center;color:white;font-size:17px; margin-top: 20px;'>There are currently no Tipsters</span>";
		}
		echo $responseSort;
	}
	function fetch_record()
	{
		$tipsPlaced = $this->betfair_model->getdata(); 
	}
	function update_wallet()
	{
		$tipsPlaced = $this->betfair_model->update_wallet_balance(); 
	}
	function filter_competition(){
				$userId = $this->session->userdata('userid');
				$data['userId'] = $userId;
				$comp_id = $this->uri->segment(2,0);
				$event_type= $this->uri->segment(3,0);
				$data['hostPath'] =$this->config->item('path');
				$event_type_id = $this->betfair_model->getCompetitionId($event_type);
				$data['event_type_id'] = $event_type_id ;
				$data['event_type'] = $event_type;
				$data['league'] = $this->betfair_model->getLeagueDataBYCompId($event_type_id,$comp_id);
				$this->load->library('pagination');
				$config['base_url'] = base_url().'home/place-tips';
				$config['total_rows'] = count($data['league']);
				$config['per_page'] = 10;//$this->config->item('pagesize');
				$config['uri_segment'] = 4;
				$config['ajax'] = true;
				$this->pagination->initialize($config); 
				$data['total_rows'] = count($data['league']);
				$data['page_number'] = $this->uri->segment(4,0);
				$data['page_size'] = 10;
				//echo $event_type_id.','.$comp_id.','.$data['page_number'].','.$data['page_size'];die;
				$data['league'] = $this->betfair_model->getLeagueDataBYCompId($event_type_id,$comp_id,$data['page_number'], $data['page_size']);
				//echo '<pre>';print_r($data['league']);die;
				echo $this->load->view("filter_place_tip",$data);	
	}
	function get_pagination(){
				$event_type = $this->uri->segment(2,0);
				$data['event_type'] = $event_type;
				$userId = $this->session->userdata('userid');
				$data['userId'] = $userId;
				$data['hostPath'] =$this->config->item('path');
				if($event_type)
				{
					$data['category'] = $this->category_model->getAllCategories();
					$eventType = $event_type;
				}
				else
				{
					$data['category'] = $this->category_model->getAllCategories();
					$eventType = $category[0]['slugUrl'];
				}
				$data['eventType'] = $eventType;
				$event_type_id = $this->betfair_model->getCompetitionId($eventType);
				$data['event_type_id'] = $event_type_id ;
				$data['comp'] = $this->betfair_model->getAllCompById($event_type_id);
				$data['league'] = $this->betfair_model->getLeagueData($event_type_id);
				//echo '<pre>';print_r($data['league']);die;
				$this->load->library('pagination');
				$config['base_url'] = base_url().'home/get_pagination';
				$config['total_rows'] = count($data['league']);
				$config['per_page'] = 10;//$this->config->item('pagesize');
				$config['uri_segment'] = 3;
				$config['ajax'] = true;
				$this->pagination->initialize($config); 
				$data['total_rows'] = count($data['league']);
				$data['page_number'] = $this->uri->segment(3,0);
				$data['page_size'] = 10;
				
				$data['league'] = $this->betfair_model->getLeagueData($event_type_id,$data['page_number'], $data['page_size']);
			//	echo '<pre>';print_r($data['league']);die;
			
				echo $this->load->view("paging_leagues",$data);	
				
		}
	function markets(){
			$data = array();
			$resp = $this->authorize->user_loggedIn();
			if($resp==false)
			{
				$data['login_status'] = 0;
			}else{
				$data['login_status'] = 1;
				}
			$userId = $this->session->userdata('userid');
			$data['userId'] = $userId;
			$data['userData'] = $this->member_model->getMemberDetailsById($userId);
			$event_id =  $this->input->post('event_id');
			$data['event_id'] = $event_id;
			$data['event_type_id'] =  $this->input->post('event_type_id');
			$data['sportName'] = $this->betfair_model->getEventTypeName($data['event_type_id']);
			if($this->input->post('event_id')){
					$data['competition'] = $this->betfair_model->getCompetitionDetailById($event_id);
					$off = 0;
					$limit = 20;
					$markets = $this->betfair_model->getMarkets($event_id,$off,$limit);
				foreach($markets as $key=>$market){
					$runners[] = $this->betfair_model->getRunners($market['market_id']);
					}
					$data['runnerDetail']  = $runners;
					foreach($markets as $key=>$market){
						if(substr($market['market_id'], 0, 1 ) == 1){
								$markets_inter[] = $market['market_id'];
							}else{
								$markets_aust[] = $market['market_id'];
								}
						
						}
				$markets_inter = array_unique($markets_inter);
				$markets_aust = array_unique($markets_aust);
				
				$data['hostPath'] =$this->config->item('path');
				$data['MarketidInter'] = implode(',',$markets_inter);
				$data['MarketidAus'] = implode(',',$markets_aust);
				$data['category'] = $this->category_model->getAllCategories();
				$data['page_body'] = $this->load->view("markets",$data,true);
				$this->load->view("templates_main",$data);	
		}else{
			redirect(base_url().'select');
			}
	}
	function loadMoreMarkets(){
		$userId = $this->session->userdata('userid');
		$data['userId'] = $userId;
		$uriVal= $this->uri->segment(2);
		$event_id= $this->uri->segment(3);
		if($uriVal < 2 ) {
		$pagesize = 20*($uriVal);
		$istart = 0;
		} 
		else {
		$pagesize = 20;
		$istart = ($uriVal -1) * $pagesize;
		$nstart = ($uriVal) * $pagesize;
		//$nextData = $this->patients_model->getMarkets($provider_id,$pagesize,$nstart);
		}
		$markets = $this->betfair_model->getMarkets($event_id,$istart,$pagesize);
		echo count($markets).'#########';
		foreach($markets as $key=>$market){
			$runners[] = $this->betfair_model->getRunners($market['market_id']);
			}
			$data['runnerDetail']  = $runners;
			foreach($markets as $key=>$market){
				if(substr($market['market_id'], 0, 1 ) == 1){
						$markets_inter[] = $market['market_id'];
					}else{
						$markets_aust[] = $market['market_id'];
						}
				
				}
			$markets_inter = array_unique($markets_inter);
			$markets_aust = array_unique($markets_aust);
			$data['hostPath'] =$this->config->item('path');
			$data['MarketidInter'] = implode(',',$markets_inter);
			$data['MarketidAus'] = implode(',',$markets_aust);
			echo $this->load->view("loadMoreMarkets",$data,true);
		}
		function user_public_profile(){
			$data = array();
			if($this->input->get('q')){
					$id_val = $this->input->get('q');
					$tempArr = explode('?',$id_val);
					$user_id = $tempArr[0];
				}
				if($this->uri->segment(2,0)){
					$user_id = $this->uri->segment(2,0);
				}
			$data['user_id'] = $user_id;
			$login_userId = $this->session->userdata('userid');
			if(!$user_id){
				redirect(base_url().'home');
				}
			$resp = $this->authorize->user_loggedIn();
			if($resp==false)
			{
				$data['login_status'] = 0;
			}else{
				$data['login_status'] = 1;
				$data['userData'] = $this->member_model->getMemberDetailsById($login_userId);
				}	
			$data['profiler_userData'] = $this->member_model->getMemberDetailsById($user_id);
			if(!$data['profiler_userData']){
				redirect(base_url().'home');
				}
			if($_SERVER['REMOTE_ADDR']=='127.0.0.1' || $_SERVER['REMOTE_ADDR']=='::1' || strstr($_SERVER['REMOTE_ADDR'],'192.168.')==$_SERVER['REMOTE_ADDR'])
			{
				$remoteAddr = '122.173.135.80';
			}
			//$timeZone = $this->authorize->getUserTimeZone($remoteAddr);
			$data['timezone'] = $this->session->userdata('timezone');
			
			if($data['timezone']=='')
			{
				$timeZone = $this->authorize->getUserTimeZone($remoteAddr);
				$this->session->set_userdata('timezone',$timeZone);
				$data['timezone'] = $timeZone;
			}
			
			date_default_timezone_set($timeZone);
			$data['type_tip'] ='';
			$data['colortheme'] = "#2888C9";
			$isToken = $this->register_user->getTwitterPublicToken($user_id);
			$data['twitter_user_id'] = '';
			if($isToken)
			{
				$res1 = $isToken;
				$sp1 = explode('&',$res1);
				$final = explode('=',$sp1[0]);
				$final1 = explode('=',$sp1[1]);
				$user_id_twitter = explode('-',$final[1]);
				$data['twitter_user_id'] = $user_id_twitter[0];
				/*$oauth = array( 'oauth_consumer_key' => $this->key,
					'oauth_nonce' => time(),
					'oauth_signature_method' => 'HMAC-SHA1',
					'oauth_token' => $final[1],
					'oauth_timestamp' => time(),
					'oauth_version' => '1.0');
				$url = "https://api.twitter.com/1.1/account/verify_credentials.json";
				$base_info = $this->buildBaseString($url, 'GET', $oauth);
				$composite_key = rawurlencode($this->secret) . '&' . rawurlencode($final1[1]);
				$oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
				$oauth['oauth_signature'] = $oauth_signature;
				$header = array($this->buildAuthorizationHeader($oauth), 'Expect:');
				$options = array( CURLOPT_HTTPHEADER => $header,
								  CURLOPT_HEADER => false,
								  CURLOPT_URL => $url,
								  CURLOPT_RETURNTRANSFER => true,                 
								  CURLOPT_SSL_VERIFYPEER => false);

				$feed = curl_init();
				curl_setopt_array($feed, $options);
				$json = curl_exec($feed);
				curl_close($feed);
				//var_dump($json);
				$twitter_data = json_decode($json,true);
				$newdata['followers_count'] = $twitter_data['followers_count'];
				$newdata['friends_count']   = $twitter_data['friends_count'];
				$newdata['statuses_count']  = $twitter_data['statuses_count'];
				$this->session->set_userdata('twitterconnectsess',$newdata);*/
			}	
			$userDataInfo = $this->betfair_model->getTipsterDataById($user_id);
			$data['userDataInfo'] = $userDataInfo;
			$data['place_tips'] = $this->betfair_model->getAllUserPlacedTips($user_id);
			
			$data['page_body'] = $this->load->view("user_public_profile",$data,TRUE);	
			$this->load->view("templates_main",$data);	
		}
		function buy_tip(){
			$place_tip_id = $this->uri->segment(3,0);
			$data['place_tip_id'] = $place_tip_id;
			$resp = $this->authorize->user_loggedIn();
			if($resp==false)
			{
				redirect(base_url().'home');
			}else{
			$data['login_status'] = 1;
			}
			$userId = $this->session->userdata('userid');
			$data['userData'] = $this->member_model->getMemberDetailsById($userId);
			$data['whisperTipFee'] = $this->config->item('whispertipfee');
			$data['user_id'] = $userId;
			$tip_detail = $this->betfair_model->getTipstersByPlaceTipId($place_tip_id);
			$data['event_type'] = $this->betfair_model->getEventTypeName($tip_detail['event_type_id']);
			$data['tips_detail'] = $tip_detail;
			$data['timeZonedata'] = $this->session->userdata('timezone');
			$data['tipsterId'] = $tip_detail['userid'];
			$data['categories'] = $this->category_model->getAllCategories();
			$users = $this->betfair_model->getuserBetPlacedEvents($tip_detail['event_type_id']);
			foreach($users as $user)
			{
				$result[$ii]['rank'] = $ii+1;
				$result[$ii]['user_id'] = $user['userid'];
				$result[$ii]['username'] = $user['username'];
				$result[$ii]['firstname'] = $user['first_name'];
				$result[$ii]['lastname'] = $user['last_name'];
				$result[$ii]['slugurl'] = $user['username'];
				$result[$ii]['profit'] = $user['profit'];
				$result[$ii]['tw_screen_name'] = $user['tw_screen_name'];
				$result[$ii]['total_placed_tips'] = $user['total_placed_tips'];
				if((strstr($user['profile_image'],'http://')==$user['profile_image'] || strstr($user['profile_image'],'https://')==$user['profile_image']) && $user['profile_image']!='')
				{
					if(strstr($user['profile_image'],'https://graph.facebook.com')==$user['profile_image'])
					{
						$result[$ii]['image'] = str_replace('normal','large',$user['profile_image']);
						$result[$ii]['image_margin'] = '42px';
					}
					else
					{
						$result[$ii]['image'] = str_replace('_normal','',$user['profile_image']);
						$result[$ii]['image_margin'] = '42px';
					}
				}
				else
				{
					if($user['profile_image']!='' && file_exists('uploads/profile_picture/thumb_'.$user['profile_image']))
					{
						$result[$ii]['image'] = base_url().'uploads/profile_picture/small_'.$user['profile_image'];
						$result[$ii]['image_margin'] = '0px;';
					}
					else
					{
						$result[$ii]['image'] = base_url().'assets/images/small_no_profile_pic.jpg';
						$result[$ii]['image_margin'] = '0px;';
					}
				}
				
				$ii++;
			
		}
		if($data['event_type']['slugUrl'] =='soccer')
		{
			$data['colortheme'] = "#B41414";
			$data['style'] = "soccer.css";
		}
		else if($data['event_type']['slugUrl'] =='cricket')
		{
			$data['colortheme'] = "#2888C9";
			$data['style'] = "cricket.css";
		}
		else if($data['event_type']['slugUrl'] =='boxing-league')
		{
			$data['colortheme'] = "#ED281B";
			$data['style'] = "boxing.css";
		}
		else if($data['event_type']['slugUrl'] =='basketball')
		{
			$data['colortheme'] = "#2888C9";
			$data['style'] = "basketball.css";
		}
		
		else if($data['event_type']['slugUrl'] =='tennis-cup')
		{
			$data['colortheme'] = "#60D1CF";
			$data['style'] = "tennis.css";
		}
		
		else if($data['event_type']['slugUrl'] =='hockey-league')
		{
			$data['colortheme'] = "#2888C9";
			$data['style'] = "hockey.css";
		}
		
		else if($data['event_type']['slugUrl'] =='rugby-league')
		{
			$data['colortheme'] = "#FFC814";
			$data['style'] = "rugby.css";
		}
		
		else if($data['event_type']['slugUrl'] =='rugby-union')
		{   
			$data['colortheme'] = "#FFC814";
			$data['style'] = "rugby.css";
		}
		
		else if($data['event_type']['slugUrl'] =='australian-rules')
		{
			$data['colortheme'] = "#ED281B";
			$data['style'] = "australian-rules.css";
		}
		
		else if($data['event_type']['slugUrl'] =='baseball')
		{
			$data['colortheme'] = "#000000";
			$data['style'] = "baseball.css";
		}
		
		else if($data['event_type']['slugUrl'] =='martial-arts')
		{
			$data['colortheme'] = "#B41414";
			$data['style'] = "martial_arts.css";
		}
		else if($data['event_type']['slugUrl'] =='golf')
		{
			$data['colortheme'] = "#000000";
			$data['style'] = "golf.css";
		}
		else if($data['event_type']['slugUrl'] =='motor-sport')
		{
			$data['colortheme'] = "#FFC814";
			$data['style'] = "motor_sport.css";
		}
		else if($data['event_type']['slugUrl'] =='american-football')
		{
			$data['colortheme'] = "#B41414";
			$data['style'] = "american_football.css";
		}
		else if($data['event_type']['slugUrl'] =='surfing')
		{
			$data['colortheme'] = "#0FB3EE";
			$data['style'] = "surfing.css";
		}
		$data['total'] = count($result);
		$data['result'] = $result;
		$data['page_body'] = $this->load->view("purchase_list_public_users",$data,TRUE);
		$this->load->view("templates_main",$data);	
		}
function Events_markets(){
			$data = array();
			$resp = $this->authorize->user_loggedIn();
			if($resp==false)
			{
				$data['login_status'] = 0;
			}else{
				$data['login_status'] = 1;
				}
			$userId = $this->session->userdata('userid');
			$data['userId'] = $userId;
			$data['userData'] = $this->member_model->getMemberDetailsById($userId);
			$event_id =  $this->uri->segment(2,0); 
			$data['event_id'] = $event_id;
			$data['event_type_id'] =  $this->input->post('event_type_id');
			$data['sportName'] = $this->betfair_model->getEventTypeName($data['event_type_id']);
			if($event_id){
					$data['competition'] = $this->betfair_model->getCompetitionDetailById($event_id);
					$off = 0;
					$limit = 20; 
					$markets = $this->betfair_model->getMarkets($event_id,$off,$limit);
				foreach($markets as $key=>$market){
					$runners[] = $this->betfair_model->getRunners($market['market_id']);
					}
					$data['runnerDetail']  = $runners;
					foreach($markets as $key=>$market){
						if(substr($market['market_id'], 0, 1 ) == 1){
								$markets_inter[] = $market['market_id'];
							}else{
								$markets_aust[] = $market['market_id'];
								}
						
						}
				$markets_inter = array_unique($markets_inter);
				$markets_aust = array_unique($markets_aust);
				
				$data['hostPath'] =$this->config->item('path');
				$data['MarketidInter'] = implode(',',$markets_inter);
				$data['MarketidAus'] = implode(',',$markets_aust);
				$data['category'] = $this->category_model->getAllCategories();
				$data['page_body'] = $this->load->view("markets",$data,true);
				$this->load->view("templates_main",$data);	
		}else{
			redirect(base_url().'select');
			}
	}
		function getWeeklyTipster(){
			$this->betfair_model->getTopWeekTipster();
		}
		function overall_tipster()
	{
		$data = array();
		$resp = $this->authorize->user_loggedIn();
		if($resp==false)
		{
			$data['login_status'] = 0;
		}else{
			$data['login_status'] = 1;
			}
		$userId = $this->session->userdata('userid');
		$users = $this->betfair_model->getTopOverallTipster();
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$filter = $this->uri->segment(2,0);
			if($filter == 'strike')
			{
				usort($users, function($a, $b) {
					return $b['strike'] > $a['strike'] ? 1 : -1;
				});
			} 
			else if($filter == 'correcttips') 
			{
				usort($users, function($a, $b) {
					return $b['total_won_tips'] > $a['total_won_tips'] ? 1 : -1;
				});
			}
			else
			{
				usort($users, function($a, $b) {
					return $b['profit'] > $a['profit'] ? 1 : -1;
				});
			}
		}
		else
		{
			usort($users, function($a, $b) {
				return $b['profit'] > $a['profit'] ? 1 : -1;
			});
		}
		$ii=0;
		$c = 1;
		foreach($users as $user)
		{
			if($c == 101)
				break;
				$result[$ii]['rank'] = $ii+1;
				$result[$ii]['user_id'] = $user['userid'];
				$result[$ii]['username'] = $user['username'];
				$result[$ii]['firstname'] = $user['first_name'];
				$result[$ii]['lastname'] = $user['last_name'];
				$result[$ii]['slugurl'] = $user['username'];
				$result[$ii]['profit'] = $user['profit'];
				$result[$ii]['tw_token'] = $user['twitter_token'];
				$result[$ii]['tw_screen_name'] = $user['tw_screen_name'];
				$result[$ii]['total_placed_tips'] = $user['total_placed_tips'];
				$result[$ii]['total_won_tips'] = $user['total_won_tips'];
				$result[$ii]['total_loss_tips'] = $user['total_loss_tips'];
				$result[$ii]['total_odds'] = $user['total_odds'];
				if((strstr($user['profile_image'],'http://')==$user['profile_image'] || strstr($user['profile_image'],'https://')==$user['profile_image']) && $user['profile_image']!='')
				{
					if(strstr($user['profile_image'],'https://graph.facebook.com')==$user['profile_image'])
					{
						$result[$ii]['image'] = str_replace('normal','large',$user['profile_image']);
						$result[$ii]['image_margin'] = '42px';
					}
					else
					{
						$result[$ii]['image'] = str_replace('_normal','',$user['profile_image']);
						$result[$ii]['image_margin'] = '42px';
					}
				}
				else
				{
					if($user['profile_image']!='' && file_exists('uploads/profile_picture/thumb_'.$user['profile_image']))
					{
						$result[$ii]['image'] = base_url().'uploads/profile_picture/small_'.$user['profile_image'];
						$result[$ii]['image_margin'] = '0px;';
					}
					else
					{
						$result[$ii]['image'] = base_url().'assets/images/small_no_profile_pic.jpg';
						$result[$ii]['image_margin'] = '0px;';
					}
				}
				
				$ii++;
				$c++;
			
		}	
		$data['total'] = count($result);
		$data['result'] = $result;
		$userId = $this->session->userdata('userid');
		$data['userData'] = $this->member_model->getMemberDetailsById($userId);
		$data['colortheme'] = "#2888C9";
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$viewData = $this->load->view("ajax_overall_tipster",$data,true);
			echo $viewData;
			exit;
		}
		else
		{
			$data['page_body'] = $this->load->view("overall_tipster",$data,true);
			$this->load->view("templates_main",$data);		
		}
	}

}

<?php class Paypal extends Controller {

	function Paypal()
	{
		parent::Controller();	
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->config->load('config');
		$this->load->library('email');
		$this->load->model('wallet_model');
		//$this->config->set_item('enable_query_strings', TRUE);
	}
	function process()
	{
		$val = BASEPATH;
		$basepath = str_replace('system', 'assets', BASEPATH);
		$filetoinclude = $basepath.'paypal.class.php';
		include_once $filetoinclude;
		$type =  $this->uri->segment(3,0);
		$amountValue =  $this->input->post('addamount');
		if($type==1)
		{
			$amount = $amountValue;
			$sol_type = '';
			$landingPage='';
			$PayPalReturnURL = base_url()."?c=home&m=payment_success_email";	
			$PayPalCancelURL = base_url()."?c=home&m=walletAddMoney";
		}
		else
		{
			$amount = $amountValue;
			$sol_type = 'Sole';
			$landingPage='Billing';
			$PayPalReturnURL = base_url()."?c=home&m=paymentsuccessCredit";	
			$PayPalCancelURL = base_url()."?c=home&m=walletAddMoney";
		}
		$this->load->library('email');
		$price = 1;
		$PayPalMode = $this->config->item('PayPalMode');
		$PayPalApiUsername = 'nick_api1.whispertip.com';
		$PayPalApiPassword = 'GJU4P9YUM58RP4VW';	
		$PayPalApiSignature = 'AFcWxV21C7fd0v3bYYYRCpSSRl31A9tBpjPyVAzGFQveuo1dVnQRwy8F';	
		$receiver_email = "nick-facilitator@whispertip.com";
		$PayPalCurrencyCode = 'AUD';	
		$itemname ="Tips";
		$ItemNumber = 1;
		$itemQty1 = 1;
		$ItemTotalPrice = ($amount*$itemQty1);
		$this->session->set_userdata('total_amount',$ItemTotalPrice);
		$padata = '&CURRENCYCODE='.urlencode($PayPalCurrencyCode).
				'&PAYMENTACTION=Sale'.
				'&ALLOWNOTE=1'.
				'&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode($PayPalCurrencyCode).
				'&PAYMENTREQUEST_0_AMT='.urlencode($ItemTotalPrice).
				'&PAYMENTREQUEST_0_ITEMAMT='.urlencode($ItemTotalPrice). 
				'&L_PAYMENTREQUEST_0_QTY0='. urlencode($itemQty1).
				'&L_PAYMENTREQUEST_0_AMT0='.urlencode($amount).
				'&L_PAYMENTREQUEST_0_NAME0='.urlencode($itemname).
				'&L_PAYMENTREQUEST_0_NUMBER0='.urlencode($ItemNumber).
				'&AMT='.urlencode($ItemTotalPrice).				
				'&RETURNURL='.urlencode($PayPalReturnURL ).
				'&CANCELURL='.urlencode($PayPalCancelURL);	
				'&SOLUTIONTYPE='.urlencode($sol_type).
				'&LANDINGPAGE='.urlencode($landingPage);
				
		//	echo '<pre>'; print_r($padata);die;
		$paypal= new MyPayPal();
		$httpParsedResponseAr = $paypal->PPHttpPost('SetExpressCheckout', $padata, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode, $sol_type,$landingPage);
		//Respond according to message we receive from Paypal
		if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
		{
			
			$paypalurl ='https://www.'.$PayPalMode.'paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='.$httpParsedResponseAr["TOKEN"].'';
			echo $paypalurl;die;
			return $paypalurl;
			//header('Location: '.$paypalurl);
			
			 
		}else{
			echo '<div style="color:red"><b>Error : </b>'.urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]).'</div>';
			echo '<pre>';
			print_r($httpParsedResponseAr);
			echo '</pre>';
		}
	}
//Paypal redirects back to this page using ReturnURL, We should receive TOKEN and Payer ID
function dopaypalpayment()
{
		$basepath = str_replace('system', 'assets', BASEPATH);
		$filetoinclude = $basepath.'paypal.class.php';
		include_once $filetoinclude;
		$PayPalMode = $this->config->item('PayPalMode');
		$type =  $this->uri->segment(3,0);
		$total_amount = $this->session->userdata('total_amount');
		$amount = $total_amount;
		$PayPalApiUsername = 'nick_api1.whispertip.com';
		$PayPalApiPassword = 'GJU4P9YUM58RP4VW';	
		$PayPalApiSignature = 'AFcWxV21C7fd0v3bYYYRCpSSRl31A9tBpjPyVAzGFQveuo1dVnQRwy8F';	
		$receiver_email = "nick-facilitator@whispertip.com";
		$PayPalCurrencyCode = 'AUD';	
		$token_value = $this->session->userdata('token_value');
		$playerid = $token_value['payer_id'];
		$token = $token_value['token'];
		if(isset($token) && isset($playerid))
		{
			$itemname ="Tips";
			$ItemNumber = 1;
			$itemQty1 = 1;
			$ItemTotalPrice = ($amount*$itemQty1);
			
			$padata = 	'&TOKEN='.urlencode($token).
								'&PAYERID='.urlencode($playerid).
								'&PAYMENTACTION='.urlencode("SALE").
								'&AMT='.urlencode($ItemTotalPrice).
								'&CURRENCYCODE='.urlencode($PayPalCurrencyCode);
			$paypal= new MyPayPal();
			$httpParsedResponseAr = $paypal->PPHttpPost('DoExpressCheckoutPayment', $padata, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode);
			//echo '<pre>';print_r($httpParsedResponseAr);die;
			if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) 
			{
						if('Completed' == $httpParsedResponseAr["PAYMENTSTATUS"] || 'Pending' == $httpParsedResponseAr["PAYMENTSTATUS"])
						{
								$type = 'credit card'; 
								if($httpParsedResponseAr["TRANSACTIONID"]){
								$transactionId = $httpParsedResponseAr["TRANSACTIONID"];
								$this->wallet_model->addInWallet($amount,$transactionId,$type);
								$this->session->set_flashdata('success',"Money add successfully into wallet.");
								redirect(base_url().'home/mywallet/');
							}
							else
							{
								$this->session->set_flashdata('error',"Transcation is pending");
								redirect(base_url().'home/mywallet/');
							}
						}
						else
						{
							$error_message = $httpParsedResponseAr['L_LONGMESSAGE0'];
							$FinalMessage = str_replace("%20",' ',$error_message);
							$full_message = str_replace("%2E",'.',$FinalMessage);
							$this->session->set_flashdata('error',$full_message);
							$this->session->set_flashdata('error',"Some error occured while processing your account");
							redirect(base_url().'home/mywallet/');
						}
					
		} else{
						
						$error_message = $httpParsedResponseAr['L_LONGMESSAGE0'];
						$FinalMessage = str_replace("%20",' ',$error_message);
						$full_message = str_replace("%2E",'.',$FinalMessage);
						$this->session->set_flashdata('error',$full_message);
						redirect(base_url().'home/mywallet/');
					}
		}
	}
}

		


<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/***********************************************************
DoDirectPaymentReceipt.php

Submits a credit card transaction to PayPal using a
DoDirectPayment request.

The code collects transaction parameters from the form
displayed by DoDirectPayment.php then constructs and sends
the DoDirectPayment request string to the PayPal server.
The paymentType variable becomes the PAYMENTACTION parameter
of the request string.

After the PayPal server returns the response, the code
displays the API request and response in the browser.
If the response from PayPal was a success, it displays the
response parameters. If the response was an error, it
displays the errors.

Called by DoDirectPayment.php.

Calls CallerService.php and APIError.php.

***********************************************************/

require_once 'CallerService.php';
class DoDirectPaymentReceipt {
//require_once("../../include/global.inc.php");

//session_start();


/**
 * Get required parameters from the web form for the request
 */
	function DoDirectPaymentReceipt()
		{
			$this->CI =& get_instance();
			$this->CI->load->library('session');
		}

	public function doPayment($userData){
		$paymentType ="Sale";
		$first_name = $userData['first_name'];
		$last_name = $userData['last_name'];
		$email = $userData['email'];
		$amount = $_POST['amount'];
		$ccnumber = $_POST['ccnumber'];
		$cvv = $_POST['cvv'];
		$month = $_POST['month'];
		$year = $_POST['year'];
		$currencycredit = $_POST['currencycredit'];
		$creditCardType =urlencode('Visa');
		$creditCardNumber = urlencode($ccnumber);
		$expDateMonth =urlencode($month);

		// Month must be padded with leading zero
		$padDateMonth = str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
		$expDateYear =urlencode($year);
		$cvv2Number = urlencode($cvv);
		$ipAddress = $_SERVER['REMOTE_ADDR'];
		//$currencyCode=urlencode($_POST['currency']);
		$currencyCode=$currencycredit;
		//$paymentType=urlencode($_POST['paymentType']);

		/* Construct the request string that will be sent to PayPal.
		   The variable $nvpstr contains all the variables and is a
		   name value pair string with & as a delimiter */
		$nvpstr="&PAYMENTACTION=$paymentType&IPADDRESS=$ipAddress&EMAIL=$email&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber&EXPDATE=". $padDateMonth.$expDateYear."&CVV2=$cvv2Number&FIRSTNAME=$first_name&LASTNAME=$lastName".
		"&COUNTRYCODE=GB&CURRENCYCODE=$currencyCode";

		/* Make the API call to PayPal, using API signature.
		   The API response is stored in an associative array called $resArray */
		$resArray=hash_call("DoDirectPayment",$nvpstr);
		//echo"<pre>";
		//print_r($resArray);die;
		/* Display the API response back to the browser.
		   If the response from PayPal was a success, display the response parameters'
		   If the response was an error, display the errors received using APIError.php.
		   */
		  // echo "<pre>";
		  // print_r($resArray);
		  // die();
		//  echo '<pre>';print_r($resArray);die;
		if($resArray=='error6')
		{
			$respArray['L_LONGMESSAGE0'] ='Transaction was unable to process, Please tyr again after some time.';
			return $respArray;
			//die('here11');
		}
		else
		{
			$ack = strtoupper($resArray["ACK"]);
			//print_r($ack);die;
			if($ack!="SUCCESS")  
			{
				$_SESSION['reshash']=$resArray;
			}
		if($ack=="SUCCESS")
			{
				$dataSessVal = array(
				'totalAmount' => $resArray['AMT'],
				'TRANSACTIONID' => $resArray['TRANSACTIONID'],
				);
			return $dataSessVal;
				
			}
			else if($ack=='FAILURE'){
				return $resArray;
			}
		}
		ob_start();
	}
}
?>

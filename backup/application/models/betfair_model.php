<?php

class Betfair_model extends Model
{
	function Betfair_model()
	{
		parent::Model();
		$this->CI = & get_instance();
	}
	
	function getEventTypeEvents($eventtypeId)
	{
		$result2 = array();
		$dateArr = $this->x_week_range(date('Y-m-d'));
		
		$this->db->select("*");
		$this->db->where("event_type_id",$eventtypeId);
		$query = $this->db->get("betfair_events_competitions");
		if($query->num_rows()>0)
		{
			$result = $query->result_array();
			foreach($result as $res)
			{
				$openDate1 = $dateArr[0]." ".date('H:i:s');
				$openDate2 = $dateArr[1]." ".date('H:i:s');
				$this->db->select("*");
				$this->db->where("competition_id",$res['competition_id']);
				$this->db->where("open_date >= ",$openDate1);
				$this->db->where("open_date <= ",$openDate2);
				$query1 = $this->db->get("betfair_events");
				if($query1->num_rows()>0)
				{
					$result1 = $query1->result_array();
					foreach($result1 as $resp1)
					{
						if(strpos($resp1['name'],'@')!==false)
						{
							$explodeName = explode('@',$resp1['name']);
						}
						else
						{
							$explodeName = explode('v',$resp1['name']);
						}
						if(is_array($explodeName) && count($explodeName)==2)
						{
							$result2[] = $resp1;
						}
					}
				}
			
			}
		}
		//echo "<pre>";print_r($result2);die;
		return $result2;
	}
	function getEventTypesCompetitions($eventtypeId)
	{
		$result = array();
		$this->db->select("*");
		$this->db->where("event_type_id",$eventtypeId);
		$query = $this->db->get("betfair_events_competitions");
		if($query->num_rows()>0)
		{
			$result = $query->result_array();
		}
		return $result;
	}
	function getCompetitionById($competition_id)
	{
		$result = array();
		$this->db->select("*");
		$this->db->where("competition_id",$competition_id);
		$query = $this->db->get("betfair_events_competitions");
		if($query->num_rows()>0)
		{
			$result = $query->row_array();
		}
		return $result;
	}
	
	function getEvents($competition_id,$nav_id=null)
	{
		$exArr = explode(',',$competition_id);
		$curDate = date('Y-m-d H:i:s');
		//$dateArr = $this->x_week_range3(date('Y-m-d'));
		//$openDate1 = $dateArr[0]." ".date('H:i:s');
		//$openDate2 = $dateArr[1]." ".date('H:i:s');
		$time_val = $this->session->userdata('timezone');
		$result = array();
		foreach($exArr as $key=>$val){
		$sql = "SELECT * FROM (`betfair_events`)WHERE `competition_id` = $val AND CONVERT(open_date, DATE) >= CONVERT((NOW()-INTERVAL 1 DAY),DATE) ORDER BY `open_date` asc";
		$query = $this->db->query($sql);
	//	echo $this->db->last_query();die;
		if($query->num_rows()>0)
		{
			$result = $query->result_array();
		//	echo '<pre>';print_r($result);die;
			foreach($result as $value)
			{
				$eventDate = $this->convert_time_zone($value['open_date'],$value['timezone'],$time_val);
				if($eventDate >= $curDate) {
					$sql1 = "SELECT * FROM (`betfair_events`)WHERE `competition_id` = $val and event_id = '".$value['event_id']."' ORDER BY `open_date` asc";
					$query1 = $this->db->query($sql1);
					if($query1->num_rows()>0)
					{
						$result1[] = $query1->row_array();
					}
				}	
			}
			//$finalArr = array(0=>$result1);
			return $result1;
			//echo '<pre>';print_r($result1);
			//echo '<pre>';print_r($finalArr);die;
		}
	}
}


function getEventsById($competition_id,$nav_id)
	{
		//echo $competition_id;die;
		$exArr = explode(',',$competition_id);
		//echo '<pre>';print_r($exArr);die;
		$dateArr = $this->x_week_range(date('Y-m-d'));
		$openDate1 = $dateArr[0]." ".date('H:i:s');
		$openDate2 = $dateArr[1]." ".date('H:i:s');
		$result = array();
		foreach($exArr as $key=>$val){
		$sql = "SELECT * FROM (`betfair_events`)WHERE `competition_id` = $val AND CONVERT(open_date, DATE) >= CONVERT((NOW()),DATE)
ORDER BY `open_date` asc";
		$query = $this->db->query($sql);
		if($query->num_rows()>0)
		{
			$result[] = $query->result_array();
		}
		
		//echo $this->db->last_query();echo "##########";
}
		return $result;
}

function getEventInfo($event_id)
	{
		$dateArr = $this->x_week_range(date('Y-m-d'));
		$openDate1 = $dateArr[0]." ".date('H:i:s');
		$openDate2 = $dateArr[1]." ".date('H:i:s');
	//	$result = array();
	$result = '';
		$sql = "SELECT * FROM (`betfair_events`)WHERE `event_id` = $event_id AND CONVERT(open_date, DATE) >= CONVERT((NOW()),DATE)
ORDER BY `open_date` asc";
		$query = $this->db->query($sql);
		if($query->num_rows()>0)
		{
			$result = "Active";
		}
	else
	{
	$result = "Inactive";	
	}
		return $result;
}

	function getEventsBynav($competition_id,$nav_id)
	{
		if($competition_id ==0)
		{
			$navresult = array();
			$this->db->select("*");
			$this->db->where("parent_id",$nav_id);
			$this->db->where("active",1);
			$query = $this->db->get("navigation");
			if($query->num_rows()>0)
			{
				$navresult = $query->result_array();
			}
			//echo '<pre>';print_r($navresult);die;
			return $navresult;
		}
		
	}
	function getEventsBynavComp($competition_id,$nav_id)
	{
		$ExplodeData = explode(',',$competition_id);
			foreach($ExplodeData as $key1) {
				$this->db->select("*");
				$this->db->where("competition_id",$key1);
				$this->db->where("type =",'');
				$query = $this->db->get("navigation");
				if($query->num_rows()>0)
				{
					$navresult[] = $query->result_array();
				}
			}
			//echo "<pre>";print_r($navresult);die;
			return $navresult;
			
		
	}
	
	function getEventsMarkets($status ='' ,$event_id)
	{
		$result = array();
		$sql = "select * from betfair_events_markets where event_id=$event_id and market_id!=''";
		if($status==1){
			$limit_sql = " LIMIT 0,4";
		}else{
			$limit_sql = "";
			}
		$sql_new = $sql.$limit_sql;
		$query = $this->db->query($sql_new);
		//echo $this->db->last_query();die;
		if($query->num_rows()>0)
		{
			$result = $query->result_array();
		}
		//echo '<pre>';print_r($result);die;
		return $result;
	}
	
	function getEventsMarketsCount($status ='' ,$event_id)
	{
		$result = array();
		$sql = "select count(*) from betfair_events_markets where event_id=$event_id and market_id!=''";
		if($status==1){
			$limit_sql = " LIMIT 0,4";
		}else{
			$limit_sql = "";
			}
		$sql_new = $sql.$limit_sql;
		$this->db->where('event_id', $event_id);
		$this->db->from('betfair_events_markets');
		return $this->db->count_all_results();
	}
	function getWinningTeamByMarket($market_id)
	{
		//echo $comp_id.','.$event_id;die;
		$result = array();
		$this->db->select("*");
		$this->db->where("market_id",$market_id);
		$query = $this->db->get("betfair_markets_odds");
		if($query->num_rows()>0)
		{
			$result = $query->result_array();
		}
		//echo $this->db->last_query();die;
		return $result;
	}
	function getEventsName($event_id)
	{
		$result = array();
		$this->db->select("*");
		$this->db->where("event_id",$event_id);
		$query = $this->db->get("betfair_events");
		if($query->num_rows()>0)
		{
			$result = $query->row_array();
		}
		return $result;
	}
	function getuserName($tipsterId)
	{
		$this->db->select("*");
		$this->db->where("userid",$tipsterId);
		$query = $this->db->get("users");
		if($query->num_rows()>0)
		{
			$result = $query->row_array();
		}
		return $result;
	}
	function getEventsMarketsByMarketId($market_id)
	{
		$result = array();
		$this->db->select("*");
		$this->db->where("market_id",$market_id);
		$query = $this->db->get("betfair_events_markets");
		if($query->num_rows()>0)
		{
			$result = $query->row_array();
		}
		return $result;
	}
	function getEventsMarketsNameByMarketId($market_id,$type)
	{
		$result = array();
		$this->db->select("market_name");
		$this->db->where("market_id",$market_id);
		$this->db->where("market_type ",$type);
		$query = $this->db->get("betfair_events_markets");
		if($query->num_rows()>0)
		{
			$result = $query->row_array();
		}
		return $result;
	}
	function getMarketOddsByMarketId($market_id)
	{
		$result = array();
		$this->db->select("*");
		$this->db->distinct();
		$this->db->where("market_id",$market_id);
		$query = $this->db->get("betfair_markets_odds");
		if($query->num_rows()>0)
		{
			$result = $query->result_array();
		}
		return $result;
	}
	function getMarketType($market_id)
	{
		$result = array();
		$this->db->select("market_type,event_id,market_name");
		$this->db->distinct();
		$this->db->where("market_id",$market_id);
		$query = $this->db->get("betfair_events_markets");
		if($query->num_rows()>0)
		{
			$result = $query->row_array();
		}
		return $result;
	}
	function savePlacedTip()
	{
		$eventId = $this->input->post('eventId');
		$marketId = $this->input->post('marketId');
		$selectId = $this->input->post('selectId');
		$competitionId = $this->input->post('competitionId');
		$selectedOdd = $this->input->post('selectedOdd');
		$eventTypeId = $this->input->post('eventTypeId');
		$userId = $this->session->userdata('userid');
		
		$this->db->select('*');
		$this->db->where('event_id',$eventId);
		$this->db->where('userid',$userId);
		$resp = $this->db->get('user_placed_tip');
		if($resp->num_rows()>0)
		{
			return false;
		}
		else
		{
			$arrIns = array('market_id'=>$marketId,
						'event_type_id'=>$eventTypeId,
						'event_id'=>$eventId,
						'competition_id'=>$competitionId,
						'selection_id'=>$selectId,
						'selected_odd'=>$selectedOdd,
						'userid'=>$userId,
						'date_placed'=>date('Y-m-d H:i:s'));
			$this->db->insert('user_placed_tip',$arrIns);
			return true;
		}
		
		
	}
	
	function savePlacedTip_new()
	{
	//	echo '<pre>';print_r($_POST);die;
		$eventId = $this->input->post('eventId');
		$marketId = $this->input->post('marketId');
		$selectId = $this->input->post('selectId');
		$competitionId = $this->input->post('competitionId');
		$selectedOdd = $this->input->post('selectedOdd');
		$eventTypeId = $this->input->post('eventTypeId');
		$teamName = $this->input->post('teamName');
		$eventName = $this->input->post('eventName');
		$Timezone = $this->input->post('Timezone');
		$openDate = $this->input->post('openDate');
		$userId = $this->session->userdata('userid');
		$this->db->select('*');
		$this->db->where('market_id',$marketId);
		$this->db->where('userid',$userId);
		$resp = $this->db->get('user_placed_tip');
		if($resp->num_rows()>0)
		{
			return false;
		}
		else
		{
			$arrIns = array('market_id'=>$marketId,
						'event_type_id'=>$eventTypeId,
						'event_id'=>$eventId,
						'competition_id'=>$competitionId,
						'selection_id'=>$selectId,
						'selected_odd'=>$selectedOdd,
						'team_name'=>$teamName,
						'event_name'=>$eventName,
						'open_date'=>$openDate,
						'timezone'=>$Timezone,
						'userid'=>$userId,
						'date_placed'=>date('Y-m-d H:i:s'));
			$this->db->insert('user_placed_tip',$arrIns);
			return true;
		}
		
		
	}
	function savePlacedTipFreeSell()
	{
		$placed_tip_sess = $this->session->userdata('placed_tip_sess_val');
		$userId = $this->session->userdata('userid');
		$placed_tip_type = $this->input->post('placed_tip_type');
		$eventId = $placed_tip_sess['event_id'];
		$marketId = $placed_tip_sess['marketId'];
		$selectId = $placed_tip_sess['selectId'];
		$competitionId = $placed_tip_sess['competitionId'];
		$selectedOdd = $placed_tip_sess['selectedOdd'];
		$eventTypeId = $placed_tip_sess['eventTypeId'];
		$teamName = $placed_tip_sess['teamName'];
		$eventName =$placed_tip_sess['eventName'];
		$Timezone = $placed_tip_sess['Timezone'];
		$openDate = $placed_tip_sess['openDate'];
		$this->db->select('*');
		$this->db->where('market_id',$marketId);
		$this->db->where('userid',$userId);
		$resp = $this->db->get('user_placed_tip');
		if($resp->num_rows()>0)
		{
			return false;
		}
		else
		{
			$arrIns = array('market_id'=>$marketId,
						'event_type_id'=>$eventTypeId,
						'event_id'=>$eventId,
						'competition_id'=>$competitionId,
						'selection_id'=>$selectId,
						'selected_odd'=>$selectedOdd,
						'team_name'=>$teamName,
						'event_name'=>$eventName,
						'open_date'=>$openDate,
						'timezone'=>$Timezone,
						'userid'=>$userId,
						'tip_type'=>$placed_tip_type,
						'date_placed'=>date('Y-m-d H:i:s'));
			$this->db->insert('user_placed_tip',$arrIns);
			
			$updateIns = array('tip_placed'=>1);
			$this->db->where('event_id',$eventId);
			$this->db->update('betfair_events',$updateIns);
			return true;
		}
		
		
	}
	function checkPlacedTips()
	{
		$eventId = $this->input->post('eventId');
		$marketId = $this->input->post('marketId');
		$selectId = $this->input->post('selectId');
		$competitionId = $this->input->post('competitionId');
		$selectedOdd = $this->input->post('selectedOdd');
		$eventTypeId = $this->input->post('eventTypeId');
		$teamName = $this->input->post('teamName');
		$eventName = $this->input->post('eventName');
		$Timezone = $this->input->post('Timezone');
		$openDate = $this->input->post('openDate');
		$userId = $this->session->userdata('userid');
		$sess_val = array(
		'event_id'=>$eventId,
		'marketId'=>$marketId,
		'competitionId'=>$competitionId,
		'selectId'=>$selectId,
		'selectedOdd'=>$selectedOdd,
		'eventTypeId'=>$eventTypeId,
		'teamName'=>$teamName,
		'eventName'=>$eventName,
		'Timezone'=>$Timezone,
		'openDate'=>$openDate,
		'userId'=>$userId,
		);
		$this->session->set_userdata('placed_tip_sess_val',$sess_val);
		$this->db->select('*');
		$this->db->where('market_id',$marketId);
		$this->db->where('userid',$userId);
		$resp = $this->db->get('user_placed_tip');
		if($resp->num_rows()>0)
		{
			return false;
		}	
		else
		{
			return true;
		}
	}
	function savePurchaseTip($place_id)
	{
		$dateArr = $this->x_week_range2(date('Y-m-d'));
		$eventTypeId = $this->input->post('eventTypeId');
		$tipsterId = $this->input->post('tipsterId');
		$market_id = $this->input->post('market_id');
		$event_id = $this->input->post('event_id');
		$selected_odd = $this->input->post('selected_odd');
		$open_date = $this->input->post('open_date');
		$userId = $this->session->userdata('userid');
		$this->db->select('*');
		$this->db->where('event_type_id',$eventTypeId);
		$this->db->where('tipster_id',$tipsterId);
		$this->db->where('userid',$userId);
		$this->db->where('event_id',$event_id);
		$this->db->where('market_id',$market_id);
//		$this->db->where('date_purchased >= ',$dateArr[0]);
	//	$this->db->where('date_purchased <= ',$dateArr[1]);
		$resp = $this->db->get('user_purchase_tip');
		if($resp->num_rows()>0)
		{
			return false;
		}
		else
		{
			$arrIns = array('tipster_id'=>$tipsterId,
						'userid'=>$userId,
						'event_type_id'=>$eventTypeId,
						'event_id'=>$event_id,
						'market_id'=>$market_id,
						'selected_odd'=>$selected_odd,
						'open_date'=>$open_date,
						'placed_tip_id'=>$place_id,
						'date_purchased'=>date('Y-m-d H:i:s'));
			$this->db->insert('user_purchase_tip',$arrIns);
			$ins = $this->db->insert_id();
			if($ins)
			{
				$tipAmount = $this->config->item('whispertipfee');
				$whispertipcharges = $this->config->item('whispertipcharges');
				$whisperCharges =$whispertipcharges;
				$tipAmountTipster = $tipAmount-$whisperCharges;
				$insTipster = array('userid'=>$tipsterId,
								'amount'=>$tipAmountTipster,
								'transaction_type'=>'credit',
								'transaction_datetime'=>date('Y-m-d H:i:s'),
								'transaction_status'=>1,
								'purchased_tip_id'=>$ins,
								'payment_type'=>'virtual');
				$this->db->insert('user_transactions',$insTipster);			
				$insT = $this->db->insert_id();
				if($insT)
				{
					$sql1 = "select sum(amount) as amount from user_transactions where userid='".$tipsterId."' and transaction_type='credit' and transaction_status=1";
					$query1 = $this->db->query($sql1);
					$res1 = $query1->row_array();
					$amount1 = $res1['amount'];
					
					$sql2 = "select sum(amount) as amount from user_transactions where userid='".$tipsterId."' and transaction_type='debit' and transaction_status=1";
					$query2 = $this->db->query($sql2);
					$res2 = $query2->row_array();
					$amount2 = $res2['amount'];
					
					$balanceAmount = $amount1 - $amount2;
					$updArr1 = array('balance'=>$balanceAmount,
									'last_updated_datetime'=>date('Y-m-d H:i:s'));
					$this->db->where('userid',$tipsterId);
					$this->db->update('user_wallet',$updArr1);
				}
				
				$insUser = array('userid'=>$userId,
								'amount'=>$tipAmount,
								'transaction_type'=>'debit',
								'transaction_datetime'=>date('Y-m-d H:i:s'),
								'transaction_status'=>1,
								'purchased_tip_id'=>$ins,
								'payment_type'=>'virtual');
				$this->db->insert('user_transactions',$insUser);	
				$insU = $this->db->insert_id();
				if($insU)
				{
					$sql1 = "select sum(amount) as amount from user_transactions where userid='".$userId."' and transaction_type='credit' and transaction_status=1 ";
					$query1 = $this->db->query($sql1);
					$res1 = $query1->row_array();
					$amount1 = $res1['amount'];
					
					$sql2 = "select sum(amount) as amount from user_transactions where userid='".$userId."' and transaction_type='debit' and transaction_status=1 ";
					$query2 = $this->db->query($sql2);
					$res2 = $query2->row_array();
					$amount2 = $res2['amount'];
					
					$balanceAmount = $amount1 - $amount2;
					$updArr2 = array('balance'=>$balanceAmount,
									'last_updated_datetime'=>date('Y-m-d H:i:s'));
					$this->db->where('userid',$userId);
					$this->db->update('user_wallet',$updArr2);
				}
				
						
			}
			return true;
		}
		
		
	}
	function savePurchaseTipFree($place_id)
	{
		$dateArr = $this->x_week_range2(date('Y-m-d'));
		$eventTypeId = $this->input->post('eventTypeId');
		$tipsterId = $this->input->post('tipsterId');
		$market_id = $this->input->post('market_id');
		$event_id = $this->input->post('event_id');
		$selected_odd = $this->input->post('selected_odd');
		$open_date = $this->input->post('open_date');
		$userId = $this->session->userdata('userid');
		$this->db->select('*');
		$this->db->where('event_type_id',$eventTypeId);
		$this->db->where('tipster_id',$tipsterId);
		$this->db->where('userid',$userId);
		$this->db->where('event_id',$event_id);
		$this->db->where('market_id',$market_id);
//		$this->db->where('date_purchased >= ',$dateArr[0]);
	//	$this->db->where('date_purchased <= ',$dateArr[1]);
		$resp = $this->db->get('user_purchase_tip');
		if($resp->num_rows()>0)
		{
			return false;
		}
		else
		{
			$arrIns = array('tipster_id'=>$tipsterId,
						'userid'=>$userId,
						'event_type_id'=>$eventTypeId,
						'event_id'=>$event_id,
						'market_id'=>$market_id,
						'selected_odd'=>$selected_odd,
						'open_date'=>$open_date,
						'placed_tip_id'=>$place_id,
						'date_purchased'=>date('Y-m-d H:i:s'));
			$this->db->insert('user_purchase_tip',$arrIns);
			$ins = $this->db->insert_id();
			if($ins)
			{
				return true;
			}
		}
	}
	function one_week_now($date) 
	{
		$ts = strtotime($date);
		$start = (date('w', $ts) == 0) ? $ts : strtotime('today', $ts);
		 return array(date('Y-m-d', $start),
		 date('Y-m-d', strtotime('-1 week', $start)));
		 $openDate1 = $dateArr[0]." ".date('H:i:s');
	}
	function one_year_back_now($date) 
	{
		$ts = strtotime($date);
		$start = (date('w', $ts) == 0) ? $ts : strtotime('today', $ts);
		 return array(date('Y-m-d', $start),
		 date('Y-m-d', strtotime('-1 year', $start)));
		 $openDate1 = $dateArr[0]." ".date('H:i:s');
	}
	function one_month_back_now($date) 
	{
		$ts = strtotime($date);
		$start = (date('w', $ts) == 0) ? $ts : strtotime('today', $ts);
		 return array(date('Y-m-d', $start),
		 date('Y-m-d', strtotime('last month', $start)));
		 $openDate1 = $dateArr[0]." ".date('H:i:s');
	}
	function x_week_range($date) 
	{
		$ts = strtotime($date);
		$start = (date('w', $ts) == 0) ? $ts : strtotime('today', $ts);
		return array(date('Y-m-d', $start),
		 date('Y-m-d', strtotime('next monday', $start)));
	}
	
	function x_week_range3($date) 
	{
		$ts = strtotime($date);
		$start = (date('w', $ts) == 0) ? $ts : strtotime('today', $ts);
		 return array(date('Y-m-d', $start),
		 date('Y-m-d', strtotime('+2 week', $start)));
		 $openDate1 = $dateArr[0]." ".date('H:i:s');
	}
	function x_week_range2($date) 
	{
		$ts = strtotime($date);
		$start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday', $ts);
		return array(date('Y-m-d', $start),
		 date('Y-m-d', strtotime('next monday', $ts)));
	}
	function x_week_range_week_before($date) 
	{
		$ts = strtotime($date);
		$start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday', $ts);
		return array(date('Y-m-d', $start),
		 date('Y-m-d', strtotime('last monday', $start)));
	}
	function getEventTypesCompetitionsByUserId($eventtypeId,$userid)
	{
		$result = array();
		$this->db->select("*");
		$this->db->where("event_type_id",$eventtypeId);
		$query = $this->db->get("betfair_events_competitions");
		if($query->num_rows()>0)
		{
			$result = $query->result_array();
		}
		return $result;
	}
	function getEventsByUserId($userId)
	{
		$dateArr = $this->x_week_range(date('Y-m-d'));
		$data = array();
		$dataNew = array();
		$this->db->select('event_id');
		$this->db->where('userid',$userId);
		$this->db->order_by("date_placed", "asc"); 	
		$query = $this->db->get("user_placed_tip");	
			if($query->num_rows()>0)
			{
				$data = $query->result_array();
			}
			$aa = array();
			foreach($data as $val)
			{
					$aa[] = '"'.$val['event_id'].'"';
			}
			$aa = implode(",",$aa);
			if($aa)
			{
				$queryNew = $this->db->query('SELECT * FROM (betfair_events) WHERE event_id IN('.$aa.') and open_date >="'.$dateArr[0].'" and open_date <="'.$dateArr[1].'"' );	
					echo $this->db->last_query();

					if($query->num_rows()>0)
					{
						return $queryNew->result_array();
						
					}
			
			}
			
	}
	function getEventsByUserIdAndEventType($userId,$eventTypeId)
	{
		$dateArr = $this->x_week_range(date('Y-m-d'));
		$openDate1 = $dateArr[0]." ".date('00:00:00');
		$openDate2 = $dateArr[1]." ".date('H:i:s');
		$data = array();
		/*$dataNew = array();
		$this->db->select('*');
		$this->db->where('userid',$userId);
		$this->db->where('event_type_id',$eventTypeId);
		$this->db->order_by("date_placed", "asc"); 	
		$query = $this->db->get("user_placed_tip");	
			if($query->num_rows()>0)
			{
				$data = $query->result_array();
			}
			//echo $this->db->last_query();die;
			$aa = array();
			foreach($data as $val)
			{
					$aa[] = '"'.$val['market_id'].'"';
			}
			$aa = implode(",",$aa);
			if($aa)
			{
				*/
				$query = "select a.*,b.status from user_placed_tip a,betfair_events_results b where a.userid =$userId and a.event_type_id =$eventTypeId and a.market_id = b.market_id and b.status!='closed' group by b.market_id";
				$queryNew = $this->db->query($query);
				if($queryNew->num_rows()>0)
					{
						return $queryNew->result_array();
						
					}
	}
	
	function getPlacedTip($eventId,$marketId)
	{
		$resultPlaced = '';
		$userId = $this->session->userdata('userid');
		
		$this->db->select('*');
		$this->db->where('event_id',$eventId);
		$this->db->where('market_id',$marketId);
		$this->db->where('userid',$userId);
		$resp = $this->db->get('user_placed_tip');
		if($resp->num_rows()>0)
		{
			$result = $resp->row_array();
			$query = "select * from betfair_events_markets where market_id='".$marketId."' and event_id='".$eventId."' ";
			$resSel = $this->db->query($query);
			if($resSel->num_rows())
			{
				$respSel = $resSel->row_array();
				$team1_id = $respSel['team1_id'];
				$team2_id = $respSel['team2_id'];
				$draw_id  = $respSel['draw_id'];
				if($result['selection_id']==$team1_id)
				{
					$resultPlaced = $respSel['team1_name'];
				}
				if($result['selection_id']==$team2_id)
				{
					$resultPlaced =  $respSel['team2_name'];
				}
				if($result['selection_id']==$draw_id)
				{
					$resultPlaced = $respSel['draw_name'];
				}
			}
		}
		else
		{
			
		}
		return $resultPlaced;
	}
	function getPlacedTipByComp($eventId,$marketId,$userId)
	{
		$resultPlaced = '';
		//$userId = $this->session->userdata('userid');
		$this->db->select('*');
		$this->db->where('market_id',$marketId);
		$this->db->where('event_id',$eventId);
		$this->db->where('userid',$userId);
		$resp = $this->db->get('user_placed_tip');
		if($resp->num_rows()>0)
		{
			$respSel1 = array();
			$respSel1 = $resp->row_array();
			return $respSel1;
		}
		
		
	}
	function getoddbyIds($eventId,$marketId,$userId,$teamid)
	{
			$query = "select team_name,team_odds from  betfair_markets_odds where market_id='".$marketId."' and event_id='".$eventId."' and team_id='".$teamid."' ";
			$resSel = $this->db->query($query);
			if($resSel->num_rows())
			{
				$respSel = $resSel->row_array();
			}
		
		return $respSel;
	}
	function getdistinctoddbyIds($eventId,$marketId,$userId)
	{
			$query = "select team_name,team_odds from  betfair_markets_odds where market_id='".$marketId."' and event_id='".$eventId."'";
			$resSel = $this->db->query($query);
			if($resSel->num_rows())
			{
				$respSel = $resSel->row_array();
				$team_name = $respSel['team_name'];

			}
		
		return $team_name;
	}
	function getTipsterPlacedTip($eventId,$marketId,$tipsterId)
	{
		$resultPlaced = '';
		$userId = $this->session->userdata('userid');
		
		$this->db->select('*');
		$this->db->where('event_id',$eventId);
		$this->db->where('market_id',$marketId);
		$this->db->where('userid',$tipsterId);
		$resp = $this->db->get('user_placed_tip');
		if($resp->num_rows()>0)
		{
			$result = $resp->row_array();
			$query = "select * from betfair_events_markets where market_id='".$marketId."' and event_id='".$eventId."' ";
			$resSel = $this->db->query($query);
			if($resSel->num_rows())
			{
				$respSel = $resSel->row_array();
				$team1_id = $respSel['team1_id'];
				$team2_id = $respSel['team2_id'];
				$draw_id  = $respSel['draw_id'];
				if($result['selection_id']==$team1_id)
				{
					$resultPlaced = $respSel['team1_name'];
				}
				if($result['selection_id']==$team2_id)
				{
					$resultPlaced =  $respSel['team2_name'];
				}
				if($result['selection_id']==$draw_id)
				{
					$resultPlaced = $respSel['draw_name'];
				}
			}
		}
		else
		{
			
		}
		return $resultPlaced;
	}
	function getUserPlacedTips($event_typeId,$userId=0)
	{
		$resultArr1 = array();
		if(!$userId)
		{
			$userId = $this->session->userdata('userid');
		}
		
		$query = "select a.competition_id,a.`market_id` from user_placed_tip a, betfair_events_competitions b where a.userId=$userId and a.competition_id=b.competition_id and b.event_type_id=$event_typeId order by a.date_placed DESC";
				
		$resp = $this->db->query($query);
		if($resp->num_rows()>0)
		{
			$result = $resp->result_array();
			foreach($result as $res)
			{
				$result = $resp->row_array();
				$query = "select distinct a.*,d.selection_id as winner_id from user_placed_tip a,betfair_events_markets c,betfair_events_results d
				where a.competition_id ='".$res['competition_id']."' 
				and a.market_id ='".$res['market_id']."' 
				and userid 	='".$userId."' 
				and c.event_id = a.event_id
				and d.market_id = a.market_id
				and d.status='closed' 
				order by a.date_placed ASC
				";
				$resSel = $this->db->query($query);
				if($resSel->num_rows()>0)
				{
					$resultArr = $resSel->result_array();
					$resultArr1 = array_merge($resultArr1,$resSel->result_array());
						
				}

			}
		}
	
		return $resultArr1;
	}
	
	function getCompetitionId($event_type)
	{
		$event_type = ucwords($event_type);
		$this->db->where("slugUrl",$event_type);
		$res = $this->db->get("categories");
		if($res->num_rows())
		{
			$response = $res->row_array();
			return $response['event_type_id'];
		}
	}
	function getcategoriesById($event_type_id)
	{
		$this->db->where("event_type_id",$event_type_id);
		$res = $this->db->get("categories");
		if($res->num_rows())
		{
			$response = $res->row_array();
			return $response;
		}
	}
	function getResultByMarketId($marketId)
	{
		$this->db->where("market_id",$marketId);
		$res = $this->db->get("betfair_events_results");
		if($res->num_rows())
		{
			$response = $res->row_array();
			return $response;
		}
	}
	function getEventsCompetition($event_type_id)
	{
		$this->db->where("event_type_id",$event_type_id);
		$res = $this->db->get("betfair_events_competitions");
		if($res->num_rows())
		{
			$response = $res->result_array();
			return $response;
		}
	}
	function getuserBetPlacedEvents($event_type_id)
	{
		$result = array();
		$dateArr = $this->x_week_range(date('Y-m-d'));
		$openDate1 = $dateArr[0]." ".date('H:i:s');
		$openDate2 = $dateArr[1]." ".date('H:i:s');
		$query1 = "select usr.userid AS userid,tps.*
				from
				users as usr 
				left join 
				(SELECT distinct(a.userid) as tipster_id,  
						a.first_name,a.twitter_token,a.last_name, a.email, a.gender,
						a.dob, a.facebook_id, a.twitter_id, a.profile_image,a.tw_screen_name, a.via,
						a.active, b.tipsters_statistics_id, b.won_tips, 
						b.loose_tips, b.total_tips, b.strike_rate, 
						b.ave_odds, b.rank,b.profit
				FROM users AS a, tipsters_statistics AS b 
				where 
				a.userid = b.userid
				AND a.active =1
				AND a.verified =1
				AND b.event_type_id=$event_type_id) as tps
				on (usr.userid=tps.tipster_id)
				where
				usr.active=1
				and usr.verified=1
				ORDER BY tps.profit DESC , tps.strike_rate DESC
					";
					/*
		$query1 = "select distinct(a.userid), a.userid, 
						a.first_name, a.last_name, a.email, a.gender,
						a.dob, a.facebook_id, a.twitter_id, a.via,
						a.active, b.tipsters_statistics_id, b.won_tips, 
						b.loose_tips, b.total_tips, b.strike_rate, 
						b.ave_odds, b.rank from 
					users as a left join 
					tipsters_statistics as b on  
					a.userid=b.userid
					where
					a.active=1 and
					a.verified=1 and
					b.event_type_id=$event_type_id
					order by b.won_tips desc , strike_rate desc
					";*/
					
		$resp1 = $this->db->query($query1);
		$result1 = $resp1->result_array();
		//echo $this->db->last_query();die;
	//	echo "<pre>";
	//	print_r($result1);die;
		foreach($result1 as $res11)
		{
			$sqll = "SELECT distinct(a.userid) from user_placed_tip a where a.userid='".$res11['userid']."' and a.event_type_id ='".$event_type_id."'";
			$resp2 = $this->db->query($sqll);
			//echo $this->db->last_query();die;
				if($resp2->num_rows())
				{
					$result2 = $res11;
					$query = "SELECT count(*) as total_placed_tips,u.userid, u.username, u.first_name, u.last_name, u.email, u.gender,u.gender,u.tw_screen_name,u.dob, u.profile_image,a.event_id 
						FROM betfair_events as a,user_placed_tip as b ,users as u
						WHERE a.event_id=b.event_id 
						and u.userid = b.userid
						and b.event_type_id = ".$event_type_id." 
						AND b.userid=".$res11['userid'];
						//echo $this->db->last_query();
						$resp = $this->db->query($query);
						if($resp->num_rows())
						{
							$res = $resp->row_array();
							$res1 = array_merge($result2,$res);
							$result[] = $res1;
							
						}
				}
		}
		return $result;
	}
	
	
	 function getuserBetPlacedEventsByUser($user_id,$event_type_id)
	{
		$result = array();
		$result1 = array();
		$curDate = date('Y-m-d H:i:s');
		//$dateArr = $this->x_week_range3(date('Y-m-d'));
		//$openDate1 = $dateArr[0]." ".date('H:i:s');
		//$openDate2 = $dateArr[1]." ".date('H:i:s');
		$time_val = $this->session->userdata('timezone');
			$query = "SELECT distinct(b.event_type_id),b.open_date,b.placed_tip_id FROM betfair_events as a,user_placed_tip as b WHERE a.event_id=b.event_id and b.open_date >=CONVERT((NOW()-INTERVAL 1 DAY),DATE) and  b.event_type_id='".$event_type_id."' and b.userid='".$user_id."'" ;
			$resp = $this->db->query($query);
			if($resp->num_rows())
			{
				$result = $resp->result_array();
				//echo '<pre>'; print_r($result);die;
				foreach($result as $value)
				{
					$curDate = date('Y-m-d H:i:s');
					$eventDate = $this->convert_time_zone($value['open_date'],$value['timezone'],$time_val);
					if($eventDate >= $curDate) {
					$query1 = "SELECT * FROM betfair_events as a,user_placed_tip as b WHERE a.event_id=b.event_id  and b.event_type_id ='".$event_type_id."' group by b.event_id order by b.open_date ASC";
					$resp1 = $this->db->query($query1);
					if($resp1->num_rows())
					{
						$result1 = $resp1->result_array();
					}
				}
					
				}
			}
			//echo '<pre>'; print_r($result);die;
		return $result1;
	}
	
	function getuserBetPlacedEventsByUser2($user_id,$event_type_id)
	{
		$result = array();
		$curDate = date('Y-m-d H:i:s');
		//$dateArr = $this->x_week_range3(date('Y-m-d'));
	//	$openDate1 = $dateArr[0]." ".date('H:i:s');
	//	$openDate2 = $dateArr[1]." ".date('H:i:s');
		$time_val = $this->session->userdata('timezone');
			$query = "SELECT distinct b.event_type_id FROM betfair_events as a,user_placed_tip as b WHERE a.event_id=b.event_id and b.open_date >=convert(now(),DATE) and  b.event_type_id='".$event_type_id."' and b.userid=".$user_id ;
			$resp = $this->db->query($query);
			if($resp->num_rows())
			{
				$result = $resp->result_array();
			}
		return $result;
	}
	function countTipPlaced($user_id)
	{
		$result = array();
		//$dateArr = $this->x_week_range3(date('Y-m-d'));
		//$openDate1 = $dateArr[0]." ".date('H:i:s');
		//$openDate2 = $dateArr[1]." ".date('H:i:s');
		$time_val = $this->session->userdata('timezone');
			$query = "SELECT distinct b.event_type_id,b.open_date ,b.timezone FROM betfair_events as a,user_placed_tip as b WHERE a.event_id=b.event_id  and b.open_date >=CONVERT((NOW()-INTERVAL 1 DAY),DATE) AND b.userid=".$user_id;
			$resp = $this->db->query($query);
			if($resp->num_rows())
			{
				$result = $resp->result_array();
				foreach($result as $value)
				{
					$date = date('Y-m-d H:i:s');
					$eventDate = $this->convert_time_zone($value['open_date'],$value['timezone'],$time_val);
					if($eventDate>=$date)
					{
							$result1[] = $value;
					}
				}
				
			}
			return $result1;
	}
	function getTipsPlacedByEvents($user_id,$event_id)
	{
		$result = array();
	//	$dateArr = $this->x_week_range3(date('Y-m-d'));
	//	$openDate1 = $dateArr[0]." ".date('H:i:s');
	//	$openDate2 = $dateArr[1]." ".date('H:i:s');
		$time_val = $this->session->userdata('timezone');
			$query = "SELECT b.* FROM betfair_events as a,user_placed_tip as b WHERE a.event_id=b.event_id  and b.event_id='".$event_id."' and b.open_date >=CONVERT((NOW()-INTERVAL 1 DAY),DATE) AND b.userid=".$user_id;
			$resp = $this->db->query($query);
			if($resp->num_rows())
			{
				$result = $resp->result_array();
				foreach($result as $value)
				{
					$date = date('Y-m-d H:i:s');
					$eventDate = $this->convert_time_zone($value['open_date'],$value['timezone'],$time_val);
					if($eventDate>=$date)
					{
							$result1[] = $value;
					}
				}
				
			}
			return $result1;
	}
	function getAllTipsPlacedByUser($user_id,$event_type_id)
	{
		$result = array();
		
		//$dateArr = $this->x_week_range3(date('Y-m-d'));
	//	$openDate1 = $dateArr[0]." ".date('H:i:s');
	//	$openDate2 = $dateArr[1]." ".date('H:i:s');
		$time_val = $this->session->userdata('timezone');
			$query = "SELECT b.* FROM betfair_events as a,user_placed_tip as b WHERE a.event_id=b.event_id and b.event_type_id='".$event_type_id."' and b.open_date >=CONVERT((NOW()-INTERVAL 1 DAY),DATE) and b.userid=".$user_id;
			$resp = $this->db->query($query);
			
			if($resp->num_rows())
			{
				$result = $resp->result_array();
				//echo '<pre>';print_r($result);die;
				foreach($result as $value)
				{
					$date = date('Y-m-d H:i:s');
					$eventDate = $this->convert_time_zone($value['open_date'],$value['timezone'],$time_val);
					if($eventDate>=$date)
					{
							$result1[] = $value;
					}
				}
				
			}
			//echo '<pre>';print_r($result1);die;
			return $result1;
	}
	function getuserBetPlacedEventsByUserBySports($event_id)
	{
		$result = array();
		$date = date('Y-m-d H:i:s');
		//$dateArr = $this->x_week_range3(date('Y-m-d'));
		//$openDate1 = $dateArr[0]." ".date('H:i:s');
		//$openDate2 = $dateArr[1]." ".date('H:i:s');
		$time_val = $this->session->userdata('timezone'); 
			$query = "SELECT * FROM betfair_events as a,user_placed_tip as b WHERE a.event_id=b.event_id  and b.event_type_id =$event_id and b.open_date >=CONVERT((NOW()-INTERVAL 1 DAY),DATE) group by b.event_id order by b.open_date ASC";
			$resp = $this->db->query($query);
			if($resp->num_rows())
			{
				$result = $resp->result_array();
				foreach($result as $value)
				{
					$eventDate = $this->convert_time_zone($value['open_date'],$value['timezone'],$time_val);
					if($eventDate>=$date)
					{
							$result1[] = $value;
					}
					
				}
				
			}
		return $result1;
	}
	function getuserBetPlacedEventsByUserSports($event_id,$user_id)
	{
		$result = array();
		$date = date('Y-m-d H:i:s');
		//$dateArr = $this->x_week_range3(date('Y-m-d'));
		//$openDate1 = $dateArr[0]." ".date('H:i:s');
		//$openDate2 = $dateArr[1]." ".date('H:i:s');
		$time_val = $this->session->userdata('timezone');
			$query = "SELECT a.*,b.* FROM betfair_events as a,user_placed_tip as b WHERE a.event_id=b.event_id and  b.userid='".$user_id."' and b.event_type_id =$event_id and b.open_date >=convert(now(),DATE) group by b.event_id order by b.open_date ASC";
			$resp = $this->db->query($query);
			if($resp->num_rows())
			{
				$result = $resp->result_array();
				foreach($result as $value)
				{
					$eventDate = $this->convert_time_zone($value['open_date'],$value['timezone'],$time_val);
					if($eventDate>=$date)
					{
							$result1[] = $value;
					}
					
				}
				
			}
		return $result1;
	}
	function listuserBetPlacedEvents($competition_id,$userid)
	{
		$result = array();
		$dateArr = $this->x_week_range(date('Y-m-d'));
		
		$query = "SELECT *
				FROM betfair_events as a,user_placed_tip as b
				WHERE a.event_id=b.event_id 
				and b.userid = ".$userid."
				and b.competition_id = ".$competition_id." 
				AND a.open_date >= '".$dateArr[0]."' 
				AND a.open_date <= '".$dateArr[1]."' ";
				//echo $this->db->last_query();
		$resp = $this->db->query($query);
		if($resp->num_rows())
		{
			$result = $resp->result_array();
		}
		return $result;
	}
	
	function listPurchasedTip($userId,$tipsterId)
	{
		$result1 = array();
		$dateArr = $this->x_week_range2(date('Y-m-d'));
		$openDate1 = $dateArr[0]." ".date('H:i:s');
		$openDate2 = $dateArr[1]." ".date('H:i:s');
		$query = "SELECT *
				FROM  user_purchase_tip
				WHERE userid =".$userId."
				AND tipster_id =".$tipsterId."	
				AND date_purchased >= '".$openDate1."' 
				AND date_purchased <= '".$openDate2."' ";
		$resp = $this->db->query($query);
		$dateArr1 = $this->x_week_range(date('Y-m-d'));
		if($resp->num_rows())
		{
			$result = $resp->result_array();
			foreach($result as $res)
			{
				$this->db->select("*");
				$this->db->where("event_type_id",$res['event_type_id']);
				$queryComp = $this->db->get("betfair_events_competitions");
				if($queryComp->num_rows())
				{
					$resultComp = $queryComp->result_array();
					foreach($resultComp as $resComp)
					{
						$this->db->select("*");
						$this->db->where("competition_id",$resComp['competition_id']);
						$this->db->where("userid",$res['tipster_id']);
						$this->db->where("date_placed >=",$openDate1);
						$this->db->where("date_placed <=",$openDate2);
						$query1 = $this->db->get("user_placed_tip");
						//echo $this->db->last_query();die;
						if($query1->num_rows())
						{
							$result1 = array_merge($result1,$query1->result_array());
							//$result1[] = $query1->result_array();
						}
					}
					//die;
				}
			}
		}
		//echo "<pre>";print_r($result1);die;
		return $result1;
	}
	function listPurchasedTipBySport($userId,$tipsterId,$event_type_id)
	{
		$result1 = array();
		$dateArr = $this->x_week_range2(date('Y-m-d'));
		//$dateArr1 = $this->x_week_range3(date('Y-m-d'));
		$openDate1 = $dateArr[0]." ".date('H:i:s');
		$openDate2 = $dateArr[1]." ".date('H:i:s');
		//$openDateWeek1 = $dateArr1[0]." ".date('H:i:s');
		//$openDateWeek2 = $dateArr1[1]." ".date('H:i:s');
		$query = "SELECT *
				FROM  user_purchase_tip
				WHERE userid =".$userId."
				AND tipster_id =".$tipsterId."	
				AND event_type_id =".$event_type_id."	
				AND open_date >= '".$openDate1."' 
				AND open_date < '".$openDate2."' ";
		$resp = $this->db->query($query);
		$dateArr1 = $this->x_week_range(date('Y-m-d'));
		if($resp->num_rows())
		{
			$result = $resp->result_array();
			foreach($result as $res)
			{
				$this->db->select("*");
				$this->db->where("event_type_id",$res['event_type_id']);
				$queryComp = $this->db->get("betfair_events_competitions");
				if($queryComp->num_rows())
				{
					$resultComp = $queryComp->result_array();
					foreach($resultComp as $resComp)
					{
						$this->db->select("*");
						$this->db->where("competition_id",$resComp['competition_id']);
						$this->db->where("userid",$tipsterId);
						$this->db->where("event_id",$res['event_id']);
						$this->db->where("market_id",$res['market_id']);
						$query1 = $this->db->get("user_placed_tip");
						if($query1->num_rows())
						{
							$result1 = array_merge($result1,$query1->result_array());
							//$result1[] = $query1->result_array();
						}
					}
					//die;
				}
			}
		}
		//echo "<pre>";print_r($result1);die;
		return $result1;
	}
	function listFutureTipBySport($userId,$tipsterId,$event_type_id)
	{
		$result1 = array();
		$dateArr = $this->x_week_range2(date('Y-m-d'));
		//$dateArr1 = $this->x_week_range3(date('Y-m-d'));
		$openDate1 = $dateArr[0]." ".date('H:i:s');
		$openDate2 = $dateArr[1]." ".date('H:i:s');
		//$openDateWeek1 = $dateArr1[0]." ".date('H:i:s');
		//$openDateWeek2 = $dateArr1[1]." ".date('H:i:s');
		$query = "SELECT *
				FROM  user_purchase_tip
				WHERE userid =".$userId."
				AND tipster_id =".$tipsterId."	
				AND event_type_id =".$event_type_id."	
				AND open_date >= '".$openDate2."'";
		$resp = $this->db->query($query);
		$dateArr1 = $this->x_week_range(date('Y-m-d'));
		if($resp->num_rows())
		{
			$result = $resp->result_array();
			foreach($result as $res)
			{
				$this->db->select("*");
				$this->db->where("event_type_id",$res['event_type_id']);
				$queryComp = $this->db->get("betfair_events_competitions");
				if($queryComp->num_rows())
				{
					$resultComp = $queryComp->result_array();
					foreach($resultComp as $resComp)
					{
						$this->db->select("*");
						$this->db->where("competition_id",$resComp['competition_id']);
						$this->db->where("userid",$tipsterId);
						$this->db->where("event_id",$res['event_id']);
						$this->db->where("market_id",$res['market_id']);
						$query1 = $this->db->get("user_placed_tip");
						if($query1->num_rows())
						{
							$result1 = array_merge($result1,$query1->result_array());
							//$result1[] = $query1->result_array();
						}
					}
					//die;
				}
			}
		}
		//echo "<pre>";print_r($result1);die;
		return $result1;
	}
	function listSportByPurchased($userId,$tipsterId,$val)
	{
		$result1 = array();
		$dateArr = $this->x_week_range2(date('Y-m-d'));
		$openDate1 = $dateArr[0]." ".date('H:i:s');
		$openDate2 = $dateArr[1]." ".date('H:i:s'); 
		//echo $openDate1.'####'.$openDate2;die;
		if($val=='current')
		{
			$value = "AND open_date > '". $openDate1."' 
			AND open_date < '".$openDate2."'";
		}
		else if($val=='past')
		{
			$value = "AND open_date <= '". $openDate1."'";
		}
		else
		{
			$value = "AND open_date >= '". $openDate2."'";
		}
		$query = "SELECT *
				FROM  user_purchase_tip
				WHERE userid =".$userId."
				AND tipster_id =".$tipsterId." ".$value."";
		$resp = $this->db->query($query);
	//	echo $this->db->last_query();die;
		$dateArr1 = $this->x_week_range(date('Y-m-d'));
		if($resp->num_rows())
		{
			$result = $resp->result_array();
			//echo '<pre>';print_r($result);die;
			foreach($result as $res)
			{
				$this->db->select("*");
				$this->db->where("event_type_id",$res['event_type_id']);
				$queryComp = $this->db->get("betfair_events_competitions");
				if($queryComp->num_rows())
				{
					$resultComp = $queryComp->result_array();
					foreach($resultComp as $resComp)
					{
						$this->db->select("*");
						$this->db->where("competition_id",$resComp['competition_id']);
						$this->db->where("userid",$tipsterId);
						$this->db->where("event_id",$res['event_id']);
						$this->db->where("market_id",$res['market_id']);
						$query1 = $this->db->get("user_placed_tip");
						if($query1->num_rows())
						{
							
							$result1 = array_merge($result1,$query1->result_array());
							
						}
					}
				}
			}
		}	//echo "<pre>";print_r($result1);die;
					$tmp = array();
					foreach ($result1 as $item) {
						if (!in_array($item['event_type_id'], $tmp)) {
							$unique[] = $item;
							$tmp[] = $item['event_type_id'];
						}
					}
					//echo "<pre>";print_r($tmp);die;
		//echo "<pre>";print_r($result1);die;
		return $tmp;
	}
	/*function listPastSportByPurchased($userId,$tipsterId)
	{
		$result = array();
		$result1 = array();
		$data1 = array();
		$dateArr = $this->x_week_range2(date('Y-m-d'));
		$openDate1 = $dateArr[0]." ".date('H:i:s');
		$openDate2 = $dateArr[1]." ".date('H:i:s');
		$query = "SELECT *
				FROM  user_purchase_tip
				WHERE userid =".$userId." AND 
				tipster_id =".$tipsterId."
				AND date_purchased <= '".$openDate1."'";
		$resp = $this->db->query($query);
		if($resp->num_rows())
		{
			$result = $resp->result_array();
			//echo '<pre>';print_r($result);die;
			foreach($result as $res)
			{
				$this->db->select("*");
				$this->db->where("event_type_id",$res['event_type_id']);
				$queryComp = $this->db->get("betfair_events_competitions");
				if($queryComp->num_rows())
				{
					$resultComp = $queryComp->result_array();
					foreach($resultComp as $resComp)
					{
						$this->db->select("*");
						$this->db->where("competition_id",$resComp['competition_id']);
						$this->db->where("userid",$tipsterId);
						$this->db->where("event_id",$res['event_id']);
						$this->db->where("market_id",$res['market_id']);
						$query1 = $this->db->get("user_placed_tip");
						if($query1->num_rows())
						{
							//echo '<pre>';print_r($query1->result_array());die;
							$result1 = array_merge($result1,$query1->result_array());
							
						}
					}
					echo '<pre>';print_r($result1);die;
				}
			}
		}
		echo '<pre>';print_r($result1);die;
					$tmp = array();
					foreach ($result1 as $item) {
						if (!in_array($item['event_type_id'], $tmp)) {
							$unique[] = $item;
							$tmp[] = $item['event_type_id'];
						}
					}
		return $tmp;
	}
	*/
	function listPastPurchasedTip($userId,$tipsterId,$event_type_id)
	{
		$result1 = array();
		$dateArr = $this->x_week_range2(date('Y-m-d'));
		$openDate1 = $dateArr[0]." ".date('H:i:s');
	//	echo $openDate1;die;
		$openDate2 = $dateArr[1]." ".date('H:i:s');
		$query = "SELECT *
				FROM  user_purchase_tip
				WHERE userid =".$userId."
				AND event_type_id  =".$event_type_id	."
				AND tipster_id =".$tipsterId."	
				AND open_date <= '".$openDate1."'";
		$resp = $this->db->query($query);
		$dateArr1 = $this->x_week_range(date('Y-m-d'));
		if($resp->num_rows())
		{
			$result = $resp->result_array();
			foreach($result as $res)
			{ 
				//$dateArr = $this->x_week_range2($res['date_purchased']);
			//	$openDate11 = $dateArr[0]." ".date('H:i:s');
			//	$openDate22 = $dateArr[1]." ".date('H:i:s');
				$this->db->select("*");
				$this->db->where("event_type_id",$res['event_type_id']);
				$queryComp = $this->db->get("betfair_events_competitions");
				if($queryComp->num_rows())
				{
					$resultComp = $queryComp->result_array();
					foreach($resultComp as $resComp)
					{
						$this->db->select("*");
						$this->db->where("competition_id",$resComp['competition_id']);
						$this->db->where("userid",$res['tipster_id']);
						$this->db->where("event_id",$res['event_id']);
						$this->db->where("market_id",$res['market_id']);
						$query1 = $this->db->get("user_placed_tip");
						//echo $this->db->last_query();die;
						if($query1->num_rows())
						{
							$result1 = array_merge($result1,$query1->result_array());
							//$result1[] = $query1->result_array();
						}
					}
					//die;
				}
			}
		}
		//echo "<pre>";print_r($result1);die;
		return $result1;
	}
	function listPurchasedTip_new($userId,$tipsterId)
	{
		$result1 = array();
		$dateArr = $this->x_week_range2(date('Y-m-d'));
		$openDate1 = $dateArr[0]." ".date('H:i:s');
		$openDate2 = $dateArr[1]." ".date('H:i:s');
		$query = "SELECT *
				FROM  user_purchase_tip
				WHERE userid =".$userId."
				AND tipster_id =".$tipsterId."
				AND date_purchased >= '".$openDate1."' 
				AND date_purchased <= '".$openDate2."'";
		$resp = $this->db->query($query);
		$dateArr1 = $this->x_week_range(date('Y-m-d'));
		if($resp->num_rows())
		{
			$result = $resp->result_array();
			foreach($result as $res)
			{
				$this->db->select("*");
				$this->db->where("event_type_id",$res['event_type_id']);
				$queryComp = $this->db->get("betfair_events_competitions");
				if($queryComp->num_rows())
				{
					$resultComp = $queryComp->result_array();
					foreach($resultComp as $resComp)
					{
						$this->db->select("*");
						$this->db->where("competition_id",$resComp['competition_id']);
						$this->db->where("userid",$res['tipster_id']);
						$query1 = $this->db->get("user_placed_tip");
						if($query1->num_rows())
						{
							$result1 = array_merge($result1,$query1->result_array());
							//$result1[] = $query1->result_array();
						}
						
					}
					
				}
			}
		}
		return $result1;
	}
	function listPurchasedTipUsers($userId,$tipsterId)
	{
		
		$result3 = array();
		$dateArr = $this->x_week_range2(date('Y-m-d'));
		
		$query = "SELECT *
				FROM  user_purchase_tip
				WHERE userid =".$userId."
				AND tipster_id =".$tipsterId."	
				AND date_purchased >= '".$dateArr[0]."' 
				AND date_purchased <= '".$dateArr[1]."' ";
		$resp = $this->db->query($query);
		
		$dateArr1 = $this->x_week_range(date('Y-m-d'));
		if($resp->num_rows())
		{
			$result = $resp->result_array();
			foreach($result as $res)
			{
				$this->db->select("*");
				$this->db->where("event_type_id",$res['event_type_id']);
				$queryComp = $this->db->get("betfair_events_competitions");
				if($queryComp->num_rows())
				{
					$resultComp = $queryComp->result_array();
					foreach($resultComp as $resComp)
					{
						$this->db->select("*");
						$this->db->where("competition_id",$resComp['competition_id']);
						$this->db->where("userid",$res['tipster_id']);
						//$this->db->where("date_placed >=",$dateArr1[0]);
						//$this->db->where("date_placed <=",$dateArr1[1]);
						$query1 = $this->db->get("user_placed_tip");
						//echo $this->db->last_query();
						if($query1->num_rows())
						{
							$result2 = $query1->result_array();
							foreach($result2 as $res2)
							{
								$respA = $this->betfair_model->getEventById($res2['event_id']);
								if(count($respA))
								{
									if(strpos($respA['name'],'@')!==false)
									{
										$explodeName = explode('@',$respA['name']);
									}
									else
									{
										$explodeName = explode('v',$respA['name']);
									}
									if(is_array($explodeName) && count($explodeName	)==2)
									{
										$result3[] = $this->betfair_model->getEventById($res2['event_id']);
									}
								}
							}
						}
					}
				}
			}
		}
		return $result3;
	}
	
	function getUserPurchasedTispstersId($userId)
	{
		
		$result = array();
		$data1 = array();
		$dateArr = $this->x_week_range2(date('Y-m-d'));
		$openDate1 = $dateArr[0]." ".date('H:i:s');
		$openDate2 = $dateArr[1]." ".date('H:i:s');
		//echo $openDate1.'######'.$openDate2;die;
		$query = "SELECT distinct(tipster_id)
				FROM  user_purchase_tip
				WHERE userid =".$userId."
				AND open_date > '".$openDate1."' 
				AND open_date < '".$openDate2."' ";
		$resp = $this->db->query($query);
		//echo $this->db->last_query();
		if($resp->num_rows())
		{
			$ii=0;
			$result =  $resp->result_array();
			//echo '<pre>';print_r($result);die;
			foreach($result as $res)
			{
				$data = $this->betfair_model->listPurchasedTipUsers($userId,$res['tipster_id']);
				//echo '<pre>';print_r($data);die;
				$memberDetail = $this->member_model->getMemberDetailsById($res['tipster_id']);
				if((strstr($memberDetail['profile_image'],'http://')==$memberDetail['profile_image'] || strstr($memberDetail['profile_image'],'https://')==$memberDetail['profile_image']) && $memberDetail['profile_image']!='')
				{
					if(strstr($memberDetail['profile_image'],'https://graph.facebook.com')==$memberDetail['profile_image'])
					{
						$result[$ii]['image'] = str_replace('normal','large',$memberDetail['profile_image']);
						$result[$ii]['image_margin'] = '42px';
					}
					else
					{
						$result[$ii]['image'] = str_replace('_normal','',$memberDetail['profile_image']);
						$result[$ii]['image_margin'] = '42px;';
					}
				}
				else
				{
					if($memberDetail['profile_image']!='' && file_exists('uploads/profile_picture/thumb_'.$memberDetail['profile_image']))
					{
						$result[$ii]['image'] = base_url().'uploads/profile_picture/small_'.$memberDetail['profile_image'];
						$result[$ii]['image_margin'] = '0px;';
					}
					else
					{
						$result[$ii]['image'] = base_url().'assets/images/small_no_profile_pic.jpg';
						$result[$ii]['image_margin'] = '0px;';
					}
				}
				$result[$ii]['user_id'] = $memberDetail['userid'];
				$result[$ii]['firstname'] = $memberDetail['first_name'];
				$result[$ii]['lastname'] = $memberDetail['last_name'];
				$result[$ii]['total_placed_tips'] = count($data);
				$ii++;
			}
		}
		$data1['total'] = count($result);
		$data1['result'] = $result;
		return $data1;
	}
	
	function getTipersterPurchasedList($userId)
	{
		$result = array();
		$data1 = array();
		$dateArr = $this->x_week_range2(date('Y-m-d'));
		$openDate1 = $dateArr[0]." ".date('H:i:s');
		$openDate2 = $dateArr[1]." ".date('H:i:s');
		$query = "SELECT distinct(tipster_id)
				FROM  user_purchase_tip
				WHERE userid =".$userId."
				AND open_date <= '".$openDate1."'";
		$resp = $this->db->query($query);
		if($resp->num_rows())
		{
			$ii=0;
			$result =  $resp->result_array();
			foreach($result as $res)
			{
				$data = $this->betfair_model->listPurchasedTipUsers($userId,$res['tipster_id']);
				//echo '<pre>';print_r($data);die;
				$memberDetail = $this->member_model->getMemberDetailsById($res['tipster_id']);
				if((strstr($memberDetail['profile_image'],'http://')==$memberDetail['profile_image'] || strstr($memberDetail['profile_image'],'https://')==$memberDetail['profile_image']) && $memberDetail['profile_image']!='')
				{
					if(strstr($memberDetail['profile_image'],'https://graph.facebook.com')==$memberDetail['profile_image'])
					{
						$result[$ii]['image'] = str_replace('normal','large',$memberDetail['profile_image']);
						$result[$ii]['image_margin'] = '42px';
					}
					else
					{
						$result[$ii]['image'] = str_replace('_normal','',$memberDetail['profile_image']);
						$result[$ii]['image_margin'] = '42px;';
					}
				}
				else
				{
					if($memberDetail['profile_image']!='' && file_exists('uploads/profile_picture/thumb_'.$memberDetail['profile_image']))
					{
						$result[$ii]['image'] = base_url().'uploads/profile_picture/small_'.$memberDetail['profile_image'];
						$result[$ii]['image_margin'] = '0px;';
					}
					else
					{
						$result[$ii]['image'] = base_url().'assets/images/small_no_profile_pic.jpg';
						$result[$ii]['image_margin'] = '0px;';
					}
				}
				$result[$ii]['user_id'] = $memberDetail['userid'];
				$result[$ii]['firstname'] = $memberDetail['first_name'];
				$result[$ii]['lastname'] = $memberDetail['last_name'];
				$result[$ii]['total_placed_tips'] = count($data);
				$ii++;
			}
		}
		$data1['total'] = count($result);
		$data1['result'] = $result;
		return $data1;
	}
	function getFuturePurchasedTisps($userId)
	{
		$result = array();
		$data1 = array();
		$dateArr = $this->x_week_range2(date('Y-m-d'));
		$openDate1 = $dateArr[0]." ".date('H:i:s');
		$openDate2 = $dateArr[1]." ".date('H:i:s');
		$query = "SELECT distinct(tipster_id) 	
				FROM  user_purchase_tip
				WHERE userid =".$userId."
				AND open_date >= '".$openDate2."'";
		$resp = $this->db->query($query);
		if($resp->num_rows())
		{
			$ii=0;
			$result =  $resp->result_array();
			foreach($result as $res)
			{
				$data = $this->betfair_model->listPurchasedTipUsers($userId,$res['tipster_id']);
				//echo '<pre>';print_r($data);die;
				$memberDetail = $this->member_model->getMemberDetailsById($res['tipster_id']);
				if((strstr($memberDetail['profile_image'],'http://')==$memberDetail['profile_image'] || strstr($memberDetail['profile_image'],'https://')==$memberDetail['profile_image']) && $memberDetail['profile_image']!='')
				{
					if(strstr($memberDetail['profile_image'],'https://graph.facebook.com')==$memberDetail['profile_image'])
					{
						$result[$ii]['image'] = str_replace('normal','large',$memberDetail['profile_image']);
						$result[$ii]['image_margin'] = '42px';
					}
					else
					{
						$result[$ii]['image'] = str_replace('_normal','',$memberDetail['profile_image']);
						$result[$ii]['image_margin'] = '42px;';
					}
				}
				else
				{
					if($memberDetail['profile_image']!='' && file_exists('uploads/profile_picture/thumb_'.$memberDetail['profile_image']))
					{
						$result[$ii]['image'] = base_url().'uploads/profile_picture/small_'.$memberDetail['profile_image'];
						$result[$ii]['image_margin'] = '0px;';
					}
					else
					{
						$result[$ii]['image'] = base_url().'assets/images/small_no_profile_pic.jpg';
						$result[$ii]['image_margin'] = '0px;';
					}
				}
				$result[$ii]['user_id'] = $memberDetail['userid'];
				$result[$ii]['firstname'] = $memberDetail['first_name'];
				$result[$ii]['lastname'] = $memberDetail['last_name'];
				$result[$ii]['total_placed_tips'] = count($data);
				$ii++;
			}
		}
		$data1['total'] = count($result);
		$data1['result'] = $result;
		return $data1;
	}
	function getUserPlacedTipsEventsId($userId)
	{
		//echo $userId;
		$result = array();
		$result2 = array();
		$dateArr = $this->x_week_range(date('Y-m-d'));
		$openDate1 = $dateArr[0]." ".date('00:00:00');
		$openDate2 = $dateArr[1]." ".date('H:i:s');
		$catQuery = "select * from categories where active=1";
		$catres = $this->db->query($catQuery);
		if($catres->num_rows())
		{
			$ii=0;
			$catresp = $catres->result_array();
			foreach($catresp as $cat)
			{
				$result1 = array();
				$query = "SELECT * FROM betfair_events_competitions where event_type_id=".$cat['event_type_id'];
				$resp = $this->db->query($query);
				if($resp->num_rows())
				{
					$result = $resp->result_array();
					foreach($result as $res)
					{
						$query1 = "select a.*,b.status from user_placed_tip a,betfair_events_results b where a.userid =$userId AND a.competition_id = ".$res['competition_id']." and a.market_id = b.market_id and b.status!='closed' order by a.event_id";
						$resp1 = $this->db->query($query1);
						if($resp1->num_rows())
						{
							$result1 =  array_merge($result1,$resp1->result_array());
							$result2[$ii]['category_id'] = $cat['name'];
							$result2[$ii]['name'] = $cat['name'];
							$result2[$ii]['description'] = $cat['description'];
							$result2[$ii]['slugurl'] = $cat['name'];
							$result2[$ii]['image'] = base_url().'uploads/category_images/thumb_'.$cat['image'];
							$result2[$ii]['event_type_id'] = $cat['event_type_id'];
							$result2[$ii]['tips_placed'] = count($result1);
						}
					}
					if(isset($result2[$ii]))
					{
						$ii++;
					}
				}
				
			}
		}
		//echo $this->db->last_query();die;
				
		//echo  "<pre>";
	//	print_r($result1);
		$data1['total'] = count($result2);
		$data1['result'] = $result2;
		//die();
		return $data1;
	}
	
	function getEventById($eventId)
	{
		$dateArr = $this->x_week_range(date('Y-m-d'));
		$result = array();
		$this->db->select("*");
		$this->db->where("event_id",$eventId);
	//	$this->db->where("open_date >= ",$dateArr[0]);
		//$this->db->where("open_date <= ",$dateArr[1]);
		$query = $this->db->get("betfair_events");
		if($query->num_rows()>0)
		{
			$result = $query->row_array();
		}
		return $result;
	}
	
	function getUserAllPlacedTips($userId)
	{
		$resultArr = array();
		$query = "select distinct placed_tip_id,a.selection_id as placed,b.selection_id as result
				from 
				user_placed_tip a,betfair_events_results b 
				where a.userid = $userId 
				and a.market_id=b.market_id
				and b.status='closed'
				order by a.date_placed desc";
		$resSel = $this->db->query($query);
		if($resSel->num_rows()>0)
		{
			$resultArr = $resSel->result_array();
		}
		return $resultArr;
	}
	function getUserAllPlacedTipsEventType($userId,$eventTypeId)
	{
		$resultArr = array();
		$query = "select distinct placed_tip_id,a.selection_id as placed,b.selection_id as result
				from 
				user_placed_tip a,betfair_events_results b 
				where a.userid = $userId 
				and a.event_type_id =$eventTypeId
				and a.market_id=b.market_id
				and b.status='closed'
				order by a.date_placed desc";
		$resSel = $this->db->query($query);
		if($resSel->num_rows()>0)
		{
			$resultArr = $resSel->result_array();
		}
		return $resultArr;
	}
	
	function getUserStatistics($userId)
	{
		$resultArr = array();
		$query = "select * from tipsters_statistics where userid=$userId";
		$resSel = $this->db->query($query);
		if($resSel->num_rows()>0)
		{
			$resultArr = $resSel->row_array();
		}
		return $resultArr;
	}
	function getUserStatisticsEventType($userId,$eventTypeId)
	{
		$resultArr = array();
		$query = "select * from tipsters_statistics where userid=$userId and event_type_id=$eventTypeId";
		$resSel = $this->db->query($query);
		if($resSel->num_rows()>0)
		{
			$resultArr = $resSel->row_array();
		}
		return $resultArr;
	}
	
	function getUserOddsBets($userId,$eventTypeId)
	{
		//echo $userId.','.$eventTypeId;
		$this->db->select('sum(selected_odd) as total, count(*) as number_records');
		$this->db->where('userId',$userId);
		if($eventTypeId)
		{
			$this->db->where('event_type_id',$eventTypeId);
		}
		$this->db->where('status',1);	
		$query1 = $this->db->get('user_placed_tip');
		if($query1->num_rows())
		{
			$res1 = $query1->row_array();
			$winning_odds = ($res1['total']*100);
		}
		else
		{
			$winning_odds = 0;
		}
		
		$this->db->select('sum(selected_odd) as total,count(*) as number_records');
		$this->db->where('userId',$userId);
		if($eventTypeId)
		{
			$this->db->where('event_type_id',$eventTypeId);
		}
		$this->db->where('status',0);
		$query2 = $this->db->get('user_placed_tip');
		
		if($query2->num_rows())
		{
			$res2 = $query2->row_array();
			$loosing_odds = $res2['number_records']*100;
		}
		else
		{
			$loosing_odds = 0;
		}
		$total_odds = $winning_odds - $loosing_odds;
		$total_odds = $winning_odds ;
		return $total_odds;
	}
	function getTotalWonloss($userId,$eventTypeId)
	{
		$result =array();
		$this->db->select('count(*) as total_Won');
		$this->db->where('userId',$userId);
		if($eventTypeId)
		{
			$this->db->where('event_type_id',$eventTypeId);
		}
		$this->db->where('status',1);
		$query1 = $this->db->get('user_placed_tip');
		if($query1->num_rows())
		{
			$res1 = $query1->row_array();
			$won =$res1['total_Won'];
		}
		$this->db->select('count(*) as total_placed');
		$this->db->where('userId',$userId);
		if($eventTypeId)
		{
			$this->db->where('event_type_id',$eventTypeId);
		}
		$this->db->where('stats_done',1);
		$query2 = $this->db->get('user_placed_tip');
		if($query2->num_rows())
		{
			$res2 = $query2->row_array();
			$loss = $res2['total_placed'];
		}
		$result = array('total_win'=>$won,'total_placed_tip'=>$loss);
		return $result;
	}
	
	function getUserStatisticsForProfile($userId,$event_type_id)
	{
		$query1 = "select usr.userid AS userid,tps.*
				from
				users as usr 
				left join 
				(SELECT distinct(a.userid) as tipster_id,  
						a.first_name,a.tw_screen_name, a.last_name, a.email, a.gender,
						a.dob, a.facebook_id, a.twitter_id, a.via,
						a.active, b.tipsters_statistics_id, b.won_tips,b.profit, 
						b.loose_tips, b.total_tips, b.strike_rate, 
						b.ave_odds, b.rank
				FROM users AS a, tipsters_statistics AS b 
				where 
				a.userid = b.userid
				AND a.active =1
				AND a.verified =1
				AND b.event_type_id=$event_type_id) as tps
				on (usr.userid=tps.tipster_id)
				where
				usr.active=1
				and usr.verified=1
				ORDER BY tps.won_tips DESC , tps.strike_rate DESC
					";
		$resp1 = $this->db->query($query1);
		$result1 = $resp1->result_array();
		$ii=1;
		foreach($result1 as $resp1)
		{
			if($resp1['userid']==$userId)
			{
				$resp1['user_rank']=$ii;
				return $resp1;
			}
			$ii++;
		}
	}
	
	//$time="2014-01-10 13:36:00";
	//$newTimeZone = "Asia/Kolkata";
	//$currentTimeZone = "GMT";
	function convert_time_zone($timeFromDatabase, $currentTimeZone, $newTimeZone)
	{
		$timeFromDatabase.'####'.$currentTimeZone.'####'.$newTimeZone.'$$$$$';
		$currentTimeZone = 'GMT';
		$currentTime = new DateTimeZone($currentTimeZone);
		$date = new DateTime($timeFromDatabase, $currentTime);
		$newTimeStamp = new DateTimeZone($newTimeZone);
		$date->setTimezone($newTimeStamp);
		return $date->format('Y-m-d H:i:s');
	}
	function getOddsByMarketId($market_id)
	{
		$result = array();
		$this->db->select('*');
		$this->db->where('market_id',$market_id);
		$resp = $this->db->get('betfair_markets_odds');
		if($resp->num_rows()>0)
		{
			$result = $resp->result_array();
		}
		return $result;
	}
	function getAllComp($eventTypeid)
	{
		$result = array();
		$this->db->select('*');
		$this->db->where('event_type_id',$eventTypeid);
		$this->db->where('active',1);
		$this->db->order_by('name','ASC');
		
		$resp = $this->db->get('betfair_events_competitions');
		if($resp->num_rows()>0)
		{
			$result = $resp->result_array();
		}
		return $result;
	}
	function getMarketName($evengetEventInfoBYEventIdt_id,$market_id)
	{
		$type = array();
		$this->db->select('*');
		$this->db->where('event_id',$event_id);
		$this->db->where('market_id',$market_id);
		$resp = $this->db->get('betfair_events_markets');
		if($resp->num_rows()>0)
		{
			$respSel1 = $resp->row_array();
			$type = $respSel1['market_type'];
		}
		return $type;
	}
	function getAllCompById($eventTypeid)
	{
		//echo $eventTypeid;die;
		$result = array();
		/*$this->db->select('*');
		$this->db->where('event_type_id',$eventTypeid);
		$this->db->where('active',1);
		//$this->db->where('parent_id ==',0);
		$this->db->where('flag ==',1);
		$this->db->order_by('name','ASC');
		$resp = $this->db->get('navigation');
		*/
		$sql = "select * from navigation where event_type_id=$eventTypeid and type = '' and active =1 and comp_active  =1 order by name ASC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
			$result = $query->result_array();
		}
		//echo $this->db->last_query();die;
		return $result;
	}
	function CountCompetetionByEventId($event_type_id)
	{
		$data = array();
		$this->db->select('*');
		$this->db->where('comp_active',1);
		$this->db->where('event_type_id',$event_type_id);
		$resp = $this->db->get('navigation');
		if($resp->num_rows()>0)
		{
			$data = $resp->result_array();
		}
		return $data;
	}
	function getEventTypeName($event_type_id)
	{
		$data = array();
		$this->db->select('*');
		$this->db->where('event_type_id',$event_type_id);
		$resp = $this->db->get('categories');
	//	echo $this->db->last_query();die;
		if($resp->num_rows()>0)
		{
			$data = $resp->row_array();
		}
		return $data;
	}
	function getEventTypeById($comp_id)
	{
		$data = array();
		$this->db->select('*');
		$this->db->where('competition_id',$comp_id);
		$resp = $this->db->get('betfair_events_competitions');
		if($resp->num_rows()>0)
		{
			$data = $resp->row_array();
		}
		return $data;
	}
	function getMarketNameById($market_id)
	{
		$data = array();
		$this->db->select('*');
		$this->db->where('market_id',$market_id);
		$resp = $this->db->get('betfair_events_markets');
		if($resp->num_rows()>0)
		{
			$data = $resp->row_array();
		}
		return $data;
	}
	function getEventDetailById($event_id)
	{
		$data = array();
		$this->db->select('is_aust');
		$this->db->where('event_id',$event_id);
		$resp = $this->db->get('betfair_events');
		if($resp->num_rows()>0)
		{
			$data = $resp->row_array();
		}
		return $data;
	}
	function getEventDetail($event_id)
	{
		$data = array();
		$this->db->select('*');
		$this->db->where('event_id',$event_id);
		$resp = $this->db->get('betfair_events');
		if($resp->num_rows()>0)
		{
			$data = $resp->row_array();
		}
		return $data;
	}
	function checkTipPlacedMyMarketId($market_id,$user_id)
	{
		$data = array();
		$this->db->select('*');
		$this->db->where('market_id',$market_id);
		$this->db->where('userid',$user_id);
		$resp = $this->db->get('user_placed_tip');
		if($resp->num_rows()>0)
		{
			return $data = $resp->row_array();
		}
		else 
		{
			return false;	
		}
	}
	function delete_place_tip($id)
	{
		$this->db->where('placed_tip_id',$id);
		$this->db->delete('user_placed_tip');
		return true;
	}
	function getSuggestionText($search ='')
	{
			$data = array();
			$curDate = date('Y-m-d H:i:s');
		//	$dateArr = $this->x_week_range3(date('Y-m-d'));
			//$openDate1 = $dateArr[0]." ".date('H:i:s');
			//$openDate2 = $dateArr[1]." ".date('H:i:s');
			$sql = "SELECT * FROM (`betfair_events`)WHERE CONVERT(open_date, DATE) >= CONVERT((NOW()-INTERVAL 1 DAY),DATE) ORDER BY `open_date` asc";
			$query = $this->db->query($sql);
			$time_val = $this->session->userdata('timezone');
			if($query->num_rows()>0)
			{
				$result = $query->result_array();
				foreach($result as $value)
				{
					$eventDate = $this->convert_time_zone($value['open_date'],$value['timezone'],$time_val);
					$date = date('Y-m-d H:i:s');
					if($eventDate >= $date) {
						$sql1 = "SELECT * FROM (`betfair_events`)WHERE event_id = '".$value['event_id']."' and  name like '%$search%' ORDER BY `open_date` asc";
						$query1 = $this->db->query($sql1);
						if($query1->num_rows()>0)
						{
							foreach($query1->result_array() as $row) {
							$time_val = $this->session->userdata('timezone');
							$date = $this->convert_time_zone($row['open_date'],$row['timezone'],$time_val);
							$date = date("l",strtotime($date))." ".date("d/m/Y",strtotime($date));
							$str = $row['name'].','.$date;
							$dd['name'] =  $str;
							//$dd['id'] = $row['event_id'];
							echo $str.'|'.$row['event_id']."\n";
							}
						}
					}	
				}
		}
		/*	
			//$sql = "select * from betfair_events where name like '%$search%' and convert('open_date',DATETIME)>Now()" ;
			$sql = "select * from betfair_events where name like '%$search%'and convert(`open_date`,DATETIME)>=Now()";
			/*$this->db->like('name', $search, 'both');
			$this->db->or_like('open_date', $search, 'both');
			$this->db->where("convert('open_date',DATETIME) >=",Now());
			$query = $this->db->get('betfair_events');
			
			$query = $this->db->query($sql);
			$dd = array();
			//echo $this->db->last_query();die;
			if($query->num_rows() >0)
			{
				foreach($query->result_array() as $row) {
					$time_val = $this->session->userdata('timezone');
					$date = $this->convert_time_zone($row['open_date'],$row['timezone'],$time_val);
					$date = date("l",strtotime($date))." ".date("d/m/Y",strtotime($date));
					$str = $row['name'].','.$date;
					$dd['name'] =  $str;
					//$dd['id'] = $row['event_id'];
					echo $str.'|'.$row['event_id']."\n";
				}
			}
			*/
	}
	function getEventNameByEventId($event_id)
	{
		$data = array();
		$data1 = array();
		$data2 = array();
		$result1 = array();
		$this->db->select('competition_id,open_date,name,timezone');
		$this->db->where('event_id',$event_id);
		$resp = $this->db->get('betfair_events');
		if($resp->num_rows()>0)
		{
			$data = $resp->row_array();
			
			$this->db->select('event_type_id');
			$this->db->where('competition_id',$data['competition_id']);
			$resp1 = $this->db->get('betfair_events_competitions');
			if($resp1->num_rows()>0)
			{
				$data1 = $resp1->row_array();
				$this->db->select('slugUrl,event_type_id');
				$this->db->where('event_type_id',$data1['event_type_id']);
				$resp = $this->db->get('categories');
				if($resp->num_rows()>0)
				{
					$data2 = $resp->row_array();
				}
				$result1 =  array_merge($data1,$data2,$data);
				
			}
		}
		
		return $result1;
	}
	function getUserdataByEventType()
	{
		$data = array();
		$this->db->select('*');
		$resp = $this->db->get('users');
		if($resp->num_rows()>0)
		{
			$data = $resp->result_array();
		}
		return $data;
	}
	function getusersortByEventId($event_type_id,$status)
	{
		if($status =='overall')
		{
			$sort_by ='strike_rate'; 
		}
		else if($status =='strikerate')
		{
			$sort_by ='won_tips';
		}
		else if($status =='profit')
		{
			$sort_by ='profit';	
		}
		else
		{
			$sqll = "SELECT distinct(`userid`) from user_placed_tip";
			$resp2 = $this->db->query($sqll);
			if($resp2->num_rows())
			{
				$res = $resp2->result_array();
			//	echo '<pre>';print_r($res);die;
				foreach($res as $id)
				{
					$sqll1 = "select count(*) as total_won_tip from (select `status` as orderstatus11, stats_done as sd from user_placed_tip where `userid`='".$id['userid']."'AND `event_type_id`='".$event_type_id."' order by`date_placed` DESC limit 5 ) as t1 where t1.orderstatus11=1 AND t1.sd= 1";
					$resp3 = $this->db->query($sqll1);
					$res2 = $resp3->row_array();
					$user_id = $id['userid'];
						$arrVal[$user_id] = $res2['total_won_tip'];
				}
				asort($arrVal);
				$reverseArr = array_reverse($arrVal,true);
				$result1 = array();
				foreach($reverseArr as $key=>$val)
				{
					$result = array();
					$dateArr = $this->x_week_range(date('Y-m-d'));
					$openDate1 = $dateArr[0]." ".date('H:i:s');
					$openDate2 = $dateArr[1]." ".date('H:i:s');
					$query1 = "SELECT distinct(a.userid),a.first_name,a.last_name,a.twitter_token, a.email, a.gender,
								a.dob, a.facebook_id, a.twitter_id,a.via,a.profile_image,a.tw_screen_name,a.username,
								a.active, b.tipsters_statistics_id, b.won_tips,b.profit, 
								b.loose_tips, b.total_tips, b.strike_rate, 
								b.ave_odds, b.rank
						FROM users AS a, tipsters_statistics AS b 
						where 
						a.userid = b.userid
						AND a.active =1
						AND a.verified =1
						AND a.userid =$key
						AND b.event_type_id=$event_type_id";
						$resp1 = $this->db->query($query1);
						$result1 = $resp1->row_array();
						//$result_final = array();
						if($resp1->num_rows())
						{
							$query = "SELECT count(*) as total_placed_tips,u.userid,u.twitter_token,u.username, u.first_name, u.last_name, u.email, u.gender, u.dob, u.profile_image,u.tw_screen_name,a.event_id 
							FROM betfair_events as a,user_placed_tip as b ,users as u
							WHERE a.event_id=b.event_id 
							and u.userid = b.userid
							and b.event_type_id =".$event_type_id."  
							AND b.userid=".$result1['userid'];
							$respNew = $this->db->query($query);
							$result11 = $respNew->row_array();
							if($result11['total_placed_tips']> 0) {
								$result_final[] = $result1;
							}
						}
						
				
				}
//				echo '<pre>';print_r($result_final);die;
				return $result_final;
			}
			
		}
		$result = array();
		$dateArr = $this->x_week_range(date('Y-m-d'));
		$openDate1 = $dateArr[0]." ".date('H:i:s');
		$openDate2 = $dateArr[1]." ".date('H:i:s');
		$query1 = "select usr.userid AS userid,tps.*
				from
				users as usr 
				left join 
				(SELECT distinct(a.userid) as tipster_id,  
						a.first_name, a.last_name, a.email, a.gender,
						a.dob, a.facebook_id,a.twitter_token, a.twitter_id, a.via,
						a.active, b.tipsters_statistics_id, b.won_tips,b.profit, 
						b.loose_tips, b.total_tips, b.strike_rate, 
						b.ave_odds, b.rank
				FROM users AS a, tipsters_statistics AS b 
				where 
				a.userid = b.userid
				AND a.active =1
				AND a.verified =1
				AND b.event_type_id=$event_type_id) as tps
				on (usr.userid=tps.tipster_id)
				where
				usr.active=1
				and usr.verified=1
				ORDER BY tps.$sort_by DESC , tps.profit DESC";
		$resp1 = $this->db->query($query1);
		$result1 = $resp1->result_array();
		//echo "<pre>";
		//print_r($result1);
		//die();
		foreach($result1 as $res11)
		{
			$sqll = "SELECT distinct(a.userid) from user_placed_tip a where a.userid='".$res11['userid']."' and a.event_type_id ='".$event_type_id."'";
			$resp2 = $this->db->query($sqll);
			//echo $this->db->last_query();die;
				if($resp2->num_rows())
				{
					$result2 = $res11;
					$query = "SELECT count(*) as total_placed_tips,u.userid, u.username, u.first_name, u.last_name, u.email, u.gender, u.dob,u.twitter_token, u.profile_image,u.tw_screen_name,a.event_id 
						FROM betfair_events as a,user_placed_tip as b ,users as u
						WHERE a.event_id=b.event_id 
						and u.userid = b.userid
						and b.event_type_id = ".$event_type_id." 
						AND b.userid=".$res11['userid'];
						$resp = $this->db->query($query);
						if($resp->num_rows())
						{
							$res = $resp->row_array();
							$res1 = array_merge($result2,$res);
							$result[] = $res1;
							
						}
				}
		}
		
		return $result;
	}
	function getusersortByEventIdBySport($event_id,$event_type_id)
	{
		$result = array();
		$data = array();
		$dateArr = $this->x_week_range(date('Y-m-d'));
		$openDate1 = $dateArr[0]." ".date('H:i:s');
		$openDate2 = $dateArr[1]." ".date('H:i:s');
		$time_val = $this->session->userdata('timezone');
		$query = "SELECT distinct (a.userid),a.twitter_token, a.username, a.first_name, a.last_name, a.email, a.gender, a.dob, a.profile_image,b.event_id,c.open_date,c.timezone FROM users as a,betfair_events as b ,user_placed_tip as c where a.userid=c.userid and b.event_id = c.event_id and c.event_id = $event_id and c.event_type_id = ".$event_type_id." and c.open_date >=CONVERT((NOW()-INTERVAL 1 DAY),DATE)";
		$resp1 = $this->db->query($query);
		$result1 = $resp1->result_array();
		//echo '<pre>';print_r($result1);die;
		foreach($result1 as $res1)
		{
			$eventDate = $this->convert_time_zone($res1['open_date'],$res1['timezone'],$time_val);
			$curDate = date('Y-m-d H:i:s');
			if($eventDate >= $curDate) {
					$user_id = $res1['userid'];
						$query1 = "select usr.userid,usr.username,usr.first_name, usr.last_name, usr.email, usr.gender, usr.dob, usr.profile_image,usr.tw_screen_name,stat.tipsters_statistics_id, stat.won_tips, stat.profit,
						stat.loose_tips, stat.total_tips, stat.strike_rate, 
						stat.ave_odds, stat.rank from users as usr,tipsters_statistics AS stat where usr.userid=stat.userid AND stat.userid=$user_id and usr.active =1 AND usr.verified =1 AND stat.event_type_id= $event_type_id ORDER BY stat.won_tips DESC";
				$resp11 = $this->db->query($query1);
				if($resp11->num_rows())
				{
					$data[] = $resp11->row_array();
				}
				else
				{
					$data[] = $res1;	
				}
			}
			
		}
	//	echo '<pre>';print_r($data);die;
		return $data;

	}
	
	function totalTipPlaced($user_id,$event_type_id)
	{
	
		$result = array();
		$curDate = date('Y-m-d H:i:s');
		$time_val = $this->session->userdata('timezone');
		$query = "SELECT b.*,u.userid, u.username, u.first_name, u.last_name, u.email, u.gender, u.dob, u.profile_image,a.event_id 
				FROM betfair_events as a,user_placed_tip as b ,users as u
				WHERE a.event_id=b.event_id 
				and u.userid = b.userid
				and b.event_type_id = ".$event_type_id." 
				AND b.open_date >= CONVERT((NOW()-INTERVAL 1 DAY),DATE)
				AND b.userid=".$user_id;
		$resp = $this->db->query($query);
		if($resp->num_rows())
		{
			$res = $resp->result_array();
		//	echo '<pre>';print_r($res);die;
			$i = 0;
			foreach($res as $value)
				{
					$eventDate = $this->convert_time_zone($value['open_date'],$value['timezone'],$time_val);
					$curDate = date('Y-m-d H:i:s');
					if($eventDate >= $curDate) {
						$query1 = "SELECT u.userid, u.username, u.first_name, u.last_name, u.email, u.gender, u.dob, u.profile_image,a.event_id 
						FROM betfair_events as a,user_placed_tip as b ,users as u
						WHERE a.event_id=b.event_id 
						and u.userid = b.userid
						and b.event_type_id = ".$event_type_id."
						AND b.userid=".$user_id;
						$resp1 = $this->db->query($query1);
						//echo $this->db->last_query();die;
						if($resp1->num_rows())
						{
							$result1[] = $resp1->row_array();
						}
						$i++;
				}
					
				}
				return $i;
				
				//return $result1;
		}
		
	}
	function totalTipPlacedBysport($user_id,$event_type_id,$event_id)
	{
		$result = array();
		$dateArr = $this->x_week_range2(date('Y-m-d'));
		$openDate1 = $dateArr[0]." ".date('H:i:s');
		$openDate2 = $dateArr[1]." ".date('H:i:s');
		$query = "SELECT count(*) as total_placed_tips,u.userid, u.username, u.first_name, u.last_name, u.email, u.gender, u.dob, u.profile_image,a.event_id 
				FROM betfair_events as a,user_placed_tip as b ,users as u
				WHERE a.event_id=b.event_id 
				and b.event_id=$event_id
				and u.userid = b.userid
				and b.event_type_id = ".$event_type_id." 
				AND b.open_date >= '".$openDate1."' 
				AND b.open_date <= '".$openDate2."' 
				AND a.open_date >=convert(now(),DATE) 
				AND b.userid=".$user_id;
		$resp = $this->db->query($query);
		if($resp->num_rows())
		{
			$res = $resp->row_array();
		}
		return $res;
	}
	function getEventInfoBYEventId($event_id)
	{
		$data = array();
		$this->db->select('*');
		$this->db->where('event_id',$event_id);
		$resp = $this->db->get('betfair_events');
		if($resp->num_rows()>0)
		{
			$data = $resp->row_array();
		}
		return $data;	
	}
	function getPurchasedtipResult($comp_id,$market_id,$tipster_id)
	{
		$resultArr = array();
		$query = "select distinct a.*,d.selection_id as winner_id from user_placed_tip a,betfair_events_markets c,betfair_events_results d
			where a.competition_id ='".$comp_id."' 
			and a.market_id ='".$market_id."' 
			and userid 	='".$tipster_id."' 
			and c.event_id = a.event_id
			and d.market_id = a.market_id
			and d.status='closed' 
			order by a.date_placed desc
			";
			$resSel = $this->db->query($query);
			if($resSel->num_rows()>0)
			{
				$resultArr = $resSel->row_array();
			}
			return 	$resultArr;
	}
	function getUserPurchaseEvent($userId,$market_id)
	{
			$resultArr = array();
			$sql = "select * from user_purchase_tip where tipster_id=$userId and market_id=$market_id";
			$resSel = $this->db->query($sql);
			if($resSel->num_rows()>0)
			{
				$resultArr = $resSel->row_array();
			}
			return 	$resultArr;
			//echo '<pre>';print_r($resultArr);die;
	}
	function getUserInfo($userId)
	{
			$resultArr = array();
			$sql = "select * from users where userid=$userId";
			$resSel = $this->db->query($sql);
			if($resSel->num_rows()>0)
			{
				$resultArr = $resSel->row_array();
			}
			return 	$resultArr;
			//echo '<pre>';print_r($resultArr);die;
	}
	function getPlacePageContent()
	{
			$resultArr = array();
			$sql = "select * from tbl_how_it_work where how_it_id=1";
			$resSel = $this->db->query($sql);
			if($resSel->num_rows()>0)
			{
				$resultArr = $resSel->row_array();
			}
			return 	$resultArr;
			//echo '<pre>';print_r($resultArr);die;
	}
	function getBuyPageContent()
	{
			$resultArr = array();
			$sql = "select * from tbl_how_it_work where how_it_id=2";
			$resSel = $this->db->query($sql);
			if($resSel->num_rows()>0)
			{
				$resultArr = $resSel->row_array();
			}
			return 	$resultArr;
			//echo '<pre>';print_r($resultArr);die;
	}
	function getAlreadyPurchasedTips($user_id,$tipster_id,$event_id,$market_id,$event_type_id)
	{
			$resultArr = array();
			$sql = "select * from user_purchase_tip where userid=$user_id and tipster_id=$tipster_id and event_id=$event_id and event_type_id=$event_type_id and market_id=$market_id";
			$resSel = $this->db->query($sql);
			if($resSel->num_rows()>0)
			{
				$resultArr = $resSel->row_array();
			}
			return 	$resultArr;
			//echo '<pre>';print_r($resultArr);die;
	}
	function getAllPlacedUsers()
	{
			$resultArr = array();
			$FinalArr = array();
			$dateArr = $this->x_week_range_week_before(date('Y-m-d'));
			$openDate1 = $dateArr[0]." ".date('H:i:s');
			$openDate2 = $dateArr[1]." ".date('H:i:s');
			$query = "SELECT distinct(userid)
				FROM user_placed_tip
				WHERE open_date <='".$openDate1."'
				And open_date >='".$openDate2."'";
			$resSel = $this->db->query($query);
			if($resSel->num_rows()>0)
			{
				$resultArr = $resSel->result_array();
				foreach($resultArr as $res1)
				{
					$sql1 = "select distinct(a.placed_tip_id),a.market_id,a.open_date,a.market_id,a.event_name,a.timezone,a.team_name,a.selected_odd,a.userid,a.selection_id as placedtip,
							b.selection_id as resulttip
							from user_placed_tip a , betfair_events_results b 
							where a.userid='".$res1['userid']."'
							and a.market_id=b.market_id 
							and a.open_date <='".$openDate1."'
							and a.open_date >= '".$openDate2."'
							and b.status='closed'";
					$queryExec = $this->db->query($sql1);
					$result = array();
					$index = 1;
					if($queryExec->num_rows()>0)
					{
						$resultArr = $queryExec->result_array();
						foreach($resultArr as $item)
						{
							$result['userid'] = $item['userid'];
							$result['event_name' . $index]  = $item['event_name'];
							$result['team_name' . $index]  = $item['team_name'];
							$result['selected_odd' . $index]  = $item['selected_odd'];
							$result['placedtip' . $index]  = $item['placedtip'];
							$result['resulttip' . $index]  = $item['resulttip'];
							$result['open_date' . $index]  = $item['open_date'];
							$result['timezone' . $index]  = $item['timezone'];
							$result['market_id' . $index]  = $item['market_id'];
							$index++;
					   }
					   $FinalArr[] = $result;
					}
				} 
				//echo '<pre>';print_r($FinalArr);die;
				return $FinalArr;
			}
		}
		function saveTrackingInformation($transc_id,$type_trans,$aaa)
		{
			$val_final = $transc_id.'##########'.$aaa;
			$arrIns = array('messages'=>$val_final,
						'date_added'=>date('Y-m-d H:i:s'));
			$this->db->insert('track_transactions',$arrIns);
		}
		function trackErrors($final_message)
		{
			$arrIns = array('message'=>$final_message,
						'date_added'=>date('Y-m-d H:i:s'));
			$this->db->insert('error_logs',$arrIns);
			return true;
		}
		
		function getMyPurchasedTipById($placed_tip_id)
		{
			$data = array();
			$this->db->select('*');
			$this->db->where('placed_tip_id',$placed_tip_id);
			$resp = $this->db->get('user_placed_tip');
			if($resp->num_rows()>0)
			{
				$data = $resp->row_array();
			}
			return $data;	
		}
		function deleteusers($user_id)
		{
			/******************************** USER BACKUP***************************/
			$this->db->trans_start();

			$sql_user = "INSERT INTO `users_backup` SELECT * FROM `users` WHERE `userid`=$user_id";
			$sql_placed = "INSERT INTO `user_placed_tip_backup` SELECT * FROM `user_placed_tip` WHERE `userid`=$user_id";
			
			$sql_purchased = "INSERT INTO `user_purchase_tip_backup` SELECT * FROM `user_purchase_tip` WHERE `tipster_id`=$user_id";
			$sql_statistics = "INSERT INTO `tipsters_statistics_backup` SELECT * FROM `tipsters_statistics` WHERE `userid`=$user_id";
			
			$resSel1 = $this->db->query($sql_user);
			$resSel2 = $this->db->query($sql_placed);
			$resSel3 = $this->db->query($sql_purchased);
			$resSel4 = $this->db->query($sql_statistics);
			$sql_user_del = "delete from users WHERE `userid`=$user_id";
			$sql_placed_del = "delete from user_placed_tip WHERE `userid`=$user_id";

			$sql_purchase_del = "delete from user_purchase_tip WHERE `tipster_id`=$user_id";
			$sql_statistics_del = "delete from tipsters_statistics WHERE `userid`=$user_id";
			$resSel11 = $this->db->query($sql_user_del);
			$resSel22 = $this->db->query($sql_placed_del);

			$resSel33 = $this->db->query($sql_purchase_del);
			$resSel44 = $this->db->query($sql_statistics_del);
			/******************************** DELETE USER***************************/
			
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
				return false;
			} 
			else
			{
				return true;	
			}
		}
		function sortByTime1($sortVal,$eventTypeId,$timerange)
		{
			$resultArr = array();
			if($timerange=='year'){
				$dateArr = $this->one_year_back_now(date('Y-m-d'));
				}
				else if($timerange=='month'){
					$dateArr = $this->one_month_back_now(date('Y-m-d'));
				}
				else
				{
					$dateArr = $this->one_week_now(date('Y-m-d'));
				}
			$week_start_date = $dateArr[0]." ".date('H:i:s');
			$week_end_date = $dateArr[1]." ".date('H:i:s');
			$query = "SELECT distinct(userid)
				FROM user_placed_tip
				WHERE open_date <='".$week_start_date."'
				And open_date >='".$week_end_date."'
				And event_type_id =$eventTypeId and stats_done=1";
			$resSel = $this->db->query($query);
			//echo $this->db->last_query();die;
			if($resSel->num_rows()>0)
			{
				$resultArr = $resSel->result_array();
				$user_id_val ='';
				$final_profit =0;
				$winning_odds =0;
				$loosing_odds =0;
				foreach($resultArr as $res1)
				{
					$user_id_val = $res1['userid'];
					$this->db->select('sum(selected_odd) as total, count(*) as number_records');
					$this->db->where('userId',$user_id_val);
					$this->db->where('event_type_id',$eventTypeId);
					$this->db->where('status',1);
					$this->db->where("open_date <= ",$week_start_date);
					$this->db->where("open_date >= ",$week_end_date);	
					$query1 = $this->db->get('user_placed_tip');
					if($query1->num_rows())
					{
						$res1 = $query1->row_array();
						$winning_odds = ($res1['total']*100)-(100*$res1['number_records']);
					}
					else
					{
						$winning_odds = 0;
					}
					$this->db->select('sum(selected_odd) as total,count(*) as number_records');
					$this->db->where('userId',$user_id_val);
					$this->db->where('event_type_id',$eventTypeId);
					$this->db->where('status',0);
					$this->db->where("open_date <= ",$week_start_date);
					$this->db->where("open_date >= ",$week_end_date);	
					$query2 = $this->db->get('user_placed_tip');
					if($query2->num_rows())
					{
						$res2 = $query2->row_array();
						$loosing_odds = $res2['number_records']*100;
					}
					else
					{
						$loosing_odds = 0;
					}
					$final_profit = $winning_odds - $loosing_odds;
					$total_odds = $winning_odds ;
					
					
					$sql3 = "select distinct a.*
								from user_placed_tip a , betfair_events_results b 
								where a.open_date <='".$week_start_date."'
								And a.open_date >='".$week_end_date."'
								And a.event_type_id = ".$eventTypeId."
								and a.userid=".$user_id_val."
								And a.market_id=b.market_id
								And b.status='closed' and stats_done=1
								";
						$resSel = $this->db->query($sql3);
						$won = 0;
						$loose = 0;
						$selected_odd = 0; 
						$ave_odds = 0;
						if($resSel->num_rows()>0)
						{
							$resultArr = $resSel->result_array();
							foreach($resultArr as $res1)
							{
								if($res1['status']==1)
								{
									$won++;
									$selected_odd += $res1['selected_odd'];	
								}
								else
								{
									$loose++;
								}
							}
							$total = $won + $loose;
							$strikeRate = round(($won/$total)*100,2);
							if($won)
							{
								$ave_odds = $selected_odd/$won;
							}
							
							$total_won = $won;
							$loose = $loose;
						}
					$sql_user = "select * from users where userid=$user_id_val";
					$sql_exc = $this->db->query($sql_user);
					if($sql_exc->num_rows()>0)
						{	
						$query1 = "select usr.userid,usr.first_name,usr.profile_image,usr.tw_screen_name,usr.last_name,usr.email, usr.gender,
									usr.dob, usr.facebook_id, usr.twitter_id, usr.via,
									usr.active FROM users AS usr
									where usr.userid = ".$user_id_val."
									AND usr.active =1
									AND usr.verified =1";
						$resp1 = $this->db->query($query1);
						$result1 = $resp1->row_array();
						$result1['profit'] = $final_profit;
						$result1['strikeRate'] = $strikeRate;
						$result1['ave_odds'] = $ave_odds;
						$result1['total_won'] = $total_won;
						$result1['total_loss'] = $loose;
						$result1['total_won_loss'] = $total;
						$arrVal[] = $result1;
					}
				} 
				if($sortVal=='strike_rate')
				{
					foreach ($arrVal as $key => $row) {
						$mid[$key]  = $row['strikeRate'];
					}	
				}
				else if($sortVal=='profit')
				{
					foreach ($arrVal as $key => $row) {
						$mid[$key]  = $row['profit'];
					}	
				}
				else
				{
					foreach ($arrVal as $key => $row) {
						$mid[$key]  = $row['total_won'];
					}	
				}
				
				array_multisort($mid, SORT_DESC, $arrVal);
				return $arrVal;
			}
		}	
		function sortByTime($sortVal,$eventTypeId,$timerange)
		{
			$resultArr = array();
			if($timerange=='year'){
				$dateArr = $this->one_year_back_now(date('Y-m-d'));
				}
				else if($timerange=='month'){
					$dateArr = $this->one_month_back_now(date('Y-m-d'));
				}
				else
				{
					$dateArr = $this->one_week_now(date('Y-m-d'));
				}
			$week_start_date = $dateArr[0]." ".date('H:i:s');
			$week_end_date = $dateArr[1]." ".date('H:i:s');
			$query = "SELECT distinct(userid)
				FROM user_placed_tip
				WHERE open_date <='".$week_start_date."'
				And open_date >='".$week_end_date."'
				And event_type_id =$eventTypeId and stats_done=1";
			$resSel = $this->db->query($query);
			//echo $this->db->last_query();die;
			if($resSel->num_rows()>0)
			{
				$resultArr = $resSel->result_array();
				$user_id_val ='';
				$final_profit =0;
				$winning_odds =0;
				$loosing_odds =0;
				$flag_won= 0;
				$flag_loss= 0;
				$won = 0;
				$loose = 0;
				$ii=0;
				foreach($resultArr as $res1)
				{
					$user_id_val = $res1['userid'];
					
					$sqlList = "select distinct a.*
								from user_placed_tip a,users usr
								where a.open_date <='".$week_start_date."'
								And a.open_date >='".$week_end_date."'
								And a.event_type_id = ".$eventTypeId."
								and a.userid=".$user_id_val."
								and a.stats_done=1";
						$resSelList = $this->db->query($sqlList);
						$sum_selected_odd = 0;
						$sum_selected_odd_loss = 0;
						$sum_selected_odd_won=0;
						$total_sel_loss = 0;
						$total_sel_won = 0;
						$selOdd = 0;
						$flag_won= 0;
						$flag_loss= 0;
						$won = 0;
						$loose = 0;
						$selected_odd = 0; 
						$total_won = 0;
						if($resSelList->num_rows())
						{
							$resultArrList = $resSelList->result_array();
							foreach($resultArrList as $selList)
							{
								if($selList['status']==1){
									$sum_selected_odd_won = $selList['selected_odd']+$sum_selected_odd_won;
									$flag_won++;
									$won++;
								}
								else
								{
									$sum_selected_odd_loss = $selList['selected_odd']+$sum_selected_odd_loss;
									$flag_loss++;
									$loose++;
								}
								
							}
							$winning_odds = ($sum_selected_odd_won*100)-(100*$flag_won);
							$Loss_odds = $flag_loss*100;
							$total_profit = $winning_odds-$Loss_odds;
							
							/***************WON LOSS **************/
							$total = $won + $loose;
							$strikeRate = round(($won/$total)*100,2);
							if($won)
							{
								$ave_odds = $sum_selected_odd_won/$won;
							}
							$total_won = $won;
							$loose = $loose;
							$sql_user = "select * from users where userid=$user_id_val";
							$sql_exc = $this->db->query($sql_user);
							if($sql_exc->num_rows()>0)
							{	
								$query1 = "select usr.userid,usr.first_name,usr.profile_image,usr.tw_screen_name,usr.twitter_token,usr.last_name,usr.email, usr.gender,
									usr.dob, usr.facebook_id, usr.twitter_id, usr.via,
									usr.active FROM users AS usr
									where usr.userid = ".$user_id_val."
									AND usr.active =1
									AND usr.verified =1";
									$resp1 = $this->db->query($query1);
									$result1 = $resp1->row_array();
									$result1['profit'] = $total_profit;
									$result1['strikeRate'] = $strikeRate;
									$result1['ave_odds'] = $ave_odds;
									$result1['total_won'] = $total_won;
									$result1['total_loss'] = $loose;
									$result1['total_won_loss'] = $total;
									$arrVal[] = $result1;
								}
									//echo '<pre>';print_r($arrVal);die;
						}
						$ii++;
					}
				if($sortVal=='strike_rate')
				{
					foreach ($arrVal as $key => $row) {
						$mid[$key]  = $row['strikeRate'];
					}	
				}
				else if($sortVal=='profit')
				{
					foreach ($arrVal as $key => $row) {
						$mid[$key]  = $row['profit'];
					}	
				}
				else
				{
					foreach ($arrVal as $key => $row) {
						$mid[$key]  = $row['total_won'];
					}	
				}
				
				array_multisort($mid, SORT_DESC, $arrVal);
				return $arrVal;
			}
		}	
		function getdata()
		{
			$user_id = 144;
			$result = array();
			$this->db->select("*");
			$this->db->where("userid",$user_id);
			$query = $this->db->get("user_purchase_tip");
			if($query->num_rows()>0)
			{
				$result = $query->result_array();
					//echo '<pre>';print_r($result);die;	
				foreach($result as $data)
				{
					$this->db->select("*");
					$this->db->where("userid",$data['tipster_id']);
					$this->db->where("market_id",$data['market_id']);
					$this->db->where("event_id",$data['event_id']);
					$query = $this->db->get("user_placed_tip");	
					$result1[] = $query->result_array();
				}
			echo '<pre>';print_r($result1);die;	
			}
			
			}
		function update_wallet_balance()
		{
			$this->db->select("*");
			$query = $this->db->get("users");	
			$result1 = $query->result_array();
			foreach($result1 as $res){
				$sql1 = "select sum(amount) as amount from user_transactions where userid='".$res['userid']."' and transaction_type='credit' and transaction_status=1";
				$query1 = $this->db->query($sql1);
				$res1 = $query1->row_array();
				$amount1 = $res1['amount'];
			
				$sql2 = "select sum(amount) as amount from user_transactions where userid='".$res['userid']."' and transaction_type='debit' and transaction_status=1";
				$query2 = $this->db->query($sql2);
				$res2 = $query2->row_array();
				$amount2 = $res2['amount'];
				
				$balanceAmount = $amount1 - $amount2;
				$updArr1 = array('balance'=>$balanceAmount,
								'last_updated_datetime'=>date('Y-m-d H:i:s'));
				$this->db->where('userid',$res['userid']);
				$this->db->update('user_wallet',$updArr1);
			}
			echo "succ";
		}
		function getCompetitionNameById($comp_id){
			$this->db->select('name');
			$this->db->where('competition_id',$comp_id);
			$resp = $this->db->get('betfair_events_competitions');
			if($resp->num_rows()>0)
			{
				$data = $resp->row_array();
			}
			return $data;	
		}
		function getLeagueData($event_type_id,$start_position=0, $pagesize=0){
				$country_time_zone = $this->session->userdata('timezone');
				
				$sql2="
						SELECT nav.*, 
						be.name as event_name, be.open_date, be.timezone, be.event_id, CONVERT_TZ(be.open_date,'GMT','$country_time_zone') as convertedtime 
						FROM navigation nav 
						INNER JOIN 
						betfair_events as be
						ON be.competition_id = nav.competition_id 
						WHERE 
						nav.event_type_id = $event_type_id AND 
						type='' AND 
						active=1 AND 
						comp_active=1 AND 
						CONVERT(be.open_date, DATE) >= CONVERT((NOW()-INTERVAL 1 DAY),DATE) 
						HAVING convertedtime >= NOW() 
						ORDER BY open_date";
					if($pagesize){
						$sql_limit = " LIMIT $start_position,$pagesize";
						}else{
							$sql_limit = '';
							}
				$sql =  $sql2.$sql_limit;
				$query = $this->db->query($sql);
				$res2 = $query->result_array();
				return $res2;
				//echo '<pre>';print_r($res2);die;
				
				
				/*$sql2="
				SELECT nav.*, be.name as event_name, be.open_date, be.timezone, be.event_id, be.convertedtime, be.market_id, be.market_type, be.market_name,be.event_market_id 
				FROM navigation nav 
				LEFT JOIN (SELECT b.competition_id, name, a.open_date, timezone, b.event_id, CONVERT_TZ(a.open_date,a.timezone,'$country_time_zone') as convertedtime, b.market_id, b.market_type, b.market_name, b.event_market_id from betfair_events a, betfair_events_markets as b where a.event_id = b.event_id and b.market_type = 'MATCH_ODDS') as be ON be.competition_id = nav.competition_id 
				WHERE nav.event_type_id = $event_type_id AND type='' AND active=1 AND comp_active=1 AND CONVERT(be.open_date, DATE) >= CONVERT((NOW()-INTERVAL 1 DAY),DATE) HAVING convertedtime >= NOW() ORDER BY open_date";
				
				if($pagesize){
					$sql_limit = " LIMIT $start_position,$pagesize";
					}else{
						$sql_limit = '';
						}
				$sql =  $sql.$sql_limit;
				$query = $this->db->query($sql);
				$res2 = $query->result_array();
				return $res2;
				//echo '<pre>';print_r($res2);die;
				*/
			}
		function getCompetitionDetailById($event_id)
		{
			$this->db->where("event_id",$event_id);
			$res = $this->db->get("betfair_events");
			if($res->num_rows())
			{
				return $res->row_array();
			}
		}
		function getLeagueDataBYCompId($event_type_id,$comp_id,$start_position=0, $pagesize=0){
			if($comp_id==0){
				$str = '';
				}else{
					$str = 'AND nav.competition_id='.$comp_id;
					}
				$country_time_zone = $this->session->userdata('timezone');
				$sql2="
				SELECT nav.*, 
						be.name as event_name, be.open_date, be.timezone, be.event_id, CONVERT_TZ(be.open_date,'GMT','$country_time_zone') as convertedtime 
						FROM navigation nav 
						INNER JOIN 
						betfair_events as be
						ON be.competition_id = nav.competition_id 
						WHERE 
						nav.event_type_id = $event_type_id $str AND 
						type='' AND 
						active=1 AND 
						comp_active=1 AND 
						CONVERT(be.open_date, DATE) >= CONVERT((NOW()-INTERVAL 1 DAY),DATE) 
						HAVING convertedtime >= NOW() 
						ORDER BY open_date";
				if($pagesize){
					$sql_limit = " LIMIT $start_position,$pagesize";
					}else{
						$sql_limit = '';
						}
				$sql =  $sql2.$sql_limit;
				//echo $sql;die;
				$query = $this->db->query($sql);
				$res2 = $query->result_array();
				return $res2;
				//echo '<pre>';print_r($res2);die;
			}
			function getMarkets($event_id,$off,$limit){
				//$sql_runner = "SELECT bem.market_id, bem.market_type,bem.competition_id, bem.market_name, br.selection_id,br.runner_name
				//FROM  betfair_events_markets bem
				//INNER JOIN  betfair_event_runners br ON bem.market_id = br.market_id and bem.event_id=27359028";
				$sql_runner = "SELECT * FROM betfair_events_markets where event_id=$event_id and market_id !='' limit $off,$limit";
				$query = $this->db->query($sql_runner);
				$res2 = $query->result_array();
				return $res2;
			}
			function getRunners($market_id){
				$data = array();
				$this->db->select('*');
				$this->db->where('market_id',$market_id);
				$this->db->group_by('selection_id'); 
				$resp = $this->db->get('betfair_event_runners');
				//echo $this->db->last_query();die;
				if($resp->num_rows()>0)
				{
					$data = $resp->result_array();
				}
				return $data;
			}
			function getMarketInfo($market_id){
				$data = array();
				$this->db->select('*');
				$this->db->where('market_id',$market_id);
				$resp = $this->db->get('betfair_events_markets');
				if($resp->num_rows()>0)
				{
					$data = $resp->row_array();
				}
				return $data;	
			}
			function getMatchOddEventsMarkets($event_id){
				$data = array();
				$this->db->select('*');
				$this->db->where('event_id',$event_id);
				$this->db->where('market_type','MATCH_ODDS');
				$resp = $this->db->get('betfair_events_markets');
				if($resp->num_rows()>0)
				{
					$data = $resp->row_array();
				}
				return $data;	
			}
			function getAllUserPlacedTips($user_id){
				$data = array();
				$this->db->select('*');
				$this->db->where('userid',$user_id);
				$this->db->order_by('open_date','DESC');
				$this->db->limit('50');
				$resp = $this->db->get('user_placed_tip');
				if($resp->num_rows()>0)
				{
					return $data = $resp->result_array();
				}
			}
			function getSportNameById($event_type_id){
				$data = array();
				$this->db->select('*');
				$this->db->where('event_type_id',$event_type_id);
				$resp = $this->db->get('categories');
				if($resp->num_rows()>0)
				{
					return $data = $resp->row_array();
				}
			}
		function getTipstersByPlaceTipId($place_tip_id)
		{
				$data = array();
				$this->db->select('*');
				$this->db->where('placed_tip_id',$place_tip_id);
				$resp = $this->db->get('user_placed_tip');
				if($resp->num_rows()>0)
				{
					return $data = $resp->row_array();
				}
		}
		function getTopWeekTipster(){
			$dateArr = $this->one_week_now(date('Y-m-d'));
			$date = new DateTime(date('Y-m-d'));	
			$weekNumber = $date->format("W");
			$year =  date("Y");
			$sql = "SELECT a.userid, SUM( a.`status` ) AS total_won_tip FROM user_placed_tip a WHERE a.date_placed >= '".$dateArr[1]."' AND a.date_placed <= '".$dateArr[0]."' AND a.status=1 GROUP BY a.userid order by total_won_tip DESC limit 1";
			$query = $this->db->query($sql);
				if($query->num_rows())
				{
					$result = $query->row_array();
					$this->db->select('*');
					$this->db->where('year',$year);
					$this->db->where('week',$weekNumber);
					$resp = $this->db->get('top_weekly_tipsters');
					if($resp->num_rows()>0)
					{
						$updArr1 = array('correct_tips'=>$result['total_won_tip'],'tipster_id'=>$result['userid'],'week_start_date'=>$dateArr[1],'week_end_date'=>$dateArr[0]);
						$this->db->where('week',$weekNumber);
						$this->db->where('year',$year);
						$this->db->update('top_weekly_tipsters',$updArr1);
						return true;
					}else{
					$arrIns = array('tipster_id'=>$result['userid'],
									'correct_tips' =>$result['total_won_tip'],
									'week'=>$weekNumber,
									'year'=>$year,
									'week_start_date'=>$dateArr[1],
									'week_end_date'=>$dateArr[0]
									);
						$this->db->insert('top_weekly_tipsters',$arrIns);
						return true;
					}
				}
		}
		function getCurrentWeekTipster(){
			$userId = $this->session->userdata('userid');
			$date = new DateTime(date('Y-m-d'));
			$weekNumber = $date->format("W");
			$year =  date("Y");
			$this->db->select('*');
			$this->db->where('year',$year);
			$this->db->where('week',$weekNumber);
			$resp = $this->db->get('top_weekly_tipsters');
			if($resp->num_rows()>0)
				{
					$result = $resp->row_array();
					if($result){
							$this->db->select('*');
							$this->db->where('userid',$userId);
							$respUser = $this->db->get('users');
							$result_user = $respUser->row_array();
								if($result_user['current_tipster_week']==$weekNumber){
										$result['status'] = 0;
									}else if($result_user['current_tipster_week']==''){
										$result['status'] = 1;
										
									}else if($result_user['current_tipster_week']!=$weekNumber){
										$result['status'] = 1;
										}
									$weekIns = array('current_tipster_week'=>$weekNumber);
									$this->db->where('userid',$userId);
									$this->db->update('users',$weekIns);
									return $result;
									
							}
					}
			}
			function getTipsterProfile($tipsterId,$week_start,$week_end){
				//$dateArr = $this->one_week_now(date('Y-m-d'));
			 	$sql = "SELECT SUM( a.`stats_done` ) AS total_tips,b.first_name,b.last_name,b.gender,b.twitter_token,b.profile_image FROM user_placed_tip a INNER JOIN users b ON a.userid = b.userid WHERE a.date_placed >= '".$week_start."' AND a.date_placed <= '".$week_end."' AND a.stats_done=1 AND a.userid=".$tipsterId;
				$query = $this->db->query($sql);
				if($query->num_rows())
				{
					return $result = $query->row_array();
				}
			}
			
			function getTopOverallTipster() {
				$result = array();
				$sqll = "SELECT distinct(userid) from user_placed_tip"; 
				$resp2 = $this->db->query($sqll);
				if($resp2->num_rows())
				{
					$result1 = $resp2->result_array();
					$i = 0;
					foreach($result1 as $row)
					{
						$query = "SELECT count(*) as total_placed_tips,u.userid, u.username, u.first_name, u.last_name, u.email, u.gender,u.gender,u.twitter_token,u.tw_screen_name,u.dob, u.profile_image
						FROM user_placed_tip as b ,users as u
						where u.userid = b.userid and u.active =1 AND u.verified =1 and b.stats_done=1 
						AND b.userid=".$row['userid'];
						$resp = $this->db->query($query);
						if($resp->num_rows())
						{
							$result2 = $resp->row_array();
							if($result2['total_placed_tips'] > 0) {
								$query1 = "SELECT count(*) as total_won_tips,sum(selected_odd) as total_odds FROM user_placed_tip where status=1 AND userid=".$row['userid'];
								$resp1 = $this->db->query($query1);
								$winning_odds = 0;
								$total_odds = 0;
								$total_won_tips = 0;
								if($resp1->num_rows())
								{
									$result3 = $resp1->row_array();
									$total_won_tips = $result3['total_won_tips'];
									$total_odds = $result3['total_odds'];
									if($result3['total_odds'] > 0) {
										$winning_odds = ($result3['total_odds']*100)-(100*$result3['total_won_tips']);
									}
								}
								
								$query2 = "SELECT count(*) as total_loss_tips,sum(selected_odd) as total_odds FROM user_placed_tip where status=0 AND userid=".$row['userid'];
								$resp3 = $this->db->query($query2);
								$loss_odds = 0;
								$total_loss_tips = 0;
								if($resp3->num_rows())
								{
									$result4 = $resp3->row_array();
									$total_loss_tips = $result4['total_loss_tips'];
									if($result4['total_odds'] > 0){
										$loss_odds = ($result4['total_loss_tips']*100);
									}
								}
								$final_profit = $winning_odds - $loss_odds; 
								if($result2['total_placed_tips'] < 10) 
								{
									$strike = 0;	
								} else {
									$strike = round(($total_won_tips/$result2['total_placed_tips'])*100,2);
								}
								$result[$i]['userid'] = $row['userid'];
								$result[$i]['username'] = $result2['username'];
								$result[$i]['first_name'] = $result2['first_name'];
								$result[$i]['last_name'] = $result2['last_name'];
								$result[$i]['profit'] = $final_profit;
								$result[$i]['total_won_tips'] = $total_won_tips;
								$result[$i]['total_loss_tips'] = $total_loss_tips;
								$result[$i]['strike'] = $strike;
								$result[$i]['tw_screen_name'] = $result2['tw_screen_name'];
								$result[$i]['twitter_token'] = $result2['twitter_token'];
								$result[$i]['total_placed_tips'] = $result2['total_placed_tips'];
								$result[$i]['profile_image'] = $result2['profile_image'];
								$result[$i]['total_odds'] = $total_odds;
								
								$i++;
							}
						}
					}
				}
			return $result;
		}
		
		function getTipsterDataById($user_id) {
				$result = array();
				$result['username'] = '';
				$result['first_name'] = '';
				$result['last_name'] = '';
				$result['profit'] = '';
				$result['total_won_tips'] = '';
				$result['total_loss_tips'] = '';
				$result['strike'] = '';
				$result['total_placed_tips'] = '';
				$result['total_odds'] = '';
				$query = "SELECT count(*) as total_placed_tips,u.userid, u.username, u.first_name, u.last_name, u.email, u.gender,u.gender,u.twitter_token,u.tw_screen_name,u.dob, u.profile_image
				FROM user_placed_tip as b ,users as u
				where u.userid = b.userid and u.active =1 AND u.verified =1 and b.stats_done=1 
				AND b.userid=".$user_id;
				$resp = $this->db->query($query);
				if($resp->num_rows())
				{
					$result2 = $resp->row_array();
					if($result2['total_placed_tips'] > 0) {
						$query1 = "SELECT count(*) as total_won_tips,sum(selected_odd) as total_odds FROM user_placed_tip where status=1 AND userid=".$user_id;
						$resp1 = $this->db->query($query1);
						$winning_odds = 0;
						$total_odds = 0;
						$total_won_tips = 0;
						if($resp1->num_rows())
						{
							$result3 = $resp1->row_array();
							$total_won_tips = $result3['total_won_tips'];
							$total_odds = $result3['total_odds'];
							if($result3['total_odds'] > 0) {
								$winning_odds = ($result3['total_odds']*100)-(100*$result3['total_won_tips']);
							}
						}
						$query2 = "SELECT count(*) as total_loss_tips,sum(selected_odd) as total_odds FROM user_placed_tip where status=0 AND userid=".$user_id;
						$resp3 = $this->db->query($query2);
						$loss_odds = 0;
						$total_loss_tips = 0;
						if($resp3->num_rows())
						{
							$result4 = $resp3->row_array();
							$total_loss_tips = $result4['total_loss_tips'];
							if($result4['total_odds'] > 0){
								$loss_odds = ($result4['total_loss_tips']*100);
							}
						}
						$final_profit = $winning_odds - $loss_odds; 
						if($result2['total_placed_tips'] < 10) 
						{
							$strike = 0;	
						} else {
							$strike = round(($total_won_tips/$result2['total_placed_tips'])*100,2);
						}
						$result['username'] = $result2['username'];
						$result['first_name'] = $result2['first_name'];
						$result['last_name'] = $result2['last_name'];
						$result['profit'] = $final_profit;
						$result['total_won_tips'] = $total_won_tips;
						$result['total_loss_tips'] = $total_loss_tips;
						$result['strike'] = $strike;
						$result['total_placed_tips'] = $result2['total_placed_tips'];
						$result['total_odds'] = $total_odds;
					}
				}
			return $result;
		}
	}
?>

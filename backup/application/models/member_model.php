<?php

class Member_model extends Model
{
	function Member_model()
	{
		parent::Model();
		$this->CI = & get_instance();
		$this->load->library('email');
	}
	
	function getAllMembers()
	{
		$data = array();
		$this->db->select('*');
		$query = $this->db->get('users');
		if($query->num_rows())
		{
			$data = $query->result_array();
			return $data;
		}
		else
		{
			return $data;
		}
	}
	
	function getMemberDetailsById($userid)
	{
		$data = array();
		$this->db->select('*');
		$this->db->where('userid',$userid);
		$query = $this->db->get('users');
		if($query->num_rows())
		{
			$data = $query->row_array();
			return $data;
		}
		else
		{
			return $data;
		}
	}
	
	function getPaidDetails()
	{
		$data = array();
		$userid = $this->session->userdata('userid');
		$this->db->select('*');
		$this->db->where('userid',$userid);
		$query = $this->db->get('user_bet_rates');
		if($query->num_rows())
		{
			$data = $query->row_array();
			return $data;
		}
		else
		{
			return $data;
		}
	}
	
	function updateGetPaid()
	{
		$userId = $this->session->userdata('userid');
		$this->db->select('*');
		$this->db->where('userid',$userId);
		$sel = $this->db->get('user_bet_rates');
		
		$insArr = array('customer_price'=>$this->input->post('customerprice'),
						'whisper_tip'=>$this->input->post('whisperamount'),
						'purchase_amount'=>$this->input->post('purchaseamount'));
						
		if($sel->num_rows())
		{
			$this->db->where('userid',$userId);
			$fl = $this->db->update('user_bet_rates',$insArr);
		}
		else
		{
			$insArr['userid'] = $userId;
			$fl = $this->db->insert('user_bet_rates',$insArr);
		}
		
		if($fl)
			return true;
		else
			return false;
	}
	
	function updateProfile()
	{
		$userId = $this->session->userdata('userid');
			
		$insArr = array('first_name'=>$this->input->post('first_name'),
						'last_name'=>$this->input->post('last_name'),
						'dob'=>date('Y-m-d',strtotime($this->input->post('dob'))),
						'gender'=>$this->input->post('gender'));
						
		$this->db->where('userid',$userId);
		$fl = $this->db->update('users',$insArr);
						
		$this->load->library('upload');
		$this->load->library('image_lib');
		
		$upload_path = './uploads/profile_picture/';
		
		
		if ($upload_path) 
		{
			$upload_path = $upload_path;
			if(isset($_FILES['profilePic']['name']) && $_FILES['profilePic']['error']==0)
			{
				$image143 = $_FILES['profilePic']['name'];
				$config['upload_path'] = $upload_path;//'./uploads/';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|swf|flv';
				$config['encrypt_name'] = TRUE;
				$config['max_size']	= 5000;
				$config['max_width']  = 0;
				$config['max_height']  = 0;
				$this->upload->initialize($config);
				if( $this->upload->do_upload('profilePic'))
				{
					$image_data = $this->upload->data();
					$image_name = $image_data['file_name'];
					
					$new_image_name=$upload_path.'thumb_'.$image_name;
					$new_image_name1=$upload_path.'small_'.$image_name;
					$this->image_lib->create_thumbnail($upload_path,$image_name,$new_image_name,125,118,"1d1d1d");
					$this->image_lib->create_thumbnail($upload_path,$image_name,$new_image_name1,280,192,"1d1d1d");
					
					$data= array('profile_image'=>$image_name);					
					$this->db->where('userid',$userId);
					$fl = $this->db->update('users',$data);
				}
			}
		}
		if($fl)
			return true;
		else
			return false;
	}
	
}
?>

<?php

class Navigation_model extends Model
{
	function Category_model()
	{
		parent::Model();
	}

function getAllEventType()
{

   $this->db->select('*');
   $query = $this->db->get('categories');
   if($query->num_rows() > 0)
   {
	  $userdata = $query->result_array() ;   
   }
   else
   {
	  $userdata = array() ;   
   }
   return $userdata ;
}
function getAllEventTypeById($id)
{

   $this->db->select('*');
   $query = $this->db->where('event_type_id',$id);
   $query = $this->db->get('betfair_events_types');
   if($query->num_rows() > 0)
   {
	  $userdata = $query->row_array() ;   
   }
   else
   {
	  $userdata = array() ;   
   }
   return $userdata ;
}
function getNavTypeById($id)
{

   $this->db->select('*');
   $query = $this->db->where('navigation_id',$id);
   $query = $this->db->get('navigation');
   if($query->num_rows() > 0)
   {
	  $userdata = $query->row_array() ;   
   }
   else
   {
	  $userdata = array() ;   
   }
   return $userdata ;
}

function getAllCompetitionbyEventType($event_id)
{

   $this->db->select('*');
	$this->db->where('event_type_id',$event_id);
	$this->db->where('comp_active',1);
	$this->db->where('type =' ,'');
   $query = $this->db->get('navigation');
   if($query->num_rows() > 0)
   {
	  $userdata = $query->result_array() ;   
   }
   else
   {
	  $userdata = array() ;   
   }
   //echo $this->db->last_query();die;
   return $userdata ;
}
function navigationcomp($uri_seg)
{
	$nav = $this->input->post('radio_nav');
	if($nav =='compitition')
	{
		if($this->input->post('competitions')){
			$competitions = $this->input->post('competitions');
			foreach($competitions as $key)
			{
				$update = array(
							'flag' => 0,
			);
			//echo '<pre>';print_r($update);die;
			$this->db->where('event_type_id', $uri_seg);
			$this->db->where('competition_id', $key);
			$this->db->update('navigation', $update);	
				//$i++;
			}
			
			$competitions[] = $competitions;
			$temp = '';
			foreach($competitions as $comp=>$val)
			{
				$competition_val = implode(",", $val);
			}
			$cat_name = $this->input->post('cat_name');
			$insert = array(
			'event_type_id' => $uri_seg,
			'name' => $cat_name,
			'competition_id' => $competition_val,
			'parent_id' => 0, 
			'comp_active' => 1 ,
			'active' => 1 ,
			'type' => 'comp'
			);
			//echo '<pre>';print_r($insert);die;
			$this->db->insert('navigation', $insert);
			$insert_id = $this->db->insert_id();
			if($insert_id) {
			return true;	
			}
			else
			{
			return false;
			}	
		}
		else
		{
		return false;	
		}
	}
	else
	{
		$cat_name = $this->input->post('cat_name');
		$insert = array(
		'event_type_id' => $uri_seg,
		'name' => $cat_name,
		'competition_id' => 0,
		'parent_id' => 0, 
		'active' => 1 ,
		'comp_active' => 1 ,
		'type' => 'custom'
		);
		//echo '<pre>';print_r($insert);die;
		$this->db->insert('navigation', $insert);
		$insert_id = $this->db->insert_id();
		if($insert_id) {
		return true;	
		}
		else
		{
		return false;
		}	
		
	}
}
function getAllNavigation($eventtype_id)
{

   $this->db->select('*');
	$this->db->where('event_type_id',$eventtype_id);
	$this->db->where('comp_active',1);
	$this->db->where('type !=','Submenu');
	$this->db->order_by('name','ASC');
	$query = $this->db->get('navigation');
   if($query->num_rows() > 0)
   {
	  $userdata = $query->result_array() ;   
   }
   else
   {
	  $userdata = array() ;   
   }
   //echo $this->db->last_query();die;
   return $userdata ;
}
function getCompitetionById($comp_id)
{
	$userdata = array();
	$CompArray  = explode(',',$comp_id);
	//echo '<pre>';print_r($CompArray);die;
	foreach($CompArray as $com=>$val)
	{
		$this->db->select('*');
		$this->db->where('competition_id',$val);
	//	$this->db->where('type =','Submenu');
		$query = $this->db->get('navigation');
		if($query->num_rows() > 0)
		{
			$userdata[] = $query->row_array() ;   
		}
	}
		return $userdata;
}

function getAllNavigationBYID($nav_id)
{
	$this->db->select('*');
	$this->db->where('navigation_id',$nav_id);
	$query = $this->db->get('navigation');
	   if($query->num_rows() > 0)
	   {
		  $userdata = $query->row_array() ;   
	   }
	   else
	   {
		  $userdata = array() ;   
	   }
	   return $userdata ;
}
function getAllSubNavigationBYID($nav_id)
{
	$this->db->select('*');
	$this->db->where('navigation_id',$nav_id);
	$query = $this->db->get('navigation');
	   if($query->num_rows() > 0)
	   {
		  $userdata = $query->row_array() ;   
	   }
	   else
	   {
		  $userdata = array() ;   
	   }
	   return $userdata ;
}
function EditNavComp($nav_id,$event_type_id)
{
	$type_edit = $this->input->post('type_edit');
	$nav = $this->input->post('radio_nav');
	if($type_edit!='competition'){
	if($nav =='compitition')
	{
		
			$competitions = $this->input->post('competitions');
			$this->db->select('competition_id');
			$this->db->where('navigation_id',$nav_id);
			$query1 = $this->db->get('navigation');
			$data = array();
			if($row=$query1->num_rows()>0)
			{
				$data = $query1->row_array();
			}
			foreach($data as $key)
			{
				$ExplodeData = explode(',',$key);
				foreach($ExplodeData as $key1) {
				$update = array(
							'flag' => 1,
			);
			$this->db->where('event_type_id', $event_type_id);
			$this->db->where('competition_id', $key1);
			$this->db->update('navigation', $update);	
			}
		}
		foreach($competitions as $key)
			{
				$update = array(
							'flag' => 0,
			);
			$this->db->where('event_type_id', $event_type_id);
			$this->db->where('competition_id', $key);
			$this->db->update('navigation', $update);	
			}
			
		$this->db->select('*');
		$this->db->where('navigation_id',$nav_id);
		$this->db->where('event_type_id',$event_type_id);
		$this->db->where('parent_id',0);
		$query1 = $this->db->get('navigation');
		   if($query1->num_rows() > 0)
		   {
				$this->db->where('parent_id', $nav_id);
				$this->db->delete('navigation');
				$competitions[] = $this->input->post('competitions');
			$temp = '';
			foreach($competitions as $comp=>$val)
			{
				$competition_val = implode(",", $val);
			}
			$cat_name = $this->input->post('cat_name');
			$update = array(
			'event_type_id' => $event_type_id,
			'name' => $cat_name,
			'competition_id' => $competition_val,
			'parent_id' => 0, 
			'active' => 1 ,
			'type' => 'comp'
			);
		//	echo '<pre>';print_r($update);die;
			$this->db->where('navigation_id ', $nav_id);
			$this->db->update('navigation', $update);
			return true;

		   }
		   else
		   {
			$competitions[] = $this->input->post('competitions');
			$temp = '';
			foreach($competitions as $comp=>$val)
			{
				$competition_val = implode(",", $val);
			}
			$cat_name = $this->input->post('cat_name');
			$update = array(
			'event_type_id' => $event_type_id,
			'name' => $cat_name,
			'competition_id' => $competition_val,
			'parent_id' => 0, 
			'active' => 1 ,
			'type' => 'comp'
			);
		//	echo '<pre>';print_r($update);die;
			$this->db->where('navigation_id ', $nav_id);
			$this->db->update('navigation', $update);
			return true;
}}
	else
	{
		$cat_name = $this->input->post('cat_name');
		$update = array(
		'event_type_id' => $event_type_id,
		'name' => $cat_name,
		'competition_id' => 0,
		'parent_id' => 0, 
		'active' => 1,
		'comp_active' => 1,
		'type' => 'custom'
		);
	
	//echo '<pre>';print_r($update);die;
	$this->db->where('navigation_id', $nav_id);
	$this->db->update('navigation', $update);
		return true;
	}
}
else
{

		$cat_name = $this->input->post('cat_name');
		$update = array(
		'name' => $cat_name,
		);
	$this->db->where('navigation_id', $nav_id);
	$this->db->update('navigation', $update);
		return true;
		
}		
	
}

function Editnavigationcomp($uri_seg)
{
	$competitions[] = $this->input->post('competitions');
	$temp = '';
	foreach($competitions as $comp=>$val)
	{
		$competition_val = implode(",", $val);
	}
	$cat_name = $this->input->post('cat_name');
	$active = $this->input->post('active');
	$active = ($active) ? 1 : 0;
	
	$update = array(
	'event_type_id' => $uri_seg,
	'name' => $cat_name,
	'competition_id' => $competition_val,
	'parent_id' => 0, 
	'active' => $active 
	);
	
	$this->db->where('navigation_id ', $uri_seg);
	$this->db->update('navigation', $update);
	return true;	
	
}
function Subnavigationcomp($uri_seg,$nav_id)
{
		if($this->input->post('competitions')){
			$competitions = $this->input->post('competitions');
			//echo '<pre>';print_r($competitions);die;
			//$i=0;
			foreach($competitions as $key)
			{
				$update = array(
							'flag' => 0,
			);
			//echo '<pre>';print_r($update);die;
			$this->db->where('event_type_id', $uri_seg);
			$this->db->where('competition_id', $key);
			$this->db->update('navigation', $update);	
				//$i++;
			}
			//echo $this->db->last_query();die;
			$competitions[] =$competitions; 
			$temp = '';
			foreach($competitions as $comp=>$val)
			{
				$competition_val = implode(",", $val);
			}
			$cat_name = $this->input->post('cat_name');
			$insert = array(
			'event_type_id' => $uri_seg,
			'name' => $cat_name,
			'competition_id' => $competition_val,
			'parent_id' => $nav_id, 
			'active' => 1 ,
			'type' => 'Submenu'
			);
			//echo '<pre>';print_r($insert);die;
			$this->db->insert('navigation', $insert);
			$insert_id = $this->db->insert_id();
			if($insert_id) {
			return true;	
			}
			else
			{
			return false;
			}	
		}
}
function EditSubnavigationcomp($uri_seg,$nav_id,$nav_id_sub)
{
		if($this->input->post('competitions')){
			$competitions = $this->input->post('competitions');
			$this->db->select('competition_id');
			$this->db->where('navigation_id',$nav_id_sub);
			$query1 = $this->db->get('navigation');
			$data = array();
			if($row=$query1->num_rows()>0)
			{
				$data = $query1->row_array();
			}
			foreach($data as $key)
			{
				$ExplodeData = explode(',',$key);
				foreach($ExplodeData as $key1) {
				$update = array(
							'flag' => 1,
			);
			$this->db->where('event_type_id', $uri_seg);
			$this->db->where('competition_id', $key1);
			$this->db->update('navigation', $update);	
			}
		}
			foreach($competitions as $key)
			{
				$update = array(
							'flag' => 0,
			);
			$this->db->where('event_type_id', $uri_seg);
			$this->db->where('competition_id', $key);
			$this->db->update('navigation', $update);	
			}
			$competitions[] = $competitions;
			$temp = '';
			foreach($competitions as $comp=>$val)
			{
				$competition_val = implode(",", $val);
			}
			
			$cat_name = $this->input->post('cat_name');
			$update = array(
			'event_type_id' => $uri_seg,
			'name' => $cat_name,
			'competition_id' => $competition_val,
			'parent_id' => $nav_id, 
			'active' => 1 ,
			'type' => 'Submenu'
			);
			//echo '<pre>';print_r($update);die;
			$this->db->where('navigation_id', $nav_id_sub);
			$this->db->update('navigation', $update);
				
		}
}
function getAllSubmenuById($nav_id,$event_id)
{
   $this->db->select('*');
   $query = $this->db->where('parent_id',$nav_id);
   $query = $this->db->where('event_type_id',$event_id);
   $query = $this->db->get('navigation');
   if($query->num_rows() > 0)
   {
	  $userdata = $query->result_array() ;   
   }
   else
   {
	  $userdata = array() ;   
   }
   return $userdata ;
}
function activeInactiveCategory($nav_id,$status,$EventtypeId)
{
	//echo $nav_id;die;
	if($status==0)
		{
			$status1=1;
			$flag = 0;
		}
	else
		{
			$status1=0;
			$flag = 1;
					
		}
		if($nav_id)
		{
			
			$this->db->select('competition_id');
			$this->db->where('navigation_id',$nav_id);
			$query1 = $this->db->get('navigation');
			$data = array();
			if($row=$query1->num_rows()>0)
			{
				$data = $query1->row_array();
			}
			foreach($data as $key)
			{
				$ExplodeData = explode(',',$key);
				foreach($ExplodeData as $key1) {
				$update = array(
							'flag' => $flag,
			);
			$this->db->where('event_type_id', $EventtypeId);
			$this->db->where('competition_id', $key1);
			$this->db->update('navigation', $update);	
			$this->db->set('active',$status1);
			$this->db->where('navigation_id',$nav_id);
			$this->db->update('navigation');
			
		}
	}
	return true;
}
			else
			{
				
			return false;	
			}
		}
function activeInactiveNavigation($nav_id,$status)
{
		if($nav_id)
		{
			$this->db->set('active',$status);
			$this->db->where('navigation_id',$nav_id);
			$this->db->update('navigation');
			return true;
			
		}
		else
		{
		return false;	
		}
	}
function activeInactiveCompMulti()
{
	
		//echo '<pre>';print_r($_POST);die;
		if($_POST){
			if($_POST['Activebtn']){
			foreach($_POST['multiactive'] as $ids)
			{
				$this->db->set('active',1);
				$this->db->where('navigation_id',$ids);
				$this->db->update('navigation');
			}
			return true;
		}
		else{
			foreach($_POST['multiactive'] as $ids)
			{
				$this->db->set('active',0);
				$this->db->where('navigation_id',$ids);
				$this->db->update('navigation');
			}
			return true;
		}
		
	}
	else{
		
		return false;
		}
	
}
	
}

?>

<?php

class Profit_model extends Model
{
	function Profit_model()
	{
		parent::Model();
		$this->CI = & get_instance();
	}
	function one_week_now($date) 
	{
		$ts = strtotime($date);
		$start = (date('w', $ts) == 0) ? $ts : strtotime('today', $ts);
		 return array(date('Y-m-d', $start),
		 date('Y-m-d', strtotime('-1 week', $start)));
		 $openDate1 = $dateArr[0]." ".date('H:i:s');
	}
	function getProfitByTime()
	{
			$resultArr = array();
			$eventTypeId= 1;
			$dateArr = $this->one_week_now(date('Y-m-d'));
			$week_start_date = $dateArr[0]." ".date('H:i:s');
			$week_end_date = $dateArr[1]." ".date('H:i:s');
			$query = "SELECT distinct(userid)
				FROM user_placed_tip
				WHERE open_date <='".$week_start_date."'
				And open_date >='".$week_end_date."'
				And event_type_id =$eventTypeId";
			$resSel = $this->db->query($query);
			if($resSel->num_rows()>0)
			{
				$resultArr = $resSel->result_array();
				foreach($resultArr as $res1)
				{
					$user_id_val = $res1['userid'];
					$this->db->select('sum(selected_odd) as total, count(*) as number_records');
					$this->db->where('userId',$res1['userid']);
					$this->db->where('event_type_id',$eventTypeId);
					$this->db->where('status',1);
					$this->db->where("open_date <= ",$week_start_date);
					$this->db->where("open_date >= ",$week_end_date);	
					$query1 = $this->db->get('user_placed_tip');
					if($query1->num_rows())
					{
						$res1 = $query1->row_array();
						$winning_odds = ($res1['total']*100);
					}
					else
					{
						$winning_odds = 0;
					}
					$this->db->select('sum(selected_odd) as total,count(*) as number_records');
					$this->db->where('userId',$res1['userid']);
					$this->db->where('event_type_id',$eventTypeId);
					$this->db->where('status',0);
					$this->db->where("open_date <= ",$week_start_date);
					$this->db->where("open_date >= ",$week_end_date);	
					$query2 = $this->db->get('user_placed_tip');
					if($query2->num_rows())
					{
						$res2 = $query2->row_array();
						$loosing_odds = $res2['number_records']*100;
					}
					else
					{
						$loosing_odds = 0;
					}
					$total_odds = $winning_odds - $loosing_odds;
					$total_odds = $winning_odds ;//Profit By Time
					$sql3 = "select distinct a.*
								from user_placed_tip a , betfair_events_results b 
								where
								a.open_date <='".$week_start_date."'
								And a.open_date >='".$week_end_date."'
								And a.event_type_id = ".$eventTypeId."
								and a.userid=".$res1['userid']."
								And a.market_id=b.market_id
								And b.status='closed' and stats_done=1
								";
						$resSel = $this->db->query($sql3);
						echo $this->db->last_query();die;
						if($resSel->num_rows()>0)
						{
							$user_id_val= $resFirst['userid'];
							$resultArr = $resSel->result_array();
							foreach($resultArr as $res1)
							{
								$won = 0;
								$loose = 0;
								$selected_odd = 0; 
								$ave_odds = 0;
								$placed_tip_id = $val['placed_tip_id'];
								if($res1['status']==1)
								{
									$won++;
									$selected_odd += $resp3['selected_odd'];	
								}
								else
								{
									$loose++;
								}
							}
							$total = $won + $loose;
							$strikeRate = round(($won/$total)*100,2);
							if($won)
							{
								$ave_odds = $selected_odd/$won;
							}
							$total_won = $won;
							$total_value = $total;
						}
					if($user_id_val>0){
					$query1 = "select usr.userid,usr.first_name,usr.last_name,usr.email, usr.gender,
									usr.dob, usr.facebook_id, usr.twitter_id, usr.via,
									usr.active FROM users AS usr
									where usr.userid = $user_id_val
									AND usr.active =1
									AND usr.verified =1";
									echo $query1;die;
						$resp1 = $this->db->query($query1);
						$result1 = $resp1->row_array();
						$result1['profit'] = $total_odds;
						$result1['strikeRate'] = $strikeRate;
						$result1['ave_odds'] = $ave_odds;
						$result1['total_won'] = $total_won;
						$result1['total_value'] = $total_value;
						$arrVal[] = $result1;
					}
				} 
				return $arrVal;
			}
		}
	function getStrikeRateByTime()
	{
		$eventTypeId= 1;
		$dateArr = $this->one_week_now(date('Y-m-d'));
		$week_start_date = $dateArr[0]." ".date('H:i:s');
		$week_end_date = $dateArr[1]." ".date('H:i:s');
		$queryFirst = "SELECT distinct(userid)
				FROM user_placed_tip
				WHERE open_date <='".$week_start_date."'
				And open_date >='".$week_end_date."'
				And event_type_id =$eventTypeId";
			$resSel1 = $this->db->query($queryFirst);
			//echo $this->db->last_query();die;
			if($resSel1->num_rows()>0)
			{
				$resultArrFirst = $resSel1->result_array();
				foreach($resultArrFirst as $resFirst)
				{
					$sql3 = "select distinct a.*
								from user_placed_tip a , betfair_events_results b 
								where
								a.open_date <='".$week_start_date."'
								And a.open_date >='".$week_end_date."'
								And a.event_type_id = ".$eventTypeId."
								and a.userid=".$resFirst['userid']."
								And a.market_id=b.market_id
								And b.status='closed' and stats_done=1
								";
						$resSel = $this->db->query($sql3);
						if($resSel->num_rows()>0)
						{
							$user_id_val= $resFirst['userid'];
							$resultArr = $resSel->result_array();
							foreach($resultArr as $res1)
							{
								$won = 0;
								$loose = 0;
								$selected_odd = 0; 
								$ave_odds = 0;
								$placed_tip_id = $val['placed_tip_id'];
								if($res1['status']==1)
								{
									$won++;
									$selected_odd += $resp3['selected_odd'];	
								}
								else
								{
									$loose++;
								}
							}
							$total = $won + $loose;
							$strikeRate = round(($won/$total)*100,2);
							if($user_id_val>0){
							$query1 = "select usr.userid,usr.first_name,usr.last_name,usr.email, usr.gender,
									usr.dob, usr.facebook_id, usr.twitter_id, usr.via,
									usr.active,stat.tipsters_statistics_id, stat.won_tips,stat.profit, 
									stat.loose_tips, stat.total_tips, 
									stat.ave_odds, stat.rank FROM users AS usr, tipsters_statistics AS stat 
									where usr.userid = $user_id_val
									AND usr.active =1
									AND usr.verified =1 and stat.event_type_id=$eventTypeId and stat.userid=$user_id_val";
								$resp1 = $this->db->query($query1);
								$result1 = $resp1->row_array();
								$result1['strike_rate'] = $strikeRate;
								$arrVal[] = $result1;
							}	
						}
					}
					return $arrVal;die;
				}
		}
}
?>

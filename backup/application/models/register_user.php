<?php

class Register_user extends Model
{
	function Register_user()
	{
		parent::Model();
		$this->CI = & get_instance();
		$this->load->library('email');
	}
	function check_user($email)
	{
		$this->db->select('*');
		$this->db->where('email',$email);
		$this->db->where('verified', 1);
		$query = $this->db->get('users');
		if($query->num_rows() >0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	function check_email($email)
	{
		$this->db->select('*');
		$this->db->where('email',$email);
		$query = $this->db->get('users');
		if($query->num_rows() >0)
		{
			$response = $query->row_array();
			return $response;
		}
		else
		{
			return false;
		}
	}
	
	function check_user_twitter($id)
	{
		$this->db->select('*');
		$this->db->where('twitter_id',$id);
		$query = $this->db->get('users');
		if($query->num_rows() >0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}	
		
	
	function facebook($email, $facebook_id, $first_name, $last_name, $screen_name, $gender, $birthday)
	{
		$username = $first_name;
		$birthday = date('Y-m-d', strtotime($birthday));
		$data = array(
		'first_name' => $first_name,
		'last_name' => $last_name,
		'email' => $email,
		'facebook_id' => $facebook_id,
		'dob' => $birthday,
		'via' => 'facebook',
		'active' => '1',
		'gender'=> $gender,
		'verified' => '1',
		'signup_date' => date('Y/m/d H:m:s'),
		'profile_image'=>'https://graph.facebook.com/'.$facebook_id.'/picture?type=normal'
		);
		$this->db->insert('users',$data);		
		$id = $this->db->insert_id();
		
		if($id)
		{
			$user_wallet = array(
				'userid' => $id,
				'balance' => 0.00,
				'active'=>1,
				'last_updated_datetime'=>date('Y/m/d H:m:s'),
			);
			$this->db->insert('user_wallet',$user_wallet);
			$key =$this->config->item('mailchimp_key');
			$list_id =$this->config->item('mailchimp_list_id');
			$val = BASEPATH;
			$basepath = str_replace('system', 'assets', BASEPATH);
			$filetoinclude = $basepath.'MCAPI.class.php';
			include_once $filetoinclude;
			$api = new MCAPI($key);//Live
			$merge_vars = array('FNAME'=>$first_name, 'LNAME'=>$last_name); 
			$api->listSubscribe($list_id, $email, $merge_vars);			
			return true;
		}
		else
		{
			return false;
		}
			
	}
	function twitter($email)
	{
		$key =$this->config->item('mailchimp_key');
		$list_id =$this->config->item('mailchimp_list_id');
		$twitter = $this->session->userdata('twittersess');		
		$data = array(
		'first_name' => $twitter['first_name'],		
		'email' => $email,
		'twitter_id' => $twitter['twitter_id'],		
		'profile_image' => $twitter['profile_image'],		
		'via' => 'twitter',	
		'active' => '1',
		'verified' => '1',
		'signup_date' => date('Y/m/d H:m:s'),
		'tw_screen_name' => $twitter['screen_name'],
		);
		$this->db->insert('users',$data);			
		$id = $this->db->insert_id();
		if($id)
		{
			$user_wallet = array(
				'userid' => $id,
				'balance' => 0.00,
				'active'=>1,
				'last_updated_datetime'=>date('Y/m/d H:m:s'),
			);
			
			
			$this->db->insert('user_wallet',$user_wallet);	
			$val = BASEPATH;
			$basepath = str_replace('system', 'assets', BASEPATH);
			$filetoinclude = $basepath.'MCAPI.class.php';
			include_once $filetoinclude;
			$api = new MCAPI($key);//Live
			$merge_vars = array('FNAME'=>$twitter['first_name']); 
			$api->listSubscribe($list_id, $email, $merge_vars);
			return true;	
			
		}
		
		/*
		$to = $email;
		$email_template_id = 1;
		$first_name = $twitter['first_name'];
		$last_name = '';
		$name = $twitter['first_name'];
		$key = $this->authorize->generatePassword(12);
		$password = $key;
		$data123 = array('verification_key'=>$key);
		$this->db->where('userid',$id);
		$this->db->update('users',$data123);
		$code = base64_encode($email.'&'.$key);
		$email_content = $this->getEmailTemplate($email_template_id);
		$message = $email_content[0]['body'];
		$link = '<a href="'.base_url().'home/activate/'.$code.'">CLICK HERE TO VERIFY YOUR ACCOUNT</a>';
		$search = array('{FIRSTNAME}','{LASTNAME}','{SITENAME}','{ACTIVATIONLINK}','{USEREMAIL}','{USERNAME}','{PASSWORD}', '{SITELINK}');
		$replace= array($first_name,$last_name,'Whispertip',$link,$to,$to,$password,'www.whispertip.com');
		$body=str_replace($search,$replace,$message);
		
		$email_body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd
				html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
				<!-- Facebook sharing information tags -->
				<meta property="og:title" content="*|MC:SUBJECT|*" />

				<title>*|MC:SUBJECT|*</title>
				<style type="text/css">
				body{ }
				#templatePreheader{}
				#templateContainer{}
				a{}
				#templateBody{}
				span{}

				 @media only screen and (max-width: 600px) {
						body,table,td,p,a,li,blockquote {
						-webkit-text-size-adjust:none !important;
						}
						table {width: 100% !important;}
						.responsive-image img {
						height: auto !important;
						max-width: 100% !important;
						width: 100% !important;
						}
					}
				</style>
				</head>
				<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="background:#eeeeee; font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:10px;">
				<center>
				<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable">
				<tr>
				<td align="center" valign="top">
				<!-- // Begin Template Preheader \\ -->
				<table border="0" cellpadding="10" cellspacing="0" width="550" id="templatePreheader" style="background:#dcdcdc; border-bottom:2px dotted #fff;border-radius:10px 10px 0px 0px;-moz-border-radius:10px 10px 0px 0px;-webkit-border-radius:10px 10px 0px 0px;">
				<tr>
				<td valign="top" class="preheaderContent">
				<center><img src="www.whispertip.com/assets/images/sports_logo.png" width="200"/></img></center>
				</td>
				</tr>
				</table>
				<!-- // End Template Preheader \\ -->
				<table border="0" cellpadding="0" cellspacing="0" width="550" id="templateContainer" style="background:#ffffff;border-radius:0px 0px 10px 10px;-moz-border-radius:0px 0px 10px 10px;-webkit-border-radius:0px 0px 10px 10px;">
				<tr>
				<td align="center" valign="top">
				<!-- // Begin Template Body \\ -->
				<table border="0" cellpadding="0" cellspacing="0" width="550" id="templateBody" style="border-bottom:1px solid #ddd;">
				<tr>
				<td colspan="3" valign="top" class="bodyContent">

				<!-- // Begin Module: Standard Content \\ -->
				<table border="0" cellpadding="20" cellspacing="0" width="100%">
				<tr>
				<td valign="top">
				<div mc:edit="std_content00">
				'.$body.'

				</div>
				</td>
				</tr>
				</table>
				<!-- // End Module: Standard Content \\ -->

				</td>
				</tr>

				</table>
				<!-- // End Template Body \\ -->
				</td>
				</tr>
				<tr>
				<td align="center" valign="top">
				<!-- // Begin Template Footer \\ -->
				<table border="0" cellpadding="10" cellspacing="0" width="550" id="templateFooter">
				<tr>
				<td valign="top" class="footerContent">

				<!-- // Begin Module: Standard Footer \\ -->
				<table border="0" cellpadding="10" cellspacing="0" width="100%">
				<tr>
					<td style=" width: 96px; font-size:14px;">Follow Us On: <a href="http://instagram.com/whispertip" target="_blank">
					<img src="http://whispertip.com/assets/images/icon_instagram.png" style="margin-left: 5px; vertical-align: bottom;"></a>
					<a href="http://twitter.com/whispertip" target="_blank"><img src="http://whispertip.com/assets/images/icon_tweet.png" style="margin-left: 5px; vertical-align: bottom;"></a>
					<a href="https://facebook.com/whispertip" target="_blank"><img src="http://whispertip.com/assets/images/icon_facebook.png" style="margin-left: 5px; vertical-align: bottom;"></a></td>
					
				</tr>
				</table><!-- // End Module: Standard Footer \\ -->

				</td>
				</tr>
				</table>
				<!-- // End Template Footer \\ -->
				</td>
				</tr>
				</table>
				<br />
				</td>
				</tr>
				</table>
				</center>
				</body>
				</html>
';			
		$config['charset'] = 'iso-8859-1';
		$config['mailtype'] = 'html';
		$subject = $email_content[0]['subject'];
		$from = $email_content[0]['from'];
		$this->load->library('email');
		$this->email->initialize($config);
		$this->email->from($from,$name);
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($email_body);
		$email_status = $this->email->send();
		*/
		
	}
	
	function registerUser()
	{
		$key =$this->config->item('mailchimp_key');
		$list_id =$this->config->item('mailchimp_list_id');
		$data = array(
		'email' => $this->input->post('userEmail'),
		'username' => $this->input->post('userName'),
		'password'=>$this->input->post('userPassword'),
		'first_name'=>$this->input->post('userFirstName'),
		'last_name'=>$this->input->post('userLastName'),
		'active' => '1',
		'verified' => '1',
		'signup_date' => date('Y/m/d H:m:s'),
		);
		$this->db->insert('users',$data);			
		$id = $this->db->insert_id();
		if($id)
		{
			$user_wallet = array(
				'userid' => $id,
				'balance' => 0.00,
				'active'=>1,
				'last_updated_datetime'=>date('Y/m/d H:m:s'),
			);
			$this->db->insert('user_wallet',$user_wallet);			
		}
		$val = BASEPATH;
		$basepath = str_replace('system', 'assets', BASEPATH);
		$filetoinclude = $basepath.'MCAPI.class.php';
		include_once $filetoinclude;
		$api = new MCAPI($key);//Live
		$merge_vars = array('FNAME'=>$this->input->post('userFirstName'), 'LNAME'=>$this->input->post('userLastName')); 
		$api->listSubscribe($list_id, $this->input->post('userEmail'), $merge_vars);
		/* $email = $this->input->post('userEmail');
		$to = $email;
		$email_template_id = 1;
		$first_name = $this->input->post('userName');
		$last_name = '';
		$name = $this->input->post('userName');
		$key = $this->authorize->generatePassword(12);
		$password = $key;
		$data123 = array('verification_key'=>$key);
		$this->db->where('userid',$id);
		$this->db->update('users',$data123);
		$code = base64_encode($email.'&'.$key);
		$email_content = $this->getEmailTemplate($email_template_id);
		$message = $email_content[0]['body'];
		$link = '<a href="'.base_url().'home/activate/'.$code.'">CLICK HERE TO VERIFY YOUR ACCOUNT</a>';
		$search = array('{FIRSTNAME}','{LASTNAME}','{SITENAME}','{ACTIVATIONLINK}','{USEREMAIL}','{USERNAME}','{PASSWORD}', '{SITELINK}');
		$replace= array($first_name,$last_name,'Whispertip',$link,$to,$to,$password,'demo.whispertip.com/beta/home');
		$body=str_replace($search,$replace,$message);	
		
		$email_body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd
				html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
				<!-- Facebook sharing information tags -->
				<meta property="og:title" content="*|MC:SUBJECT|*" />

				<title>*|MC:SUBJECT|*</title>
				<style type="text/css">
				body{ }
				#templatePreheader{}
				#templateContainer{}
				a{}
				#templateBody{}
				span{}

				 @media only screen and (max-width: 600px) {
						body,table,td,p,a,li,blockquote {
						-webkit-text-size-adjust:none !important;
						}
						table {width: 100% !important;}
						.responsive-image img {
						height: auto !important;
						max-width: 100% !important;
						width: 100% !important;
						}
					}
				</style>
				</head>
				<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="background:#eeeeee; font-family:Arial, Helvetica, sans-serif; font-size:14px; padding:10px;">
				<center>
				<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable">
				<tr>
				<td align="center" valign="top">
				<!-- // Begin Template Preheader \\ -->
				<table border="0" cellpadding="10" cellspacing="0" width="550" id="templatePreheader" style="background:#dcdcdc; border-bottom:2px dotted #fff;border-radius:10px 10px 0px 0px;-moz-border-radius:10px 10px 0px 0px;-webkit-border-radius:10px 10px 0px 0px;">
				<tr>
				<td valign="top" class="preheaderContent">
				<center><img src="demo.whispertip.com/beta/assets/images/sports_logo.png" width="200"/></img></center>
				</td>
				</tr>
				</table>
				<!-- // End Template Preheader \\ -->
				<table border="0" cellpadding="0" cellspacing="0" width="550" id="templateContainer" style="background:#ffffff;border-radius:0px 0px 10px 10px;-moz-border-radius:0px 0px 10px 10px;-webkit-border-radius:0px 0px 10px 10px;">
				<tr>
				<td align="center" valign="top">
				<!-- // Begin Template Body \\ -->
				<table border="0" cellpadding="0" cellspacing="0" width="550" id="templateBody" style="border-bottom:1px solid #ddd;">
				<tr>
				<td colspan="3" valign="top" class="bodyContent">

				<!-- // Begin Module: Standard Content \\ -->
				<table border="0" cellpadding="20" cellspacing="0" width="100%">
				<tr>
				<td valign="top">
				<div mc:edit="std_content00">
				'.$body.'

				</div>
				</td>
				</tr>
				</table>
				<!-- // End Module: Standard Content \\ -->

				</td>
				</tr>

				</table>
				<!-- // End Template Body \\ -->
				</td>
				</tr>
				<tr>
				<td align="center" valign="top">
				<!-- // Begin Template Footer \\ -->
				<table border="0" cellpadding="10" cellspacing="0" width="550" id="templateFooter">
				<tr>
				<td valign="top" class="footerContent">

				<!-- // Begin Module: Standard Footer \\ -->
				<table border="0" cellpadding="10" cellspacing="0" width="100%">
				<tr>
					<td style=" width: 96px; font-size:14px;">Follow Us On: <a href="http://instagram.com/whispertip" target="_blank">
					<img src="demo.whispertip.com/beta/assets/images/icon_instagram.png" style="margin-left: 5px; vertical-align: bottom;"></a>
					<a href="http://twitter.com/whispertip" target="_blank"><img src="demo.whispertip.com/beta/assets/images/icon_tweet.png" style="margin-left: 5px; vertical-align: bottom;"></a>
					<a href="https://facebook.com/whispertip" target="_blank"><img src="demo.whispertip.com/beta/assets/images/icon_facebook.png" style="margin-left: 5px; vertical-align: bottom;"></a></td>
					
				</tr>
				</table><!-- // End Module: Standard Footer \\ -->

				</td>
				</tr>
				</table>
				<!-- // End Template Footer \\ -->
				</td>
				</tr>
				</table>
				<br />
				</td>
				</tr>
				</table>
				</center>
				</body>
				</html>
';		
		$config['charset'] = 'iso-8859-1';
		$config['mailtype'] = 'html';
		$subject = $email_content[0]['subject'];
		$from = $email_content[0]['from'];
		$this->load->library('email');
		$this->email->initialize($config);
		$this->email->from($from,$name);
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($email_body);
		$email_status = $this->email->send();
		
		if($id)
		{
			return true;
		}
		else
		{
			return false;
		}
		
		*/
		 return true;
	}
	
	function getEmailTemplate($id)
	{
		$this->db->select('*');
		$this->db->where('email_template_id', $id);
		$query = $this->db->get('email_templates');
		return $query->result_array();
	}
	
	function loginuserSocial($email)
	{
		$data = array();
		$this->db->select('*');
		$this->db->where('email',$email);
		$this->db->where('verified',1);
		$query = $this->db->get('users');
		if($query->num_rows() > 0)
		{
			$data = $query->row_array();
		}
		return $data;	
	}
	
	function verifyCode($email,$code)
	{
		$this->db->select('*');
		$this->db->where('email',$email);
		$this->db->where('verification_key',$code);
		$query = $this->db->get('users');
		if($query->num_rows() > 0)
		{
			$data = array('verified'=> 1);
			$this->db->where('email',$email);
			$this->db->update('users',$data);
			return true;	
		}
		else{
				return false;
			}
	}
	
	function login($userName,$userPassword)
	{
		$sql = "SELECT *
				FROM (`users`)
				WHERE (`username` = '".$userName."' OR `email` = '".$userName."')
				AND `password` = '".$userPassword."'
				AND `verified` = 1";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
			$data = $query->row_array();
			$useremail = $data['email'];
			$userid = $data['userid'];
			$username = $data['username'];
			$this->CI->session->set_userdata('useremail', $useremail);
			$this->CI->session->set_userdata('userid', $userid);
			$this->CI->session->set_userdata('username', $username);
			$this->CI->session->set_userdata('user_loggedin', true);
			return true;	
		}
		else
		{
			return false;
		}
	}
	
	function insertTwitterToken($token)
	{
		$explodeStr = explode('&',$token);
		//echo '<pre>';print_r($explodeStr);die;
		$screen_name = $explodeStr[3];
		$explodeStr1 = explode('=',$screen_name);
		$userId = $this->CI->session->userdata('userid');
		$update = array('twitter_token'=>$token,'tw_screen_name'=>$explodeStr1[1]);
		$this->db->where('userid',$userId);
		$fl = $this->db->update('users',$update);
		if($fl)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function getTwitterToken()
	{
		$userId = $this->CI->session->userdata('userid');
		$this->db->where('userid',$userId);
		$query = $this->db->get('users');
		if($query->num_rows())
		{
			$result = $query->row_array();
			if($result['twitter_token']!='')
			{
				return $result['twitter_token'];
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	function check_user_name($user_name)
	{
		$this->db->select('*');
		$this->db->where('username',$user_name);
		$this->db->where('verified', 1);
		$query = $this->db->get('users');
		if($query->num_rows() >0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	function getTwitterPublicToken($userId)
	{
		$this->db->where('userid',$userId);
		$query = $this->db->get('users');
		if($query->num_rows())
		{
			$result = $query->row_array();
			if($result['twitter_token']!='')
			{
				return $result['twitter_token'];
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
}
?>

<?php
class User_authorization_model extends Model {


	function User_authorization_model()
	{
		parent::Model();
		$this->CI =& get_instance();
	}
	
	function validate_admin_credentials($username, $password)
	{
		$this->db->select('username');
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$query = $this->db->get('admin');
		if ($query->num_rows() > 0)
		{		
			return true;		
		}
		else
		{
			return false;
		}
	}

	function record_admin_login($user)
	{
		$update = array(
		'last_login' => date("Y-m-d H:i:s"),
		'last_login_ip' => $_SERVER['REMOTE_ADDR']
		);
		
		$this->db->where('username', $user);
		if( $this->db->update('admin', $update) )
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
	
	function changeAdminPassword()
	{
		$old_pass = $this->input->post('old_password');
		$new_pass = $this->input->post('new_password');
		$confirm_pass = $this->input->post('confirm_password');
		
		$this->db->select('*');
		$this->db->where('active', 1);
		$query = $this->db->get('admin');
		
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$password = $row->password;
				if($password == $old_pass)
				{
					$update = array(
					'password' => $new_pass
					);
					$this->db->update('admin', $update);
					
					return true;
				}
				else
				{
					return false;
				}
				
			}
		}
	}	
	
	function validate_user_credentials($username,$pass)
	{
		/*
				$sql = "select userid, username, password from users
						where username='$username' and
						password='$password'";
		*/
				
		$sql="select a.userid,a.username,a.password,b.primary_email 
				from users a,users_contact_details b
				 where a.userid = b.userid 
				 and a.username='$username'
				 and a.password='$pass' and active=1";
		//print $sql;die;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0)
		{
			$user = $query->row_array();
			//print $user['userid'];	die;
			return $user['userid'];		
		}
		else
		{
			return false;
		}
	}
	
	function validate_user_credentials_for_facebookuser($username)
	{
		$sql="select a.userid,a.username,a.password,b.primary_email from users a,users_contact_details b where a.userid = b.userid and b.primary_email='$username'";
		//print $sql;die;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0)
		{
			$user = $query->row_array();
			//print $user['userid'];	die;
			return $user['userid'];		
		}
		else
		{
			return false;
		}
	}

	function record_user_login($userid)
	{
		$update = array(
		'last_login' => date("Y-m-d H:i:s"),
		'active' => 1
		);
		
		$this->db->where('userid', $userid);
		if( $this->db->update('users', $update) )
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
	
	function changeDealerPassword($user)
	{
		$old_pass = $this->input->post('old_password');
		$new_pass = $this->input->post('new_password');
		$confirm_pass = $this->input->post('confirm_password');
		
		$this->db->select('*');
		$this->db->where('username', $user);
		$this->db->where('active', 1);
		$query = $this->db->get('users');
		
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$password = $row->password;
				if($password == $old_pass)
				{
					$update = array(
					'password' => $new_pass
					);
					$this->db->update('users', $update);
					
					return true;
				}
				else
				{
					return false;
				}
				
			}
		}
	}	

	function is_account_admin($user_account_id, $username)
	{
		$sql = "select a.user_account_id from users a, account_admins b 
				where a.userid=b.userid and 
				a.user_account_id=b.user_account_id and
				a.username='$username' and 
				a.user_account_id='$user_account_id'";
		$query = $this->db->query($sql);
		
		if($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function validate_website_credentials($username, $password)
	{
		$sql = "select a.username,a.user_account_id as website_account_id from users a, users_accounts b 
				where
				a.user_account_id=b.user_account_id and
				a.active=1 and
				b.account_role_id=2 and 
				a.username='$username' and
				a.password='$password'";
		
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0)
		{
			$dealer = $query->row_array();
			return $dealer['website_account_id'];		
		}
		else
		{
			return false;
		}
	}

	function record_website_login($user)
	{
		$update = array(
		'last_login' => date("Y-m-d H:i:s")
		);
		
		$this->db->where('username', $user);
		if( $this->db->update('users', $update) )
		{
			return true;
		}
		else
		{
			return false;
		}	
	}

	/////// to add front users ///////////
	function addFrontUser()
	{
		$insert = array(
		'firstname' => $this->input->post('firstname'),
		'lastname' => $this->input->post('lastname'),
		'email' => $this->input->post('email'),
		'newsletter_subscription' => $this->input->post('newsletter'),
		'date_created' => date("Y-m-d H:i:s")
		);
		if($this->db->insert('front_users', $insert))
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
	
	/////// to get all front users //////////
	function getAllFrontUsers($txtSearch='', $start_position=0, $pagesize=0)
	{
		$users = array();
		$this->db->select('*');

		if($txtSearch)
		$this->db->like('firstname', $txtSearch, 'both'); 
		$this->db->or_like('lastname', $txtSearch, 'both'); 
		
		if($pagesize)
		{
			$query = $this->db->get('front_users', $pagesize, $start_position);
		}
		else
		{
			$query = $this->db->get('front_users');
		}
	
		if($query->num_rows() > 0)
		{
			$users = $query->result_array();
		}
		return $users;
	}
	
	////////// to get user details by id ////////
	function getUserDetailsById($front_userid)
	{
		$users = array();
		$this->db->select('*');
		
		$this->db->where('front_userid', $front_userid);
		$query = $this->db->get('front_users');
		if($query->num_rows() > 0)
		{
			$users = $query->row_array();
		}
		return $users;
	}
	
	///////// to edit user /////////
	function editUser($front_userid)
	{
		if($front_userid)
		{
			$update = array(
			'firstname' => $this->input->post('firstname'),
			'lastname' => $this->input->post('lastname'),
			'email' => $this->input->post('email'),
			'newsletter_subscription' => $this->input->post('newsletter')
			);
			$this->db->where('front_userid', $front_userid);
			$this->db->update('front_users', $update);
		return true;
		}	
		else
		{
			return false;
		}
	}
	
	////// to delete users //////////
	function deleteUser($front_userid)
	{
		if($front_userid)
		{
			$this->db->where('front_userid', $front_userid);
			$this->db->delete('front_users');
			return true;
		}
		else
		{
			return false;
		}
	}
	
	///////// to get all contactus submissions /////////
	function getAllContactUsSubmissions($txtSearch, $start_position=0, $pagesize=0)
	{
		$submissions = array();
		$this->db->select('*');

		if($txtSearch)
		$this->db->like('name', $txtSearch, 'both'); 
		$this->db->or_like('email', $txtSearch, 'both'); 
		
		if($pagesize)
		{
			$query = $this->db->get('front_contactus', $pagesize, $start_position);
		}
		else
		{
			$query = $this->db->get('front_contactus');
		}
		
		if($query->num_rows() > 0)
		{
			$submissions = $query->result_array();
		}
		return $submissions;
	}
	
	////////// to post contactus submission //////////
	function postContactUsSubmission()
	{
		$insert = array(
		'name ' => $this->input->post('name'),
		'email ' => $this->input->post('email'),
		'query ' => $this->input->post('query'),
		'date_created' => date("Y-m-d H:i:s")
		);
		if($this->db->insert('front_contactus', $insert))
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
	
	function getUserDefaultLanguageByUsername($username)
	{
		$language_id=1;
		$this->db->select('language_id');
		$this->db->where('username', $username);
		$query = $this->db->get('users');
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$language_id = $row->language_id;
		}
		return $language_id;
	}
	
	function checkUserExistsByEmail($email)
	{
		$data = array();
		$this->db->select('username, password');
		$this->db->where('email', $email);
		
		$query = $this->db->get('users');
		if($query->num_rows() > 0)
		{
			$data = $query->row_array();
		}
			return $data;		
	}
	function getAllTestimonial()
	{
		$sql="SELECT * FROM testimonials order by rand() limit 1";
		$query = $this->db->query($sql);				
						
		
		if($query->num_rows()>0)
		{
			$data=$query->result_array();
			return $data;
		}
		else
		{
			return $data=array();
		}
	}
	function checEmailExistance($email)
	{
		$this->db->select('*');
		$this->db->where('email',$email);
		$query = $this->db->get('users');
		if($query->num_rows()>0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
?>

<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Authorize {

	function Authorize()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('user_authorization_model');		
		$this->CI->load->model('betfair_model');		
	}
	
	// this function checks if admin is logged in
	// returns: "true" if admin is logged in and "false" on error
	function is_admin_logged_in()
	{
		$admin_username = $this->CI->session->userdata('admin_username');
		$bool_admin_loggedin = $this->CI->session->userdata('admin_loggedin');
	
		if( (isset($admin_username) && $admin_username != '') && (isset($bool_admin_loggedin) && $bool_admin_loggedin == true) )
			return true;
		else
			return false;
	}
	
	// this function checks the username and password from database
	// parameters: $user -> username of admin, $pass -> password of admin
	// Returns: true if admin credentials matches and false on error
	function validate_admin_credentials($user, $pass)
	{
		if($this->CI->user_authorization_model->validate_admin_credentials($user, $pass) == true)
		{
			if( $this->CI->user_authorization_model->record_admin_login($user) == true )
			{
				$this->CI->session->set_userdata('admin_username', $user);
				$this->CI->session->set_userdata('admin_loggedin', true);
				return true;
			}
			else
			{
				//unable to update admin data
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	// this finction whenever calls do logout of admin 
	// and will unset the session varialbles having login details of admin
	function do_admin_logout()
	{
		$this->CI->session->unset_userdata('admin_username');
		$this->CI->session->unset_userdata('admin_loggedin');
		return true;
	}
			
		
	function is_account_admin($user_account_id, $username)
	{
		if( $this->CI->user_authorization_model->is_account_admin($user_account_id, $username) == true )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function validate_user_credentials_social($user)
	{
		$udata = $this->CI->register_user->loginuserSocial($user);
		if($udata)
		{
			$userid = $udata['userid'];
			$useremail = $udata['email'];
			$username = $udata['username'];
			$this->CI->session->set_userdata('useremail', $useremail);
			$this->CI->session->set_userdata('userid', $userid);
			$this->CI->session->set_userdata('username', $username);
			$this->CI->session->set_userdata('user_loggedin', true);
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function user_loggedIn()
	{
		$useremail = $this->CI->session->userdata('useremail');
		$userid = $this->CI->session->userdata('userid');
		$username = $this->CI->session->userdata('username');
		$user_loggedin = $this->CI->session->userdata('user_loggedin');
		if($useremail!='' && $userid!='' && $user_loggedin!='')
		{
			return true;
		}
		else
		{
			return false;
		}                                    

	}
	
	function generatePassword($length = 8) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}
	
	function getSlug($string, $separator = '-' )
	{
		$string = html_entity_decode($string, ENT_QUOTES, 'UTF-8');
		//print $string; die();affiliate_model
		$accents_regex = '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i';
		$special_cases = array( '&' => 'and');
		$string = mb_strtolower( trim( $string ), 'UTF-8' );
		$string = str_replace( array_keys($special_cases), array_values( $special_cases), $string );
		$string = preg_replace( $accents_regex, '$1', htmlentities( $string, ENT_QUOTES, 'UTF-8' ) );
		$string = preg_replace("/[^a-z0-9]/u", "$separator", $string);
		$string = preg_replace("/[$separator]+/u", "$separator", $string);
		return $string ;
	}
	
	function getUserTimeZone($ip)
	{
		$url = "http://ip-api.com/json/".$ip;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = json_decode(curl_exec($ch));
	   
		curl_close($ch);
		//echo "<pre>";
		//print_r($response);
		if($response->status=='success')
		{
			$timeZone = $response->timezone;
		}
		else
		{
			$timeZone = false;
		}
		return $timeZone;
	}
	
	function getMenuLinks($url,$cId=0,$eId=0)
	{
		require(FCPATH.'assets/simple_html_dom.php');
		$url = $url;//"http://www.betfair.com/exchange/football?container=false&modules=nav&isAjax=true&ts=1393837442410&alt=json";
		//$proxy = "199.195.141.245:80";		
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		//curl_setopt($ch, CURLOPT_PROXY, $proxy);
	//	curl_setopt($ch, CURLOPT_PROXYUSERPWD, "whispertip:yhc5346gt365");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = json_decode(curl_exec($ch)); 
		
		
		curl_close($ch);
		//echo "<pre>";print_r($response);die;
		$str = $response->children;
		$html = str_get_html($str);
		$allLinks ="<div style=' float: right;left: 15px;position: absolute;top: -17px;width: 100%;z-index: 1000;'><img src='".base_url()."assets/images/cross.png' id='' class='crossing' style='float:right;cursor:pointer;height: 26px;
    width: 26px;' onclick='closepopup()'></div>";
		$allLinks .= '<div id="scrollLeague" class="detail_popup season" style="width:99%;height: 460px;">';
		foreach($html->find('ul[class=children]') as $ul) {
		 foreach ($ul->find('li') as $li) {
		   foreach($li->find('a') as $a)
		   {
			   $furl = explode('/',$a->href);
			   $url = explode('?',$furl[count($furl)-1]);
			   $aalink = $url[1];
				$mkid = explode("=",$aalink);
			   if(trim($url[0])=='market' || $mkid[0]=='mi')
			   {
				   $mkid = explode("=",$aalink);
				   if($mkid[0]=='mi')
				   {
					  $mkid1 = explode("&",$mkid[1]); 
					  $mkid2 = explode("&",$mkid[2]); 
					  $mkid = $mkid2[0].'.'.$mkid1[0];
					}
					else {
					$aalink = $url[1];
					$mkid = $mkid[1];
					}
					//die;
					$flres = $this->CI->betfair_model->getEventsMarketsByMarketId($mkid);
					//echo '<pre>';print_r($flres);die;
					if(count($flres))
					{
						//echo $a->innertext;
						//$flres1 = $this->CI->betfair_model->getEventsMarketsNameByMarketId($mkid,$a->innertext);
						//echo "<pre>";print_r($flres1);
						$eId = $flres['event_id'];
						$cId = $flres['competition_id'];
						$allLinks .="<div class='purchase_row pur_bor2' data-one='".$eId."' data-two='".$mkid."' data-three='".$cId."' style='cursor:pointer;' >". $a->innertext."</div>";
					}
					
				}
			   else if(trim($url[0])=='event')
			   {
					$mkid = explode("=",$url[1]);
					$mkid = $mkid[count($mkid)-1];
					$aalink = "http://www.betfair.com/exchange/football/event?".$url[1]."&container=false&modules=nav&isAjax=true&ts=1393837442410&alt=json";
					$allLinks .="<div class='purchase_row getmenu' data-three='".$cId."' data-one='".$mkid."' style='cursor:pointer;' data-url='".$aalink."' \">".$a->innertext ."</div>";
				}
				else
				{
					$mkid = explode("=",$url[1]);
					$mkid = $mkid[count($mkid)-1];
					$aalink = "http://www.betfair.com/exchange/football/competition?".$url[1]."&container=false&modules=nav&isAjax=true&ts=1393837442410&alt=json";
					$allLinks .="<div class='purchase_row getmenu' data-three='".$mkid."' style='cursor:pointer;' data-url='".$aalink."' >".$a->innertext ."</div>";	
				}
			   
			 }
		   }
		}
		$allLinks .= '</div>';
		return $allLinks;
	}
	
	function getMenuLinksDB($comp=0,$event=0,$market=0,$data,$val)
	{
		$allLinks ='';
		if($event!=0){
		if($val==3)
		{
			if($event){
			echo '<ul id="buy_sports" class="place_sports_list" style="display: block;">';
			foreach($event as $val1){
				$allLinks .="<li class='col-lg-4 col-md-4 col-sm-6 col-xs-12 list_eventByNav' style='width:100%;' data-one='".$val1['competition_id']."' data-two='".$val1['navigation_id']."' data-three='".$val['parent_id']."' data-four='".$val['type']."'>".$val1['name']."</li>";
			}
			$alllinks ="</ul>";
		}
		else
		{
			echo "<span style='color:#ffffff'>There are currently no Competitions</span>";
		}
		}
		else if($val==1)
		{
			if($event){
			$i=0;
		foreach($event as $val1){
			foreach($val1 as $val111){
				$eventDate = $this->CI->betfair_model->convert_time_zone($val111['open_date'],$val111['timezone'],$data);
				$allLinks1 = date("l",strtotime($eventDate))." ".date("d/m/Y",strtotime($eventDate));
				if(strpos($val111['name'],'@')!==false)
				{
					$flag = 0;
					$explodeName = explode('@',$val111['name']);
				}
				else if(strpos($val111['name'],' v ')!== false)
				{
					$flag = 0;
					$explodeName = explode(' v',$val111['name']);
				}
				else
				{
					$flag = 1;
					$explodeName =$val111['name'];
				}
				if($flag==0)
				{
					$value = "<span>".$explodeName[0]."</span><p>vs</p><span>".$explodeName[1]."</span>";
				}
				else
				{
					$value ="<span>".$Expval[0]."</span>";
				}
				$allLinks .="<div class='tournament'>
				<div class='title'>".$allLinks1."</div>	
				<div class='teams getmarkets' data-one='".$val111['competition_id']."' data-two='".$val111['event_id']."' data-three='".$allLinks1."' data-four='".$value."'>".$value."
				</div>
				</div>";
				$i++;
			}
		}
	}
	else
	{
	echo "<span style='color:#ffffff'>There are currently no events</span>";		
	}
	
	}
		else if($val==2)
		{
			if($event){
			echo "<ul id='buy_sports' class='place_sports_list' style='display: block;'>";
			foreach($event as $val3){
				foreach($val3 as $val4){
				$allLinks .="<li class='col-lg-4 col-md-4 col-sm-6 col-xs-12 list_event' style='width:100%;' data-one='".$val4['competition_id']."' data-two='".$val4['navigation_id']."' data-three='".$val['type']."'>".$val4['name']."</li>";
				}
			}
			$allLinks .="</ul>";
		}
		else
		{
			echo "<span style='color:#ffffff'>There are currently no Competitions</span>";	
		}
		}
		else if($val==4)
		{
			if($event){
			foreach($event as $val1){
			foreach($val1 as $val111){
				$eventDate = $this->CI->betfair_model->convert_time_zone($val111['open_date'],$val111['timezone'],$data);
				$allLinks1 = date("l",strtotime($eventDate))." ".date("d/m/Y",strtotime($eventDate));
				if(strpos($val111['name'],'@')!==false)
				{
					$flag = 0;
					$explodeName = explode('@',$val111['name']);
				}
				else if(strpos($val111['name'],' v ')!== false)
				{
					$flag = 0;
					$explodeName = explode(' v',$val111['name']);
				}
				else
				{
					$flag = 1;
					$explodeName =$val111['name'];
				}
				if($flag==0)
				{
					$value = "<span>".$explodeName[0]."</span><p>vs</p><span>".$explodeName[1]."</span>";
				}
				else
				{
					$value ="<span>".$Expval[0]."</span>";
				}
				$allLinks .="<div class='tournament'>
				<div class='title'>".$allLinks1."</div>	
				<div class='teams getmarkets' data-one='".$val111['competition_id']."' data-two='".$val111['event_id']."' data-three='".$allLinks1."' data-four='".$value."'>".$value."
				</div>
				</div>";
			}
		}
	}
	else
	{
	echo "<span style='color:#ffffff'>There are currently no events</span>";	
	}
		}
		else if($val==5)
		{
			echo '<ul id="buy_sports" class="place_sports_list" style="display: block;">';
			foreach($event as $val1){
			foreach($val1 as $val111){
				$allLinks .="<li class='col-lg-4 col-md-4 col-sm-6 col-xs-12 list_event' style='width:100%;' data-one='".$val111['competition_id']."'>".$val111['name']."</li>";
				
			}
		}
		$alllinks ="</ul>";
		}
		}
	
	else {
		if($market) {
			$sess = $this->CI->session->userdata('date');
			if(isset($sess))
			{
				$date = $this->CI->session->userdata('date');
				$event_name =$this->CI->session->userdata('event_name');
			}
			else
			{
				$date = '';
				$event_name = '';
			}
			$allLinks .="<div class='tournament'>
				<div class='title'>".$date."</div>	
				<div class='teams'>".$event_name."</div>
				</div><ul class='row sports_list com_list'>";
					
		foreach($market as $val1){
			
			$allLinks .="<li class='col-lg-4 col-md-4 col-sm-6 col-xs-12 marketsodd' data-one='".$val1['event_id']."' data-two='".$val1['market_id']."' data-three='".$val1['competition_id']."'>
				<a href='#'>".$val1['market_name']."</a></li>";
		}
		$allLinks .="</ul>";
	}
	else
	{
		echo "<span style='color:#ffffff'>There are currently no Markets</span>";
	}
		
		}
		$allLinks .="</div>";
	return $allLinks;
	}
}
?>

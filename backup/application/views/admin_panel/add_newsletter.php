<link rel="stylesheet" type="text/css" href="<?php print base_url();?>assets/js/yui/build/calendar/assets/skins/sam/calendar.css" />
<script type="text/javascript" src="<?php print base_url();?>assets/js/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="<?php print base_url();?>assets/js/yui/build/calendar/calendar-min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php print base_url();?>assets/js/yui/build/calendar/assets/skins/sam/calendar.css" />
<div id="box">
<h3><?php print 'Add Newsletter';//@$section_heading; ?>

</h3>
<br>
<script language="JavaScript" src="<?php print base_url();?>assets/js/gen_validatorv31.js" type="text/javascript"></script>
<form name="cataddfrm" method="post" action="" enctype="multipart/form-data">

<style>
.redtext
{
	color:#ff0000;
}
table,tr,td
{
	border:none;
}
</style>


<div class="redtext"><?php echo validation_errors(); ?></div>
<input type="hidden" value="<?php echo $this->uri->segment(4); ?>" name="productcategory" id="productcategory" >
<table width="500" border="0">

  <tr height="30">
    <td width="150">Subject&nbsp;<em class="redtext">*</em></td>
    <td>
        <input class="textbox" type="text" name="subject" id="subject">
    </td>
  </tr>

  <tr>
  <td valign="top">From:&nbsp;<em class="redtext">*</em></td>
  <td>
  <div>
    <input class="textbox" type="text" name="from" id="from">
  </div>
  </td>
  </tr>
  <tr>
			<td>Newsletter Body:&nbsp;<em class="redtext">*</em></td>
			<td>
			<?php
			if(strstr($_SERVER['HTTP_HOST'], 'whispertip/beta'))
			{
				$server = $_SERVER['DOCUMENT_ROOT'];
			}
			else
			{
				$server = FCPATH;
			}
			$this->load->file($server."/assets/ckeditor/ckeditor.php",true);
			$ckeditor = new CKEditor();
			$ckeditor->basePath = base_url().'assets/ckeditor/';
			$ckeditor->config['width'] = 600;
			$ckeditor->editor('content1',$this->input->get_value('content1',''));
			?>
			</td>
		</tr>
		
		<tr>
			<td>From:&nbsp;<em class="redtext">*</em></td>
			<td style="float: left; margin-top: 15px;">
				 <?php $mnt=$this->input->get_value('monthly','', 'return'); 
			
		?>
			<input  name="monthly" type="radio" id="monthly" <?php if($mnt=='monthly'){ print'checked="checked"';}?> value="monthly" onClick="showoption(this.value);">&nbsp;Send monthly
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input name="monthly" type="radio" id="monthly" <?php if($mnt=='later'){ print'checked="checked"';}?> value="later" onClick="showoption(this.value);">&nbsp;Send later
			
			 <div id="spLater" class="yui-skin-sam"  <?php if($mnt=='later'){ print'style="display:block;margin:0 410px 0 5px;float:right;padding:0px;clear:block;"';} else { print'style="display:none;margin:0 410px 0 5px;float:right;padding:0px;clear:none;"';}?>>
        	<input type="text" name="senddate" id="senddate" class="textbox" style="width:70px;height:14px;margin-top: 10px;" maxlength="0">&nbsp;<img id="show1up" src="<?php print base_url().'assets/images/calendar-icon.png';?>" border="0" align="top" style="cursor:pointer;margin-top:9px;" >
            <div id="cal2Container"></div>            
        </div>
        <div id="spMonthly" <?php if($mnt=='monthly'){ print'style="display:block;margin:-22px 310px 0 5px;float:right;padding:0px;clear:block;"';} else { print'style="display:none;margin:-22px 310px 0 5px;float:right;padding:0px;clear:none;"';}?>>
        	<span style="margin-left: 20px;">on day</span>&nbsp;&nbsp;
            <select  name="send_monthly_day" id="send_monthly_day">
  			<?php
  			for($i=1;$i<=30;$i++)
  			{
  				print '<option value="'.$i.'">'.$i.'</option>';
			}
			?>
  			</select>
			</td>
		</tr>
		
  <tr height="30">
	  <td>Active:</td>
	  <td>
		  <?php $chk=$this->input->get_value('active', '', 'return');?>

    <input class="checkbox" type="checkbox" name="active" id="active"  value="1" <?php if($chk == 1) print 'checked="checked"';?>/></td>
	  </tr>
  <tr height="30">
  <td></td>
  <td>
    <input  class="button" type="submit" name="add" id="add" value="Add" style="width:55px;" />&nbsp;&nbsp;
    <input class="button"  style="width:65px;" type="button" name="btncancel" id="btncancel" value="Cancel" onclick="cancel();" />
    
  </td>
  </tr>
  <input type='hidden' name="type" value="add">
 </table>
</form>

</div>
<script type="text/javascript">
var frmvalidator = new Validator("cataddfrm");
frmvalidator.addValidation("subject","req","Please fill the subject field");
frmvalidator.addValidation("from","req","Please fill the from field");
frmvalidator.addValidation("from","email","Please Enter Valid email");
frmvalidator.addValidation("content1","req","Please fill the newsletter body field");
frmvalidator.addValidation("monthly","selone_radio","Please select month or date");
	function cancel()
	{
	var url='<?php echo base_url();?>admin_panel/email/manage_newsletter';
	
	location.href=url;
	
	}



function showoption(val)
{
	if(val=="monthly")
	{
		document.getElementById("spMonthly").style.display = "block";
		document.getElementById("spLater").style.display = "none";
	}
	else
	{
	document.getElementById("spMonthly").style.display = "none";
	document.getElementById("spLater").style.display = "block";
	}
}
</script>
<script type="text/javascript">
	YAHOO.namespace("example.calendar");

	YAHOO.example.calendar.init = function() {

		function handleSelect(type,args,obj) {
			var dates = args[0]; 
			var date = dates[0];
			var year = date[0], month = date[1], day = date[2];
			
			var txtDate1 = document.getElementById("senddate");
			txtDate1.value = month + "/" + day + "/" + year;
			YAHOO.example.calendar.cal2.hide();
		}

		YAHOO.example.calendar.cal2 = new YAHOO.widget.Calendar("cal2","cal2Container", { title:"Pick a date", close:true } );
		YAHOO.example.calendar.cal2.selectEvent.subscribe(handleSelect, YAHOO.example.calendar.cal2, true);
		YAHOO.example.calendar.cal2.render();
		YAHOO.util.Event.addListener("show1up", "click", YAHOO.example.calendar.cal2.show, YAHOO.example.calendar.cal2, true);
	}

	YAHOO.util.Event.onDOMReady(YAHOO.example.calendar.init);
</script>						


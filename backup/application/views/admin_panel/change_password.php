<script language="JavaScript" src="<?php print base_url();?>assets/js/gen_validatorv31.js" type="text/javascript"></script>
<div style="color:#FF0000"><?php echo validation_errors(); ?></div>
<form name="frmChangePass" id="frmChangePass" action="" method="post">

	<table width="100%" cellpadding="0" cellspacing="0" border="0">
    	<tr height="30">
        	<td width="180">
				<?php print $this->lang->line('admin_old_password');?>:<span class="span_compulsory">*</span>
            </td>
            <td>
                <input type="password" class="textbox" name="old_password" id="old_password" value=""  />
            </td>
        </tr>
        
        <tr height="30">
        	<td width="180">
				<?php print $this->lang->line('admin_new_password');?>:<span class="span_compulsory">*</span>
            </td>
            <td>
                <input type="password" class="textbox" name="new_password" id="new_password" value=""  />
            </td>
        </tr>
        
        <tr height="30">
        	<td width="220">
				<?php print $this->lang->line('admin_confirm_password');?>:<span class="span_compulsory">*</span>
            </td>
            <td>
                <input type="password" class="textbox" name="confirm_password" id="confirm_password" value=""  />
            </td>
        </tr>
        
        <tr height="30">
            <td></td>
            <td>
                <div style="float:left;"><input type="submit" value="<?php print $this->lang->line('admin_edit');?>" class="button" style="width:65px;"/><div class="buttonEnding"></div></div>
                <div style="float:left;"><input type="button" value="<?php print $this->lang->line('admin_cancel');?>" class="button" onclick="javascript:document.location.href='<?php print base_url();?>admin_panel/home';" style="width:65px;"/><div class="buttonEnding"></div></div>
            </td>
        </tr>
     </table>
</form>
<script language="javascript">
var frmvalidator = new Validator("frmChangePass");
frmvalidator.addValidation("old_password","req","<?php print $this->lang->line('validation_required', $this->lang->line('admin_old_password'));?>");
frmvalidator.addValidation("new_password","req","<?php print $this->lang->line('validation_required', $this->lang->line('admin_new_password'));?>");
frmvalidator.addValidation("confirm_password","req","<?php print $this->lang->line('validation_required', $this->lang->line('admin_confirm_password'));?>");

document.getElementById("old_password").focus();
</script>

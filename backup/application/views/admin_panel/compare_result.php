<?php 
if($_SERVER['HTTP_HOST']=='localhost')
{
	global $ssl_crt ;
	global $ssl_key ;
	$ssl_crt = "/var/www/betfair/ssl/client-2048.crt";
	$ssl_key = "/var/www/betfair/ssl/client-2048.key";
}
else
{
	global $ssl_crt ;
	global $ssl_key ;
	$ssl_crt = "/var/ssl_betfair/client-2048.crt";
	$ssl_key = "/var/ssl_betfair/client-2048.key";
}

function certlogin($appKey,$params)
{
	global $ssl_crt;
	global $ssl_key;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://identitysso-api.betfair.com/api/certlogin");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSLCERT, $ssl_crt);
    curl_setopt($ch, CURLOPT_SSLKEY, $ssl_key);

	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey
    ));

    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $response = json_decode(curl_exec($ch));
   
    curl_close($ch);

    if ($response->loginStatus == "SUCCESS") {
        return $response;
    } else {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    }
}
function debug($debugString)
{
    global $DEBUG;
    if ($DEBUG)
        echo $debugString . "\n\n";
}

function sportsApingRequest($appKey, $sessionToken, $operation, $params,$market_country)
{
    $ch = curl_init();
    if($market_country =='aus'){
		curl_setopt($ch, CURLOPT_URL, "https://api-au.betfair.com/exchange/betting/json-rpc/v1");
	}
	else{
		curl_setopt($ch, CURLOPT_URL, "https://api.betfair.com/exchange/betting/json-rpc/v1");
	}
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey,
        'X-Authentication: ' . $sessionToken,
        'Accept: application/json',
        'Content-Type: application/json'
    ));

    $postData =
        '[{ "jsonrpc": "2.0", "method": "SportsAPING/v1.0/' . $operation . '", "params" :' . $params . ', "id": 1}]';

    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

    //debug('Post Data: ' . $postData);
    $response = json_decode(curl_exec($ch));
    //debug('Response: ' . json_encode($response));
 //$info = curl_getinfo($ch);
// echo '<pre>';print_r($response);die;
    curl_close($ch);

    if (isset($response[0]->error)) {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    } else {
        return $response;
    }
   


}
function getNextRacingMarket($appKey, $sessionToken, $horseRacingEventTypeId,$market_country)
{

    $params = '{"filter":{"eventIds":["' . $horseRacingEventTypeId . '"],
              "marketTypeCodes":["'.$markType.'"]},
              "sort":"FIRST_TO_START",
              "maxResults":"1",
              "marketProjection":["RUNNER_DESCRIPTION"]}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketCatalogue', $params,$market_country);
	if($jsonResponse[0]->result)
	{
		return $jsonResponse[0]->result[0];
	}
	else
	{
			return '';
	}
}
function getMarketBook($appKey, $sessionToken, $marketId,$market_country)
{
   $params = '{"marketIds":["' . $marketId . '"]}';

	$jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketBook', $params,$market_country);
	if($jsonResponse[0]->result)
	{
		return $jsonResponse[0]->result[0];
	}
	else
	{
			return '';
	}
}
	
	$appKey = '4ae3bb5f6f042268359bba5d24dd3c2ecb1b2f37';
	$params = 'username=nlarkins&password=budda2468';
	$resp = certlogin($appKey,$params);
	
	$sessionToken = 'b0olRvuT4forfmXWaUKH59/W0mlmbizDcP7jOWVegLo=';
	$sessionToken = $resp->sessionToken;
//echo $sessionToken;die;
	$appKey1 = '97Wtq7sjReOr0uWk';
?>
<div id="box">
<h3><?php print 'Result';
?></h3>
<?php
	$parts  = explode('.', (string)$market_id);	
		if($parts[0]==2)
		{
			$market_country = 'aus';
		}
		else
		{
			$market_country = 'inter';
		}
	$marketType2 = getMarketBook($appKey1, $sessionToken, $market_id,$market_country);
	$api_result = '';
	if($marketType2){
		if(trim($marketType2->status)=="CLOSED")
		{
			if(count($marketType2) && isset($marketType2->runners))
			{
				foreach($marketType2->runners as $runner)
					{
						if(isset($runner->status))
						{
							$team1_st = $runner->status;
							if(trim($team1_st)=='WINNER')
							{
								$selectionId = $runner->selectionId;
							}
						}
					}
			}
			$api_result = $this->result_model->getRunnerById($market_id,$selectionId);
		}
}else{
		$api_result = '';
	}

?>
<form name="result_update" method="POST">
	<span style="color:red"><?php echo validation_errors(); ?></span>
	<table id="tblGrid" width="100%" border="0" cellspacing="12" class="borderclass">
			<tr class="rowheader" height="30" >
				<td style="padding-left:5px;"><b>Result As per as betafair API: WINNER:- <?php echo $api_result ? $api_result['runner_name'] : 'No Result available';?></b></td>
				</tr><tr>
					<?php if($sport_result=='open'){ ?>
						<td style="padding-left:5px;">Market is still open.</td>
						<?php }else if($sport_result=='no_record'){ ?>
							<td style="padding-left:5px;">There are currently no records for runners in database.</td>
							<?php } else if($sport_result['runner_name']){?>
							<td style="padding-left:5px;"><b>Result As per as Whispertip:  WINNER:- <?php echo $sport_result ? $sport_result['runner_name'] : 'No Result available In database';?></b></td>
				<?php }?>
			</tr>
			</table>
			<br>
			<h3>Runners</h3>
			<table id="tblGrid" width="100%" border="0" cellspacing="12" class="borderclass">
			<tr class="rowheader" height="30" >
				<td style="padding-left:5px;"><b>Select</b></td>
				<td style="padding-left:5px;"><b>Runner Name</b></td>
				<td style="padding-left:5px;"><b>Selection Id</b></td>
			</tr>
			<?php 
			if($category_results){
				
			foreach($category_results as $runner){
				$chk = '';
				if($sport_result){
					if($runner['selection_id'] == $sport_result['selection_id']){
						$chk = 'checked=checked';
						}
					}
				?>
			<tr class="rowheader" height="30" >
				<td style="padding-left:5px;"><input type="radio" name="radio_runner" value="<?php echo $runner['selection_id'];?>"  <?php echo $chk;?> ></td>
				<td style="padding-left:5px;"><?php echo $runner['runner_name'];?></td>
				<td style="padding-left:5px;"><?php echo $runner['selection_id'];?></td>
			</tr>
			<?php } }else{ ?>
				<tr class="rowheader" height="30" >
					<td  colspan="3"  style="text-align: center;">
					There are currently no runners in database.
					</td>
				<tr class="rowheader" height="30" >
				<?php }?>
	</table>
	<input type='hidden' value="<?php echo $market_id;?>" id="marketId" name="marketId">
	<div><input type="submit" value="Save result" style="cursor:pointer"></div>
</form>
</div>
</div>

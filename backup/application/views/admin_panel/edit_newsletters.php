<div id="box">
<h3><?php print 'Edit Newsletter';//@$section_heading; ?>

</h3>
<br>
<script language="JavaScript" src="<?php print base_url();?>assets/js/gen_validatorv31.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php print base_url();?>assets/js/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="<?php print base_url();?>assets/js/yui/build/calendar/calendar-min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php print base_url();?>assets/js/yui/build/calendar/assets/skins/sam/calendar.css" />

<style type="text/css">
	#cal2Container { display:none; position:absolute;z-index:1}
</style>
<style>
.table,td,tr
{
	border:none;
}
.redtext
{
 color:#FF0000;
}
</style>


<style>
.yui-editor-container {
    padding-bottom: 2px;
}
</style>
<div class="yui-skin-sam">
<form name="pageeditform" method="post" action="">
<div class="redtext"><?php echo validation_errors(); ?></div>

<table width="100%" border="0" cellspacing="12">
  <tr>
    <td><b>Subject:</b>&nbsp;<em class="redtext">*</em></td>
    <td>
        <input class="textbox" type="text" name="subject" id="pagetitle" value="<?php $this->input->get_value('subject',@$newsletter['subject'], 'print'); ?>">
    </td>
  </tr>
  <tr>
    <td valign="top"><b>From:</b>&nbsp;<em class="redtext">*</em></td>
    <td>
    <input class="textbox" type="text" name="from" id="pagesubtitle" value="<?php $this->input->get_value('from',@$newsletter['from_email'], 'print'); ?>"/>
      </td>
  </tr>
  
  <tr>
  <td valign="top"><b>Newsletters Body:</b></td>
  <td>
		<div>
		<?php
		if(strstr($_SERVER['HTTP_HOST'], 'goodies.2.bluehatdesign.com'))
		{
			$server = $_SERVER['DOCUMENT_ROOT'];
		}
		else
		{
			$server = '/var/www/nucynergy/development/nucynergy';
		}
		$this->load->file($server."/assets/ckeditor/ckeditor.php",true);
		$ckeditor = new CKEditor();
		$ckeditor->basePath = base_url().'assets/ckeditor/';
		$ckeditor->config['width'] = 600;
		$ckeditor->editor('content1',$this->input->get_value('content1',@$newsletter['body'], ''));
		?>
		</div>	

		    
  
  </td>
  
  </tr>
  <tr height="30">
  	<td></td>
    <td>
    	<div style="padding:0 0 0 0;margin:0 0 0 0;">
        
        <input  name="monthly" type="radio" id="monthly" value="monthly" <?php if(@$newsletter['send_monthly'] == 1) print 'checked="checked"';  ?> onClick="showoption(this.value);">&nbsp;Send monthly
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    	<input name="monthly" type="radio" id="latter" value="later" <?php
		$dt=@$newsletter['send_date']; 
		if(@$newsletter['send_date']) print 'checked="checked"';  ?> 
        onClick="showoption(this.value);">&nbsp;Send later
        <div id="spLater" <?php 
		if(@$newsletter['send_date'])
		{
		print'style="display:block;margin:0 410px 0 5px;float:right;padding:0px;"';
		}
		else
		{
		print'style="display:none;margin:0 410px 0 5px;float:right;padding:0px;"';
		}
		?>>
        	<input type="text" name="senddate" id="senddate" value="<?php echo date("n/j/Y", strtotime($dt)); ?>" class="textbox" style="width:70px;" maxlength="0">&nbsp;<img id="show1up" alt="img" src="<?php print base_url().'assets/images/calendar-icon.png';?>" border="0" align="top" style="cursor:pointer;margin-top:1px;" >
            <div id="cal2Container"></div>            
        </div>
        <div id="spMonthly"   <?php if(@$newsletter['send_monthly'] == 1) 
		{
		print 'style="display:block;margin:0 410px 0 5px;float:right;padding:0px;clear:none"';
		}
		else
		{
		print 'style="display:none;  margin:0 410px 0 5px;float:right;padding:0px;clear:none"';
		}
		 ?>>
        	on day&nbsp;&nbsp;
            <select  name="send_monthly_day" id="send_monthly_day" >
  			<?php
  			for($i=1;$i<=30;$i++)
  			{
  				$selected = ($i == @$newsletter['send_monthly_day']) ? ' selected ' : '';
				print '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
			}
			?>
  			</select>
        </div>
      </div>
    </td>
  </tr>
 
 <tr>
  <td><b>Active:</b></td>
  <td><label>
  <?php $chk=$this->input->get_value('active', @$newsletter['active'], 'return');?>
    <input class="checkbox" type="checkbox" name="active" id="active"  value="1" <?php if($chk == 1) print 'checked="checked"';?>/>
  </label></td>
  </tr>
  <tr>
  <td></td>
  <td  colspan="3">
    <input type="submit" value="Edit" class="button" style="width:55px;"/>&nbsp;&nbsp;
    <input  type="hidden" name="editnewsletter" id="editnewsletter" value="editnewsletter" />
    &nbsp;&nbsp;
    <input class="button"  type="button" name="btncancel" id="btncancel" value="Cancel" style="width:65px;"
     onClick="javascript:document.location.href='<?php echo base_url();?>admin_panel/email/manage_newletter';"/>
  </td>
  </tr>
 </table>
</form>
</div>

</div>
<script type="text/javascript">
	
var frmvalidator = new Validator("pageeditform");
frmvalidator.addValidation("pagetitle","req","Field subject is requried.");
frmvalidator.addValidation("pagesubtitle","req","Field from is requried.");
frmvalidator.addValidation("pagesubtitle","email","Please enter valid Email");
frmvalidator.addValidation("monthly","selone_radio","Please select month or date");


function showoption(val)
{
	if(val=="monthly")
	{
		document.getElementById("spMonthly").style.display = "block";
		document.getElementById("spLater").style.display = "none";
	}
	else
	{
		document.getElementById("spMonthly").style.display = "none";
		document.getElementById("spLater").style.display = "block";
	}
	
}

</script>
<script type="text/javascript">
	var txtDate1 = document.getElementById("senddate");
	YAHOO.namespace("example.calendar");
	YAHOO.example.calendar.init = function() {

		function handleSelect(type,args,obj) {
			var dates = args[0]; 
			var date = dates[0];
			var year = date[0], month = date[1], day = date[2];
			
			
			txtDate1.value = month + "/" + day + "/" + year;
			YAHOO.example.calendar.cal2.hide();
		}

		YAHOO.example.calendar.cal2 = new YAHOO.widget.Calendar("cal2","cal2Container", { title:"Pick a date", close:true, mindate: "<?php print date("n/j/Y");?>" } );
		YAHOO.example.calendar.cal2.selectEvent.subscribe(handleSelect, YAHOO.example.calendar.cal2, true);
		YAHOO.example.calendar.cal2.select(txtDate1.value);
		YAHOO.example.calendar.cal2.render();
		YAHOO.util.Event.addListener("show1up", "click", YAHOO.example.calendar.cal2.show, YAHOO.example.calendar.cal2, true);
	}

	YAHOO.util.Event.onDOMReady(YAHOO.example.calendar.init);
</script>						

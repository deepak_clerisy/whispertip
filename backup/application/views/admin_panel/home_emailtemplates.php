<div id="box">
<h3><?php print 'Manage Email Templates';//@$section_heading; ?>

</h3>
<br>

<span style="float:left; padding-right:5px;margin-left:8px;">Search Email Template</span>
<input type="text" class="textbox" style="float:left;" name="txtSearch" id="txtSearch" value="<?php print $txtSearch; ?>" onBlur="javascript: stripIt(this);" onKeyPress="javascript: SubmitSearch(this, event)"> &nbsp;
<input type="button" style="width:65px;" onClick="javascript: SubmitSearch(this, event)" value="<?php print $this->lang->line('admin_search');?>" class="button"/><div class="buttonEnding"></div>

<table width="100%">
<tr>
<td align="right" style="border:none;">
<div style="float:right; margin-bottom:5px;"><?php print $this->pagination->create_links(); ?></div>
</td>
</tr>
</table>

<table width="100%"  cellpadding="0" cellspacing="0" border="0">
	<tr class="rowheader" height="30" style="background-color:#DAEAED;">
    	<td><b><?php print $this->lang->line('admin_templates_name');?></b></td>
        <td><b><?php print $this->lang->line('admin_subject_name');?></b></td>
    	<td><b><?php print $this->lang->line('admin_active');?></b></td>
    	<td><b><?php print $this->lang->line('admin_actions');?></b></td>
    </tr>
    <?php
	if(count($emailtemplate)>0)
	{
		$idx=0;
		$start_position = $page_number;
		$end_position = ($start_position + $page_size) - 1;
		foreach ( $emailtemplate as $row )
		{
			$rowclass = (($idx % 2)==0) ? ' class="row2" ' : ' class="row1" ';
			$active = $row['active']==1 ? '<font color="green">Yes</font>' : '<font color="red">No</font>';
			print '
			<tr '.$rowclass.' height="25">
				<td>'.$row['template_name'].'</td>
				<td>'.$row['subject'].'</td>
				<td>'.$active.'</td>
				<td>
					<a href="'.base_url().'admin_panel/email/edit_emailtemplate/'.$row['email_template_id'].'">
					<img src="'.base_url().'assets/css/admin/img/icons/edit_pencil1.png" title="'.$this->lang->line('admin_edit').'" width="16"/></a> 
				</td>
			</tr>
				';
				$idx++;
		}
	}
	else
	{
		print '
			<tr class="row1">
				<td colspan="4">'.$this->lang->line('admin_no_records_found').'</td>
			</tr>';
	}
	?>
</table>

</div>
<script type="text/javascript">

function SubmitSearch(element, e)
{
	if(element.type=="text")
	{
		var keycode;
		if (e)
		{
		  keycode = e.which;
			if (keycode == 13)
			{
				var sstr = document.getElementById("txtSearch").value;
				turl = '<?php print base_url();?>admin_panel/email/index/0/'+sstr;
				location.href = turl;
			}
		}
	}
	
	if(element.type=="button")
	{
		var sstr = document.getElementById("txtSearch").value;
		turl = '<?php print base_url();?>admin_panel/email/index/0/'+sstr;
		location.href = turl;
	}
}
function stripIt(x)
{
	x.value = x.value.replace(/['"]/g,'');
}
document.getElementById("txtSearch").focus();
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $page_title; ?></title>
<link rel="stylesheet" type="text/css" href="<?php print base_url();?>assets/css/admin/css/style.css" />
<link rel="stylesheet" type="text/css" href="<?php print base_url();?>assets/css/admin/css/theme1.css" />
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="<?php print base_url();?>assets/css/admin/css/ie-sucks.css" />
<![endif]-->
<script language="JavaScript" src="<?php print base_url();?>assets/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script language="JavaScript" src="<?php print base_url();?>assets/js/gen_validatorv31.js" type="text/javascript"></script>
</head>
<body style="background-color:#375a90;">
<div id="container" style="background-color:#375a90;height:100%; width:100%">
  <div id="header" style="height:200px;background-color:#375a90;"></div>
  <div class="welcome" style="text-align:center; width:100%; font-size:20px; margin-top:-50px; float:left;">
  <div style="width:405px;font-family: Arial,Tahoma,Verdana;font-size: 12px;position:absolute;margin-left:480px;margin-top:-75px;">
		<?php 
			if(isset($error))print '<p style="background:#912222; color:#fff; font-weight:bold;">'.$error.'</p>';
			if(isset($success))print '<p style="background:#4E7440; color:#fff; font-weight:bold; ">'.$success.'</p>';
			if(isset($message))print '<p style="background:#FFD900; color:#000; font-weight:bold; ">'.$message.'</p>';
		?>
  </div>
  <div style="float:left;width:100%">
	  WHISPERTIPS
  </div>
  <?php print 'Admin Panel';//$this->session->userdata('website_domain');?></div>
   <div class="login_outer">
    <div class="top_bg"> </div>
    <div class="middle_bg">
	  <form id="form" name="formlogin" method="post" action="" >
        <fieldset id="personal" style="padding-left:45px;">
        <legend style="width:180px;margin-left:-30px;padding-left:8px;"><?php print $this->lang->line('admin_login');?></legend>
        <label for="lastname" style="width:200px; text-align:left;"><?php print $this->lang->line('admin_label_username');?> :</label>
        <div style="clear:both"></div>
        <input name="username" id="username" type="text" tabindex="1"  style="width:250px;"/><br />
        <label for="firstname" style="width:200px; text-align:left;"><?php print $this->lang->line('admin_label_password');?> :</label>
        <div style="clear:both"></div>
        <input name="password" id="password" type="password" tabindex="2" style="width:250px;"/><br />
        <div style="float:left; margin-top:10px; height:auto; width:300px;">
          <div class="youhave">
          	<input id="button1" name="submit" type="submit" value="<?php  print $this->lang->line('admin_btn_login'); ?>"  style="width:95px;margin-left:0px;margin-right:42px;"/>
          </div>
        </div>
        </fieldset>
      </form>
    </div>
    <div class="bottom_bg"></div>
  </div>
</div>
<script language="javascript">
var frmvalidator = new Validator("form");
frmvalidator.addValidation("username","req","username field is required");
frmvalidator.addValidation("password","req","password field is required");
document.getElementById("username").focus();
</script>
</body>
</html>

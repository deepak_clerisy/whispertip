<div id="box">
<h3><?php print 'Manage Categories';//@$section_heading; ?>

</h3>

<table width="100%">
<tr>
	<td align="right" style="border:none;">
	<div style="float:right; margin-bottom:5px;"></div>
	</td>
</tr>
</table>
<span style="float:right;margin-right:8px;">
<input type="button" style="width:125px;" onClick="location.href='<?php print base_url();?>admin_panel/category/add_category'" value="<?php print $this->lang->line('admin_Add_Category');?>" class="button"/>
</span>
<table width="100%"  cellpadding="0" cellspacing="0" border="0">
	<tr class="rowheader" height="30" style="background-color:#F7F6F1;color:#375B91;">
    	<td><b><?php print "Category Image";?></b></td>
    	<td><b><?php print $this->lang->line('admin_category_name');?></b></td>
    	<td><b>Active</b></td>
    	<td><b><?php print $this->lang->line('admin_action');?></b></td>
    </tr>
    <?php
    
if(count($categories) > 0)
{		
	$idx=1;
	$totalNum = count($categories);	

	foreach($categories as $category)
	{	
		$rowclass = (($idx % 2)==0) ? ' class="row1" ' : ' class="row2" ';
		print '
		<tr '.$rowclass.' height="25">
			<td><img src="'.base_url().'uploads/category_images/thumb_'.$category['image'].'" width="40px;"></td>
			<td>'.$category['name'].'</td>';
		if($category['active']) 
			{ ?>
					<td><a href="<?php echo base_url();?>admin_panel/category/active_inactive/<?php echo $category['category_id']?>/1">			
					<img src="<?php echo base_url();?>assets/images/tick.gif" title="click to deactivate">
					</a>
					</td>
				<?php
				 } else { ?>
					<td><a href="<?php echo base_url();?>admin_panel/category/active_inactive/<?php echo $category['category_id']?>/0"><img src="<?php echo base_url();?>assets/images/cross.gif" title="click to activate"></a></td>
				<?php }			
			print '<td>        	
			<a href="'.base_url().'admin_panel/category/edit_category/'.$category['category_id'].'">Edit Category</a>
		   </td>
		</tr>
		';

		$idx++;
		
	}
 }
	else
	{
	print '
		<tr class="row1">
			<td colspan="5">'.$this->lang->line('admin_no_records_found').'</td>
		</tr>';
	}
	?>
</table>
</div>
<script type="text/javascript">
function ConfirmDelete(userid)
{
	if( confirm("<?php print $this->lang->line('admin_category_deletion');?>") == true)
	{
		turl = '<?php print base_url();?>admin_panel/shopping/delete_categories/'+userid;
		location.href=turl;
		
	}

}
function SubmitSearch(element, e)
{
	if(element.type=="text")
	{
		var keycode;
		if (e)
		{
		  keycode = e.which;
			if (keycode == 13)
			{
				var sstr = document.getElementById("txtSearch").value;
				turl = '<?php print base_url();?>admin_panel/shopping/index/0/'+sstr;
				location.href = turl;
			}
		}
	}
	
	if(element.type=="button")
	{
		var sstr = document.getElementById("txtSearch").value;
		turl = '<?php print base_url();?>admin_panel/shopping/index/0/'+sstr;
		location.href = turl;
	}
}
function stripIt(x)
{
	x.value = x.value.replace(/['"]/g,'');
}

document.getElementById("txtSearch").focus();
function downDisplayOrder(category_id,display_order)
{
	xajax_down_display_order(category_id,display_order);
}
function upDisplayOrder(category_id,display_order)
{
	xajax_up_display_order(category_id,display_order);
}
</script>

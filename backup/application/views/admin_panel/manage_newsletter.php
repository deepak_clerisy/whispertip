<div id="box">
<h3><?php print 'Manage Newsletter';//@$section_heading; ?>

</h3>

<table width="100%">
<tr>
	<td align="right" style="border:none;">
	<div style="float:right; margin-bottom:5px;"></div>
	</td>
</tr>
</table>
<span style="float:right;margin-right:8px;">
<input type="button" style="width:125px;" onClick="location.href='<?php print base_url();?>admin_panel/email/add_newsletter'" value="Add Newsletter"class="button"/>
</span>
<table width="100%"  cellpadding="0" cellspacing="0" border="0">
	<tr class="rowheader" height="30" style="background-color:#F7F6F1;color:#375B91;">
    	<td><b>Subject</b></td>
    	<td><b>Date Created</b></td>
    	<td><b>Scheduled On</b></td>
    	<td><b>Active</b></td>
    	<td><b>Action</b></td>
    </tr>
   <?php if($newsletter_fields) {
	   $idx=0;
	  foreach($newsletter_fields as $row)
		{ 
		if(@$row['send_monthly'] == 1)
			{
			$mnt='every month on day'.' '.$row['send_monthly_day'];
			
			}
			else
			{
			$mnt=date("n/j/Y", strtotime($row['send_date']));
			}

			
			$rowclass = (($idx % 2)==0) ? ' class="row2" ' : ' class="row1" ';
			$active = $row['active']==1 ? '<font color="green">Yes</font>' : '<font color="red">No</font>';
			print '
			<tr '.$rowclass.' height="25">
				<td>'.$row['subject'].'</td>
				<td>'.date("m/d/Y", strtotime($row['date_created'])).'</td>
				<td>'.$mnt.'</td>
				<td>'.$active.'</td>
				<td>
					<a href="'.base_url().'admin_panel/email/edit_newsletter/'.$row['newsletter_id'].'"><img src="'.base_url().'assets/css/admin/img/icons/edit_pencil1.png" title="'.$this->lang->line('admin_edit').'" width="16"/></a>
				    <a href="javascript:void(0);" onclick="ConfirmDelete('.$row['newsletter_id'].')"><img src="'.base_url().'assets/css/admin/img/icons/delete_round.gif" title="Are you sure you want to delete ?" width="16"/></a>   
				</td>
			</tr>
			';
				$idx++;
	} 
	   } else { ?>
			<tr class="row1">
				<td colspan="5" align="center">There are currently no Newsletter</td>
			</tr>
		   
	<?php 	 }?>
</table>

</div>
<script type="text/javascript">
function ConfirmDelete(userid)
{
	if( confirm("<?php print $this->lang->line('admin_category_deletion');?>") == true)
	{
		turl = '<?php print base_url();?>admin_panel/email/delete_newsletter/'+userid;
		location.href=turl;
		
	}

}
function SubmitSearch(element, e)
{
	if(element.type=="text")
	{
		var keycode;
		if (e)
		{
		  keycode = e.which;
			if (keycode == 13)
			{
				var sstr = document.getElementById("txtSearch").value;
				turl = '<?php print base_url();?>admin_panel/shopping/index/0/'+sstr;
				location.href = turl;
			}
		}
	}
	
	if(element.type=="button")
	{
		var sstr = document.getElementById("txtSearch").value;
		turl = '<?php print base_url();?>admin_panel/shopping/index/0/'+sstr;
		location.href = turl;
	}
}
function stripIt(x)
{
	x.value = x.value.replace(/['"]/g,'');
}

document.getElementById("txtSearch").focus();
function downDisplayOrder(category_id,display_order)
{
	xajax_down_display_order(category_id,display_order);
}
function upDisplayOrder(category_id,display_order)
{
	xajax_up_display_order(category_id,display_order);
}
</script>

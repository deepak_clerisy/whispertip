<div id="box">
<h3><?php print 'Manage Pages';//@$section_heading; ?>

</h3>
<br>
<span style="float:left; padding-right:5px;margin-left:8px;">Search Page</span>
<input type="text" class="textbox" style="float:left;" name="txtSearch" id="txtSearch" value="<?php print $txtSearch; ?>" onblur="javascript: stripIt(this);" onkeypress="javascript: SubmitSearch(this, event)"> &nbsp;
<input type="button" style="width:65px;" onClick="javascript: SubmitSearch(this, event)" value="<?php print $this->lang->line('admin_search');?>" class="button"/><!--<div class="buttonEnding"></div>-->
<div style="float:right; margin-bottom:5px;margin-right:5px;"><?php print $this->pagination->create_links(); ?></div>
<table border="0" style="border:none;">
<tr  style="border:none;">
	<td style="border:none;"></td>
</tr>
</table>
<table width="100%"  cellpadding="0" cellspacing="0" border="0">
	<tr class="rowheader" height="30" style="background-color:#F7F6F1;color:#654322;">
    	<td><b><?php print $this->lang->line('admin_title');?></b></td>
        <td><b><?php print $this->lang->line('admin_date_created');?></b></td>
    	<td><b><?php print $this->lang->line('admin_active');?></b></td>
    	<td><b><?php print $this->lang->line('admin_actions');?></b></td>
    </tr>
    <?php
	if(count($pages)>0)
	{
		$idx=0;
		$start_position = $page_number;
		$end_position = ($start_position + $page_size) - 1;
		foreach ( $pages as $row )
		{

			$rowclass = (($idx % 2)==0) ? ' class="row2" ' : ' class="row1" ';
			$active = $row['active']==1 ? '<font color="green">Yes</font>' : '<font color="red">No</font>';
			print '
			<tr '.$rowclass.' height="25">
				<td width="35%">'.$row['page_title'].'</td>
				<td>'.date("m/d/Y", strtotime($row['date_modified'])).'</td>
				<td>'.$active.'</td>
				<td>
					<a href="'.base_url().'admin_panel/content/edit_page/'.$row['static_page_id'].'">
					<img src="'.base_url().'assets/css/admin/img/icons/edit_pencil1.png" title="'.$this->lang->line('admin_edit').'" width="16"/></a>   
				</td>
			</tr>
				';
				$idx++;
		}
	}
	else
	{
		print '
			<tr class="row1">
				<td colspan="4">'.$this->lang->line('admin_no_records_found').'</td>
			</tr>';
	}
	?>
</table>

</div>
<script type="text/javascript">
function ConfirmDelete(userid)
{
	if( confirm("<?php print $this->lang->line('admin_user_deletion');?>") == true)
	{
		turl = '<?php print base_url();?>admin_panel/content/delete_pages/'+userid;
		location.href=turl;
		
	}

}
function SubmitSearch(element, e)
{
	if(element.type=="text")
	{
		var keycode;
		if (e)
		{
		  keycode = e.which;
			if (keycode == 13)
			{
				var sstr = document.getElementById("txtSearch").value;
				turl = '<?php print base_url();?>admin_panel/content/index/0/'+sstr;
				location.href = turl;
			}
		}
	}
	
	if(element.type=="button")
	{
		var sstr = document.getElementById("txtSearch").value;
		turl = '<?php print base_url();?>admin_panel/content/index/0/'+sstr;
		location.href = turl;
	}
}
function stripIt(x)
{
	x.value = x.value.replace(/['"]/g,'');
}
document.getElementById("txtSearch").focus();
</script>

<div id="box">
<h3><?php print 'Manage Pages';//@$section_heading; ?>

</h3>
<br>
<table border="0" style="border:none;">
<tr  style="border:none;">
	<td style="border:none;"></td>
</tr>
</table>
<table width="100%"  cellpadding="0" cellspacing="0" border="0">
	<tr class="rowheader" height="30" style="background-color:#F7F6F1;color:#654322;">
    	<td><b><?php print $this->lang->line('admin_title');?></b></td>
        <td><b><?php print $this->lang->line('admin_date_created');?></b></td>
    	<td><b><?php print $this->lang->line('admin_actions');?></b></td>
    </tr>
    <?php
	if(count($field)>0)
	{
		foreach ($field as $row )
		{
			print '
			<tr height="25">
				<td width="35%">'.$row['page_title'].'</td>
				<td>'.date("m/d/Y", strtotime($row['date_created'])).'</td>
				<td>
					<a href="'.base_url().'admin_panel/content/place_edit_page/'.$row['how_it_id'].'">
					<img src="'.base_url().'assets/css/admin/img/icons/edit_pencil1.png" title="'.$this->lang->line('admin_edit').'" width="16"/></a>   
				</td>
			</tr>
				';
		}
	}
	else
	{
		print '
			<tr class="row1">
				<td colspan="4">'.$this->lang->line('admin_no_records_found').'</td>
			</tr>';
	}
	?>
</table>

</div>
<script type="text/javascript">
function ConfirmDelete(userid)
{
	if( confirm("<?php print $this->lang->line('admin_user_deletion');?>") == true)
	{
		turl = '<?php print base_url();?>admin_panel/content/delete_pages/'+userid;
		location.href=turl;
		
	}

}
function SubmitSearch(element, e)
{
	if(element.type=="text")
	{
		var keycode;
		if (e)
		{
		  keycode = e.which;
			if (keycode == 13)
			{
				var sstr = document.getElementById("txtSearch").value;
				turl = '<?php print base_url();?>admin_panel/content/index/0/'+sstr;
				location.href = turl;
			}
		}
	}
	
	if(element.type=="button")
	{
		var sstr = document.getElementById("txtSearch").value;
		turl = '<?php print base_url();?>admin_panel/content/index/0/'+sstr;
		location.href = turl;
	}
}
function stripIt(x)
{
	x.value = x.value.replace(/['"]/g,'');
}
document.getElementById("txtSearch").focus();
</script>

<div id="box">
<h3><?php print 'Edit Page';//@$section_heading; ?>

</h3>
<br>
<script language="JavaScript" src="<?php print base_url();?>assets/js/gen_validatorv31.js" type="text/javascript"></script>
<style>
.table,td,tr
{
	border:none;
}
.redtext
{
 color:#FF0000;
}
</style>

<div class="yui-skin-sam">
<form name="pageeditform" method="post" action="">
<div class="redtext"><?php echo validation_errors(); ?></div>

<table width="100%" border="0" cellspacing="12">
  <tr>
    <td><b>Page Title:</b>&nbsp;<em class="redtext">*</em></td>
    <td>
        <input class="textbox" type="text" name="pagetitle" id="pagetitle" style="width:265px;" value="<?php print @$pages['page_title']; ?>">
    </td>
  </tr>
  <tr>
    <td valign="top"><b>Heading1:</b>&nbsp;<em class="redtext">*</em></td>
    <td>      
       <textarea class="textbox" style="border:solid 1px #CCCCCC;width:269px;" name="heading1" id="heading1" rows="3"><?php print @$pages['heading1'];?></textarea>
      </td>
  </tr>
  <tr>
    <td valign="top"><b>Description1:</b>&nbsp;<em class="redtext">*</em></td>
    <td>
        <textarea style="border:solid 1px #CCCCCC;width:269px;" name="desc1" id="desc1" rows="3"><?php print @$pages['description1'];?></textarea>
    </td>
  </tr>
  <tr>
    <td valign="top"><b>Heading2:</b>&nbsp;<em class="redtext">*</em></td>
    <td>      
       <textarea class="textbox" style="border:solid 1px #CCCCCC;width:269px;" name="heading2" id="heading2" rows="3"><?php print @$pages['heading2'];?></textarea>
      </td>
  </tr>
  <tr>
    <td valign="top"><b>Description2:</b>&nbsp;<em class="redtext">*</em></td>
    <td>
        <textarea style="border:solid 1px #CCCCCC;width:269px;" name="desc2" id="desc2" rows="3"><?php print @$pages['description2'];?></textarea>
    </td>
  </tr>
  <tr>
    <td valign="top"><b>Heading3:</b>&nbsp;<em class="redtext">*</em></td>
    <td>      
       <textarea class="textbox" style="border:solid 1px #CCCCCC;width:269px;" name="heading3" id="heading3" rows="3"><?php print @$pages['heading3'];?></textarea>
      </td>
  </tr>
  <tr>
    <td valign="top"><b>Description3:</b>&nbsp;<em class="redtext">*</em></td>
    <td>
        <textarea style="border:solid 1px #CCCCCC;width:269px;" name="desc3" id="desc3" rows="3"><?php print @$pages['description3'];?></textarea>
    </td>
  </tr>
  <?php if($page_name==1){?>
  <tr>
    <td valign="top"><b>Heading4:</b>&nbsp;<em class="redtext">*</em></td>
    <td>      
       <textarea class="textbox" style="border:solid 1px #CCCCCC;width:269px;" name="heading4" id="heading4" rows="3"><?php print @$pages['heading4'];?></textarea>
      </td>
  </tr>
  <tr>
    <td valign="top"><b>Description4</b>&nbsp;<em class="redtext">*</em></td>
    <td>
        <textarea style="border:solid 1px #CCCCCC;width:269px;" name="desc4" id="desc4" rows="3"><?php print @$pages['description4'];?></textarea>
    </td>
  </tr>
  <?php } ?>
  <tr>
  <td></td>
  <td >
    <input type="submit" value="Edit" class="button" style="width:55px;"/>&nbsp;&nbsp;
    <input  type="hidden" name="editpages" id="editpages" value="editpages" />
    &nbsp;&nbsp;
    <input class="button"  type="button" name="btncancel" style="width:65px;" id="btncancel" value="Cancel"
     onClick="javascript:document.location.href='<?php echo base_url();?>admin_panel/content/manage_place_tip';"/>
    
  </td>
  </tr>
 </table>
</form>
</div>

</div>
<script type="text/javascript">
	
var frmvalidator = new Validator("pageeditform");
frmvalidator.addValidation("pagetitle","req","Field page title is required.");
//document.getElementById("old_password").focus();


</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
<title><?php print $page_title; ?></title>
<link rel="stylesheet" type="text/css" href="<?php print base_url();?>assets/css/admin/css/style.css" />
<link rel="stylesheet" type="text/css" href="<?php print base_url();?>assets/css/admin/css/theme1.css" />
<!--
<script type="text/javascript" src="<?php print base_url();?>assets/js/yui/build/yahoo-dom-event/yahoo-dom-event.js"></script>
<script type="text/javascript" src="<?php print base_url();?>assets/js/yui/build/container/container-min.js"></script>
<script type="text/javascript" src="<?php print base_url();?>assets/js/yui/build/animation/animation-min.js"></script>
<script type="text/javascript" src="<?php print base_url();?>assets/js/yui/build/calendar/calendar-min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php print base_url();?>assets/js/yui/build/calendar/assets/skins/sam/calendar.css" />
-->
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="<?php print base_url();?>assets/css/admin/css/ie-sucks.css" />
<![endif]-->

<?php if(isset($extra_head_content)) print $extra_head_content;?>
<style>

</style>
</head>

<body <?php if(isset($bodyclass)) print $bodyclass;?>>
	<div id="container">
    	<div id="header">
        	<h2 style="float:left; width:400px;">
        	<div style="float:left;width:100%;margin-top:-18px;">WHISPERTIPS</div>
        	<span style="font-size:13px;margin-left:34px;"><?php print 'Admin Panel';?></span>
        	</h2>
            <div class="welcome">
            
				<?php
				$url=base_url().'blog/automatic_login';
				if( $this->authorize->is_admin_logged_in() == true)
				print $this->lang->line('admin_welcome')." ". $this->session->userdata('admin_username') . ' &nbsp;|&nbsp;<a style="color:#FFFFFF; text-decoration:none;" href="'.base_url().'admin_panel/login/logout">'.$this->lang->line('admin_logout').'</a><br>';
				print '<a style="color:#FFFFFF; text-decoration:none;" href="'.$url.'"><span>'.$this->lang->line('admin_automatic_blog_login').'</span></a>';
				?>
			</div>
			<!-- Top Links -->
			<?php if(isset($top_links)) print $top_links; ?>
			<!-- Top Links -->

		<!-- Main Content Starts Here -->
		<div id="content">
			<?php
			if(isset($error))
			{
				print '
				<div id="message-red" style="display:block">
				    <table cellspacing="0" cellpadding="0" style="width:600px;border:0px;">
					<tr>
					    <td class="red-left" style="border:0px;">'.$error.'</td>
					    <td class="red-right" style="padding:0px;border:0px;">
						<a class="close-red" onclick="CloseMessage()"><img alt="" src="'.base_url().'assets/css/admin/img/close_red.png" align="left" style="margin:0px;"></a>
					    </td>
					</tr>
				    </table>
				</div>';
				
			}
			if(isset($success))
			{
				print '
				<div id="message-green" style="display:block" >
				    <table cellspacing="0" cellpadding="0" style="width:600px;border:0px;">
					<tr>
					    <td class="green-left" style="border:0px;">'.$success.'</td>
					    <td class="green-right" style="padding:0px;border:0px;">
						<a class="close-green" onclick="CloseMessage()"><img alt="" src="'.base_url().'assets/css/admin/img/close_green.png" align="left" style="margin:0px;"></a>
					    </td>
					</tr>
				    </table>
				</div>';  
				
			}
			if(isset($message))
			{
				print '
				<div id="message-yellow" style="display:block">
				    <table cellspacing="0" cellpadding="0" style="width:600px;border:0px;">
					<tr>
					    <td class="yellow-left" style="border:0px;">'.$success.'</td>
					    <td class="yellow-right" style="padding:0px;border:0px;">
						<a class="close-yellow" onclick="CloseMessage()"><img alt="" src="'.base_url().'assets/css/admin/img/close_yellow.png" align="left" style="margin:0px;"></a>
					    </td>
					</tr>
				    </table>
				</div>';  
				
			}
			?>
			<?php if(isset($page_body)) print $page_body; ?>
		</div>
		 <!-- Main Content Ends Here -->
           
    </div>

    <!-- Footer Starts Here -->
    <div id="footer">
        &copy; whispertip.com
    </div>
    <!-- Footer Ends Here -->
   
</div>

<script>

function CloseMessage()
{
	var divs = document.getElementsByTagName("div");
	for(var i=0;i<divs.length;i++)
	{
		if( divs[i].id.substr(0,8)=="message-")
		{
			divs[i].style.display="none";
		}
	}
}

<?php if( isset($error) || isset($message) || isset($success)) {
	print 'var t=setTimeout("CloseMessage()",10000);';
}
?>
</script>
</body>
</html>

<div class="<?php echo $className;?> <?php echo $colorCode;?>">
	<div class="container">
		<div class="main_outer">
			<center><img src="<?php echo base_url() ?>assets/images/<?php echo $logo;?>.png" /></center>		
			<div class="content">
				<div class="sort"><span>sort:</span>    <a href="#">OVERALL RANKING</a>   <a href="#">STRIKE RATE</a>   <a href="#">BEST OVER LAST 5</a></div>
			<div class="stickyribbon">
			<ul class="left_links">
				<li><span>sport</span></li>
				<?php if($categories) { 
					foreach($categories as $cat) {
						if($eventType == $cat['slugUrl'])
						 {
							 $style = 'color:#65D2D0';
						 }
						 else
						 {
							 $style= 'color:#777777';
						}
					?>
				<li><a href="<?php echo base_url() ?>tipsters/<?php echo $cat['slugUrl']; ?>" style="<?php echo $style;?>"><?php echo $cat['name'];?></a></li>
			<?php } } else { ?>
				<li><span>No sport Found</span></li>
			<?php 	} ?>
			</ul>
			</div>
			<div class="right_content">
				<?php foreach($result as $res) { 
					$userStats = $this->betfair_model->getUserStatisticsForProfile($res['user_id'],$event_type_id);
					if(count($userStats))
					{
						$stRate = $userStats['strike_rate'];
						$ave_odds = round($userStats['ave_odds'],3);
						$rank = $userStats['user_rank'];
					}
					else
					{
						$stRate=0;
						$ave_odds=0;
						$rank = 0;
					}
					
					?>
				<div class="blog">
					<div class="number"><span>NO</span><sub><?php echo $res['rank'];?></sub></div>
					<div class="blog_img"><img src="<?php echo $res['image']; ?>" style="height:80px;width: 74px;">
					<a href="#" class="buy_tip">buy tips</a></div>
					<div class="blog_dis">
							<h1><?php echo $res['firstname'].' '.$res['lastname'];?><div class="pull-right"><img src="<?php echo base_url() ?>assets/images/blog_icon1.png"/><img src="<?php echo base_url() ?>assets/images/blog_icon2.png"/></div></h1>
							<div class="blog_row"><div class="pull-left">Average Ranking</div><div class="pull-right"><?php echo $rank;?></div></div>
							<div class="blog_row"><div class="pull-left">Average odds</div><div class="pull-right"><?php echo $ave_odds;?></div></div>
							<div class="blog_row"><div class="pull-left">Strike rate</div><div class="pull-right"><?php echo $stRate;?></div></div>
							<div class="blog_row"><div class="pull-left">tip price</div><div class="pull-right">$6.00</div></div>
					</div>
				</div>
				<?php } ?>
				
				<!-- <div class="blog">
					<div class="number"><span>NO</span><sub>1</sub></div>
					<div class="blog_img"><img src="<?php echo base_url() ?>assets/images/user1.png"/><a href="#" class="buy_tip">buy tips</a></div>
					<div class="blog_dis">
							<h1>Monica laskowski <div class="pull-right"><img src="<?php echo base_url() ?>assets/images/blog_icon1.png"/><img src="<?php echo base_url() ?>assets/images/blog_icon2.png"/></div></h1>
							<div class="blog_row"><div class="pull-left">Average Ranking</div><div class="pull-right">3RD</div></div>
							<div class="blog_row"><div class="pull-left">Average odds</div><div class="pull-right">2.88</div></div>
							<div class="blog_row"><div class="pull-left">Strike rate</div><div class="pull-right">36.36</div></div>
							<div class="blog_row"><div class="pull-left">tip price</div><div class="pull-right">$6.00</div></div>
					</div>
				</div>
				
				<div class="blog">
					<div class="number"><span>NO</span><sub>1</sub></div>
					<div class="blog_img"><img src="<?php echo base_url() ?>assets/images/user1.png"/><a href="#" class="buy_tip">buy tips</a></div>
					<div class="blog_dis">
							<h1>Monica laskowski <div class="pull-right"><img src="<?php echo base_url() ?>assets/images/blog_icon1.png"/><img src="<?php echo base_url() ?>assets/images/blog_icon2.png"/></div></h1>
							<div class="blog_row"><div class="pull-left">Average Ranking</div><div class="pull-right">3RD</div></div>
							<div class="blog_row"><div class="pull-left">Average odds</div><div class="pull-right">2.88</div></div>
							<div class="blog_row"><div class="pull-left">Strike rate</div><div class="pull-right">36.36</div></div>
							<div class="blog_row"><div class="pull-left">tip price</div><div class="pull-right">$6.00</div></div>
					</div>
				</div>
				
				<div class="blog">
					<div class="number"><span>NO</span><sub>1</sub></div>
					<div class="blog_img"><img src="<?php echo base_url() ?>assets/images/user1.png"/><a href="#" class="buy_tip">buy tips</a></div>
					<div class="blog_dis">
							<h1>Monica laskowski <div class="pull-right"><img src="<?php echo base_url() ?>assets/images/blog_icon1.png"/><img src="<?php echo base_url() ?>assets/images/blog_icon2.png"/></div></h1>
							<div class="blog_row"><div class="pull-left">Average Ranking</div><div class="pull-right">3RD</div></div>
							<div class="blog_row"><div class="pull-left">Average odds</div><div class="pull-right">2.88</div></div>
							<div class="blog_row"><div class="pull-left">Strike rate</div><div class="pull-right">36.36</div></div>
							<div class="blog_row"><div class="pull-left">tip price</div><div class="pull-right">$6.00</div></div>
					</div>
				</div>
				
				<div class="blog">
					<div class="number"><span>NO</span><sub>1</sub></div>
					<div class="blog_img"><img src="<?php echo base_url() ?>assets/images/user1.png"/><a href="#" class="buy_tip">buy tips</a></div>
					<div class="blog_dis">
							<h1>Monica laskowski <div class="pull-right"><img src="<?php echo base_url() ?>assets/images/blog_icon1.png"/><img src="<?php echo base_url() ?>assets/images/blog_icon2.png"/></div></h1>
							<div class="blog_row"><div class="pull-left">Average Ranking</div><div class="pull-right">3RD</div></div>
							<div class="blog_row"><div class="pull-left">Average odds</div><div class="pull-right">2.88</div></div>
							<div class="blog_row"><div class="pull-left">Strike rate</div><div class="pull-right">36.36</div></div>
							<div class="blog_row"><div class="pull-left">tip price</div><div class="pull-right">$6.00</div></div>
					</div>
				</div>
				
				<div class="blog">
					<div class="number"><span>NO</span><sub>1</sub></div>
					<div class="blog_img"><img src="<?php echo base_url() ?>assets/images/user1.png"/><a href="#" class="buy_tip">buy tips</a></div>
					<div class="blog_dis">
							<h1>Monica laskowski <div class="pull-right"><img src="<?php echo base_url() ?>assets/images/blog_icon1.png"/><img src="<?php echo base_url() ?>assets/images/blog_icon2.png"/></div></h1>
							<div class="blog_row"><div class="pull-left">Average Ranking</div><div class="pull-right">3RD</div></div>
							<div class="blog_row"><div class="pull-left">Average odds</div><div class="pull-right">2.88</div></div>
							<div class="blog_row"><div class="pull-left">Strike rate</div><div class="pull-right">36.36</div></div>
							<div class="blog_row"><div class="pull-left">tip price</div><div class="pull-right">$6.00</div></div>
					</div>
				</div>


				<div class="blog">
					<div class="number"><span>NO</span><sub>1</sub></div>
					<div class="blog_img"><img src="<?php echo base_url() ?>assets/images/user1.png"/><a href="#" class="buy_tip">buy tips</a></div>
					<div class="blog_dis">
							<h1>Monica laskowski <div class="pull-right"><img src="<?php echo base_url() ?>assets/images/blog_icon1.png"/><img src="<?php echo base_url() ?>assets/images/blog_icon2.png"/></div></h1>
							<div class="blog_row"><div class="pull-left">Average Ranking</div><div class="pull-right">3RD</div></div>
							<div class="blog_row"><div class="pull-left">Average odds</div><div class="pull-right">2.88</div></div>
							<div class="blog_row"><div class="pull-left">Strike rate</div><div class="pull-right">36.36</div></div>
							<div class="blog_row"><div class="pull-left">tip price</div><div class="pull-right">$6.00</div></div>
					</div>
				</div>
				
				<div class="blog">
					<div class="number"><span>NO</span><sub>1</sub></div>
					<div class="blog_img"><img src="<?php echo base_url() ?>assets/images/user1.png"/><a href="#" class="buy_tip">buy tips</a></div>
					<div class="blog_dis">
							<h1>Monica laskowski <div class="pull-right"><img src="<?php echo base_url() ?>assets/images/blog_icon1.png"/><img src="<?php echo base_url() ?>assets/images/blog_icon2.png"/></div></h1>
							<div class="blog_row"><div class="pull-left">Average Ranking</div><div class="pull-right">3RD</div></div>
							<div class="blog_row"><div class="pull-left">Average odds</div><div class="pull-right">2.88</div></div>
							<div class="blog_row"><div class="pull-left">Strike rate</div><div class="pull-right">36.36</div></div>
							<div class="blog_row"><div class="pull-left">tip price</div><div class="pull-right">$6.00</div></div>
					</div>
				</div>
			</div>
			-->
			
			</div>
		</div>
	</div>
</div>






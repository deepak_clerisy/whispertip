<?php 
if($hostPath=='localhost')
{
	global $ssl_crt ;
	global $ssl_key ;
	$ssl_crt = "/var/www/betfair/ssl/client-2048.crt";
	$ssl_key = "/var/www/betfair/ssl/client-2048.key";
}
else
{
	global $ssl_crt ;
	global $ssl_key ;
	$ssl_crt = "/var/ssl_betfair/client-2048.crt";
	$ssl_key = "/var/ssl_betfair/client-2048.key";
}

function certlogin($appKey,$params)
{
	global $ssl_crt;
	global $ssl_key;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://identitysso-api.betfair.com/api/certlogin");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSLCERT, $ssl_crt);
    curl_setopt($ch, CURLOPT_SSLKEY, $ssl_key);

	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey
    ));

    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $response = json_decode(curl_exec($ch));
   
    curl_close($ch);

    if ($response->loginStatus == "SUCCESS") {
        return $response;
    } else {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    }
}
function debug($debugString)
{
    global $DEBUG;
    if ($DEBUG)
        echo $debugString . "\n\n";
}

function sportsApingRequest($appKey, $sessionToken, $operation, $params,$market_country)
{
    $ch = curl_init();
    if($market_country =='aus'){
		curl_setopt($ch, CURLOPT_URL, "https://api-au.betfair.com/exchange/betting/json-rpc/v1");
	}
	else{
		curl_setopt($ch, CURLOPT_URL, "https://api.betfair.com/exchange/betting/json-rpc/v1");
	}
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey,
        'X-Authentication: ' . $sessionToken,
        'Accept: application/json',
        'Content-Type: application/json'
    ));

    $postData =
        '[{ "jsonrpc": "2.0", "method": "SportsAPING/v1.0/' . $operation . '", "params" :' . $params . ', "id": 1}]';

    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

    //debug('Post Data: ' . $postData);
    $response = json_decode(curl_exec($ch));
    //debug('Response: ' . json_encode($response));
 //$info = curl_getinfo($ch);
 //echo '<pre>';print_r($info);
    curl_close($ch);

    if (isset($response[0]->error)) {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    } else {
        return $response;
    }
   


}
function getNextRacingMarket($appKey, $sessionToken, $horseRacingEventTypeId,$markType,$market_country)
{

    $params = '{"filter":{"eventIds":["' . $horseRacingEventTypeId . '"],
              "marketTypeCodes":["'.$markType.'"]},
              "sort":"FIRST_TO_START",
              "maxResults":"1",
              "marketProjection":["RUNNER_DESCRIPTION"]}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketCatalogue', $params,$market_country);
	if($jsonResponse[0]->result)
	{
		return $jsonResponse[0]->result[0];
	}
	else
	{
			return '';
	}
}
function getMarketBook($appKey, $sessionToken, $marketId,$market_country)
{
   $params = '{"marketIds":["' . $marketId . '"], "priceProjection":{"priceData":["EX_BEST_OFFERS"]}}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketBook', $params,$market_country);
    return $jsonResponse;
}
	$appKey = '4ae3bb5f6f042268359bba5d24dd3c2ecb1b2f37';
	$params = 'username=nlarkins&password=budda2468';
	$resp = certlogin($appKey,$params);
	
	$sessionToken = 'b0olRvuT4forfmXWaUKH59/W0mlmbizDcP7jOWVegLo=';
	$sessionToken = $resp->sessionToken;
//echo $sessionToken;die;
	$appKey1 = '97Wtq7sjReOr0uWk';
?>
    <div class="table">
           <?php 
              //   echo '<pre>';print_r($league);die;
           			if($league) { 
							$ii = 0;
							foreach($league as $leagues){
									$MatchOddMarket = $this->betfair_model->getMatchOddEventsMarkets($leagues['event_id']);
									$market = $this->betfair_model->getEventsMarketsCount('',$leagues['event_id']);
									$totalMarket = $market;
									$tipPlaced = $this->betfair_model->checkTipPlacedMyMarketId($MatchOddMarket['market_id'],$userId);
								?>
								<div class="table-row comp_grey">
									<div class="table-cell" style="border-right:none"><h2><?php echo $leagues['name'];?></h2></div>
									<div class="table-cell" style="border-right:none"></div>
									<div class="table-cell" style="border-right:none"></div>
									<div class="table-cell" style="border-right:none"></div>
									</div>
									<div class="table-row">
										<div class="table-cell">
											<h1><?php echo $leagues['event_name'];?></h1>
											<h5><?php echo $leagues['convertedtime']; ?></h5>
											<form name="markets_<?php echo $leagues['event_id'];?>" id="market_<?php echo $leagues['event_id'];?>" method="post" action="<?php echo base_url()?>markets">
												<?php if($totalMarket>0) {?>
													<span class="more_markets" data-uri="<?php echo $leagues['event_id'];?>" style="cursor:pointer;"><?php echo $totalMarket;?> Markets > </span>
												<?php } else{ ?>
													<span>There are currently No Markets</span>
													<?php }?>	
													<input type="hidden" value="<?php echo $leagues['event_id']?>" name="event_id">
													<input type="hidden" value="<?php echo $leagues['event_type_id']?>" name="event_type_id">
												</form>
											</span>
										</div>
									<?php 
										$parts  = explode('.', (string)$MatchOddMarket['market_id']);	
										if(strpos($leagues['event_name'],'@')!==false)
											{
												$flag = 1;
												$explodeName = explode('@',$leagues['event_name']);
											}
											else if(strpos($leagues['event_name'],' v ')!==false)
											{
												$flag = 1;
												$explodeName = explode(' v ',$leagues['event_name']);
											}
											else
											{
												$flag = 0;
												$explodeName =$leagues['name'];
											}
												if($flag==1){
													$marketDetail = $this->betfair_model->getRunners($MatchOddMarket['market_id']);
													$cnt = count($marketDetail);
													if($marketDetail){
														$flag_new = 0;
														foreach($marketDetail as $detail){	
														if($flag_new==0){
																if(substr($detail['market_id'], 0, 1 ) == 1){
																	$stat ='inter';
																}else{
																	$stat ='aus';
																	}	
																$type = getMarketBook($appKey1, $sessionToken, 
																$detail['market_id'],$stat);	
													}
													$flag_new++;
												 if(count($type)){
													 $selection_odd ='';
													 $addclass ='';
													 $addColorclass ='';
													  	foreach($type as $market){
															foreach($market->result as $market_result){
																foreach($market_result->runners as $runner){
																	$selection_id = $runner->selectionId;
																	if($tipPlaced['selection_id']==$detail['selection_id'] && $tipPlaced['market_id']==$detail['market_id'])
																	{
																		$addBackgroundclass = 'bet_placed';
																		$addColorclass = 'bet_placed_color';
																	}
																	else
																	{
																		$addBackgroundclass = '';
																		$addColorclass = '';
																	}
																	if($detail['selection_id']==$selection_id && $detail['market_id']==$market_result->marketId)
																	{
																		$selection_odd = $runner->ex->availableToBack[0]->price;
																	}
																}
														}
														}
													}
													 $explMArket = explode('.',$MatchOddMarket['market_id']);
												?> 
												<div class="table-cell value-cell place_bet_notify <?php echo $addBackgroundclass;?>" data-ten="<?php echo $leagues['event_type_id'];?>" data-nine="<?php echo $leagues['timezone'];?>" data-eight="<?php echo $leagues['open_date'];?>" data-seven="<?php echo $leagues['event_name'];?>" data-six="<?php echo $detail['runner_name'];?>" data-five="<?php echo $selection_odd;?>" data-four="<?php echo $leagues['competition_id'];?>" data-three="<?php echo $detail['selection_id'];?>" data-two="<?php echo $MatchOddMarket['market_id'];?>" data-one="<?php echo $leagues['event_id'];?>" title="click to place tip." style="<?php echo $addBackgroundclass;?>" id="<?php echo 'selection_'.$explMArket[1].$detail['selection_id'] ?>">
														<h2 class="<?php echo $addColorclass;?>"><?php echo $detail['runner_name'];?></h2>
														<span class="<?php echo $addColorclass;?>"><?php echo $selection_odd;?></span>
													</div>
											<?php 
											}
										if($cnt==2){ ?>
												<div class="table-cell">
												<h2></h2>
												<span></span>
												</div>
										<?php }	
											 }else{ ?>
												<div class="table-cell value-cell place_bet_notify">
													<h2><?php echo $explodeName[0];?></h2>
													<span></span>
												</div>
											<div class="table-cell value-cell place_bet_notify">
													<h2><?php echo $explodeName[1];?></h2>
													<span></span>
												</div>
											<div class="table-cell value-cell place_bet_notify">
													<h2>The Draw</h2>
													<span></span>
												</div>
												
											<?php 	} }else{ ?>
											<div class="table-cell value-cell">
													<h2></h2>
													<span></span>
												</div>
											<div class="table-cell value-cell">
													<h2></h2>
													<h5></h5>
												</div>
											<div class="table-cell value-cell">
													<h2></h2>
													<span></span>
												</div>
										<?php 	}
									?>	
									</div>
						<?php 
						$ii++;
						}
						}else{ ?>
							<span style="border: medium none;float: left;padding: 4px;text-align: center;width: 100%;">There are currently no Events found</span>
							<?php }
							?>
							
                        </div>
                        <div class="paging" style="float: right;">
                       <span class="pagingspn">
						<a href="javascript:void(0);" style="cursor:text;margin-right: 6px;">
							<?php print $this->pagination->create_links(); ?>
							</span>
                            </div>
<input type="hidden" value="<?php echo $event_type;?>" id="event_type_id">

<script>

$('div.paging a').each(function()
{
	var href = $(this).attr('href');
	var temp_href = href.split('/');
	var cnt = temp_href.length;
	var cnt_last = temp_href[cnt-1];
	$(this).attr('id','a_'+cnt_last);
	$(this).attr('href','javascript:void(0)');
});
$('div.paging a').click(function(){
	var id = $(this).attr('id');
	var temp_page_number = id.split('=');
	var page_number = temp_page_number[1];
	var event_type_id = $('#event_type_id').val();
	comp_id = $('#competition_filter').val();
	event_type_id = $('#event_type_id').val();
	setTimeout(function()
		{
			$('#table_container').load(base_url+'filter_competition/'+comp_id+'/'+ event_type_id+'/'+page_number);
		}, 100);	
});

</script>

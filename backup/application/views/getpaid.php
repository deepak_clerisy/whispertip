<div id="container" style="margin-top:70px;">
         	<div class="left_section">
            	<div class="user_detail">
                	<center>
						<?php 
							if($userData['profile_image']!='' && file_exists('uploads/profile_picture/thumb_'.$userData['profile_image']))
							{
								echo "<img src='".base_url().'uploads/profile_picture/thumb_'.$userData['profile_image']."' />";
							}
							else
							{
								echo "<img src='".base_url().'assets/images/pro_img.png'."'>";
							}
						?>
					</center>
                    <h1><?php echo $userData['first_name'].' '.$userData['last_name']; ?></h1>
                    <span>@<?php echo $userData['username'];?><?php ?></span>
                    <p>-<?php echo $userData['email']; ?></p>
                </div>
                <div class="left_section_footer">
                	<div class="follow">
                    	<span>172</span>
                        <p>TWEET</p>
                    </div>
                    <div class="follow">
                    	<span>295</span>
                        <p>FOLLOWING</p>
                    </div>
                    <div class="follow">
                    	<span>310</span>
                        <p>FOLLOWING</p>
                    </div>
                </div>
            </div>
            
            <div class="right_section">
            	<h1>Get Paid</h1>
            	<?php 
					if(isset($success))
					{
						echo "<div style='position:absolute;width:600px;color:green;margin-top:-55px;margin-left:200px'>".$success."</div>";
					}
					if(isset($error))
					{
						echo "<div style='position:absolute;width:600px;color:red;margin-top:-55px;margin-left:200px'>".$error."</div>";
					}
            	?>
                <form name="getpaidFrm" id="getpaidFrm" method="post" action="">
                	<div style="float:left;width:100%;margin-bottom:10px;color:#fff;">
						Please set your tip price. This amount will be credited to your paypal account when someone purchases tip from you.
					</div>
                	<div style="float:left;">
						<div style="float:left;padding-right:10px;color:#fff;padding-top:10px;">Paid to you</div>
						<input name="customerprice" id="customerprice" type="text" class="input_txt" value="<?php if(isset($paidDetails['customer_price'])) echo $paidDetails['customer_price']; ?>" placeholder="" onkeyup="customerCharge(this)" onkeypress="customerCharge(this)">
					</div>
                    <div style="float:left;">
						<div style="float:left;padding-right:10px;color:#fff;padding-top:10px;">+ Whisper tip fee</div>
						<input name="whisperamount" id="whisperamount" type="text" class="input_txt" value="<?php if(isset($paidDetails['whisper_tip'])) echo $paidDetails['whisper_tip']; ?>" placeholder="" readonly>
					</div>
					<div style="float:left;">
						<div style="float:left;padding-right:10px;color:#fff;padding-top:10px;">Charged to purchase</div>
						<input name="purchaseamount" id="purchaseamount" type="text" class="input_txt" value="<?php if(isset($paidDetails['purchase_amount'])) echo $paidDetails['purchase_amount']; ?>" placeholder="" onkeyup="customerCharge1(this)" onkeypress="customerCharge1(this)">
					</div>
					<!--
					<div style="float:left;width:500px;">
						<div class="input_label"><input type="checkbox" class="input_check" ><label class="lable_txt"><a href="#">Forgot Password</a></label></div>
					</div>
					<div style="float:left;width:500px;">
						<div class="input_label"><input type="radio" class="input_check" ><label class="lable_txt" ><a href="#">Forgot Password</a></label></div>
					</div>-->
                    <a href="#" id="saveGetPaid" class="form_btn" style="margin-right:20px;">Save</a>
                    <a href="<?php echo base_url().'categories' ?>" class="form_btn_cancel" style="clear:none;">Cancel</a>
                </form>
            </div>
         </div>
<script>
var tipPer = "<?php echo $this->config->item('whispertipfee'); ?>";
function customerCharge(obj)
{
	var amount = obj.value;
	var whispertipfee = (amount/tipPer).toFixed(2);
	if(!isNaN(whispertipfee))
	{
		document.getElementById('whisperamount').value=whispertipfee;
	}
	
	var purchaseamount = (parseFloat(amount)+parseFloat(whispertipfee)).toFixed(2);
	if(!isNaN(purchaseamount))
	{
		document.getElementById('purchaseamount').value=purchaseamount;
	}
}
function customerCharge1(obj)
{
	var amount = obj.value;
	var customerprice = ((amount*tipPer)/(parseFloat(tipPer)+1)).toFixed(2);
	var whispertipfee = (customerprice/10).toFixed(2);
	var customerprice = amount - whispertipfee;
	if(!isNaN(whispertipfee))
	{
		document.getElementById('whisperamount').value=whispertipfee;
	}
	if(!isNaN(customerprice))
	{
		document.getElementById('customerprice').value=customerprice;
	}
}
</script>

<style>
.sort_sel{color:#2888C9 !important;}
</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/golf.css">
<div class="golf black">
	<div class="container">
		<div class="main_outer">
			<center><a href="<?php echo base_url()?>select"><img src="<?php echo base_url() ?>assets/images/golf_logo.png" /></a></center>		
			<div class="content">
		<div class="sort" ><span>sort:</span> <a href="javascript:void(0)" data-uri="profit" class='sorting_content sort_sel' data-two="<?php echo $event_type_id;?>">PROFIT</a>
		<a href="javascript:void(0)" data-uri="overall" class='sorting_content ' data-two="<?php echo $event_type_id;?>">STRIKE RATE</a>
		<a href="javascript:void(0)" data-uri="strikerate" data-two="<?php echo $event_type_id;?>" class='sorting_content'>CORRECT TIPS</a> 
				<a href="javascript:void(0)" data-uri="best" data-two="<?php echo $event_type_id;?>" class='sorting_content'>BEST OVER LAST 5</a>
				<a href="javascript:void(0)" data-uri="sport" data-two="<?php echo $event_type_id;?>" class='sorting_content'>UPCOMING GAMES</a>
				<select id="time_range" class="time_sort" id="time_range">
						<option value="">Filter By Time</option>
						<option value="week">Last 7 days</option>
						<option value="month">Last Month</option>
						<option value="year">Last Year</option>
				</select>
				<select id="sort_by" class="time_sort"  style="display:none;">
						<option value="">Select Sort By:</option>
						<option value="strike_rate">Strike Rate</option>
						<option value="profit">Profit</option>
						<option value="correct">Correct Tips</option>
				</select>
				</div>
			<div class="stickyribbon">
			<div class="menu1" ><?php if($eventType) { echo $eventType; }else { echo "Menu";}?>
			<a class="menu_icon collapsed"  data-target=".nav-collapse1" data-toggle="collapse"></a>
			</div>
			<ul class="left_links nav-collapse1 in collapse" id="left_link_ul" style="height:auto;position:static;overflow:hidden;">
				<li><span>sport</span></li>
				<?php if($categories) { 
					foreach($categories as $cat) {
						
					?>
				<li><a href="<?php echo base_url() ?>tipsters/<?php echo $cat['slugUrl']; ?>"  <?php if($cat['event_type_id']==$event_type_id){ ?> style="color:<?php echo $colortheme ?>";<?php } ?>><?php echo $cat['name'];?></a></li>
			<?php } } else { ?>
				<li><span>No sport Found</span></li>
			<?php 	} ?>
			</ul>
			</div>
			<div class="right_content sortingDiv">
				<?php 
				if($result) {
				
				foreach($result as $res) { 
					$tipsPlaced = $this->betfair_model->getuserBetPlacedEventsByUser($res['user_id'],$event_type_id);
					$userStats = $this->betfair_model->getUserStatisticsForProfile($res['user_id'],$event_type_id);
					//$resBetOdds = $this->betfair_model->getUserOddsBets($res['user_id'],$event_type_id);
					$totalWonloss = $this->betfair_model->getTotalWonloss($res['user_id'],$event_type_id);
					if(count($userStats))
					{
						$stRate = $userStats['strike_rate'];
						$ave_odds = round($userStats['ave_odds'],3);
						$rank = $userStats['user_rank'];
					}
					else
					{
						$stRate=0;
						$ave_odds=0;
						$rank = 0;
					}
					if($totalWonloss['total_placed_tip'] < 20) {
								$strike = 0;	
							} 
							else
							{
								$strike = $stRate;	
							}
					$twitter_user_id='';
					if($res['tw_token'])
					{
						$res1 = $res['tw_token'];
						$sp1 = explode('&',$res1);
						$final = explode('=',$sp1[0]);
						$final1 = explode('=',$sp1[1]);
						$user_id_twitter = explode('-',$final[1]);
						$twitter_user_id = $user_id_twitter[0];
					}	
					
					if($twitter_user_id)
					{
						$redirect = '<a href="javascript:void(0)" onclick="twitter_share('.$twitter_user_id.')" class="twitter_share1" title="Follow">';
						$twitter_style="display:block";	
					}
					else
					{
						$redirect = '<a href="javascript:void(0)">';
						$twitter_style="display:none";
					}
					?>
				<div class="blog">
					<div class="number"><span>NO</span><sub><?php echo $res['rank'];?></sub></div>
					<div class="blog_img">
					<?php if(count($tipsPlaced)) { ?><a class="loadPurchaser" data-uri="<?php echo $res['user_id'];?>"><?php  }?>
					<img src="<?php echo $res['image']; ?>" style="height:99px;width: 77px;"></a>
					<a class="buy_tip loadPurchaser" data-uri="<?php echo $res['user_id'];?>">
					<?php if(count($tipsPlaced)) { echo "buy tips "; }?></a></div>
					<div class="blog_dis">
							<h1>
								<?php if(count($tipsPlaced)) { ?>
								<a class="loadPurchaser" data-uri="<?php echo $res['user_id'];?>"><?php  }?>
								<?php echo $res['firstname'].' '.$res['lastname'];?></a><div class="pull-right" style="<?php echo $twitter_style;?>">
								<?php echo $redirect;?><img src="<?php echo base_url() ?>assets/images/blog_icon1.png"/>
									<img src="<?php echo base_url() ?>assets/images/blog_icon2.png"/>
									</a>
								</div></h1>
							<div class="blog_row"><div class="pull-left">Correct Tip</div><div class="pull-right"><?php echo $totalWonloss['total_win'].'/'.$totalWonloss['total_placed_tip'];?></div></div>
							<div class="blog_row"><div class="pull-left">Average odds</div><div class="pull-right"><?php echo $ave_odds;?></div></div>
							<?php if($totalWonloss['total_placed_tip'] >20) { ?>
							<div class="blog_row"><div class="pull-left">Strike rate</div><div class="pull-right"><?php echo $strike;?></div></div>
							<?php } ?>
							<div class="blog_row">
								<div class="hintModal">
								<div class="pull-left">Profit</div>
								<div class="pull-right"><?php 
									if(isset($res['profit'])){
										echo '$'.$res['profit'];
									} else
									{
										echo '$0.00';
									}
									?></div>
							<div class="hintModal_container">
								Based off placing $100 on each tips you would have made
							</div>
							</div>
							</div>
							<?php $total = $this->betfair_model->totalTipPlaced($res['user_id'],$event_type_id);?>
							<div class="blog_row"><div class="pull-left">Available Tips</div><div class="pull-right">
							<?php if($total)
							{ 
								echo $total;
							}
							else 
							{ 
								echo "0";
							}?></div></div>
					</div>
				</div>
				<?php } }
				else
				{
					echo  "<span style='float:left;width:100%;text-align:center;color:white;font-size:17px; margin-top: 20px;'>There are currently no Tipsters</span>";
					
				}?>
				
				</div>
		</div>
	</div>
</div>
<input type="hidden" value="<?php echo $event_type_id; ?>" name="league_id" id="league_id">
<input type="hidden" value="<?php echo base_url();?>" id="url">
<input type="hidden" value="<?php echo $res['user_id'];?>" id="user_id">
<input type="hidden" value="<?php echo $eventType; ?>" name="eventType" id="eventType">
<input type="hidden" value="<?php echo $login_status; ?>" name="login_status" id="login_status">

<input type="hidden" value="" id="timerange">




<?php 
 $type = @$Type['market_type'];

if($_SERVER['HTTP_HOST']=='localhost')
{
	global $ssl_crt ;
	global $ssl_key ;
	$ssl_crt = "/var/www/betfair/ssl/client-2048.crt";
	$ssl_key = "/var/www/betfair/ssl/client-2048.key";
}
else
{
	global $ssl_crt ;
	global $ssl_key ;
	$ssl_crt = "/var/ssl_betfair/client-2048.crt";
	$ssl_key = "/var/ssl_betfair/client-2048.key";
}

function certlogin($appKey,$params)
{
	global $ssl_crt;
	global $ssl_key;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://identitysso-api.betfair.com/api/certlogin");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSLCERT, $ssl_crt);
    curl_setopt($ch, CURLOPT_SSLKEY, $ssl_key);

	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey
    ));

    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $response = json_decode(curl_exec($ch));
   
    curl_close($ch);

    if ($response->loginStatus == "SUCCESS") {
        return $response;
    } else {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    }
}
function debug($debugString)
{
    global $DEBUG;
    if ($DEBUG)
        echo $debugString . "\n\n";
}

function sportsApingRequest($appKey, $sessionToken, $operation, $params,$market_country)
{
    $ch = curl_init();
    if($market_country =='aus'){
		curl_setopt($ch, CURLOPT_URL, "https://api-au.betfair.com/exchange/betting/json-rpc/v1");
	}
	else{
		curl_setopt($ch, CURLOPT_URL, "https://api.betfair.com/exchange/betting/json-rpc/v1");
	}
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey,
        'X-Authentication: ' . $sessionToken,
        'Accept: application/json',
        'Content-Type: application/json'
    ));

    $postData =
        '[{ "jsonrpc": "2.0", "method": "SportsAPING/v1.0/' . $operation . '", "params" :' . $params . ', "id": 1}]';

    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

    //debug('Post Data: ' . $postData);
    $response = json_decode(curl_exec($ch));
    //debug('Response: ' . json_encode($response));
 //$info = curl_getinfo($ch);
 //echo '<pre>';print_r($info);
    curl_close($ch);

    if (isset($response[0]->error)) {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    } else {
        return $response;
    }
   


}
function getNextRacingMarket($appKey, $sessionToken, $horseRacingEventTypeId,$markType,$market_country)
{

    $params = '{"filter":{"eventIds":["' . $horseRacingEventTypeId . '"],
              "marketTypeCodes":["'.$markType.'"]},
              "sort":"FIRST_TO_START",
              "maxResults":"1",
              "marketProjection":["RUNNER_DESCRIPTION"]}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketCatalogue', $params,$market_country);

    return $jsonResponse[0]->result[0];
}
function getMarketBook($appKey, $sessionToken, $marketId,$market_country)
{
    $params = '{"marketIds":["' . $marketId . '"], "priceProjection":{"priceData":["EX_BEST_OFFERS"]}}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketBook', $params,$market_country);

    return $jsonResponse[0]->result[0];
}
	$appKey = '4ae3bb5f6f042268359bba5d24dd3c2ecb1b2f37';
	$params = 'username=nlarkins&password=budda2468';
	//$resp = certlogin($appKey,$params);
	
	$sessionToken = 'b0olRvuT4forfmXWaUKH59/W0mlmbizDcP7jOWVegLo=';
	//$sessionToken = $resp->sessionToken;
//echo $sessionToken;die;
	$appKey1 = '97Wtq7sjReOr0uWk';
?>
<div style=' float: right;left: 15px;position: absolute;top: -17px;width: 100%;z-index: 1000;'>
	<img src='<?php echo base_url(); ?>assets/images/cross.png' id='' class='crossing' style='float:right;cursor:pointer;height: 26px;
    width: 26px;' onclick='closepopup()'>
</div>
<div  class="detail_popup season" style="width:99%;height: 460px;">
                	
                    <div id="scrollLeague" class="detail_popup" style="width:99%;">
                        
                        
                        <div class="purchase_row" style="background:none;color:#fff;">
							<?php 
								$eventDate = $this->betfair_model->convert_time_zone($event['open_date'],$event['timezone'],$this->session->userdata('timezone'));
								echo date("l",strtotime($eventDate))." ".date("d/m/Y",strtotime($eventDate));  
							?>
						</div>
                        <div class="purchase_row" data-one="<?php echo $event_id; ?>" data-two="<?php echo $market_id; ?>" data-three="<?php echo $competition_id; ?>">
                        	<div class="time" style="padding:18px 0px;"><?php echo date("h:i a",strtotime($eventDate)); ?><br></div>
                           <!-- <div class="win">Odds</div>-->
                           
                           <Div class="leaguemodds" style="float:left;width:100%;">
							   <?php 
								$parts  = explode('.', (string)$market_id);	
								if($parts[0]==2)
								{
									$market_country = 'aus';
								}
								else
								{
									$market_country = 'inter';
								}
							   $marketType1 = getNextRacingMarket($appKey1, $sessionToken, $event_id,$type,$market_country);
								if($marketType1) 
								{
									//$market_id = $marketType1->marketId;
									$marketType2 = getMarketBook($appKey1, $sessionToken, $market_id,$market_country);
									foreach($marketType1->runners as $resp)
									{
										$team_id = $resp->selectionId;
										$team_name = $resp->runnerName;
										
										$team_odds = 0;
										if(count($marketType2->runners))
										{
											foreach($marketType2->runners as $respm)
											{			
												$team_id_odd = 	$respm->selectionId;
												if($team_id==$team_id_odd)
												{
													$team_odds = @$respm->ex->availableToBack[0]->price;
												}
											}
										}
									   ?>
									   <div class="country1" style="width: 470px;cursor:pointer;margin:3px 0px;"  >
										<span style="height:25px;float:left;width:auto;"><?php echo $team_name; ?></span>
										
										<div style="float:left;width:162px;">
										<div class="btn_blue place_odds" style="margin-top:1px;margin-bottom:1px;min-height:12px;margin-right:10px;width:20px;">	
											<?php echo $team_odds ; ?>
										</div>
										<div class="btn_green place_bet" style="margin-top:1px;margin-bottom:1px;margin-right:10px;width:20px;" data-one="<?php echo $event_id; ?>" data-two="<?php echo $market_id; ?>" data-three="<?php echo $team_id; ?>" data-four="<?php echo $competition_id; ?>"data-five="<?php echo $team_odds ;?>" data-six="<?php echo $team_name ;?>">	
											BET
										</div>
										</div>
						
										</div>
									   <?php
									}
								}
								
							   ?>
                        	
                        
							</div>
                    	</div>
                    	
                    	</div>
                    	<div id="messagePlace" class="message" style="display:none;width:617px">
							You have successfully placed the tip.
						</div>
                </div>

<?php 
if($hostPath=='localhost')
{
	global $ssl_crt ;
	global $ssl_key ;
	$ssl_crt = "/var/www/betfair/ssl/client-2048.crt";
	$ssl_key = "/var/www/betfair/ssl/client-2048.key";
}
else
{
	global $ssl_crt ;
	global $ssl_key ;
	$ssl_crt = "/var/ssl_betfair/client-2048.crt";
	$ssl_key = "/var/ssl_betfair/client-2048.key";
}

function certlogin($appKey,$params)
{
	global $ssl_crt;
	global $ssl_key;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://identitysso-api.betfair.com/api/certlogin");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSLCERT, $ssl_crt);
    curl_setopt($ch, CURLOPT_SSLKEY, $ssl_key);

	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey
    ));

    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $response = json_decode(curl_exec($ch));
   
    curl_close($ch);

    if ($response->loginStatus == "SUCCESS") {
        return $response;
    } else {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    }
}
function debug($debugString)
{
    global $DEBUG;
    if ($DEBUG)
        echo $debugString . "\n\n";
}

function sportsApingRequest($appKey, $sessionToken, $operation, $params,$market_country)
{
    $ch = curl_init();
    if($market_country =='aus'){
		curl_setopt($ch, CURLOPT_URL, "https://api-au.betfair.com/exchange/betting/json-rpc/v1");
	}
	else{
		curl_setopt($ch, CURLOPT_URL, "https://api.betfair.com/exchange/betting/json-rpc/v1");
	}
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey,
        'X-Authentication: ' . $sessionToken,
        'Accept: application/json',
        'Content-Type: application/json'
    ));

    $postData =
        '[{ "jsonrpc": "2.0", "method": "SportsAPING/v1.0/' . $operation . '", "params" :' . $params . ', "id": 1}]';

    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

    //debug('Post Data: ' . $postData);
    $response = json_decode(curl_exec($ch));
    //debug('Response: ' . json_encode($response));
 //$info = curl_getinfo($ch);
 //echo '<pre>';print_r($info);
    curl_close($ch);

    if (isset($response[0]->error)) {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    } else {
        return $response;
    }
   


}
function getNextRacingMarket($appKey, $sessionToken, $horseRacingEventTypeId,$markType,$market_country)
{

    $params = '{"filter":{"eventIds":["' . $horseRacingEventTypeId . '"],
              "marketTypeCodes":["'.$markType.'"]},
              "sort":"FIRST_TO_START",
              "maxResults":"1",
              "marketProjection":["RUNNER_DESCRIPTION"]}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketCatalogue', $params,$market_country);
	if($jsonResponse[0]->result)
	{
		return $jsonResponse[0]->result[0];
	}
	else
	{
			return '';
	}
}
function getMarketBook($appKey, $sessionToken, $marketId,$market_country)
{
	$params = '{"marketIds":[' . $marketId . '], "priceProjection":{"priceData":["EX_BEST_OFFERS"]}}';
	$jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketBook', $params,$market_country);

    return $jsonResponse;
}
	$appKey = '4ae3bb5f6f042268359bba5d24dd3c2ecb1b2f37';
	$params = 'username=nlarkins&password=budda2468';
	$resp = certlogin($appKey,$params);
	
	$sessionToken = 'b0olRvuT4forfmXWaUKH59/W0mlmbizDcP7jOWVegLo=';
	$sessionToken = $resp->sessionToken;
//echo $sessionToken;die;
	$appKey1 = '97Wtq7sjReOr0uWk';
?>
<script type="text/javascript">
    $(document).ready(function() {
        $(".custom-select").each(function() {
            $(this).wrap("<span class='select-wrapper'></span>");
            $(this).after("<span class='holder'></span>");
        });
        $(".custom-select").change(function() {
            var selectedOption = $(this).find(":selected").text();
            $(this).next(".holder").text(selectedOption);
        }).trigger('change');
    })
</script>
                       <?php 
                       if($runnerDetail) { 
						   if($MarketidInter){
								$marketTypeInter = getMarketBook($appKey1, $sessionToken, $MarketidInter,'inter');
							}
							if($MarketidAus){
								$marketTypeAust = getMarketBook($appKey1, $sessionToken, $MarketidAus,'aus');
								}
						   $i = 1;
						foreach($runnerDetail as $key=>$catalogue){
							$market_info = $this->betfair_model->getMarketInfo($catalogue[0]['market_id']);  
							$explMArket = explode('.',$catalogue[0]['market_id']); 
						   ?>  
                          <div class="panel panel-default">
                            <div class="panel-heading active">
                              <h3 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne<?php echo $explMArket[1];?>">
                                 <?php echo $market_info['market_name'];?>
                                </a>
                              </h3>
                            </div>

                            <div id="collapseOne<?php echo $explMArket[1];?>" class="panel-collapse collapse">
                              <div class="panel-body inner-detail">
                                  <div class="media accordion-inner">
                                        <div class="media-body">
                                        	<ul>
                                        	 <?php foreach($catalogue as $team){ 
												 $marketDesc = $this->betfair_model->getMarketInfo($team['market_id']); 
												  $event_desc = $this->betfair_model->getEventDetai($marketDesc['event_id']);
												   $tipPlaced = $this->betfair_model->checkTipPlacedMyMarketId($team['market_id'],$userId);
												 if(substr($team['market_id'], 0, 1 ) == 1){
													$type =  $marketTypeInter;
												 }else{
													 $type =  $marketTypeAust;
													 }
													 $selection_odd ='';
												 if(count($type[0]->result)){
														foreach($type as $market){
															foreach($market->result as $market_result){
																foreach($market_result->runners as $runner){
																	$selection_id = $runner->selectionId;
																	if($tipPlaced['selection_id']==$team['selection_id'] && $tipPlaced['market_id']==$team['market_id'])
																	{
																		$addBackgroundclass = 'bet_placed';
																		$addColorclass = 'bet_placed_color';
																	}
																	else
																	{
																		$addBackgroundclass = '';
																		$addColorclass = '';
																	}
																	if($team['selection_id']==$selection_id && $team['market_id']==$market_result->marketId)
																	{
																		$selection_odd = $runner->ex->availableToBack[0]->price;
																	}
																}
															}
														}
													}
												 ?>
                                        		<li><div class="left-area"><?php echo $team['runner_name'];?></div>
													
													<div class="right-area place_bet_notify <?php echo $addBackgroundclass;?>" data-ten="<?php echo $event_type_id;?>" data-nine="<?php echo $event_desc['timezone'];?>" data-eight="<?php echo $event_desc['open_date'];?>" data-seven="<?php echo $event_desc['name'];?>" data-six="<?php echo $team['runner_name'];?>" data-five="<?php echo $selection_odd;?>" data-four="<?php echo $event_desc['competition_id'];?>" data-three="<?php echo $team['selection_id'];?>" data-two="<?php echo $team['market_id'];?>" data-one="<?php echo $event_desc['event_id'];?>"  id="<?php echo 'selection_'.$explMArket[1].$detail['selection_id'] ?>">
													
													<?php echo $selection_odd;?></div>
                                        		</li>
											<?php } ?>
                                        	</ul>    
                                        </div>
                                  </div>
                              </div>
                            </div>
                          </div>
                    
                   <?php 
                   $i++;
                     $flag++;
                   } } else{
					   echo '<span  style="  float: left;font-size: 16px;margin-top: 10px;text-align: center;width: 100%;">No More data to show</span>';
					   }?>  				
		
<input type="hidden" value="<?php echo $login_status; ?>" name="login_status" id="login_status">

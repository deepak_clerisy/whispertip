
	<div class="login_outer" style="margin:0px auto;">
		
			<div class="login via-social" style="<?php if(isset($mode)){echo 'display:none;';}?>">
				<a href="<?php echo base_url()?>facebook" class="login_btn f_btn" name="facebook">Log in with facebook</a>
				<a href="<?php echo base_url()?>twitter" class="login_btn t_btn" name="twitter">Log in with twitter</a>
				<a href="#" class="login_btn g_btn" name="email">Log in with Email</a>
				<div class="social_support" style="font-size:11px;margin-top:10px;  float:left;width:100%;">
				<p style="display: inline-block;font-size: 14px;margin: 0;">Don't have an account? <a class="sign_up" style="font-size: 14px;color:#7a7a7a;font-family: 'tradegothicbold_condensed'; display: inline-block;">Sign up</a></p>
				<span style="text-align: center; width: 100%; margin:6px 0 6px 0;"><a href="javascript:void(0)" class="pull-right forgot" style="float: none;font-size: 14px; margin: 0 auto;">Forgot your password?</a></span>
				<a href="<?php echo base_url()?>home/how_it_work/place" style=" color: #B41414; font-size: 12px;margin:0 auto;margin-top: 59px;background: none repeat scroll 0 0 #B41414;    border-radius: 4px 4px 4px 4px;color: #fff;display: inline-block;font: bold 0.875rem;padding: 0.75rem 2.5rem;text-align: center;" target="_blank">HOW IT WORKS</a>
				</div>
			</div>
			<!--------------------------------------------------------------------------------------------->
			<!----------------------------------------- Login --------------------------------------->
			<!--------------------------------------------------------------------------------------------->
				<div class="login via-email" style="display:none;">
			 <div id="userLoginError" class="entry_row" style="margin-top:2px;margin-bottom:0px;color:red;">
           </div>
            <form name="loginfm" id="loginfm" method="post">
			 	<input type="text" name="lguserName" id="lguserName" placeholder="User Name or Email" onkeypress="checkButton(event)"/>
				<input type="password" name="lguserPassword" id="lguserPassword" placeholder="Password" onkeypress="checkButton(event)"/>
				<a href="#" class="login_btn"  id="lgloginBtn" style="float:left;width:76px;">Login</a>
				<a href="#" class="login_btn" id="cancelforgot" style="float:left;width:76px;margin-left:60px;" name="cancelButtn">Cancel</a>
				<input type="checkbox" style="display:inline-block; float:left;margin-top:6px;"/><span>Remember me</span><a href="#" class="pull-right forgot">Forgot your password?</a>
				<input type="hidden" value="<?php echo base_url();?>" id="url" name="url">
				</form>
			</div>
			<!--------------------------------------------------------------------------------------------->
			<!----------------------------------------- Forgot --------------------------------------->
			<!--------------------------------------------------------------------------------------------->
			<div class="login via-forgot" style="display:none;">
			<input type="text" name="lguserName" id="lguserName" placeholder="Email Address" style="visibility:hidden;" />
			<div id="userForgotError" class="entry_row" style="margin-top:2px;margin-bottom:0px;color:red;">
				
            </div>
			<input type="text" name="lguserName" id="forgotEmail" placeholder="Email Address" />
				<a href="#" class="login_btn" name="sendbttn" id="sendforgotbtn" style="float:left;width:76px;">Send</a>
				<a href="#" class="login_btn" id="cancelforgot" name="cancel_forgot" style="float:left;width:76px;margin-left:60px;">Cancel</a>
			</div>
			
			<!--------------------------------------------------------------------------------------------->
			<!----------------------------------------- SIGN UP --------------------------------------->
			<!--------------------------------------------------------------------------------------------->
			
			<div class="login via-signup" style="display:none;">
			<div id="userSignUpError" class="entry_row" style="margin-top:2px;margin-bottom:0px;color:green;display:none;">
            </div>
			<input type="text" name="userName" id="userName" placeholder="Username" />
			<input type="text" name="userEmail" id="userEmail" placeholder="Your Email" />
			<input type="text" name="userFirstName" id="userFirstName" placeholder="First Name" />
			<input type="text" name="userLastName" id="userLastName" placeholder="Last Name" />
			<input type="password" name="userPassword" id="userPassword" placeholder="Password" />
			<input type="password" name="usercPass" id="usercPass" placeholder="Confirm Password" />
			<input type="text" name="userCountry" id="userCountry" placeholder="Country" />
				<span style="width:100%;">By clicking below to sign up, you are agreeing to the Whispertip <a href="#" style="color: #347DB2;font-weight: bold;" id="termsServices">TERMS OF SERVICES AND PRIVACY POLICY</a></span>
				<a href="#" class="login_btn" name="sendbttn" id="signupBtn" style="float:left;width:76px;">Sign up</a>
				<a href="#" class="login_btn" name="cancel_signup" id="cancelforgot" style="float:left;width:76px;margin-left:60px;">Cancel</a>
			</div>
			
			<!--------------------------------------------------------------------------------------------->
			<!----------------------------------------- Twitter Email Verification --------------------------->
			<!--------------------------------------------------------------------------------------------->
		<div class="login" id="emailGetMask" style="display:none;">
		 <div id="" class="popup_row getEmail_message" style="color:#557D00"> </div>
          	 	<input name="email_id" id="getemail" type="text" placeholder="Email id"/>
			 	
				<input name="email_id" id="getemail_verify" type="text" placeholder="Repeat-Email id"/>
				<a href="#" class="login_btn_twitter twitternew" id="getEmailLgnBtn" style="float:left;width:76px;">Submit</a>
				<a href="#" class="login_btn_twitter" id="canceltwitter" style="float:left;width:76px;margin-left:60px;" name="cancelButtn">Cancel</a>
				<input type="hidden" value="<?php echo base_url();?>" id="url" name="url">
			</div>
			
			
		</div>
	</div>
<?php if(isset($mode))
	 { ?>
	 <script src="<?php echo base_url() ?>assets/js/jquery-1.7.1.min.js"></script>
<script>
	   $(document).ready(function(){		  		 
		$('.via-email').css('display','none');
		$('.via-social').css('display','none');
		$('.via-forgot').css('display','none');
		$('.via-signup').css('display','none');
		$('#emailGetMask').show();
	  });
	</script>
	<?php }
	$this->session->unset_userdata('twitter_auth');	
	?>


<?php 
if($hostPath=='localhost')
{
	global $ssl_crt ;
	global $ssl_key ;
	$ssl_crt = "/var/www/betfair/ssl/client-2048.crt";
	$ssl_key = "/var/www/betfair/ssl/client-2048.key";
}
else
{
	global $ssl_crt ;
	global $ssl_key ;
	$ssl_crt = "/var/ssl_betfair/client-2048.crt";
	$ssl_key = "/var/ssl_betfair/client-2048.key";
}

function certlogin($appKey,$params)
{
	global $ssl_crt;
	global $ssl_key;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://identitysso-api.betfair.com/api/certlogin");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSLCERT, $ssl_crt);
    curl_setopt($ch, CURLOPT_SSLKEY, $ssl_key);

	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey
    ));

    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $response = json_decode(curl_exec($ch));
   
    curl_close($ch);

    if ($response->loginStatus == "SUCCESS") {
        return $response;
    } else {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    }
}
function debug($debugString)
{
    global $DEBUG;
    if ($DEBUG)
        echo $debugString . "\n\n";
}

function sportsApingRequest($appKey, $sessionToken, $operation, $params,$market_country)
{
    $ch = curl_init();
    if($market_country =='aus'){
		curl_setopt($ch, CURLOPT_URL, "https://api-au.betfair.com/exchange/betting/json-rpc/v1");
	}
	else{
		curl_setopt($ch, CURLOPT_URL, "https://api.betfair.com/exchange/betting/json-rpc/v1");
	}
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey,
        'X-Authentication: ' . $sessionToken,
        'Accept: application/json',
        'Content-Type: application/json'
    ));

    $postData =
        '[{ "jsonrpc": "2.0", "method": "SportsAPING/v1.0/' . $operation . '", "params" :' . $params . ', "id": 1}]';

    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

    //debug('Post Data: ' . $postData);
    $response = json_decode(curl_exec($ch));
    //debug('Response: ' . json_encode($response));
 //$info = curl_getinfo($ch);
 //echo '<pre>';print_r($info);
    curl_close($ch);

    if (isset($response[0]->error)) {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    } else {
        return $response;
    }
   


}
function getNextRacingMarket($appKey, $sessionToken, $horseRacingEventTypeId,$markType,$market_country)
{

    $params = '{"filter":{"eventIds":["' . $horseRacingEventTypeId . '"],
              "marketTypeCodes":["'.$markType.'"]},
              "sort":"FIRST_TO_START",
              "maxResults":"1",
              "marketProjection":["RUNNER_DESCRIPTION"]}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketCatalogue', $params,$market_country);
	if($jsonResponse[0]->result)
	{
		return $jsonResponse[0]->result[0];
	}
	else
	{
			return '';
	}
}
function getMarketBook($appKey, $sessionToken, $marketId,$market_country)
{
	$params = '{"marketIds":[' . $marketId . '], "priceProjection":{"priceData":["EX_BEST_OFFERS"]}}';
	$jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketBook', $params,$market_country);

    return $jsonResponse;
}
function getMarketBookSingleMarket($appKey, $sessionToken, $marketId,$market_country)
{
	$params = '{"marketIds":["' . $marketId . '"], "priceProjection":{"priceData":["EX_BEST_OFFERS"]}}';
	$jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketBook', $params,$market_country);

    return $jsonResponse;
}
	$appKey = '4ae3bb5f6f042268359bba5d24dd3c2ecb1b2f37';
	$params = 'username=nlarkins&password=budda2468';
	$resp = certlogin($appKey,$params);
	
	$sessionToken = 'b0olRvuT4forfmXWaUKH59/W0mlmbizDcP7jOWVegLo=';
	$sessionToken = $resp->sessionToken;
//echo $sessionToken;die;
	$appKey1 = '97Wtq7sjReOr0uWk';
?>
<script>
$(function(){
		    var stickyRibbonTop = $('.stickyribbon').offset().top;
		      
		    $(window).scroll(function(){
		            if( $(window).scrollTop() > stickyRibbonTop ) {
		                    $('.stickyribbon').css({position: 'fixed', top: '90px'});
		            } else {
		                    $('.stickyribbon').css({position: 'static', top: '0px'});
		            }
		    });
		});
    
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".custom-select").each(function() {
            $(this).wrap("<span class='select-wrapper'></span>");
            $(this).after("<span class='holder'></span>");
        });
        $(".custom-select").change(function() {
            var selectedOption = $(this).find(":selected").text();
            $(this).next(".holder").text(selectedOption);
        }).trigger('change');
    })
</script>
<?php 
	if($MarketidInter){
		$marketTypeInter = getMarketBook($appKey1, $sessionToken, $MarketidInter,'inter');
	}
	if($MarketidAus){
		$marketTypeAust = getMarketBook($appKey1, $sessionToken, $MarketidAus,'aus');
		}

    ?>
 <?php  
  if($event_type_id ==1)
		{
			$data['colortheme'] = "#B41414";
		}
		else if($event_type_id ==4)
		{
			$data['colortheme'] = "#2888C9";
		}
		else if($event_type_id ==6)
		{
			$data['colortheme'] = "#ED281B";
		}
		else if($event_type_id ==7522)
		{
			$data['colortheme'] = "#2888C9";
		}
		
		else if($event_type_id ==2)
		{
			$data['colortheme'] = "#60D1CF";
		}
		
		else if($event_type_id ==7524)
		{
			$data['colortheme'] = "#2888C9";
		}
		
		else if($event_type_id ==1477)
		{
			$data['colortheme'] = "#FFC814";
		}
		
		else if($event_type_id ==5)
		{   
			$data['colortheme'] = "#FFC814";
		}
		
		else if($event_type_id ==61420)
		{
			$data['colortheme'] = "#ED281B";
		}
		
		else if($event_type_id ==7511)
		{
			$data['colortheme'] = "#000000";
		}
		
		else if($event_type_id ==26420387)
		{
			$data['colortheme'] = "#B41414";
		}
		else if($event_type_id ==3)
		{
			$data['colortheme'] = "#000000";
		}
		else if($event_type_id ==8)
		{
			$data['colortheme'] = "#FFC814";
		}
		else if($event_type_id ==6423)
		{
			$data['colortheme'] = "#B41414";
		}
		else if($event_type_id ==100)
		{
			$data['colortheme'] = "#0FB3EE";
		}
		?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/soccer.css">
<div class="football dark_red">
    <div class="container">
        <div class="main_outer">
            <center><a href="<?php echo base_url()?>select/place"><img src="<?php echo base_url() ?>assets/images/football_logo.png"/></a>
            </center>
            <div class="content">
                <div class="sort new-short">
                </div>
                <div class="stickyribbon">
                    <ul class="left_links nav-collapse1 in collapse" id="left_link_ul" style="height:auto;position:static;overflow:hidden;">
				<li><span>sport</span></li>
				<?php if($category) { 
					foreach($category as $cat) {
					?>
				<li>
					<a href="<?php echo base_url() ?>place-tips/<?php echo $cat['slugUrl']; ?>" <?php if($cat['event_type_id']==$event_type_id){ ?> style="color:<?php echo $data['colortheme']; ?>";<?php } ?>><?php echo $cat['name'];?></a>
				</li>
			<?php } } else { ?>
				<li><span>No sport Found</span></li>
			<?php 	} ?>
			</ul>
                </div>
                <?php 
                $country_time_zone = $this->session->userdata('timezone');
                $eventDate = $this->betfair_model->convert_time_zone($competition['open_date'],$competition['timezone'],$country_time_zone);
				$allLinks1 = date("l",strtotime($eventDate))." ".date("d/m/Y",strtotime($eventDate));
				
				if(strpos($competition['name'],'@')!==false)
				{
					$flag = 0;
					$explodeName = explode('@',$competition['name']);
				}
				else if(strpos($competition['name'],' v ')!== false)
				{
					$flag = 0;
					$explodeName = explode(' v',$competition['name']);
				}
				else
				{
					$flag = 1;
					$explodeName =$competition['name'];
				}
				if($flag==0)
				{
					$value = "<span>".$explodeName[0]."</span><p>vs</p><span>".$explodeName[1]."</span>";
				}
				else
				{
					$value ="<span>".$explodeName."</span>";
				}
				?>
                <div class="right_content new-top" id="page_content">
                   <div class="tournament">
				<div class="title"><?php echo $allLinks1;?>
				<a href="<?php echo base_url()?>place_tips/<?php echo $sportName['slugUrl']; ?>" class="buy_tip loadPurchaser" id="backbuttn" style="float: right;position: absolute;right: 0;top: 15px; width: 59px;cursor:pointer;font-size:18px;background:<?php echo $data['colortheme']?>">Back</a>
				</div>	
				<div class="teams"><?php echo $value;?></div>
				</div>
				<div class="panel-group accordion1" id="accordion1">
                       <?php 
                      // echo '<pre>';print_r($runnerDetail);die;
                       if($runnerDetail) { 
						   foreach($runnerDetail as $key=>$catalogue){
							$market_info = $this->betfair_model->getMarketInfo($catalogue[0]['market_id']);   
						   $explMArket = explode('.',$catalogue[0]['market_id']);
						   ?>  
                          <div class="panel panel-default">
                            <div class="panel-heading active">
                              <h3 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne<?php echo $explMArket[1];?>">
                                 <?php echo $market_info['market_name'];?>
                                </a>
                              </h3>
                            </div>

				<div id="collapseOne<?php echo $explMArket[1];?>" class="panel-collapse collapse">
				  <div class="panel-body inner-detail">
					  <div class="media accordion-inner">
							<div class="media-body">
								<ul>
								 <?php 
								 $i=0;
								 foreach($catalogue as $team){ 
									 $marketDesc = $this->betfair_model->getMarketInfo($team['market_id']); 
									  $event_desc = $this->betfair_model->getEventDetail($marketDesc['event_id']);
									  $tipPlaced = $this->betfair_model->checkTipPlacedMyMarketId($team['market_id'],$userId);
									 if(substr($team['market_id'], 0, 1 ) == 1){
										$type =  $marketTypeInter;
										$country_type = 'inter';
									 }else{
										 $type =  $marketTypeAust;
										 $country_type = 'aus';
										 }
										 $stat = 0;
										 foreach($type as $market){
												foreach($market->result as $market_result){
													if($market_result->marketId==$team['market_id']){
														$stat = 1;
														}
												}
											}
									if($stat==0){
										$type = getMarketBookSingleMarket($appKey1, $sessionToken, $team['market_id'],$country_type);
									}

									 $selection_odd ='';
									 if(count($type[0]->result)){
											foreach($type as $market){
												foreach($market->result as $market_result){
													foreach($market_result->runners as $runner){
														$selection_id = $runner->selectionId;
														if($tipPlaced['selection_id']==$team['selection_id'] && $tipPlaced['market_id']==$team['market_id'])
														{
															$addBackgroundclass = 'bet_placed';
															$addColorclass = 'bet_placed_color';
														}
														else
														{
															$addBackgroundclass = '';
															$addColorclass = '';
														}
														if($team['selection_id']==$selection_id && $team['market_id']==$market_result->marketId)
														{
															$selection_odd = $runner->ex->availableToBack[0]->price;
														}
													}
												}
											}
										}
									 ?>
									<li><div class="left-area"><?php echo $team['runner_name'];?></div>
										
										<div class="right-area place_bet_notify <?php echo $addBackgroundclass;?>" data-ten="<?php echo $event_type_id;?>" data-nine="<?php echo $event_desc['timezone'];?>" data-eight="<?php echo $event_desc['open_date'];?>" data-seven="<?php echo $event_desc['name'];?>" data-six="<?php echo $team['runner_name'];?>" data-five="<?php echo $selection_odd;?>" data-four="<?php echo $event_desc['competition_id'];?>" data-three="<?php echo $team['selection_id'];?>" data-two="<?php echo $team['market_id'];?>" data-one="<?php echo $event_desc['event_id'];?>" id="<?php echo 'selection_'.$explMArket[1].$team['selection_id'] ?>">
										
										<?php echo $selection_odd;?></div>
									</li>
								<?php 
								
								} ?>
								</ul>    
							</div>
					  </div>
				  </div>
				</div>
               </div>
                    
                   <?php 
                   $i++;
                   } }?>  				
						</div>
							 
					    </div>
                    </div>
                </div>
            <input type="hidden" id="loadPatients" name="loadPatients" value="2" />
            <input type="hidden" id="event_id" name="event_id" value="<?php echo $event_id;?>" />
            <input type="hidden" id="finalData" name="finalData" value="0" />
            </div>
        </div>
    </div>
</div>
<input type="hidden" value="<?php echo $event_type;?>" id="event_type_id">
<input type="hidden" value="<?php echo $login_status; ?>" name="login_status" id="login_status">
<script>
$('div.paging a').each(function()
{
	var href = $(this).attr('href');
	var temp_href = href.split('/');
	var cnt = temp_href.length;
	var cnt_last = temp_href[cnt-1];
	$(this).attr('id','a_'+cnt_last);
	$(this).attr('href','javascript:void(0)');
});
$('div.paging a').click(function(){
	var id = $(this).attr('id');
	var temp_page_number = id.split('=');
	var page_number = temp_page_number[1];
	var event_type_id = $('#event_type_id').val();
	/*$("#loading_img").css({ 
			'display':'block',
			'top':FinalHeight,
			'left':FinalWidth,
			'position':'fixed'
		
		});
	*/
	setTimeout(function()
		{
			$('#table_container').load(base_url+'get_pagination/'+event_type_id+'/'+page_number);
		}, 1000);	
});

</script>

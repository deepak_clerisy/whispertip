<!-- ***************************TWITTER CONNECT************************* ---------->
<?php
			if($userData['twitter_token'])
			{
				$twitterdata = $this->session->userdata('twitterconnectsess'); 
				if(isset($twitterdata['statuses_count'])) 
				{
					$tweets =  $twitterdata['statuses_count'];
				}
				if(isset($twitterdata['followers_count']))
				{
					$followers =  $twitterdata['followers_count'];
				}
				if(isset($twitterdata['friends_count']))
				{
					$following =  $twitterdata['friends_count'];
				}
		     }
			else
			{
				$img_connect = 'blog_icon1.png';
			}
                 ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/<?php echo $style;?>">
<div class="<?php echo $className;?>">
	<div class="container">
		<div class="main_outer">
			<center><a href="<?php echo base_url()?>select"><img src="<?php echo base_url() ?>assets/images/<?php echo $logo;?>" /></a></center>		
			<div class="content">
		<div class="sort" style="padding:0;height:0"></div>

			<div class="stickyribbon">
			<div class="menu1" >My Wallet
			<a class="menu_icon collapsed"  data-target=".nav-collapse1" data-toggle="collapse"></a>
			</div>
			<ul class="left_links nav-collapse1 in collapse" id="left_link_ul" style="height:auto;position:static;overflow:hidden;">
			
				<li><span>sport</span></li>
				<?php if($categories) { 
					foreach($categories as $cat) {
						
					?>
				<li><a href="<?php echo base_url() ?>select" style="<?php echo $style;?>"><?php echo $cat['name'];?></a></li>
			<?php } } else { ?>
				<li><span>No sport Found</span></li>
			<?php 	} ?>
			</ul>
			</div>
			<?php 
					if(isset($success))
					{
						$message = $success;
					}
					if(isset($error))
					{
						$message = $error;
					}
            	?>
			<?php 
			if((strstr($userData['profile_image'],'http://')==$userData['profile_image'] || strstr($userData['profile_image'],'https://')==$userData['profile_image']) && $userData['profile_image']!='')
					{
						$image =  "<img src='".str_replace('_normal','',$userData['profile_image'])."' style='height:142px;width:140px;'/>";
					}
					else
					{
						if($userData['profile_image']!='' && file_exists('uploads/profile_picture/thumb_'.$userData['profile_image']))
						{
							$image =   "<img src='".base_url().'uploads/profile_picture/thumb_'.$userData['profile_image']."' style='height:142px;width:140px;'/>";
						}
						else
						{
							$image =   "<img src='".base_url().'assets/images/default-no-profile-pics.jpg'."' style='height:142px;width:140px;'>";
						}
					}
					if($userData['facebook_id'])
						{
							$redirect = '<a href="http://www.facebook.com/profile.php?id='.$userData['facebook_id'].'" target="_blank">';	
						}
						else
						{
							$redirect = '<a href="#">';
						}
						if($userData['tw_screen_name'])
						{
							$redirect_twitter = '<a href="https://twitter.com/'.$userData['tw_screen_name'].'" target="_blank">';	
						}
						else
						{
							$redirect_twitter = '<a href="#">';
						}
					?>
			<div class="right_content" id="page_content" >
				
				<div class="blog coach_blog">
					<div class="blog_img" style="width:137px;"><?php echo $image;?></div>
					<div class="blog_dis">
							<div class="blog_row username" ><?php echo $userData['first_name'].' '.$userData['last_name'];?></div>
							
							<div class="blog_row" ><div class="pull-left">Gender</div><div class="pull-right"><?php echo $userData['gender']; ?></div></div>
							<?php if($userData['twitter_token']) { ?>
							<div class="blog_row" ><div class="pull-left">Twitter</div><div class="pull-right" style=" text-align: right; width: 80px;"><?php echo '<b>'.$tweets.'</b>'.' Tweet, <b>'.$followers.'</b> Followers <b>,'.$following.'</b>  Following'?></div></div>
							<?php } else { ?>
							<div class="blog_row" ><div class="pull-left">Not Connected</div><div class="pull-right"><img src="<?php echo base_url()?>assets/images/<?php echo $img_connect;?>" id="connectTwitter" style="cursor:pointer;"></div></div>
							<?php } ?>
							
							<div class="blog_row" ><div class="pull-left">connect</div>
							<div class="pull-right" style="padding-top:5px;">
								<?php echo $redirect_twitter; ?><img src="<?php echo base_url()?>assets/images/blog_icon1.png"/></a>
								<?php echo $redirect; ?><img src="<?php echo base_url()?>assets/images/blog_icon3.png"/></a>
								<a href="mailto:<?php echo $userData['email'];?>" >
								<img src="<?php echo base_url()?>assets/images/blog_icon4.png"/></a>
							</div>
							</div>
					</div>
					<?php 
					$credit_checkout = $this->session->userdata('creditpayment');
					//echo '<pre>';print_r($credit_checkout);die;
					if($credit_checkout=='credit')
					{
						$style ='display:none';	
						$style_new = 'display:block';
						$val = '';
					}
					else if($credit_checkout =='email')
					{
						$style ='display:none';	
						$style_new = 'display:block;margin-top: 16px;';
						$val = 1;
					}
					else
					{
						$style ='';	
						$style_new = 'display:none;margin-top: 16px;';
						$val = '';
					}
					?>
				<div class="but_tip_coach mywallet">
						<div class="widthdraw">
						<div class="widthdraw_heading">My Wallet <span style="float:right;"></span></div>
						<p>Current Balance in your Wallet: <b>AUD <?php echo $wallet_data['balance']; ?></b></p>
						<div id="walletwrapper" style="<?php echo $style;?>">
							<span>You can add money in to your wallet.</span>
							<a href="#" class="buy_tip pull-left addmoney">Add money via Paypal</a>
							<a href="#" class="buy_tip pull-left addcreditcard">Add money via Credit card</a>
							<span>You can withdraw money from your wallet.</span>
							<a href="#" class="buy_tip pull-left withdrawmoney">Withdraw money</a>
						</div>
						<div id="confirm_credit_payment" style="<?php echo $style_new;?>">
						<span style="color: rgb(119, 119, 119); margin-bottom: 11px; font-size: 14px;">Thanks for successfully authenticating Paypal.Please click confirm button to add money in your wallet. </span>
							<a href="<?php echo base_url()?>paypal/dopaypalpayment/<?php echo $val?>" class="buy_tip pull-left">Confirm</a>
							<a href="javascript:void(0)" class="buy_tip pull-left cancelpaypalpayment">Cancel</a>
							</div>
						</div>
						
					</div>
					<?php  $this->session->unset_userdata('creditpayment');
							?>
						<div class="but_tip_coach withdraw" style="display:none;">
							<div class="widthdraw">
							<div class="widthdraw_heading">My Wallet <a class="buy_tip loadPurchaser backwithdraw">Back</a></div>
							<p>Current Balance in your Wallet: <b>AUD <?php echo $wallet_data['balance']; ?></b></p>
							<form id="withdrawMoneyForm" name="addMoneyForm" method="post" action="<?php echo base_url().'withdrawMoney' ?>" >
							<span style="width:120px;display:inline-block; text-align:right; padding-right:5px;">Paypal Email</span>
							<input type="text" class="input_txt" name="paypalemail" id="paypalemail"/>
							<span style="width:120px;display:inline-block; text-align:right;padding-right:5px;">AUD</span>
							<input type="text" class="input_txt" name="withdrawamount" id="withdrawamount" />
							<a href="#" class="buy_tip pull-left" id="withdrawAmountBtn" style="margin-left:123px;margin-top:5px;">Withdraw</a>
						</div>
						</form>
					</div>
						<div class="but_tip_coach addmoneyfm" style="display:none;">
							<div class="widthdraw">
								<div class="widthdraw_heading">My Wallet <a class="buy_tip loadPurchaser backwithdraw">Back</a></div>
								<p>Current Balance in your Wallet: <b>AUD <?php echo $wallet_data['balance']; ?></b></p>
								<span style="width:120px;display:inline-block; text-align:right;padding-right:5px;">AUD</span>
								<input type="text" name="addamount" id="addamount" class="input_txt" placeholder="Amount"/>
								<a href="#" class="buy_tip pull-left" style="margin-left:123px;margin-top:5px;" onclick="AddWalletMoney('<?php echo base_url()?>')">Add</a>
								
							</div>
					</div>
						<div class="but_tip_coach creditcard" style="display:none;">
							<div class="widthdraw">
								<div class="widthdraw_heading">My Wallet <a class="buy_tip loadPurchaser backwithdraw">Back</a></div>
								<p>Current Balance in your Wallet: <b>AUD <?php echo $wallet_data['balance']; ?></b></p>
								<span style="width:120px;display:inline-block; text-align:right;padding-right:5px;">AUD</span>
								<input type="text" name="addcreditamount" id="addcreditamount" class="input_txt" placeholder="Amount" placeholder="Amount"/>
								<a href="#" class="buy_tip pull-left" style="margin-left:123px;margin-top:5px;" onclick="AddWalletMoneyCredit('<?php echo base_url()?>')">Add</a>
								
								</form>
							</div>
					</div>
					<?php 
					if(isset($success))
					{
						echo "<span style='color: #008000;float: left;margin-top: 12px;width: 100%;' class='err_msg'>".$success."</span>";
					}
					if(isset($error))
					{
						echo "<span style='color: #FF0000;float: left;margin-top: 12px;width: 100%;' class='err_msg'>".$error."</span>";
					}
            	?>
			</div>
			
			</div>
			
		</div>
	</div>
</div>
<script>
function AddWalletMoney(url)
{
	var addamount = $.trim($('#addamount').val());
	if (addamount == '' || addamount <0) {
		$('#addamount').focus();
		$('#addamount').addClass('error');
	}
	else
	{
		var windowWidth = $(window).width();
		var windowHeight = $(window).height();
		var FinalHeight = windowHeight/2;
		var FinalWidth = (windowWidth/2)-100;
		$("#loading_img").css({ 
			'display':'block',
			'top':FinalHeight,
			'left':FinalWidth,
			'position':'fixed'
		
		});
	var urls =  url+"paypal/process/1";
	//alert(urls);return false;
	var formdata='addamount='+addamount;
		$.ajax({
		type:"post",
		url: urls,
		data:formdata,
		 success: function (response)
		{
			if(response){
				$("#loading_img").css('display','none');
				window.location.href=response;
				//myWindow=window.open(response,'_blank','width=810,height=570');
			//myWindow=window.open(response,'_blank','width=810,height=570');
			var timer = setInterval(function() {  
			if(myWindow.closed) {  
				clearInterval(timer);  
				var urls = url+'home/walletAddMoney';
				location.href=urls;
			}  
		}, 1000); 
				}
			
		}
	});	
	
	}

}
function AddWalletMoneyCredit(url)
{
	var addamount = $.trim($('#addcreditamount').val());
	if (addamount == '' || addamount <0) {
		$('#addcreditamount').focus();
		$('#addcreditamount').addClass('error');
	}
	else
	{
		var windowWidth = $(window).width();
		var windowHeight = $(window).height();
		var FinalHeight = windowHeight/2;
		var FinalWidth = (windowWidth/2)-100;
		$("#loading_img").css({ 
			'display':'block',
			'top':FinalHeight,
			'left':FinalWidth,
			'position':'fixed'
		
		});
	var urls =  url+"paypal/process/0";
	var formdata='addamount='+addamount;
		$.ajax({
		type:"post",
		url: urls,
		data:formdata,
		 success: function (response)
		{
			if(response){
			$("#loading_img").css('display','none');
			window.location.href=response;
			//myWindow=window.open(response,'_blank','width=810,height=570');
			
			var timer = setInterval(function() {  
			if(myWindow.closed) {  
				clearInterval(timer);  
				var urls = url+'home/walletAddMoney';
				location.href=urls;
			}  
		}, 1000); 
				}
			
		}
	});	
	
	}

}
function payviacreditcard(urls)
{
	var url =  urls+"paypal/process/0";
	myWindow=window.open(url,'_blank','scrollbars=1');

}
function payviaemail(urls)
{
	var url =  urls+"paypal/process/1";
	myWindow=window.open(url,'_blank','scrollbars=1');

}
/*
function pay_credit_card(urls)
	{
		var url=urls+"paypal/dopaypalpayment";
		var windowWidth = $(window).width();
		var windowHeight = $(window).height();
		var FinalHeight = windowHeight/2;
		var FinalWidth = windowWidth/2;
		
		$("#loading_img").css({ 
			'display':'block',
			'top':FinalHeight,
			'left':FinalWidth,
			'position':'fixed'
		
		});
		$.ajax({
		type:"post",
		url: url,
		 success: function (response)
		{
			if(response)
			{  
					$("#loading_img").css('display','none');
					location.reload();
				//$("#payment_after_normal").css('display','none');
				//$("#contact_msg1").css('display','block');
			}
			
		}
	});
}

*/
</script>



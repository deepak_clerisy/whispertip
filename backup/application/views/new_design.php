<!DOCTYPE html">
<html lang="en">

<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/front/style_new.css">
 
 <title>WhisperTip</title>
  <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/football_title_new.ico" type="image/x-icon" />
<script src="<?php echo base_url() ?>assets/js/jquery-1.7.1.min.js"></script>
<script src="<?php echo base_url()?>assets/js/html5.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/js/miscellaneous.js" type="text/javascript"></script>
 <script>
	var base_url = '<?php echo  base_url();?>';
</script>
<?php if($auth == 'authenticated')
	 { ?>
<script>
	  $(document).ready(function(){
		 $('#popup_container_login').attr('style','display:block;');
	  });
	</script>
	<?php }?>
</head>

<body>
<!-- Header start here -->
<header class="header">
	<div class="container">
		<div class="logo">
			<h1><a href=""><img src="<?php echo base_url()?>assets/images/logo.png"></a></h1>
		</div>
		<div class="login_registration">
			<input type="button" class="button registration" value="Login">
			<input type="button" class="button registration" id="signup"  value="Registration">
		</div>
	</div>
</header>
<!-- Header end here -->
<!-- Banner start here -->
<div class="banner">
	<div class="container">
		<div class="left_text">
				<h1>Buy or Selling online </h1>
				<h2>Sports tips</h2>
				<h5>Get ahead of the game!</h5>
				<p>Love sport and love having a bet on it but you are constantly losing? Buy from the best! Whispertip is home to the best tipsters in the world. </p>
				<a href="<?php echo base_url()?>home/select">Lets Start Now <img src="<?php echo base_url()?>assets/images/button_start.png"></a>
		</div>
		<div class="right_image">
			<img src="<?php echo base_url()?>assets/images/man.png"/>
		</div>
	</div>
</div>
<!-- Banner start here -->

<!-- Chance winning  -->
<div class="chance_winning_wrapper">
    <div class="container">
	    <div class="content_section">
		     <h2>Increase your <span> chances of winning</span></h2>
			 <p>Before placing your bets, login to Whispertip and buy from the people who know! Simply select the sport you're interested in and buy from Tipster's with a proven track record. Whispertip's leaderboard ranks tipsters on <strong>correct tips, strike rate,</strong> profit (based off placing $100 bets) and <strong>average tipping odds.</strong></p>
		</div>
		<img class="mobile_1" src="<?php echo base_url()?>assets/images/mobile_1.png" alt="" />
	</div>
</div>
<!-- Chance winning  -->

<!-- Section -->
<div class="section">
     <div class="banner_img_2">
	    <div class="drop_arrow"> <img src="<?php echo base_url()?>assets/images/drop_arrow.png" alt="" /></div>
	 </div>
</div>
<!-- Section -->


<!-- Chance winning  -->
<div class="tipstar_wrapper">
    <div class="container">
	    <div class="content_section">
		     <h2>Prove you're a <span> Professional Tipster</span></h2>
			 <p>Think you've got what it takes to become a professional tipster? Prove it. Log in and place tips on live sporting markets from around the world. Monetize your knowledge. Use Twitter and Facebook to TWEET/SHARE your tips driving more exposure to your Whispertip Profile. People will buy your tips and the more succesful you are, the more tips you will sell. </p>
		</div>
		<img class="tablet" src="<?php echo base_url()?>assets/images/tablet.png" alt="" />
	</div>
</div>
<!-- Chance winning  -->


<!-- Section -->
<div class="section">
    <div class="banner_img_1">
	   <div class="drop_arrow"> <img src="<?php echo base_url()?>assets/images/drop_arrow.png" alt="" /></div>
	</div>
</div>
<!-- Section -->



<!-- sporting Knowledge start here -->
<div class="features_content">
	<div class="container">
		<h1>Sporting <span>Knowledge</span></h1>
		<h4>Track and manage your tips from anywhere.</h4>
		<div class="system_show">
			<div class="left_like">
				<h2>I want to buy Tips</h2>
				<p>Select I want to "BUY" tips for, followed by the desired sport.</p>
				<img src="<?php echo base_url()?>assets/images/left-right.png"/>
			</div>
			
			<div class="sell_tip">
				<h2>I want to Sell Tips</h2>
				<p>Select I want to "SELL" tips for, followed by the desired sport..</p>
				<img src="<?php echo base_url()?>assets/images/right-left.png">
			</div>
			<div class="clear"></div>
			<div class="spots">
				<h2>Spots Categories</h2>
				<p>Select from our wide range of live sports markets.</p>
				<img src="<?php echo base_url()?>assets/images/bottom_top.png"/>
			</div>
			<img class="mack-book" src="<?php echo base_url()?>assets/images/mack_system.png" alt="" />
		</div>
		<div class="row1"><a class="lets-start" href="<?php echo base_url()?>home/select">Lets Start Now <img src="<?php echo base_url()?>assets/images/button_start.png" alt="" /></a> </div>
		
	</div>
</div>
<!-- sporting Knowledge start here -->


<!-- Section -->
<div class="section">
    <div class="banner_img_3">
	   <div class="drop_arrow"> <img src="<?php echo base_url()?>assets/images/drop_arrow.png" alt="" /></div>
	</div>
</div>
<!-- Section -->

<!-- Footer section start here -->
<div class="footer">
	<div class="container">
		<div class="left_link">
			<ul>
				<li><a href="<?php echo base_url()?>terms_conditions/do_login" target="_blank">Terms and conditions</a></li>
				<li><a href="<?php echo base_url()?>privacy_policy/do_login">Privacy policy</a></li>
				<li><a href="mailto:support@whispertip.com" >Contact Us</a></li>
			</ul>
		</div>
		<div class="social_icon">
			<p>Get Social With Us</p>
			<ul>
			<li><a href="https://facebook.com/whispertip" target="_blank"><img src="<?php echo base_url()?>assets/images/facebook.png"></a></li>
			<li><a href="http://twitter.com/whispertip" target="_blank"><img src="<?php echo base_url()?>assets/images/twitter.png"></a></li>
			<li><a href="http://instagram.com/whispertip" target="_blank"><img src="<?php echo base_url()?>assets/images/instagram.png"></a></li>
			</ul>
		</div>
	</div>
</div>
<!-- Footer section start here -->


<!--/////////////////////////popup-start/////////////////////////////-->
	<div class="popup_container_login"  id="popup_container_login" style="display:none">
		<div class="popup_outer_login">
			<a href="javascript:void(0)" class="popup_img_login" style="left:5px;"></a>
			<a href="javascript:void(0)" class="popup_img_login" style="right:15px; top:10px;">
				<img src="<?php echo base_url();?>assets/images_new/cross.png" class="close_popup_login" /></a>
			<h1>Login Form</h1>
			
			<div class="reg_option">
				<?php if($auth == 'authenticated')
					{
						$url =  base_url()."home/login_popup/auth"; 
					} 
					else
					{
						$url = base_url()."home/login_popup";
					}
						 ?>
				<script>
				$(document).ready(function(){
					$('#iframe').load('<?php echo $url;?>');
					})
				</script>
				<div src="<?php echo $url;?>" id="iframe" frameborder="0" ></div>
			</div>
	</div>
	
<!--////////////////////////////////////// popup ////////////////////////////////////-->

</body>

</html>

<style>
.sort_sel{color:#2888C9 !important;}
.sort{padding-left:0px;}
.right_content{width:100%;text-align:center;float:none;}
.blog_dis h1{text-align:left;}
</style>
<script type="text/javascript">
 $(document).ready(function() {
	var view = $('#show_data').val();
	pages = Math.ceil('<?php echo count($result)  ?>'/30);
	$('.blog').hide();
	$('.show_'+view).show();
	$('.show_more').click(function() {
		$('#show_data').val(parseInt($('#show_data').val()) + 1);
		var view = $('#show_data').val();
		$('.show_'+view).slideDown();
		if(pages == view)
		{
			$('.show_more').hide();
		}
	});
});
</script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/basketball.css">
<div class="sport blue">
	<div class="container">
		<div class="main_outer">
			<center><a href="<?php echo base_url()?>select"><img src="<?php echo base_url() ?>assets/images/coach_logo.png" /></a></center>		
			<div class="content">
				<div class="sort" ><span>sort:</span>
				<a href="javascript:void(0)" data-uri="profit" class='sorting_content_overall sort_sel'>PROFIT</a>
				<a href="javascript:void(0)" data-uri="strike" class='sorting_content_overall'>STRIKE RATE</a> 
				<a href="javascript:void(0)" data-uri="correcttips" class='sorting_content_overall'>CORRECT TIPS</a> 
			</div>
			<div class="stickyribbon">
				<div class="menu1"><?php echo "Menu"; ?>
				<a class="menu_icon collapsed" data-target=".nav-collapse1" data-toggle="collapse"></a>
			</div></div>
			<div class="right_content sortingDiv">
				<?php 
				if($result) {
				$count = 1;	
				foreach($result as $res) { 
					$ave_odds = '-';
					if($res['total_won_tips'])
					{
						$ave_odds = round(($res['total_odds']/$res['total_won_tips']),2);
					}
					if($res['total_placed_tips'] < 10) 
					{
						$strike = 0;	
					} else {
						$strike = round(($res['total_won_tips']/$res['total_placed_tips'])*100,2);
					} 
					$twitter_user_id='';
					if($res['tw_token'])
					{
						$res1 = $res['tw_token'];
						$sp1 = explode('&',$res1);
						$final = explode('=',$sp1[0]);
						$final1 = explode('=',$sp1[1]);
						$user_id_twitter = explode('-',$final[1]);
						$twitter_user_id = $user_id_twitter[0];
					}	
					if($twitter_user_id)
					{
						$redirect = '<a href="javascript:void(0)" onclick="twitter_share('.$twitter_user_id.')" class="twitter_share1" title="Follow">';
						$twitter_style="display:block";	
					}
					else
					{
						$redirect = '<a href="javascript:void(0)">';
						$twitter_style="display:none";
					}
					?>
						<div class="blog show_<?php echo ceil($count/30); ?>">
							<div class="number"><span>NO</span><sub><?php echo $res['rank'];?></sub></div>
							<div class="blog_img">
								<img src="<?php echo $res['image']; ?>" style="height:99px;width: 77px;"></a>
							</div>
							<div class="blog_dis">
									<h1>
										<?php echo $res['firstname'].' '.$res['lastname'];?></a><div class="pull-right" style="<?php echo $twitter_style;?>">
										<?php echo $redirect;?><img src="<?php echo base_url() ?>assets/images/blog_icon1.png"/>
											<img src="<?php echo base_url() ?>assets/images/blog_icon2.png"/>
											</a>
										</div>
									</h1>
									<div class="blog_row"><div class="pull-left">Correct Tip</div><div class="pull-right"><?php echo $res['total_won_tips'].'/'.$res['total_placed_tips'];?></div></div>
									<div class="blog_row"><div class="pull-left">Average odds</div><div class="pull-right"><?php echo $ave_odds;?></div></div>
									<div class="blog_row"><div class="pull-left">Strike rate</div><div class="pull-right"><?php echo $strike;?></div></div>
									<div class="blog_row">
										<div class="hintModal">
										<div class="pull-left">Profit</div>
										<div class="pull-right"><?php 
											if(isset($res['profit'])){
												echo '$'.$res['profit'];
											} else
											{
												echo '$0.00';
											}
											?></div>
									<div class="hintModal_container">
										Based off placing $100 on each tips you would have made
									</div>
									</div>
									</div>
							</div>
						</div>
				<?php $count++; } }
				else
				{
					echo  "<span style='float:left;width:100%;text-align:center;color:white;font-size:17px; margin-top: 20px;'>There are currently no Tipsters</span>";
				}
				if(count($result) > 30) {
				?>
					<div style="clear:both;margin-right:57px;text-align:right;"><a href="javascript:void(0);" class="show_more">Show More</a></div>
				<?php } ?>
				<input type="hidden" value="<?php echo $login_status; ?>" name="login_status" id="login_status">
				<input type="hidden" value="1" id="show_data">
				</div>
		</div>
	</div>
</div>
<input type="hidden" value="<?php echo base_url();?>" id="url">
<input type="hidden" value="<?php echo $res['user_id'];?>" id="user_id">

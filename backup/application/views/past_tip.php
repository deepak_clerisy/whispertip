<?php 
if($_SERVER['HTTP_HOST']=='localhost')
{
	global $ssl_crt ;
	global $ssl_key ;
	$ssl_crt = "/var/www/betfair/ssl/client-2048.crt";
	$ssl_key = "/var/www/betfair/ssl/client-2048.key";
}
else
{
	global $ssl_crt ;
	global $ssl_key ;
	$ssl_crt = "/var/ssl_betfair/client-2048.crt";
	$ssl_key = "/var/ssl_betfair/client-2048.key";
}

function certlogin($appKey,$params)
{
	global $ssl_crt;
	global $ssl_key;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://identitysso-api.betfair.com/api/certlogin");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSLCERT, $ssl_crt);
    curl_setopt($ch, CURLOPT_SSLKEY, $ssl_key);

	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey
    ));

    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $response = json_decode(curl_exec($ch));
   
    curl_close($ch);

    if ($response->loginStatus == "SUCCESS") {
        return $response;
    } else {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    }
}
function debug($debugString)
{
    global $DEBUG;
    if ($DEBUG)
        echo $debugString . "\n\n";
}

function sportsApingRequest($appKey, $sessionToken, $operation, $params,$market_country)
{
    $ch = curl_init();
    if($market_country =='aus'){
		curl_setopt($ch, CURLOPT_URL, "https://api-au.betfair.com/exchange/betting/json-rpc/v1");
	}
	else{
		curl_setopt($ch, CURLOPT_URL, "https://api.betfair.com/exchange/betting/json-rpc/v1");
	}
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Application: ' . $appKey,
        'X-Authentication: ' . $sessionToken,
        'Accept: application/json',
        'Content-Type: application/json'
    ));

    $postData =
        '[{ "jsonrpc": "2.0", "method": "SportsAPING/v1.0/' . $operation . '", "params" :' . $params . ', "id": 1}]';

    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

    debug('Post Data: ' . $postData);
    $response = json_decode(curl_exec($ch));
    debug('Response: ' . json_encode($response));

    curl_close($ch);

    if (isset($response[0]->error)) {
        echo 'Call to api-ng failed: ' . "\n";
        echo  'Response: ' . json_encode($response);
        exit(-1);
    } else {
        return $response;
    }

}
function getNextRacingMarket($appKey, $sessionToken, $horseRacingEventTypeId,$markType,$market_country)
{
    $params = '{"filter":{"eventIds":["' . $horseRacingEventTypeId . '"],
              "marketTypeCodes":["'.$markType.'"]},
              "sort":"FIRST_TO_START",
              "maxResults":"1",
              "marketProjection":["RUNNER_DESCRIPTION"]}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketCatalogue', $params,$market_country);

    return $jsonResponse[0]->result[0];
}
function getMarketBook($appKey, $sessionToken, $marketId,$market_country)
{
    $params = '{"marketIds":["' . $marketId . '"], "priceProjection":{"priceData":["EX_BEST_OFFERS"]}}';

    $jsonResponse = sportsApingRequest($appKey, $sessionToken, 'listMarketBook', $params,$market_country);

    return $jsonResponse[0]->result[0];
}
	$appKey = '4ae3bb5f6f042268359bba5d24dd3c2ecb1b2f37';
	$params = 'username=nlarkins&password=budda2468';
	$resp = certlogin($appKey,$params);

	//$sessionToken = 'Oc7tHJKtYJeViN/Auey+3EPOncSCPhQZvRB7DWuxZMA=';
	$sessionToken = $resp->sessionToken;

	$appKey1 = '97Wtq7sjReOr0uWk';


?>
					<div class="teams"><?php echo $tip_type;?> <a class="buy_tip loadPurchaser" onclick="window.location.reload();">Back</a></div><br>
						 <?php
                        if($events) {
							$listDate = '';
							$flag = 0;
							foreach($events as $event)
							{
								$placedTip = $this->betfair_model->getPlacedTipByComp($event['event_id'],$event['market_id'],$userId);
								$MarketName = $this->betfair_model->getMarketNameById($event['market_id']);
								$event_name = $this->category_model->getAllCategoriesBYId($event['event_type_id']);
								$time_val = $this->session->userdata('timezone');
								$date = $this->betfair_model->convert_time_zone($event['open_date'],$event['timezone'],$time_val);
								$date1 = date("l",strtotime($date))." ".date("d/m/Y",strtotime($date));
				        ?>
						<span style="border:none;"><?php echo $date1. ' - ' . date("h:i a",strtotime($date)); ?></span>
						
						<div class="team_names no_border"><?php echo $event['event_name']; ?> (<?php echo $event_name['name'];?>)</div>
							<div style="text-align:center;">
								<div class="team_names" style="display:block;"><p><?php echo $MarketName['market_name']; ?></p></div>
							</div>
							<div style="text-align:center;">
								<div class="blog_dis">
								 <?php 
							$parts  = explode('.', (string)$event['market_id']);	
								if($parts[0]==2)
								{
									$market_country = 'aus';
								}
								else
								{
									$market_country = 'inter';
								}
								$Type = $this->betfair_model->getMarketType($event['market_id']);
								$type = @$Type['market_type'];
								$marketType1 = getNextRacingMarket($appKey1, $sessionToken, $event['event_id'],$type,$market_country);
							   if($marketType1) {
								$marketType2 = getMarketBook($appKey1, $sessionToken, $event['market_id'],$market_country);
							  foreach($marketType1->runners as $resp)
								{
									$market_id = $marketType1->marketId;
									$team_id = $resp->selectionId;
									if($team_id == $event['selection_id'])
									{
									$Color='color:#2888C9;';	
									}
									else
									{
										$Color='color:#ffffff;'; 
									}
									$team_name = $resp->runnerName;
								if(count($marketType2->runners))
								{
									foreach($marketType2->runners as $respm)
									{			
										$team_id_odd = 	$respm->selectionId;
										if($team_id==$team_id_odd)
										{
										$team_odds = @$respm->ex->availableToBack[0]->price;
										}
									}
								}
									
							   ?>	
									
								<div class="blog_row"><div class="pull-left" style="<?php echo $Color;?>"><?php echo $team_name; ?></div><div class="pull-right"><?php echo $team_odds ; ?></div></div>
							<?PHP }  } else { 
								$result = $this->betfair_model->getPurchasedtipResult($event['competition_id'],$event['market_id'],$tipsterId);
								if($result){
								$color_green = 'color:#008000;';
								$color_red = 'color:#FF0000;';
								$listDate = '';
								$ii=0;
								$market_name = $this->betfair_model->getMarketType($result['market_id']);	
									if(date("d/m/Y",strtotime($result['open_date'])) != $listDate)
									{
										$flDate = 1;
										$listDate = date("d/m/Y",strtotime($result['open_date']));
									}
									else
									{
										$flDate = 0;
									} ?>
									<div class="result_row1">
										  <div class="result_date">
											  <?php 
										$eventDate1 = $this->betfair_model->convert_time_zone($result['open_date'],$result['timezone'],$this->session->userdata('timezone'));
										echo date("h:i a",strtotime($eventDate1)); 
									?>
											  </div>
										</div>
										<div class="result_detail1">
										<section style="width:40%;"> <span><?php echo $result['team_name'];?></span></p><span>ODDS - </span><span> <?php echo $result['selected_odd'];?></span></section>		
										<?php if($result['winner_id']==$result['selection_id']) { ?>
										<section style="<?php echo $color_green; ?>">(Won)</section>
										<?php } else { ?>
										<section style="<?php echo $color_red; ?>">(Loss)</section>
										<?php 	}?>
								</div>	
							
						<?php } } ?>
					</div>
						</div><br><br>
							<?php } } else { echo "Currently No Tip Found"; }?>
						

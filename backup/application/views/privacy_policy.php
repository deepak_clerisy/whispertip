<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/basketball.css">
<style>
	.ulclass {
	 color: #777777;
    line-height: 23px;
    margin-left: 31px;
}
p{
	line-height:23px;
	}
	</style>
<div class="sport">
	<div class="container">
		<div class="sports_outer"><div class="stickyribbon"></div>
			<center>
			<a href="<?php echo base_url()?>select"><img src="<?php echo base_url();?>assets/images/sports_logo.png"/></a></center>
			<div class="sell_buy">
			PRIVACY POLICY
			</div>
		<div class="outer_tips" id="outer_place_tip">
			<div class="tips_outer" style="width:641px;">
				<div class="tip_list">
					<p>This Privacy Policy is designed to help you understand how we collect and use
					the personal information you decide to share, and help you make informed
					decisions when using the Website, located at and its directly associated
					domains (collectively, "the Website") owned and run by Pty Ltd (also referred
					to as "us" or "we").
					By using or accessing the Website, you are accepting the practices described in
					this Privacy Policy.
					</p>
					<h1>1. The Information We Collect</h1>
					<p>
					When you visit the Website you provide us with two types of information:
					personal information you knowingly choose to disclose that is collected by us
					and Webite use information collected by us as you interact with .
					When you register with , you provide us with certain personal information,
					such as your name, your email address, your telephone number, your address
					and any other personal or preference information that you provide to us. When
					you enter he Website, we collect your browser type and IP address. This
					information is gathered for all Website visitors. In addition, we store certain
					information from your browser using "cookies." A cookie is a piece of data
					stored on the user's computer tied to information about the user. By default,
					we use a persistent cookie that stores your login ID (but not your password) to
					make it easier for you to login when you come back to he Website. You can
					remove or block this cookie using the settings in your browser if you want to
					disable this convenience feature.</p>
					<p>
						When you use The Website, you may set up your personal profile, form
						relationships, send messages, perform searches and queries, form groups and
						transmit information through various channels. We collect this information so
						that we can provide you the service and offer personalized features. In most
						cases, we retain it so that, for instance, you can return to view prior messages
						you have sent or easily see your following feed. When you update information,
						we usually keep a backup copy of the prior version for a reasonable period of
						time to enable reversion to the prior version of that information.
						You post User Content (as defined in The Website ) on he ite at your own risk.
						Although we allow you to set privacy options that limit access to your pages,
						please be aware that no security measures are perfect or impenetrable. We
						cannot control the actions of other sers with whom you may choose to share
						your pages and information. Therefore, we cannot and do not guarantee that ser
						ontent you post on he ite will not be viewed by unauthorized persons.
						We are not responsible for circumvention of any privacy settings or security
						measures contained on the Site. You understand and acknowledge that, even
						after removal, copies of User Content may remain viewable in cached and
						archived pages or if other Users have copied or stored your User Content. Any
						improper collection or misuse of information provided on The Website is a
						violation of the The Website Terms of Service and should be reported to The
						Website by .</p>
						<p>
						If you choose to use our invitation service to tell a friend about our site, we will
						ask you for information needed to send the invitation, such as your friend's
						email address. We will send your friend an email or instant message in your
						name inviting him or her to visit the site and may send up to two reminders to
						them.</p>
						<p>
						The Website stores this information to send invitations and reminders, to
						register a friend connection if your invitation is accepted, to allow you to see
						invitations you have sent, and to track the success of our referral program.
						Your friend may contact us to request that we remove this information from
						our database. By using The Website, you are consenting to have your personal
						data transferred to and processed in Australia and Singapore.</p>
				</div>
				<div class="tip_list">
					<h1>2. Use of Information Obtained by The Website</h1>
					<p>When you register with The Website, you create your own profile and privacy
						settings. Your profile information such as your username, suburb, state,
						country, photo, posts and tipping information are displayed to people.
						We may occasionally use your name and email address to send you
						notifications regarding new services offered by The Website that we think you
						may find valuable.Profile information is used by The Website primarily to be
						presented back to and edited by you when you access the service and to be
						presented to others permitted to view that information by you. In some cases
						where your privacy settings permit it (e.g., posting to your wall), other The
						Website users may be able to supplement your profile.</p>
					<p>Profile information you submit to The Website will be available to users of The
						Website. Any profile information displayed will be available in search results
						across The Website network and those limited pieces of information may be
						made available to third party search engines. This is primarily so other users
						can find you and send a mate request.</p>
						<p>
						The Website may send you service-related announcements from time to time
						through the general operation of the service. For instance, if a friend sends you
						a new message or someone posts on your wall, you may receive an email
						alerting you to that fact.</p>
						<p>
						Generally, you may opt out of such emails from the 'My Profile' page when you
						are logged in, though The Website reserves the right to send you notices about
						your account even if you opt out of all voluntary email notifications.</p>
						</div>
						<div class="tip_list">
							<h1>3. Sharing Your Information with Third Parties</h1>
						<p>
						The Website is about sharing information with others. We allow you to enter
						the information which is shown to other users of The Website. We do not
						provide contact information to third party marketers without your permission
						with the exception of competition sponsors to notify the winners and award
						prizes. We share your information with third parties only in limited
						circumstances where we believe such sharing is</p>
						<p>1) reasonably necessary to offer the service,<br>
						2) legally required or,<br>
						3) permitted by you.<br>
						</p>
						<p>For example:</p>
						<ul class="ulclass">
							<li> ●  We may provide information to service providers to help us bring you
							the services we offer. Specifically, we may use third parties to facilitate
							our business, such as to host the service at a co-location facility for
							servers, to send out email updates about The Website, to remove
							repetitive information from our user lists, to process payments for
							products or services, to offer an online job application process, or to
							provide search results or links (including sponsored links). In
							connection with these offerings and business operations, our service
							providers may have access to your personal information for use for a
							limited time in connection with these business activities. Where we
							utilize third parties for the processing of any personal information, we
							implement reasonable contractual and technical protections limiting the
							use of that information to The Website-specified purposes.</li>
							<li> ●  We may be required to disclose user information pursuant to lawful
							requests, such as subpoenas or court orders, or in compliance with
							applicable laws. We do not reveal information until we have a good faith
							belief that an information request by law enforcement or private
							litigants meets applicable legal standards. Additionally, we may share
							account or other information when we believe it is necessary to comply
							with law, to protect our interests or property, to prevent fraud or other
							illegal activity perpetrated through The Website service or using The
							Website name, or to prevent imminent bodily harm. This may include
							sharing information with other companies, lawyers, agents or
							government agencies.</li>
							<li>
							●  We may provide services jointly with other companies on The Website
							and we may share customer information with that company in
							connection with your use of that service.</li>
							<li> ●  If the ownership of all or substantially all of The Website business, or
							individual business units owned by , were to change, your user
							information may be transferred to the new owner so the service can
							continue operations. In any such transfer of information, your user
							information would remain subject to the promises made in any
							pre-existing Privacy Policy.</li></ul>
							<p>
							When you use The Website, certain information you post or share with third
							parties (e.g., a friend or someone in your network), such as personal
							information, comments, messages, photos, tips, tipping trends or other
							information, may be shared with other users in accordance with the privacy
							settings you select.</p>
							<p>
							All such sharing of information is done at your own risk. Please keep in mind
							that if you disclose personal information in your profile or when posting
							comments, messages, photos, tips or other items , this information may become
							publicly available.</p>
				</div>
				<div class="tip_list">
					<h1>4. Links</h1>
					<p>
					The Website may contain links to other websites. We are of course not
					responsible for the privacy practices of other web sites. We encourage our
					users to be aware when they leave our site to read the privacy statements of
					each and every web site that collects personally identifiable information. This
					Privacy Policy applies solely to information collected by The Website.</p>
			</div>
				<div class="tip_list">
					<h1>5. Third Party Advertising</h1>
					<p>
						Advertisements that appear on The Website are sometimes delivered (or
						"served") directly to users by third party advertisers. They automatically
						receive your IP address when this happens. These third party advertisers may
						also download cookies to your computer, or use other technologies such as
						JavaScript and "web beacons" (also known as "1x1 gifs") to measure the
						effectiveness of their ads and to personalize advertising content.
						Doing this allows the advertising network to recognize your computer each
						time they send you an advertisement in order to measure the effectiveness of
						their ads and to personalize advertising content. In this way, they may compile
						information about where individuals using your computer or browser saw
						their advertisements and determine which advertisements are clicked.
						The Website does not have access to or control of the cookies that may be
						placed by the third party advertisers. Third party advertisers have no access to
						your contact information stored on The Website unless you choose to share it
						with them.This privacy policy covers the use of cookies by The Website and
						does not cover the use of cookies or other tracking technologies by any of its
						advertisers.</p>
			</div>
				<div class="tip_list">
					<h1>6. Changing or Removing Information</h1>
					<p>
					Access and control over most personal information on The Website is readily
					available through the profile editing tools. The Website users may modify or
					delete any of their profile information at any time by logging into their
					account. Information will be updated immediately.
					Individuals who wish to deactivate their The Website account may do so
					by with a request to have their account deactivated. Removed information may
					persist in backup copies for a reasonable period of time but will not be
					generally available to members of The Website.
					Where you make use of the communication features of the service to share
					information with other individuals on The Website, however, (e.g., sending a
					personal message to another The Website user) you generally cannot remove
					such communications.</p>
			</div>
				<div class="tip_list">
					<h1>7. Security</h1>
					<p>
					The Website takes appropriate precautions to protect our users' information.
					Your account information is located on a secured server behind a firewall.</p>
			</div>
				<div class="tip_list">
					<h1>8. Terms of Use, Notices and Revisions</h1>
					<p>
					Your use of The Website, and any disputes arising from it, is subject to this
					Privacy Policy as well as our Terms of Use and all of its dispute resolution
					provisions including arbitration, limitation on damages and choice of law. We
					reserve the right to change our Privacy Policy and our Terms of Use at any
					time. Non-material changes and clarifications will take effect immediately, and
					material changes will take effect within 30 days of their posting on this site. If
					we make changes, we will post them and will indicate at the top of this page
					the policy's new effective date.</p>
					<p>If we make material changes to this policy, we will notify you here, by email, or
					through notice on our home page. We encourage you to refer to this policy on
					an ongoing basis so that you understand our current privacy policy. Unless
					stated otherwise, our current privacy policy applies to all information that we
					have about you and your account.</p>
			</div>
				<div class="tip_list">
					<h1>9. Contacting the Web Site</h1>
					<p>
					If you have any questions about this privacy policy, please .This Privacy Policy
					is designed to help you understand how we collect and use the personal
					information you decide to share, and help you make informed decisions when
					using the Website, located at and its directly associated domains (collectively,
					the "Website" and refered by the company in the context of "we" or "us")
					By using or accessing the Website, you are accepting the practices described in
					this Privacy Policy.</p>
			</div>
			</div>
			<div class="social_support" style="margin-top:150px;">
					Social sports Tipping EXCHANGE
				</div>
				<div class="social_support" style="font-family: 'tradegothicbold_condensed';margin-bottom:20px;">
					GET AHEAD OF THE GAME
				</div>

		</div>
	</div>
</div>
</body>
</html>
<script>
 $(".click_info").click(function(){
	var uri = $(this).attr('data-uri');
	if(uri =='buy') {
			$("#outer_buy_tip").fadeIn();
			$("#outer_place_tip").fadeOut();
			$("#click_place").removeClass("sel");
			$("#click_buy").addClass("sel");
	   }
		 if(uri =='place') 
			{
				$("#outer_buy_tip").fadeOut();
				$("#outer_place_tip").fadeIn();
				$("#click_place").addClass("sel");
				$("#click_buy").removeClass("sel");
			}
	});
</script>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/basketball.css">
<!-- ***************************TWITTER CONNECT************************* ---------->
<?php
			if($userData['twitter_token'])
			{
				$twitterdata = $this->session->userdata('twitterconnectsess'); 
				if(isset($twitterdata['statuses_count'])) 
				{
					$tweets =  $twitterdata['statuses_count'];
				}
				if(isset($twitterdata['followers_count']))
				{
					$followers =  $twitterdata['followers_count'];
				}
				if(isset($twitterdata['friends_count']))
				{
					$following =  $twitterdata['friends_count'];
				}
		     }
			else
			{
				$img_connect = 'blog_icon1.png';
			}
                 ?>
<div class="sport blue">
	<div class="container">
		<div class="main_outer">
			<center><a href="<?php echo base_url()?>select"><img src="<?php echo base_url()?>assets/images/coach_logo.png"/></a></center>	
		<div class="content">
				<div class="sort" style="padding:0"></div>
			<div class="stickyribbon">
			<div class="menu1" >Profile
			<a class="menu_icon collapsed"  data-target=".nav-collapse1" data-toggle="collapse"></a>
			</div>
			<ul class="left_links nav-collapse1 in collapse" id="left_link_ul" style="height:auto;position:static;overflow:hidden;">
				<li><span>sport</span></li>
				<?php if($categories) { 
					foreach($categories as $cat) {
						
					?>
				<li><a href="<?php echo base_url() ?>select"><?php echo $cat['name'];?></a></li>
			<?php } } else { ?>
				<li><span>No sport Found</span></li>
			<?php 	} ?>
			</ul>
			</div>
			<?php 
				if((strstr($userData['profile_image'],'http://')==$userData['profile_image'] || strstr($userData['profile_image'],'https://')==$userData['profile_image']) && $userData['profile_image']!='')
					{
						$image =  "<img src='".str_replace('_normal','',$userData['profile_image'])."' style='height:142px;width:140px;'/>";
					}
					else
					{
						if($userData['profile_image']!='' && file_exists('uploads/profile_picture/thumb_'.$userData['profile_image']))
						{
							$image =   "<img src='".base_url().'uploads/profile_picture/thumb_'.$userData['profile_image']."' style='height:142px;width:140px;'>";
						}
						else
						{
							$image =   "<img src='".base_url().'assets/images/default-no-profile-pics.jpg'."' style='height:142px;width:140px;'>";
						}
					}
					if($userData['facebook_id'])
					{
						$redirect = '<a href="http://www.facebook.com/profile.php?id='.$userData['facebook_id'].'" target="_blank">';	
					}
					else
					{
						$redirect = '<a href="#">';
					}
					if($userData['tw_screen_name'])
					{
						$redirect_twitter = '<a href="https://twitter.com/'.$userData['tw_screen_name'].'" target="_blank">';	
					}
					else
					{
						$redirect_twitter = '<a href="#">';
					}
				?>
			<div class="right_content" id="page_content">
			
				<div class="blog coach_blog">
					<div class="blog_img" style="width:137px;"><?php echo $image;?></div>
					<div class="blog_dis">
							<div class="blog_row username"><?php echo $userData['first_name'].' '.$userData['last_name'];?></div>
							<div class="blog_row"><div class="pull-left">Gender</div><div class="pull-right"><?php echo $userData['gender']; ?></div></div>
							<?php if($userData['twitter_token']) { ?>
							<div class="blog_row"><div class="pull-left">Twitter</div><div class="pull-right" style=" text-align: right; width: 80px;"><?php echo '<b>'.$tweets.'</b>'.' Tweet, <b>'.$followers.'</b> Followers <b>,'.$following.'</b>  Following'?></div></div>
							<?php } else { ?>
							<div class="blog_row"><div class="pull-left">Not Connected</div><div class="pull-right"><img src="<?php echo base_url()?>assets/images/<?php echo $img_connect;?>" id="connectTwitter" style="cursor:pointer;"></div></div>
							<?php } ?>
							
							<div class="blog_row"><div class="pull-left">connect</div>
							<div class="pull-right" style="padding-top:5px;">
								<?php echo $redirect_twitter; ?><img src="<?php echo base_url()?>assets/images/blog_icon1.png"/></a>
								<?php echo $redirect; ?><img src="<?php echo base_url()?>assets/images/blog_icon3.png"/></a>
								<a href="mailto:<?php echo $userData['email'];?>" >
								<img src="<?php echo base_url()?>assets/images/blog_icon4.png"/></a>
							</div>
							</div>
					</div>
					<div class="but_tip_coach">
						<div class="widthdraw">
						<div class="widthdraw_heading">Edit Profile <span style="float: right; cursor:pointer" id="delete_user" data-uri="<?php echo $userData['userid'];?>">delete my Account</span></div>
						 <form id="editProfilefrm" name="editProfilefrm" enctype="multipart/form-data" method="post" action="">
							<label>First Name</label>
							<input name="first_name" id="first_name" type="text" class="input_txt" placeholder="First Name" value="<?php echo $userData['first_name']; ?>">
							<label>LAST Name</label>						
							<input name="last_name" id="last_name" type="text" class="input_txt" placeholder="Last Name" value="<?php echo $userData['last_name']; ?>">
							<label>DOB</label>						
							<input name="dob" id="dob" type="text" class="input_txt" placeholder="dd-mm-yyyy" value="<?php echo date('d-m-Y',strtotime($userData['dob'])); ?>">
							<label>profile picture</label>
							<input type="file" class="input_txt" style="display:inline-block; padding:0px;" name="profilePic" id="profilePic"/>
							<label>Gender</label>
							<input type="radio" class="input_txt" name="gender" value="male" <?php if($userData['gender']=='male' || $userData['gender']=='') echo "checked";?>>
							<label class="option">Male</label>
							<input type="radio" class="input_txt" value="female" name="gender"<?php if($userData['gender']=='female') echo "checked";?>><label class="option">Female</label>
							<a href="#" id="saveProfile"  class="buy_tip pull-left" style="margin-left:123px;margin-top:5px;">Save Changes</a>
						</form>
						</div>
						
					</div>
					<?php 
					if(isset($success))
					{
						echo "<span style='float: left;margin-top: 14px;text-align: center;color:green'>".$success."</span>";
					}
					if(isset($error))
					{
						echo "<span style='float: left;margin-top: 14px;text-align: center;color:red'>".$success."</span>";
					}
            	?>
				
				</div>
						
				</div>
			</div>
		</div>
	</div>
</div>

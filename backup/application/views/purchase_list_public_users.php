<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/"<?php echo $style;?>>
<div class="rugby orange">
	<div class="container">
		<div class="main_outer">
			<center><a href="<?php echo base_url()?>select"><img src="<?php echo base_url() ?>assets/images/rugby_logo.png" /></a></center>		
			<div class="content">
			<div class="sort" ><span>sort:</span> 
			<a href="javascript:void(0)" data-uri="profit" class='sorting_content sort_sel' data-two="<?php echo $tips_detail['event_type_id'];?>">PROFIT</a>
			<a href="javascript:void(0)" data-uri="overall" class='sorting_content' data-two="<?php echo $tips_detail['event_type_id'];?>">STRIKE RATE</a>
			<a href="javascript:void(0)" data-uri="strikerate" data-two="<?php echo $tips_detail['event_type_id'];?>" class='sorting_content'>CORRECT TIPS</a> 
				<a href="javascript:void(0)" data-uri="best" data-two="<?php echo $tips_detail['event_type_id'];?>" class='sorting_content'>BEST OVER LAST 5</a>
				<a href="javascript:void(0)" data-uri="sport" data-two="<?php echo $tips_detail['event_type_id'];?>" class='sorting_content'>UPCOMING GAMES</a>
				<select id="time_range" class="time_sort" id="time_range">
						<option value="">Filter By Time</option>
						<option value="week">Last 7 days</option>
						<option value="month">Last Month</option>
						<option value="year">Last Year</option>
				</select>
				<select id="sort_by" class="time_sort"  style="display:none;">
						<option value="">Select Sort By:</option>
						<option value="strike_rate">Strike Rate</option>
						<option value="profit">Profit</option>
						<option value="correct">Correct Tips</option>
				</select>
				</div>
			<div class="stickyribbon">
			<div class="menu1" ><?php if($event_type['name']) { echo $event_type['name']; }else { echo "Menu";}?>
			<a class="menu_icon collapsed"  data-target=".nav-collapse1" data-toggle="collapse"></a>
			</div>
			<ul class="left_links nav-collapse1 in collapse" id="left_link_ul" style="height:auto;position:static;overflow:hidden;">
				<li><span>sport</span></li>
				<?php if($categories) { 
					foreach($categories as $cat) {
						
					?>
				<li><a href="<?php echo base_url() ?>tipsters/<?php echo $cat['slugUrl']; ?>"  <?php if($cat['event_type_id']==$tips_detail['event_type_id']){ ?> style="color:<?php echo $colortheme ?>";<?php } ?>><?php echo $cat['name'];?></a></li>
			<?php } } else { ?>
				<li><span>No sport Found</span></li>
			<?php 	} ?>
			</ul>
			</div>
			<div class="right_content sortingDiv">	
			<?php 
			foreach($result as $res) { 
					if($res['user_id'] == $tipsterId)
					{
						$image = $res['image'];
						$User_name = $res['firstname'].' '.$res['lastname'];
						$userStats = $this->betfair_model->getUserStatisticsForProfile($tips_detail['userid'],$tips_detail['event_type_id']);
						if(count($userStats))
						{
							$stRate = $userStats['strike_rate'];
							$ave_odds = round($userStats['ave_odds'],3);
							$rank = $userStats['user_rank'];
						}
						else
						{
							$stRate=0;
							$ave_odds=0;
							$rank = 0;
						}
					}
					if($userStats['facebook_id'])
						{
							$redirect = '<a href="http://www.facebook.com/profile.php?id='.$userStats['facebook_id'].'" target="_blank">';	
						}
						else
						{
							$redirect = '<a href="#">';
						}
						if($userStats['tw_screen_name'])
						{
							$redirect_twitter = '<a href="https://twitter.com/'.$userStats['tw_screen_name'].'" target="_blank">';	
						}
						else
						{
							$redirect_twitter = '<a href="#">';
						}
				}
			?>
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/basketball.css">
			<div class="right_content" id="page_content" style="padding-top:0px;">
				<div class="blog coach_blog" style="width:auto;display: block;">
					<div class="blog_img" style="width:137px;"><img src="<?php echo $image;?>" style="width:130px;height:144px;"></div>
					<div class="blog_dis">
							<div class="blog_row username"><?php echo $User_name;?></div>
							<div class="blog_row"><div class="pull-left">Average odds</div><div class="pull-right"><?php echo $ave_odds;?></div></div>
							<div class="blog_row"><div class="pull-left">Strike rate</div><div class="pull-right"><?php echo $stRate;?></div></div>
							<div class="blog_row"><div class="pull-left">tip price</div><div class="pull-right">$6.00</div></div>
							<div class="blog_row"><div class="pull-left">connect</div>
							<div class="pull-right" style="padding-top:5px;">
								<?php echo $redirect_twitter; ?><img src="<?php echo base_url()?>assets/images/blog_icon1.png"/></a>
								<?php echo $redirect; ?><img src="<?php echo base_url()?>assets/images/blog_icon3.png"/></a>
								<a href="mailto:<?php echo $userStats['email'];?>" >
								<img src="<?php echo base_url()?>assets/images/blog_icon4.png"/></a>
							</div></div>
					</div>
					<div class="but_tip_coach" id="buyTipSports">
						<?php 
						
						 if($tips_detail) { 
							 
							 $i = 1;
							$CheckPurchasedTips = $this->betfair_model->getAlreadyPurchasedTips($user_id,$tips_detail['userid'],$tips_detail['event_id'],$tips_detail['market_id'],$tips_detail['event_type_id']);
							$count_tip = count($CheckPurchasedTips);
							if($count_tip)
							{
								$txt = '<span>(Already Purchased)</span>';
							}
							else
							{
								$txt = '';
							}
							if($tips_detail['tip_type']=='free')
							{
								$tiptext = '<span style="color:green; font-size: 14px;">(free)</span>';
							}
							else
							{
								$tiptext = '<span style="color:red;font-size: 14px;">(paid)</span>';
							}
							$tips_price = $tips_detail['selected_odd'];
							if ($tips_price == 0){
									$tip_val = "Odds yet to be posted yet";
									$style= "margin-left:15px;float:right";
									$style1= "margin-left:21px;width:auto;vertical-align:top";
								}
								else if ($tips_price > 0 && $tips_price <= 1){
									$tip_val = "$0.50 - $1.00";
									$style= "margin-left:15px;float:right";
									$style1= "margin-left:21px;width:auto;vertical-align:top";
								}
								else if($tips_price >= 1.00 && $tips_price <= 1.50){
									$tip_val = "$1.00 - $1.50";	
									$style= "margin-left:15px;float:right";
									$style1= "margin-left:21px;width:auto;vertical-align:top";
								}
								else if($tips_price >= 1.50 && $tips_price < 2.00){
									$tip_val = "$1.50 - $2.00";	
									$style= "margin-left:15px;float:right";
									$style1= "margin-left:21px;width:auto;vertical-align:top";
								}
								else if($tips_price >= 2.00){
									$tip_val = "$2.00 - higher";
									$style= "margin-left:15px;float:right";
									$style1= "margin-left:21px;width:164px;vertical-align:top";	
								}

						$eventDate = $this->betfair_model->convert_time_zone($tips_detail['open_date'],$tips_detail['timezone'],$timeZonedata);
						$allLinks1 = date("l",strtotime($eventDate))." ".date("d/m/Y",strtotime($eventDate));
							?>
						
						<span style="width:100%;float:left; margin-top: 15px;"><label>Tip <?php echo $i;?></label><label style="<?php echo $style1;?>"><?php echo $tip_val .$tiptext;?></label><label style="<?php echo $style;?>"><a class="buy_tip" style="float:none" id="puchase_tips_sport" data-one="<?php echo $tips_detail['market_id'];?>" data-two="<?php echo $tips_detail['event_id'];?>" data-three="<?php echo $tips_detail['selected_odd'];?>" data-four="<?php echo $tips_detail['event_type_id'];?>" data-five="<?php echo $tips_detail['open_date'];?>" data-six="<?php echo $place_tip_id;?>" data-seven="<?php echo $tips_detail['tip_type'];?>">Buy Tip</a></label>
						<span>Game held on <?php echo $allLinks1;?><?php echo $txt;?></span>
						</span>
						
					<?php $i++;   } else
					{
						echo '<span>Tipester Not Placed any Tips</span>';
					}
					 ?>
					 <input type="hidden" value="<?php echo $user_id;?>" id="session_user_id">
					 <input type="hidden" value="<?php echo $tips_detail['userid'];?>" id="user_id">
					 <input type="hidden" value="<?php echo base_url();?>" id="url">
					 <input type="hidden" value="<?php echo $whisperTipFee;?>" id="whisperTipFee">
					 <input type="hidden" value="<?php echo $tips_detail['event_type_id'];?>" id="event_type_id">
					 <input type="hidden" value="" id="market_id_hiddn">
					 <input type="hidden" value="" id="event_id_hiddn">
					 <input type="hidden" value="" id="selected_odd_hiddn">
					 <input type="hidden" value="" id="event_type_select">
					 <input type="hidden" value="" id="open_date">
					 <input type="hidden" value="<?php echo $place_tip_id; ?>" id="placed_tip_id" name="placed_tip_id">	
				</div>
				<!------------------------------------------ POP UP Purchase ---------------------------------------->
<!--------------------------------------------------------------------------------------------->
					<div id="purchaseTipPopup" class="but_tip_coach"  style="display:none;">
						<p>This action will deduct AUD 6.00 from your wallet.<br/>
							Are you sure you want to purchase this tip?</p>
						<div id="loginDiv" style="float:left;width:100%">
							<a href="#" class="signin" id="purchaseTipBtnFinal"  style="float:left;background:<?php echo $colortheme;?>">Purchase</a>
							<a href="#" class="signin" id="closePurchasePop"  style="float:left;background:<?php echo $colortheme;?>">Cancel</a>
						</div>
						<span class="error_message"></span>
					</div>
					
					<!------------------------------------------  Wallet ---------------------------------------->
<!--------------------------------------------------------------------------------------------->
					<div id="walletErrorPopup" class="but_tip_coach"  style="display:none;">
						<p>You don't have sufficient funds to purchase this tip. If you want to add money in your wallet, click Add Money button?</p>
						<div id="loginDiv" style="float:left;">
							<form name="walletAddForm" id="walletAddForm" method="post" action="<?php echo base_url().'mywallet'; ?>">
							<input type="hidden" name="eventType" id="eventType" value="<?php echo $event_type['slugUrl'];?>">
							<input type="hidden" name="show_add_money" id="show_add_money" value="1">
							<a href="#" class="signin" id="closewalletError"  style="background:<?php echo $colortheme;?>">Cancel</a>
							<a href="#" class="signin" id="addMoneyBtnFinal"  style="background:<?php echo $colortheme;?>">Add Money
							</a>
							</form>
						</div>
					</div>
					
				</div>
				</div>
				<!------------------------------------------  Notify popup ---------------------------------------->
				</div>
		</div>
	</div>
	</div>

 <!--------------------------------------------------------------------------------------------->
<!------------------------------------------ POP UP Purchase ---------------------------------------->
<!--------------------------------------------------------------------------------------------->

<div id="purchaseTipPopup" class="popup_outer" style="display:none;z-index:5000;">

    	<div class="email_outer signup1" id="innerPurchaseTip" style="height:120px; margin-top:200px;text-align:center;width:480px">
        	<a href="#"><img src="<?php echo base_url(); ?>assets/images/cross.png" id="closePurchasePop" class="cross"></a>
            <h1>This action will deduct AUD 6.00 from your wallet.<br/>
            Are you sure you want to purchase this tip?</h1>
            <div id="loginDiv" style="float:left;">
				<a href="javascript:void(0)" class="signin" id="purchaseTipBtnFinal" style="margin-left:94px;">Purchase</a>
				<a href="javascript:void(0)" class="signin" id="closePurchasePop" style="margin-left:30px;">Cancel</a>
			</div>
			
        </div>

</div>
<!--------------------------------------------------------------------------------------------->
<!------------------------------------------ POP UP Purchase ---------------------------------------->
<!--------------------------------------------------------------------------------------------->
<!------------------------------------------ POP UP Wallet ---------------------------------------->
<!--------------------------------------------------------------------------------------------->
<div class="popup_mask" id="emailGetMask" style="display:none;opacity:.5;z-index:1500"> </div>
<div id="walletErrorPopup" class="popup_outer" style="display:none;z-index:5000;">

    	<div class="email_outer signup1" id="innerwalletError" style="height:120px; margin-top:200px;text-align:center;width:480px">
        	<a href="#"><img src="<?php echo base_url(); ?>assets/images/cross.png" id="closewalletError" class="cross"></a>
            <h1>You don't have sufficient funds to purchase this tip. If you want to add money in your wallet, click Add Money button?</h1>
            <div id="loginDiv" style="float:left;">
				<form name="walletAddForm" id="walletAddForm" method="post" action="<?php echo base_url().'mywallet'; ?>">
					<input type="hidden" name="show_add_money" id="show_add_money" value="1">
				</form>
				<a href="javascript:void(0)" class="signin" id="addMoneyBtnFinal" style="margin-left:94px;">Add Money</a>
				<a href="javascript:void(0)" class="signin" id="closewalletError" style="margin-left:30px;">Cancel</a>
			</div>
			
        </div>

</div>
<!--------------------------------------------------------------------------------------------->
<!------------------------------------------ POP UP Purchase ---------------------------------------->
<!--------------------------------------------------------------------------------------------->    
<div id="container" style="margin-top:70px;">
         	<div class="left_section">
            	<div class="user_detail">
                	<center>
						<?php 
							if((strstr($userData['profile_image'],'http://')==$userData['profile_image'] || strstr($userData['profile_image'],'https://')==$userData['profile_image']) && $userData['profile_image']!='')
							{
								echo "<img src='".str_replace('_normal','',$userData['profile_image'])."' height='125' width='118'/>";
							}
							else
							{
								if($userData['profile_image']!='' && file_exists('uploads/profile_picture/thumb_'.$userData['profile_image']))
								{
									echo "<img src='".base_url().'uploads/profile_picture/thumb_'.$userData['profile_image']."' />";
								}
								else
								{
									echo "<img src='".base_url().'assets/images/pro_img.png'."'>";
								}
							}
						?>
                	</center>
                    <h1><?php echo $userData['first_name'].' '.$userData['last_name']; ?></h1>
                    <span>@<?php echo $userData['username'];?><?php ?></span>
                    <p>-<?php echo $userData['email']; ?></p>
                    <div id="rankingsDiv" style="width:260px;margin:auto;display:;">
                    <div class="score" style="text-align:center"><span>Ranking:</span>
                    <?php
						$splRank = str_split($rank);
						$splLast = $splRank[count($splRank)-1];
						if($splLast==1)
						{
							$ext =  "st";
						}
						else if($splLast==2)
						{
							$ext =  "nd";
						}
						else if($splLast==3)
						{
							$ext =  "rd";
						}
						else
						{
							$ext =  "th";
						}
						echo $rank.$ext;
                    ?>
                    </div>
                    <div class="score" style="text-align:center"><span>Ave Odds:</span><?php echo $ave_odds; ?></div>
                    <div class="score" style="text-align:center"><span>Strike Rate:</span><?php echo $strikeRate; ?>%</div>
                    </div>
                    <?php
						if($userData['tw_screen_name']!='')
						{
                    ?>
                    <center>
						<a href="https://twitter.com/intent/follow?screen_name=<?php echo $userData['tw_screen_name']; ?>">
							<img src="<?php echo base_url().'assets/images/follow_me.png'; ?>" class="follow_me" style="cursor:pointer;">
						</a>
						<script src="https://platform.twitter.com/widgets.js"></script>
					</center>
                    <?php
						}
                    ?>
                </div>
               <?php
					if($userData['twitter_token'])
					{
						$twitterdata = $this->session->userdata('twitterconnectsess'); 
                ?>
					<div class="left_section_footer">
						<div class="follow">
							<span>
								<?php 
									if(isset($twitterdata['statuses_count']))
									echo $twitterdata['statuses_count'];
								?>
							</span>
							<p>TWEET</p>
						</div>
						<div class="follow">
							<span>
								<?php 
									if(isset($twitterdata['followers_count']))
									echo $twitterdata['followers_count'];
								?>
							</span>
							<p>FOLLOWERS</p>
						</div>
						<div class="follow">
							<span>
								<?php 
									if(isset($twitterdata['friends_count']))
									echo $twitterdata['friends_count'];
								?>
							</span>
							<p>FOLLOWING</p>
						</div>
					</div>
                 <?php 
					}
					
                 ?>
            </div>
            
           
            
            <div class="right_section" style="margin-bottom:15px;min-height:630px;height:auto;">
            	<div id="catcontainer" class="imag"></div>
                <div id="usergamedetails" class="detail_popup" style="position:absolute;z-index:1000;display:none;width:625px">
                	
                </div>
            </div>
            
             <div class="play" style="background:none;padding-left:12px;">Following <span>Ned’s</span> Tips You<br> Would Have Made <span>$10,000</span></div>
            
         </div>
<script type="text/x-handlebars-template" id="waterfall-tpl">
{{#result}}
    <div class="item">
		<a href="#" class="g_plus"></a>
        <a href="javascript:void(0)" id="evetnTypeDiv{{event_type_id}}" data-one="{{event_type_id}}" class="ranks"><img src="{{image}}" width="192" height="280" />
		<div class="img_detail">
		<b>{{name}}</b>
		{{description}}
		</div>
    </div>
{{/result}}
</script>
<script src="<?php echo base_url() ?>assets/js/libs/jquery/jquery.js"></script>
<script src="<?php echo base_url() ?>assets/js/libs/handlebars/handlebars.js"></script>
<script src="<?php echo base_url() ?>assets/js/waterfall.js"></script>
<script>
	var www_root = "<?php echo base_url(); ?>";
	var userId = "<?php  echo $userId; ?>";
	var whisperTipFee = "<?php echo $this->config->item('whispertipfee'); ?>";
	var event_type_id="<?php echo $event_type_id; ?>";
	var ii=0;
$('#catcontainer').waterfall({
    itemCls: 'item',
    colWidth: 222,  
    gutterWidth:0,
    gutterHeight: 15,
    isFadeIn: true,
    checkImagesLoaded: false,
    path: function(page) {
        //return '<?php echo base_url() ?>assets/data/data1.json';
        return '<?php echo base_url() ?>/getUserPlacedTip/'+userId;
        //return '<?php echo base_url() ?>/getUserPlacedTip getCategories';
    },
    callbacks:{
		loadingFinished: function() {
					if(ii==0)
						{
							$('#waterfall-loading').hide();
							jQuery('#evetnTypeDiv1').trigger('click');
							ii++;
						}
              
					}
			}
});
jQuery(document).on('click', '.ranks', function () {
		var left = $(this).parent().css("left");
		var top = $(this).parent().css("top");
		if(top == 'auto')
		{
			top = 0;
		}
			top = parseInt(top)+280;
			top = top+"px";
		var width = $(document).width();
		var event_type_id = $(this).attr('data-one');
		if(parseInt(left)+526>parseInt(width))
		{
			var popLift = parseInt(left);
			var left = parseInt(left)+526-parseInt(width);
				left = popLift-left-60;
				
			jQuery('#usergamedetails').css("left",left);
			jQuery('#usergamedetails').css("top",top);
		}
		else
		{
			jQuery('#usergamedetails').css("left",left);
			jQuery('#usergamedetails').css("top",top);
		}
		
		//var url1 = www_root+'tips_results/'+event_type_id+'/'+userId;
		var url1 = www_root+'purchasetip_data/'+event_type_id+'/'+userId;
			jQuery.ajax({
						url: url1,
						type: 'get',
						success: function (data) {
						
							$('#usergamedetails').html(data);
							$('#usergamedetails').show();
							$("#scrollLeague").mCustomScrollbar({
								scrollButtons:{
									enable:true
								}
							});
						},
						error: function (httpRequest, textStatus, errorThrown) {

							alert(url1+"status=" + textStatus + ",error=" + errorThrown);
						}
					});
		
	});
	var sel_event_id =0;
	var sel_market_id =0;
	var sel_user_id =0;
	var sel_competition_id =0;
jQuery(document).on('click', '.pur_bor', function () {
		$('#messagePurchase').hide();
		$('.pur_bor').removeClass('pur_bor_selected');
		$('#purchaseTipBtn').show();
		$(this).addClass('pur_bor_selected');
		sel_event_id = $(this).attr('data-one');
		sel_market_id = $(this).attr('data-two');
		sel_user_id = $(this).attr('data-three');
		sel_competition_id = $(this).attr('data-four');
		
	});
jQuery(document).on('click', '#purchaseTipBtn', function () {
	var url1 = base_url + 'home/check_balance/';
	
	jQuery.ajax({
                    url: url1,
                    type: 'get',
                    datatype:'json',
                    success: function (data) {
						var res = jQuery.parseJSON(data);
						var bal = parseFloat(res.balance);
						if(bal>=whisperTipFee)
						{
							$('#purchaseTipPopup').fadeIn('slow');    
							$('#emailGetMask').fadeIn('slow');
						}
						else
						{
							$('#walletErrorPopup').fadeIn('slow');    
							$('#emailGetMask').fadeIn('slow');
						}
                    },
                    error: function (httpRequest, textStatus, errorThrown) {

                        alert(url1+"status=" + textStatus + ",error=" + errorThrown);
                    }
                });
	
	
});
jQuery(document).on('click', '#purchaseTipBtnFinal', function () {
		
		var url1 = base_url + 'home/save_purchase_tip/';
		jQuery.ajax({
				url: url1,
				type: 'POST',
				data:{competitionId:sel_competition_id,tipsterId:sel_user_id},
				dataType: 'JSON',
				async:false,
				success: function (data) {
					
					if (data == 'error') {							
						$('#messagePurchase').css('border','2px solid #CC0000');
						$('#messagePurchase').css('color','#CC0000');
						$('#messagePurchase').text('You have already purchased this tip from this tipster.');
					}else {
						$('#messagePurchase').css('border','2px solid #01FF01');
						$('#messagePurchase').css('color','#01FF01');
						$('#messagePurchase').text('You have successfully purchased this tip.');
						$('#messagePurchase').append('<a href="'+www_root+'mypurchasedtip">Click here</a> to view this purchased tip.');
						}
				},
				error: function (httpRequest, textStatus, errorThrown) {

					alert(url1+"status=" + textStatus + ",error=" + errorThrown);
				}
			});
		
		$('#purchaseTipBtn').hide();
		$('#emailGetMask').hide();
		$('#purchaseTipPopup').hide();
		$('#messagePurchase').show();
	});
jQuery(document).on('click', '#addMoneyBtnFinal', function () {
		$('#walletAddForm').submit();
	});
	
</script>

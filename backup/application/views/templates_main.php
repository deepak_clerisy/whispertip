<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <title>WhisperTip</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
         <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/football_title_new.ico" type="image/x-icon" />
	<link href="<?php echo base_url();?>assets/css/front/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
	<script src="<?php echo base_url() ?>assets/js/html5.js" type="text/javascript"></script>
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/front/style.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/front/bootstrap.css">
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/popModal.css">
        <script src="<?php echo base_url() ?>assets/js/jquery-1.7.1.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/front/login_popup.css">
        <script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery.autocomplete.js"></script>
        <script src="<?php echo base_url() ?>assets/js/miscellaneous.js"></script>
 <script>
	var base_url = '<?php echo  base_url();?>';
</script>
<script>
       	window.twttr = (function(d, s, id) {
            var t, js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "https://platform.twitter.com/widgets.js";
            fjs.parentNode.insertBefore(js, fjs);
            return window.twttr || (t = {
                _e: [],
                ready: function(f) {
                    t._e.push(f)
                }
            })
        }(document, "script", "twitter-wjs"));
	</script>
	<script>
	  !function(g,s,q,r,d){r=g[r]=g[r]||function(){(r.q=r.q||[]).push(
	  arguments)};d=s.createElement(q);q=s.getElementsByTagName(q)[0];
	  d.src='//d1l6p2sc9645hc.cloudfront.net/tracker.js';q.parentNode.
	  insertBefore(d,q)}(window,document,'script','_gs');

	  _gs('GSN-972951-J');
	</script>
	<script type="text/javascript">
		function selectItem(li) {
		 var id = li.extra[0];
		  window.location.href='<?php echo base_url()?>Events_markets/'+id;
			//show_airport_seat(id,'<?php echo base_url();?>');
	  }
	   function formatItem(row) {
    	return row[0];
		}
		$( document ).ready(function() {
		$("#search_comp").autocomplete(
					"<?php echo base_url();?>",
					{
						delay:10,
						minChars:2,
						matchSubset:1,
						matchContains:1,
						cacheLength:10,			
						onItemSelect:selectItem,								
						formatItem:formatItem,
						autoFill:true
					}
			);
		    var stickyRibbonTop = $('.stickyribbon').offset().top;
		    var logo = $('center').offset().top;
			var logoDiv = $('center').height();
			var divMain = logo +  logoDiv;
		    $(window).scroll(function(){
		            if( $(window).scrollTop() >= divMain ) {
		                    $('.stickyribbon').css({position: 'fixed', top: "57px"});
		            } else {
		                    $('.stickyribbon').css({position: 'static', top: '0px'});
		            }
		    });
		    $(window).scroll(function(){
			var stickyDiv1 = $('.sort').offset().top;
			var logo = $('center').offset().top;
			var logoDiv = $('center').height();
			var divMain = logo +  logoDiv;
			
		 	       if( $(window).scrollTop() >= divMain ) {
		                    $('.sort').css({position: 'fixed', top: "37px"});
		            } else {
		                    $('.sort').css({position: 'absolute', top: '0px'});
		            }
		    });
	$(".loadPurchaser").live("click",function(){	   
		var url = $('#url').val();
		var eventType = $('#eventType').val();
		var user_id = $(this).attr('data-uri');
		var status  = 0;
		var flag = 1;
	var url1 = url+'home/purchase_list';
		jQuery.ajax({
				url: url1,
				type: 'POST',
				data:{'user_id':user_id,'eventType':eventType,'status':status,'flag':flag},
				success: function (data) {
					if(data) {
						$('.right_content').html(data);
					}
				},
			
			});	
		});

		});
		</script>
<style>
	#loading_img {
		background: none repeat scroll 0 0 #E5E5E5;
		border: 4px solid #FFFFFF;
		border-radius: 7px 7px 7px 7px;
		box-shadow: 0 0 5px #555555;
		padding: 10px;
		text-align: center;
		width: 200px;
	}
	.spin-loading {
		-webkit-animation: spin 1.5s infinite linear;
		-moz-animation: spin 1.5s infinite linear;
		-o-animation: spin 1.5s infinite linear;
		-ms-animation: spin 1.5s infinite linear;
	}
		@-webkit-keyframes spin {
		0% { -webkit-transform: rotate(0deg);}
		100% { -webkit-transform: rotate(360deg);}
	}
		@-moz-keyframes spin {
		0% { -moz-transform: rotate(0deg);}
		100% { -moz-transform: rotate(360deg);}
	}
		@-o-keyframes spin {
		0% { -o-transform: rotate(0deg);}
		100% { -o-transform: rotate(360deg);}
	}
		@-ms-keyframes spin {
		0% { -ms-transform: rotate(0deg);}
		100% { -ms-transform: rotate(360deg);}
	}

</style>
</head>
<body>
	<div class="popup_container">
    <div class="popup_outer">
    	<div class="popup_inner">
        	<div class="title_bg">
            	<a href="#"><img src="<?php echo base_url()?>assets/images/cross.png" class="close_popup" style="float: right;" /></a>            
            </div>
            <div class="msg-content">You have already placed tip on this game</div>
            <div class="twitter_content" style="font-size: 13px;margin-left: 0;margin-top: 8px;">
            Share your tips on Twitter! Build a following and prove you are the most knowledgable sports fan on earth!
            </div>
             <div class="purchase_tip_notify" style="font-size: 13px;margin-left: 0;margin-top: 8px;">
			
            </div>
           <div class="twitter_content1" style=" display: inline-block;margin-left: 8px;margin-top: 7px;">
            <a href="javascript:void(0);" target="_blank" class='twitter_share' style="float:left;position:relative"><img src="<?php echo base_url()?>assets/images/s2.png" style="cursor:pointer;"></a>
            <?php /* 
            $desc = "This is my first PHP tweet";
				print '<iframe allowtransparency="true" frameborder="0" scrolling="no" src="https://platform.twitter.com/widgets/tweet_button.html?count=none&via=WhisperTip&url=http%3A%2F%2Fwww%2Ewhispertip%2Ecom" style="width:130px; height:20px;"></iframe>';
*/
				?>
            </div>
            <div class="twitter_content1" style=" display: inline-block;margin-left: 8px;margin-top:13px;">
			<img src="<?php echo base_url()?>assets/images/s1.png" style="cursor:pointer;" onclick="share('<?php echo $userId;?>')">
            </div>
        </div>
    </div>
</div>
	<div class="popup_container_placed">
    <div class="popup_outer_placed">
    	<div class="popup_inner_placed">
        	<div class="title_bg_placed">
            	<a href="#"><img src="<?php echo base_url()?>assets/images/cross.png" class="close_popup_placed" style="float: right;" /></a>            
            </div>
            <div class="msg-content_placed"></div>
            <div class="twitter_content_placed" style="display: block;font-size: 20px;margin-bottom: 25px;margin-left: 0;margin-top: -13px;">
             Please select tip price
             </div>
             <div class="purchase_tip_notify_placed" style="font-size: 13px;margin-left: 0;margin-top: 8px;">
             <select name="radio_placed" id="radio_placed_tip" style="color: #000000;font-size: 14px;font-weight: bold;width: 112px;">
				<option value="free" selected="selected">Free</option>
				<option value="sell">$6</option>
             </select>
		    </div>
            <span><a href="javascript:void(0)" style="position: relative; margin-top: 14px;" class="tip_btn place_bet">Place a Tip</a></span>
            <input type="hidden" value="" id="selectionDiv">
        </div>
    </div>
</div>
	<?php 
	
	$ch = curl_init();
			$geturl = "http://api.espn.com/v1/sports/news/headlines/top/?apikey=qg5ct3w7js335y7f2zwhkq5e";			
			curl_setopt($ch, CURLOPT_URL, $geturl);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		    $userdata = curl_exec($ch);
			curl_close($ch);
			$userJson = json_decode($userdata);
			foreach($userJson->headlines as $headline)
			{
				$Heading[] =  $headline->headline;
				$url[] = $headline->links->web->href;
				
			}
			$finalArr = array_combine($Heading, $url);
	
	if($type_tip =='place') {
			$style_purchase = "font-weight:normal";
			$style_place = "font-weight:bold";
		} 
		else if($type_tip =='purchase')
		{
			$style_purchase = "font-weight:bold";
			$style_place = "font-weight:normal";
		}
		else
		{
			$style_purchase = "font-weight:normal";
			$style_place = "font-weight:normal";
		}
		?>
	<?php if($place_val!='loginPage') { ?>
<header>
	<?php 
	if($login_status==1){
			if((strstr($userData['profile_image'],'http://')==$userData['profile_image'] || strstr($userData['profile_image'],'https://')==$userData['profile_image']) && $userData['profile_image']!='')
				{
					echo "<img src='".str_replace('_normal','',$userData['profile_image'])."' style='height:21px;width:30px';/>";
				}
				else
				{
					if($userData['profile_image']!='' && file_exists('uploads/profile_picture/thumb_'.$userData['profile_image']))
					{
						echo "<img src='".base_url().'uploads/profile_picture/thumb_'.$userData['profile_image']."' style='height:21px;width:30px'/>";
					}
					else
					{
						echo "<img src='".base_url().'assets/images/default-no-profile-pics.jpg'."' style='height:21px;width:30px'>";
					}
				}
			?>
	<span class="btn-navbar user_name" data-target=".nav-collapse" data-toggle="collapse"><?php echo $userData['first_name'].' '.$userData['last_name'];?></span>
	<?php }else{ ?>
		<span><a href="javascript:void(0)" class="button registration" style="color:#777;">Login</a> | <a href="javascript:void(0)" class="button registration"  style="color:#777;">Signup</a></span>
		<?php }?>
	<span style="font-weight:normal;">I WANT TO <span style="<?php echo $style_purchase; ?>">
	<a href="<?php echo base_url()?>select/buy" style="color: #777777">BUY</a></span>/<span style="<?php echo $style_place; ?>"><a href="<?php echo base_url()?>select/place" style="color: #777777" >PLACE</a></span>TIPS</span>
	<input class="search" type="text" placeholder="Search By Event Name" id="search_comp" style="text-transform:none;text-decoration:none; color: #444444;"/>
		
	<div class="media_link">
		<a href="<?php echo base_url()?>overall_tipster" style="color: #777777;margin-right:17px;" >Overall Tipsters</a>
		<a href="<?php echo base_url()?>terms_conditions" style="color: #777777;margin-right:17px;" >Terms and conditions</a>
		<a href="<?php echo base_url()?>privacy_policy" style="color: #777777;margin-right:17px;" >Privacy policy</a>
		<a href="mailto:support@whispertip.com" style="color: #777777;margin-right:17px;" >Contact us</a>
		<a href="<?php echo base_url()?>how_it_work" style="color: #777777;margin-right:17px;" >HOw it works</a>
		<a href="http://twitter.com/whispertip" target="_blank"><img src="<?php echo base_url() ?>assets/images/icon_tweet.png"/></a>
		<a href="https://facebook.com/whispertip" target="_blank"><img src="<?php echo base_url() ?>assets/images/icon_facebook.png"/></a>
		<a href="http://instagram.com/whispertip" target="_blank"><img src="<?php echo base_url() ?>assets/images/icon_instagram.png"/></a>
		
	</div>
</header>
<?php }?>
<nav class="nav-collapse collapse" style="position:fixed;z-index:2;top:43px;">
	<ul class="nav red" style="background:<?php echo $colortheme;?>">
		<li><a href="<?php echo base_url()?>user_profile">My Profile</a></li>
		<li><a href="<?php echo base_url()?>profile">Edit Profile</a></li>
		<li><a href="<?php echo base_url()?>mywallet">My Wallet</a></li>
		<li><a href="<?php echo base_url()?>myplacedtip">My Placed Tip</a></li>
		<li><a href="<?php echo base_url()?>mypurchasedtip">My Purchased Tip</a></li>
		<li><a href="<?php echo base_url()?>logout">Logout</a></li>
	</ul>
</nav>
<input type="hidden" value="<?php echo base_url()?>" id="url">
  <?php echo $page_body; ?>
<div id="loading_img" style="display:none";>
			<img class="spin-loading" style= "margin-top: 10px;" border="0" src="<?php echo base_url()?>assets/images/football_spin.png" />
			<p style="margin-top: 10px;">LOADING PLEASE WAIT...</p>
		</div>

<footer>

<marquee behavior="scroll" scrollamount="4" direction="left" onmouseover="this.stop();" onmouseout="this.start();">
	<?php 
	foreach($finalArr as $key=>$val) {
		echo "<a href='".$val."' target='_blank' class='newsfeed'>".$key." | </a>   ";
		
		} ?>
	</marquee>
</footer>
<script src="<?php echo base_url(); ?>assets/js/popModal.js"></script>
<script>
  var user_id = '<?php echo $userId;?>';
  $(".twitter_share").attr("href", "http://twitter.com/intent/tweet?url=http%3A%2F%2Fwww%2Ewhispertip%2Ecom%2Fuser_public_profile%2F"+user_id+"&text=Whispertip&via=WhisperTip");
</script>
<div class="popup_container_login"  id="popup_container_login" style="display:none">
		<div class="popup_outer_login">
			<a href="javascript:void(0)" class="popup_img_login" style="left:5px;"></a>
			<a href="javascript:void(0)" class="popup_img_login" style="right:15px; top:10px;">
				<img src="<?php echo base_url();?>assets/images_new/cross.png" class="close_popup_login" /></a>
			<h1>Login Form</h1>
			
			<div class="reg_option">
				<?php if($auth == 'authenticated')
					{
						$url =  base_url()."home/login_popup/auth"; 
					} 
					else
					{
						$url = base_url()."home/login_popup";
					}
						 ?>
				<script>
				$(document).ready(function(){
					$('#iframe').load('<?php echo $url;?>');
					})
				</script>
				<div src="<?php echo $url;?>" id="iframe" frameborder="0" ></div>
			</div>
	</div></div>
<?php  
if($login_status==1 && $weekly_tipster['status']==1){
			if((strstr($tipster['profile_image'],'http://')==$tipster['profile_image'] || strstr($tipster['profile_image'],'https://')==$tipster['profile_image']) && $tipster['profile_image']!='')
					{
						$image =  "<img src='".str_replace('_normal','',$tipster['profile_image'])."' style='height:142px;width:140px;'/>";
					}
					else
					{
						if($tipster['profile_image']!='' && file_exists('uploads/profile_picture/thumb_'.$tipster['profile_image']))
						{
							$image =   "<img src='".base_url().'uploads/profile_picture/thumb_'.$tipster['profile_image']."' style='height:142px;width:140px;'/>";
						}
						else
						{
						$image =   "<img src='".base_url().'assets/images/default-no-profile-pics.jpg'."' style='height:142px;width:140px;'>";
						}
					}
	 ?>
	<div class="popup_container_login tipster_popup"  id="popup_container_tipster">
		<div class="popup_outer_login tipster_popup_outer">
			<a href="javascript:void(0)" class="popup_img_login" style="left:5px;"></a>
			<a href="javascript:void(0)" class="popup_img_login" style="right:15px; top:10px;">
				<img src="<?php echo base_url();?>assets/images_new/cross.png" class="close_popup_login" /></a>
			<h1>Tipster of the Week</h1>
			
			<div style="margin-bottom:17px; width:100%;">
				<div class="blog_row username" style="padding:12px;color:#333;border-bottom:none"><?php echo $tipster['first_name'].' '.$tipster['last_name']?></div>
					<div style="width:137px;" class="blog_img">
						<?php echo $image;?>
						<!--<a data-uri="8" class="view_week_tipster_profile">View profile</a>-->
						</div>
					<div style="width: 383px;display:inline-block" class="blog_dis_poopup">
							<?php if($tipster['gender']){ ?>
							<div class="blog_row"  style="padding:10px 6px"><div class="pull-left">Gender</div><div class="pull-right"><?php echo ucwords($tipster['gender']); ?></div></div>
							<?php }?>
							<div class="blog_row" style="padding:7px 6px"><div class="pull-left">Tips placed last week</div>
								<div class="pull-right">
									<div class="twitter_content1">
										<?php echo $tipster['total_tips'];?>
									</div>
								</div>
							</div>
							<div class="blog_row" style="padding:7px 6px"><div class="pull-left" >Tips Won last week</div>
								<div class="pull-right">
									<div class="twitter_content1">
										<?php echo $weekly_tipster['correct_tips'];?>
									</div>
								</div>
							</div>
							<div class="blog_row"  style="padding:7px 6px"><div class="pull-left" >Strike Rate last week</div>
								<div class="pull-right">
									<div class="twitter_content1">
										<?php echo $tipster['strike_rate'];?>
									</div>
								</div>
							</div>
							<?php if($tipster['twitter_token']) { ?>
							<div class="blog_row"  style="padding:7px 6px"><div class="pull-left">Twitter</div><div class="pull-right" style=" text-align: right; width: 157px;line-height: 20px;"><?php echo '<b>'.$statuses_count.'</b>'.' Tweets <b></br>'.$followers_count.'</b> Followers <b></br>'.$friends_count.'</b>  Following'?></div></div>
							<?php } else { ?>
							<?php } ?>

						<?php if($twitter_user_id) {?>
							<div class="blog_row" style="padding:10px 6px">
								<div class="pull-left" >
									<a href="javascript:void(0);" onclick="twitter_share11(<?php echo $twitter_user_id;?>)" class='twitter_share1' style="float:left;position:relative">
									<img src="<?php echo base_url()?>assets/images/follow.png" style="cursor:pointer;" title="Follow"></a>
								</div>
							</div>
							<?php } ?>							
					</div>
				</div>
	</div>


<?php 	} ?>
</body>
<script type="text/javascript">
function twitter_share11(user_id) {
	window.open("https://twitter.com/intent/follow?user_id="+user_id,'_blank','width=810,height=570');
}
</script>
</html>


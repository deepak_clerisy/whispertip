<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/basketball.css">
<style>
	.ulclass {
	 color: #777777;
    line-height: 23px;
    margin-left: 31px;
}
p{
	line-height:23px;
	}
	</style>
<div class="sport">
	<div class="container">
		<div class="sports_outer">
			<center>
			<a href="<?php echo base_url()?>select"><img src="<?php echo base_url();?>assets/images/sports_logo.png"/></a></center>
			<div class="sell_buy">
			TERMS AND CONDITIONS
			</div>
		<div class="outer_tips" id="outer_place_tip">
			<div class="tips_outer" style="width:641px;">
				<div class="tip_list">
					<p>Access to, and use of this website is subject to these terms and conditions. By
					accessing, browsing or otherwise using the site you signify your acceptance of
					these terms. If at any time you do not agree to these terms you should
					discontinue your use of this website.</p>
					<h1>1. Privacy</h1>
					<p>Your privacy is very important to us. We designed our to make
					important disclosures to you about how we collect and use the
					information you post on The Website. We encourage you to read the
					Privacy Policy, and to use the information it contains to help make
					informed decisions.</p>
				</div>
			</div>
		<div class="tip_list">
					<h1>2. Site Information and Content</h1>
					<p><ul class="ulclass"><li>a. While The Website takes all care in the preparation of
					information we accept no responsibility nor warrant the accuracy
					of the information displayed.</li>
					<li>b. Information on The Website has been provided for entertainment
					purposes only and The Website accepts no responsibility for any
					assumptions based or derived from any information displayed or
					assumed.</li>
					<li> c. WARNING: GAMBLING INVOKES RISK. For the avoidance of doubt,
					we are not responsible for your gambling losses. Any monetary
					investment based on information displayed on the site (or
					affiliated sites) is at the risk of the investor.</li>
					<li>d. All information relating to race fields, including horse numbers,
					scratchings, odds and statistics should be checked with an official
					source.</li>
					</p>
				</div>
				<div class="tip_list">
					<h1>3. Sharing Your Content and Information</h1>
					<p>You own all of the content and information you post on The Website. In
					order for us to use certain types of content and provide you with The
					Website, you agree to the following:
					<ul class="ulclass"><li>a. For content that is covered by intellectual property rights, like
					photos ("IP content"), you specifically give us the following
					permission, subject to your : you grant us a non-exclusive,
					transferable, sub-licensable, royalty-free, worldwide license to
					use any IP content that you post on or in connection with The
					Website ("IP License"). This IP License ends when you delete
					your IP content or your account (except to the extent your
					content has been shared with others, and they have not deleted
					it).</li>
					<li>b. When you delete IP content, it is deleted in a manner similar to
					emptying the recycle bin on a computer. However, you
					understand that removed content may persist in backup or
					cached copies for a reasonable period of time.</li>
					<li>c. We always appreciate your feedback or other suggestions about
					The Website, but you understand that we may use them without
					any obligation to compensate you for them (just as you have no
					obligation to offer them).<li></p>
				</div>	
				<div class="tip_list">
					<h1>4. Safety</h1>
					<p>We do our best to keep The Website safe, but we cannot guarantee it.
					We need your help in order to do that, which includes the following
					commitments:
					<ul class="ulclass"><li>a. You will not send or otherwise post unauthorized commercial
					communications to users (such as spam).</li>
				<li>	b. You will not collect users' information, or otherwise access The
					Website, using automated means (such as harvesting bots, robots,
					spiders, or scrapers) without our permission.</li>
				<li>	c. You will not upload viruses or other malicious code.</li>
				<li>	d. You will not solicit login information or access an account
					belonging to someone else.</li>
				<li>	e. You will not bully, intimidate, or harass any user.</li>
				<li>	f. You will not post content that is hateful, threatening,
					pornographic, or that contains nudity or graphic or gratuitous
					violence.</li>
					<li>g. You will not use The Website to do anything unlawful,
					misleading, malicious, or discriminatory.</li>
					You will not facilitate or encourage any violations of this Statement.</p>
				</div>	
				<div class="tip_list">
					<h1>5. Registration and Account Security</h1>
					<p>The Website users provide their real names and information, and we
					need your help to keep it that way. Here are some commitments you
					make to us relating to registering and maintaining the security of your
					account:
					<ul class="ulclass"><li>a. You will not provide any false personal information on The
					Website, or create an account for anyone other than yourself
					without permission.
					<li>b. You will not use The Website if you are under 18.</li>
					<li>c. You will keep your contact information accurate and up-to-date.</li>
				<li>	d. You will not share your password, let anyone else access your
					account, or do anything else that might jeopardize the security of
					your account.</li>
				<li>	e. You will not transfer your account to anyone without first
					getting our written permission.</li>
				<li>	f. You will not have more than one active account without first
					getting our written permission.</li></p>
				</div>	
				<div class="tip_list">
					<h1>6. Protecting Other People's Rights</h1>
					<p>We respect other people's rights, and expect you to do the same.
					<ul class="ulclass"><li>a. You will not post content or take any action on The Website that
					infringes someone else's rights or otherwise violates the law. can
					remove any content you post on The Website if we believe that it
					violates this Statement.</li>
					<li>b. If we removed your content for infringing someone else's
					copyright, and you believe we removed it by mistake please
					contact us.</li>
					<li>c. If you repeatedly infringe other people's intellectual property
					rights, we will disable your account when appropriate.</li>
					<li>d. You will not use our copyrights or trademarks without our
					written permission.</li>
					<li>e. You will not post anyone's identification documents or sensitive
					financial information on The Website.</li></p>
				</div>	
				
				<div class="tip_list">
					<h1>8. You agree to our tipping terms</h1>
					<p><ul class="ulclass"><li>Access to, including buying and selling tips on The Website are subject to the
					following Terms and Conditions. By buying, selling or accessing tipping
					information you signify your acceptance of these terms. Any sale commission
					rates advertised are at a percentage which applied after GST has been excluded
					from a sale.</li></p>
					1. Selling Tips
					<ul class="ulclass"><li> a.You agree to supply horse racing tips and tipping comments to
					the best of your knowledge.</li>
					<li> b. The Website reserves the right to suspend your account if your
					tips are deemed to be similar to the tips that have been</li>
					purchased from other users or copied from another source.</li>
					<li> c. After tips "lock out" (which is typically 20 minutes before the
					first race of the day) under no circumstances can the information
					be modified.</li>
					<li> d. The user is aware that a percentage of each sale made will be paid
					to The Website, currently 16.66% for individual sales and
					subscriptions.</li></ul></p>
					2. Buying and Accessing Tips
					<ul class="ulclass"><li> a. You must be over 18 years of age to purchase tips. You should
					comply with the law in your country concerning the age at which
					a person is allowed to gamble.</li>
					<li>b. Tips may be offered seven days a week. There may, however, be
					some days when no tips can be advised.</li>
					<li>c. The user is aware that a percentage of their purchases, currently
					83.34% for individual purchases and subscriptions will be paid to
					the seller.</li>
					<li>d. By following the advice of tips you realise that there is risk
					involved and it is possible that your stake money may be lost.
					You accept full responsibility for the outcome of your own bets
					and understand that the recommendations are for information
					and entertainment only.</li>
					<li>e. The service is intended for personal use only and is not
					transferable. No part of the service may be reproduced, displayed
					in public, broadcast or used for any other purpose.</li>
					<li>f. The user is aware that a percentage of their sales made will be
					paid to The Website, currently 16.66% for individual sales and
					subscriptions.</li>
					<li>g. The user is aware that when a purchase is made their username
					may be made available to view by the seller. No other personal
					information will be shared with the seller.</li>

				</div>	
				<div class="tip_list">
					<h1>9. You agree to our payment terms</h1>
					<p><ul class="ulclass"><li>When you make payments through The Website, you agree to these Payments
					Terms.</li></p>
					1. Making Purchases
					<ul class="ulclass"><li>a. When you confirm a transaction on The Website, you agree to be
					bound by and pay for that transaction.</li>
					<li>b. Pay attention to the details of the transaction, because your total
					price may include taxes, fees, and shipping costs, which you are
					responsible for paying.</li>
					<li>c. Don't commit to a transaction unless you are ready to pay,
					because all sales are final.</li>
				<li>	d. When you make a "purchase" the sale may include commission
					paid to another user.</li>
				<li>	e. If you order something that becomes unavailable before it can be
					provided to you, your only remedy is to receive a refund of your
					purchase price.</li>
					<li>f. When you "purchase" tipping information we commit to making
					the information accessible to you by means of the PURCHASED
					TIPS page and via. email. Our obligation ends when we deliver the
					information to you.</li>
					<li>g. Even though we use terms like "purchase," "buy," "sell," to talk
					about transactions related to tips, we don't transfer an ownership
					of this information. For example, the tips we make available to
					you are licensed to you, not sold.</li>
				<li>	h. Information sold to you is licensed to you for personal use only.
					Under no circumstances can you forward, re-sell or disclose
					details of your sale to another person or for commercial purposes
					without the written consent of The Website.</li>
					<li>i. WE MAKE NO WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED,
					WITH RESPECT TO ANY PRODUCTS OR SERVICES SOLD ON OR
					THROUGH .</li></ul>
					2. Payment Sources
					<p>We want to make payments convenient, so we allow you to make
					payments using a number of different payment sources, like credit
					cards, direct debits, bank transfers and PayPal.</p>
					<ul class="ulclass"><li>a. When you provide a payment source to us, you confirm that you
					are permitted to use that payment source. You also authorize us
					to collect and store it, along with other related transaction
					information. We will not store enough information which would
					allow us to action a transaction on your behalf.</li>
					<li>b. When you make a payment, you authorize us (and our designated
					payment processor) to charge the full amount to the payment
					source you designate for the transaction.</li>
				<li>	c. If you pay by credit or debit card we may obtain a pre-approval
					from the issuer of the card for an amount up to the amount of the
					purchase. We will bill your card at the time of purchase or
					shortly thereafter. If you cancel a transaction before completion,
					that pre-approval may result in your funds not otherwise being
					immediately available.</li>
				<li>	d. If you pay by debit card and your payment results in an overdraft
					or other fee from your bank, you alone are responsible for that
					fee.</li>
				</ul>
				3. Delivery of your purchase
				<p>Products purchased from The Website are delivered in an electronic
				form and adhere to delivery to ensure you receive the information in a
				timely manner.</p>
				<ul class="ulclass"><li>a. After ordering online, you will receive access to the information
				via the 'My Purchased Tips' page, accessible from the main menu.
				You may also receive an email containing this information
				however we cannot guarantee the delivery of this email.</li>
				<li>b. In some cases their may be delays between payment processing
				and the information made available to you from the 'My
				Purchased Tips' page. We will attempt to provide this
				information to you within 10 minutes, otherwise, you are
				entitled to a full refund.</li></ul>
				4. Purchasing and Using Credit on your account
				<p>You may purchase credits from The Website to purchase tips, Insider
				Selections or to send Blackbook SMS notifications.</p>
				<ul class="ulclass"><li>a. When you purchase or receive credits, you do not own the
				credits. Rather, you receive a limited right to use such credits in
				connection with certain features on The Website, such as the
				purchase of tips.</li>
				<li>b. You will not transfer credit to anyone, or transfer them to anyone
				outside of The Website.</li>
				<li>c. We may change the purchase price for credits at any time as well
				as the ways that you can use or transfer credits. We also reserve
				the right to stop issuing credits.</li>
				<li>d. If you leave a balance of credits unused for three years, we may
				redeem those credits.</li>
				<li>e. If you deactivate your account and do not reactivate it within 6
				months, or if you delete your account, you will lose any
				accumulated credit.</li>
				<li>f. If we deactivate your account and you do not meet any conditions
				necessary to reinstate it within 6 months, we may redeem those
				credits.</li>
				</ul>
				5. Actions We May Take
				<p>As part of our effort to keep The Website safe, we may take certain
				actions to reduce liability for users and us.</p>
				<ul class="ulclass"><li>a. We may make any inquiries that we consider necessary, either
				directly or through third parties, concerning your identity and
				creditworthiness.</li>
				<li>b. We may cancel any transaction if we believe the transaction
				violates these Payments Terms or the Statement of , or we
				believe doing so may prevent financial loss.</li>
				<li>c. We may place a delay on a payment for a period of time, limit
				payment sources for a transaction, limit your ability to make a
				payment or deactivate your account if we believe doing so may
				prevent financial loss.</li>
				<li>d. We may contact your payment source issuer, law enforcement, or
				impacted third parties (including other users) and share details
				of any payments you are associated with if we believe doing so
				may prevent financial loss or a violation of law.</li>
				<li>e. We may cancel any credits transferred, assigned, or sold in
				violation of these Payments Terms or the Statement of .</li>
			<li>	f. If you purchase advertising, and your payment method fails or
				your account is past due, we may take steps to collect past due
				amounts using other collection mechanisms. You agree to pay all
				expenses associated with such collection, including reasonable
				attorneys' fees. Interest will accrue on any past due amounts at
				the rate of the lesser of 1% per month or the lawful maximum.</li>
				</ul>
				6. Disputes, Reversals and Refunds
				<ul class="ulclass"><li>a. If you believe that an unauthorized or otherwise problematic
				transaction has taken place under your account, you agree to
				notify us immediately, so that we may take action to prevent
				financial loss.</li>
				<li>b. To the fullest extent permitted by law, you waive all claims
				against us related to payments unless you submit the claim to us
				within 30 days after the charge.</li>
				<li>c. You are responsible for and agree to reimburse us for all
				reversals, charge-backs, claims, fees, fines, penalties and other
				liability incurred by us (including costs and related expenses)
				that were caused by or arising out of payments that you
				authorized or accepted.</li>
				<li>d. If you enter into a transaction with a third party and have a
				dispute over the goods or services you purchased we have no
				liability for such goods or services. Our only involvement with
				regard to such transaction is as a payment agent.</li>
			<li>	e. We may intervene in disputes between users concerning
				payments but have no obligation to do so.
				f. Your only remedy for a technical failure or interruption of
				service is to request that your transaction be completed at a later
				time.</li>
				<li>g. Refunds are not offered on the basis that the tips purchased are
				offered to the user immediately after payment has been
				completed. This indicates that the transaction has been
				completed and information has been used by the purchaser.</li>
			<li>	h. The Website reserves the right to offer refunds to users under
				abidance with the law and special circumstances evaluated by the
				administrators at The Website. Payments may be refunded by
				Pay Pal or credit to your The Website account and the balance of
				the sellers account will be adjusted accordingly. If you wish to
				request a refund, please .</li>
				</ul>
				7. Special Provisions Applicable to Advertisers
				<ul class="ulclass"><li>a. When you purchase advertising on or through The Website, you
				agree to pay all amounts specified in the order, along with any
				applicable taxes.</li>
				<li>b. You are responsible for maintaining the security of your
				advertising account, and you understand that you will be charged
				for any orders placed on or through your advertising account.</li>
				<li>c. You can cancel an advertising order at any time but your ads may
				run for several days after you notify us, and you are still
				responsible for paying for those ads.</li>
			<li>	d. Without limiting section 4.1, your order constitutes your written
				authorization for us to obtain your personal and/or business
				credit report from a credit bureau. We may obtain your credit
				report when you place an order, or at any time thereafter.</li>
			<li>	e. It is your responsibility to remit any taxes that apply to your
				transactions. You agree to indemnify and hold us harmless from
				and against any claim arising out of your failure to do so.</li>
				</ul>
				8. Notices and Amendments to These Payments Terms
					<ul class="ulclass"><li>a. We may provide notices to you by posting them on our website,
					or by sending them to an email address or street address that
					you previously provided to us. Website and email notices shall be
					considered received by you within 24 hours of the time posted
					or sent; notices by mail shall be considered received within three
					business days of the time sent.</li>
					<li>b. Except as otherwise stated, you must send notices to us relating
					to payments and these Payment Terms by postal mail to:
					Attention: Legal Department.</li>
				<li>	c. The Payment Terms in place at the time you confirm a transaction
					will govern that transaction.</li>
				<li>	d. We may change these Payment Terms, in whole or in part, even
					though the changes may affect accumulated credits. All
					amendments to these Payment Terms will be made pursuant to
					Section of the .</li>
					</ul>
					9. Other
				<ul class="ulclass"><li>a. All of the commitments you make in the The Website Statement
				of Rights and Responsibilities apply to payments through The
				Website. In the event of any conflict between these Payments
				Terms and the The Website Statement of Rights and
				Responsibilities, the Payments Terms shall prevail.</li>
				<li>b. "Us," "we," "our," "claim" and "The Website" mean the same as
				they do in the Statement of Rights and Responsibilities.</li>
				<li>c. Some countries may restrict or prohibit your ability to make
				payments through The Website. Nothing in these Payments
				Terms should be read to override or circumvent any such foreign
				laws.</li></ul>
				</div>	
				<div class="tip_list">
					<h1>10. You agree to our comments terms</h1>
					<p><ul class="ulclass"><li>We reserve the right to remove any posts it deems to be inappropriate. Anyone
					in breach of these rules may risk having their account suspended.</li>
					1. General Contributions "Comments" including forum posts, wall posts,
					news comments, form guide comments, tipping comments, private
					comments, status updates and comments made to any page throughout
					the website must adhere to follow these rules. Users agree not to post
					any item that:
					<ul class="ulclass"><li>a. Reveals tips that you are selling or have purchased prior to the
					event. You are free to discuss the chances of a feature race
					outcome providing it is informative and interesting for the rest
					of the community to read.</li>
					<li>b. Is defamatory or discloses any information that the customer has
					no legal right to disclose.</li>
					<li>c. Is abusive, hateful, racist, bigoted, sexist, harassing, threatening,
					inflammatory, defamatory, knowingly false, vulgar, obscene,
					sexually-oriented, profane or is otherwise in violation of any
					applicable law, rule or regulation.</li>
					<li>d. Is any form of advertising (including affiliate programs),
					promotional material or any other form of commercial activity.</li>
					<li>e. Is intentionally false or misleading statements or any statement
					seeking to unfairly manipulate a market.</li>
					<li>f. Attempts to publish, collect or store data relating to customers
					including personal details such as email addresses, phone
					numbers and residential addresses.</li>
					<li>g. Contains a link to any other website address of, or promotes the
					services of, a competitor of The Website.</li>
					<li>h. Is in a language other than English.</li>
					<li>i. Is protected by any form of intellectual property whether
					registered or unregistered or any contractual, statutory and
					equitable obligations of confidence.</li>
					<li>j. Is or may amount to collusion or activities of a suspicious or
					criminal nature.</li>
					<li>k. Is a post which is related to or leads to the transmission of any
					software or computer files that contain a virus, worm or other
					disruptive or harmful component.</li></ul>
					2. Forum Contributions
					If you post Comments on the forum you must adhere to the following
					additional rules. Users agree not to post any item that:
					<ul class="ulclass"><li>a. Is a site complaint or enquiry or query needing a response by
					The Website. Please to discuss these matters.</li>
					<li>b. Is in relation to a private tipping competition or club.</li>
					<li>c. Is not relevant to the forum category or discussion.</li>

				</div>	
				<div class="tip_list">
					<h1>11. About Advertisements on The Website</h1>
					<p>
					Our goal is to deliver ads that are not only valuable to advertisers, but
					also valuable to you. In order to do that, you agree to the following:
					<ul class="ulclass"><li>a. We do not give your content to advertisers.</li>
					<li>b. You understand that we may not always identify paid services
					and communications as such.</li>
					</p>
				</div>	
				<div class="tip_list">
					<h1>12. Special Provisions Applicable to Advertisers</h1>
					<p>
					You can target your specific audience by buying ads on The Website or
					our publisher network. The following additional terms apply to you if
					advertise with us:
					If you are placing ads on someone else's behalf, we need to make sure
					you have permission to place those ads, including the following:
					<ul class="ulclass"><li>a. When you advertise with us, you will tell us the type of
					advertising you want to buy, the amount you want to spend. If we
					accept your advertisement, we will deliver your ads as inventory
					becomes available.</li>
				<li>	b. You will pay in accordance with our . The amount you owe will be
					calculated based on your advertising requirements.</li>
				<li>	c. We will determine the size, placement, and positioning of your
					ads.</li>
				<li>	d. We do not guarantee the activity that your ads will receive, such
					as the number of clicks you will get.</li>
				<li>	e. We cannot control how people interact with your ads, and are not
					responsible for click fraud or other improper actions that affect
					the cost of running ads.</li>
				<li>	f. You will not offer any contest or sweepstakes ("promotion")
					without our prior written consent. If we consent, you take full
					responsibility for the promotion and all applicable laws.</li>
				<li>	g. You can cancel your advertisement at any time by contacting us
					but it may take us seven days before the ad stops running.</li>
				<li>	h. Our license to run your ad will end when we have completed
					your rder. You understand, however, that if users have interacted
					with your ads, your ads may remain until the users delete it.</li>
				<li>	i. We can use your ads and related information for marketing or
					promotional purposes.</li>
			<li>		j. You will not issue any press release or make public statements
					about your relationship with The Website without written
					permission.</li>
				<li>	k. We may reject or remove any ad for any reason.</li>
			<li>		l. You warrant that you have the legal authority to bind the
					advertiser to this Statement.</li>
			<li>		m. You agree that if the advertiser you represent violates this
					Statement, we may hold you responsible for that violation.</li>

					</p>
				</div>	
				<div class="tip_list">
					<h1>13. Amendments</h1>
					<p>
					From time to time we may amend the terms. By continuing to use the
					site after such amendments have been made you are agreeing to be
					bound by the terms as amended. If at any time you do not agree to these
					terms you should discontinue your use of the website
					</p>
				</div>	
				<div class="tip_list">
					<h1>14. Termination</h1>
					<p>
					If you violate the letter or spirit of this Statement, or otherwise create
					possible legal exposure for us, we can stop providing all or part of The
					Website to you. We will generally try to notify you, but have no
					obligation to do so. You may also delete your account or disable your
					application at any time by . In all such cases, this Statement will still
					apply.
					</p>
				</div>	
				<div class="tip_list">
					<h1>15. Disputes</h1>
					<p>
					<ul class="ulclass"><li>a. You will resolve any claim, cause of action or dispute ("claim")
					you have with us arising out of or relating to this Statement or
					The Website in a state or federal court located in Australia. The
					laws of the Australia will govern this Statement, as well as any
					claim that might arise between you and us, without regard to
					conflict of law provisions. You agree to submit to the personal
					jurisdiction of the courts located in Australia for the purpose of
					litigating all such claims.</li>
					<li>b. If anyone brings a claim against us related to your actions or your
					content on The Website, you will indemnify and hold us harmless
					from and against all damages, losses, and expenses of any kind
					(including reasonable legal fees and costs) related to such claim.</li>
					<li>c. WE TRY TO KEEP UP, BUG-FREE, AND SAFE, BUT YOU USE IT AT
					YOUR OWN RISK. WE ARE PROVIDING "AS IS" WITHOUT ANY
					EXPRESS OR IMPLIED WARRANTIES INCLUDING, BUT NOT
					LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY,
					FITNESS FOR A PARTICULAR PURPOSE, AND
					NON-INFRINGEMENT. WE DO NOT GUARANTEE THAT WILL BE
					SAFE OR SECURE. IS NOT RESPONSIBLE FOR THE ACTIONS OR
					CONTENT OF THIRD PARTIES, AND YOU RELEASE US, OUR
					DIRECTORS, OFFICERS, EMPLOYEES, AND AGENTS FROM ANY
					CLAIMS AND DAMAGES, KNOWN AND UNKNOWN, ARISING OUT
					OF OR IN ANY WAY CONNECTED WITH ANY CLAIM YOU HAVE
					AGAINST ANY SUCH THIRD PARTIES. WE WILL NOT BE LIABLE TO
					YOU FOR ANY LOST PROFITS OR OTHER CONSEQUENTIAL,
					SPECIAL, INDIRECT, OR INCIDENTAL DAMAGES ARISING OUT OF
					OR IN CONNECTION WITH THIS STATEMENT OR , EVEN IF WE
					HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
					OUR AGGREGATE LIABILITY ARISING OUT OF THIS STATEMENT
					OR WILL NOT EXCEED THE GREATER OF ONE HUNDRED
					DOLLARS ($100) OR THE AMOUNT YOU HAVE PAID US IN THE
					PAST TWELVE MONTHS. APPLICABLE LAW MAY NOT ALLOW THE
					LIMITATION OR EXCLUSION OF LIABILITY OR INCIDENTAL OR
					CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR
					EXCLUSION MAY NOT APPLY TO YOU. IN SUCH CASES, 'S
					LIABILITY WILL BE LIMITED TO THE FULLEST EXTENT
					PERMITTED BY APPLICABLE LAW.</li>
					</p>
				</div>	
				<div class="tip_list">
					<h1>16. Definitions</h1>
					<p>
				<ul class="ulclass"><li>a. By "The Website" we mean the features and services we make
				available, including through (a) our website at and any other The
				Website branded or co-branded websites (including
				sub-domains, international versions, and mobile versions); (b)
				our Platform; and (c) other media, devices or networks now
				existing or later developed.</li>
				<li>b. By "us," "we" and "our" we mean Pty Ltd. and/or its affiliates.</li>
				<li>c. By "Platform" we mean a set of APIs and services that enable
				applications, developers, operators or services to retrieve data
				from The Website and provide data to us relating to The Website
				users.</li>
				<li>d. By "content" we mean the content, information and tipping you
				post on The Website, including information about you and the
				actions you take.</li>
				<li>e. By "post" we mean post on The Website or otherwise make
				available to us.</li>
			<li>	f. By "use" we mean use, copy, publicly perform or display,
				distribute, modify, translate, and create derivative works of.</li>
					</p>
				</div>	
				<div class="tip_list">
					<h1>17. Other</h1>
					<p>
					<ul class="ulclass"><li>a. This Statement makes up the entire agreement between the
					parties regarding The Website, and supersedes any prior
					agreements.</li>
					<li>b. If any portion of this Statement is found to be unenforceable, the
					remaining portion will remain in full force and effect.</li>
					<li>c. If we fail to enforce any of this Statement, it will not be
					considered a waiver.</li>
				<li>	d. Any amendment to or waiver of this Statement must be made in
					writing and signed by us.</li>
				<li>	e. You will not transfer any of your rights or obligations under this
					Statement to anyone else without our consent.</li>
				<li>	f. All of our rights and obligations under this Statement are freely
					assignable by us in connection with a merger, acquisition, or sale
					of assets, or by operation of law or otherwise.</li>
				<li>	g. Nothing in this Agreement shall prevent us from complying with
					the law.</li>
				<li>	h. This Statement does not confer any third party beneficiary
					rights.</li>
					</p>
				</div>	
			<div class="social_support" style="margin-top:150px;">
					Social sports Tipping EXCHANGE
				</div>
				<div class="social_support" style="font-family: 'tradegothicbold_condensed';margin-bottom:20px;">
					GET AHEAD OF THE GAME
				</div>

		</div>
	</div>
</div>
</body>
</html>
<script>
 $(".click_info").click(function(){
	var uri = $(this).attr('data-uri');
	if(uri =='buy') {
			$("#outer_buy_tip").fadeIn();
			$("#outer_place_tip").fadeOut();
			$("#click_place").removeClass("sel");
			$("#click_buy").addClass("sel");
	   }
		 if(uri =='place') 
			{
				$("#outer_buy_tip").fadeOut();
				$("#outer_place_tip").fadeIn();
				$("#click_place").addClass("sel");
				$("#click_buy").removeClass("sel");
			}
	});
</script>

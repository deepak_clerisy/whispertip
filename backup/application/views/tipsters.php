<div id="container" style="margin-top:70px;min-height:700px;"></div>
<div id="profilePopup" class="detail_popup pro_rating_outer" style='position:absolute;z-index:100;display:none;'>
</div>
<div id="usergamedetails"  style="position:absolute;z-index:1000;display:none;">
</div>
<form id="purchaseTipForm" name="purchaseTipForm" method="post" action="<?php echo base_url()."purchase_tip_tipster"; ?>">
<input type="hidden" name="hidEventTypeId" id="hidEventTypeId">
<input type="hidden" name="hidUserId" id="hidUserId">
</form>
<script type="text/x-handlebars-template" id="waterfall-tpl">
{{#result}}
    <div class="item" style="height:280px;">
		<div class="cap"><span>{{rank}}</span></div>
		<a href="#" class="g_plus"></a>
		<a href="javascript:void(0)" onclick="openPurchase(this);" data-one="<?php echo $event_type_id; ?>" data-id="{{user_id}}" class="tips">{{total_placed_tips}} Tips</a>
        <img src="{{image}}" width="192" height="" style="margin-top:{{image_margin}}"/>
		<div class="img_detail">
		<b>{{firstname}} {{lastname}}</b>
		</div>
    </div>
{{/result}}
</script>
<script src="<?php echo base_url() ?>assets/js/libs/jquery/jquery.js"></script>
<script src="<?php echo base_url() ?>assets/js/libs/handlebars/handlebars.js"></script>
<script src="<?php echo base_url() ?>assets/js/waterfall.js"></script>
<script>
	var sess_user_id = "<?php echo $this->session->userdata('userid'); ?>";
	var whisperTipFee = "<?php echo $this->config->item('whispertipfee'); ?>";
	var event_type="<?php echo $event_type; ?>";
	var event_type_id="<?php echo $event_type_id; ?>";
	var user_id=0;
$('#container').waterfall({
    itemCls: 'item',
    colWidth: 222,  
    gutterWidth:0,
    gutterHeight: 15,
    isFadeIn: true,
    checkImagesLoaded: false,
    path: function(page) {
        //return '<?php echo base_url() ?>assets/data/data1.json';
        return '<?php echo base_url() ?>/getTipsters/'+event_type;
    }
});
</script>

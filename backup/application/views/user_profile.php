<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/front/basketball.css">

<!-- ***************************TWITTER CONNECT************************* ---------->
<?php
			if($userData['twitter_token'])
			{
				$twitterdata = $this->session->userdata('twitterconnectsess'); 
				if(isset($twitterdata['statuses_count'])) 
				{
					$tweets =  $twitterdata['statuses_count'];
				}
				if(isset($twitterdata['followers_count']))
				{
					$followers =  $twitterdata['followers_count'];
				}
				if(isset($twitterdata['friends_count']))
				{
					$following =  $twitterdata['friends_count'];
				}
		     }
			else
			{
				$img_connect = 'blog_icon1.png';
			}
			
			if($userData['facebook_id'])
					{
						$redirect = '<a href="http://www.facebook.com/profile.php?id='.$userData['facebook_id'].'" target="_blank">';	
					}
					else
					{
						$redirect = '<a href="#">';
					}
					if($userData['tw_screen_name'])
					{
						$redirect_twitter = '<a href="https://twitter.com/'.$userData['tw_screen_name'].'" target="_blank">';	
					}
					else
					{
						$redirect_twitter = '<a href="#">';
					}
                 ?>
<div class="sport blue">
	<div class="container">
		<div class="main_outer">
			<center><a href="<?php echo base_url()?>select"><img src="<?php echo base_url()?>assets/images/coach_logo.png"/></a></center>		
		<div class="content">
			<div class="sort"></div>
				<div class="stickyribbon">
			<div class="menu1" >Profile
			<a class="menu_icon collapsed"  data-target=".nav-collapse1" data-toggle="collapse"></a>
			</div>
			<ul class="left_links nav-collapse1 in collapse" id="left_link_ul" style="height:auto;position:static;overflow:hidden;">
				<li><span>sport</span></li>
				<?php if($categories) { 
					foreach($categories as $cat) {
						
					?>
				<li><a href="<?php echo base_url() ?>select" ><?php echo $cat['name'];?></a></li>
			<?php } } else { ?>
				<li><span>No sport Found</span></li>
			<?php 	} ?>
			</ul>
			</div>
			<?php 
			if((strstr($userData['profile_image'],'http://')==$userData['profile_image'] || strstr($userData['profile_image'],'https://')==$userData['profile_image']) && $userData['profile_image']!='')
					{
						$image =  "<img src='".str_replace('_normal','',$userData['profile_image'])."' style='height:142px;width:140px;'/>";
					}
					else
					{
						if($userData['profile_image']!='' && file_exists('uploads/profile_picture/thumb_'.$userData['profile_image']))
						{
							$image =   "<img src='".base_url().'uploads/profile_picture/thumb_'.$userData['profile_image']."' style='height:142px;width:140px;'/>";
						}
						else
						{
						$image =   "<img src='".base_url().'assets/images/default-no-profile-pics.jpg'."' style='height:142px;width:140px;'>";

						}
					}
					?>
			<div class="right_content" id="page_content">
			
				<div class="blog coach_blog" style="margin-bottom:17px;">
					<div class="blog_img" style="width:137px;"><?php echo $image;?></div>
					<div class="blog_dis">
							<div class="blog_row username"><?php echo $userData['first_name'].' '.$userData['last_name'];?></div>
							<div class="blog_row"><div class="pull-left">Gender</div><div class="pull-right"><?php echo $userData['gender']; ?></div></div>
							<?php if($userData['twitter_token']) { ?>
							<div class="blog_row"><div class="pull-left">Twitter</div><div class="pull-right" style=" text-align: right; width: 80px;"><?php echo '<b>'.$tweets.'</b>'.' Tweet, <b>'.$followers.'</b> Followers <b>,'.$following.'</b>  Following'?></div></div>
							<?php } else { ?>
							<div class="blog_row"><div class="pull-left">Not Connected</div><div class="pull-right"><img src="<?php echo base_url()?>assets/images/<?php echo $img_connect;?>" id="connectTwitter" style="cursor:pointer;"></div></div>
							<?php } ?>
							
							<div class="blog_row"><div class="pull-left">connect</div>
							<div class="pull-right" style="padding-top:5px;">
								<?php echo $redirect_twitter; ?><img src="<?php echo base_url()?>assets/images/blog_icon1.png"/></a>
								<?php echo $redirect; ?><img src="<?php echo base_url()?>assets/images/blog_icon3.png"/></a>
								<a href="mailto:<?php echo $userData['email'];?>" >
								<img src="<?php echo base_url()?>assets/images/blog_icon4.png"/></a>
							</div>
							</div>
					</div>
				</div>
				
				<div class="widthdraw wiresult">
						<div class="widthdraw_heading">Results <a class="buy_tip" id="backbuttn_profile" style="cursor: pointer;float: right;font-size: 15px;height: 20px;margin-right: 75px;right: 0;width: 53px;z-index: 1;display:none" onclick="window.location.href='<?php echo base_url()?>user_profile'">Back</div>
						<ul class="row sports_list" id="buy_sports">
						<?php 
						if($categories) {
						foreach($categories as $cat) { ?>
							<li class="col-lg-4 col-md-4 col-sm-6 col-xs-12"><a href="javascript:void(0)" class="userresult" data-one="<?php echo $cat['event_type_id'];?>" data-two="<?php if($diff_user) {echo $userId;} else {echo $this->session->userdata('userid');}  ?>"><?php echo $cat['name'];?></a>
						<?php } } else { echo "No Categories Found";} ?>
					</ul>
						
						

				</div>
						
				</div>
			</div>
		</div>
	</div>
</div>

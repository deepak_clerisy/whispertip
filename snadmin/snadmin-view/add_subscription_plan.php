<?php //echo '<pre>';print_r($single_plan);
	$id=$plan_name=$plan_duration=$thr_price=$cl_price=$pl_comments='';
	if(($page_title=='Edit Subscription Plan'))
	{
		$id=$single_plan['id'];
		$plan_name=$single_plan['name'];
		$plan_duration=$single_plan['duration'];
		$thr_price=$single_plan['therapist_price'];
		$cl_price=$single_plan['client_price'];
		$pl_comments=$single_plan['comments'];
	}

?>
<style>
	
	.textarea-main{
			border: 2px solid #eeeeee;
			padding: 5px 10px;
		}
		.input-block span {
			  float: right;
			  position: relative;
			  width: 60%;
			}
		.btn_rt{
			margin-right:-72px;
			margin-top: 105px;
			}	
	</style>


	<div class="account-right-div">
		<div class="dashboard-heading"><h2><?php echo $page_title; ?></h2></div>
		<?php echo $snbreadcrum; ?>
		<div class="dashboard-inner">
			<div class="main-dash-summry Edit-profile">
				<form method="post" name ="frmAddPlan" id="frmAddPlan" enctype="multipart/form-data"> 
				<div class="input-row">
					<div class="full">
						<div class="input-block">
							<label>Plan Name :</label>
							<span class='reg_span'>
								<input type="text" id='pname' name="pname" value="<?php echo $plan_name; ?>" class="inputbox-main"/>
							</span>
						</div>
					</div>
				</div>
				<div class="input-row pull-right">
					<div class="full">
						<div class="input-block">
							<label>Duration :</label>
							<span class='reg_span'>
								<input type="text" id='pduration' class="inputbox-main" type="text" name="pduration" value="<?php echo $plan_duration; ?>"/>
							</span>
						</div>
					</div>
				</div>
				<div class="input-row">
					<div class="full">
						<div class="input-block">
							<label>Therapist Price :</label>
							<span class='reg_span'>
								<input type="text" id='th_price' class="inputbox-main" type="text" name="th_price" value="<?php echo $thr_price; ?>">
							</span>
						</div>
					</div>
				</div>
				<div class="input-row pull-right">
					<div class="full">
						<div class="input-block">
							<label>Client Price :</label>
							<span class='reg_span'>
								<input type="text" id='c_price' class="inputbox-main" type="text" name="c_price" value="<?php echo $cl_price; ?>">
							</span>
						</div>
					</div>
				</div>				
				<div class="input-row ">
					<div class="full">
						<div class="input-block">
							<label>Comments :</label>
							<span class='reg_span'>
								<textarea rows='5' cols='33' id='pcomments' class="textarea-main" name="pcomments"><?php echo $pl_comments; ?></textarea>
							</span>
						</div>
					</div>
				</div>
				<div class="input-row">
					<div class="full">
						<div class="input-block">
							<label> &nbsp; </label>
							<span class=' reg_span_btn btn_rt'>
								<input type="submit" value="Submit" class="btn-submit btn"> 
								<input type="button" value="Cancel" onclick="cancelButton();" class="btn-submit btn"> 
							</span>
						</div>
					</div>	
				</div>
				</form>
			</div>
		</div>
	</div>

	
<script type="text/javascript">
	
	  $(function() {	 
		$("#frmAddPlan").validate({
					rules: {
						pname: { 
							required:true,
							remote:{
								url: "<?php echo base_url();?>snadmin/subscription/check_plan",
								type: "post",
								data: {
									pname_val: function() {
										return $( "#pname" ).val();
									}
								}
							}
						},
						pduration: "required",
						th_price: "required",
						c_price: "required",
						pcomments: "required"
					},
					messages: {
						pname: {
								required: "Please enter subscription plan name.",
								remote: "Subscription plan already exists, Enter diffrent."
						},
						pduration: "Please enter plan duration.",
						th_price: "Please enter therapist price.",
						c_price: "Please enter client price.",
						pcomments: "Please enter comments."
					},
				
			 errorElement:"div",
			errorClass:"valid-error",
			submitHandler: function(form) {
				form.submit();
			}
		});
	});
	
	function cancelButton()
	  {
		location.assign("<?php echo base_url(); ?>snadmin/subscription/manage");
	  } 
	 </script>

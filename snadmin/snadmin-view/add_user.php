<?php //echo '<pre>';print_r($user_list);
$lang=$id=$fname=$lname=$email=$role=$active=$username=$username_type=$email_type=$role_type=$selectedTherapist=$profile_image=$selectedTherapist=$selectedTherapistImage=$location=$subscription=$datausedforresearch=$emailfornewmsg=$emailfornewactivity=$newupdateemail=$timezone_select=$title='';
if(($page_title=='Edit User'))
{

$id=$user_list[0]->id;
$title=$user_list[0]->title;
$fname=$user_list[0]->firstname;
$lname=$user_list[0]->lastname;
$email=$user_list[0]->email;
$role=$user_list[0]->account_type;
$active=$user_list[0]->status;
$username=$user_list[0]->username;
$lang=$user_list[0]->language;
$subscription=$user_list[0]->subscription_id;
$timezone_select=$user_list[0]->timezone;
$location=$user_list[0]->location;
if($role=='client')
{
$selectedTherapist=$user_list[0]->therapist_id;

$selectedTherapistImage=$user_list[0]->profile_pic;
}
else
{
 $profile_image=$user_list[0]->profile_pic;
}

if($user_list[0]->agree_research=="on")
{
  $datausedforresearch="checked";
}
if($user_list[0]->news_update=="on")
{
  $newupdateemail="checked";
}
if($user_list[0]->new_activity=="on")
{
  $emailfornewactivity="checked";
}
if($user_list[0]->new_message=="on")
{
  $emailfornewmsg="checked";
}
$username_type=$email_type="readonly";
//$role_type="disabled";
}


?>

<form method="post" id="add_user_form" enctype="multipart/form-data"> 
<input type="hidden" id="hidden_image_interpreter" name="hidden_image_interpreter" value="">
<div class="account-right-div">
					
		<div class="dashboard-heading"><h2><?php echo $page_title; ?></h2></div>
		<?php echo $snbreadcrum; ?>
		<div class="dashboard-inner">
			<div class="main-dash-summry Edit-profile">
			  <form name='add_hospital_form' id='add_hospital_form' method='post'>
				<input type="hidden" name="hospitalid" value="<?php echo $id; ?>" id="hospitalid">
				<div class="input-row">
					<div class="full">
						<div class="input-block">
							<label>Title :</label>
							<span class='reg_span'>
								<select id='title' class="inputbox-main input_select"  name="title">
								<option value="">Select Title</option>
								<option value="mr" <?php if($title=='mr') { echo 'selected'; } ?>>Mr</option>
								<option value="mrs" <?php if($title=='mrs') { echo 'selected'; } ?>>Mrs</option>
								<option value="miss" <?php if($title=='miss') { echo 'selected'; } ?>>Miss</option>
								<option value="ms" <?php if($title=='ms') { echo 'selected'; } ?>>Ms</option>
								<option value="dr" <?php if($title=='dr') { echo 'selected'; } ?>>Dr</option>
								<option value="professor" <?php if($title=='professor') { echo 'selected'; } ?>>Professor</option>
								<option value="other" <?php if($title=='other') { echo 'selected'; } ?>>Other</option>
								</select></span>
						</div>
					</div>
				</div>
				
				
				<div class="input-row pull-right">
					<div class="full">
						<div class="input-block">
							<label>First Name :</label>
							<span class='reg_span'><input type="text" id='fname' name="fname" value="<?php echo $fname; ?>" class="inputbox-main"></span>
						</div>
					</div>
				</div>
				
				<div class="input-row">
					<div class="full">
						<div class="input-block">
							<label>Last Name :</label>
							<span class='reg_span'><input type="text" id='lname' class="inputbox-main" type="text" name="lname" value="<?php echo $lname; ?>"></span>
						</div>
					</div>
				</div>
				<div class="input-row pull-right">
					<div class="full">
						<div class="input-block">
							<label>Email :</label>
							<span class='reg_span'><input <?php echo $email_type; ?> type="text" id='email' class="inputbox-main" type="text" name="email" value="<?php echo $email; ?>"></span>
						</div>
					</div>
				</div>
				<div class="input-row">
					<div class="full">
						<div class="input-block">
							<label>Username :</label>
							<span class='reg_span'><input <?php echo $username_type; ?> type="text" id='username' class="inputbox-main" type="text" name="username" value="<?php echo $username; ?>"></span>
						</div>
					</div>
				</div>
				
				<?php  if($page_title=='Add User') { ?>
				<div class="input-row pull-right">
					<div class="full">
						<div class="input-block">
							<label>Password :</label>
							<span class='reg_span'><input type="password" id='pwd' class="inputbox-main" type="text" name="pwd" value=""></span>
						</div>
					</div>
				</div>
				
				<div class="input-row">
					<div class="full">
						<div class="input-block">
							<label>Confirm Password :</label>
							<span class='reg_span'>
								<input type="password" id='confirmPwd' class="inputbox-main" type="text" name='confirmPwd' value="">
							</span>
						</div>
					</div>
				</div>
				<?php } ?>
				
				<div class="input-row pull-right">
					<div class="full">
						<div class="input-block">
							<label>Account Type :</label>
							<span class='reg_span'>
								<select <?php echo $role_type; ?> id='role' class="inputbox-main input_select"  name="role" onchange="acccountType();">
									<option value="">Select Account Type</option>
									<option value="therapist" <?php if($role=='therapist') { echo 'selected'; } ?>>Therapist</option>
									<option value="client" <?php if($role=='client') { echo 'selected'; } ?>>Client</option>
								</select>
							</span>
						</div>
					</div>
				</div>
				
				
				
				<?php foreach($therapist_list as $therapist) 
					{ 
						if($therapist->id == $selectedTherapist) 
						{ 
							$thera_pist =  $therapist->username; 
						}
						else
						{
							$thera_pist ='';
						} 
					}
				 ?>
				<div class="forClient" style="<?php if($role=='client') { echo "display:block"; } else { echo "display:none"; } ?>">
					<div class="input-row">
						<div class="full">
							<div class="input-block">
								<label>Therapist Username:</label>
								<span class='reg_span'>
									<input type="text" id='therapist' class="inputbox-main" type="text" name="therapist" value="<?php echo $thera_pist; ?>">
								</span>
							</div>
						</div>
					</div>
					<div class="input-row pull-right">
					<div class="full">
						<div class="input-block">
							<label>Language :</label>
							<span class='reg_span'>
								<select id='language' class="inputbox-main input_select"  name="language">
								<option value="">Select Language</option>
								<option value="english" <?php if($lang=='english') { echo 'selected'; } ?>>English</option>
								<option value="spanish" <?php if($lang=='spanish') { echo 'selected'; } ?>>Spanish</option>
								</select>
							</span>
						</div>
					</div>
				</div>
				</div>
									
				<div class="input-row">
					<div class="full">
						<div class="input-block">
							<label>Location :</label>
							<span class='reg_span'>
								<input class="inputbox-main" type="text" id="register_location" name="register_location" value="<?php echo $location; ?>"/>
							</span>
						</div>
					</div>
				</div>
				
				<div class="input-row pull-right">
					<div class="full">
						<div class="input-block">
							<label>Time Zone :</label>
							<span class='reg_span'>
								<select class="inputbox-main input_select" id="register_timezone" name="register_timezone">
									<option value="">Select Time Zone</option>
									<?php foreach($timezone as $zone) { ?>
									<option value="<?php echo $zone['timezone_id']; ?>" <?php if($timezone_select==$zone['timezone_id']) { echo "selected"; } ?>><?php echo $zone['timezone']; ?></option>
									<?php } ?>
								</select> 
							</span>
						</div>
					</div>
				</div>
				
				<div class="input-row pull-left">
					<div class="full">
						<div class="input-block">
							<label>Subscription :</label>
							<span class='reg_span'>
								<select class="inputbox-main input_select" id="suscription" name="suscription">
									<option value="free">Free</option>
									<option value="3">3 months</option>
									<option value="12">12 months</option>
								</select> 
							</span>
						</div>
					</div>
				</div>
				<div class="forTherapist" style="<?php if($role=='therapist') { echo "display:block"; } else { echo "display:none"; } ?>">
				<?php if($profile_image) { 
					$src=base_url().'/uploads/users/'.$profile_image;	
				}
				else
				{
					$src=base_url().'/uploads/users/';
				}
					?>
					<div class="input-row pull-right">
						<div class="full">
							<div class="input-block">
								<label>Image :</label>
								<span class='reg_span'><input type="file" name="therapist_image">
								<?php if($page_title=='Edit User') { ?>
								<img src="<?php echo $src; ?>" style="top:-10px;" height="60" width="60">
								<?php } ?>
								</span>
							</div>
						</div>
					</div>
					
					
				</div>
				<div class="input-row full-input-width">
					<div class="full">
						<div class="input-block">
							<label></label>
								<input type="checkbox" name="newmessage" id="newmessage" <?php echo $emailfornewmsg; ?>/>
								<span class="agreebox"> Receive email when new message is received. </a> </span>
						</div>
					</div>
				</div>
				<div class="input-row full-input-width">
					<div class="full">
						<div class="input-block">
							<label></label> 
								<input type="checkbox" name="newactitity" id="newactitity" <?php echo $emailfornewactivity; ?>/>
								<span class="agreebox"> Receive email when new activity is recorded. </a> </span>
						</div>
					</div>
				</div>
				<div class="input-row full-input-width">
					<div class="full">
						<div class="input-block">
							<label></label>
								<input type="checkbox" name="receivenews" id="receivenews" <?php echo $newupdateemail; ?>/>
								<span class="agreebox"> Receive news updated via email. </a> </span>
						</div>
					</div>
				</div>
				
				<div class="input-row full-input-width">
					<div class="full">
						<div class="input-block">
							<label></label>
								<input type="checkbox" name="dataidentified" id="dataidentified" <?php echo $datausedforresearch; ?>/>
								<span class="agreebox"> I agree for my data (deidentified) to be used for research. </a> </span>
						</div>
					</div>
				</div>
				
				
				<div class="input-row">
					<div class="full">
						<div class="input-block">
							<label> &nbsp; </label>
							<span class='reg_span reg_span_btn'>
								<input type="submit" value="Submit" class="btn-submit btn"> 
								<input type="button" value="Cancel" onclick="javascript:history.go(-1);" class="btn-submit btn"> 
							</span>
						</div>
					</div>	
				</div>
				</form>
			</div>
		</div>
	</div>
</form>
	
<script type="text/javascript">
	$(document).ready(function () {
			  var value = $("input[name=change_pwd]:checked", "#add_user_form").val();
			 	if(value=="1"){
					$('.change_pwd_block').show();
					$('#password').attr('name', 'password');
				} else if(value=="0"){
					$('.change_pwd_block').hide();
					$('#password').attr('name', 'changed');
				}
			$("input:radio[name=change_pwd]").click(function() {
				var value = $(this).val();
				if(value=="1"){
					$('.change_pwd_block').show();
					$('#password').attr('name', 'password');
				} else {
					$('.change_pwd_block').hide();
					$('#password').attr('name', 'changed');
				}
			});
					   
		});
	  $(function() {
		  
		  var hospitalid=$("#hospitalid").val();
		$("#add_user_form").validate({
					rules: {
						fname: "required",
						lname: "required",
							pwd: {
								required: true,
								minlength: 5
							},
							confirmPwd: {
								equalTo: "#pwd"
							},
						email: {
							required: true,
							email: true
						},
						
						role: "required",
						username: "required",
						language: "required",
						
						therapist:{
								required:true,
								remote: {
									url: "<?php echo base_url();?>frontend/home/check_therapist",
									type: "post",
									data: {
									username: function() {
										return $( "#therapist" ).val();
									  }
									}
								}
							},
					},
					messages: {
					fname: "Please enter first name.",
					lname: "Please enter last name.",
					pwd: {
						required: "Please enter a password.",
						minlength: "Your password must be at least 5 characters long."
					},
					
					confirmPwd:"Your confirmation password do not match.",
					
					email: {
						required: "Please enter an email address.",
						email: "Please enter a valid email address."
					},
					role: "Please select account type.",
					username: "Please enter username.",
					language: "Please select language.",
					therapist: {
									required:"Please enter therapist username.",
									remote:"Therapist doesn't exist, try another."
								},
				},
			 errorElement:"div",
			errorClass:"valid-error",
			submitHandler: function(form) {
				form.submit();
			}
		});
	});
	  
	 function cancelButton()
	  {
		location.assign("<?php echo base_url(); ?>snadmin/dashboard");
	  } 
	  
	  function acccountType()
	  {
	   var type=$('#role').val();
	   if(type=='therapist')
	   {
	    $('.forTherapist').show();
	    $('.forClient').hide();
	   }
	   else if(type=='client')
	   {
		 $('.forTherapist').hide();
	     $('.forClient').show();
	   }
	   else
	   {
	    $('.forTherapist').hide();
	    $('.forClient').hide();
	   }
	  }
	</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title>Dashboard</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/snadmin/css/style.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/snadmin/css/font-awesome.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/snadmin/css/font-awesome.min.css"/>
<script src="<?php echo base_url(); ?>assets/js/jquery-1.9.0.min.js"></script>
<script>
	
$(document).on('click','.slide-menu',function(){
   // $("#slide-menu").click(function(){
        $(this).find(".sub-menu").slideToggle();
        $(this).find('.fa-angle-down, .fa-angle-up').toggleClass('fa-angle-down fa-angle-up');
        //$(".A, .B").toggleClass("A B");
   // });
});

</script>

</head>

<body>
	
	<header>
		
		<div id="wrapper2">
			
			<div class="header-base">
				
				
				<div class="logo-div">
					<a href ="<?php echo base_url();?>admin/dashboard"><img src="<?php echo base_url(); ?>assets/snadmin/images/logo.png"/></a>
				</div>
				
				<div class="header-right-main">
				<nav  class="dashboard-nav">
					<div class="welcometxt"> <h2>Welcome <span><?php echo $admin_name; ?></span></h2></div>
					<ul><li ><a href="<?php echo base_url()?>snadmin/logout" class="active sign-out">Logout</a></li>
						
					</ul>
				</nav>
				
				
				<div class="navi-outer">
					<ul>
						<li> <a class="active" href="<?php echo base_url(); ?>snadmin/dashboard"> Dashboard </a> </li>
						<li> <a href="<?php echo base_url(); ?>snadmin/changepassword"> Change Password  </a> </li>
						<li class="slide-menu"> <a href="#"> Manage Users <i class="fa fa-angle-down menu-down"></i> </a> 
							<ul class="sub-menu">
								<li> <a href="<?php echo base_url(); ?>snadmin/manage_user/task/search/search/client"> Clients  </a> </li>
								<li> <a href="<?php echo base_url(); ?>snadmin/manage_user/task/search/search/therapist"> Therapists  </a> </li>
							</ul>
						</li>
						<li> <a href="<?php echo base_url();?>snadmin/manage_languages"> Manage Languages  </a> </li>
						
						<li> <a href="<?php echo base_url();?>snadmin/subscription/manage"> Subscription Plans </a> </li>
						
						<li class="slide-menu"> <a class="<?php if($page_title=='Manage Newsletters') { echo 'active'; } ?>"href="#"> Newsletters <i class="fa fa-angle-down menu-down"></i> </a> 
							<ul class="sub-menu">
								<li> <a href="<?php echo base_url();?>snadmin/manage_newsletters"> Manage Newsletters  </a> </li>
								<li> <a href="<?php echo base_url(); ?>snadmin/manage_email_template"> Manage Email Template  </a> </li>
							</ul>
						</li>
					</ul>
				</div>
				</div>
				
			</div>
			
		</div>
		
	</header>
	
	
<div class="border-div"></div>	

<div class="clr"></div>


<div class="regi-main dashboard-main-outer">
	
	<div id="wrapper2">
		<!--dashboard-->
		<div class="dashboard-outer">
			<div class="one-third-block">
				<a href="<?php echo base_url(); ?>snadmin/changepassword"><div class="dashboard-icon-block">
					<div class="icon-dash interpreter"></div>
					<div class="dash-content"><h2>Change Password</h2></div>
				</div></a>
			</div>
			<div class="one-third-block">
				<a href="<?php echo base_url(); ?>snadmin/manage_user/task/search/search/client"><div class="dashboard-icon-block">
					<div class="icon-dash client"></div>
					<div class="dash-content "><h2>Clients</h2></div>
				</div></a>
			</div>
			<div class="one-third-block">
				<a href="<?php echo base_url(); ?>snadmin/manage_user/task/search/search/therapist"><div class="dashboard-icon-block">
					<div class="icon-dash therapist"></div>
					<div class="dash-content "><h2>Therapists</h2></div>
				</div></a>
			</div>
			<!--div class="one-third-block">
				<a href="<?php echo base_url(); ?>snadmin/manage_languages"><div class="dashboard-icon-block">
					<div class="icon-dash files"></div>
					<div class="dash-content "><h2>Manage Languages</h2></div>
				</div></a>
			</div-->
			<div class="one-third-block">
				<a href="#"><div class="dashboard-icon-block">
					<div class="icon-dash calender"></div>
					<div class="dash-content "><h2>Calender</h2></div>
				</div></a>
			</div>
			<div class="one-third-block">
				<a href="#"><div class="dashboard-icon-block">
					<div class="icon-dash workshop"></div>
					<div class="dash-content "><h2>Workshops</h2></div>
				</div></a>
			</div>
			<div class="one-third-block">
				<a href="#"><div class="dashboard-icon-block">
					<div class="icon-dash files"></div>
					<div class="dash-content "><h2>My Files</h2></div>
				</div></a>
			</div>
			

		</div>
		<!--dashboard end-->
	</div>
	
</div>

<footer>
	<div class="wrapper footer-base">
		<p>Copyright&copy;2015 securenest.org. All rights reserved </p>
	</div>
</footer>
</body>

</html>

<html>

<head>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<title><?php echo $page_title; ?></title>

	<script src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.sumoselect.js"></script>
	<link href="<?php echo base_url(); ?>assets/interprator/css/sumoselect.css" rel="stylesheet" />
	<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>

	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/interprator/css/style.css"/>
	<style>
	.button-login { margin-top: 22px;}
	</style>
	
	<script >
		setTimeout(function(){ $('#succ_flash').fadeOut('slow'); }, 4000);
	</script>
</head>

<body>
	
	<header>
		
		<div id="wrapper2">
			
			<div class="header-base">
				
				<div class="logo-div">
					<a href ="<?php echo base_url();?>">
						<img src="<?php echo base_url(); ?>assets/interprator/images/AlphaVMI.png"/>
					</a>
				</div>
				
			</div>
			
		</div>
		
	</header>
	
	
<div class="border-div"></div>	




<div class="regi-main forget_regi-main <?php if($this->session->flashdata('success')!='' || $this->session->flashdata('fail')!='') { echo 'lognMsg';} ?>">

		
		<div class="regi-base login_main">
					
						<div class="login-box">
							<?php if($this->session->flashdata('success'))
								{?> 
								<div class="messages forgot_pass_Succmsg">
								<div class="alert alert-success success_msg_forgot" id="succ_flash"><?php echo $this->session->flashdata('success');?></div>
								</div>
								<?php } ?>	
								<?php if($this->session->flashdata('fail'))
								{?> 
								<div class="messages forgot_pass_msg">
								<div class="alert alert-error" id="succ_flash"><?php echo $this->session->flashdata('fail');?></div>
								</div>
								<?php } ?>	
						<h2><img src="<?php echo base_url(); ?>assets/interprator/images/login_icon.png"/><div class="login_title"><span>Forgot Password</span><p>of your account</p></div></h2>
						<form method="post" action="" class="form-horizontal" id="login-form">
								<fieldset>
							
								<div class="input-prepend">
									<label>E-mail:</label>
									<input type="text" name="email" value=""/>
									<?php echo form_error('email','<div class="login-error">', '</div>'); ?>
								</div>
								<div class="clearfix"></div>

								<div class="button-login">	
									<button class="btn login" type="submit">Submit</button>
									<a href="<?php echo base_url();?>/interpreter/login" class="cancel_link">Cancel</a>
								</div>
								
								</fieldset>
								
					</form>
					</div>			

		
	</div>

<script>
	$(function() {
		$("#login-form").validate({
			rules: {
			  email: {
					required: true,
					email: true
				},
			},
			messages: {
				email: {
					required: "Please enter an email address.",
					email: "Please enter a valid email address."
				},
			},
			errorElement:"div",
			errorClass:"login-error",
			submitHandler: function(form) {
			form.submit();
			}
		});

	});
</script>

</div>

<footer>
	<div class="wrapper">
		<p>Copyright &copy; Alphavmi.com</p>
	</div>
</footer>
	
</body>

</html>

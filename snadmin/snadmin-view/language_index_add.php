<script src="<?php echo base_url(); ?>assets/js/jquery-1.9.0.min.js"></script>
	<!--script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script-->
	<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<div class="account-right-div">
					<div class="dashboard-heading"><h2><?php echo $page_title; ?></h2></div>
					<?php echo $snbreadcrum; ?>
					<div class="dashboard-inner language_section">
						<div class="main-dash-summry Edit-profile">
						  <form name="frmEditGroup" id="frmEditGroup" action="<?php echo base_url(); ?>snadmin/add_new_translation" method="post">
							<div class="input-row">
								<div class="full">
									<div class="input-block">
										<label>Langauge Index</label>
										<span class='reg_span'>
											<input type="text" name="text_index" id="text_index" class="inputbox-main specialChar" value=""  />
											
										</span>
									</div>
								</div>
							</div>
							<?php
							$languages = $this->session->userdata('languages');
							//~ print_r($languages); die;
							if($languages)
							{
								foreach ($languages as $language)
								{
									?>	
								<div class="input-row">
								<div class="full">
									<div class="input-block">
										<label>
											Text (<?php print $language['folder'];?>)
										</label>
										<span class='reg_span'>
											<input type="text" name="field_<?php print $language['folder'];?>" id="field_<?php print $language['folder'];?>" class="inputbox-main" value="" />
										</span>
								</div>	</div>	
							</div>
									<?php
								}
							}
							?>
							
							<div class="input-row">
								<div class="full">
									<div class="input-block">
										
										<span class='reg_span reg_span_btn'><input type="submit" value="Submit" class="btn-submit btn"> <input type="button" value="Cancel" onclick="cancelButton();" class="btn-submit btn"> </span>
									</div>
								</div>	
							</div>
							</form>
						</div>
					</div>
				</div>

	<script type="text/javascript">
		
		$(document).ready(function(){
		  //$("#myform").validate();
		  jQuery.validator.addMethod("specialChar", function(value, element) {
				var fn = $("#text_index").val();
				var regex = /^[0-9a-zA-Z\_]+$/
				//alert(regex.test(fn));
				if(regex.test(fn) == 1)
				{
					return true;
				}
				else
				{
					return false;
				}
		  });
		});
		
		$(function()
		{
			$("#frmEditGroup").validate({
				rules: {
					text_index: { 	required:true,
									specialChar:true
								},	
					field_en: "required",
					field_sp: "required"
				},
			   
				messages: {
					text_index: { required: "Please enter language index.",
								specialChar: "Only special character underscore'_' is allowed."
							},
					field_en: "Please enter english language text",
					field_sp: "Please enter spanish language text"
					},
				
				 errorElement:"div",
				errorClass:"valid-error",
				submitHandler: function(form) {
					form.submit();
				}
			});
		});
	
	 function cancelButton()
	  {
		location.assign("<?php echo base_url(); ?>snadmin/manage_languages");
	  } 
	</script>

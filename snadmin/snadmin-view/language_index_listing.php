<style>
	.my_table_div .action-main-block a{
	border-radius: 3px;
    width: 33px;}
    
    .search_form {
	  width: 75%;
	  float:left;
	  margin-bottom: -20px;
	}
    .right_btns {
	  width: 25%;float:right;
	}
	.input-block span {
		width: 100%;
	}
	.input-block span.reg_span {
    float: none;
	}
	.valid-error {
		margin-top: 0px;
	}
</style>
<div class="dashboard-heading">
	<h2><?php echo $page_title; ?></h2> 
	<!--img class='searchiImg' onclick='searchList();' src='<?php echo base_url(); ?>assets/snadmin/images/search11.png'-->
	
		
	
</div>
		
<div class="dashboard-inner">
			<div class="dash-search">
				<div class="search_form">
					<form name="frm_search_lang" id="frm_search_lang" method="post" action="<?php echo base_url();?>snadmin/manage_languages">
					<div class="input-block" style="width:26%;">
						<span class="reg_span">
							<input type="text" name="search_lang_text" id="search_lang_text" class="list-search" placeholder="Enter Language Index" value='<?php echo $txtToSearch; ?>' />
						</span>
					</div>
					<span class='reg_span reg_span_btn'>
						<button class="btn-submit btn ">
							<span> <i class="fa fa-search search-icon"></i></span>
							Search
						</button>	
					</span>
                </div>
                
                <div class="right_btns">
					<a href="<?php echo base_url(); ?>snadmin/add_new_translation" class="add-user"><i></i> <span>Add Language Index</span></a>
				</div>
				</form>
			</div>
			<?php 
			
				//echo $sortAs;
				if($sortAs == 'desc')
				{
					krsort($translations);
					$index_sort_arrow = 'fa fa-angle-up menu-down';
				}
				if($sortAs == 'asc')
				{
					ksort($translations);
					$index_sort_arrow = 'fa fa-angle-down menu-down';
				}
			?>
				<div class="main-dash-summry Edit-profile nopadding11">
					<!--table-->
				<div class="my_table_div">
					<table class="fixes_layout">
						<thead>
							<tr>
								<th > <h1 class=""> S.No. </h1> </th>
								<th><a class='underline_classs' href='<?php echo base_url(); ?>snadmin/manage_languages/<?php echo $sort; ?>'>
									 <h1 class=""> 
										Language Index <i class="<?php echo $index_sort_arrow; ?>"></i>
									</h1>
								</th>
								<th > English </th>
								<th > Spanish </th>
								<!--th> <h1 class="">Manager</a></h1> </th-->
								 <th> <h1 class=""> Actions </h1> </th>
							</tr>
						</thead>
						<tbody>
						<?php
							
							//echo '<pre>';print_r($translations); die;
							$count = 1;
							if(count($translations) > 0)
							{
								$idx=0;
								$start_position = $page_number;
								$end_position = ($start_position + $page_size) - 1;
								//print $start_position.'|'.$end_position;
								foreach($translations as $index => $value)
								{
									if($idx >= $start_position && $idx <= $end_position)
									{
										$rowclass = (($idx % 2)==0) ? ' class="row1" ' : ' class="row2" ';
							?>
							<tr>
								<td><?php echo $count; $count++;?></td>
								<td><?php echo $index; ?></td>
								<td class="wide"><?php echo $value; ?></td>
								<td class="wide"><?php echo $translations_spanish[$index]; ?></td>
								<td class="action-main-block"><a data-toggle="tooltip" title="Edit Language Translation" href="<?php echo base_url().'snadmin/edit_language_translation/'.$index; ?>" class="edit">1</a></td>
								<!--td class="action-main-block">
									 <a data-toggle="tooltip" title="Edit User" href='<?php echo base_url(); ?>snadmin/upd_user/editId/<?php echo $userInfo->user_id; ?>' class="edit">1</a>
									  <a data-toggle="tooltip" title="Delete User" onclick="deluser(<?php echo $userInfo->user_id; ?>);" class="del">1</a> 
								</td-->
							 </tr>
							<?php }
							$idx++;} } else { ?>
							<tr>
							<td colspan="5" style='text-align:center;'>No result fouind.</td>
							</tr>
					  <?php } ?>
						</tbody>
												
					</table>
				<!--end-->
				</div>
			<div class='pagination'>
				<?php echo $this->pagination->create_links(); ?>
				</div>
			</div>
		</div>
		<script type="text/javascript">
		
		
		
		$(function() {
		$("#frm_search_lang").validate({
			rules: {
				search_lang_text: "required"		
			},
		   
			messages: {
				search_lang_text: "Please enter language index."
				},
			
			 errorElement:"div",
			errorClass:"valid-error",
			submitHandler: function(form) {
				form.submit();
			}
		});
	  });
			
		</script>
			
		
		

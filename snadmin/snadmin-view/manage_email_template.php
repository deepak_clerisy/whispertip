<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<!--script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script-->
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/snadmin/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/snadmin/js/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/snadmin/js/adapters/jquery.js"></script>

<script>
	$(function(){
		CKEDITOR.replace( 'template_body', {
			width :547,
			height: 200,
			toolbarGroups: [
				{"name":"document","groups":["mode"]},
				{"name":"basicstyles","groups":["basicstyles"]},
				
				{"name":"paragraph","groups":["list","blocks"]},
				
				{"name":"insert","groups":["insert"]},
				{"name":"styles","groups":["styles"]},
				{"name":"links","groups":["links"]},
 				
			],
			// Remove the redundant buttons from toolbar groups defined above.
			removeButtons: 'Anchor'
		});
	});

</script>
 <?php //echo '<pre>';print_r($allUsers); ?>
	<form method="post" name="frmEmailTemplate" id="frmEmailTemplate" enctype="multipart/form-data"> 
		<div class="account-right-div">
			<div class="dashboard-heading"><h2><?php echo $page_title; ?></h2></div>
			<?php
				$from_name = $from_email = $template_body = '';
				if(isset($email_template_data['from_name'])) $from_name = $email_template_data['from_name'];
				if(isset($email_template_data['from_email'])) $from_email = $email_template_data['from_email'];
				if(isset($email_template_data['template_body'])) $template_body = htmlspecialchars_decode($email_template_data['template_body']);
			?>
			<div class="dashboard-inner">
				<div class="main-dash-summry Edit-profile">
					<div class="input-row full-input-width">
						<div class="full">
							<div class="input-block">
								<label>From Name :  </label>
								<span class='reg_span'><input type="text"  id="from_name" class="inputbox-main" name="from_name" value="<?php echo $from_name; ?>" placeholder="Enter Name"></span>
							</div>
						</div>
					</div>
					<div class="input-row full-input-width">
						<div class="full">
							<div class="input-block">
								<label>From Email :  </label>
								<span class='reg_span'><input type="text"  id="from_email" class="inputbox-main" name="from_email" value="<?php echo $from_email; ?>" placeholder="Enter Email"></span>
							</div>
						</div>
					</div>
					<div class="input-row full-input-width">
						<div class="full">
							<div class="input-block">
								<label>Email Body :  </label>
								<span class='reg_span' style="height:auto;"><textarea id="template_body" class="" name="template_body"><?php echo $template_body; ?></textarea></span>
							</div>
						</div>
					</div>
					<div class="input-row">
						<div class="full">
							<div class="input-block">
								<label> &nbsp; </label>
								<span class='reg_span reg_span_btn'><input type="submit" value="Submit" class="btn-submit btn">
								<input type="button" value="Cancel" onclick="cancelButton()" class="btn-submit btn">
								 </span>
							</div>
						</div>	
					</div>
					
					
				</div>
			</div>
		</div>
	</form>
	
<script type="text/javascript">
	
	  $(function() {
		$("#frmEmailTemplate").validate({
			rules: {
				from_name: "required",
				from_email: "required",
			    emailBody: 
				{
					required: function() 
					{
						CKEDITOR.instances.emailBody.updateElement();
					}
				},
                
			},
		   
			messages: {
				from_name: "Please enter name",
				from_email: "Please enter email",
			    emailBody: "Please enter body",
			},
			errorPlacement: function(error, element) 
			{
				if (element.attr("name") == "emailBody") 
				{
					error.insertAfter("#cke_emailBody");
				}
				else 
				{
					error.insertAfter(element);
				}
			},
			errorElement:"div",
			errorClass:"valid-error",
			submitHandler: function(form) {
				form.submit();
			}
		});
	  });
	  
	  function cancelButton()
	  {
		location.assign("<?php echo base_url(); ?>snadmin/dashboard");
	  } 
	</script>
	
	<style>
		.list_mrgn{ margin-bottom:13px; width:245px;}
	</style>

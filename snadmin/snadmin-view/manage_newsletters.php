<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<!--script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script-->
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/snadmin/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/snadmin/js/bootstrap-multiselect.js"></script>
<script src="<?php echo base_url(); ?>assets/snadmin/js/ckeditor/ckeditor.js"></script>
<link href="<?php echo base_url(); ?>assets/snadmin/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/snadmin/css/bootstrap-multiselect.css" rel="stylesheet">

<script>
	$(function(){
		
		$("#newsletterUsers").multiselect({
			includeSelectAllOption : true,
			selectAllText: 'Select all users',
			maxHeight: 200,
			buttonText: function(options, select) {
				var total_options = $("#newsletterUsers option").length;
				if (options.length === 0) {
					return "Select a user"+' <b class="caret"></b>';
				}
				else if(options.length === total_options)
				{
					return "All Users"+'  <b class="caret"></b>' ;
				}
				else if (options.length > 10) {
					var labels = [];
					var i = 1;
					for(i=0; i<=1; i++){
						var option = $(options[i]);
						 if (option.attr('label') !== undefined) {
							 labels.push(option.attr('label'));
						 }
						 else {
							 labels.push(option.html());
						 }
					 };
					 return labels.join(', ') + '...  <b class="caret"></b>';
				}
				else {
					 var labels = [];
					 options.each(function() {
						 if ($(this).attr('label') !== undefined) {
							 labels.push($(this).attr('label'));
						 }
						 else {
							 labels.push($(this).html());
						 }
					 });
					 return labels.join(', ') + '  <b class="caret"></b>';
				 }
			}
			
		});
		$(".message_type").change(function(){
			var value = $(this).val();
			$('.msg_type').hide();
			$('#' + value + "_msg_type").show();
		});
		$(".message_type[value='template']").trigger('click');

		CKEDITOR.replace( 'newsletterMessage', {
			width :547,
			height: 200,
			// Define the toolbar groups as it is a more accessible solution.
			toolbarGroups: [
				{"name":"document","groups":["mode"]},
				{"name":"basicstyles","groups":["basicstyles"]},
				
				{"name":"paragraph","groups":["list","blocks"]},
				
				{"name":"insert","groups":["insert"]},
				{"name":"styles","groups":["styles"]},
				{"name":"links","groups":["links"]},
 				
			],
			// Remove the redundant buttons from toolbar groups defined above.
			removeButtons: 'Anchor'
		});
	});
	$(".multiselect").addClass('inputbox-main');
</script>


 <?php //echo '<pre>';print_r($allUsers); ?>
	<form method="post" name="frmNewsletter" id="frmNewsletter" enctype="multipart/form-data"> 
		<div class="account-right-div">
			<div class="dashboard-heading"><h2><?php echo $page_title; ?></h2></div>
			
			<div class="dashboard-inner">
				<div class="main-dash-summry Edit-profile">
					<div class="input-row full-input-width">
						<div class="full">
							<div class="input-block">
								<label>Users :</label>
								<span class='reg_span'>
									<select multiple name="newsletterUsers[]" id="newsletterUsers" class="list-search list_mrgn " id="list-search">
										<?php foreach($allUsers as $user){ ?>
											<option value="<?php echo $user->id; ?>" ><?php echo $user->username; ?></option>
										<?php } ?>		
									</select>
								</span>
							</div>
						</div>
					</div>
					<div class="input-row full-input-width">
						<div class="full">
							<div class="input-block">
								<label>Subject :  </label>
								<span class='reg_span'><input type="text"  id="newsletterSubject" class="inputbox-main" name="newsletterSubject" value="" placeholder="Enter Subject"></span>
							</div>
						</div>
					</div>
					<div class="input-row full-input-width">
						<div class="full">
							<div class="input-block">
								<label>Message :  </label>
								<span class='reg_span' style="height:auto;"><textarea id="newsletterMessage" class="" name="newsletterMessage" value=""></textarea></span>
							</div>
						</div>
					</div>
					<div class="input-row">
						<div class="full">
							<div class="input-block">
								<label> &nbsp; </label>
								<span class='reg_span reg_span_btn'><input type="submit" value="Submit" class="btn-submit btn">
								<input type="button" value="Cancel" onclick="cancelButton()" class="btn-submit btn">
								 </span>
							</div>
						</div>	
					</div>
					
					
				</div>
			</div>
		</div>
	</form>
	
<script type="text/javascript">
	
	  $(function() {
			$.validator.addMethod("needsSelection", function(value, element) {
				return $(element).multiselect("getChecked").length > 0;
			});
			$.validator.messages.needsSelection = 'Select a user.';
		$("#frmNewsletter").validate({
			ignore:[],
			rules: {
				newsletterMessage: 
				{
					required: function() 
					{
						CKEDITOR.instances.newsletterMessage.updateElement();
					}
				},
                newsletterUsers: {
					'required': true,
					'needsSelection': true
				},
				newsletterTemplates: "required",
			    newsletterSubject: "required"			
			},
		   
			messages: {
				newsletterUsers: "Please select user.",
				newsletterTemplates: "Please select template.",
				newsletterSubject:"Please enter subject.",
				newsletterMessage:"Please enter Message"
			},
			errorPlacement: function(error, element) 
			{
				if (element.attr("name") == "newsletterMessage") 
				{
					error.insertAfter("#cke_newsletterMessage");
				} 
				else if (element.attr("name") == "newsletterUsers") 
				{
					error.insertAfter("button.multiselect");
				} 
				else 
				{
					error.insertAfter(element);
				}
			},
			errorElement:"div",
			errorClass:"valid-error",
			submitHandler: function(form) {
				form.submit();
			}
		});
	  });
	  
	  function cancelButton()
	  {
		location.assign("<?php echo base_url(); ?>snadmin/dashboard");
	  } 
	</script>
	
	<style>
		.list_mrgn{ margin-bottom:13px; width:245px;}
	</style>

<script>
    function add_user()
    {
        window.location = "<?php echo base_url(); ?>snadmin/upd_user";
    }
    function deluser(delId)
    {
        if(confirm('Are you sure, you want to delete ?')) {
            $.ajax({url: '<?php echo base_url(); ?>snadmin/delUser/deleteId/'+delId,
                success: function (result) {
                    location.reload();
                },
                error: function (request,error) {
                            //alert('Network error has occurred please try again!');
                        }
                    });
        }
    }
    /*$(document).ready(function() {    
    $('#getSearch').keypress(function(e){
                     if (e.keyCode == 13){
                         var search=$(this).val();
                         if(search!='')
                          {
                           window.location = "<?php echo base_url(); ?>snadmin/manage_user/task/search/search/"+search;
                          }
                          else
                          {
                           window.location = "<?php echo base_url(); ?>snadmin/manage_user/";
                          }
                     }
                });   
        });
*/
function searchUser()
{
    var search=$('#getSearch').val();
    var searchUserEmail=$('#searchUserEmail').val();
      //alert(searchUserEmail);
     
      if(search!='')
      {
          window.location = "<?php echo base_url(); ?>snadmin/manage_user/task/search/search/"+search;
      }
      else
      {
          window.location = "<?php echo base_url(); ?>snadmin/manage_user/";
      }
    }   

  /* 
    function listUser(userId)
    {
      window.location = "<?php echo base_url(); ?>snadmin/list_user/task/listUser/userId/"+userId;
    }
    */
    
    
</script>
<style>
	..btn{
		float:right !important;
		}
</style>
<?php
//echo $sortBy;die;

$sort_arrow_uname='fa fa-angle-down menu-down';
$sort_arrow_email='fa fa-angle-down menu-down';
$sort_arrow_accType='fa fa-angle-down menu-down';

$array = $this->uri->uri_to_assoc(3);
if(array_key_exists ('task' , $array)) {
    if($array['task']=='search')
    {
        $search=$array['search'];
        $sort='asc';
    }
    else if($array['task']=='sorting')
    {
        if($array['order']=='asc')
        {   
			if($sortBy == 'username')
            {
				$sort='desc';
				$search='';
            
				$sort_arrow_uname='fa fa-angle-up menu-down';
			}
			if($sortBy == 'email')
			{
				$sort='desc';
				$search='';
				
				$sort_arrow_email='fa fa-angle-up menu-down';
			}
            if($sortBy == 'account_type')
            {
				$sort='desc';
				$search='';
				
				$sort_arrow_accType='fa fa-angle-up menu-down';
			}
            
            
        }
        else
        {
            
            if($sortBy == 'username')
            {
				$sort='asc';
				$search='';
				
				$sort_arrow_uname='fa fa-angle-down menu-down';
			}
			if($sortBy =='email')
			{
				$sort='asc';
				$search='';
				
				$sort_arrow_email='fa fa-angle-down menu-down';
			}
            if($sortBy == 'account_type')
            {
				$sort='asc';
				$search='';
				
				$sort_arrow_accType='fa fa-angle-down menu-down';
			}
            
        }
    }
    else
    {
       
        if($sortBy == 'username')
		{
			$sort='asc';
			$search='';
			
			$sort_arrow_uname='fa fa-angle-down menu-down';
		}
		if($sortBy == 'email')
		{
			$sort='asc';
			$search='';
			
			$sort_arrow_email='fa fa-angle-down menu-down';
		}
		if($sortBy == 'account_type')
		{
			$sort='asc';
			$search='';
			
			$sort_arrow_accType='fa fa-angle-down menu-down';
		}
    }
}
else
{
    $sort='asc';
    $search='';
}


if(!empty($search_type)){
    $search = $search_type;
}

?>
    <!--ul style='float:right;'>
    <?php $i=0; if(count($user_data)) { $i++; ?>
   
    <li><a href=''><?php echo 1; ?></a></li>
    <li><a href=''><?php echo 2; ?></a></li>
    <li><a href=''><?php echo 3; ?></a></li>
    <li><a href=''><?php echo 4; ?></a></li>
   
    <?php } ?></ul-->
   
    <!--link rel="stylesheet" href="<?php echo base_url(); ?>assets/snadmin/css/bootstrap.min.css"-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/snadmin/js/bootstrap.min.js"></script>   
   
    <div class="dashboard-heading"><h2><?php echo $page_title; ?></h2>
        <!--img class='searchiImg' onclick='searchList();' src='<?php echo base_url(); ?>assets/snadmin/images/search11.png'-->

        <!--input id='getSearch' type="Search" value="<?php echo $search; ?>" placeholder='Search by user name' class="list-search"-->
        <!--label class='searchby'>Search by user name</label-->

    </div>

    <div class="dashboard-inner">
        <div class="dash-search">
            <form name="frmSearchUser" id="frmSearchUser" method="post" action="<?php echo base_url();?>snadmin/manage_user">                           
                <select name="getSearch" id="getSearch" class="list-search" id="list-search">
                    <option value="all" <?php if($search=='all') { echo "selected"; } ?>>All</option>
                    <option value="client" <?php if($search=='client') { echo "selected"; } ?>>Client</option>
                    <option value="therapist" <?php if($search=='therapist') { echo "selected"; } ?>>Therapist</option>
                </select>

                <input type="text" value="<?php echo $email;?>" name="searchUserEmail" id="searchUserEmail" class="list-search" placeholder="Username or Email" />
                <!--input type="submit" class="btn-submit btn cl" value="Search"  /-->


                <!-- <button class="btn-submit btn" onclick="searchUser()"> <span> <i class="fa fa-search search-icon"></i> -->
                <button class="btn-submit btn">
                    <span> <i class="fa fa-search search-icon"></i></span>
                    Search
                </button>
                <a href="#" class="add-user" onclick='add_user()'><i></i> <span>Add User</span></a>
            </form>
        </div>

        <div class="main-dash-summry Edit-profile nopadding11">
            <!--table-->
            <div class="my_table_div">
                <table class="fixes_layout">
                    <thead>
                        <tr>
                            <th class="forWidthSno"> <h1 class=""> S.No. </h1> </th>
                            <th>
								<a class='underline_classs' href='<?php echo base_url(); ?>snadmin/manage_user/task/sorting/sort/username/order/<?php echo $sort; ?>'>
									<h1 class="">
										User Name <i class="<?php echo $sort_arrow_uname; ?>"></i>
									</h1>
								</a>
                            </th>
                            <th class="forWidth">
								<a class='underline_classs' href='<?php echo base_url(); ?>snadmin/manage_user/task/sorting/sort/email/order/<?php echo $sort; ?>'>
									<h1 class=""> 
										Email <i class="<?php echo $sort_arrow_email; ?>"></i>
									</h1>
								</a>	
							</th>
                            <th>
								<a class='underline_classs' href='<?php echo base_url(); ?>snadmin/manage_user/task/sorting/sort/account_type/order/<?php echo $sort; ?>'>
									<h1 class=""> 
										Account Type <i class="<?php echo $sort_arrow_accType; ?>"></i>
									</h1>
								</a>	
							</th>
                            <th> <h1 class=""> Status </h1> </th>
                            <th> <h1 class=""> Actions </h1> </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                                            //echo '<pre>';print_r($user_data);
                        if(array_key_exists ('page' , $array))
                        {
                            if($array['page'])
                            {
                                $i=$array['page'];   
                            }else
                            {
                                $i=0;   
                            }                                   
                        }
                        else
                        {
                            $i=0;
                        }
                        if(!empty($user_data)) { foreach($user_data as $userInfo) { $i++;
                                    /*if($userInfo->logo)
                                        {
                                         $img=base_url().'uploads/user/'.$userInfo->logo;
                                        }
                                        else
                                        {
                                         $img=base_url().'assets/snadmin/images/default_shape.png';
                                        }
                                        */
                                        
                                        if($userInfo->status == 1)
                                        {
                                            $img = base_url().'assets/snadmin/images/status-active.png';
                                            $title="Active";
                                        }
                                        else
                                        {
                                            $img = base_url().'assets/snadmin/images/status-inactive.png';
                                            $title="Inactive";
                                        }
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td class="wide"><span data-toggle="tooltip" title="<?php echo $userInfo->username; ?>" ><?php echo $userInfo->username; ?></span></td>
                                            <td class="email_word"><?php echo $userInfo->email; ?></td>
                                            <td class="type_case"><?php echo $userInfo->account_type; ?></td>
                                            <!--td class="type_case"><?php echo $userInfo->account_type; ?></td-->
                                           
                                            <td ><a href="<?php echo base_url();?>snadmin/change_user_status/<?php echo $userInfo->id; ?>"><img src='<?php echo $img;?>' title='<?php echo 
                                            $title; ?>' style="cursor:pointer"/></a></td>
                                           
                                            <td class="action-main-block">
                                                <a title="Edit User" href='<?php echo base_url(); ?>snadmin/upd_user/editId/<?php echo $userInfo->id; ?>' class="edit">&nbsp;</a>
                                                <a title="Delete User" onclick="deluser(<?php echo $userInfo->id; ?>);" class="del">&nbsp;</a>
                                            </td>
                                        </tr>
                                        <?php } } else { ?>
                                        <tr>
                                            <td colspan="5" style='text-align:center;'>No result fouind.</td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>

                                </table>
                                <!--end-->
                            </div>
                            <div class='pagination'>
                                <?php echo $this->pagination->create_links(); ?>
                            </div>
                        </div>
                    </div>
                    <script>
                        $(document).ready(function(){
                            $('[data-toggle="tooltip"]').tooltip();  
                        });
                    </script>

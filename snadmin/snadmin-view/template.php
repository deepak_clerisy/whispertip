<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title><?php echo $page_title; ?></title>
<link href="<?php echo base_url(); ?>assets/snadmin/css/style.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/snadmin/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/snadmin/css/font-awesome.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/jquery-1.9.0.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.sumoselect.js"></script>
	<link href="<?php echo base_url(); ?>assets/interprator/css/sumoselect.css" rel="stylesheet" />
	<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
</head>



<script>
	
$(document).on('click','.slide-menu',function(){
   // $("#slide-menu").click(function(){
        $(this).find(".sub-menu").slideToggle();
        $(this).find('.fa-angle-down, .fa-angle-up').toggleClass('fa-angle-down fa-angle-up');
        //$(".A, .B").toggleClass("A B");
   // });
});

</script>

<body>
	
	<header>
		
		<div id="wrapper2">
			
			<div class="header-base">
				
				<div class="logo-div">
					<a href ="<?php echo base_url();?>snadmin/dashboard"><img src="<?php echo base_url(); ?>assets/snadmin/images/logo.png"/></a>
				</div>
				
				<div class="header-right-main">
				<nav  class="dashboard-nav">
					<div class="welcometxt"> <h2>Welcome <span><?php echo $admin_name; ?></span></h2></div>
					<ul><li ><a href="<?php echo base_url()?>snadmin/logout" class="active sign-out">Logout</a></li>
						
					</ul>
				</nav>
				
				<div class="navi-outer">
					<ul>
						<li> <a href="<?php echo base_url(); ?>snadmin/dashboard"> Dashboard </a> </li>
						
						<li> <a class="<?php if($page_title=='Change Password') { echo 'active'; } ?>" href="<?php echo base_url(); ?>snadmin/changepassword"> Change Password  </a> </li>
						
						<li class="slide-menu"> <a class="<?php if($page_title=='Manage Users') { echo 'active'; } ?>" href="#"> Manage Users <i class="fa fa-angle-down menu-down"></i> </a> 
							<ul class="sub-menu">
								<li> <a href="<?php echo base_url(); ?>snadmin/manage_user/task/search/search/client"> Clients  </a> </li>
								<li> <a href="<?php echo base_url(); ?>snadmin/manage_user/task/search/search/therapist"> Therapists  </a> </li>
							</ul>
						</li>
						
						<li> <a class="<?php if($page_title=='Manage Languages') { echo 'active'; } ?>" href="<?php echo base_url();?>snadmin/manage_languages"> Manage Languages  </a> </li>

						<li> <a href="<?php echo base_url();?>snadmin/subscription/manage"> Subscription Plans </a> </li>

						<li class="slide-menu"> <a class="<?php if($page_title=='Manage Newsletters') { echo 'active'; } ?>"href="#"> Newsletters <i class="fa fa-angle-down menu-down"></i> </a> 
							<ul class="sub-menu">
								<li> <a href="<?php echo base_url();?>snadmin/manage_newsletters"> Manage Newsletters  </a> </li>
								<li> <a href="<?php echo base_url(); ?>snadmin/manage_email_template"> Manage Email Template  </a> </li>
							</ul>
						</li>

					</ul>
				</div>
				</div>
			</div>
			
		</div>
		
	</header>
	
	
<div class="border-div"></div>	
<div class="clr"></div>
<script >
	setTimeout(function(){ $('#succ_flash').fadeOut('slow'); }, 2000);
</script>
<div class="hotel-listing">
	<div class="regi-main">
		
		<div id="wrapper2">
			<div class="my-account">
				<div class="messages"> 

					<?php if($this->session->flashdata('success'))
					{?> 
					<div class="alert alert-success" id="succ_flash"><img src="<?php echo base_url(); ?>assets/snadmin/images/img_1.png">
					<?php echo $this->session->flashdata('success');?><!--img style="float:right" src="<?php echo base_url(); ?>assets/snadmin/images/close_icon.jpeg"--></div>
					<?php } ?>	
					
					<?php if($this->session->flashdata('fail'))
					{?> 
					<div class="alert alert-error" id="succ_flash"><?php echo $this->session->flashdata('fail');?></div>
					<?php } ?>
				</div>
				<div class="myaccount-outer">
					<!--div class="my-account-left">
						<ul class="dashboard-listing">
							<!--li class="manage-hosptial <?php  if($page=='hospital') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>snadmin/manage_user">Manage Users </a></li-->
							<!--li class="manage-hosptial <?php  if($page=='hospital') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>snadmin/manage_languages">Manage Languages </a></li-->
							<!--li class="dashboard <?php  if($page=='dashboard') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>snadmin/dashboard">Dashboard</a></li>
							<li class="change_pwd <?php  if($page=='chg_pwd') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>snadmin/changepassword">Change Password </a></li>
							<li class="manage_user <?php  if($page=='users') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>snadmin/manage_user/task/search/search/all">Manage Users </a></li>
							<li class="manage_user <?php  if($page=='manage_lang') { echo 'active'; } ?>"><a href="<?php echo base_url(); ?>snadmin/manage_languages">Manage Languages </a></li>
						</ul-->
					</div-->
					<div class="account-right-div">
						<?php echo $body; ?>
				</div>
			
			</div>
			
		</div>
		
	</div>

</div>
<footer>
	<div class="wrapper">
		<p>Copyright&copy;2015 securenest.org. All rights reserved </p>
	</div>
</footer>
</body>

</html>

